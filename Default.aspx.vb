﻿Option Explicit On
Option Strict On
Option Compare Text
Public Class _Default
    Inherits System.Web.UI.Page
    Private WebFct As Virtualia.Net.Controles.WebFonctions
    Private SiV3_7 As Boolean = False
    Private SiTypeAccesManager As Boolean = False
    Private SiTypeAccesGRH As Boolean = False
    Private WsAppliAccedee As String
    Private WsDossierPer As Virtualia.Net.WebAppli.SelfV4.DossierPersonne = Nothing

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim ChaineIntranetV3 As String = ""
        Dim ChaineGrhV3 As String = ""
        Dim CRetour As Boolean
        Dim Msg As String = "Erreur lors de l'identification."
        Dim SessionUtlisateur As Virtualia.Net.Session.ObjetSession = Nothing
        Dim NomConnexion As String = ""
        Dim FiltreEta As String = ""
        Dim FiltreV3 As String = ""
        Dim SpecifiqueModule As Virtualia.Net.Session.SessionModule = Nothing

        Dim ObjetClefV3 As Virtualia.Net.Session.ClefV3Connexion = Nothing
        Dim OldClefV3 As Virtualia.Net.Session.ClefV3OldCnx = Nothing
        Dim ObjetTest As Virtualia.Net.Session.ClefV3Connexion

        WebFct = New Virtualia.Net.Controles.WebFonctions(Me, 1)

        '************* Version 3.7 *******************************************************************************
        ChaineIntranetV3 = Request.Form("ClefV4") 'Issu du POST - Intranet RH
        If ChaineIntranetV3 Is Nothing OrElse ChaineIntranetV3 = "" Then
            ChaineIntranetV3 = Request.QueryString("ClefV4") 'Issu du GET - VirtualiaRHV2.exe
        End If
        '** Spécifique ONISEP
        If ChaineIntranetV3 IsNot Nothing AndAlso ChaineIntranetV3 <> "" Then
            If ChaineIntranetV3 = "onisep_fp" Then
                ObjetTest = New Virtualia.Net.Session.ClefV3Connexion("")
                ChaineIntranetV3 = ObjetTest.Chaine_Test("VirtualiaIntranet", "Activite_FP", 0) 'PFR BD 8
            End If
        End If
        '** Uniquement pour test
        ObjetTest = New Virtualia.Net.Session.ClefV3Connexion("")
        'ChaineIntranetV3 = ObjetTest.Chaine_Test("Virtualia", "PFR", 0) 'PFR BD 11
        'ChaineIntranetV3 = ObjetTest.Chaine_Test("GAUTIER", "ManagerPFR", 20206) 'PFR BD 11

        'ChaineIntranetV3 = ObjetTest.Chaine_Test("Virtualia", "Activite_FP", 0) 'Onisep BD 7
        'ChaineIntranetV3 = ObjetTest.Chaine_Test("GIBRAT", "SelfActivite_FP", 23) 'BD 7 1360 = ABSALON
        'ChaineIntranetV3 = ObjetTest.Chaine_Test("ROITEL", "ManagerActivite_FP", 459) 'BD 7 459 = COBESSI
        'ChaineIntranetV3 = ObjetTest.Chaine_Test("BARON", "SelfActivite_Honda", 89)
        'ChaineIntranetV3 = ObjetTest.Chaine_Test("GAUTIER", "SelfHSupp", 20206) 'PFR BD 11
        'ChaineIntranetV3 = ObjetTest.Chaine_Test("GAUTIER", "SelfDemandeFOR", 19146)
        'ChaineIntranetV3 = ObjetTest.Chaine_Test("GAUTIER", "SelfHSupp", 38) 'PFR BD 11
        'ChaineIntranetV3 = ObjetTest.Chaine_Test("GRIFFIT", "SelfHeureSupp", 23) 'Heures Supp BD 4 BELOEIL 55, GRIFFIT 154, PRECART 13
        ChaineIntranetV3 = ObjetTest.Chaine_Test("Virtualia", "HeureSupp", 0)
        'ChaineIntranetV3 = ObjetTest.Chaine_Test("PRECART", "ManagerHeureSupp", 38)
        '**
        If ChaineIntranetV3 IsNot Nothing AndAlso ChaineIntranetV3 <> "" Then
            SiV3_7 = True
            ObjetClefV3 = New Virtualia.Net.Session.ClefV3Connexion(ChaineIntranetV3)
        End If
        '************ OLD ******************************************************************************************
        If SiV3_7 = False Then
            ChaineIntranetV3 = Request.Form("ChainePOST") 'Issu du POST - Intranet RH
            If ChaineIntranetV3 = "" Then
                ChaineIntranetV3 = Request.QueryString("ChainePOST") 'Issu du GET - VirtualiaRHV2.exe
            End If
            If ChaineIntranetV3 Is Nothing OrElse ChaineIntranetV3 = "" Then
                ChaineGrhV3 = Request.Form("CLEF")
                If ChaineGrhV3 = "" Then
                    ChaineGrhV3 = Request.QueryString("CLEF") 'Issu du GET - VirtualiaRHV2.exe
                End If
                If ChaineGrhV3 <> "" Then
                    SiTypeAccesGRH = True
                End If
            End If
            '** Uniquement pour test
            'ChaineIntranetV3 = "IntranetRhVTAPOSTSelfActivite_FPVTAPOSTVTAPOSTCEVTAPOST459VTAPOSTPERRODINVTAPOST502" 'ONISEP Test COBESSI = 459
            'ChaineIntranetV3 = "IntranetRhVTAPOSTManagerActivite_FPVTAPOSTVTAPOSTCEVTAPOST2969VTAPOSTPERRODINVTAPOST3427" 'ONISEP Test GROS Manager GIBRAT
            'ChaineIntranetV3 = "IntranetRhVTAPOSTSelfPaieVTAPOSTVTAPOSTCEVTAPOST2253VTAPOSTPERRODINVTAPOST2253" 'Bulletin de Paie
            'ChaineIntranetV3 = "IntranetRhVTAPOSTSelfDemandeFORVTAPOSTVTAPOSTCEVTAPOST18104VTAPOSTPERRODINVTAPOST17179" 'Demande de formation
            '**
            Select Case SiTypeAccesGRH
                Case True
                    OldClefV3 = New Virtualia.Net.Session.ClefV3OldCnx(ChaineGrhV3, True)
                Case False
                    If ChaineIntranetV3 Is Nothing OrElse ChaineIntranetV3 = "" Then
                        Msg &= " Demande non valide"
                        Response.Redirect("~/Fenetres/Commun/ErreurApplication.aspx?Msg=" & Msg)
                        Exit Sub
                    End If
                    OldClefV3 = New Virtualia.Net.Session.ClefV3OldCnx(ChaineIntranetV3, False)
            End Select
        End If

        Select Case SiV3_7
            Case True
                If ObjetClefV3.SiAccesOK(CType(WebFct.PointeurGlobal, Virtualia.Net.Session.ObjetGlobal)) = False Then
                    Msg &= " Accés invalide. " & ObjetClefV3.Detail_ErreurCnx
                    Response.Redirect("~/Fenetres/Commun/ErreurApplication.aspx?Msg=" & Msg)
                    Exit Sub
                End If
                NomConnexion = ObjetClefV3.Nom_CnxV3
                SiTypeAccesManager = ObjetClefV3.SiAcces_Manager
                SiTypeAccesGRH = ObjetClefV3.SiAcces_DRH
                WsAppliAccedee = ObjetClefV3.Appli_Accedee
                WsDossierPer = ObjetClefV3.ObjetDossier
            Case False
                Select Case SiTypeAccesGRH
                    Case True
                        CRetour = OldClefV3.SiAccesDRH_OK(CType(WebFct.PointeurGlobal, Virtualia.Net.Session.ObjetGlobal))
                        If CRetour = False Then
                            Msg &= " Clef invalide."
                            Response.Redirect("~/Fenetres/Commun/ErreurApplication.aspx?Msg=" & Msg)
                            Exit Sub
                        End If
                    Case False
                        CRetour = OldClefV3.SiAccesSelf_OK(CType(WebFct.PointeurGlobal, Virtualia.Net.Session.ObjetGlobal))
                        If CRetour = False Then
                            Msg &= " Accés invalide."
                            Response.Redirect("Fenetres/Commun/ErreurApplication.aspx?Msg=" & Msg)
                            Exit Sub
                        End If
                End Select
                NomConnexion = OldClefV3.Nom_CnxV3
                SiTypeAccesManager = OldClefV3.SiAcces_Manager
                WsAppliAccedee = OldClefV3.Appli_Accedee
                WsDossierPer = OldClefV3.ObjetDossier
        End Select

        '******************
        If Session.Item("IDVirtualia") Is Nothing Then
            Session.Add("IDVirtualia", Session.SessionID)
        End If
        If SessionUtlisateur Is Nothing Then
            Select Case SiTypeAccesGRH
                Case True
                    SessionUtlisateur = WebFct.PointeurGlobal.ItemSessionParLogin(NomConnexion)
                    If SessionUtlisateur Is Nothing Then
                        SessionUtlisateur = WebFct.PointeurGlobal.AjouterUneSessionModule(Session.SessionID, NomConnexion, "", "")
                    Else
                        SessionUtlisateur.V_IDSession = Session.SessionID
                    End If
                    SpecifiqueModule = New Virtualia.Net.Session.SessionModule(SessionUtlisateur)
                    SessionUtlisateur.V_PointeurSessionModule = SpecifiqueModule
                    Select Case SiV3_7
                        Case True
                            CType(SessionUtlisateur.V_PointeurSessionModule, Virtualia.Net.Session.SessionModule).UtilisateurV3 = ObjetClefV3.ObjetUti_CnxV3
                        Case False
                            CType(SessionUtlisateur.V_PointeurSessionModule, Virtualia.Net.Session.SessionModule).UtilisateurV3 = OldClefV3.ObjetUti_CnxV3
                    End Select
                Case False
                    SessionUtlisateur = WebFct.PointeurGlobal.ItemSessionModule(Session.SessionID)
                    If SessionUtlisateur Is Nothing AndAlso WsDossierPer IsNot Nothing Then
                        SessionUtlisateur = WebFct.PointeurGlobal.AjouterUneSessionModule(Session.SessionID, NomConnexion, "", "")
                    End If
                    If WsDossierPer IsNot Nothing Then
                        WsDossierPer.Initialiser()
                    End If
                    SpecifiqueModule = New Virtualia.Net.Session.SessionModule(SessionUtlisateur)
                    SessionUtlisateur.V_PointeurSessionModule = SpecifiqueModule
                    CType(SessionUtlisateur.V_PointeurSessionModule, Virtualia.Net.Session.SessionModule).DossierPER = WsDossierPer
            End Select
        End If

        Session.Clear()
        Session.Add("IDVirtualia", Session.SessionID)
        SpecifiqueModule.SiAccesManager = SiTypeAccesManager
        SpecifiqueModule.SiAccesGRH = SiTypeAccesGRH

        If SiTypeAccesManager = True Then
            Select Case WsAppliAccedee
                Case "ManagerPFR"
                    If SpecifiqueModule.SiAccesManagerVerifie(WsDossierPer.Ide_Dossier, False) = False Then
                        Msg &= "Accés invalide."
                        Response.Redirect("Fenetres/Commun/ErreurApplication.aspx?Msg=" & Msg)
                        Exit Sub
                    End If
                Case Else
                    If SpecifiqueModule.InfosAccesManager = False Then
                        Msg &= "Accés manager invalide."
                        Response.Redirect("Fenetres/Commun/ErreurApplication.aspx?Msg=" & Msg)
                        Exit Sub
                    End If
            End Select
            Session.Add("Manager", NomConnexion)
        End If

        Dim CacheCookie As Virtualia.Net.VCaches.CacheIdentification = Nothing
        If CacheCookie Is Nothing Then
            CacheCookie = New Virtualia.Net.VCaches.CacheIdentification
        End If
        CacheCookie.Nom_Identification = SessionUtlisateur.V_NomdeConnexion
        CacheCookie.NumeroSession = Session.SessionID
        WebFct.EcrireCookie(Me, CacheCookie)

        Call AfficherFenetre(SpecifiqueModule)

    End Sub

    Private Sub AfficherFenetre(ByVal ObjetSession As Virtualia.Net.Session.SessionModule)
        If ObjetSession Is Nothing Then
            Exit Sub
        End If
        Dim ChaineCnx As String = "Connexion de "
        If SiTypeAccesManager = True Then
            ChaineCnx &= ObjetSession.DossierPER.Nom_Prenom_Manager
        ElseIf SiTypeAccesGRH = True Then
            ChaineCnx &= ObjetSession.VParent.V_NomdeConnexion
        Else
            ChaineCnx = ""
        End If
        If ObjetSession.DossierPER IsNot Nothing Then
            ChaineCnx &= " --> au dossier de "
            ChaineCnx &= ObjetSession.DossierPER.Nom & Strings.Space(1) & ObjetSession.DossierPER.Prenom
            ObjetSession.DossierPER.Initialiser()
        End If
        Call ObjetSession.VPointeurGlobal.EcrireLogTraitement(WsAppliAccedee, True, ChaineCnx)

        Select Case WsAppliAccedee
            Case "ManagerPFR", "PFR"
                ObjetSession.VueActiveAlPha = "Base100"
                Response.Redirect("~/Fenetres/FPEtat/FrmPFR.aspx?IDVirtualia=" & ObjetSession.VParent.V_IDSession)
            Case "Activite_FP", "ManagerActivite_FP", "SelfActivite_FP"
                Response.Redirect("~/Fenetres/FPEtat/FrmSemaineActivite.aspx?IDVirtualia=" & ObjetSession.VParent.V_IDSession)
            Case "ManagerActivite_Honda", "SelfActivite_Honda"
                ObjetSession.VueActiveAlPha = "Activite"
                Response.Redirect("~/Fenetres/Honda/FrmActiviteH.aspx?IDVirtualia=" & ObjetSession.VParent.V_IDSession)
            Case "SelfAcompte_Honda"
                ObjetSession.VueActiveAlPha = "Acompte"
                Response.Redirect("~/Fenetres/Honda/FrmActiviteH.aspx?IDVirtualia=" & ObjetSession.VParent.V_IDSession)
            Case "SelfDemandeFOR"
                Response.Redirect("~/Fenetres/Commun/FrmDemandeFormation.aspx?IDVirtualia=" & ObjetSession.VParent.V_IDSession)
            Case "SelfPaie"
                Response.Redirect("~/Fenetres/FPEtat/FrmDuplicataBulletin.aspx?IDVirtualia=" & ObjetSession.VParent.V_IDSession)
            Case "SelfHSupp"
                Response.Redirect("~/Fenetres/FPEtat/FrmHeuresSup.aspx?IDVirtualia=" & ObjetSession.VParent.V_IDSession)
            Case "HeureSupp", "ManagerHeureSupp", "SelfHeureSupp"
                Response.Redirect("~/Fenetres/Commun/FrmHeuresSupplementaires.aspx?IDVirtualia=" & ObjetSession.VParent.V_IDSession)
            Case Else
                Response.Redirect("Fenetres/Commun/ErreurApplication.aspx?Msg=" & "Accés invalide, application inconnue - " & WsAppliAccedee)
        End Select
    End Sub

End Class