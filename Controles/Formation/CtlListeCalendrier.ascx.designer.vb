﻿'------------------------------------------------------------------------------
' <généré automatiquement>
'     Ce code a été généré par un outil.
'
'     Les modifications apportées à ce fichier peuvent provoquer un comportement incorrect et seront perdues si
'     le code est régénéré.
' </généré automatiquement>
'------------------------------------------------------------------------------

Option Strict On
Option Explicit On


Partial Public Class CtlListeCalendrier
    
    '''<summary>
    '''Contrôle CadreCalendrier.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CadreCalendrier As Global.System.Web.UI.WebControls.Table
    
    '''<summary>
    '''Contrôle CadreChoix.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CadreChoix As Global.System.Web.UI.WebControls.Table
    
    '''<summary>
    '''Contrôle EtiAnnee.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents EtiAnnee As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle ListeChoixAnnee.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents ListeChoixAnnee As Global.System.Web.UI.WebControls.DropDownList
    
    '''<summary>
    '''Contrôle EtiMois.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents EtiMois As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle ListeChoixMois.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents ListeChoixMois As Global.System.Web.UI.WebControls.DropDownList
    
    '''<summary>
    '''Contrôle EtiFiltreN1.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents EtiFiltreN1 As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle ListeChoixFiltreN1.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents ListeChoixFiltreN1 As Global.System.Web.UI.WebControls.DropDownList
    
    '''<summary>
    '''Contrôle PanelEnteteJour.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents PanelEnteteJour As Global.System.Web.UI.WebControls.Panel
    
    '''<summary>
    '''Contrôle CadreEnteteJour.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CadreEnteteJour As Global.System.Web.UI.WebControls.Table
    
    '''<summary>
    '''Contrôle EtiNeutreGauche.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents EtiNeutreGauche As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle CellJour01.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CellJour01 As Global.System.Web.UI.WebControls.TableCell
    
    '''<summary>
    '''Contrôle EtiJour01.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents EtiJour01 As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle CellJour02.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CellJour02 As Global.System.Web.UI.WebControls.TableCell
    
    '''<summary>
    '''Contrôle EtiJour02.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents EtiJour02 As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle CellJour03.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CellJour03 As Global.System.Web.UI.WebControls.TableCell
    
    '''<summary>
    '''Contrôle EtiJour03.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents EtiJour03 As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle CellJour04.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CellJour04 As Global.System.Web.UI.WebControls.TableCell
    
    '''<summary>
    '''Contrôle EtiJour04.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents EtiJour04 As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle CellJour05.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CellJour05 As Global.System.Web.UI.WebControls.TableCell
    
    '''<summary>
    '''Contrôle EtiJour05.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents EtiJour05 As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle CellJour06.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CellJour06 As Global.System.Web.UI.WebControls.TableCell
    
    '''<summary>
    '''Contrôle EtiJour06.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents EtiJour06 As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle CellJour07.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CellJour07 As Global.System.Web.UI.WebControls.TableCell
    
    '''<summary>
    '''Contrôle EtiJour07.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents EtiJour07 As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle CellJour08.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CellJour08 As Global.System.Web.UI.WebControls.TableCell
    
    '''<summary>
    '''Contrôle EtiJour08.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents EtiJour08 As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle CellJour09.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CellJour09 As Global.System.Web.UI.WebControls.TableCell
    
    '''<summary>
    '''Contrôle EtiJour09.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents EtiJour09 As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle CellJour10.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CellJour10 As Global.System.Web.UI.WebControls.TableCell
    
    '''<summary>
    '''Contrôle EtiJour10.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents EtiJour10 As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle CellJour11.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CellJour11 As Global.System.Web.UI.WebControls.TableCell
    
    '''<summary>
    '''Contrôle EtiJour11.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents EtiJour11 As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle CellJour12.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CellJour12 As Global.System.Web.UI.WebControls.TableCell
    
    '''<summary>
    '''Contrôle EtiJour12.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents EtiJour12 As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle CellJour13.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CellJour13 As Global.System.Web.UI.WebControls.TableCell
    
    '''<summary>
    '''Contrôle EtiJour13.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents EtiJour13 As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle CellJour14.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CellJour14 As Global.System.Web.UI.WebControls.TableCell
    
    '''<summary>
    '''Contrôle EtiJour14.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents EtiJour14 As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle CellJour15.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CellJour15 As Global.System.Web.UI.WebControls.TableCell
    
    '''<summary>
    '''Contrôle EtiJour15.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents EtiJour15 As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle CellJour16.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CellJour16 As Global.System.Web.UI.WebControls.TableCell
    
    '''<summary>
    '''Contrôle EtiJour16.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents EtiJour16 As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle CellJour17.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CellJour17 As Global.System.Web.UI.WebControls.TableCell
    
    '''<summary>
    '''Contrôle EtiJour17.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents EtiJour17 As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle CellJour18.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CellJour18 As Global.System.Web.UI.WebControls.TableCell
    
    '''<summary>
    '''Contrôle EtiJour18.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents EtiJour18 As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle CellJour19.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CellJour19 As Global.System.Web.UI.WebControls.TableCell
    
    '''<summary>
    '''Contrôle EtiJour19.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents EtiJour19 As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle CellJour20.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CellJour20 As Global.System.Web.UI.WebControls.TableCell
    
    '''<summary>
    '''Contrôle EtiJour20.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents EtiJour20 As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle CellJour21.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CellJour21 As Global.System.Web.UI.WebControls.TableCell
    
    '''<summary>
    '''Contrôle EtiJour21.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents EtiJour21 As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle CellJour22.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CellJour22 As Global.System.Web.UI.WebControls.TableCell
    
    '''<summary>
    '''Contrôle EtiJour22.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents EtiJour22 As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle CellJour23.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CellJour23 As Global.System.Web.UI.WebControls.TableCell
    
    '''<summary>
    '''Contrôle EtiJour23.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents EtiJour23 As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle CellJour24.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CellJour24 As Global.System.Web.UI.WebControls.TableCell
    
    '''<summary>
    '''Contrôle EtiJour24.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents EtiJour24 As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle CellJour25.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CellJour25 As Global.System.Web.UI.WebControls.TableCell
    
    '''<summary>
    '''Contrôle EtiJour25.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents EtiJour25 As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle CellJour26.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CellJour26 As Global.System.Web.UI.WebControls.TableCell
    
    '''<summary>
    '''Contrôle EtiJour26.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents EtiJour26 As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle CellJour27.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CellJour27 As Global.System.Web.UI.WebControls.TableCell
    
    '''<summary>
    '''Contrôle EtiJour27.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents EtiJour27 As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle CellJour28.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CellJour28 As Global.System.Web.UI.WebControls.TableCell
    
    '''<summary>
    '''Contrôle EtiJour28.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents EtiJour28 As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle CellJour29.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CellJour29 As Global.System.Web.UI.WebControls.TableCell
    
    '''<summary>
    '''Contrôle EtiJour29.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents EtiJour29 As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle CellJour30.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CellJour30 As Global.System.Web.UI.WebControls.TableCell
    
    '''<summary>
    '''Contrôle EtiJour30.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents EtiJour30 As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle CellJour31.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CellJour31 As Global.System.Web.UI.WebControls.TableCell
    
    '''<summary>
    '''Contrôle EtiJour31.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents EtiJour31 As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle EtiNeutreDroite.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents EtiNeutreDroite As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle PanelLignes.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents PanelLignes As Global.System.Web.UI.WebControls.Panel
    
    '''<summary>
    '''Contrôle CadreLignes.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CadreLignes As Global.System.Web.UI.WebControls.Table
    
    '''<summary>
    '''Contrôle RowGabarit.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents RowGabarit As Global.System.Web.UI.WebControls.TableRow
    
    '''<summary>
    '''Contrôle EtiSep01.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents EtiSep01 As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle EtiSep02.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents EtiSep02 As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle EtiSep03.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents EtiSep03 As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle EtiSep04.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents EtiSep04 As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle EtiSep05.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents EtiSep05 As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle EtiSep06.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents EtiSep06 As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle EtiSep07.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents EtiSep07 As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle EtiSep08.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents EtiSep08 As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle EtiSep09.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents EtiSep09 As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle EtiSep10.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents EtiSep10 As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle EtiSep11.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents EtiSep11 As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle EtiSep12.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents EtiSep12 As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle EtiSep13.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents EtiSep13 As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle EtiSep14.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents EtiSep14 As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle EtiSep15.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents EtiSep15 As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle EtiSep16.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents EtiSep16 As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle EtiSep17.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents EtiSep17 As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle EtiSep18.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents EtiSep18 As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle EtiSep19.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents EtiSep19 As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle EtiSep20.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents EtiSep20 As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle EtiSep21.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents EtiSep21 As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle EtiSep22.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents EtiSep22 As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle EtiSep23.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents EtiSep23 As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle EtiSep24.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents EtiSep24 As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle EtiSep25.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents EtiSep25 As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle EtiSep26.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents EtiSep26 As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle EtiSep27.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents EtiSep27 As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle EtiSep28.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents EtiSep28 As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle EtiSep29.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents EtiSep29 As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle EtiSep30.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents EtiSep30 As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle EtiSep31.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents EtiSep31 As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle EtiSep32.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents EtiSep32 As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle EtiSep33.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents EtiSep33 As Global.System.Web.UI.WebControls.Label
End Class
