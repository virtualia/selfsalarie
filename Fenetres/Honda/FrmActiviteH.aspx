﻿<%@ Page Language="vb" AutoEventWireup="false" MasterPageFile="~/PagesMaitres/VirtualiaMain.Master" CodeBehind="FrmActiviteH.aspx.vb" 
    Inherits="Virtualia.Net.FrmActiviteH" UICulture="fr" MaintainScrollPositionOnPostback="True" %>

<%@ MasterType VirtualPath="~/PagesMaitres/VirtualiaMain.master" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<%@ Register src="~/Controles/Commun/VMessage.ascx" tagname="VMessage" tagprefix="Virtualia" %>
<%@ Register src="~/Controles/Honda/PER_ACTIVITE_HONDA.ascx" tagname="VActivite" tagprefix="Virtualia" %>
<%@ Register src="~/Controles/Honda/PER_ACOMPTE_HONDA.ascx" tagname="VAcompte" tagprefix="Virtualia" %>

<asp:Content ID="Corps" ContentPlaceHolderID="Principal" runat="server">
    <asp:Table ID="CadreGeneral" runat="server" HorizontalAlign="Center" Width="1050px">
         <asp:TableRow>
            <asp:TableCell>
                <asp:Panel ID="CadreAffichage" runat="server" BackColor="Transparent" HorizontalAlign="Center" Width="1050px"
                     BackImageUrl="~/Images/Fonds/Fond_Saisie.jpg" BorderStyle="None">
                    <asp:Table ID="CadreActivite" runat="server" Width="1040px" HorizontalAlign="Center" borderStyle="None">
                        <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Center" VerticalAlign="Top">
                                <asp:MultiView ID="MultiHonda" runat="server" ActiveViewIndex="0">
                                    <asp:View ID="VueActivite" runat="server">
                                        <Virtualia:VActivite ID="ActiviteHonda" runat="server" />
                                    </asp:View>
                                    <asp:View ID="VueAcompte" runat="server">
                                        <Virtualia:VAcompte ID="AcompteHonda" runat="server" />
                                    </asp:View>
                                </asp:MultiView>
                            </asp:TableCell>
                        </asp:TableRow>
                    </asp:Table>
                </asp:Panel>
            </asp:TableCell>
        </asp:TableRow>
        <asp:TableRow>
             <asp:TableCell ID="CellMessage" Visible="false">
                <ajaxToolkit:ModalPopupExtender ID="PopupMsg" runat="server" TargetControlID="HPopupMsg" PopupControlID="PanelMsgPopup" BackgroundCssClass="modalBackground" />
                <asp:Panel ID="PanelMsgPopup" runat="server">
                    <Virtualia:VMessage id="MsgVirtualia" runat="server" />
                </asp:Panel>
                <asp:HiddenField ID="HPopupMsg" runat="server" Value="0" />
            </asp:TableCell>
        </asp:TableRow>
    </asp:Table>
</asp:Content>
