﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="CtlLectureActivite.ascx.vb" Inherits="Virtualia.Net.CtlLectureActivite" %>

<style type="text/css">
    .LigneTable
    {
        border-style:none;
        margin-top:0px;
        position:static;
        z-index:99;
    }
.CellEtiIntitule
    {
        background-color:#7AA09A;
        border-color:LightGray;
        border-style:none;
        text-align:center;
        vertical-align:middle;
    }
.EtiIntitule
    {
        background-color:#7AA09A;
        border-style:none;
        color:#E2F5F1;
        height:23px;
        font-family:'Trebuchet MS';
        font-size:smaller;
        font-style:normal;
        font-weight:normal;
        margin-top:1px;
        padding-top: 4px;
        text-indent:3px;
        text-align:left;
        width:95px;
        word-wrap:normal;
    }
.DonIntitule
    {
        background-color:#E2F5F1;
        border-color:#B0E0D7;
        border-style:inset;
        border-width:2px;
        color:Black;
        height:20px;
        font-family:'Trebuchet MS';
        font-size:10px;
        font-style:normal;
        font-weight:normal;
        margin-top:1px;
        text-indent:1px;
        text-align:left;
        width:560px;
        word-wrap:normal;
    }
.DateDonnee
    {
        background-color:#7AA09A;
        border-color:LightGray;
        border-style:none;
        color:#E2F5F1;
        font-family:'Trebuchet MS';
        font-size:11px;
        font-style:normal;
        font-weight:bold;
        height:15px;
        margin-bottom:0px;
        margin-left:4px;
        margin-top:0px;
        padding-top: 2px;
        text-align:center;
        text-indent:4px;
        width:29px;
    }
.JourDonnee
    {
        background-color:#7AA09A;
        border-color:LightGray;
        border-style:none;
        color:#E2F5F1;
        font-family:'Trebuchet MS';
        font-size:11px;
        font-style:normal;
        font-weight:bold;
        height:15px;
        margin-bottom:0px;
        margin-left:4px;
        margin-top:0px;
        padding-top: 2px;
        text-align:center;
        text-indent:4px;
        width:29px;
    }
.Donnee
    {
        background-color:#E2F5F1;
        border-color:#B0E0D7;
        border-style:inset;
        border-width:2px;
        color:black;
        height:23px;
        font-family:'Trebuchet MS';
        font-size:small;
        font-style:normal;
        margin-left:1px;
        margin-top:1px;
        text-indent:1px;
        text-align:center;
        width:29px;
    }
.CellCmd
    {
        border-color:#B0E0D7;
        border-style:none;
        text-align:center;
        vertical-align:middle;
    }
.ImageCmd
    {
        background-color:transparent;
        border-color:#B0E0D7;
        border-style:outset;
        border-width:2px;
        height:14px;
        width:14px;
    }
</style>

<asp:Table ID="CadreGlobal" runat="server" CellPadding="0" CellSpacing="0">
    <asp:TableRow>
        <asp:TableCell>
            <asp:Table ID="CadreElement_01" runat="server" CellPadding="0" CellSpacing="0" Width="950px" backcolor="#7AA09A"
                  BorderStyle="Notset" BorderWidth="1px" BorderColor="LightGray" HorizontalAlign="Left" Visible="false">
                <asp:TableRow>
                    <asp:TableCell>
                        <asp:Table ID="CadreIntitule_01" runat="server" CssClass="LigneTable" Height="30px" CellPadding="0" Width="665px"  
                            CellSpacing="0" HorizontalAlign="Center" BorderStyle="None">
                            <asp:TableRow>
                                <asp:TableCell ID="CellEtiActivite_01" CssClass="CellEtiIntitule"> 
                                    <asp:Label ID="EtiActivite_01" runat="server" CssClass="EtiIntitule" Text="Projet" />      
                                </asp:TableCell> 
                                <asp:TableCell ID="CellDonActivite_01" CssClass="CellEtiIntitule" > 
                                    <asp:TextBox ID="DonActivite_01" runat="server" CssClass="DonIntitule" ReadOnly="true"
                                        Text="" Visible="true" AutoPostBack="false" TextMode="SingleLine"> 
                                    </asp:TextBox> 
                                </asp:TableCell>     
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell ID="CellEtiProduit_01" CssClass="CellEtiIntitule"> 
                                    <asp:Label ID="EtiProduit_01" runat="server" CssClass="EtiIntitule" Text="Etape" />   
                                </asp:TableCell> 
                                <asp:TableCell ID="CellDonProduit_01" CssClass="CellEtiIntitule" > 
                                    <asp:TextBox ID="DonProduit_01" runat="server" CssClass="DonIntitule" ReadOnly="true"
                                        Text="" Visible="true" AutoPostBack="false" TextMode="SingleLine"> 
                                    </asp:TextBox>
                                </asp:TableCell>     
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell ID="CellEtiPrecision_01" CssClass="CellEtiIntitule"> 
                                    <asp:Label ID="EtiPrecision_01" runat="server" CssClass="EtiIntitule" Text="Précision" />     
                                </asp:TableCell> 
                                <asp:TableCell ID="CellDonPrecision_01" CssClass="CellEtiIntitule"> 
                                    <asp:TextBox ID="DonPrecision_01" runat="server" CssClass="DonIntitule" ReadOnly="true"
                                        Text="" Visible="true" AutoPostBack="false" TextMode="SingleLine"> 
                                    </asp:TextBox>
                                </asp:TableCell>
                            </asp:TableRow>
                        </asp:Table>
                    </asp:TableCell>
                    <asp:TableCell>
                        <asp:Table ID="CadreRepartition_01" runat="server" Height="30px" CellPadding="0" Width="261px"  
                            CellSpacing="0" HorizontalAlign="Center" Visible="true" BorderStyle="None">
                            <asp:TableRow>
                                <asp:TableCell ID="CellDateLundi_01" CssClass="CellEtiIntitule"> 
                                    <asp:Label ID="EtiDateLundi_01" runat="server" CssClass="DateDonnee" Text="" ToolTip="Lundi" />        
                                </asp:TableCell>
                                <asp:TableCell ID="CellDateMardi_01" CssClass="CellEtiIntitule"> 
                                    <asp:Label ID="EtiDateMardi_01" runat="server" CssClass="DateDonnee" Text="" ToolTip="Mardi" />   
                                </asp:TableCell>
                                <asp:TableCell ID="CellDateMercredi_01" CssClass="CellEtiIntitule"> 
                                    <asp:Label ID="EtiDateMercredi_01" runat="server" CssClass="DateDonnee" Text="" ToolTip="Mercredi" />
                                </asp:TableCell>
                                <asp:TableCell ID="CellDateJeudi_01" CssClass="CellEtiIntitule"> 
                                    <asp:Label ID="EtiDateJeudi_01" runat="server" CssClass="DateDonnee" Text="" ToolTip="Jeudi" />
                                </asp:TableCell>
                                <asp:TableCell ID="CellDateVendredi_01" CssClass="CellEtiIntitule"> 
                                    <asp:Label ID="EtiDateVendredi_01" runat="server" CssClass="DateDonnee" Text="" ToolTip="Vendredi" />
                                </asp:TableCell>
                                <asp:TableCell ID="CellDateSamedi_01" CssClass="CellEtiIntitule"> 
                                    <asp:Label ID="EtiDateSamedi_01" runat="server" CssClass="DateDonnee" Text="" ToolTip="Samedi"/>
                                </asp:TableCell>
                                <asp:TableCell ID="CellDateDimanche_01" CssClass="CellEtiIntitule"> 
                                    <asp:Label ID="EtiDateDimanche_01" runat="server" CssClass="DateDonnee" Text="" ToolTip="Dimanche"/>
                                </asp:TableCell>
                                <asp:TableCell ID="CellDateSemaine_01" CssClass="CellEtiIntitule"> 
                                    <asp:Label ID="EtiDateSemaine_01" runat="server" CssClass="DateDonnee" Text="" Width="20px" />
                                </asp:TableCell>     
                             </asp:TableRow>
                             <asp:TableRow>
                                <asp:TableCell ID="CellLundi_01" CssClass="CellEtiIntitule"> 
                                    <asp:Label ID="EtiLundi_01" runat="server" CssClass="JourDonnee" Text="L" />       
                                </asp:TableCell>
                                <asp:TableCell ID="CellMardi_01" CssClass="CellEtiIntitule"> 
                                    <asp:Label ID="EtiMardi_01" runat="server" CssClass="JourDonnee" Text="M" />
                                </asp:TableCell>
                                <asp:TableCell ID="CellMercredi_01" CssClass="CellEtiIntitule"> 
                                    <asp:Label ID="EtiMercredi_01" runat="server" CssClass="JourDonnee" Text="M" />
                                </asp:TableCell>
                                <asp:TableCell ID="CellJeudi_01" CssClass="CellEtiIntitule"> 
                                    <asp:Label ID="EtiJeudi_01" runat="server" CssClass="JourDonnee" Text="J" />
                                </asp:TableCell>
                                <asp:TableCell ID="Cell01_Vendredi" CssClass="CellEtiIntitule"> 
                                    <asp:Label ID="Eti01_Vendredi" runat="server" CssClass="JourDonnee" Text="V" />
                                </asp:TableCell>
                                <asp:TableCell ID="CellSamedi_01" CssClass="CellEtiIntitule"> 
                                    <asp:Label ID="EtiSamedi_01" runat="server" CssClass="JourDonnee" Text="S" />
                                </asp:TableCell>
                                <asp:TableCell ID="CellDimanche_01" CssClass="CellEtiIntitule"> 
                                    <asp:Label ID="EtiDimanche_01" runat="server" CssClass="JourDonnee" Text="D" />
                                </asp:TableCell>
                                <asp:TableCell ID="CellSemaine_01" CssClass="CellEtiIntitule"> 
                                    <asp:Label ID="EtiSemaine_01" runat="server" CssClass="JourDonnee" Text="" Width="20px" />
                                </asp:TableCell>     
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell>
                                   <asp:TextBox ID="Donnee_01_00" runat="server" CssClass="Donnee" Text="100" ToolTip="lundi"
                                        Visible="true" ReadOnly="true" AutoPostBack="false" TextMode="SingleLine"> 
                                    </asp:TextBox>
                                </asp:TableCell>
                                <asp:TableCell>
                                   <asp:TextBox ID="Donnee_01_01" runat="server" CssClass="Donnee" Text="" ToolTip="mardi"
                                        Visible="true" ReadOnly="true" AutoPostBack="false" TextMode="SingleLine">
                                    </asp:TextBox>
                                </asp:TableCell>
                                <asp:TableCell>
                                   <asp:TextBox ID="Donnee_01_02" runat="server" CssClass="Donnee" Text="" ToolTip="mercredi"
                                        Visible="true" ReadOnly="true" AutoPostBack="false" TextMode="SingleLine"> 
                                    </asp:TextBox>
                                </asp:TableCell>
                                <asp:TableCell>
                                <asp:TextBox ID="Donnee_01_03" runat="server" CssClass="Donnee" Text="" ToolTip="jeudi"
                                        Visible="true" ReadOnly="true" AutoPostBack="false" TextMode="SingleLine">
                                    </asp:TextBox>
                                </asp:TableCell>
                                <asp:TableCell>
                                   <asp:TextBox ID="Donnee_01_04" runat="server" CssClass="Donnee" Text="" ToolTip="vendredi"
                                        Visible="true" ReadOnly="true" AutoPostBack="false" TextMode="SingleLine"> 
                                    </asp:TextBox>
                                </asp:TableCell>
                                <asp:TableCell>
                                   <asp:TextBox ID="Donnee_01_05" runat="server" CssClass="Donnee" Text="" ToolTip="samedi"
                                        Visible="true" ReadOnly="true" AutoPostBack="false" TextMode="SingleLine">
                                    </asp:TextBox>
                                </asp:TableCell>
                                <asp:TableCell>
                                   <asp:TextBox ID="Donnee_01_06" runat="server" CssClass="Donnee" Text="" ToolTip="dimanche"
                                        Visible="true" ReadOnly="true" AutoPostBack="false" TextMode="SingleLine">
                                    </asp:TextBox>
                                </asp:TableCell>
                                <asp:TableCell ID="CellModifier_01" CssClass="CellCmd">  
                                   <asp:ImageButton ID="CmdModifier_01" runat="server" CssClass="ImageCmd" ImageAlign="Middle" ImageUrl="~/Images/Boutons/Modifier.bmp" ToolTip="Modifier" />
                                    <asp:ImageButton ID="CmdSupprimer_01" runat="server" CssClass="ImageCmd" ImageAlign="Middle" ImageUrl="~/Images/Boutons/Supprimer.bmp" ToolTip="Supprimer" />
                                </asp:TableCell>
                            </asp:TableRow>
                        </asp:Table>
                     </asp:TableCell>
                </asp:TableRow>
            </asp:Table>
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell Height="5px"></asp:TableCell>
    </asp:TableRow>
</asp:Table>