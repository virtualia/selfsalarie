﻿'------------------------------------------------------------------------------
' <généré automatiquement>
'     Ce code a été généré par un outil.
'
'     Les modifications apportées à ce fichier peuvent provoquer un comportement incorrect et seront perdues si
'     le code est régénéré.
' </généré automatiquement>
'------------------------------------------------------------------------------

Option Strict On
Option Explicit On


Partial Public Class CtlEditionActivite
    
    '''<summary>
    '''Contrôle PanelGlobal.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents PanelGlobal As Global.System.Web.UI.WebControls.Panel
    
    '''<summary>
    '''Contrôle CadreHaut.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CadreHaut As Global.System.Web.UI.WebControls.Table
    
    '''<summary>
    '''Contrôle CellRetour.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CellRetour As Global.System.Web.UI.WebControls.TableCell
    
    '''<summary>
    '''Contrôle CommandeRetour.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CommandeRetour As Global.System.Web.UI.WebControls.ImageButton
    
    '''<summary>
    '''Contrôle Etiquette.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Etiquette As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle CadreCmdStd.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CadreCmdStd As Global.System.Web.UI.WebControls.Table
    
    '''<summary>
    '''Contrôle CommandeCsv.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CommandeCsv As Global.System.Web.UI.WebControls.Button
    
    '''<summary>
    '''Contrôle CommandeTxt.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CommandeTxt As Global.System.Web.UI.WebControls.Button
    
    '''<summary>
    '''Contrôle CommandeExec.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CommandeExec As Global.System.Web.UI.WebControls.Button
    
    '''<summary>
    '''Contrôle UpdatePanelBas.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents UpdatePanelBas As Global.System.Web.UI.UpdatePanel
    
    '''<summary>
    '''Contrôle HorlogeExec.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents HorlogeExec As Global.System.Web.UI.Timer
    
    '''<summary>
    '''Contrôle CadreBas.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CadreBas As Global.System.Web.UI.WebControls.Table

    '''<summary>
    '''Contrôle OptionVisa.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents OptionVisa As Global.Virtualia.Net.Controles_VTrioHorizontalRadio

    '''<summary>
    '''Contrôle VComboMois.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents VComboMois As Global.Virtualia.Net.Controles_VListeCombo
    
    '''<summary>
    '''Contrôle VComboStructureN1.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents VComboStructureN1 As Global.Virtualia.Net.Controles_VListeCombo
    
    '''<summary>
    '''Contrôle EtiHDebut.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents EtiHDebut As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle EtiHFin.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents EtiHFin As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle EtiTraitement.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents EtiTraitement As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle UpdateAttente.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents UpdateAttente As Global.System.Web.UI.UpdateProgress
    
    '''<summary>
    '''Contrôle HNumPage.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents HNumPage As Global.System.Web.UI.WebControls.HiddenField
End Class
