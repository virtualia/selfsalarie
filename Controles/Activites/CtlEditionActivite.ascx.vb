﻿Option Strict On
Option Explicit On
Option Compare Text
Imports Virtualia.Systeme.Evenements
Imports VI = Virtualia.Systeme.Constantes
Public Class CtlEditionActivite
    Inherits System.Web.UI.UserControl
    Public Delegate Sub Retour_MsgEventHandler(ByVal sender As Object, ByVal e As Virtualia.Systeme.Evenements.MessageRetourEventArgs)
    Public Event ValeurRetour As Retour_MsgEventHandler
    Private WebFct As Virtualia.Net.Controles.WebFonctions
    Private WsNomStateCache As String = "VEtatActivite"
    Private WsCtl_Cache As Virtualia.Net.Controles.CacheControleActivite
    Private WsNumObjet As Integer = 112

    Protected Overridable Sub V_Retour(ByVal e As Virtualia.Systeme.Evenements.MessageRetourEventArgs)
        RaiseEvent ValeurRetour(Me, e)
    End Sub

    Private Property CacheVirControle As Virtualia.Net.Controles.CacheControleActivite
        Get
            If Me.ViewState(WsNomStateCache) IsNot Nothing Then
                Return CType(Me.ViewState(WsNomStateCache), Virtualia.Net.Controles.CacheControleActivite)
            End If
            Dim NewCache As Virtualia.Net.Controles.CacheControleActivite
            NewCache = New Virtualia.Net.Controles.CacheControleActivite
            Return NewCache
        End Get
        Set(value As Virtualia.Net.Controles.CacheControleActivite)
            If value Is Nothing Then
                Exit Property
            End If
            If Me.ViewState(WsNomStateCache) IsNot Nothing Then
                Me.ViewState.Remove(WsNomStateCache)
            End If
            Me.ViewState.Add(WsNomStateCache, value)
        End Set
    End Property

    Private Sub Page_Init(sender As Object, e As EventArgs) Handles Me.Init
        WebFct = New Virtualia.Net.Controles.WebFonctions(Me, 0)
    End Sub

    Private Sub Page_PreRender(sender As Object, e As EventArgs) Handles Me.PreRender
        WsCtl_Cache = CacheVirControle
        If WsCtl_Cache.DateValeur = "" And CInt(HNumPage.Value) = 0 Then
            Call FaireListeMois()
            Call FaireListNiveau1()
            Exit Sub
        End If
        If CInt(HNumPage.Value) = 0 Then
            EtiHDebut.Text = ""
            EtiHFin.Text = ""
            Exit Sub
        End If
        If WebFct.PointeurUtilisateur Is Nothing Then
            Exit Sub
        End If
        Dim NoObjet As Integer = 1
        Dim LstFiches As List(Of Virtualia.Systeme.MetaModele.VIR_FICHE)
        Dim LstRes As List(Of Virtualia.Systeme.MetaModele.VIR_FICHE) = Nothing
        Dim RequeteFiltre As String = ""
        Dim Libel As String = "Préparation pour  la période sélectionnée " & WsCtl_Cache.ValeurSelection & " Etape N° "

        EtiTraitement.Text = Libel & HNumPage.Value
        Select Case CInt(HNumPage.Value)
            Case 1
                HNumPage.Value = "2"
                Exit Sub
            Case 2
                LstFiches = WebFct.PointeurGlobal.SelectionObjetDate(Nothing, WsCtl_Cache.DateValeur,
                                                                     WebFct.ViRhDates.DateSaisieVerifiee("31" & Strings.Right(WsCtl_Cache.DateValeur, 8)), WsNumObjet)
                If LstFiches Is Nothing Then
                    HNumPage.Value = "0"
                    HorlogeExec.Enabled = False
                    EtiTraitement.Text = "Préparation pour  la période sélectionnée " & WsCtl_Cache.ValeurSelection & " terminée"
                    Exit Sub
                End If
                CType(WebFct.PointeurUtilisateur.V_PointeurSessionModule, Virtualia.Net.Session.SessionModule).ExeListeFiches = LstFiches
                HNumPage.Value = "3"
                Exit Sub
            Case 3
                LstFiches = CType(WebFct.PointeurUtilisateur.V_PointeurSessionModule, Virtualia.Net.Session.SessionModule).ExeListeFiches
                RequeteFiltre = CType(WebFct.PointeurGlobal, Virtualia.Net.Session.ObjetGlobal).VirClauseFiltre(0, WsCtl_Cache.DateValeur,
                                                             WebFct.ViRhDates.DateSaisieVerifiee("31" & Strings.Right(WsCtl_Cache.DateValeur, 8)), WsNumObjet)

                LstRes = WebFct.PointeurGlobal.SelectionObjetDate(Nothing, WsCtl_Cache.DateValeur, WebFct.ViRhDates.DateSaisieVerifiee("31" & Strings.Right(WsCtl_Cache.DateValeur, 8)),
                                                                     VI.ObjetPer.ObaBulletin, RequeteFiltre)
                If LstRes IsNot Nothing Then
                    LstFiches.AddRange(LstRes)
                    CType(WebFct.PointeurUtilisateur.V_PointeurSessionModule, Virtualia.Net.Session.SessionModule).ExeListeFiches = LstFiches
                End If

            Case 4 To 19
                LstFiches = CType(WebFct.PointeurUtilisateur.V_PointeurSessionModule, Virtualia.Net.Session.SessionModule).ExeListeFiches
                RequeteFiltre = CType(WebFct.PointeurGlobal, Virtualia.Net.Session.ObjetGlobal).VirClauseFiltre(0, WsCtl_Cache.DateValeur,
                                                             WebFct.ViRhDates.DateSaisieVerifiee("31" & Strings.Right(WsCtl_Cache.DateValeur, 8)), WsNumObjet)
                Select Case CInt(HNumPage.Value)
                    Case 4
                        NoObjet = VI.ObjetPer.ObaCivil
                    Case 5
                        NoObjet = VI.ObjetPer.ObaStatut
                    Case 6
                        NoObjet = VI.ObjetPer.ObaActivite
                    Case 7
                        NoObjet = VI.ObjetPer.ObaGrade
                    Case 8
                        NoObjet = VI.ObjetPer.ObaSociete
                    Case 9
                        NoObjet = VI.ObjetPer.ObaOrganigramme
                    Case 10
                        NoObjet = VI.ObjetPer.ObaPresence
                    Case 11
                        NoObjet = VI.ObjetPer.ObaAbsence
                    Case 12
                        NoObjet = VI.ObjetPer.ObaFormation
                    Case Else
                        HNumPage.Value = "20"
                        Exit Sub
                End Select
                Select Case CInt(HNumPage.Value)
                    Case 11, 12
                        LstRes = WebFct.PointeurGlobal.SelectionObjetDate(Nothing, WebFct.ViRhDates.CalcDateMoinsJour(WsCtl_Cache.DateValeur, "0", "360"), WebFct.ViRhDates.CalcDateMoinsJour(WsCtl_Cache.DateValeur, "366", "0"),
                                                                                        NoObjet, RequeteFiltre)
                    Case Else
                        LstRes = WebFct.PointeurGlobal.SelectionObjetValable(0, WebFct.ViRhDates.DateSaisieVerifiee("31" & Strings.Right(WsCtl_Cache.DateValeur, 8)),
                                                                                        NoObjet, RequeteFiltre)
                End Select
                If LstRes IsNot Nothing Then
                    LstFiches.AddRange(LstRes)
                    CType(WebFct.PointeurUtilisateur.V_PointeurSessionModule, Virtualia.Net.Session.SessionModule).ExeListeFiches = LstFiches
                End If

            Case Else
                If CType(WebFct.PointeurUtilisateur.V_PointeurSessionModule, Virtualia.Net.Session.SessionModule).ExeListeFiches IsNot Nothing Then
                    Call FaireListeEdition(WsCtl_Cache.Clef)
                End If
                HNumPage.Value = "0"
                HorlogeExec.Enabled = False
                EtiTraitement.Text = "Préparation pour  la période sélectionnée " & WsCtl_Cache.ValeurSelection & " terminée"
                If CType(WebFct.PointeurUtilisateur.V_PointeurSessionModule, Virtualia.Net.Session.SessionModule).ExeListeMoisActivite IsNot Nothing Then
                    EtiTraitement.Text &= " pour " & CType(WebFct.PointeurUtilisateur.V_PointeurSessionModule, Virtualia.Net.Session.SessionModule).ExeListeMoisActivite.Count & " dossiers."
                Else
                    EtiTraitement.Text &= " mais aucune valeur trouvée."
                End If
                EtiHFin.Text = System.DateTime.Now.ToString("dd/MM/yyyy hh:mm:ss")
                Exit Sub
        End Select
        HNumPage.Value = CStr(CInt(HNumPage.Value) + 1)
    End Sub

    Private Sub FaireListeEdition(ByVal StructureN1 As String)
        Dim LstAllFiches As List(Of Virtualia.Systeme.MetaModele.VIR_FICHE)
        Dim ListeEtatCivil As List(Of Virtualia.Systeme.MetaModele.VIR_FICHE)
        Dim ListeDossiers As List(Of Virtualia.Net.WebAppli.SelfV4.MoisActivite)
        Dim ItemDossier As Virtualia.Net.WebAppli.SelfV4.MoisActivite
        Dim LstIde As List(Of Integer)
        Dim DllExperte As Virtualia.Metier.Expertes.Partition
        Dim Chaine As String

        If WebFct.PointeurUtilisateur Is Nothing Then
            Exit Sub
        End If
        LstAllFiches = CType(WebFct.PointeurUtilisateur.V_PointeurSessionModule, Virtualia.Net.Session.SessionModule).ExeListeFiches
        If LstAllFiches Is Nothing Then
            Exit Sub
        End If
        ListeEtatCivil = LstAllFiches.FindAll(Function(Recherche) Recherche.NumeroObjet = VI.ObjetPer.ObaCivil).ToList
        If ListeEtatCivil Is Nothing Then
            Exit Sub
        End If
        LstIde = New List(Of Integer)
        ListeDossiers = New List(Of Virtualia.Net.WebAppli.SelfV4.MoisActivite)
        For Each FichePER As Virtualia.Systeme.MetaModele.VIR_FICHE In ListeEtatCivil
            ItemDossier = New Virtualia.Net.WebAppli.SelfV4.MoisActivite(WebFct.PointeurUtilisateur, WsCtl_Cache.DateValeur_ToDate.Year,
                            WsCtl_Cache.DateValeur_ToDate.Month, OptionVisa.RadioGaucheCheck, LstAllFiches.FindAll(Function(Recherche) Recherche.Ide_Dossier = FichePER.Ide_Dossier).ToList)
            If ItemDossier.SiOK = True Then
                Select Case StructureN1
                    Case "(Tout)", ""
                        ListeDossiers.Add(ItemDossier)
                        LstIde.Add(FichePER.Ide_Dossier)
                    Case Else
                        If ItemDossier.Structure_N1 = StructureN1 Then
                            ListeDossiers.Add(ItemDossier)
                            LstIde.Add(FichePER.Ide_Dossier)
                        End If
                End Select
            End If
        Next
        DllExperte = WebFct.PointeurGlobal.PointeurDllExpert
        DllExperte.PreselectiondeDossiers = LstIde
        DllExperte.ClasseExperte(VI.PointdeVue.PVueApplicatif, VI.ObjetPer.ObaActivite) = VI.OptionInfo.DicoExport
        For Each ItemDossier In ListeDossiers
            Chaine = DllExperte.Donnee(VI.PointdeVue.PVueApplicatif, VI.ObjetPer.ObaActivite, 930, ItemDossier.Identifiant,
                                        WsCtl_Cache.DateValeur, WebFct.ViRhDates.DateSaisieVerifiee("31" & Strings.Right(WsCtl_Cache.DateValeur, 8)))
            ItemDossier.ETP_Activite = WebFct.ViRhFonction.ConversionDouble(Chaine)

        Next
        CType(WebFct.PointeurUtilisateur.V_PointeurSessionModule, Virtualia.Net.Session.SessionModule).ExeListeMoisActivite = ListeDossiers
    End Sub

    Private Sub CommandeExec_Click(sender As Object, e As System.EventArgs) Handles CommandeExec.Click
        EtiHDebut.Text = System.DateTime.Now.ToString("dd/MM/yyyy hh:mm:ss")
        HNumPage.Value = "1"
        HorlogeExec.Enabled = True
    End Sub

    Private Sub FaireListeMois()
        Dim Liste_Mois As System.Text.StringBuilder
        Dim Libel As String
        Dim MoisW As Integer
        Dim AnneeW As Integer

        AnneeW = Now.Year - 1
        MoisW = 1
        Liste_Mois = New System.Text.StringBuilder
        Do
            Liste_Mois.Append(WebFct.ViRhDates.MoisEnClair(MoisW) & Strings.Space(1) & AnneeW & VI.Tild)
            MoisW += 1
            If MoisW > 12 Then
                MoisW = 1
                AnneeW += 1
                If AnneeW > Now.Year Then
                    Exit Do
                End If
            End If
            If AnneeW = Now.Year And MoisW > Now.Month Then
                Exit Do
            End If
        Loop
        Libel = "Aucun mois" & VI.Tild & "Un mois" & VI.Tild & "mois"
        VComboMois.V_Liste(Libel) = Liste_Mois.ToString
        VComboMois.LstText = WebFct.ViRhDates.MoisEnClair(Now.Month) & Strings.Space(1) & Now.Year

        WsCtl_Cache = CacheVirControle
        WsCtl_Cache.DateValeur = "01/" & Strings.Format(Now.Month, "00") & "/" & AnneeW
        WsCtl_Cache.ValeurSelection = WebFct.ViRhDates.MoisEnClair(Now.Month) & Strings.Space(1) & Now.Year
        CacheVirControle = WsCtl_Cache
    End Sub

    Private Sub FaireListNiveau1()
        Dim Libel As String
        Libel = "Aucune structure" & VI.Tild & "Une structure" & VI.Tild & "structures"
        VComboStructureN1.V_Liste(Libel) = "(Tout)" & VI.Tild & CType(WebFct.PointeurGlobal, Virtualia.Net.Session.ObjetGlobal).LesStructures_N1
        VComboStructureN1.LstText = "(Tout)"
        WsCtl_Cache = CacheVirControle
        WsCtl_Cache.Clef = ""
        CacheVirControle = WsCtl_Cache
    End Sub

    Private Sub VComboMois_ValeurChange(sender As Object, e As Systeme.Evenements.DonneeChangeEventArgs) Handles VComboMois.ValeurChange
        WsCtl_Cache = CacheVirControle
        Select Case Strings.Left(e.Valeur, 4)
            Case "Janv"
                WsCtl_Cache.DateValeur = "01/01/" & Strings.Right(e.Valeur, 4)
            Case "Févr"
                WsCtl_Cache.DateValeur = "01/02/" & Strings.Right(e.Valeur, 4)
            Case "Mars"
                WsCtl_Cache.DateValeur = "01/03/" & Strings.Right(e.Valeur, 4)
            Case "Avri"
                WsCtl_Cache.DateValeur = "01/04/" & Strings.Right(e.Valeur, 4)
            Case "Mai "
                WsCtl_Cache.DateValeur = "01/05/" & Strings.Right(e.Valeur, 4)
            Case "Juin"
                WsCtl_Cache.DateValeur = "01/06/" & Strings.Right(e.Valeur, 4)
            Case "Juil"
                WsCtl_Cache.DateValeur = "01/07/" & Strings.Right(e.Valeur, 4)
            Case "Août"
                WsCtl_Cache.DateValeur = "01/08/" & Strings.Right(e.Valeur, 4)
            Case "Sept"
                WsCtl_Cache.DateValeur = "01/09/" & Strings.Right(e.Valeur, 4)
            Case "Octo"
                WsCtl_Cache.DateValeur = "01/10/" & Strings.Right(e.Valeur, 4)
            Case "Nove"
                WsCtl_Cache.DateValeur = "01/11/" & Strings.Right(e.Valeur, 4)
            Case "Déce"
                WsCtl_Cache.DateValeur = "01/12/" & Strings.Right(e.Valeur, 4)
        End Select
        WsCtl_Cache.ValeurSelection = e.Valeur
        CacheVirControle = WsCtl_Cache
    End Sub

    Private Sub VComboStructureN1_ValeurChange(sender As Object, e As DonneeChangeEventArgs) Handles VComboStructureN1.ValeurChange
        WsCtl_Cache = CacheVirControle
        WsCtl_Cache.Clef = e.Valeur
        CacheVirControle = WsCtl_Cache
    End Sub

    Private Sub CommandeEdit_Click(sender As Object, e As EventArgs) Handles CommandeCsv.Click, CommandeTxt.Click
        Dim UtiSession As Virtualia.Net.Session.SessionModule = CType(WebFct.PointeurUtilisateur.V_PointeurSessionModule, Virtualia.Net.Session.SessionModule)
        Dim NomFichier As String = ""
        If UtiSession Is Nothing Then
            Exit Sub
        End If
        If UtiSession.ExeListeMoisActivite Is Nothing Then
            Exit Sub
        End If
        If UtiSession.VParent.V_NomdeConnexion <> "" Then
            NomFichier = WebFct.PointeurGlobal.VirRepertoireTemporaire & "\" & UtiSession.VParent.V_NomdeConnexion & "." & Strings.Right(CType(sender, System.Web.UI.Control).ID, 3)
        ElseIf UtiSession.DossierPER IsNot Nothing Then
            NomFichier = WebFct.PointeurGlobal.VirRepertoireTemporaire & "\" & UtiSession.DossierPER.Nom & "." & Strings.Right(CType(sender, System.Web.UI.Control).ID, 3)
        Else
            Exit Sub
        End If
        Dim FluxTeleChargement As Byte()

        Call EcrireFichier(NomFichier, Strings.Right(CType(sender, System.Web.UI.Control).ID, 3))

        If My.Computer.FileSystem.FileExists(NomFichier) = False Then
            Exit Sub
        End If
        FluxTeleChargement = My.Computer.FileSystem.ReadAllBytes(NomFichier)
        If FluxTeleChargement IsNot Nothing Then
            Dim response As System.Web.HttpResponse = System.Web.HttpContext.Current.Response
            response.Clear()
            response.AddHeader("Content-Type", "binary/octet-stream")
            response.AddHeader("Content-Disposition", "attachment; filename=" & NomFichier & "; size=" & FluxTeleChargement.Length.ToString())
            response.Flush()
            response.BinaryWrite(FluxTeleChargement)
            response.Flush()
            response.End()
        End If
    End Sub

    Private Sub EcrireFichier(ByVal Nom_Fichier As String, ByVal Extension As String)
        Dim FicStream As System.IO.FileStream
        Dim FicWriter As System.IO.StreamWriter
        Dim SysCodeIso As System.Text.Encoding = System.Text.Encoding.GetEncoding(1252)
        Dim ListeDossiers As List(Of Virtualia.Net.WebAppli.SelfV4.MoisActivite)
        Dim LstLignes As List(Of String)
        Dim Sep As String = Strings.Chr(9)

        If WebFct Is Nothing Then
            WebFct = New Virtualia.Net.Controles.WebFonctions(Me, 0)
        End If
        ListeDossiers = CType(WebFct.PointeurUtilisateur.V_PointeurSessionModule, Virtualia.Net.Session.SessionModule).ExeListeMoisActivite
        If ListeDossiers Is Nothing OrElse ListeDossiers.Count = 0 Then
            Exit Sub
        End If
        If Extension = "Csv" Then
            Sep = VI.PointVirgule
        End If
        FicStream = New System.IO.FileStream(Nom_Fichier, IO.FileMode.Create, IO.FileAccess.Write)
        FicWriter = New System.IO.StreamWriter(FicStream, SysCodeIso)

        FicWriter.WriteLine(ListeDossiers.Item(0).Entete_Grid_No1(Sep))
        For Each ItemMois As Virtualia.Net.WebAppli.SelfV4.MoisActivite In ListeDossiers
            LstLignes = ItemMois.Lignes_Grid_No1(Sep)
            If LstLignes IsNot Nothing Then
                For Each Ligne As String In LstLignes
                    FicWriter.WriteLine(Ligne)
                Next
            End If
        Next

        'FicWriter.WriteLine(ListeDossiers.Item(0).Entete_Grid_No2(Sep))
        'For Each ItemMois As Virtualia.Net.WebAppli.SelfV4.MoisActivite In ListeDossiers
        '    LstLignes = ItemMois.Lignes_Grid_No2(Sep)
        '    For Each Ligne As String In LstLignes
        '        FicWriter.WriteLine(Ligne)
        '    Next
        'Next

        FicWriter.Flush()
        FicWriter.Close()
    End Sub

    Private Sub CommandeRetour_Click(sender As Object, e As ImageClickEventArgs) Handles CommandeRetour.Click
        Dim Evenement As New Virtualia.Systeme.Evenements.MessageRetourEventArgs("E", 0, "", "")
        Call V_Retour(Evenement)
    End Sub

End Class