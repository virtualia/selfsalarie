﻿Option Strict On
Option Explicit On
Option Compare Text
Imports VI = Virtualia.Systeme.Constantes
Public Class VirtualiaFenetre_PER_EtatIndividuel_Astreintes
    Inherits Virtualia.Net.Controles.ObjetWebControle
    Private WsDossierPer As Virtualia.Net.WebAppli.SelfV4.DossierPersonne = Nothing
    Private WsNombreAstreinte As Integer
    Private WsNomStateUti As String = "Utilisateur"
    Private WsCacheUti As ArrayList
    Private WsAnneeSelection As Integer
    Private WsMoisSelection As Integer
    Private V_ListeFichesMois As List(Of String) = Nothing
    Private V_ListeFichesMoisSelection As List(Of String) = Nothing
    Private V_ListeFichesMoisSelectionSamedi As List(Of String) = Nothing
    Private V_ListeFichesMoisSelectionDimanche As List(Of String) = Nothing
    Private V_ListeFichesMoisSelectionJourFerie As List(Of String) = Nothing
    Private WsListeFiches As List(Of String) = Nothing

    Public WriteOnly Property Identifiant() As Integer
        Set(ByVal value As Integer)
            If V_Identifiant <> value Then
                V_Identifiant = value
            End If
        End Set
    End Property

    'Protected Overrides Sub Page_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init
    '    Dim Tableaudata(0) As String
    '    Tableaudata = Strings.Split(Request.Url.AbsoluteUri, "/Fenetres/", -1)

    '    '' CadreLogoCNED.BackImageUrl = Tableaudata(0) & "/Images/General/LogoCNED.jpg"
    'End Sub

    Protected Overrides Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        Dim Chaine1 As String = ""
        Dim Chaine2 As String = ""
        Dim NbSamedi As Integer = 0
        Dim NbDimanche As Integer = 0
        Dim NbJourFerie As Integer = 0
        Dim DateSamedi As String = ""
        Dim DateDimanche As String = ""
        Dim DateJourFerie As String = ""
        Dim NbMax As Integer = 0
        Dim MontantTauxAstreinte As Double
        Dim MontantAstreinte As Double
        Dim Fiche_Etablissement As TablesObjet.ShemaPER.PER_COLLECTIVITE = Nothing
        Dim Fiche_Affectation As TablesObjet.ShemaPER.PER_AFFECTATION = Nothing
        Dim LstRes As List(Of Virtualia.Systeme.MetaModele.VIR_FICHE) = Nothing
        Dim Fiche_Annualisation As TablesObjet.ShemaPER.PER_ANNUALISATION = Nothing

        I_TableAsstreinte.Rows.Clear()

        Dim UtiSession As Virtualia.Net.Session.SessionModule = CType(V_WebFonction.PointeurUtilisateur.V_PointeurSessionModule, Virtualia.Net.Session.SessionModule)
        If UtiSession Is Nothing Then
            Exit Sub
        End If

        WsDossierPer = UtiSession.DossierPER
        WsMoisSelection = UtiSession.DossierPER.MoisSelection
        WsAnneeSelection = UtiSession.DossierPER.AnneeSelection

        Fiche_Etablissement = UtiSession.DossierPER.VFiche_Etablissement(V_WebFonction.ViRhDates.DateduJour)
        Fiche_Affectation = UtiSession.DossierPER.VFiche_Affectation(V_WebFonction.ViRhDates.DateduJour)
        LstRes = UtiSession.DossierPER.VParent.SelectionObjetValable(V_Identifiant, V_WebFonction.ViRhDates.DateduJour, VI.ObjetPer.ObaAnnualisation)
        If LstRes IsNot Nothing Then
            Fiche_Annualisation = CType(LstRes.Item(0), Virtualia.TablesObjet.ShemaPER.PER_ANNUALISATION)
        End If

        Dim LstDecision = CType(V_WebFonction.PointeurGlobal, Virtualia.Net.Session.ObjetGlobal).ListeTablesDecision(50)
        If LstDecision IsNot Nothing Then
            For Each OutDecision As Virtualia.TablesObjet.ShemaREF.OUT_DECISION In LstDecision
                If Year(V_WebFonction.ViRhDates.DateTypee(OutDecision.ValeurInterne(0))).ToString = Year(Now).ToString Then
                    MontantTauxAstreinte = V_WebFonction.ViRhFonction.ConversionDouble(OutDecision.CodeExterne(0))
                End If
            Next
        End If

        WsListeFiches = New List(Of String)
        V_ListeFichesMoisSelection = New List(Of String)
        V_ListeFichesMois = New List(Of String)
        V_ListeFichesMoisSelectionSamedi = New List(Of String)
        V_ListeFichesMoisSelectionDimanche = New List(Of String)
        V_ListeFichesMoisSelectionJourFerie = New List(Of String)

        WsListeFiches = UtiSession.DossierPER.V_ListeFiches


        If WsListeFiches IsNot Nothing Then
            V_ListeFichesMois = WsListeFiches
            For IndiceAstreinte = 0 To WsListeFiches.Count - 1
                Dim moisAstreinte = Strings.Right(Strings.Left(V_ListeFichesMois(IndiceAstreinte), 5), 2)
                If CInt(moisAstreinte) = WsMoisSelection Then
                    '' récupération de la nature
                    Dim TableauData(0) As String
                    TableauData = Strings.Split(V_ListeFichesMois(IndiceAstreinte), VI.Tild)
                    If TableauData(2) = "Oui" Then
                        Select Case TableauData(1)
                            Case Is = "SAMEDI"
                                V_ListeFichesMoisSelectionSamedi.Add(TableauData(0))
                                NbSamedi += 1
                            Case Is = "DIMANCHE"
                                V_ListeFichesMoisSelectionDimanche.Add(TableauData(0))
                                NbDimanche += 1
                            Case Is = "JOUR FERIE"
                                V_ListeFichesMoisSelectionJourFerie.Add(TableauData(0))
                                NbJourFerie += 1
                        End Select
                        V_ListeFichesMoisSelection.Add(V_ListeFichesMois(IndiceAstreinte))
                    End If
                End If
            Next
        End If

        NbMax = Max(NbSamedi, NbDimanche, NbJourFerie)
        WsNombreAstreinte = V_ListeFichesMoisSelection.Count


        For IndiceAstreinte = 0 To NbMax - 1
            Dim tRow As New TableRow()

            Dim tCell As New TableCell()
            tCell.Width = 250
            tCell.HorizontalAlign = HorizontalAlign.Center
            If IndiceAstreinte < NbSamedi Then
                DateSamedi = V_ListeFichesMoisSelectionSamedi(IndiceAstreinte)
            Else
                DateSamedi = ""
            End If
            tCell.Text = DateSamedi

            Dim tCell1 As New TableCell()
            tCell1.Width = 250
            tCell1.HorizontalAlign = HorizontalAlign.Center
            If IndiceAstreinte < NbDimanche Then
                DateDimanche = V_ListeFichesMoisSelectionDimanche(IndiceAstreinte)
            Else
                DateDimanche = ""
            End If
            tCell1.Text = DateDimanche

            Dim tCell2 As New TableCell()
            tCell2.Width = 247
            tCell2.HorizontalAlign = HorizontalAlign.Center
            If IndiceAstreinte < NbJourFerie Then
                DateJourFerie = V_ListeFichesMoisSelectionJourFerie(IndiceAstreinte)
            Else
                DateJourFerie = ""
            End If
            tCell2.Text = DateJourFerie
            ' Add new TableCell object to row.
            tRow.Cells.Add(tCell)
            tRow.Cells.Add(tCell1)
            tRow.Cells.Add(tCell2)

            ' Add new row to table.
            I_TableAsstreinte.Rows.Add(tRow)
        Next


        'WsNombreAstreinte = V_ListeFiches.Count
        MontantAstreinte = MontantTauxAstreinte * WsNombreAstreinte

        Mois.Text = V_WebFonction.ViRhDates.MoisEnClair(WsMoisSelection) & Strings.Space(1) & WsAnneeSelection
        Juridiction.Text = Fiche_Etablissement.Administration
        Fonctions.Text = Fiche_Affectation.Fonction_exercee
        ServiceAstreinte.Text = Fiche_Annualisation.RegimeduContrat
        NombreAstreinte.Text = WsNombreAstreinte.ToString
        Remuneration.Text = Strings.Format(MontantAstreinte, "# ### ### ##0")
        DateEtat.Text = V_WebFonction.ViRhDates.DateduJour.ToString
        DirecteurGreffe.Text = ""
        NomEtPrenom.Text = UtiSession.DossierPER.Civilite & Strings.Space(1) & UtiSession.DossierPER.Nom & Strings.Space(1) & UtiSession.DossierPER.Prenom




    End Sub

    Private Function Max(nbSamedi As Integer, nbDimanche As Integer, nbJourFerie As Integer) As Integer
        Dim NbMax As Integer = 0
        If (nbSamedi > nbDimanche) Then
            NbMax = nbSamedi
        Else
            NbMax = nbDimanche
            If (NbMax > nbJourFerie) Then
                NbMax = NbMax
            Else
                NbMax = nbJourFerie
            End If
        End If
        Return NbMax
    End Function

    Private Sub FaireListeChoix()
        Dim UtiSession As Virtualia.Net.Session.SessionModule = CType(V_WebFonction.PointeurUtilisateur.V_PointeurSessionModule, Virtualia.Net.Session.SessionModule)
        If UtiSession Is Nothing Then
            Exit Sub
        End If
        Dim MontantTauxAstreinte As Double
        Dim MontantAstreinte As Double
        Dim Fiche_Etablissement As TablesObjet.ShemaPER.PER_COLLECTIVITE = Nothing
        Dim Fiche_Affectation As TablesObjet.ShemaPER.PER_AFFECTATION = Nothing
        Dim LstRes As List(Of Virtualia.Systeme.MetaModele.VIR_FICHE) = Nothing
        Dim Fiche_Annualisation As TablesObjet.ShemaPER.PER_ANNUALISATION = Nothing

        Fiche_Etablissement = UtiSession.DossierPER.VFiche_Etablissement(V_WebFonction.ViRhDates.DateduJour)
        Fiche_Affectation = UtiSession.DossierPER.VFiche_Affectation(V_WebFonction.ViRhDates.DateduJour)
        LstRes = UtiSession.DossierPER.VParent.SelectionObjetValable(V_Identifiant, V_WebFonction.ViRhDates.DateduJour, VI.ObjetPer.ObaAnnualisation)
        If LstRes IsNot Nothing Then
            Fiche_Annualisation = CType(LstRes.Item(0), Virtualia.TablesObjet.ShemaPER.PER_ANNUALISATION)
        End If

        Dim LstDecision = CType(V_WebFonction.PointeurGlobal, Virtualia.Net.Session.ObjetGlobal).ListeTablesDecision(50)
        If LstDecision IsNot Nothing Then
            For Each OutDecision As Virtualia.TablesObjet.ShemaREF.OUT_DECISION In LstDecision
                If Year(V_WebFonction.ViRhDates.DateTypee(OutDecision.ValeurInterne(0))).ToString = Year(Now).ToString Then
                    MontantTauxAstreinte = V_WebFonction.ViRhFonction.ConversionDouble(OutDecision.CodeExterne(0))
                End If
            Next
        End If
        'WsNombreAstreinte = V_ListeFiches.Count
        MontantAstreinte = MontantTauxAstreinte * WsNombreAstreinte
        WsMoisSelection = UtiSession.DossierPER.MoisSelection
        WsAnneeSelection = UtiSession.DossierPER.AnneeSelection
        Mois.Text = V_WebFonction.ViRhDates.MoisEnClair(WsMoisSelection) & Strings.Space(1) & WsAnneeSelection
        Juridiction.Text = Fiche_Etablissement.Administration
        Fonctions.Text = Fiche_Affectation.Fonction_exercee
        ServiceAstreinte.Text = Fiche_Annualisation.RegimeduContrat
        NombreAstreinte.Text = WsNombreAstreinte.ToString
        Remuneration.Text = Strings.Format(MontantAstreinte, "# ### ### ##0")
        DateEtat.Text = V_WebFonction.ViRhDates.DateduJour.ToString
        DirecteurGreffe.Text = ""
        NomEtPrenom.Text = UtiSession.DossierPER.Civilite & Strings.Space(1) & UtiSession.DossierPER.Nom & Strings.Space(1) & UtiSession.DossierPER.Prenom
    End Sub

    Private Sub LireLaFiche()
        Dim CacheDonnee As List(Of String) = V_CacheMaj
        If CacheDonnee Is Nothing Then
            Exit Sub
        End If
        Dim NumInfo As Integer
        Dim SiEnLecture As Boolean = False
        Dim Ctl As Control
        Dim VirControle As Controles_VCoupleEtiDonnee
        Dim IndiceI As Integer = 0
        If CacheDonnee(5) IsNot Nothing Then
            If CacheDonnee(5).ToString <> "" And CacheDonnee(5).ToString <> V_WebFonction.ViRhDates.DateduJour Then
                SiEnLecture = True
            End If
        End If
        Dim UtiSession As Virtualia.Net.Session.SessionModule = CType(V_WebFonction.PointeurUtilisateur.V_PointeurSessionModule, Virtualia.Net.Session.SessionModule)
        If UtiSession Is Nothing Then
            SiEnLecture = True
        End If
        Do
            Ctl = V_WebFonction.VirWebControle(Me.CadreInfo, "InfoH", IndiceI)
            If Ctl Is Nothing Then
                Exit Do
            End If
            NumInfo = CInt(Strings.Right(Ctl.ID, 2))
            VirControle = CType(Ctl, Controles_VCoupleEtiDonnee)
            If CacheDonnee(NumInfo) Is Nothing Then
                VirControle.DonText = ""
            Else
                VirControle.DonText = CacheDonnee(NumInfo).ToString
            End If
            VirControle.V_SiEnLectureSeule = True
            IndiceI += 1
        Loop

        IndiceI = 0
        Dim VirDonneeVerticale As Controles_VCoupleVerticalEtiDonnee
        Do
            Ctl = V_WebFonction.VirWebControle(Me.CadreInfo, "InfoV", IndiceI)
            If Ctl Is Nothing Then
                Exit Do
            End If
            NumInfo = CInt(Strings.Right(Ctl.ID, 2))
            VirDonneeVerticale = CType(Ctl, Controles_VCoupleVerticalEtiDonnee)
            If CacheDonnee(NumInfo) Is Nothing Then
                VirDonneeVerticale.DonText = ""
            Else
                VirDonneeVerticale.DonText = CacheDonnee(NumInfo).ToString
            End If
            VirDonneeVerticale.V_SiEnLectureSeule = SiEnLecture
            VirDonneeVerticale.V_SiAutoPostBack = Not SiEnLecture
            'If SiEnLecture = False Then
            '    If UtiSession.SiAccesGRH = True Then
            '        InfoV03.V_SiEnLectureSeule = True
            '        InfoV06.V_SiEnLectureSeule = False
            '        InfoV06.V_SiAutoPostBack = True
            '    Else
            '        InfoV03.V_SiEnLectureSeule = False
            '        InfoV03.V_SiAutoPostBack = True
            '        InfoV06.V_SiEnLectureSeule = True
            '    End If
            'End If
            IndiceI += 1
        Loop

        Dim VirDonneeDate As VCoupleEtiDate
        IndiceI = 0
        Do
            Ctl = V_WebFonction.VirWebControle(Me.CadreInfo, "InfoD", IndiceI)
            If Ctl Is Nothing Then
                Exit Do
            End If
            NumInfo = CInt(Strings.Right(Ctl.ID, 2))
            VirDonneeDate = CType(Ctl, VCoupleEtiDate)
            If CacheDonnee(NumInfo) Is Nothing Then
                VirDonneeDate.DonText = ""
            Else
                VirDonneeDate.DonText = CacheDonnee(NumInfo).ToString
            End If
            If VirDonneeDate.SiDateFin = True Then
                VirDonneeDate.DateDebut = CacheDonnee(0).ToString
            End If
            VirDonneeDate.V_SiEnLectureSeule = SiEnLecture
            VirDonneeDate.V_SiAutoPostBack = Not SiEnLecture
            IndiceI += 1
        Loop

        Dim VirRadioH As Controles_VTrioHorizontalRadio
        IndiceI = 0
        Do
            Ctl = V_WebFonction.VirWebControle(Me.CadreInfo, "RadioH", IndiceI)
            If Ctl Is Nothing Then
                Exit Do
            End If
            NumInfo = CInt(Strings.Right(Ctl.ID, 2))
            VirRadioH = CType(Ctl, Controles_VTrioHorizontalRadio)
            VirRadioH.RadioGaucheCheck = False
            VirRadioH.RadioCentreCheck = False
            VirRadioH.RadioDroiteCheck = False
            If CacheDonnee(NumInfo) IsNot Nothing Then
                If CacheDonnee(NumInfo).ToString <> "" Then
                    Select Case CacheDonnee(NumInfo).ToString
                        Case Is = VirRadioH.RadioGaucheText
                            VirRadioH.RadioGaucheCheck = True
                        Case Is = VirRadioH.RadioCentreText
                            VirRadioH.RadioCentreCheck = True
                        Case Is = VirRadioH.RadioDroiteText
                            VirRadioH.RadioDroiteCheck = True
                    End Select
                End If
            End If
            VirRadioH.V_SiEnLectureSeule = SiEnLecture
            VirRadioH.V_SiAutoPostBack = Not SiEnLecture
            If SiEnLecture = False Then
                If UtiSession.SiAccesGRH = True Then
                    VirRadioH.V_SiEnLectureSeule = False
                    VirRadioH.V_SiAutoPostBack = True
                Else
                    VirRadioH.V_SiEnLectureSeule = True
                End If
            End If
            IndiceI += 1
        Loop

    End Sub

    Private Sub ActualiserListe()
        Dim LstLibels As New List(Of String)
        'LstLibels.Add("Aucune journée d'astreinte")
        'LstLibels.Add("Une journée d'astreinte")
        'LstLibels.Add("journées d'astreinte")
        'ListeGrille.V_LibelCaption = LstLibels

        'Dim LstColonnes As New List(Of String)
        'LstColonnes.Add("Samedi")
        'LstColonnes.Add("Dimanche")
        'LstColonnes.Add("Jour férié")
        'LstColonnes.Add("Date validation")
        'ListeGrille.V_LibelColonne = LstColonnes

        'If V_Identifiant > 0 Then
        '    Call InitialiserControles()
        '    If V_IndexFiche = -1 Then
        '        ListeGrille.V_Liste = Nothing
        '        Exit Sub
        '    End If

        '    V_ListeFichesMoisSelection = New List(Of String)
        '    V_ListeFichesMois = New List(Of String)

        '    If V_ListeFiches IsNot Nothing Then
        '        V_ListeFichesMois = V_ListeFiches
        '        For IndiceAstreinte = 0 To V_ListeFiches.Count - 1
        '            Dim moisAstreinte = Strings.Right(Strings.Left(V_ListeFichesMois(IndiceAstreinte), 5), 2)
        '            If CInt(moisAstreinte) = WsMoisSelection Then
        '                V_ListeFichesMoisSelection.Add(V_ListeFichesMois(IndiceAstreinte))
        '            End If

        '        Next
        '    End If
        '    ListeGrille.V_Liste = V_ListeFichesMoisSelection
        '    WsNombreAstreinte = V_ListeFichesMoisSelection.Count

        'End If
        'FaireListeChoix()
    End Sub

    Private ReadOnly Property ValeurLue(ByVal NoObjet As Integer, ByVal NoInfo As Integer) As String
        Get
            If WsDossierPer Is Nothing Then
                Dim UtiSession As Virtualia.Net.Session.SessionModule = CType(V_WebFonction.PointeurUtilisateur.V_PointeurSessionModule, Virtualia.Net.Session.SessionModule)
                If UtiSession IsNot Nothing Then
                    WsDossierPer = UtiSession.DossierPER
                End If
                If WsDossierPer Is Nothing Then
                    Return ""
                End If
            End If
            'If WsDossierPer.Is Nothing Then
            '    Return ""
            'End If
            'Select Case NoObjet
            '    Case 151
            '        If WsDossierPer.Objet_151(Rang) IsNot Nothing Then
            '            Return WsDossierPer.Objet_151(Rang).V_TableauData(NoInfo).ToString
            '        End If
            '    Case 154
            '        If WsDossierPer.Objet_154(Rang) IsNot Nothing Then
            '            Return WsDossierPer.Objet_154(Rang).V_TableauData(NoInfo).ToString
            '        End If
            'End Select
            'Return ""
        End Get
    End Property


    Public Property V_NomUtilisateur As String
        Get
            If Me.ViewState(WsNomStateUti) Is Nothing Then
                Return ""
            End If
            WsCacheUti = CType(Me.ViewState(WsNomStateUti), ArrayList)
            If WsCacheUti IsNot Nothing Then
                Return WsCacheUti(0).ToString
            Else
                Return ""
            End If
        End Get
        Set(ByVal value As String)
            If Me.ViewState(WsNomStateUti) IsNot Nothing Then
                Me.ViewState.Remove(WsNomStateUti)
            End If
            WsCacheUti = New ArrayList
            WsCacheUti.Add(value)
            Me.ViewState.Add(WsNomStateUti, WsCacheUti)
        End Set
    End Property

End Class