﻿Option Explicit On
Option Strict On
Option Compare Text
Imports VI = Virtualia.Systeme.Constantes
Namespace WebAppli
    Namespace SelfV4
        Public Class DossierHeuresSup
            Private WsParent As Virtualia.Net.WebAppli.SelfV4.DossierPersonne

            Private WsLstHeuresSup As List(Of Virtualia.TablesObjet.ShemaPER.PER_EVENEMENTJOUR)
            Private WsLstRecuperation As List(Of Virtualia.TablesObjet.ShemaPER.PER_ABSENCE)
            Private WsLstPlanning As List(Of Virtualia.TablesObjet.ShemaPER.PER_TRAVAIL)

            Private WsTotalHeuresSup As Double
            Private WsTotalRecuperations As Double
            Private WsSolde As Double
            Private WsSoldeAnterieur As Double

            Public ReadOnly Property TotalCreditHeuresSup(ByVal Annee As String) As String
                Get
                    Dim TotalHSAnte As Double
                    Dim TotalRecupAnte As Double

                    WsTotalHeuresSup = 0
                    WsTotalRecuperations = 0
                    WsSolde = 0
                    WsSoldeAnterieur = 0

                    If WsLstHeuresSup IsNot Nothing Then
                        For Each FichePER In WsLstHeuresSup
                            If FichePER.Date_Valeur_ToDate.Year < CInt(Annee) Then
                                If FichePER.V_Coefficient_HS > 0 Then
                                    TotalHSAnte += WsParent.VParent.VirRhFonction.ConversionDouble(WsParent.VParent.VirRhDates.CalcHeure(FichePER.Duree, "", 1)) * FichePER.V_Coefficient_HS
                                ElseIf FichePER.V_Forfait_HS > 0 Then
                                    TotalHSAnte += FichePER.V_Forfait_HS
                                End If
                            End If
                            If FichePER.Date_Valeur_ToDate.Year = CInt(Annee) Then
                                If FichePER.V_Coefficient_HS > 0 Then
                                    WsTotalHeuresSup += WsParent.VParent.VirRhFonction.ConversionDouble(WsParent.VParent.VirRhDates.CalcHeure(FichePER.Duree, "", 1)) * FichePER.V_Coefficient_HS
                                ElseIf FichePER.V_Forfait_HS > 0 Then
                                    WsTotalHeuresSup += FichePER.V_Forfait_HS
                                End If
                            End If
                        Next
                    End If
                    If WsLstRecuperation IsNot Nothing Then
                        For Each FichePERABS In WsLstRecuperation
                            If FichePERABS.Date_Valeur_ToDate.Year < CInt(Annee) Then
                                TotalRecupAnte += FichePERABS.JoursOuvres * HoraireJournalier(FichePERABS.Date_de_Valeur)
                            End If
                            If FichePERABS.Date_Valeur_ToDate.Year = CInt(Annee) Then
                                WsTotalRecuperations += FichePERABS.JoursOuvres * HoraireJournalier(FichePERABS.Date_de_Valeur)
                            End If
                        Next
                    End If
                    WsSoldeAnterieur = TotalHSAnte - TotalRecupAnte
                    WsSolde = WsTotalHeuresSup - WsTotalRecuperations + WsSoldeAnterieur
                    Return WsParent.VParent.VirRhDates.CalcHeure(CStr(CInt(WsTotalHeuresSup)), "", 2)
                End Get
            End Property

            Public ReadOnly Property TotalRecuperations As String
                Get
                    Return WsParent.VParent.VirRhDates.CalcHeure(CStr(CInt(WsTotalRecuperations)), "", 2)
                End Get
            End Property

            Public ReadOnly Property Solde_HS As String
                Get
                    If WsSolde <= 0 Then
                        Return ""
                    End If
                    Return WsParent.VParent.VirRhDates.CalcHeure(CStr(CInt(WsSolde)), "", 2)
                End Get
            End Property

            Public ReadOnly Property Solde_HS_Anterieur As String
                Get
                    If WsSoldeAnterieur <= 0 Then
                        Return ""
                    End If
                    Return WsParent.VParent.VirRhDates.CalcHeure(CStr(CInt(WsSoldeAnterieur)), "", 2)
                End Get
            End Property

            Public ReadOnly Property ListeDesHeuresSup(ByVal Annee As String) As List(Of Virtualia.TablesObjet.ShemaPER.PER_EVENEMENTJOUR)
                Get
                    If WsLstHeuresSup Is Nothing Then
                        Return Nothing
                    End If
                    Dim SousListe As New List(Of Virtualia.TablesObjet.ShemaPER.PER_EVENEMENTJOUR)
                    For Each FichePER In WsLstHeuresSup
                        If FichePER.Date_Valeur_ToDate.Year = CInt(Annee) Then
                            SousListe.Add(FichePER)
                        End If
                    Next
                    If SousListe.Count = 0 Then
                        Return Nothing
                    End If
                    Return SousListe
                End Get
            End Property

            Public ReadOnly Property ListeDesRecuperations(ByVal Annee As String) As List(Of Virtualia.TablesObjet.ShemaPER.PER_ABSENCE)
                Get
                    If WsLstRecuperation Is Nothing Then
                        Return Nothing
                    End If
                    Dim SousListe As New List(Of Virtualia.TablesObjet.ShemaPER.PER_ABSENCE)
                    For Each FichePERABS In WsLstRecuperation
                        If FichePERABS.Date_Valeur_ToDate.Year = CInt(Annee) Then
                            SousListe.Add(FichePERABS)
                        End If
                    Next
                    If SousListe.Count = 0 Then
                        Return Nothing
                    End If
                    Return SousListe
                End Get
            End Property

            Private WriteOnly Property ListedesFiches As List(Of Virtualia.Systeme.MetaModele.VIR_FICHE)
                Set(ByVal value As List(Of Virtualia.Systeme.MetaModele.VIR_FICHE))
                    Dim Predicat As Virtualia.Systeme.MetaModele.Predicats.PredicateFiche
                    Dim TabObjet As List(Of Integer)
                    Dim LstFiches As List(Of Virtualia.Systeme.MetaModele.VIR_FICHE)

                    TabObjet = New List(Of Integer)
                    TabObjet.Add(VI.ObjetPer.ObaPresence)
                    Predicat = New Virtualia.Systeme.MetaModele.Predicats.PredicateFiche(TabObjet)
                    LstFiches = value.FindAll(AddressOf Predicat.SiFicheDansListeObjets)
                    If LstFiches IsNot Nothing Then
                        WsLstPlanning = New List(Of Virtualia.TablesObjet.ShemaPER.PER_TRAVAIL)
                        For Each FichePER In LstFiches
                            WsLstPlanning.Add(CType(FichePER, Virtualia.TablesObjet.ShemaPER.PER_TRAVAIL))
                        Next
                        If WsLstPlanning.Count = 0 Then
                            WsLstPlanning = Nothing
                        End If
                    End If

                    TabObjet = New List(Of Integer)
                    TabObjet.Add(VI.ObjetPer.ObaAbsence)
                    Predicat = New Virtualia.Systeme.MetaModele.Predicats.PredicateFiche(TabObjet)
                    LstFiches = value.FindAll(AddressOf Predicat.SiFicheDansListeObjets)
                    If LstFiches IsNot Nothing Then
                        WsLstRecuperation = New List(Of Virtualia.TablesObjet.ShemaPER.PER_ABSENCE)
                        For Each FichePER In LstFiches
                            If CType(FichePER, Virtualia.TablesObjet.ShemaPER.PER_ABSENCE).Nature = "Crédit Heures supplémentaires" Then
                                WsLstRecuperation.Add(CType(FichePER, Virtualia.TablesObjet.ShemaPER.PER_ABSENCE))
                            End If
                        Next
                        If WsLstRecuperation.Count = 0 Then
                            WsLstRecuperation = Nothing
                        End If
                    End If

                    Dim Coefficient As Double
                    Dim Forfait As Double

                    TabObjet = New List(Of Integer)
                    TabObjet.Add(VI.ObjetPer.ObaJournee)
                    Predicat = New Virtualia.Systeme.MetaModele.Predicats.PredicateFiche(TabObjet)
                    LstFiches = value.FindAll(AddressOf Predicat.SiFicheDansListeObjets)
                    If LstFiches IsNot Nothing Then
                        WsLstHeuresSup = New List(Of Virtualia.TablesObjet.ShemaPER.PER_EVENEMENTJOUR)
                        For Each FichePER In LstFiches
                            Coefficient = Coefficient_HS(CType(FichePER, Virtualia.TablesObjet.ShemaPER.PER_EVENEMENTJOUR).Nature_Evt)
                            If Coefficient = 0 Then
                                Forfait = Forfait_HS(CType(FichePER, Virtualia.TablesObjet.ShemaPER.PER_EVENEMENTJOUR).Nature_Evt)
                            End If
                            If Coefficient > 0 Or Forfait > 0 Then
                                CType(FichePER, Virtualia.TablesObjet.ShemaPER.PER_EVENEMENTJOUR).V_Coefficient_HS = Coefficient
                                CType(FichePER, Virtualia.TablesObjet.ShemaPER.PER_EVENEMENTJOUR).V_Forfait_HS = Forfait
                                WsLstHeuresSup.Add(CType(FichePER, Virtualia.TablesObjet.ShemaPER.PER_EVENEMENTJOUR))
                            End If
                        Next
                        If WsLstHeuresSup.Count = 0 Then
                            WsLstHeuresSup = Nothing
                        End If
                    End If
                End Set
            End Property

            Private ReadOnly Property Coefficient_HS(ByVal NatureEvt As String) As Double
                Get
                    If WsParent.VParent.VirObjetPresence Is Nothing Then
                        Return 0
                    End If
                    Dim FichePresence As Virtualia.TablesObjet.ShemaREF.PRE_IDENTIFICATION = WsParent.VParent.VirObjetPresence.Fiche_Presence(NatureEvt)
                    If FichePresence Is Nothing Then
                        Return Nothing
                    End If
                    If FichePresence.SiHeure_Sup.ToUpper <> "OUI" Then
                        Return 0
                    End If
                    Return FichePresence.Coefficient
                End Get
            End Property

            Private ReadOnly Property Forfait_HS(ByVal NatureEvt As String) As Double
                Get
                    If WsParent.VParent.VirObjetPresence Is Nothing Then
                        Return 0
                    End If
                    Dim FichePresence As Virtualia.TablesObjet.ShemaREF.PRE_IDENTIFICATION = WsParent.VParent.VirObjetPresence.Fiche_Presence(NatureEvt)
                    If FichePresence Is Nothing Then
                        Return Nothing
                    End If
                    If FichePresence.SiHeure_Sup.ToUpper <> "OUI" Then
                        Return 0
                    End If
                    Return WsParent.VParent.VirRhFonction.ConversionDouble(WsParent.VParent.VirRhDates.CalcHeure(FichePresence.Forfait_Heures, "", 1))
                End Get
            End Property

            Public ReadOnly Property HoraireJournalier(ByVal ArgumentDate As String) As Double
                Get
                    If WsLstPlanning Is Nothing Then
                        Return 420
                    End If
                    For Each FichePER In WsLstPlanning
                        Select Case WsParent.VParent.VirRhDates.ComparerDates(FichePER.Date_de_Valeur, ArgumentDate)
                            Case VI.ComparaisonDates.Egalite, VI.ComparaisonDates.PlusPetit
                                Return WsParent.VParent.VirRhFonction.ConversionDouble(WsParent.VParent.VirRhDates.CalcHeure(FichePER.HoraireJournalier, "", 1))
                        End Select
                    Next
                    Return 420
                End Get
            End Property

            Private Sub LireFiches()
                '** Infos complémentaires
                Dim TableObjet(2) As Integer
                Dim TabIde As List(Of Integer)
                Dim ToutUnObjet As Virtualia.Ressources.Datas.EnsembleFiches
                Dim ListeAllFiches As List(Of Virtualia.Systeme.MetaModele.VIR_FICHE)

                TableObjet(0) = VI.ObjetPer.ObaAbsence
                TableObjet(1) = VI.ObjetPer.ObaJournee
                TableObjet(2) = VI.ObjetPer.ObaPresence
                TabIde = New List(Of Integer)
                TabIde.Add(WsParent.Ide_Dossier)
                ListeAllFiches = New List(Of Virtualia.Systeme.MetaModele.VIR_FICHE)
                For IndiceI = 0 To TableObjet.Count - 1
                    ToutUnObjet = New Virtualia.Ressources.Datas.EnsembleFiches(WsParent.VParent.VirNomUtilisateur,  VI.PointdeVue.PVueApplicatif)
                    ToutUnObjet.NumeroObjet(TabIde, True) = TableObjet(IndiceI)
                    ListeAllFiches.AddRange(ToutUnObjet.ListedesFiches)
                Next IndiceI
                ListedesFiches = ListeAllFiches
            End Sub

            Public Sub New(ByVal host As Virtualia.Net.WebAppli.SelfV4.DossierPersonne)
                WsParent = host
                Call LireFiches()
            End Sub

        End Class
    End Namespace
End Namespace