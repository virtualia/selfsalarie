﻿Public Class ErreurApplication
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        EtiDateJour.Text = Strings.Format(System.DateTime.Now, "D")
        EtiMessage.Text = Request.QueryString("Msg")
    End Sub

End Class