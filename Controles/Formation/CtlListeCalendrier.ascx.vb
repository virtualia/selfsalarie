﻿Option Strict On
Option Explicit On
Option Compare Text
Imports System.Drawing
Public Class CtlListeCalendrier
    Inherits System.Web.UI.UserControl
    Private WsAppelant As String
    Private WsCtl_Cache As Virtualia.Net.VCaches.CacheStdCalendrier
    Private WsNomStateCache As String = "VListeCalendrier"
    Private WebFct As Virtualia.Net.Controles.WebFonctions
    Private WsLstBoutons As List(Of System.Web.UI.WebControls.Button)
    Private Couleur_Paire As Color
    Private Couleur_ImPaire As Color
    Private CouleurWE_Entete As Color
    Private CouleurWE As Color
    Private CouleurFerie_Entete As Color
    Private CouleurFerie As Color
    Private CouleurBouton As Color
    Private CouleurFond_Normal As Color
    Private Couleur_PoliceNormal As Color
    Public Delegate Sub Valeur_ChangeEventHandler(ByVal sender As Object, ByVal e As Virtualia.Systeme.Evenements.DonneeChangeEventArgs)
    Public Event ValeurChange As Valeur_ChangeEventHandler
    '***********************************************************
    Protected Overridable Sub IdeSel_Change(ByVal e As Virtualia.Systeme.Evenements.DonneeChangeEventArgs)
        RaiseEvent ValeurChange(Me, e)
    End Sub
    Public Property IDAppelant As String
        Get
            Return WsAppelant
        End Get
        Set(value As String)
            WsAppelant = value
        End Set
    End Property
    Private ReadOnly Property V_WebFonction As Virtualia.Net.Controles.WebFonctions
        Get
            If WebFct Is Nothing Then
                WebFct = New Virtualia.Net.Controles.WebFonctions(Me, 0)
            End If
            Return WebFct
        End Get
    End Property
    Private Property CacheVirControle As Virtualia.Net.VCaches.CacheStdCalendrier
        Get
            If Me.ViewState(WsNomStateCache) IsNot Nothing Then
                Return CType(Me.ViewState(WsNomStateCache), Virtualia.Net.VCaches.CacheStdCalendrier)
            End If
            Dim NewCache As Virtualia.Net.VCaches.CacheStdCalendrier
            NewCache = New Virtualia.Net.VCaches.CacheStdCalendrier
            NewCache.Annee = System.DateTime.Now.Year
            NewCache.Mois = System.DateTime.Now.Month
            NewCache.Filtre_N1 = ""
            NewCache.Filtre_N2 = ""
            Return NewCache
        End Get
        Set(value As Virtualia.Net.VCaches.CacheStdCalendrier)
            If value Is Nothing Then
                Exit Property
            End If
            If Me.ViewState(WsNomStateCache) IsNot Nothing Then
                Me.ViewState.Remove(WsNomStateCache)
            End If
            Me.ViewState.Add(WsNomStateCache, value)
        End Set
    End Property

    Private Sub CtlListeCalendrier_Init(sender As Object, e As EventArgs) Handles Me.Init
        Couleur_Paire = Color.LightGray
        Couleur_ImPaire = V_WebFonction.ConvertCouleur("#E2E2E2")
        CouleurWE_Entete = Color.Gray
        CouleurWE = V_WebFonction.ConvertCouleur("#7D9F99")
        CouleurFerie_Entete = Color.Gray
        CouleurFerie = V_WebFonction.ConvertCouleur("#7D9F99")
        CouleurBouton = V_WebFonction.ConvertCouleur("#E9FDF9")
        CouleurFond_Normal = Color.Snow
        Couleur_PoliceNormal = Color.Gray
    End Sub
    Private Sub CtlListeCalendrier_Load(sender As Object, e As EventArgs) Handles Me.Load
        If IsPostBack = False Then
            Call PreparerLeCache()
            Call FaireListeChoix()
        End If
        Call ConstruireLeControle()
    End Sub
    Private Sub CtlListeCalendrier_PreRender(sender As Object, e As EventArgs) Handles Me.PreRender
        Call ColorierEntetes()
    End Sub
    Private Sub ReInitialiserControle()
        Dim LstRangees As List(Of System.Web.UI.WebControls.TableRow)
        LstRangees = (From Rangee In CadreLignes.Rows
                      Where DirectCast(Rangee, System.Web.UI.WebControls.TableRow).ID Is Nothing OrElse DirectCast(Rangee, System.Web.UI.WebControls.TableRow).ID.StartsWith("CadreLigne")
                      Select DirectCast(Rangee, System.Web.UI.WebControls.TableRow)).ToList()

        LstRangees.ForEach(Sub(Rangee)
                               CadreLignes.Rows.Remove(Rangee)
                           End Sub)
        Call ConstruireLeControle()
    End Sub
    Private Sub PreparerLeCache()
        Dim LstRes As List(Of Virtualia.Net.WebAppli.SelfV4.DossierFormation) = Nothing
        Dim LstTrie As List(Of Virtualia.Net.WebAppli.SelfV4.DossierFormation)
        Dim LigneSujet As Virtualia.Net.VCaches.CacheListeMensuelle = Nothing
        Dim Journee As Virtualia.Net.VCaches.CacheJournees = Nothing
        Dim RuptureIde As Integer = 0
        Dim LstFiltres As List(Of String)

        If WsCtl_Cache Is Nothing Then
            WsCtl_Cache = CacheVirControle
        End If
        'Lire les sujets concernés et préparer le cache
        LstRes = CType(V_WebFonction.PointeurUtilisateur.V_PointeurSessionModule, Virtualia.Net.Session.SessionModule).PointeurArmoireFOR.CalendrierSessions(WsCtl_Cache.Annee, WsCtl_Cache.Mois)
        If LstRes Is Nothing OrElse LstRes.Count = 0 Then
            CacheVirControle = WsCtl_Cache
            Exit Sub
        End If

        Select Case WsCtl_Cache.Filtre_N1
            Case ""
                LstTrie = (From Resultat In LstRes Order By Resultat.Identifiant).ToList
            Case Else
                LstTrie = (From Resultat In LstRes Where Resultat.Plan = WsCtl_Cache.Filtre_N1 Order By Resultat.Identifiant).ToList
        End Select
        If LstTrie Is Nothing OrElse LstTrie.Count = 0 Then
            Exit Sub
        End If
        For Each Resultat In LstTrie
            If Resultat.Identifiant <> RuptureIde Then
                If RuptureIde <> 0 Then
                    WsCtl_Cache.ListeSujets.Add(LigneSujet)
                End If
                LigneSujet = New Virtualia.Net.VCaches.CacheListeMensuelle
                LigneSujet.Identifiant = Resultat.Identifiant
                LigneSujet.Libelle = Resultat.IntituleStage
                LigneSujet.Filtre = Resultat.Plan
                LigneSujet.Bulle = Resultat.ToolTipSession
                LigneSujet.Bulle &= LigneSujet.Libelle
            End If
            Journee = New Virtualia.Net.VCaches.CacheJournees
            Journee.DateDebut = Resultat.Date_Debut
            Journee.DateFin = Resultat.Date_Fin
            Journee.Intitule = Resultat.IntituleSession
            Journee.Complement = ""
            Journee.HeureDebut_AM = Resultat.Heure_AM_Debut
            Journee.HeureFin_AM = Resultat.Heure_AM_Fin
            Journee.HeureDebut_PM = Resultat.Heure_PM_Debut
            Journee.HeureFin_PM = Resultat.Heure_PM_Fin
            LigneSujet.ListeJournees.Add(Journee)

            RuptureIde = Resultat.Identifiant
        Next
        WsCtl_Cache.ListeSujets.Add(LigneSujet)
        CacheVirControle = WsCtl_Cache

        Try
            LstFiltres = (From Resultat In LstRes Order By Resultat.Plan Select Resultat.Plan).Distinct.ToList
        Catch ex As Exception
            LstFiltres = Nothing
        End Try
        ListeChoixFiltreN1.Items.Clear()
        ListeChoixFiltreN1.Items.Add("")
        If LstFiltres Is Nothing OrElse LstFiltres.Count = 0 Then
            Exit Sub
        End If
        LstFiltres.ForEach(Sub(Valeur)
                               If Valeur <> "" Then
                                   ListeChoixFiltreN1.Items.Add(Valeur)
                               End If
                           End Sub)
        If WsCtl_Cache.Filtre_N1 = "" Then
            ListeChoixFiltreN1.SelectedIndex = 0
        Else
            ListeChoixFiltreN1.Text = WsCtl_Cache.Filtre_N1
        End If
    End Sub
    Private Sub ConstruireLeControle()
        If WsCtl_Cache Is Nothing Then
            WsCtl_Cache = CacheVirControle
        End If
        If WsCtl_Cache.ListeSujets.Count = 0 Then
            Exit Sub
        End If
        WsLstBoutons = New List(Of System.Web.UI.WebControls.Button)
        Dim IndiceI As Integer = 0
        For Each Element As VCaches.CacheListeMensuelle In (From ItemCache In WsCtl_Cache.ListeSujets Order By ItemCache.Libelle Select ItemCache)
            Call AjouterUneLigne(IndiceI, Element)
            IndiceI += 1
        Next
        If WsLstBoutons.Count = 0 Then
            Exit Sub
        End If
        WsLstBoutons.ForEach(Sub(Bouton)
                                 AddHandler Bouton.Click, AddressOf Me.BoutonDyna_Click
                             End Sub)
    End Sub

    Private Sub AjouterUneLigne(ByVal Index As Integer, ByVal CacheMois As VCaches.CacheListeMensuelle)
        Dim IndiceI As Integer
        Dim Rangee As System.Web.UI.WebControls.TableRow
        Dim Cellule As System.Web.UI.WebControls.TableCell
        Dim CtrlBouton As System.Web.UI.WebControls.Button
        Dim DateDebutMois As DateTime
        Dim DateFinMois As DateTime
        Dim DateCourante As DateTime
        Dim NbJourMois As Integer
        Dim ColonneSpan As Integer = 0
        Dim EtatJour As String = ""
        Dim RuptureEtat As String = ""
        Dim RuptureDate As String = ""
        Dim IndiceBtn As Integer = 0
        Dim DateDebutOK As String = ""

        Rangee = New System.Web.UI.WebControls.TableRow
        Rangee.ID = "CadreLigne" & Strings.Format(Index, "00")
        CadreLignes.Rows.Add(Rangee)

        'Cellule à gauche portant le libelle et l'identifiant du sujet
        Cellule = DirectCast(LoadControl(GetType(System.Web.UI.WebControls.TableCell), Nothing), System.Web.UI.WebControls.TableCell)
        If Index Mod 2 = 0 Then
            Cellule.BackColor = Couleur_Paire
        Else
            Cellule.BackColor = Couleur_ImPaire
        End If
        Cellule.BorderWidth = New Unit("1px")
        Cellule.BorderStyle = BorderStyle.None

        CtrlBouton = DirectCast(LoadControl(GetType(System.Web.UI.WebControls.Button), Nothing), System.Web.UI.WebControls.Button)
        CtrlBouton.ID = "Sujet" & Strings.Format(Index, "00") & "_Ide_" & Strings.Format(CacheMois.Identifiant, "000000")
        CtrlBouton.Width = New Unit("330px")
        CtrlBouton.Style.Add(HtmlTextWriterStyle.TextAlign, "left")
        CtrlBouton.Style.Add(HtmlTextWriterStyle.FontSize, "95%")
        CtrlBouton.BorderColor = Color.Transparent
        CtrlBouton.BackColor = Color.Transparent
        CtrlBouton.BorderStyle = BorderStyle.None
        If CacheMois.Libelle.Length > 50 Then
            CtrlBouton.Text = CacheMois.Libelle.Substring(0, 50) & "..."
        Else
            CtrlBouton.Text = CacheMois.Libelle
        End If
        CtrlBouton.ToolTip = CacheMois.Bulle
        CtrlBouton.Style.Add(HtmlTextWriterStyle.Cursor, "pointer")

        Cellule.Controls.Add(CtrlBouton)
        Rangee.Cells.Add(Cellule)
        WsLstBoutons.Add(CtrlBouton)

        '** Cellules Jours
        DateDebutMois = New DateTime(WsCtl_Cache.Annee, WsCtl_Cache.Mois, 1)
        DateFinMois = V_WebFonction.ViRhDates.DateTypee(V_WebFonction.ViRhDates.DateSaisieVerifiee("31/" & Strings.Format(WsCtl_Cache.Mois, "00") & "/" & WsCtl_Cache.Annee))
        NbJourMois = (DateFinMois - DateDebutMois).Days
        ColonneSpan = 0
        RuptureEtat = "NULLE"
        RuptureDate = ""
        For IndiceI = 0 To NbJourMois
            DateCourante = DateDebutMois.AddDays(IndiceI)
            EtatJour = "NULLE"
            If DateCourante.DayOfWeek = DayOfWeek.Saturday OrElse DateCourante.DayOfWeek = DayOfWeek.Sunday Then
                EtatJour = "WE"
            ElseIf V_WebFonction.ViRhDates.SiJourFerie(DateCourante.ToShortDateString) = True Then
                EtatJour = "F"
            ElseIf CacheMois.ListeJournees IsNot Nothing AndAlso CacheMois.ListeJournees.Count > 0 Then
                For Each Journee As VCaches.CacheJournees In CacheMois.ListeJournees
                    If DateTime.Compare(DateCourante, Journee.DateDebut_ToDate) >= 0 AndAlso DateTime.Compare(DateCourante, Journee.DateFin_ToDate) <= 0 Then
                        EtatJour = CaracteristiqueEditee(Journee)
                        DateDebutOK = Journee.DateDebut
                        Exit For
                    End If
                Next
            End If
            If IndiceI = 0 Then
                RuptureEtat = EtatJour
            End If
            If EtatJour = RuptureEtat Then
                ColonneSpan += 1
                Continue For
            End If
            Cellule = FaireCelluleJour(ColonneSpan)
            Select Case RuptureEtat
                Case "F"
                    Cellule.BackColor = CouleurFerie
                Case "WE"
                    Cellule.BackColor = CouleurWE
                Case "NULLE"
                    Exit Select
                Case Else  'On change d'etat
                    CtrlBouton = AjouterBoutonJour(Index, IndiceBtn, CacheMois.Identifiant, CacheMois.Libelle, RuptureEtat, RuptureDate)
                    Cellule.Controls.Add(CtrlBouton)
                    Cellule.BackColor = CouleurBouton
                    IndiceBtn += 1
            End Select
            Rangee.Cells.Add(Cellule)
            RuptureEtat = EtatJour
            RuptureDate = DateDebutOK
            ColonneSpan = 1
        Next
    End Sub

    Private Function FaireCelluleJour(ByVal CSpan As Integer) As System.Web.UI.WebControls.TableCell
        Dim CellJour As System.Web.UI.WebControls.TableCell = DirectCast(LoadControl(GetType(System.Web.UI.WebControls.TableCell), Nothing), System.Web.UI.WebControls.TableCell)
        CellJour.BorderWidth = New Unit("1px")
        CellJour.BorderColor = Color.Transparent
        CellJour.BorderStyle = BorderStyle.NotSet
        CellJour.Width = New Unit("" & (17 * CSpan) & "px")
        CellJour.ColumnSpan = CSpan
        CellJour.BackColor = Color.Transparent
        Return CellJour
    End Function

    Private Function AjouterBoutonJour(ByVal IndexLigne As Integer, ByVal IndexBouton As Integer, ByVal Ide As Integer, ByVal Libelle As String, ByVal Bulle As String, ByVal DateValeur As String) As Button
        Dim NewBouton As System.Web.UI.WebControls.Button = DirectCast(LoadControl(GetType(System.Web.UI.WebControls.Button), Nothing), System.Web.UI.WebControls.Button)
        NewBouton.BorderStyle = BorderStyle.None
        NewBouton.BorderColor = Color.Transparent
        NewBouton.ID = "CmdJour" & IndexLigne & "_" & IndexBouton & "_" & Ide & "_" & DateValeur
        NewBouton.Style.Add(HtmlTextWriterStyle.Width, "100%")
        NewBouton.ToolTip = Libelle & vbCrLf & Bulle
        NewBouton.BackColor = CouleurBouton
        NewBouton.Style.Add(HtmlTextWriterStyle.Cursor, "pointer")
        WsLstBoutons.Add(NewBouton)
        Return NewBouton
    End Function

    Private Sub ColorierEntetes()
        Dim DateDebutMois As DateTime
        Dim DateFinMois As DateTime
        Dim DateCourante As DateTime
        Dim NbJourMois As Integer
        Dim Cellule As System.Web.UI.WebControls.TableCell
        Dim EtiJour As System.Web.UI.WebControls.Label
        Dim IndiceI As Integer

        DateDebutMois = New DateTime(WsCtl_Cache.Annee, WsCtl_Cache.Mois, 1)
        DateFinMois = V_WebFonction.ViRhDates.DateTypee(V_WebFonction.ViRhDates.DateSaisieVerifiee("31/" & Strings.Format(WsCtl_Cache.Mois, "00") & "/" & WsCtl_Cache.Annee))
        NbJourMois = (DateFinMois - DateDebutMois).Days
        For IndiceI = 0 To 30
            DateCourante = DateDebutMois.AddDays(IndiceI)
            Cellule = DirectCast(V_WebFonction.VirWebControle(CadreEnteteJour, "CellJour", IndiceI), System.Web.UI.WebControls.TableCell)
            EtiJour = DirectCast(V_WebFonction.VirWebControle(CadreEnteteJour, "EtiJour", IndiceI), System.Web.UI.WebControls.Label)
            If IndiceI > NbJourMois Then
                Cellule.BorderColor = Color.Transparent
                Cellule.BackColor = Color.Transparent
                EtiJour.ForeColor = Color.Transparent
                EtiJour.ToolTip = ""
                Continue For
            End If
            If DateCourante.DayOfWeek = DayOfWeek.Saturday OrElse DateCourante.DayOfWeek = DayOfWeek.Sunday Then
                Cellule.BackColor = CouleurWE_Entete
                Cellule.BorderColor = Cellule.BackColor
                EtiJour.ForeColor = CouleurFond_Normal
                EtiJour.ToolTip = DateCourante.ToLongDateString
                Continue For
            End If
            If V_WebFonction.ViRhDates.SiJourFerie(DateCourante.ToShortDateString) = True Then
                Cellule.BackColor = CouleurFerie_Entete
                Cellule.BorderColor = Cellule.BackColor
                EtiJour.ForeColor = CouleurFond_Normal
                EtiJour.ToolTip = DateCourante.ToLongDateString & " (Férié)"
                Continue For
            End If
            Cellule.BackColor = CouleurFond_Normal
            Cellule.BorderColor = Cellule.BackColor
            EtiJour.ForeColor = Couleur_PoliceNormal
            EtiJour.ToolTip = DateCourante.ToLongDateString
        Next IndiceI
    End Sub

    Private Sub FaireListeChoix()
        Dim Element As System.Web.UI.WebControls.ListItem
        Dim IndiceI As Integer
        WsCtl_Cache = CacheVirControle
        If WsCtl_Cache Is Nothing Then
            Exit Sub
        End If
        If ListeChoixAnnee.Items.Count = 0 Then
            For IndiceI = -2 To 2
                Element = New ListItem(CStr(WsCtl_Cache.Annee + IndiceI))
                Element.Selected = (IndiceI = 0)
                ListeChoixAnnee.Items.Add(Element)
            Next
            ListeChoixAnnee.SelectedValue = CStr(WsCtl_Cache.Annee)
            ListeChoixAnnee.Text = CStr(WsCtl_Cache.Annee)
        End If
        If ListeChoixMois.Items.Count = 0 Then
            ListeChoixMois.Items.Add(New ListItem("Janvier", "1"))
            ListeChoixMois.Items.Add(New ListItem("Février", "2"))
            ListeChoixMois.Items.Add(New ListItem("Mars", "3"))
            ListeChoixMois.Items.Add(New ListItem("Avril", "4"))
            ListeChoixMois.Items.Add(New ListItem("Mai", "5"))
            ListeChoixMois.Items.Add(New ListItem("Juin", "6"))
            ListeChoixMois.Items.Add(New ListItem("Juillet", "7"))
            ListeChoixMois.Items.Add(New ListItem("Août", "8"))
            ListeChoixMois.Items.Add(New ListItem("Septembre", "9"))
            ListeChoixMois.Items.Add(New ListItem("Octobre", "10"))
            ListeChoixMois.Items.Add(New ListItem("Novembre", "11"))
            ListeChoixMois.Items.Add(New ListItem("Décembre", "12"))
            ListeChoixMois.SelectedIndex = WsCtl_Cache.Mois - 1
            ListeChoixMois.Text = ListeChoixMois.SelectedValue
        End If
    End Sub

    Private Sub BoutonDyna_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        Dim TableauData(0) As String
        Dim DateValeur As String = ""
        TableauData = Strings.Split(CType(sender, System.Web.UI.WebControls.Button).ID, "_")
        If TableauData.Length < 3 OrElse IsNumeric(TableauData(2)) = False Then
            Exit Sub
        End If
        If TableauData.Length > 3 Then
            DateValeur = TableauData(3)
        End If
        Dim Evenement As Virtualia.Systeme.Evenements.DonneeChangeEventArgs
        Evenement = New Virtualia.Systeme.Evenements.DonneeChangeEventArgs(TableauData(2), DateValeur)
        IdeSel_Change(Evenement)
    End Sub

    Private Sub ListeChoixAnnee_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ListeChoixAnnee.SelectedIndexChanged
        WsCtl_Cache = CacheVirControle
        WsCtl_Cache.Annee = Integer.Parse(ListeChoixAnnee.SelectedItem.Text)
        WsCtl_Cache.Filtre_N1 = ""
        WsCtl_Cache.Filtre_N2 = ""
        WsCtl_Cache.ListeSujets.Clear()
        Call PreparerLeCache()
        Call ReInitialiserControle()
    End Sub

    Private Sub ListeChoixMois_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ListeChoixMois.SelectedIndexChanged
        WsCtl_Cache = CacheVirControle
        WsCtl_Cache.Mois = Integer.Parse(ListeChoixMois.SelectedItem.Value)
        WsCtl_Cache.Filtre_N1 = ""
        WsCtl_Cache.Filtre_N2 = ""
        WsCtl_Cache.ListeSujets.Clear()
        Call PreparerLeCache()
        Call ReInitialiserControle()
    End Sub

    Private Sub ListeChoixFiltreN1_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ListeChoixFiltreN1.SelectedIndexChanged
        WsCtl_Cache = CacheVirControle
        WsCtl_Cache.Filtre_N1 = ListeChoixFiltreN1.SelectedItem.Text
        WsCtl_Cache.Mois = Integer.Parse(ListeChoixMois.SelectedItem.Value)
        WsCtl_Cache.ListeSujets.Clear()
        Call PreparerLeCache()
        Call ReInitialiserControle()
    End Sub

    Private ReadOnly Property CaracteristiqueEditee(ByVal ObjetJournee As VCaches.CacheJournees) As String
        Get
            Dim Chaine As String = ""
            If ObjetJournee.Intitule <> "" Then
                Chaine = ObjetJournee.Intitule & vbCrLf
            End If
            If ObjetJournee.DateDebut <> ObjetJournee.DateFin Then 'EStMulti ???
                Chaine &= "Sessions comprises entre le " & ObjetJournee.DateDebut & " et le " & ObjetJournee.DateFin
                Return Chaine
            End If
            Chaine &= "Session du " & ObjetJournee.DateDebut & " au " & ObjetJournee.DateFin
            If ObjetJournee.Complement.Trim <> "" Then  'LibLieu.Trim() <> "" Then
                Chaine &= vbCrLf & "Lieu : " & ObjetJournee.Complement
            End If
            If ObjetJournee.HeureDebut_AM <> "" Then
                If ObjetJournee.HeureFin_AM = "" Then
                    Chaine &= vbCrLf & "Horaire de " & ObjetJournee.HeureDebut_AM & " à " & ObjetJournee.HeureFin_PM
                    Return Chaine
                End If
                Chaine &= vbCrLf & "Horaire matinée de " & ObjetJournee.HeureDebut_AM & " à " & ObjetJournee.HeureFin_AM
            End If
            If ObjetJournee.HeureDebut_PM <> "" Then
                Chaine &= vbCrLf & "Horaire après-midi de " & ObjetJournee.HeureDebut_PM & " à " & ObjetJournee.HeureFin_PM
            End If
            Return Chaine
        End Get
    End Property
End Class