﻿Option Strict On
Option Explicit On
Option Compare Text
Imports VI = Virtualia.Systeme.Constantes
Public Class PER_ACOMPTE_HONDA
    Inherits Virtualia.Net.Controles.ObjetWebControle
    Private WsNumObjet As Integer = 170
    Private WsNomTable As String = "PER_DEMANDE_ACOMPTE"
    Private WsFiche As Virtualia.TablesObjet.ShemaPER.PER_DEMANDE_ACOMPTE

    Public ReadOnly Property MonCadre() As System.Web.UI.Control
        Get
            Return Me.CadreInfo
        End Get
    End Property

    Public WriteOnly Property CadreStyle() As String
        Set(ByVal value As String)
            Dim TableauData(0) As String
            Dim TableauW(0) As String
            Dim IndiceI As Integer
            TableauData = Strings.Split(value, ";")
            For IndiceI = 0 To TableauData.Count - 1
                If TableauData(IndiceI) = "" Then
                    Exit For
                End If
                TableauW = Strings.Split(TableauData(IndiceI), ":")
            Next IndiceI
        End Set
    End Property

    Public WriteOnly Property Identifiant() As Integer
        Set(ByVal value As Integer)
            Dim SiaActualiser As Boolean = False
            If Session("Identifiant") IsNot Nothing Then
                If Session("Identifiant").ToString <> CStr(value) Then
                    Session.Remove("Identifiant")
                    Session.Add("Identifiant", CStr(value))
                    SiaActualiser = True
                End If
            Else
                Session.Add("Identifiant", CStr(value))
                SiaActualiser = True
            End If
            If V_Identifiant <> value Then

                Dim ColHisto As New List(Of Integer)
                ColHisto.Add(0)
                ColHisto.Add(1)
                ColHisto.Add(2)
                ColHisto.Add(4)
                ColHisto.Add(5)
                V_CacheColHisto = ColHisto

                V_Identifiant = value
                If SiaActualiser = True Then
                    Call V_ActualiserObjet(value)
                End If
                Call ActualiserListe()
            End If
        End Set
    End Property

    Private Sub ActualiserListe()
        CadreCmdOK.Visible = False
        Dim LstLibels As New List(Of String)
        LstLibels.Add("Aucune demande d'acompte")
        LstLibels.Add("Une demande d'acompte")
        LstLibels.Add("demandes d'acompte")
        ListeGrille.V_LibelCaption = LstLibels

        Dim LstColonnes As New List(Of String)
        LstColonnes.Add("date de la demande")
        LstColonnes.Add("Mois de paie")
        LstColonnes.Add("montant")
        LstColonnes.Add("Si validé DRH")
        LstColonnes.Add("Date validation")
        LstColonnes.Add("Clef")
        ListeGrille.V_LibelColonne = LstColonnes

        If V_Identifiant > 0 Then
            Call InitialiserControles()
            If V_IndexFiche = -1 Then
                ListeGrille.V_Liste = Nothing
                Exit Sub
            End If
            ListeGrille.V_Liste = V_ListeFiches
        End If
    End Sub

    Protected Sub ListeGrille_ValeurChange(ByVal sender As Object, ByVal e As Virtualia.Systeme.Evenements.DonneeChangeEventArgs) Handles ListeGrille.ValeurChange
        If e.Valeur = "" Then
            Exit Sub
        End If
        V_Occurence = e.Valeur
    End Sub

    Private Sub LireLaFiche()
        Dim CacheDonnee As List(Of String) = V_CacheMaj
        If CacheDonnee Is Nothing Then
            Exit Sub
        End If
        Dim NumInfo As Integer
        Dim SiEnLecture As Boolean = False
        Dim Ctl As Control
        Dim VirControle As Controles_VCoupleEtiDonnee
        Dim IndiceI As Integer = 0

        If CacheDonnee(5) IsNot Nothing Then
            If CacheDonnee(5).ToString <> "" Then
                SiEnLecture = True
            End If
        End If
        Do
            Ctl = V_WebFonction.VirWebControle(Me.CadreInfo, "InfoH", IndiceI)
            If Ctl Is Nothing Then
                Exit Do
            End If
            NumInfo = CInt(Strings.Right(Ctl.ID, 2))
            VirControle = CType(Ctl, Controles_VCoupleEtiDonnee)
            If CacheDonnee(NumInfo) Is Nothing Then
                VirControle.DonText = ""
            Else
                VirControle.DonText = CacheDonnee(NumInfo).ToString
            End If
            VirControle.V_SiEnLectureSeule = SiEnLecture
            VirControle.V_SiAutoPostBack = Not (CadreCmdOK.Visible)
            Select Case NumInfo
                Case 0, 5
                    VirControle.V_SiEnLectureSeule = True
            End Select
            IndiceI += 1
        Loop

        IndiceI = 0
        Dim VirDonneeVerticale As Controles_VCoupleVerticalEtiDonnee
        Do
            Ctl = V_WebFonction.VirWebControle(Me.CadreInfo, "InfoV", IndiceI)
            If Ctl Is Nothing Then
                Exit Do
            End If
            NumInfo = CInt(Strings.Right(Ctl.ID, 2))
            VirDonneeVerticale = CType(Ctl, Controles_VCoupleVerticalEtiDonnee)
            If CacheDonnee(NumInfo) Is Nothing Then
                VirDonneeVerticale.DonText = ""
            Else
                VirDonneeVerticale.DonText = CacheDonnee(NumInfo).ToString
            End If
            VirDonneeVerticale.V_SiEnLectureSeule = SiEnLecture
            VirDonneeVerticale.V_SiAutoPostBack = Not (CadreCmdOK.Visible)
            If NumInfo = 6 Then
                VirDonneeVerticale.V_SiEnLectureSeule = True
            End If
            IndiceI += 1
        Loop

        Dim VirRadioH As Controles_VTrioHorizontalRadio
        IndiceI = 0
        Do
            Ctl = V_WebFonction.VirWebControle(Me.CadreInfo, "RadioH", IndiceI)
            If Ctl Is Nothing Then
                Exit Do
            End If
            NumInfo = CInt(Strings.Right(Ctl.ID, 2))
            VirRadioH = CType(Ctl, Controles_VTrioHorizontalRadio)
            VirRadioH.V_SiAutoPostBack = Not (CadreCmdOK.Visible)
            If CacheDonnee(NumInfo) IsNot Nothing Then
                If V_IndexFiche <> -1 And CacheDonnee(NumInfo).ToString <> "" Then
                    Select Case CacheDonnee(NumInfo).ToString
                        Case Is = VirRadioH.RadioGaucheText
                            VirRadioH.RadioGaucheCheck = True
                        Case Is = VirRadioH.RadioCentreText
                            VirRadioH.RadioCentreCheck = True
                        Case Is = VirRadioH.RadioDroiteText
                            VirRadioH.RadioDroiteCheck = True
                    End Select
                End If
            End If
            VirRadioH.V_SiEnLectureSeule = True
            IndiceI += 1
        Loop

    End Sub

    Protected Sub CommandeNew_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles CommandeNew.Click
        Call InitialiserControles()
        Call V_CommandeNewFiche()
        CadreCmdOK.Visible = True
    End Sub

    Protected Sub CommandeSupp_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles CommandeSupp.Click
        Dim CacheDonnee As List(Of String) = V_CacheMaj
        If CacheDonnee Is Nothing Then
            Exit Sub
        End If
        If CacheDonnee(5) IsNot Nothing Then
            If CacheDonnee(5).ToString <> "" Then
                Exit Sub
            End If
        End If
        Call V_CommandeSuppFiche()
    End Sub

    Public WriteOnly Property RetourDialogueSupp(ByVal Cmd As String) As String
        Set(ByVal value As String)
            V_RetourDialogueSupp(Cmd) = value
            Call ActualiserListe()
        End Set
    End Property

    Protected Sub CommandeOK_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles CommandeOK.Click
        Dim CacheDonnee As List(Of String) = V_CacheMaj
        If CacheDonnee Is Nothing Then
            Exit Sub
        End If
        If CacheDonnee(5) IsNot Nothing Then
            If CacheDonnee(5).ToString <> "" Then
                Exit Sub
            End If
        End If
        Call V_MajFiche()
        Call ActualiserListe()
        CadreCmdOK.Visible = False
        '*** Envoi d'un mail à la liste de diffusion DRH
        If CType(V_WebFonction.PointeurUtilisateur.V_PointeurSessionModule, Virtualia.Net.Session.SessionModule).SiAccesGRH = True Then
            Exit Sub
        End If
        If CType(V_WebFonction.PointeurGlobal, Virtualia.Net.Session.ObjetGlobal).VirListeDiffusionDRH Is Nothing Then
            Exit Sub
        End If
        Dim Msg As String
        Msg = CType(V_WebFonction.PointeurUtilisateur.V_PointeurSessionModule, Virtualia.Net.Session.SessionModule).DossierPER.Civilite & Strings.Space(1) & CType(V_WebFonction.PointeurUtilisateur.V_PointeurSessionModule, Virtualia.Net.Session.SessionModule).DossierPER.Nom & Strings.Space(1)
        Msg &= CType(V_WebFonction.PointeurUtilisateur.V_PointeurSessionModule, Virtualia.Net.Session.SessionModule).DossierPER.Prenom & Strings.Space(1)
        Msg &= " a saisi une demande d'acompte en date du " & CacheDonnee(0).ToString
        Call CType(V_WebFonction.PointeurGlobal, Virtualia.Net.Session.ObjetGlobal).EnvoyerEmail("", CType(V_WebFonction.PointeurUtilisateur.V_PointeurSessionModule, Virtualia.Net.Session.SessionModule).DossierPER.EMail,
                                                      CType(V_WebFonction.PointeurGlobal, Virtualia.Net.Session.ObjetGlobal).VirListeDiffusionDRH.Item(0), Msg)

    End Sub

    Protected Sub InfoVerticale_ValeurChange(ByVal sender As Object, ByVal e As Systeme.Evenements.DonneeChangeEventArgs) Handles InfoV03.ValeurChange
        Dim CacheDonnee As List(Of String) = V_CacheMaj
        Dim NumInfo As Integer = CInt(Strings.Right(CType(sender, Controles_VCoupleVerticalEtiDonnee).ID, 2))
        If CacheDonnee IsNot Nothing Then
            If CacheDonnee(NumInfo) Is Nothing Then
                CType(sender, Controles_VCoupleVerticalEtiDonnee).DonBackColor = V_WebFonction.CouleurMaj
                CadreCmdOK.Visible = True
            Else
                If CacheDonnee(NumInfo).ToString <> e.Valeur Then
                    CType(sender, Controles_VCoupleVerticalEtiDonnee).DonBackColor = V_WebFonction.CouleurMaj
                    CadreCmdOK.Visible = True
                End If
            End If
            Call V_ValeurChange(NumInfo, e.Valeur)
        End If
    End Sub

    Private Sub InfoH_ValeurChange(ByVal sender As Object, ByVal e As Systeme.Evenements.DonneeChangeEventArgs) Handles InfoH00.ValeurChange,
        InfoH01.ValeurChange, InfoH02.ValeurChange

        Dim CacheDonnee As List(Of String) = V_CacheMaj
        Dim NumInfo As Integer = CInt(Strings.Right(CType(sender, Controles_VCoupleEtiDonnee).ID, 2))
        Dim Chaine As String = e.Valeur
        If NumInfo = 1 Then
            Select Case e.Valeur.Length
                Case 4, 6
                    Chaine = Strings.Right(V_WebFonction.ViRhDates.DateSaisieVerifiee("01" & e.Valeur), 7)
                Case 5, 7
                    Chaine = Strings.Right(V_WebFonction.ViRhDates.DateSaisieVerifiee("01/" & e.Valeur), 7)
            End Select
            CType(sender, Controles_VCoupleEtiDonnee).DonText = Chaine
        End If
        If CacheDonnee IsNot Nothing Then
            If CacheDonnee(NumInfo) Is Nothing Then
                CType(sender, Controles_VCoupleEtiDonnee).DonBackColor = V_WebFonction.CouleurMaj
                CadreCmdOK.Visible = True
            Else
                If CacheDonnee(NumInfo).ToString <> Chaine Then
                    CType(sender, Controles_VCoupleEtiDonnee).DonBackColor = V_WebFonction.CouleurMaj
                    CadreCmdOK.Visible = True
                End If
            End If
            Call V_ValeurChange(NumInfo, Chaine)
            If NumInfo = 1 Then
                Call V_ValeurChange(0, V_WebFonction.ViRhDates.DateduJour) 'Date de la demande
            End If
        End If
    End Sub

    Protected Overrides Sub Page_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init
        V_Objet(TypeListe.SiListeGrid) = WsNumObjet
        V_NomTableSgbd = WsNomTable
        V_Fiche = WsFiche
        ListeGrille.Centrage_Colonne(0) = 1
        ListeGrille.Centrage_Colonne(1) = 1
        ListeGrille.Centrage_Colonne(2) = 1
        ListeGrille.Centrage_Colonne(3) = 1
        ListeGrille.Centrage_Colonne(4) = 1
        ListeGrille.Centrage_Colonne(5) = 1
    End Sub

    Protected Overrides Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        Call LireLaFiche()

        CadreInfo.BackColor = V_WebFonction.CouleurCharte(VI.PointdeVue.PVueApplicatif, "Cadre")
        Etiquette.BackColor = V_WebFonction.CouleurCharte(VI.PointdeVue.PVueApplicatif, "Titre")
        Etiquette.ForeColor = V_WebFonction.CouleurCharte(VI.PointdeVue.PVueApplicatif, "Police_Claire")
        Etiquette.BorderColor = V_WebFonction.CouleurCharte(VI.PointdeVue.PVueApplicatif, "Bordure")

    End Sub

    Private Sub InitialiserControles()
        Dim Ctl As Control
        Dim VirControle As Controles_VCoupleEtiDonnee
        Dim IndiceI As Integer = 0
        Do
            Ctl = V_WebFonction.VirWebControle(Me.CadreInfo, "InfoH", IndiceI)
            If Ctl Is Nothing Then
                Exit Do
            End If
            VirControle = CType(Ctl, Controles_VCoupleEtiDonnee)
            VirControle.DonBackColor = Drawing.Color.White
            VirControle.DonText = ""
            IndiceI += 1
        Loop

        Dim VirVertical As Controles_VCoupleVerticalEtiDonnee
        IndiceI = 0
        Do
            Ctl = V_WebFonction.VirWebControle(Me.CadreInfo, "InfoV", IndiceI)
            If Ctl Is Nothing Then
                Exit Do
            End If
            VirVertical = CType(Ctl, Controles_VCoupleVerticalEtiDonnee)
            VirVertical.DonBackColor = Drawing.Color.White
            VirVertical.DonText = ""
            IndiceI += 1
        Loop

    End Sub
End Class