﻿Option Strict Off
Option Explicit On
Option Compare Text
Imports VI = Virtualia.Systeme.Constantes
Public Class VArmoireFOR
    Inherits System.Web.UI.UserControl
    Private WebFct As Virtualia.Net.Controles.WebFonctions
    Private WsNomStateCache As String = "VForArmoire"
    Private WsCtl_Cache As Virtualia.Net.VCaches.CacheArmoire

    Public Delegate Sub Dossier_ClickEventHandler(ByVal sender As Object, ByVal e As Virtualia.Systeme.Evenements.DossierClickEventArgs)
    Public Event Dossier_Click As Dossier_ClickEventHandler

    Protected Overridable Sub VDossier_Click(ByVal e As Virtualia.Systeme.Evenements.DossierClickEventArgs)
        RaiseEvent Dossier_Click(Me, e)
    End Sub

    Public Property V_TypeArmoire As String
        Get
            WsCtl_Cache = CacheVirControle
            Return WsCtl_Cache.Type_Armoire
        End Get
        Set(value As String)
            WsCtl_Cache = CacheVirControle
            If value <> WsCtl_Cache.Type_Armoire Then
                WsCtl_Cache.Type_Armoire = value
                WsCtl_Cache.ValeurFiltre = ""
                CacheVirControle = WsCtl_Cache
                TreeListeDossier.Nodes.Clear()
            End If
        End Set
    End Property

    Private ReadOnly Property V_WebFonction As Virtualia.Net.Controles.WebFonctions
        Get
            If WebFct Is Nothing Then
                WebFct = New Virtualia.Net.Controles.WebFonctions(Me, 0)
            End If
            Return WebFct
        End Get
    End Property

    Private Property CacheVirControle As Virtualia.Net.VCaches.CacheArmoire
        Get
            If Me.ViewState(WsNomStateCache) IsNot Nothing Then
                Return CType(Me.ViewState(WsNomStateCache), Virtualia.Net.VCaches.CacheArmoire)
            End If
            Dim NewCache As Virtualia.Net.VCaches.CacheArmoire
            NewCache = New Virtualia.Net.VCaches.CacheArmoire
            Return NewCache
        End Get
        Set(value As Virtualia.Net.VCaches.CacheArmoire)
            If value Is Nothing Then
                Exit Property
            End If
            If Me.ViewState(WsNomStateCache) IsNot Nothing Then
                Me.ViewState.Remove(WsNomStateCache)
            End If
            Me.ViewState.Add(WsNomStateCache, value)
        End Set
    End Property

    Public Sub V_ListeForClear()
        WsCtl_Cache = CacheVirControle
        WsCtl_Cache.Ide_Selection = 0
        WsCtl_Cache.DataPath_Selection = ""
        WsCtl_Cache.ListeCriteres = Nothing
        WsCtl_Cache.ListeIde = Nothing
        WsCtl_Cache.ValeurFiltre = ""
        CacheVirControle = WsCtl_Cache
    End Sub

    Private Sub VArmoireFOR_Init(sender As Object, e As EventArgs) Handles Me.Init
        TreeListeDossier.ShowCheckBoxes = TreeNodeTypes.None
    End Sub

    Private Sub VArmoireFOR_PreRender(sender As Object, e As EventArgs) Handles Me.PreRender
        If TreeListeDossier.Nodes.Count > 0 Then
            Exit Sub
        End If
        If V_TypeArmoire = "ALPHA" Then
            Call FaireListeAlphabetique()
        Else
            Call FaireListeOrganisee()
        End If
    End Sub

    Private Sub FaireListeOrganisee()
        Dim LstVuesDyna As List(Of Virtualia.Net.WebAppli.SelfV4.DossierFormation) = Nothing
        Dim LstValeurs As List(Of String)
        Dim RuptureN1 As String = ""
        Dim RuptureN2 As String = ""
        Dim RuptureN3 As String = ""
        Dim RuptureN4 As String = ""
        Dim RuptureN5 As String = ""
        Dim UrlImageN1 = "~/Images/Armoire/BleuFermer16.bmp"
        Dim UrlImageN2 = "~/Images/Armoire/JauneFermer16.bmp"
        Dim UrlImageN3 = "~/Images/Armoire/VertFonceFermer16.bmp"
        Dim UrlImageN4 = "~/Images/Armoire/GrisClairFermer16.bmp"
        Dim UrlImageN5 = "~/Images/Armoire/OrangeFonceFermer16.bmp"
        Dim UrlImageRef = "~/Images/Armoire/FicheBleue.bmp"
        Dim NewNoeudN1 As TreeNode = Nothing
        Dim NewNoeudN2 As TreeNode = Nothing
        Dim NewNoeudN3 As TreeNode = Nothing
        Dim NewNoeudN4 As TreeNode = Nothing
        Dim NewNoeudN5 As TreeNode = Nothing
        Dim CptN1 As Integer = 0
        Dim CptN2 As Integer = 0
        Dim CptN3 As Integer = 0
        Dim CptN4 As Integer = 0
        Dim CptN5 As Integer = 0
        Dim SiAFaire As Boolean
        Dim Profondeur As Integer = 5
        Dim Clef As String = ""

        WsCtl_Cache = CacheVirControle
        TreeListeDossier.Nodes.Clear()
        LstFiltrePlan.Items.Clear()

        EtiStatus1.Text = ""
        Select Case V_TypeArmoire
            Case "ALPHA_A_VENIR"
                LstVuesDyna = CType(V_WebFonction.PointeurUtilisateur.V_PointeurSessionModule, Virtualia.Net.Session.SessionModule).PointeurArmoireFOR.ListeAlphabetique(WsCtl_Cache.ValeurFiltre, HSelLettre.Value)
            Case "DOMAINE_A_VENIR"
                LstVuesDyna = CType(V_WebFonction.PointeurUtilisateur.V_PointeurSessionModule, Virtualia.Net.Session.SessionModule).PointeurArmoireFOR.ListeParDomaine(WsCtl_Cache.ValeurFiltre, HSelLettre.Value)
            Case "PLAN_A_VENIR"
                LstVuesDyna = CType(V_WebFonction.PointeurUtilisateur.V_PointeurSessionModule, Virtualia.Net.Session.SessionModule).PointeurArmoireFOR.ListeParPlan(WsCtl_Cache.ValeurFiltre, HSelLettre.Value)
        End Select
        If LstVuesDyna Is Nothing OrElse LstVuesDyna.Count = 0 Then
            Exit Sub
        End If
        WsCtl_Cache = CacheVirControle
        TreeListeDossier.LevelStyles.Clear()
        TreeListeDossier.ShowCheckBoxes = System.Web.UI.WebControls.TreeNodeTypes.None

        For Each VueDyna As Virtualia.Net.WebAppli.SelfV4.DossierFormation In LstVuesDyna
            SiAFaire = True
            LstValeurs = New List(Of String)
            Select Case V_TypeArmoire
                Case "ALPHA_A_VENIR"
                    LstValeurs.Add(VueDyna.IntituleStage)
                    LstValeurs.Add(VueDyna.EtiquetteSession)
                    If VueDyna.EtiquetteSession = "" Then
                        SiAFaire = False
                    End If
                    LstValeurs.Add("")
                    LstValeurs.Add("")
                    LstValeurs.Add("")
                    UrlImageN2 = UrlImageRef
                    Profondeur = 2
                Case "DOMAINE_A_VENIR"
                    LstValeurs.Add(VueDyna.Domaine)
                    LstValeurs.Add(VueDyna.IntituleStage)
                    LstValeurs.Add(VueDyna.EtiquetteSession)
                    LstValeurs.Add("")
                    LstValeurs.Add("")
                    If VueDyna.EtiquetteSession = "" Then
                        SiAFaire = False
                    End If
                    UrlImageN3 = UrlImageRef
                    Profondeur = 3
                Case "PLAN_A_VENIR"
                    LstValeurs.Add(VueDyna.Plan)
                    LstValeurs.Add(VueDyna.Domaine)
                    LstValeurs.Add(VueDyna.Theme)
                    LstValeurs.Add(VueDyna.IntituleStage)
                    LstValeurs.Add(VueDyna.EtiquetteSession)
                    If VueDyna.EtiquetteSession = "" Then
                        SiAFaire = False
                    End If
                    UrlImageN5 = UrlImageRef
                    Profondeur = 5
            End Select
            Clef = VueDyna.Identifiant & VI.Tild & VueDyna.Date_Debut
            If SiAFaire = True Then
                Select Case LstValeurs.Item(0)
                    Case Is <> RuptureN1
                        If NewNoeudN5 IsNot Nothing Then
                            If CptN5 > 1 Then
                                NewNoeudN5.ToolTip = CptN5.ToString & " dossiers"
                            Else
                                NewNoeudN5.ToolTip = "Un dossier"
                            End If
                        End If
                        If NewNoeudN4 IsNot Nothing Then
                            If CptN4 > 1 Then
                                NewNoeudN4.ToolTip = CptN4.ToString & " dossiers"
                            Else
                                NewNoeudN4.ToolTip = "Un dossier"
                            End If
                        End If
                        If NewNoeudN3 IsNot Nothing Then
                            If CptN3 > 1 Then
                                NewNoeudN3.ToolTip = CptN3.ToString & " dossiers"
                            Else
                                NewNoeudN3.ToolTip = "Un dossier"
                            End If
                        End If
                        If NewNoeudN2 IsNot Nothing Then
                            If CptN2 > 1 Then
                                NewNoeudN2.ToolTip = CptN2.ToString & " dossiers"
                            Else
                                NewNoeudN2.ToolTip = "Un dossier"
                            End If
                        End If
                        If NewNoeudN1 IsNot Nothing Then
                            If CptN1 > 1 Then
                                NewNoeudN1.ToolTip = CptN1.ToString & " dossiers"
                            Else
                                NewNoeudN1.ToolTip = "Un dossier"
                            End If
                        End If
                        RuptureN1 = LstValeurs.Item(0)
                        RuptureN2 = ""
                        RuptureN3 = ""
                        RuptureN4 = ""
                        RuptureN5 = ""
                        NewNoeudN2 = Nothing
                        NewNoeudN3 = Nothing
                        NewNoeudN4 = Nothing
                        NewNoeudN5 = Nothing
                        CptN1 = 0
                        CptN2 = 0
                        CptN3 = 0
                        CptN4 = 0
                        CptN5 = 0

                        NewNoeudN1 = New TreeNode(RuptureN1, "N1" & VI.Tild & Clef)
                        NewNoeudN1.ImageUrl = UrlImageN1

                        NewNoeudN1.PopulateOnDemand = False
                        NewNoeudN1.ShowCheckBox = False
                        NewNoeudN1.SelectAction = TreeNodeSelectAction.SelectExpand
                        TreeListeDossier.Nodes.Add(NewNoeudN1)
                End Select

                If LstValeurs.Item(1) <> "" Then
                    Select Case LstValeurs.Item(1)
                        Case Is <> RuptureN2
                            If NewNoeudN5 IsNot Nothing Then
                                If CptN5 > 1 Then
                                    NewNoeudN5.ToolTip = CptN5.ToString & " dossiers"
                                Else
                                    NewNoeudN5.ToolTip = "Un dossier"
                                End If
                            End If
                            If NewNoeudN4 IsNot Nothing Then
                                If CptN4 > 1 Then
                                    NewNoeudN4.ToolTip = CptN4.ToString & " dossiers"
                                Else
                                    NewNoeudN4.ToolTip = "Un dossier"
                                End If
                            End If
                            If NewNoeudN3 IsNot Nothing Then
                                If CptN3 > 1 Then
                                    NewNoeudN3.ToolTip = CptN3.ToString & " dossiers"
                                Else
                                    NewNoeudN3.ToolTip = "Un dossier"
                                End If
                            End If
                            If NewNoeudN2 IsNot Nothing Then
                                If CptN2 > 1 Then
                                    NewNoeudN2.ToolTip = CptN2.ToString & " dossiers"
                                Else
                                    NewNoeudN2.ToolTip = "Un dossier"
                                End If
                            End If
                            RuptureN2 = LstValeurs.Item(1)
                            RuptureN3 = ""
                            RuptureN4 = ""
                            RuptureN5 = ""
                            NewNoeudN3 = Nothing
                            NewNoeudN4 = Nothing
                            NewNoeudN5 = Nothing
                            CptN2 = 0
                            CptN3 = 0
                            CptN4 = 0
                            CptN5 = 0

                            If NewNoeudN1 IsNot Nothing Then
                                NewNoeudN2 = New TreeNode(RuptureN2, "N2" & VI.Tild & Clef)
                                NewNoeudN2.ImageUrl = UrlImageN2
                                NewNoeudN2.PopulateOnDemand = False
                                If Profondeur = 2 Then
                                    NewNoeudN2.SelectAction = TreeNodeSelectAction.Select
                                Else
                                    NewNoeudN2.SelectAction = TreeNodeSelectAction.SelectExpand
                                End If
                                NewNoeudN1.ChildNodes.Add(NewNoeudN2)
                            End If
                    End Select
                End If

                If LstValeurs.Item(2) <> "" Then
                    Select Case LstValeurs.Item(2)
                        Case Is <> RuptureN3
                            If NewNoeudN5 IsNot Nothing Then
                                If CptN5 > 1 Then
                                    NewNoeudN5.ToolTip = CptN5.ToString & " dossiers"
                                Else
                                    NewNoeudN5.ToolTip = "Un dossier"
                                End If
                            End If
                            If NewNoeudN4 IsNot Nothing Then
                                If CptN4 > 1 Then
                                    NewNoeudN4.ToolTip = CptN4.ToString & " dossiers"
                                Else
                                    NewNoeudN4.ToolTip = "Un dossier"
                                End If
                            End If
                            If NewNoeudN3 IsNot Nothing Then
                                If CptN3 > 1 Then
                                    NewNoeudN3.ToolTip = CptN3.ToString & " dossiers"
                                Else
                                    NewNoeudN3.ToolTip = "Un dossier"
                                End If
                            End If
                            RuptureN3 = LstValeurs.Item(2)
                            RuptureN4 = ""
                            RuptureN5 = ""
                            NewNoeudN4 = Nothing
                            NewNoeudN5 = Nothing
                            CptN3 = 0
                            CptN4 = 0
                            CptN5 = 0

                            If NewNoeudN2 IsNot Nothing Then
                                NewNoeudN3 = New TreeNode(RuptureN3, "N3" & VI.Tild & Clef)
                                NewNoeudN3.ImageUrl = UrlImageN3
                                NewNoeudN3.PopulateOnDemand = False
                                If Profondeur = 3 Then
                                    NewNoeudN3.SelectAction = TreeNodeSelectAction.Select
                                Else
                                    NewNoeudN3.SelectAction = TreeNodeSelectAction.SelectExpand
                                End If
                                NewNoeudN2.ChildNodes.Add(NewNoeudN3)
                            End If
                    End Select
                End If

                If LstValeurs.Item(3) <> "" Then
                    Select Case LstValeurs.Item(3)
                        Case Is <> RuptureN4
                            If NewNoeudN5 IsNot Nothing Then
                                If CptN5 > 1 Then
                                    NewNoeudN5.ToolTip = CptN5.ToString & " dossiers"
                                Else
                                    NewNoeudN5.ToolTip = "Un dossier"
                                End If
                            End If
                            If NewNoeudN4 IsNot Nothing Then
                                If CptN4 > 1 Then
                                    NewNoeudN4.ToolTip = CptN4.ToString & " dossiers"
                                Else
                                    NewNoeudN4.ToolTip = "Un dossier"
                                End If
                            End If
                            RuptureN4 = LstValeurs.Item(3)
                            RuptureN5 = ""
                            NewNoeudN5 = Nothing
                            CptN4 = 0
                            CptN5 = 0

                            NewNoeudN4 = New TreeNode(RuptureN4, "N4" & VI.Tild & Clef)
                            NewNoeudN4.ImageUrl = UrlImageN4
                            NewNoeudN4.PopulateOnDemand = False
                            If Profondeur = 4 Then
                                NewNoeudN4.SelectAction = TreeNodeSelectAction.Select
                            Else
                                NewNoeudN4.SelectAction = TreeNodeSelectAction.SelectExpand
                            End If
                            If NewNoeudN3 Is Nothing Then
                                If NewNoeudN2 IsNot Nothing Then
                                    NewNoeudN2.ChildNodes.Add(NewNoeudN4)
                                End If
                            Else
                                NewNoeudN3.ChildNodes.Add(NewNoeudN4)
                            End If
                    End Select
                End If

                If LstValeurs.Item(4) <> "" Then
                    Select Case VueDyna.Date_Debut
                        Case Is <> RuptureN5
                            If NewNoeudN5 IsNot Nothing Then
                                If CptN5 > 1 Then
                                    NewNoeudN5.ToolTip = CptN5.ToString & " dossiers"
                                Else
                                    NewNoeudN5.ToolTip = "Un dossier"
                                End If
                            End If
                            RuptureN5 = LstValeurs.Item(4)
                            CptN5 = 0

                            NewNoeudN5 = New TreeNode(RuptureN5, "N5" & VI.Tild & Clef)
                            NewNoeudN5.ImageUrl = UrlImageN5
                            NewNoeudN5.PopulateOnDemand = False
                            NewNoeudN5.SelectAction = TreeNodeSelectAction.Select
                            If NewNoeudN4 Is Nothing Then
                                If NewNoeudN3 Is Nothing Then
                                    If NewNoeudN2 IsNot Nothing Then
                                        NewNoeudN2.ChildNodes.Add(NewNoeudN5)
                                    End If
                                Else
                                    NewNoeudN3.ChildNodes.Add(NewNoeudN5)
                                End If
                            Else
                                NewNoeudN4.ChildNodes.Add(NewNoeudN5)
                            End If
                    End Select
                End If
            End If
        Next

        If NewNoeudN5 IsNot Nothing Then
            If CptN5 > 1 Then
                NewNoeudN5.ToolTip = CptN5.ToString & " dossiers"
            Else
                NewNoeudN5.ToolTip = "Un dossier"
            End If
        End If
        If NewNoeudN4 IsNot Nothing Then
            If CptN4 > 1 Then
                NewNoeudN4.ToolTip = CptN4.ToString & " dossiers"
            Else
                NewNoeudN4.ToolTip = "Un dossier"
            End If
        End If
        If NewNoeudN3 IsNot Nothing Then
            If CptN3 > 1 Then
                NewNoeudN3.ToolTip = CptN3.ToString & " dossiers"
            Else
                NewNoeudN3.ToolTip = "Un dossier"
            End If
        End If
        If NewNoeudN2 IsNot Nothing Then
            If CptN2 > 1 Then
                NewNoeudN2.ToolTip = CptN2.ToString & " dossiers"
            Else
                NewNoeudN2.ToolTip = "Un dossier"
            End If
        End If
        If NewNoeudN1 IsNot Nothing Then
            If CptN1 > 1 Then
                NewNoeudN1.ToolTip = CptN1.ToString & " dossiers"
            Else
                NewNoeudN1.ToolTip = "Un dossier"
            End If
        End If

        EtiStatus1.Text = LstVuesDyna.Count.ToString

        If WsCtl_Cache.DataPath_Selection <> "" Then
            Try
                NewNoeudN1 = TreeListeDossier.FindNode(WsCtl_Cache.DataPath_Selection)
                TreeListeDossier.FindNode(WsCtl_Cache.DataPath_Selection).Selected = True
                TreeListeDossier.FindNode(WsCtl_Cache.DataPath_Selection).Parent.Expanded = True
            Catch ex As Exception
                NewNoeudN1 = Nothing
            End Try
        Else
            TreeListeDossier.CollapseAll()
        End If

        Dim LstFiltres As List(Of String) = (From VueDyna As Virtualia.Net.WebAppli.SelfV4.DossierFormation In LstVuesDyna Order By VueDyna.Plan Select VueDyna.Plan).Distinct.ToList
        LstFiltrePlan.Items.Add("")
        For Each Filtre In LstFiltres
            LstFiltrePlan.Items.Add(New ListItem(Filtre, Filtre))
        Next
        LstFiltrePlan.SelectedValue = WsCtl_Cache.ValeurFiltre
    End Sub

    Private Sub FaireListeAlphabetique()
        If V_WebFonction.PointeurUtilisateur Is Nothing Then
            Exit Sub
        End If
        Dim LstVuesDyna As List(Of Virtualia.Net.WebAppli.SelfV4.DossierFormation)
        Dim Rupture As String = ""
        Dim IndiceA As Integer = 0
        Dim UrlImageRef = "~/Images/Armoire/FicheBleue.bmp"
        Dim Etablissement As String = V_WebFonction.PointeurUtilisateur.Param_Etablissement

        WsCtl_Cache = CacheVirControle
        TreeListeDossier.Nodes.Clear()
        LstFiltrePlan.Items.Clear()
        EtiStatus1.Text = ""
        LstVuesDyna = CType(V_WebFonction.PointeurUtilisateur.V_PointeurSessionModule, Virtualia.Net.Session.SessionModule).PointeurArmoireFOR.ListeAlphabetique(WsCtl_Cache.ValeurFiltre, HSelLettre.Value)
        If LstVuesDyna Is Nothing OrElse LstVuesDyna.Count = 0 Then
            Exit Sub
        End If

        Call StylerlArmoire()

        For Each VueDyna As Virtualia.Net.WebAppli.SelfV4.DossierFormation In LstVuesDyna
            If VueDyna.IntituleStage <> Rupture Then
                If IndiceA < 250 Then
                    Dim NewNoeudPER As TreeNode = New TreeNode(VueDyna.IntituleStage, VueDyna.Identifiant.ToString)
                    NewNoeudPER.ImageUrl = UrlImageRef
                    NewNoeudPER.PopulateOnDemand = False
                    NewNoeudPER.SelectAction = TreeNodeSelectAction.Select
                    TreeListeDossier.Nodes.Add(NewNoeudPER)
                End If
                IndiceA += 1
                Rupture = VueDyna.IntituleStage
            End If
        Next
        EtiStatus1.Text = IndiceA.ToString
        Dim LstFiltres As List(Of String) = (From VueDyna As Virtualia.Net.WebAppli.SelfV4.DossierFormation In LstVuesDyna Order By VueDyna.Plan Select VueDyna.Plan).Distinct.ToList
        LstFiltrePlan.Items.Add("")
        For Each Filtre In LstFiltres
            LstFiltrePlan.Items.Add(New ListItem(Filtre, Filtre))
        Next
        LstFiltrePlan.SelectedValue = WsCtl_Cache.ValeurFiltre
    End Sub

    Private Sub StylerlArmoire()
        Dim Vstyle As System.Web.UI.WebControls.TreeNodeStyle
        TreeListeDossier.LevelStyles.Clear()

        TreeListeDossier.ShowCheckBoxes = System.Web.UI.WebControls.TreeNodeTypes.None

        Vstyle = New System.Web.UI.WebControls.TreeNodeStyle
        Vstyle.Font.Name = "Trebuchet MS"
        Vstyle.Font.Size = System.Web.UI.WebControls.FontUnit.Small
        Vstyle.Font.Italic = True
        Vstyle.Font.Bold = False
        Vstyle.BorderWidth = New Unit(0)
        Vstyle.BorderStyle = System.Web.UI.WebControls.BorderStyle.None
        Vstyle.BackColor = Drawing.Color.White

        TreeListeDossier.LevelStyles.Add(Vstyle)

    End Sub

    Private Sub TreeListeDossier_SelectedNodeChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles TreeListeDossier.SelectedNodeChanged
        If V_WebFonction.PointeurUtilisateur Is Nothing Then
            Exit Sub
        End If
        Dim SelIde As Integer = 0
        Dim SelDate As String = ""
        Dim Tableaudata(0) As String
        WsCtl_Cache = CacheVirControle

        If CType(sender, System.Web.UI.WebControls.TreeView).SelectedNode.SelectAction = TreeNodeSelectAction.SelectExpand Then
            WsCtl_Cache.DataPath_Selection = CType(sender, System.Web.UI.WebControls.TreeView).SelectedNode.ValuePath
            CacheVirControle = WsCtl_Cache
            Exit Sub
        End If

        If WsCtl_Cache.Type_Armoire = "ALPHA" Then
            If IsNumeric(CType(sender, System.Web.UI.WebControls.TreeView).SelectedNode.Value) Then
                SelIde = CInt(CType(sender, System.Web.UI.WebControls.TreeView).SelectedNode.Value)
            End If
        Else
            Tableaudata = Strings.Split(CType(sender, System.Web.UI.WebControls.TreeView).SelectedNode.Value, VI.Tild, -1)
            SelDate = Tableaudata(Tableaudata.Count - 1)
            If IsNumeric(Tableaudata(Tableaudata.Count - 2)) Then
                SelIde = CInt(Tableaudata(Tableaudata.Count - 2))
            End If
        End If
        WsCtl_Cache.Objet_Selection = SelDate
        WsCtl_Cache.Ide_Selection = SelIde
        WsCtl_Cache.DataPath_Selection = CType(sender, System.Web.UI.WebControls.TreeView).SelectedNode.ValuePath
        CacheVirControle = WsCtl_Cache
        If SelIde = 0 Then
            Exit Sub
        End If
        Dim Evenement As Virtualia.Systeme.Evenements.DossierClickEventArgs = Nothing
        Evenement = New Virtualia.Systeme.Evenements.DossierClickEventArgs(SelIde, SelDate)
        VDossier_Click(Evenement)
    End Sub

    Private Sub ButtonA_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs) Handles ButtonA.Click,
    ButtonB.Click, ButtonC.Click, ButtonD.Click, ButtonE.Click, ButtonF.Click, ButtonG.Click, ButtonH.Click,
    ButtonI.Click, ButtonJ.Click, ButtonK.Click, ButtonL.Click, ButtonM.Click, ButtonN.Click, ButtonO.Click,
    ButtonP.Click, ButtonQ.Click, ButtonR.Click, ButtonS.Click, ButtonT.Click, ButtonU.Click, ButtonV.Click,
    ButtonW.Click, ButtonX.Click, ButtonY.Click, ButtonZ.Click, ButtonAll.Click

        Call InitialiserBoutonsLettre()
        If CType(sender, ImageButton).ID = "ButtonAll" Then
            HSelLettre.Value = ""
            CType(sender, ImageButton).ImageUrl = "~/Images/Lettres/" & "Arobase_Sel.bmp"
        Else
            HSelLettre.Value = Strings.Right(CType(sender, ImageButton).ID, 1)
            CType(sender, ImageButton).ImageUrl = "~/Images/Lettres/" & Strings.Right(CType(sender, ImageButton).ID, 1) & "_Sel.bmp"
        End If
        DonRecherche.Text = HSelLettre.Value
        TreeListeDossier.Nodes.Clear()
    End Sub

    Private Sub InitialiserBoutonsLettre()
        Dim IndiceI As Integer = 0
        Dim Ctl As Control
        Do
            Ctl = V_WebFonction.VirWebControle(CadreLettre, "Button", IndiceI)
            If Ctl Is Nothing Then
                Exit Do
            End If
            If CType(Ctl, ImageButton).ID = "ButtonAll" Then
                CType(Ctl, ImageButton).ImageUrl = "~/Images/Lettres/" & "Arobase.bmp"
            Else
                CType(Ctl, ImageButton).ImageUrl = "~/Images/Lettres/" & Strings.Right(CType(Ctl, ImageButton).ID, 1) & ".bmp"
            End If
            IndiceI += 1
        Loop
    End Sub

    Private Sub DonRecherche_TextChanged(sender As Object, e As System.EventArgs) Handles DonRecherche.TextChanged
        HSelLettre.Value = DonRecherche.Text.ToUpper
        Call InitialiserBoutonsLettre()
        TreeListeDossier.Nodes.Clear()
    End Sub

    Private Sub LstFiltrePlan_SelectedIndexChanged(sender As Object, e As EventArgs) Handles LstFiltrePlan.SelectedIndexChanged
        WsCtl_Cache = CacheVirControle
        WsCtl_Cache.ValeurFiltre = LstFiltrePlan.SelectedItem.Text
        CacheVirControle = WsCtl_Cache
        TreeListeDossier.Nodes.Clear()
    End Sub
End Class