﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="CtrlDossierExport.ascx.vb" Inherits="Virtualia.Net.CtrlDossierExport" %>


<%@ Register Src="~/Controles/HeuresSupp/ExportPdf/CtrlExportGraphique.ascx" TagName="CTL_GRAPHIQUE" TagPrefix="HeuresSupp" %>
<%@ Register Src="~/Controles/HeuresSupp/ExportPdf/CtrlExportGraphique1.ascx" TagName="CTL_GRAPHIQUE1" TagPrefix="HeuresSupp" %>

<asp:Table ID="TbGenerale" runat="server" Width="1005px" Font-Names="Trebuchet MS" Font-Size="Small" Font-Bold="false">

    <asp:TableRow>
        <asp:TableCell>
            <div style="page-break-before:always;"></div>
        </asp:TableCell>
    </asp:TableRow>

    <asp:TableRow>
        <asp:TableCell HorizontalAlign="Center">
            <asp:Table ID="CadreExportTableau" runat="server">
            </asp:Table>
        </asp:TableCell>
    </asp:TableRow>

    <asp:TableRow>
        <asp:TableCell HorizontalAlign="Center">
            <HeuresSupp:CTL_GRAPHIQUE ID="CtrlGraph" runat="server" />
        </asp:TableCell>
    </asp:TableRow>

    <asp:TableRow>
        <asp:TableCell>
            <div style="page-break-before:always;"></div>
        </asp:TableCell>
    </asp:TableRow>

    <asp:TableRow>
        <asp:TableCell HorizontalAlign="Center">
            <HeuresSupp:CTL_GRAPHIQUE1 ID="CtrlGraph1" runat="server" />
        </asp:TableCell>
    </asp:TableRow>

    <asp:TableRow>
        <asp:TableCell>
            <div style="page-break-before:always;"></div>
        </asp:TableCell>
    </asp:TableRow>


</asp:Table>


