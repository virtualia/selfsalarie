﻿Option Strict On
Option Explicit On
Option Compare Text
Namespace PFR
    Namespace SelfV4
        Public Class LigneDotation
            Private WsRegime As String
            Private WsCategorie As String
            Private WsMontantBase100 As Double
            Private WsMontantModulation As Double
            Private WsMontantComplement As Double

            Private WsSiegeOuRegion As String
            Private WsNiveau1 As String
            Private WsNiveau2 As String
            Private WsEnveloppeModulation As Double
            Private WsCoefficientModulation As Double

            Public ReadOnly Property Regime_Categorie As String
                Get
                    Select Case WsRegime
                        Case "PFR"
                            Return "PFR " & WsCategorie
                        Case "PS"
                            Return "PS"
                        Case "F_R"
                            Return "PFR SU " & WsCategorie
                        Case Else
                            Return WsRegime & Strings.Space(1) & WsCategorie
                    End Select
                End Get
            End Property

            Public ReadOnly Property Rupture_Modulation As String
                Get
                    If SiegeOuRegion = "Région" Then
                        Return SiegeOuRegion & Direction & Service
                    Else
                        Return SiegeOuRegion & Direction
                    End If
                End Get
            End Property

            Public ReadOnly Property Direction_Modulation As String
                Get
                    If SiegeOuRegion = "Région" Then
                        Return Service
                    Else
                        Return Direction
                    End If
                End Get
            End Property

            Public Property Regime As String
                Get
                    Return WsRegime
                End Get
                Set(value As String)
                    WsRegime = value
                End Set
            End Property

            Public Property Categorie As String
                Get
                    Return WsCategorie
                End Get
                Set(value As String)
                    WsCategorie = value
                End Set
            End Property

            Public Property Montant_Base100 As Double
                Get
                    Return Math.Round(WsMontantBase100, 2)
                End Get
                Set(value As Double)
                    WsMontantBase100 = value
                End Set
            End Property

            Public Property Montant_Modulation As Double
                Get
                    Return Math.Round(WsMontantModulation, 2)
                End Get
                Set(value As Double)
                    WsMontantModulation = value
                End Set
            End Property

            Public Property Montant_Complement_Modulation As Double
                Get
                    Return Math.Round(WsMontantComplement, 2)
                End Get
                Set(value As Double)
                    WsMontantComplement = value
                End Set
            End Property

            Public Property Enveloppe_Modulation As Double
                Get
                    Return Math.Round(WsEnveloppeModulation, 2)
                End Get
                Set(value As Double)
                    WsEnveloppeModulation = value
                End Set
            End Property

            Public Property Coefficient_Modulation As Double
                Get
                    Return Math.Round(WsCoefficientModulation, 2)
                End Get
                Set(value As Double)
                    WsCoefficientModulation = value
                End Set
            End Property

            Public Property SiegeOuRegion As String
                Get
                    Return WsSiegeOuRegion
                End Get
                Set(value As String)
                    WsSiegeOuRegion = value
                End Set
            End Property

            Public Property Direction As String
                Get
                    Return WsNiveau1
                End Get
                Set(value As String)
                    WsNiveau1 = value
                End Set
            End Property

            Public Property Service As String
                Get
                    If WsNiveau2 Is Nothing OrElse WsNiveau2 = "" Then
                        Return WsNiveau1
                    End If
                    Return WsNiveau2
                End Get
                Set(value As String)
                    WsNiveau2 = value
                End Set
            End Property
        End Class
    End Namespace
End Namespace