﻿Option Strict On
Option Explicit On
Option Compare Text
Imports Virtualia.Systeme.Evenements
Imports VI = Virtualia.Systeme.Constantes
Public Class FrmPFR
    Inherits System.Web.UI.Page
    Private WebFct As Virtualia.Net.Controles.WebFonctions
    Private WsNomStateCache As String = "VPFR"
    Private WsCtl_Cache As Virtualia.Net.VCaches.CacheFenetreFrm
    Private WsLstVues As List(Of Virtualia.TablesObjet.ShemaVUE.VUE_ATTRIBUTAIRE_PFR)

    Private Property CacheVirControle As Virtualia.Net.VCaches.CacheFenetreFrm
        Get
            If Me.ViewState(WsNomStateCache) IsNot Nothing Then
                Return CType(Me.ViewState(WsNomStateCache), Virtualia.Net.VCaches.CacheFenetreFrm)
            End If
            Dim NewCache As Virtualia.Net.VCaches.CacheFenetreFrm
            NewCache = New Virtualia.Net.VCaches.CacheFenetreFrm
            Return NewCache
        End Get
        Set(value As Virtualia.Net.VCaches.CacheFenetreFrm)
            If value Is Nothing Then
                Exit Property
            End If
            If Me.ViewState(WsNomStateCache) IsNot Nothing Then
                Me.ViewState.Remove(WsNomStateCache)
            End If
            Me.ViewState.Add(WsNomStateCache, value)
        End Set
    End Property

    Private Sub FrmPFR_Init(sender As Object, e As EventArgs) Handles Me.Init
        Dim Msg As String = ""
        Dim NumSession As String = Server.HtmlDecode(Request.QueryString("IDVirtualia"))
        If NumSession <> Session.SessionID Then
            Msg = "Erreur lors de l'identification."
            Response.Redirect("~/Fenetres/Commun/ErreurApplication.aspx?Msg=" & Msg)
            Exit Sub
        End If
        WebFct = New Virtualia.Net.Controles.WebFonctions(Me, 1)
        If WebFct.PointeurUtilisateur Is Nothing Then
            Msg = "Erreur lors de l'identification."
            Response.Redirect("~/Fenetres/Commun/ErreurApplication.aspx?Msg=" & Msg)
            Exit Sub
        End If
        If WebFct.VerifierCookie(Me, Session.SessionID) = False Then
            Msg = "Erreur lors de l'identification du dossier accédé. Session invalide."
            Response.Redirect("~/Fenetres/Commun/ErreurApplication.aspx?Msg=" & Msg)
            Exit Sub
        End If
        If CType(WebFct.PointeurUtilisateur.V_PointeurSessionModule, Virtualia.Net.Session.SessionModule).SiAccesGRH = True Then
            LienSynthese.NavigateUrl = "~/Fenetres/FPEtat/FrmPFRSynthese.aspx" & "?IDVirtualia=" & NumSession & "&UrlRetour=~/Fenetres/FPEtat/FrmPFR.aspx"
            LienSynthese.Visible = True
            LienNotification.NavigateUrl = "~/Fenetres/FPEtat/FrmPFRNotification.aspx" & "?IDVirtualia=" & Session.SessionID & "&UrlRetour=~/Fenetres/FPEtat/FrmPFR.aspx"
            LienNotification.Visible = True
            LienAdmin.NavigateUrl = "~/Fenetres/Commun/FrmTachesAdmin.aspx" & "?IDVirtualia=" & NumSession & "&UrlRetour=~/Fenetres/FPEtat/FrmPFR.aspx"
            If Now.Month > 4 Then
                LienAdmin.Visible = True
            End If
        End If
    End Sub

    Private Sub FaireListeAnnee()
        Dim Chaine As System.Text.StringBuilder
        Dim Libel As String
        Dim Annee As Integer = Year(Now)
        WsCtl_Cache = CacheVirControle

        Libel = "Aucun" & VI.Tild & "Une possibilité" & VI.Tild & "possibilités"
        Chaine = New System.Text.StringBuilder
        Do
            Chaine.Append(Annee.ToString & VI.Tild)
            Annee -= 1
            If Annee < 2014 Then
                Exit Do
            End If
        Loop
        ComboSelAnnee.V_Liste(Libel) = Chaine.ToString
        If WsCtl_Cache.Annee_Selection = 0 Then
            WsCtl_Cache.Annee_Selection = Year(Now)
        End If
        CacheVirControle = WsCtl_Cache
        ComboSelAnnee.LstText = WsCtl_Cache.Annee_Selection.ToString
        If WsCtl_Cache.Annee_Selection = Year(Now) Then
            CadreAffichage.BackColor = Drawing.Color.Transparent
            CadreAffichage.BackImageUrl = "~/Images/Fonds/Fond_Saisie.jpg"
        Else
            CadreAffichage.BackColor = Drawing.Color.Firebrick
            CadreAffichage.BackImageUrl = ""
        End If
    End Sub

    Private Sub FaireListesSelection()
        Dim Chaine As System.Text.StringBuilder
        Dim Libel As String
        WsCtl_Cache = CacheVirControle

        '*** Tri
        Libel = "Aucun" & VI.Tild & "Un tri" & VI.Tild & "possibilités"
        Chaine = New System.Text.StringBuilder
        Chaine.Append("Alphabétique" & VI.Tild)
        Chaine.Append("Par affectation" & VI.Tild)
        Chaine.Append("Par grade")
        ComboSelTri.V_Liste(Libel) = Chaine.ToString
        If WsCtl_Cache.Tri_Selection = "" Then
            WsCtl_Cache.Tri_Selection = "Alphabétique"
        End If
        ComboSelTri.LstText = WsCtl_Cache.Tri_Selection

        '*** Critère de filtre
        Libel = "Aucun" & VI.Tild & "Un filtre" & VI.Tild & "possibilités"
        Chaine = New System.Text.StringBuilder
        Chaine.Append(Strings.Space(1) & VI.Tild)
        Chaine.Append("Catégorie" & VI.Tild)
        Chaine.Append("Corps" & VI.Tild)
        Chaine.Append("Grade" & VI.Tild)
        Chaine.Append("Niveau 1" & VI.Tild)
        Chaine.Append("Niveau 2" & VI.Tild)
        Chaine.Append("Niveau 3" & VI.Tild)
        Chaine.Append("Niveau 4" & VI.Tild)
        Chaine.Append("Niveau 5")
        ComboSelCritere.V_Liste(Libel) = Chaine.ToString
        If WsCtl_Cache.Critere_Selection = "" Then
            WsCtl_Cache.Critere_Selection = Strings.Space(1)
        End If
        ComboSelCritere.LstText = WsCtl_Cache.Critere_Selection

        '*** Critère de valeur du filtre
        Libel = "Aucun" & VI.Tild & "Une valeur" & VI.Tild & "valeurs"
        If WsCtl_Cache.Critere_Selection = "" OrElse WsCtl_Cache.Critere_Selection = "Nom" Then
            ComboSelValeur.V_Liste(Libel) = ""
            Exit Sub
        End If

        Dim LstValeurs As List(Of String) = Nothing
        Dim IndiceI As Integer
        If WebFct Is Nothing Then
            WebFct = New Virtualia.Net.Controles.WebFonctions(Me, 1)
        End If
        If WsLstVues Is Nothing Then
            WsLstVues = CType(WebFct.PointeurUtilisateur.V_PointeurSessionModule, Virtualia.Net.Session.SessionModule).ListeVues_PFR(WsCtl_Cache.Annee_Selection, WsCtl_Cache.Regime_Selection, WsCtl_Cache.Tri_Selection)
        End If
        If WsLstVues Is Nothing Then
            WsCtl_Cache.Valeur_Selection = ""
            ComboSelValeur.V_Liste(Libel) = ""
            Exit Sub
        End If
        Select Case WsCtl_Cache.Critere_Selection
            Case "Catégorie"
                LstValeurs = (From instance In WsLstVues Order By instance.Categorie Select instance.Categorie).Distinct.ToList
            Case "Corps"
                LstValeurs = (From instance In WsLstVues Order By instance.Corps Select instance.Corps).Distinct.ToList
            Case "Grade"
                LstValeurs = (From instance In WsLstVues Order By instance.Grade Select instance.Grade).Distinct.ToList
            Case "Niveau 1"
                LstValeurs = (From instance In WsLstVues Where instance.Structure_N1.Trim <> "" Order By instance.Structure_N1 Select instance.Structure_N1).Distinct.ToList
            Case "Niveau 2"
                LstValeurs = (From instance In WsLstVues Where instance.Structure_N2.Trim <> "" Order By instance.Structure_N2 Select instance.Structure_N2).Distinct.ToList
            Case "Niveau 3"
                LstValeurs = (From instance In WsLstVues Where instance.Structure_N3.Trim <> "" Order By instance.Structure_N3 Select instance.Structure_N3).Distinct.ToList
            Case "Niveau 4"
                LstValeurs = (From instance In WsLstVues Where instance.Structure_N4.Trim <> "" Order By instance.Structure_N4 Select instance.Structure_N4).Distinct.ToList
            Case "Niveau 5"
                LstValeurs = (From instance In WsLstVues Where instance.Structure_N5.Trim <> "" Order By instance.Structure_N5 Select instance.Structure_N5).Distinct.ToList
        End Select
        If LstValeurs Is Nothing Then
            WsCtl_Cache.Valeur_Selection = ""
            ComboSelValeur.V_Liste(Libel) = ""
            CacheVirControle = WsCtl_Cache
            Exit Sub
        End If
        Chaine = New System.Text.StringBuilder
        Chaine.Append(Strings.Space(1) & VI.Tild)
        For IndiceI = 0 To LstValeurs.Count - 1
            Chaine.Append(LstValeurs.Item(IndiceI) & VI.Tild)
        Next IndiceI
        ComboSelValeur.V_Liste(Libel) = Chaine.ToString
        If WsCtl_Cache.Valeur_Selection = "" Then
            WsCtl_Cache.Valeur_Selection = Strings.Space(1)
        End If
        CacheVirControle = WsCtl_Cache
        ComboSelValeur.LstText = WsCtl_Cache.Valeur_Selection
    End Sub

    Private Sub FrmPFR_PreRender(sender As Object, e As EventArgs) Handles Me.PreRender
        If CType(WebFct.PointeurUtilisateur.V_PointeurSessionModule, Virtualia.Net.Session.SessionModule).SiAccesGRH = True Then
            If MenuChoix.Items.Count < 6 Then
                Dim NewItem As System.Web.UI.WebControls.MenuItem = New System.Web.UI.WebControls.MenuItem
                NewItem.Text = "Informations Paye"
                NewItem.Value = "Paye"
                NewItem.ToolTip = "Informations de paye"
                MenuChoix.Items.Add(NewItem)
            End If
        End If
        WsCtl_Cache = CacheVirControle
        CadreFiger.Visible = CType(WebFct.PointeurUtilisateur.V_PointeurSessionModule, Virtualia.Net.Session.SessionModule).SiBoutonFigeVisible(WsCtl_Cache.Annee_Selection)
        WsLstVues = Nothing
        If CType(WebFct.PointeurUtilisateur.V_PointeurSessionModule, Virtualia.Net.Session.SessionModule).VueActiveAlPha = "VueMessage" Then
            Exit Sub
        End If
        If CType(WebFct.PointeurUtilisateur.V_PointeurSessionModule, Virtualia.Net.Session.SessionModule).VueActiveAlPha = "VueAttente" Then
            Exit Sub
        End If
        Call FaireListeAnnee()
        If CType(WebFct.PointeurUtilisateur.V_PointeurSessionModule, Virtualia.Net.Session.SessionModule).VueActiveAlPha = "VuePartielle" Then
            CadreSelection.Visible = False
            CadreFiltre.Visible = False
            Call PersonnaliserSaisie(WsCtl_Cache.Ide_Dossier, WsCtl_Cache.Date_Valeur, WsCtl_Cache.Index_VueActive)
            MultiVues.SetActiveView(VuePartielle)
        Else
            Call FaireListesSelection()
            Call AfficherFenetrePER(CType(WebFct.PointeurUtilisateur.V_PointeurSessionModule, Virtualia.Net.Session.SessionModule).VueActiveAlPha)
        End If
    End Sub

    Private Sub MenuChoix_MenuItemClick(sender As Object, e As System.Web.UI.WebControls.MenuEventArgs) Handles MenuChoix.MenuItemClick
        If MultiVues.ActiveViewIndex <> 6 Then
            CType(WebFct.PointeurUtilisateur.V_PointeurSessionModule, Virtualia.Net.Session.SessionModule).VueActiveAlPha = e.Item.Value
        End If
        Dim IndexVue As Integer
        Select Case e.Item.Value
            Case "Base100"
                IndexVue = 0
            Case "Modulation"
                IndexVue = 1
            Case "Prorata"
                IndexVue = 2
            Case "Affectation"
                IndexVue = 3
            Case "Enveloppe"
                IndexVue = 4
            Case "Paye"
                IndexVue = 5
        End Select
        If MultiVues.ActiveViewIndex <> 6 Then
            WebFct.PointeurUtilisateur.VueActive = IndexVue
        End If
        WsCtl_Cache = CacheVirControle
        Call PersonnaliserSaisie(WsCtl_Cache.Ide_Dossier, WsCtl_Cache.Date_Valeur, IndexVue)
    End Sub

    Private Sub AfficherFenetrePER(ByVal Parametre As String)
        Dim LstVues As List(Of Virtualia.TablesObjet.ShemaVUE.VUE_ATTRIBUTAIRE_PFR)
        Dim LstValeurs As List(Of String)
        Dim IndiceI As Integer
        Dim IndiceK As Integer
        Dim LibEntete As List(Of String)
        Dim LibColonnes As List(Of String)
        Dim LstRes As List(Of String)
        Dim Chaine As String
        Dim Montant As Double
        Dim VirControle As Virtualia.Net.Controles_VListeGrid = Nothing
        '*** Enveloppe
        Dim LigneEnveloppe As Virtualia.Net.PFR.SelfV4.LigneDotation
        Dim LstEnveloppes As List(Of Virtualia.Net.PFR.SelfV4.LigneDotation) = Nothing

        If WebFct Is Nothing Then
            WebFct = New Virtualia.Net.Controles.WebFonctions(Me, 1)
        End If
        WsCtl_Cache = CacheVirControle
        If WsCtl_Cache.Regime_Selection Is Nothing Then
            WsCtl_Cache.Regime_Selection = "PFR"
        End If
        If Parametre = "" Then
            Parametre = "Base100"
        End If

        CadreSelection.Visible = True
        CadreFiltre.Visible = True
        Select Case Parametre
            Case "Base100"
                VirControle = ListeBase100
            Case "Modulation"
                VirControle = ListeModulation
            Case "Prorata"
                VirControle = ListeProrata
            Case "Affectation"
                VirControle = ListeAffectation
            Case "Enveloppe"
                VirControle = ListeEnveloppe
                LstEnveloppes = New List(Of Virtualia.Net.PFR.SelfV4.LigneDotation)
            Case "Paye"
                VirControle = ListePaye
        End Select
        LibEntete = New List(Of String)
        LibEntete.Add("Aucune ligne")
        LibEntete.Add("Une ligne")
        LibEntete.Add("lignes")
        VirControle.V_LibelCaption = LibEntete

        LstVues = ListeDesVues
        If LstVues Is Nothing OrElse LstVues.Count = 0 Then
            VirControle.V_Liste = Nothing
            CType(WebFct.PointeurUtilisateur.V_PointeurSessionModule, Virtualia.Net.Session.SessionModule).ExeListePFR = Nothing
            Exit Sub
        End If
        EtiTotal.Text = "Nombre total de dossiers : " & LstVues.Count
        If CType(WebFct.PointeurUtilisateur.V_PointeurSessionModule, Virtualia.Net.Session.SessionModule).SiAccesGRH = True Then
            If LstVues.Count < 200 Then
                CType(WebFct.PointeurUtilisateur.V_PointeurSessionModule, Virtualia.Net.Session.SessionModule).ExeListePFR = LstVues
                If WsCtl_Cache.Regime_Selection = "PS" Then
                    LienNotification.NavigateUrl = "~/Fenetres/FPEtat/FrmPSNotification.aspx" & "?IDVirtualia=" & Session.SessionID & "&UrlRetour=~/Fenetres/FPEtat/FrmPFR.aspx"
                Else
                    LienNotification.NavigateUrl = "~/Fenetres/FPEtat/FrmPFRNotification.aspx" & "?IDVirtualia=" & Session.SessionID & "&UrlRetour=~/Fenetres/FPEtat/FrmPFR.aspx"
                End If
            Else
                CType(WebFct.PointeurUtilisateur.V_PointeurSessionModule, Virtualia.Net.Session.SessionModule).ExeListePFR = Nothing
            End If
        End If

        LibColonnes = New List(Of String)
        LstRes = New List(Of String)

        LstVues.Item(0).FaireDicoVirtuel(Parametre)
        LstValeurs = New List(Of String)
        For IndiceI = 0 To LstVues.Item(0).V_ListeDicoVirtuel.Count - 1
            LibColonnes = Strings.Split(LstVues.Item(0).V_ListeDicoVirtuel.Item(IndiceI).Etiquette, VI.Tild, -1).ToList
            Select Case LstVues.Item(0).V_ListeDicoVirtuel.Item(IndiceI).NatureDonnee
                Case "Date"
                    VirControle.Centrage_Colonne(IndiceI) = 1
                Case "Numerique"
                    If LstVues.Item(0).V_ListeDicoVirtuel.Item(IndiceI).FormatDonnee = "Decimal2" Then
                        VirControle.Centrage_Colonne(IndiceI) = 2
                    Else
                        VirControle.Centrage_Colonne(IndiceI) = 1
                    End If
                Case Else
                    If IndiceI < 5 Then
                        VirControle.Centrage_Colonne(IndiceI) = 0
                    ElseIf LstVues.Item(0).V_ListeDicoVirtuel.Item(IndiceI).Etiquette = "Nom" Or
                        LstVues.Item(0).V_ListeDicoVirtuel.Item(IndiceI).Etiquette = "Prénom" Then
                        VirControle.Centrage_Colonne(IndiceI) = 2
                    Else
                        VirControle.Centrage_Colonne(IndiceI) = 0
                    End If
            End Select
            LstValeurs.Add(LstVues.Item(0).V_ListeDicoVirtuel.Item(IndiceI).ToolTipAide)
        Next IndiceI

        LibColonnes.Add("Clef")
        VirControle.V_ToolTip = LstValeurs

        For IndiceI = 0 To LstVues.Count - 1
            LstValeurs = LstVues.Item(IndiceI).V_GrilleVirtuelle(Parametre)
            Chaine = ""
            For IndiceK = 0 To LstValeurs.Count - 2
                If LstVues.Item(0).V_ListeDicoVirtuel.Item(IndiceK).FormatDonnee = "Decimal2" Then
                    Montant = WebFct.ViRhFonction.ConversionDouble(LstValeurs(IndiceK))
                    Select Case Montant
                        Case 0
                            Chaine &= VI.Tild
                        Case Is > 0
                            Chaine &= Strings.Format(Montant, "### ##0.00") & VI.Tild
                        Case Is < 0
                            Montant = Montant * -1
                            Chaine &= "-" & Strings.Format(Montant, "### ##0.00") & VI.Tild
                    End Select
                Else
                    Chaine = LstValeurs(IndiceK) & VI.Tild
                End If
            Next IndiceK
            Chaine &= LstValeurs(LstValeurs.Count - 1)
            LstRes.Add(Chaine)
            Select Case Parametre
                Case "Enveloppe"
                    LigneEnveloppe = New Virtualia.Net.PFR.SelfV4.LigneDotation
                    LigneEnveloppe.Regime = WsCtl_Cache.Regime_Selection
                    LigneEnveloppe.Categorie = LstValeurs(3)
                    LigneEnveloppe.Montant_Modulation = WebFct.ViRhFonction.ConversionDouble(LstValeurs(4))
                    LigneEnveloppe.Enveloppe_Modulation = WebFct.ViRhFonction.ConversionDouble(LstValeurs(5))
                    LstEnveloppes.Add(LigneEnveloppe)
            End Select
        Next IndiceI
        If Parametre = "Enveloppe" Then
            If LstEnveloppes.Count > 0 Then
                LstRes.Add(StrDup(6, VI.Tild))
                Select Case WsCtl_Cache.Regime_Selection
                    Case "PFR"
                        LstRes.Add(LigneTotale(LstEnveloppes, "PFR A"))
                        LstRes.Add(LigneTotale(LstEnveloppes, "PFR B"))
                    Case "PS"
                        LstRes.Add(LigneTotale(LstEnveloppes, "PS"))
                    Case Else
                        LstRes.Add(LigneTotale(LstEnveloppes, "PFR SU A"))
                        LstRes.Add(LigneTotale(LstEnveloppes, "PFR SU B"))
                End Select
            End If
        End If
        VirControle.V_LibelColonne = LibColonnes
        VirControle.V_Liste = LstRes
        VirControle.SiColonneSelect = True

        If WsCtl_Cache.Index_Selection >= 0 Then
            VirControle.ItemSelectionne = WsCtl_Cache.Index_Selection
        End If
        Select Case Parametre
            Case "Base100"
                MultiVues.SetActiveView(VueBase100)
            Case "Modulation"
                MultiVues.SetActiveView(VueModulation)
            Case "Prorata"
                MultiVues.SetActiveView(VueProrata)
            Case "Affectation"
                MultiVues.SetActiveView(VueAffectation)
            Case "Enveloppe"
                MultiVues.SetActiveView(VueEnveloppe)
            Case "Paye"
                MultiVues.SetActiveView(VuePaye)
        End Select
    End Sub

    Private Sub PersonnaliserSaisie(ByVal Ide As Integer, ByVal DateValeur As String, ByVal IVue As Integer)
        If WebFct Is Nothing Then
            WebFct = New Virtualia.Net.Controles.WebFonctions(Me, 1)
        End If
        Dim UtiSession As Virtualia.Net.Session.SessionModule = CType(WebFct.PointeurUtilisateur.V_PointeurSessionModule, Virtualia.Net.Session.SessionModule)
        If UtiSession Is Nothing Then
            Exit Sub
        End If
        Dim Parametre As String = ""
        Dim Dictionnaire As List(Of Virtualia.Systeme.MetaModele.Outils.DictionnaireVirtuel) = Nothing
        Dim IndiceI As Integer
        Dim FicheVue As Virtualia.TablesObjet.ShemaVUE.VUE_ATTRIBUTAIRE_PFR = Nothing
        Dim LstValeurs As List(Of String)

        WsCtl_Cache = CacheVirControle
        If WsCtl_Cache.Ide_Dossier = 0 Then
            Exit Sub
        End If
        If WsLstVues Is Nothing Then
            WsLstVues = UtiSession.ListeVues_PFR(WsCtl_Cache.Annee_Selection, WsCtl_Cache.Regime_Selection, WsCtl_Cache.Tri_Selection)
        End If
        If WsLstVues Is Nothing Then
            Exit Sub
        End If
        Try
            FicheVue = (From instance In WsLstVues Select instance Where instance.Ide_Dossier = WsCtl_Cache.Ide_Dossier And
                instance.Date_de_Valeur = WsCtl_Cache.Date_Valeur).First
        Catch ex As Exception
            FicheVue = Nothing
        End Try
        If FicheVue Is Nothing Then
            Exit Sub
        End If
        If WsCtl_Cache.Index_VueActive <> IVue Then
            WsCtl_Cache.Index_VueActive = IVue
            CacheVirControle = WsCtl_Cache
        End If
        Select Case IVue
            Case 0
                Parametre = "Base100"
            Case 1
                Parametre = "Modulation"
                SaisieLigne.V_Titre = ""
            Case 2
                Parametre = "Prorata"
                SaisieLigne.V_Titre = ""
            Case 3
                Parametre = "Affectation"
                SaisieLigne.V_Titre = ""
            Case 4
                Parametre = "Enveloppe"
                SaisieLigne.V_Titre = ""
            Case 5
                Parametre = "Paye"
                SaisieLigne.V_Titre = ""
            Case Else
                Exit Sub
        End Select
        FicheVue.FaireDicoVirtuel(Parametre)
        Dictionnaire = FicheVue.V_ListeDicoVirtuel
        LstValeurs = FicheVue.V_GrilleVirtuelle(Parametre)

        SaisieLigne.V_Titre = Parametre
        SaisieLigne.V_Identifiant = Ide
        SaisieLigne.V_Occurence = DateValeur
        SaisieLigne.V_Parametre = Parametre
        If WsCtl_Cache.Regime_Selection IsNot Nothing Then
            SaisieLigne.V_Regime = WsCtl_Cache.Regime_Selection
        Else
            WsCtl_Cache.Regime_Selection = "PFR"
            SaisieLigne.V_Regime = "PFR"
        End If
        For IndiceI = 0 To SaisieLigne.NombreZones - 1
            If IndiceI < Dictionnaire.Count Then
                SaisieLigne.Donnee(IndiceI) = LstValeurs(IndiceI)
                SaisieLigne.SiZoneVisible(IndiceI) = Dictionnaire.Item(IndiceI).SiVisible
                SaisieLigne.Etiquette(IndiceI) = Dictionnaire.Item(IndiceI).Etiquette
                SaisieLigne.ToolTipAide(IndiceI) = Dictionnaire.Item(IndiceI).ToolTipAide
                SaisieLigne.NatureDonnee(IndiceI) = Dictionnaire.Item(IndiceI).NatureDonnee
                SaisieLigne.FormatDonnee(IndiceI) = Dictionnaire.Item(IndiceI).FormatDonnee
                If Strings.Left(Dictionnaire.Item(IndiceI).FormatDonnee, 5) = "Decim" Then
                    SaisieLigne.LongueurDonnee(IndiceI) = Dictionnaire.Item(IndiceI).Longueur + 1
                Else
                    If Dictionnaire.Item(IndiceI).NatureDonnee = "Date" Then
                        SaisieLigne.LongueurDonnee(IndiceI) = 10
                    Else
                        SaisieLigne.LongueurDonnee(IndiceI) = Dictionnaire.Item(IndiceI).Longueur
                    End If
                End If
                SaisieLigne.SiZoneNonSaisissable(IndiceI) = True
                If Dictionnaire.Item(IndiceI).SiNonSaisissable = False Then
                    If UtiSession.SiAccesGRH = True Then
                        SaisieLigne.SiZoneNonSaisissable(IndiceI) = False
                    ElseIf UtiSession.SiAccesManager = True And IVue = 1 Then
                        Select Case WsCtl_Cache.Regime_Selection
                            Case "PS"
                                If IndiceI = 3 Then
                                    SaisieLigne.SiZoneNonSaisissable(IndiceI) = CType(WebFct.PointeurUtilisateur.V_PointeurSessionModule, Virtualia.Net.Session.SessionModule).SiFige(WsCtl_Cache.Annee_Selection)
                                End If
                            Case Else
                                If IndiceI = 4 Then
                                    SaisieLigne.SiZoneNonSaisissable(IndiceI) = CType(WebFct.PointeurUtilisateur.V_PointeurSessionModule, Virtualia.Net.Session.SessionModule).SiFige(WsCtl_Cache.Annee_Selection)
                                End If
                        End Select
                    End If
                End If
            Else
                SaisieLigne.SiZoneVisible(IndiceI) = False
            End If
        Next IndiceI

    End Sub

    Private ReadOnly Property ListeDesVues As List(Of Virtualia.TablesObjet.ShemaVUE.VUE_ATTRIBUTAIRE_PFR)
        Get
            Dim LstVues_Regime As List(Of Virtualia.TablesObjet.ShemaVUE.VUE_ATTRIBUTAIRE_PFR)
            Dim LstVues_Filtre As List(Of Virtualia.TablesObjet.ShemaVUE.VUE_ATTRIBUTAIRE_PFR) = Nothing

            WsCtl_Cache = CacheVirControle

            LstVues_Regime = CType(WebFct.PointeurUtilisateur.V_PointeurSessionModule, Virtualia.Net.Session.SessionModule).ListeVues_PFR(WsCtl_Cache.Annee_Selection, WsCtl_Cache.Regime_Selection, WsCtl_Cache.Tri_Selection)
            If LstVues_Regime Is Nothing Then
                Return Nothing
            End If
            If WsCtl_Cache.Critere_Selection.Trim = "" Or WsCtl_Cache.Valeur_Selection = "" Then
                Return LstVues_Regime
            End If
            Select Case WsCtl_Cache.Critere_Selection
                Case "Catégorie"
                    LstVues_Filtre = (From instance In LstVues_Regime Where instance.Categorie = WsCtl_Cache.Valeur_Selection).ToList
                Case "Corps"
                    LstVues_Filtre = (From instance In LstVues_Regime Where instance.Corps = WsCtl_Cache.Valeur_Selection).ToList
                Case "Grade"
                    LstVues_Filtre = (From instance In LstVues_Regime Where instance.Grade = WsCtl_Cache.Valeur_Selection).ToList
                Case "Niveau 1"
                    LstVues_Filtre = (From instance In LstVues_Regime Where instance.Structure_N1 = WsCtl_Cache.Valeur_Selection).ToList
                Case "Niveau 2"
                    LstVues_Filtre = (From instance In LstVues_Regime Where instance.Structure_N2 = WsCtl_Cache.Valeur_Selection).ToList
                Case "Niveau 3"
                    LstVues_Filtre = (From instance In LstVues_Regime Where instance.Structure_N3 = WsCtl_Cache.Valeur_Selection).ToList
                Case "Niveau 4"
                    LstVues_Filtre = (From instance In LstVues_Regime Where instance.Structure_N4 = WsCtl_Cache.Valeur_Selection).ToList
                Case "Niveau 5"
                    LstVues_Filtre = (From instance In LstVues_Regime Where instance.Structure_N5 = WsCtl_Cache.Valeur_Selection).ToList
                Case "Nom"
                    LstVues_Filtre = (From instance In LstVues_Regime Where instance.Nom.StartsWith(WsCtl_Cache.Valeur_Selection.Trim, True, Nothing)).ToList
            End Select
            Select Case WsCtl_Cache.Tri_Selection
                Case "Par affectation"
                    Return (From instance In LstVues_Filtre Select instance Order By instance.Structure_N1, instance.Structure_N2, instance.Structure_N3,
                                                          instance.Structure_N4, instance.Structure_N5,
                                                          instance.Nom Ascending, instance.Prenom Ascending).ToList
                Case "Par grade"
                    Return (From instance In LstVues_Filtre Select instance Order By instance.Grade, instance.Nom Ascending, instance.Prenom Ascending).ToList
                Case Else
                    Return (From instance In LstVues_Filtre Select instance Order By instance.Nom Ascending, instance.Prenom Ascending).ToList
            End Select
            Return LstVues_Filtre
        End Get
    End Property

    Private Sub ListeGrid_ValeurChange(sender As Object, e As Systeme.Evenements.DonneeChangeEventArgs) Handles ListeEnveloppe.ValeurChange,
        ListeAffectation.ValeurChange, ListeBase100.ValeurChange, ListeModulation.ValeurChange, ListePaye.ValeurChange, ListeProrata.ValeurChange

        Dim Ide As Integer = 0
        Dim DateEffet As String = ""
        Dim TableauData(0) As String

        TableauData = Strings.Split(e.Valeur, "_")
        If IsNumeric(TableauData(0)) Then
            Ide = CInt(TableauData(0))
        End If
        If TableauData.Count > 1 Then
            DateEffet = TableauData(1)
        End If
        If Ide = 0 Or DateEffet = "" Then
            Exit Sub
        End If

        WsCtl_Cache = CacheVirControle
        WsCtl_Cache.Index_Selection = CType(sender, Virtualia.Net.Controles_VListeGrid).ItemSelectionne
        WsCtl_Cache.Ide_Dossier = Ide
        WsCtl_Cache.Date_Valeur = DateEffet
        CacheVirControle = WsCtl_Cache

        Call PersonnaliserSaisie(Ide, DateEffet, MultiVues.ActiveViewIndex)
        CType(WebFct.PointeurUtilisateur.V_PointeurSessionModule, Virtualia.Net.Session.SessionModule).VueActiveAlPha = "VuePartielle"

    End Sub

    Private Sub SaisieLigne_ValeurRetour(sender As Object, e As Systeme.Evenements.MessageRetourEventArgs) Handles SaisieLigne.ValeurRetour
        If WebFct Is Nothing Then
            WebFct = New Virtualia.Net.Controles.WebFonctions(Me, 1)
        End If
        CType(WebFct.PointeurUtilisateur.V_PointeurSessionModule, Virtualia.Net.Session.SessionModule).VueActiveAlPha = e.Emetteur
    End Sub

    Private Sub RadioSelRegime_ValeurChange(sender As Object, e As Systeme.Evenements.DonneeChangeEventArgs) Handles RadioSelRegime.ValeurChange
        WsCtl_Cache = CacheVirControle
        WsCtl_Cache.Regime_Selection = e.Valeur
        WsCtl_Cache.Index_Selection = -1
        If WsCtl_Cache.Critere_Selection <> "Nom" Then
            WsCtl_Cache.Critere_Selection = ""
            WsCtl_Cache.Valeur_Selection = ""
        End If
        CacheVirControle = WsCtl_Cache
        If CType(WebFct.PointeurUtilisateur.V_PointeurSessionModule, Virtualia.Net.Session.SessionModule).VueActiveAlPha = "VueAttente" Then
            CType(WebFct.PointeurUtilisateur.V_PointeurSessionModule, Virtualia.Net.Session.SessionModule).VueActiveAlPha = "Base100"
        End If
    End Sub

    Private Sub ComboSelAnnee_ValeurChange(sender As Object, e As DonneeChangeEventArgs) Handles ComboSelAnnee.ValeurChange
        WsCtl_Cache = CacheVirControle
        WsCtl_Cache.Annee_Selection = CInt(e.Valeur)
        CacheVirControle = WsCtl_Cache
        CType(WebFct.PointeurUtilisateur.V_PointeurSessionModule, Virtualia.Net.Session.SessionModule).VueActiveAlPha = ""
    End Sub

    Private Sub ComboSelTri_ValeurChange(sender As Object, e As Systeme.Evenements.DonneeChangeEventArgs) Handles ComboSelTri.ValeurChange
        WsCtl_Cache = CacheVirControle
        WsCtl_Cache.Tri_Selection = e.Valeur
        WsCtl_Cache.Index_Selection = -1
        CacheVirControle = WsCtl_Cache
    End Sub

    Private Sub ComboSelCritere_ValeurChange(sender As Object, e As Systeme.Evenements.DonneeChangeEventArgs) Handles ComboSelCritere.ValeurChange
        WsCtl_Cache = CacheVirControle
        WsCtl_Cache.Critere_Selection = e.Valeur
        WsCtl_Cache.Valeur_Selection = ""
        DonNom.Text = ""
        WsCtl_Cache.Index_Selection = -1
        CacheVirControle = WsCtl_Cache
    End Sub

    Private Sub ComboSelValeur_ValeurChange(sender As Object, e As Systeme.Evenements.DonneeChangeEventArgs) Handles ComboSelValeur.ValeurChange
        WsCtl_Cache = CacheVirControle
        WsCtl_Cache.Valeur_Selection = e.Valeur
        DonNom.Text = ""
        WsCtl_Cache.Index_Selection = -1
        CacheVirControle = WsCtl_Cache
    End Sub

    Private Sub CommandeCsv_Click(sender As Object, e As EventArgs) Handles CommandeCsv.Click
        Dim UtiSession As Virtualia.Net.Session.SessionModule = CType(WebFct.PointeurUtilisateur.V_PointeurSessionModule, Virtualia.Net.Session.SessionModule)
        Dim NomFichier As String = ""
        If UtiSession Is Nothing Then
            Exit Sub
        End If
        If UtiSession.VParent.V_NomdeConnexion <> "" Then
            NomFichier = WebFct.PointeurGlobal.VirRepertoireTemporaire & "\" & UtiSession.VParent.V_NomdeConnexion & ".csv"
        ElseIf UtiSession.DossierPER IsNot Nothing Then
            NomFichier = WebFct.PointeurGlobal.VirRepertoireTemporaire & "\" & UtiSession.DossierPER.Nom & ".csv"
        Else
            Exit Sub
        End If
        Dim FluxTeleChargement As Byte()

        Call EcrireFichierCsv(NomFichier)
        If My.Computer.FileSystem.FileExists(NomFichier) = False Then
            Exit Sub
        End If
        FluxTeleChargement = My.Computer.FileSystem.ReadAllBytes(NomFichier)
        If FluxTeleChargement IsNot Nothing Then
            Dim response As System.Web.HttpResponse = System.Web.HttpContext.Current.Response
            response.Clear()
            response.AddHeader("Content-Type", "binary/octet-stream")
            response.AddHeader("Content-Disposition", "attachment; filename=" & NomFichier & "; size=" & FluxTeleChargement.Length.ToString())
            response.Flush()
            response.BinaryWrite(FluxTeleChargement)
            response.Flush()
            response.End()
        End If
    End Sub

    Private Sub EcrireFichierCsv(ByVal Nom_Fichier As String)
        Dim FicStream As System.IO.FileStream
        Dim FicWriter As System.IO.StreamWriter
        Dim SysCodeIso As System.Text.Encoding = System.Text.Encoding.GetEncoding(1252)
        Dim LstVues As List(Of Virtualia.TablesObjet.ShemaVUE.VUE_ATTRIBUTAIRE_PFR)
        Dim LstValeurs As List(Of String)
        Dim IndiceI As Integer
        Dim IndiceK As Integer
        Dim Chaine As System.Text.StringBuilder

        If WebFct Is Nothing Then
            WebFct = New Virtualia.Net.Controles.WebFonctions(Me, 1)
        End If
        LstVues = ListeDesVues
        If LstVues Is Nothing OrElse LstVues.Count = 0 Then
            Exit Sub
        End If
        Chaine = New System.Text.StringBuilder
        FicStream = New System.IO.FileStream(Nom_Fichier, IO.FileMode.Create, IO.FileAccess.Write)
        FicWriter = New System.IO.StreamWriter(FicStream, SysCodeIso)

        LstValeurs = LstVues(0).Entete_Excel(0)
        For IndiceK = 0 To LstValeurs.Count - 2
            Chaine.Append(LstValeurs.Item(IndiceK) & VI.PointVirgule)
        Next IndiceK
        Chaine.Append(LstValeurs.Item(LstValeurs.Count - 1))
        FicWriter.WriteLine(Chaine.ToString)
        Chaine.Clear()

        LstValeurs = LstVues(0).Entete_Excel(1)
        For IndiceK = 0 To LstValeurs.Count - 2
            Chaine.Append(LstValeurs.Item(IndiceK) & VI.PointVirgule)
        Next IndiceK
        Chaine.Append(LstValeurs.Item(LstValeurs.Count - 1))
        FicWriter.WriteLine(Chaine.ToString)
        Chaine.Clear()

        For IndiceI = 0 To LstVues.Count - 1
            LstValeurs = LstVues.Item(IndiceI).V_GrilleVirtuelle("Excel")
            For IndiceK = 0 To LstValeurs.Count - 2
                Chaine.Append(LstValeurs.Item(IndiceK) & VI.PointVirgule)
            Next IndiceK
            Chaine.Append(LstValeurs.Item(LstValeurs.Count - 1))
            FicWriter.WriteLine(Chaine.ToString)
            Chaine.Clear()
        Next IndiceI

        FicWriter.Flush()
        FicWriter.Close()
    End Sub

    Private Sub DonNom_TextChanged(sender As Object, e As EventArgs) Handles DonNom.TextChanged
        WsCtl_Cache = CacheVirControle
        If DonNom.Text <> "" Then
            WsCtl_Cache.Valeur_Selection = DonNom.Text
            WsCtl_Cache.Critere_Selection = "Nom"
            WsCtl_Cache.Index_Selection = -1
        Else
            WsCtl_Cache.Valeur_Selection = ""
            WsCtl_Cache.Critere_Selection = ""
            WsCtl_Cache.Index_Selection = -1
        End If
        CacheVirControle = WsCtl_Cache
    End Sub

    Private Function LigneTotale(ByVal LstResulat As List(Of Virtualia.Net.PFR.SelfV4.LigneDotation), ByVal RegimeCategorie As String) As String
        Dim Chaine As System.Text.StringBuilder
        Dim LigneTotal As Virtualia.Net.PFR.SelfV4.LigneDotation = New Virtualia.Net.PFR.SelfV4.LigneDotation
        Dim Montant As Double

        For Each LigneEnveloppe As Virtualia.Net.PFR.SelfV4.LigneDotation In LstResulat
            If LigneEnveloppe.Regime_Categorie = RegimeCategorie Then
                LigneTotal.Enveloppe_Modulation += LigneEnveloppe.Enveloppe_Modulation
                LigneTotal.Montant_Modulation += LigneEnveloppe.Montant_Modulation
            End If
        Next
        Chaine = New System.Text.StringBuilder
        Chaine.Append("TOTAL " & RegimeCategorie & StrDup(4, VI.Tild))
        Select Case LigneTotal.Montant_Modulation
            Case 0
                Chaine.Append(VI.Tild)
            Case Is > 0
                Chaine.Append(Strings.Format(LigneTotal.Montant_Modulation, "### ##0.00") & VI.Tild)
            Case Is < 0
                Montant = Montant * -1
                Chaine.Append("-" & Strings.Format(LigneTotal.Montant_Modulation, "### ##0.00") & VI.Tild)
        End Select
        Select Case LigneTotal.Enveloppe_Modulation
            Case 0
                Chaine.Append(VI.Tild)
            Case Is > 0
                Chaine.Append(Strings.Format(LigneTotal.Enveloppe_Modulation, "### ##0.00") & VI.Tild)
            Case Is < 0
                Montant = Montant * -1
                Chaine.Append("-" & Strings.Format(LigneTotal.Enveloppe_Modulation, "### ##0.00") & VI.Tild)
        End Select
        Montant = LigneTotal.Enveloppe_Modulation - LigneTotal.Montant_Modulation
        Select Case Montant
            Case 0
                Chaine.Append(VI.Tild)
            Case Is > 0
                Chaine.Append(Strings.Format(Montant, "### ##0.00") & VI.Tild)
            Case Is < 0
                Montant = Montant * -1
                Chaine.Append("-" & Strings.Format(Montant, "### ##0.00") & VI.Tild)
        End Select
        Chaine.Append(VI.SigneBarre)
        Return Chaine.ToString
    End Function

    Private Sub CommandeFiger_Click(sender As Object, e As System.EventArgs) Handles CommandeFiger.Click
        Dim TitreMsg As String = "Opération figeant la saisie des modulations pour le niveau hiérarchique inférieur."
        Dim Msg As String = "Cette opération va figer toute possibilité de modulation par les chefs de service sous votre responsabilité." & vbCrLf
        Msg &= "Confirmez-vous la réalisation de cette opération ?"
        Dim NatureCmd As String = "Oui;Non"

        Dim Evenement As Virtualia.Systeme.Evenements.MessageSaisieEventArgs
        Evenement = New Virtualia.Systeme.Evenements.MessageSaisieEventArgs("Figer", 0, "", NatureCmd, TitreMsg, Msg)
        HPopupMsg.Value = "1"
        CellMessage.Visible = True
        MsgVirtualia.AfficherMessage = Evenement
        PopupMsg.Show()
    End Sub

    Private Sub MsgVirtualia_ValeurRetour(sender As Object, e As Systeme.Evenements.MessageRetourEventArgs) Handles MsgVirtualia.ValeurRetour
        HPopupMsg.Value = "0"
        CellMessage.Visible = False
        If e.Emetteur <> "Figer" OrElse e.ReponseMsg <> "Oui" Then
            CType(WebFct.PointeurUtilisateur.V_PointeurSessionModule, Virtualia.Net.Session.SessionModule).VueActiveAlPha = "Base100"
            MultiVues.SetActiveView(VueBase100)
            Exit Sub
        End If
        Call FigerModulation()
        CType(WebFct.PointeurUtilisateur.V_PointeurSessionModule, Virtualia.Net.Session.SessionModule).VueActiveAlPha = "VueAttente"
        MultiVues.SetActiveView(VueAttente)
    End Sub

    Private Sub FigerModulation()
        Dim LstVues As List(Of Virtualia.TablesObjet.ShemaVUE.VUE_ATTRIBUTAIRE_PFR)
        Dim Cretour As Boolean
        Dim IndiceI As Integer
        Dim Regime As String = ""

        If WebFct Is Nothing Then
            WebFct = New Virtualia.Net.Controles.WebFonctions(Me, 1)
        End If
        WsCtl_Cache = CacheVirControle
        For IndiceI = 0 To 2
            Select Case IndiceI
                Case 0
                    Regime = "PFR"
                Case 1
                    Regime = "PS"
                Case 2
                    Regime = "F_R"
            End Select
            LstVues = CType(WebFct.PointeurUtilisateur.V_PointeurSessionModule, Virtualia.Net.Session.SessionModule).ListeVues_PFR(WsCtl_Cache.Annee_Selection, Regime, "Alphabétique")
            If LstVues IsNot Nothing Then
                For Each FicheVue As Virtualia.TablesObjet.ShemaVUE.VUE_ATTRIBUTAIRE_PFR In LstVues
                    FicheVue.Niveau_Blocage = CType(WebFct.PointeurUtilisateur.V_PointeurSessionModule, Virtualia.Net.Session.SessionModule).NiveauManager + 1
                    Cretour = WebFct.PointeurGlobal.VirServiceServeur.MiseAjour_Fiche(WebFct.PointeurGlobal.VirNomUtilisateur, VI.PointdeVue.PVueVueLogique, 7,
                                                               FicheVue.Ide_Dossier, "M", FicheVue.FicheLue, FicheVue.ContenuTable)
                Next
            End If
        Next IndiceI
        EtiAttente.Text = "Opération effectuée."
    End Sub

End Class