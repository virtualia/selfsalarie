﻿'------------------------------------------------------------------------------
' <généré automatiquement>
'     Ce code a été généré par un outil.
'
'     Les modifications apportées à ce fichier peuvent provoquer un comportement incorrect et seront perdues si
'     le code est régénéré.
' </généré automatiquement>
'------------------------------------------------------------------------------

Option Strict On
Option Explicit On


Partial Public Class FrmHeuresSup

    '''<summary>
    '''Contrôle CadreGeneral.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CadreGeneral As Global.System.Web.UI.WebControls.Table

    '''<summary>
    '''Contrôle CadreAffichage.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CadreAffichage As Global.System.Web.UI.WebControls.Panel

    '''<summary>
    '''Contrôle CadreFrais.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CadreFrais As Global.System.Web.UI.WebControls.Table

    '''<summary>
    '''Contrôle EtiIdentité.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents EtiIdentité As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle CadreAnnee.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CadreAnnee As Global.System.Web.UI.WebControls.Table

    '''<summary>
    '''Contrôle EtiSelAnnee.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents EtiSelAnnee As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle DropDownAnnee.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents DropDownAnnee As Global.System.Web.UI.WebControls.DropDownList

    '''<summary>
    '''Contrôle CadreSolde.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CadreSolde As Global.System.Web.UI.WebControls.Table

    '''<summary>
    '''Contrôle EtiSoldeAnte.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents EtiSoldeAnte As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle DonSoldeAnte.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents DonSoldeAnte As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle EtiTotalHS.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents EtiTotalHS As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle DonTotalHS.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents DonTotalHS As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle EtiTotalRecup.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents EtiTotalRecup As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle DonTotalRecup.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents DonTotalRecup As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle EtiSoldeHS.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents EtiSoldeHS As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle DonSoldeHS.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents DonSoldeHS As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle EtiListeHS.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents EtiListeHS As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle ListeHS.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents ListeHS As Global.Virtualia.Net.Controles_VListeGrid

    '''<summary>
    '''Contrôle EtiListeRecup.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents EtiListeRecup As Global.System.Web.UI.WebControls.Label

    '''<summary>
    '''Contrôle ListeRecup.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents ListeRecup As Global.Virtualia.Net.Controles_VListeGrid

    '''<summary>
    '''Propriété Master.
    '''</summary>
    '''<remarks>
    '''Propriété générée automatiquement.
    '''</remarks>
    Public Shadows ReadOnly Property Master() As Virtualia.Net.VirtualiaMain
        Get
            Return CType(MyBase.Master, Virtualia.Net.VirtualiaMain)
        End Get
    End Property
End Class
