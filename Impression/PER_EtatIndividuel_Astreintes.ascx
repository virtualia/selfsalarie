﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind= "PER_EtatIndividuel_Astreintes.ascx.vb" Inherits="Virtualia.Net.VirtualiaFenetre_PER_EtatIndividuel_Astreintes" %>

<%@ Register src="~/Controles/Saisies/VCoupleVerticalEtiDonnee.ascx" tagname="VCoupleVerticalEtiDonnee" tagprefix="Virtualia" %>
<%@ Register src="~/Controles/Saisies/VTrioHorizontalRadio.ascx" tagname="VTrioHorizontalRadio" tagprefix="Virtualia" %>
<%@ Register Src="~/Controles/Commun/VListeGrid.ascx" TagName="VListeGrid" TagPrefix="Virtualia" %>

<asp:Table ID="CadreInfo" runat="server" Width="750px" Height="750px">
    <asp:TableRow>
        <asp:TableCell>
            <asp:Table ID="O_CadreInfo" runat="server" BorderStyle="None" BorderWidth="2px" Visible="true"
                BorderColor="#B0E0D7" Width="750px" HorizontalAlign="Left" style="margin-top: 1px;">
                <asp:TableRow>
                  <asp:TableCell>
                    <asp:Table ID="O_CadreTitreAstreinte" runat="server" Height="50px" CellPadding="0" Width="750px" 
                        CellSpacing="0" HorizontalAlign="Center" BorderStyle="none" BorderWidth="1px" Visible="true" BorderColor="#124545">
                        <asp:TableRow>
                            <asp:TableCell Height="5px"></asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Left">
                                <asp:Table ID="CadreLogoCAppel" runat="server" BackImageUrl="~/Images/General/LogoCAppel.jpg" Width="180px"
                                    BorderStyle="None" CellPadding="0" CellSpacing="0" Visible="true">
                                    <asp:TableRow>
                                        <asp:TableCell Height="120px" BackColor="Transparent" Width="180px"></asp:TableCell>
                                    </asp:TableRow>
                                </asp:Table>    
                            </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow>
                            <asp:TableCell Height="10px"></asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Center">
                                <asp:Label ID="O_Etiquette" runat="server" Text="ETAT MENSUEL DES ASTREINTES PAR AGENT" Height="25px" Width="746px"
                                    BackColor="Transparent"  BorderColor="#B0E0D7" BorderStyle="None"
                                    BorderWidth="1px" ForeColor="Black"
                                    Font-Bold="True" Font-Names="Trebuchet MS" Font-Size="Medium"
                                    style="margin-top: 1px; margin-left: 1px; margin-bottom: 0px;
                                    font-style: normal; text-indent: 1px; text-align: center;">
                                </asp:Label>          
                            </asp:TableCell>      
                        </asp:TableRow>
                         <asp:TableRow>
                            <asp:TableCell Height="40px" ></asp:TableCell>
                        </asp:TableRow>
                    </asp:Table>
                  </asp:TableCell>
                </asp:TableRow> 
                <asp:TableRow>
                  <asp:TableCell>
                    <asp:Table ID="O_CadreEntete" runat="server" CellPadding="0" CellSpacing="0" Width="750px">
                        <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Left">
                                 <asp:Label ID="EtiMois" runat="server" Height="20px" Width="300px"
                                    BackColor="Transparent" BorderStyle="None" Text="Mois :"
                                    ForeColor="Black" Font-Italic="True"
                                    Font-Bold="True" Font-Names="Trebuchet MS" Font-Size= "Medium"
                                    style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                    font-style: italic; text-indent: 1px; text-align: left;">
                                 </asp:Label>          
                            </asp:TableCell>
                            <asp:TableCell HorizontalAlign="Left">
                                 <asp:Label ID="Mois" runat="server" Height="20px" Width="400px"
                                    BackColor="Transparent" BorderStyle="None" 
                                    ForeColor="Black" Font-Italic="False"
                                    Font-Bold="True" Font-Names="Trebuchet MS" Font-Size= "Medium"
                                    style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                    font-style: normal; text-indent: 5px; text-align: left;">
                                 </asp:Label>          
                            </asp:TableCell> 
                        </asp:TableRow>
                        <asp:TableRow>
                            <asp:TableCell Height="10px" ColumnSpan="2"></asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Left">
                                 <asp:Label ID="EtiJuridiction" runat="server" Height="20px" Width="300px"
                                    BackColor="Transparent" BorderStyle="None" Text="Juridiction :"
                                    ForeColor="Black" Font-Italic="True"
                                    Font-Bold="True" Font-Names="Trebuchet MS" Font-Size= "Medium"
                                    style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                    font-style: italic; text-indent: 1px; text-align: left;">
                                 </asp:Label>          
                            </asp:TableCell>
                            <asp:TableCell HorizontalAlign="Left">
                                 <asp:Label ID="Juridiction" runat="server" Height="20px" Width="400px"
                                    BackColor="Transparent" BorderStyle="None" Text=""
                                    ForeColor="Black" Font-Italic="False"
                                    Font-Bold="True" Font-Names="Trebuchet MS" Font-Size= "Medium"
                                    style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                    font-style: normal; text-indent: 5px; text-align: left;">
                                 </asp:Label>          
                            </asp:TableCell> 
                        </asp:TableRow>
                         <asp:TableRow>
                            <asp:TableCell Height="10px" ColumnSpan="2"></asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Left">
                                 <asp:Label ID="EtiNomEtPrenom" runat="server" Height="20px" Width="300px"
                                    BackColor="Transparent" BorderStyle="None" Text="NOM - Prénom :"
                                    ForeColor="Black" Font-Italic="True"
                                    Font-Bold="True" Font-Names="Trebuchet MS" Font-Size= "Medium"
                                    style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                    font-style: italic; text-indent: 1px; text-align: left;">
                                 </asp:Label>          
                            </asp:TableCell>
                            <asp:TableCell HorizontalAlign="Left">
                                 <asp:Label ID="NomEtPrenom" runat="server" Height="20px" Width="400px"
                                    BackColor="Transparent" BorderStyle="None" Text=""
                                    ForeColor="Black" Font-Italic="False"
                                    Font-Bold="True" Font-Names="Trebuchet MS" Font-Size= "Medium"
                                    style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                    font-style: normal; text-indent: 5px; text-align: left;">
                                 </asp:Label>          
                            </asp:TableCell> 
                        </asp:TableRow>
                        <asp:TableRow>
                            <asp:TableCell Height="10px" ColumnSpan="2"></asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Left">
                                 <asp:Label ID="EtiFonctions" runat="server" Height="20px" Width="300px"
                                    BackColor="Transparent" BorderStyle="None" Text="Fonctions :"
                                    ForeColor="Black" Font-Italic="True"
                                    Font-Bold="True" Font-Names="Trebuchet MS" Font-Size= "Medium"
                                    style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                    font-style: italic; text-indent: 1px; text-align: left;">
                                 </asp:Label>          
                            </asp:TableCell>
                            <asp:TableCell HorizontalAlign="Left">
                                 <asp:Label ID="Fonctions" runat="server" Height="20px" Width="400px"
                                    BackColor="Transparent" BorderStyle="None" Text=""
                                    ForeColor="Black" Font-Italic="False"
                                    Font-Bold="True" Font-Names="Trebuchet MS" Font-Size= "Medium"
                                    style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                    font-style: normal; text-indent: 5px; text-align: left;">
                                 </asp:Label>          
                            </asp:TableCell> 
                        </asp:TableRow>
                        <asp:TableRow>
                            <asp:TableCell Height="10px" ColumnSpan="2"></asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Left">
                                 <asp:Label ID="EtiServiceAstreinte" runat="server" Height="20px" Width="300px"
                                    BackColor="Transparent" BorderStyle="None" Text="Service d'astreinte :"
                                    ForeColor="Black" Font-Italic="True"
                                    Font-Bold="True" Font-Names="Trebuchet MS" Font-Size= "Medium"
                                    style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                    font-style: italic; text-indent: 1px; text-align: left;">
                                 </asp:Label>          
                            </asp:TableCell>
                            <asp:TableCell HorizontalAlign="Left">
                                 <asp:Label ID="ServiceAstreinte" runat="server" Height="20px" Width="400px"
                                    BackColor="Transparent" BorderStyle="None" Text=""
                                    ForeColor="Black" Font-Italic="False"
                                    Font-Bold="True" Font-Names="Trebuchet MS" Font-Size= "Medium"
                                    style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                    font-style: normal; text-indent: 5px; text-align: left;">
                                 </asp:Label>          
                            </asp:TableCell> 
                        </asp:TableRow>

                        <asp:TableRow>
                            <asp:TableCell Height="20px" ColumnSpan="2"></asp:TableCell>
                        </asp:TableRow>
   
                    </asp:Table>
                  </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                  <asp:TableCell Height="5px" ColumnSpan="2"></asp:TableCell>
                </asp:TableRow> 
<asp:TableRow>
        <asp:TableCell>
            <asp:Table ID="I_CadreAstreintes" runat="server" CellPadding="0" CellSpacing="0" Width="750px"
            BackColor="Transparent" BorderColor="Transparent" BorderStyle="NotSet" BorderWidth="1px">
              <asp:TableRow>
                  <asp:TableCell HorizontalAlign="Left" ColumnSpan="3">
                        <asp:Table ID="Table1" runat="server" CellPadding="0" CellSpacing="0" Width="747px"
                        BackColor="Transparent" BorderColor="Black" BorderStyle="NotSet" BorderWidth="1px">
                          <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Left">
                                <asp:Label ID="Label1" runat="server" Height="25px" Width="700px"
                                   BackColor="Transparent" BorderStyle="None" 
                                   Text="JOUR ET DATE D'ASTREINTES"
                                   ForeColor="Black" Font-Italic="False" DonBorderWidth="1px"
                                   Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Medium"
                                   style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px;
                                   font-style: normal; text-indent: 1px; text-align:  center;">
                                </asp:Label>
                            </asp:TableCell>
                          </asp:TableRow>
                        </asp:Table>          
                    </asp:TableCell>  
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Table ID="I_TableEnteteDateAstreinte" runat="server" CellPadding="0" CellSpacing="0" Width="747px"
                        BackColor="Transparent" BorderColor="Black" BorderStyle="NotSet" BorderWidth="1px">
                          <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Left">
                                <asp:Label ID="I_LabelEnteteDateAstreinte" runat="server" Height="28px" Width="250px"
                                   BackColor="Transparent" BorderStyle="None" 
                                   Text="Samedi"
                                   ForeColor="Black" Font-Italic="False"
                                   Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="80%"
                                   style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                   font-style: normal; text-indent: 1px; text-align:  center;">
                                </asp:Label>
                            </asp:TableCell>
                            <asp:TableCell HorizontalAlign="Left">
                                <asp:Label ID="I_LabelEnteteNature" runat="server" Height="28px" Width="250px"
                                   BackColor="Transparent" BorderStyle="None" 
                                   Text="Dimanche"
                                   ForeColor="Black" Font-Italic="False"
                                   Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="80%"
                                   style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                   font-style: normal; text-indent: 1px; text-align:  center;">
                                </asp:Label>
                            </asp:TableCell>
                            <asp:TableCell HorizontalAlign="Left">
                                <asp:Label ID="I_LabelEnteteValide" runat="server" Height="28px" Width="240px"
                                   BackColor="Transparent" BorderStyle="None" 
                                   Text="Jour férié"
                                   ForeColor="Black" Font-Italic="False"
                                   Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="80%"
                                   style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                   font-style: normal; text-indent: 1px; text-align:  center;">
                                </asp:Label>
                            </asp:TableCell>
                          </asp:TableRow>
                        </asp:Table>          
                    </asp:TableCell>          
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:Table ID="I_TableAsstreinte" runat="server" CellPadding="0" CellSpacing="0" Width="747px"
                        BackColor="Transparent" BorderColor="Black" BorderStyle="NotSet" BorderWidth="1px">
                        </asp:Table>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                  <asp:TableCell HorizontalAlign="Left" ColumnSpan="3">
                        <asp:Table ID="I_TableEnteteNombreAstreinte" runat="server" CellPadding="0" CellSpacing="0" Width="747px"
                        BackColor="Transparent" BorderColor="Black" BorderStyle="NotSet" BorderWidth="1px">
                          <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Left">
                                <asp:Label ID="I_LabelEnteteNombreAsteinte" runat="server" Height="25px" Width="500px"
                                   BackColor="Transparent" BorderStyle="None" 
                                   Text="Nombre d'astreintes réalisées dans le mois :"
                                   ForeColor="Black" Font-Italic="False" DonBorderWidth="1px"
                                   Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="80%"
                                   style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px;
                                   font-style: normal; text-indent: 1px; text-align:  center;">
                                </asp:Label>
                            </asp:TableCell>
                            <asp:TableCell HorizontalAlign="Left">
                                <asp:Label ID="NombreAstreinte" runat="server" Height="25px" Width="200px" 
                                   BackColor="Transparent" BorderStyle="None" 
                                   ForeColor="Black" Font-Italic="False" DonBorderWidth="1px"
                                   Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="80%"
                                   style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px;
                                   font-style: normal; text-indent: 1px; text-align:  center;">
                                </asp:Label>
                            </asp:TableCell>
                          </asp:TableRow>
                        </asp:Table>          
                    </asp:TableCell>  
                </asp:TableRow>
                <asp:TableRow>
                  <asp:TableCell HorizontalAlign="Left" ColumnSpan="3">
                        <asp:Table ID="I_TableEnteteRemuneration" runat="server" CellPadding="0" CellSpacing="0" Width="747px"
                        BackColor="Transparent" BorderColor="Black" BorderStyle="NotSet" BorderWidth="1px">
                          <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Left">
                                <asp:Label ID="I_LabelEnteteRemuneration" runat="server" Height="35px" Width="500px"
                                   BackColor="Transparent" BorderStyle="None" 
                                   Text="Rémunération pour le mois : <br/> (50 euros par astreinte, plafond de 500 euros par mois)"
                                   ForeColor="Black" Font-Italic="False" DonBorderWidth="1px"
                                   Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="80%"
                                   style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px;
                                   font-style: normal; text-indent: 1px; text-align:  center;">
                                </asp:Label>
                            </asp:TableCell>
                            <asp:TableCell HorizontalAlign="Left">
                                <asp:Label ID="Remuneration" runat="server" Height="20px" Width="200px" 
                                   BackColor="Transparent" BorderStyle="None" 
                                   ForeColor="Black" Font-Italic="False" DonBorderWidth="1px"
                                   Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="80%"
                                   style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px;
                                   font-style: normal; text-indent: 1px; text-align:  center;">
                                </asp:Label>
                            </asp:TableCell>
                          </asp:TableRow>
                        </asp:Table>          
                    </asp:TableCell>  
                </asp:TableRow>
            </asp:Table>
       </asp:TableCell> 
    </asp:TableRow>
     <asp:TableRow>
                 <asp:TableCell>
                    <asp:Table ID="TablePiedDePage" runat="server" CellPadding="0" CellSpacing="0" Width="750px">
                        <asp:TableRow>
                            <asp:TableCell Height="20px" ColumnSpan="2"></asp:TableCell>
                        </asp:TableRow>

                        <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Left">
                                 <asp:Label ID="I_EtatDresseLe" runat="server" Height="20px" Width="500px"
                                    BackColor="Transparent" BorderStyle="None" Text="Etat dressé le :"
                                    ForeColor="Black" Font-Italic="True"
                                    Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Small"
                                    style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                    font-style: italic; text-indent: 1px; text-align: left;">
                                 </asp:Label>          
                            </asp:TableCell>
                            <asp:TableCell HorizontalAlign="Left">
                                 <asp:Label ID="DateEtat" runat="server" Height="20px" Width="200px"
                                    BackColor="Transparent" BorderStyle="None" Text=""
                                    ForeColor="Black" Font-Italic="False"
                                    Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Medium"
                                    style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                    font-style: normal; text-indent: 5px; text-align: left;">
                                 </asp:Label>          
                            </asp:TableCell> 
                        </asp:TableRow>
                        <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Left">
                                 <asp:Label ID="I_EtiDirecteur" runat="server" Height="20px" Width="500px"
                                    BackColor="Transparent" BorderStyle="None" Text="Le directeur/la directrice de greffe :"
                                    ForeColor="Black" Font-Italic="True"
                                    Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Small"
                                    style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                    font-style: italic; text-indent: 1px; text-align: left;">
                                 </asp:Label>          
                            </asp:TableCell>
                            <asp:TableCell HorizontalAlign="Left">
                                 <asp:Label ID="DirecteurGreffe" runat="server" Height="20px" Width="200px"
                                    BackColor="Transparent" BorderStyle="None" Text=""
                                    ForeColor="Black" Font-Italic="False"
                                    Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Medium"
                                    style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                    font-style: normal; text-indent: 5px; text-align: left;">
                                 </asp:Label>          
                            </asp:TableCell> 
                        </asp:TableRow>
                        <asp:TableRow>
                            <asp:TableCell Height="60px" ColumnSpan="2"></asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow >
                            <asp:TableCell HorizontalAlign="Left" >
                                 <asp:Label ID="I_EtiArrete" runat="server" Height="110px" Width="500px"
                                    BackColor="Transparent" Text=" Arrêté du 6 février 2017 modifiant l'arrêté du 28 décembre 2001 <br/>  fixant le taux de rémunération et les modalités de compensation <br/> horaire des astreintes effectuées <br/>  par certains agents du ministère de la justice. <br/>  <br/> <b> Carte 22 -  code indemnité 0667</b>"
                                   BorderColor="Black" BorderStyle="NotSet" BorderWidth="1px"  ForeColor="Black" Font-Italic="True" 
                                    Font-Bold="false" Font-Names="Trebuchet MS" Font-Size= "Small"
                                    style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px; 
                                    font-style: italic; text-indent: 1px; text-align: center;">
                                 </asp:Label>          
                            </asp:TableCell>
                            <asp:TableCell HorizontalAlign="Left">
                                 <asp:Label ID="I_Arrete" runat="server" Height="20px" Width="200px"
                                    BackColor="Transparent" BorderStyle="None" Text=""
                                    ForeColor="Black" Font-Italic="False"
                                    Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Medium"
                                    style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                                    font-style: normal; text-indent: 5px; text-align: left;">
                                 </asp:Label>          
                            </asp:TableCell> 
                        </asp:TableRow>
                    </asp:Table>
                  </asp:TableCell>
                </asp:TableRow>
              </asp:Table>
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableFooterRow>
        <asp:TableCell Height="30px" HorizontalAlign="Right" VerticalAlign="Bottom"> 
        </asp:TableCell>
    </asp:TableFooterRow>
</asp:Table>