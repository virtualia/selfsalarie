﻿'------------------------------------------------------------------------------
' <généré automatiquement>
'     Ce code a été généré par un outil.
'
'     Les modifications apportées à ce fichier peuvent provoquer un comportement incorrect et seront perdues si
'     le code est régénéré.
' </généré automatiquement>
'------------------------------------------------------------------------------

Option Strict On
Option Explicit On


Partial Public Class CtlPerHeureSupMensuel
    
    '''<summary>
    '''Contrôle CadreControle.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CadreControle As Global.System.Web.UI.WebControls.Table
    
    '''<summary>
    '''Contrôle CadreInfo.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CadreInfo As Global.System.Web.UI.WebControls.Table
    
    '''<summary>
    '''Contrôle CadreTitre.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CadreTitre As Global.System.Web.UI.WebControls.Table
    
    '''<summary>
    '''Contrôle EtiDateValeur.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents EtiDateValeur As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle TableauDonHeures.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents TableauDonHeures As Global.System.Web.UI.WebControls.Table
    
    '''<summary>
    '''Contrôle EtiTotalEffectue.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents EtiTotalEffectue As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle EtiTotalRemunere.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents EtiTotalRemunere As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle EtiTotalCompense.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents EtiTotalCompense As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle Eti14Premieres.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Eti14Premieres As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle EtiAuDela14.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents EtiAuDela14 As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle EtiChoixRemu14.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents EtiChoixRemu14 As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle EtiChoixRemuAuDela14.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents EtiChoixRemuAuDela14 As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle EtiCompense25.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents EtiCompense25 As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle EtiCompenseAuDela25.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents EtiCompenseAuDela25 As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle EtiTotalJour.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents EtiTotalJour As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle InfoJour14.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents InfoJour14 As Global.Virtualia.Net.Controles_VCoupleEtiDonnee
    
    '''<summary>
    '''Contrôle InfoJour25.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents InfoJour25 As Global.Virtualia.Net.Controles_VCoupleEtiDonnee
    
    '''<summary>
    '''Contrôle InfoH20.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents InfoH20 As Global.Virtualia.Net.Controles_VCoupleEtiDonnee
    
    '''<summary>
    '''Contrôle InfoH23.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents InfoH23 As Global.Virtualia.Net.Controles_VCoupleEtiDonnee
    
    '''<summary>
    '''Contrôle InfoJour40.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents InfoJour40 As Global.Virtualia.Net.Controles_VCoupleEtiDonnee
    
    '''<summary>
    '''Contrôle InfoJour41.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents InfoJour41 As Global.Virtualia.Net.Controles_VCoupleEtiDonnee
    
    '''<summary>
    '''Contrôle EtiTotalNuit.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents EtiTotalNuit As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle InfoNuit14.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents InfoNuit14 As Global.Virtualia.Net.Controles_VCoupleEtiDonnee
    
    '''<summary>
    '''Contrôle InfoNuit25.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents InfoNuit25 As Global.Virtualia.Net.Controles_VCoupleEtiDonnee
    
    '''<summary>
    '''Contrôle InfoH21.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents InfoH21 As Global.Virtualia.Net.Controles_VCoupleEtiDonnee
    
    '''<summary>
    '''Contrôle InfoH24.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents InfoH24 As Global.Virtualia.Net.Controles_VCoupleEtiDonnee
    
    '''<summary>
    '''Contrôle InfoNuit40.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents InfoNuit40 As Global.Virtualia.Net.Controles_VCoupleEtiDonnee
    
    '''<summary>
    '''Contrôle InfoNuit41.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents InfoNuit41 As Global.Virtualia.Net.Controles_VCoupleEtiDonnee
    
    '''<summary>
    '''Contrôle EtiTotalWeJf.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents EtiTotalWeJf As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle InfoJF14.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents InfoJF14 As Global.Virtualia.Net.Controles_VCoupleEtiDonnee
    
    '''<summary>
    '''Contrôle InfoJF25.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents InfoJF25 As Global.Virtualia.Net.Controles_VCoupleEtiDonnee
    
    '''<summary>
    '''Contrôle InfoH22.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents InfoH22 As Global.Virtualia.Net.Controles_VCoupleEtiDonnee
    
    '''<summary>
    '''Contrôle InfoH25.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents InfoH25 As Global.Virtualia.Net.Controles_VCoupleEtiDonnee
    
    '''<summary>
    '''Contrôle InfoJF40.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents InfoJF40 As Global.Virtualia.Net.Controles_VCoupleEtiDonnee
    
    '''<summary>
    '''Contrôle InfoJF41.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents InfoJF41 As Global.Virtualia.Net.Controles_VCoupleEtiDonnee
    
    '''<summary>
    '''Contrôle CadreDetailCompensation.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CadreDetailCompensation As Global.System.Web.UI.WebControls.Table
    
    '''<summary>
    '''Contrôle EtiHsSEmaine.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents EtiHsSEmaine As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle EtiWEJF.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents EtiWEJF As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle InfoH01.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents InfoH01 As Global.Virtualia.Net.Controles_VCoupleEtiDonnee
    
    '''<summary>
    '''Contrôle InfoH05.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents InfoH05 As Global.Virtualia.Net.Controles_VCoupleEtiDonnee
    
    '''<summary>
    '''Contrôle InfoJFT1.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents InfoJFT1 As Global.Virtualia.Net.Controles_VCoupleEtiDonnee
    
    '''<summary>
    '''Contrôle InfoH02.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents InfoH02 As Global.Virtualia.Net.Controles_VCoupleEtiDonnee
    
    '''<summary>
    '''Contrôle InfoOuv2.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents InfoOuv2 As Global.Virtualia.Net.Controles_VCoupleEtiDonnee
    
    '''<summary>
    '''Contrôle InfoH06.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents InfoH06 As Global.Virtualia.Net.Controles_VCoupleEtiDonnee
    
    '''<summary>
    '''Contrôle InfoJFT2.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents InfoJFT2 As Global.Virtualia.Net.Controles_VCoupleEtiDonnee
    
    '''<summary>
    '''Contrôle InfoH03.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents InfoH03 As Global.Virtualia.Net.Controles_VCoupleEtiDonnee
    
    '''<summary>
    '''Contrôle InfoOuv3.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents InfoOuv3 As Global.Virtualia.Net.Controles_VCoupleEtiDonnee
    
    '''<summary>
    '''Contrôle InfoH07.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents InfoH07 As Global.Virtualia.Net.Controles_VCoupleEtiDonnee
    
    '''<summary>
    '''Contrôle InfoJFT3.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents InfoJFT3 As Global.Virtualia.Net.Controles_VCoupleEtiDonnee
    
    '''<summary>
    '''Contrôle InfoH04.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents InfoH04 As Global.Virtualia.Net.Controles_VCoupleEtiDonnee
    
    '''<summary>
    '''Contrôle InfoH08.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents InfoH08 As Global.Virtualia.Net.Controles_VCoupleEtiDonnee
    
    '''<summary>
    '''Contrôle CadreDemande.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CadreDemande As Global.System.Web.UI.WebControls.Table
    
    '''<summary>
    '''Contrôle EtiVisas.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents EtiVisas As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle InfoH09.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents InfoH09 As Global.Virtualia.Net.Controles_VCoupleEtiDonnee
    
    '''<summary>
    '''Contrôle InfoH11.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents InfoH11 As Global.Virtualia.Net.Controles_VCoupleEtiDonnee
    
    '''<summary>
    '''Contrôle InfoH13.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents InfoH13 As Global.Virtualia.Net.Controles_VCoupleEtiDonnee
    
    '''<summary>
    '''Contrôle InfoH15.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents InfoH15 As Global.Virtualia.Net.Controles_VCoupleEtiDonnee
    
    '''<summary>
    '''Contrôle InfoH17.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents InfoH17 As Global.Virtualia.Net.Controles_VCoupleEtiDonnee
    
    '''<summary>
    '''Contrôle CmdDemande.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CmdDemande As Global.System.Web.UI.WebControls.Button
End Class
