﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="VBulletinPaie.ascx.vb" Inherits="Virtualia.Net.VBulletinPaie" %>

<%@ Register src="~/Controles/Commun/VListeGrid.ascx" tagname="VListeGrid" tagprefix="Virtualia" %>
<%@ Register src="~/Controles/Commun/VListeCombo.ascx" tagname="VListeCombo" tagprefix="Virtualia" %>

<asp:Table ID="CadreGeneral" runat="server" BorderStyle="None" BorderWidth="2px" Visible="true"
    BorderColor="#B0E0D7" Width="800px" HorizontalAlign="Center" style="margin-top: 1px;">
     <asp:TableRow>
        <asp:TableCell>
            <asp:Label ID="EtiTitre" runat="server" Text="FAC-SIMILE DU BULLETIN DE PAIE" Height="40px" Width="746px"
                BackColor="Transparent" BorderColor="#B0E0D7" BorderStyle="None" BorderWidth="2px" ForeColor="#124545"
                Font-Bold="True" Font-Names="Trebuchet MS" Font-Size="Large"
                style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px;
                font-style: normal; text-indent: 5px; text-align: center;">
            </asp:Label>   
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell HorizontalAlign="Center" VerticalAlign="Top" Height="30px">
            <Virtualia:VListeCombo ID="LstLibelMois" runat="server" EtiVisible="true" EtiText="Mois" EtiWidth="60px"
            V_NomTable="Mois" LstWidth="250px" LstBorderColor="#B0E0D7" LstBackColor="#EEECFD"
            EtiStyle="text-align:center" EtiBackColor="#2D8781" EtiForeColor="White" />
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell>
            <asp:Table ID="CadreBulletin" runat="server" CellPadding="0" CellSpacing="0"
                 Width="760px" HorizontalAlign="Center">
                <asp:TableRow>
                    <asp:TableCell>
                        <asp:Label ID="EtiSelMois" runat="server" Height="25px" Width="760px"
                           BackColor="#6C9690" BorderColor="#B0E0D7" BorderStyle="Outset" BorderWidth="1px" ForeColor="#D7FAF3"
                           Text="" Font-Italic="True"
                           Font-Bold="True" Font-Names="Trebuchet MS" Font-Size= "Medium"
                           style="margin-top: 5px; margin-bottom: 1px;
                           font-style: normal; text-indent: 2px; text-align: center">
                        </asp:Label>          
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                <asp:TableCell>
                    <asp:Label ID="EtiIdentite" runat="server" Text="" Height="18px" Width="760px"
                               BackColor="#B0E0D7" BorderColor="White" BorderStyle="NotSet"
                               BorderWidth="1px" ForeColor="#124545" Font-Italic="True"
                               Font-Bold="True" Font-Names="Trebuchet MS" Font-Size= "Small"
                               style="text-indent: 2px; text-align: center">
                      </asp:Label>     
                </asp:TableCell>
            </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell>
                        <asp:Table ID="CadreEntete" runat="server" CellPadding="0" CellSpacing="0"
                            Width="760px" HorizontalAlign="Center">
                            <asp:TableRow>
                                <asp:TableCell>
                                    <asp:Table ID="CadreEnteteL1" runat="server" CellPadding="0" CellSpacing="0"
                                        Width="750px" HorizontalAlign="Center">
                                        <asp:TableRow>
                                            <asp:TableCell>
                                                <asp:Label ID="EtiGestion" runat="server" Height="18px" Width="150px"
                                                    BackColor="#124545" BorderColor="White" BorderStyle="NotSet" Text="CODE GESTION"
                                                    BorderWidth="1px" ForeColor="White" Font-Italic="True"
                                                    Font-Bold="True" Font-Names="Trebuchet MS" Font-Size= "Small"
                                                    style="text-indent: 2px; text-align: center">
                                                </asp:Label>
                                            </asp:TableCell>
                                            <asp:TableCell>
                                                <asp:Label ID="EtiCodePoste" runat="server" Height="18px" Width="150px"
                                                    BackColor="#124545" BorderColor="White" BorderStyle="NotSet" Text="CODE POSTE"
                                                    BorderWidth="1px" ForeColor="White" Font-Italic="True"
                                                    Font-Bold="True" Font-Names="Trebuchet MS" Font-Size= "Small"
                                                    style="text-indent: 2px; text-align: center">
                                                </asp:Label>
                                            </asp:TableCell>
                                            <asp:TableCell>
                                                <asp:Label ID="EtiLibellePoste" runat="server" Height="18px" Width="300px"
                                                    BackColor="#124545" BorderColor="White" BorderStyle="NotSet" Text="AFFECTATION"
                                                    BorderWidth="1px" ForeColor="White" Font-Italic="True"
                                                    Font-Bold="True" Font-Names="Trebuchet MS" Font-Size= "Small"
                                                    style="text-indent: 2px; text-align: center">
                                                </asp:Label>
                                            </asp:TableCell>
                                            <asp:TableCell>
                                                <asp:Label ID="EtiMisEnPaiement" runat="server" Height="18px" Width="150px"
                                                    BackColor="#124545" BorderColor="White" BorderStyle="NotSet" Text="MIS EN PAIEMENT LE"
                                                    BorderWidth="1px" ForeColor="White" Font-Italic="True"
                                                    Font-Bold="True" Font-Names="Trebuchet MS" Font-Size= "Small"
                                                    style="text-indent: 2px; text-align: center">
                                                </asp:Label>
                                            </asp:TableCell>
                                        </asp:TableRow>
                                        <asp:TableRow>
                                            <asp:TableCell>
                                                <asp:Label ID="DonGestion" runat="server" Height="18px" Width="150px"
                                                    BackColor="#B0E0D7" BorderColor="White" BorderStyle="NotSet" Text=""
                                                    BorderWidth="1px" ForeColor="#124545" Font-Italic="True"
                                                    Font-Bold="True" Font-Names="Trebuchet MS" Font-Size= "Small"
                                                    style="text-indent: 2px; text-align: center">
                                                </asp:Label>
                                            </asp:TableCell>
                                            <asp:TableCell>
                                                <asp:Label ID="DonCodePoste" runat="server" Height="18px" Width="150px"
                                                    BackColor="#B0E0D7" BorderColor="White" BorderStyle="NotSet" Text=""
                                                    BorderWidth="1px" ForeColor="#124545" Font-Italic="True"
                                                    Font-Bold="True" Font-Names="Trebuchet MS" Font-Size= "Small"
                                                    style="text-indent: 2px; text-align: center">
                                                </asp:Label>
                                            </asp:TableCell>
                                            <asp:TableCell>
                                                <asp:Label ID="DonLibellePoste" runat="server" Height="18px" Width="300px"
                                                    BackColor="#B0E0D7" BorderColor="White" BorderStyle="NotSet" Text=""
                                                    BorderWidth="1px" ForeColor="#124545" Font-Italic="True"
                                                    Font-Bold="True" Font-Names="Trebuchet MS" Font-Size= "Small"
                                                    style="text-indent: 2px; text-align: center">
                                                </asp:Label>
                                            </asp:TableCell>
                                            <asp:TableCell>
                                                <asp:Label ID="DonMisEnPaiement" runat="server" Height="18px" Width="150px"
                                                    BackColor="#B0E0D7" BorderColor="White" BorderStyle="NotSet" Text=""
                                                    BorderWidth="1px" ForeColor="#124545" Font-Italic="True"
                                                    Font-Bold="True" Font-Names="Trebuchet MS" Font-Size= "Small"
                                                    style="text-indent: 2px; text-align: center">
                                                </asp:Label>
                                            </asp:TableCell>
                                        </asp:TableRow>
                                    </asp:Table>
                                </asp:TableCell>
                            </asp:TableRow>   
                            <asp:TableRow>
                                <asp:TableCell>
                                    <asp:Table ID="CadreEnteteL2" runat="server" CellPadding="0" CellSpacing="0"
                                        Width="750px" HorizontalAlign="Center">
                                        <asp:TableRow>
                                            <asp:TableCell>
                                                <asp:Label ID="EtiNIR" runat="server" Height="18px" Width="130px"
                                                    BackColor="#124545" BorderColor="White" BorderStyle="NotSet" Text="NIR"
                                                    BorderWidth="1px" ForeColor="White" Font-Italic="True"
                                                    Font-Bold="True" Font-Names="Trebuchet MS" Font-Size= "Small"
                                                    style="text-indent: 2px; text-align: center">
                                                </asp:Label>
                                            </asp:TableCell>
                                            <asp:TableCell>
                                                <asp:Label ID="EtiClefNIR" runat="server" Height="18px" Width="40px"
                                                    BackColor="#124545" BorderColor="White" BorderStyle="NotSet" Text="CLE"
                                                    BorderWidth="1px" ForeColor="White" Font-Italic="True"
                                                    Font-Bold="True" Font-Names="Trebuchet MS" Font-Size= "Small"
                                                    style="text-indent: 2px; text-align: center">
                                                </asp:Label>
                                            </asp:TableCell>
                                            <asp:TableCell>
                                                <asp:Label ID="EtiNumDos" runat="server" Height="18px" Width="40px"
                                                    BackColor="#124545" BorderColor="White" BorderStyle="NotSet" Text="DOS"
                                                    BorderWidth="1px" ForeColor="White" Font-Italic="True"
                                                    Font-Bold="True" Font-Names="Trebuchet MS" Font-Size= "Small"
                                                    style="text-indent: 2px; text-align: center">
                                                </asp:Label>
                                            </asp:TableCell>
                                            <asp:TableCell>
                                                <asp:Label ID="EtiGrade" runat="server" Height="18px" Width="320px"
                                                    BackColor="#124545" BorderColor="White" BorderStyle="NotSet" Text="GRADE"
                                                    BorderWidth="1px" ForeColor="White" Font-Italic="True"
                                                    Font-Bold="True" Font-Names="Trebuchet MS" Font-Size= "Small"
                                                    style="text-indent: 2px; text-align: center">
                                                </asp:Label>
                                            </asp:TableCell>
                                            <asp:TableCell>
                                                <asp:Label ID="EtiEchelon" runat="server" Height="18px" Width="40px"
                                                    BackColor="#124545" BorderColor="White" BorderStyle="NotSet" Text="ECH."
                                                    BorderWidth="1px" ForeColor="White" Font-Italic="True"
                                                    Font-Bold="True" Font-Names="Trebuchet MS" Font-Size= "Small"
                                                    style="text-indent: 2px; text-align: center">
                                                </asp:Label>
                                            </asp:TableCell>
                                            <asp:TableCell>
                                                <asp:Label ID="EtiIndice" runat="server" Height="18px" Width="40px"
                                                    BackColor="#124545" BorderColor="White" BorderStyle="NotSet" Text="IND."
                                                    BorderWidth="1px" ForeColor="White" Font-Italic="True"
                                                    Font-Bold="True" Font-Names="Trebuchet MS" Font-Size= "Small"
                                                    style="text-indent: 2px; text-align: center">
                                                </asp:Label>
                                            </asp:TableCell>
                                            <asp:TableCell>
                                                <asp:Label ID="EtiNBI" runat="server" Height="18px" Width="40px"
                                                    BackColor="#124545" BorderColor="White" BorderStyle="NotSet" Text="NBI"
                                                    BorderWidth="1px" ForeColor="White" Font-Italic="True"
                                                    Font-Bold="True" Font-Names="Trebuchet MS" Font-Size= "Small"
                                                    style="text-indent: 2px; text-align: center">
                                                </asp:Label>
                                            </asp:TableCell>
                                            <asp:TableCell>
                                                <asp:Label ID="EtiEnfants" runat="server" Height="18px" Width="40px"
                                                    BackColor="#124545" BorderColor="White" BorderStyle="NotSet" Text="ENF."
                                                    BorderWidth="1px" ForeColor="White" Font-Italic="True"
                                                    Font-Bold="True" Font-Names="Trebuchet MS" Font-Size= "Small"
                                                    style="text-indent: 2px; text-align: center">
                                                </asp:Label>
                                            </asp:TableCell>
                                            <asp:TableCell>
                                                <asp:Label ID="EtiQuotite" runat="server" Height="18px" Width="50px"
                                                    BackColor="#124545" BorderColor="White" BorderStyle="NotSet" Text="QUOT"
                                                    BorderWidth="1px" ForeColor="White" Font-Italic="True"
                                                    Font-Bold="True" Font-Names="Trebuchet MS" Font-Size= "Small"
                                                    style="text-indent: 2px; text-align: center">
                                                </asp:Label>
                                            </asp:TableCell>
                                         </asp:TableRow>
                                         <asp:TableRow>
                                            <asp:TableCell>
                                                <asp:Label ID="DonNIR" runat="server" Height="18px" Width="130px"
                                                    BackColor="#B0E0D7" BorderColor="White" BorderStyle="NotSet" Text=""
                                                    BorderWidth="1px" ForeColor="#124545" Font-Italic="True"
                                                    Font-Bold="True" Font-Names="Trebuchet MS" Font-Size= "Small"
                                                    style="text-indent: 2px; text-align: center">
                                                </asp:Label>
                                            </asp:TableCell>
                                            <asp:TableCell>
                                                <asp:Label ID="DonClefNIR" runat="server" Height="18px" Width="40px"
                                                    BackColor="#B0E0D7" BorderColor="White" BorderStyle="NotSet" Text=""
                                                    BorderWidth="1px" ForeColor="#124545" Font-Italic="True"
                                                    Font-Bold="True" Font-Names="Trebuchet MS" Font-Size= "Small"
                                                    style="text-indent: 2px; text-align: center">
                                                </asp:Label>
                                            </asp:TableCell>
                                            <asp:TableCell>
                                                <asp:Label ID="DonNumDos" runat="server" Height="18px" Width="40px"
                                                    BackColor="#B0E0D7" BorderColor="White" BorderStyle="NotSet" Text=""
                                                    BorderWidth="1px" ForeColor="#124545" Font-Italic="True"
                                                    Font-Bold="True" Font-Names="Trebuchet MS" Font-Size= "Small"
                                                    style="text-indent: 2px; text-align: center">
                                                </asp:Label>
                                            </asp:TableCell>
                                            <asp:TableCell>
                                                <asp:Label ID="DonGrade" runat="server" Height="18px" Width="320px"
                                                    BackColor="#B0E0D7" BorderColor="White" BorderStyle="NotSet" Text=""
                                                    BorderWidth="1px" ForeColor="#124545" Font-Italic="True"
                                                    Font-Bold="True" Font-Names="Trebuchet MS" Font-Size= "Small"
                                                    style="text-indent: 2px; text-align: center">
                                                </asp:Label>
                                            </asp:TableCell>
                                             <asp:TableCell>
                                                <asp:Label ID="DonEchelon" runat="server" Height="18px" Width="40px"
                                                    BackColor="#B0E0D7" BorderColor="White" BorderStyle="NotSet" Text=""
                                                    BorderWidth="1px" ForeColor="#124545" Font-Italic="True"
                                                    Font-Bold="True" Font-Names="Trebuchet MS" Font-Size= "Small"
                                                    style="text-indent: 2px; text-align: center">
                                                </asp:Label>
                                            </asp:TableCell>
                                             <asp:TableCell>
                                                <asp:Label ID="DonIndice" runat="server" Height="18px" Width="40px"
                                                    BackColor="#B0E0D7" BorderColor="White" BorderStyle="NotSet" Text=""
                                                    BorderWidth="1px" ForeColor="#124545" Font-Italic="True"
                                                    Font-Bold="True" Font-Names="Trebuchet MS" Font-Size= "Small"
                                                    style="text-indent: 2px; text-align: center">
                                                </asp:Label>
                                            </asp:TableCell>
                                             <asp:TableCell>
                                                <asp:Label ID="DonNBI" runat="server" Height="18px" Width="40px"
                                                    BackColor="#B0E0D7" BorderColor="White" BorderStyle="NotSet" Text=""
                                                    BorderWidth="1px" ForeColor="#124545" Font-Italic="True"
                                                    Font-Bold="True" Font-Names="Trebuchet MS" Font-Size= "Small"
                                                    style="text-indent: 2px; text-align: center">
                                                </asp:Label>
                                            </asp:TableCell>
                                             <asp:TableCell>
                                                <asp:Label ID="DonEnfants" runat="server" Height="18px" Width="40px"
                                                    BackColor="#B0E0D7" BorderColor="White" BorderStyle="NotSet" Text=""
                                                    BorderWidth="1px" ForeColor="#124545" Font-Italic="True"
                                                    Font-Bold="True" Font-Names="Trebuchet MS" Font-Size= "Small"
                                                    style="text-indent: 2px; text-align: center">
                                                </asp:Label>
                                            </asp:TableCell>
                                             <asp:TableCell>
                                                <asp:Label ID="DonQuotite" runat="server" Height="18px" Width="50px"
                                                    BackColor="#B0E0D7" BorderColor="White" BorderStyle="NotSet" Text=""
                                                    BorderWidth="1px" ForeColor="#124545" Font-Italic="True"
                                                    Font-Bold="True" Font-Names="Trebuchet MS" Font-Size= "Small"
                                                    style="text-indent: 2px; text-align: center">
                                                </asp:Label>
                                            </asp:TableCell>
                                        </asp:TableRow>
                                    </asp:Table>
                                </asp:TableCell>
                            </asp:TableRow>
                        </asp:Table>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell>
                        <Virtualia:VListeGrid ID="GridBulletin" runat="server" CadreWidth="760px"
                            BackColorCaption="#124545" BackColorRow="#B0E0D7" SiPagination="False" SiCaseAcocher="false"
                            SiColonneSelect="False" SiCaptionVisible="False" SiStyleBulletin="True" TaillePage="200" />
                    </asp:TableCell>
                </asp:TableRow>
            </asp:Table>
        </asp:TableCell>
    </asp:TableRow>
</asp:Table>