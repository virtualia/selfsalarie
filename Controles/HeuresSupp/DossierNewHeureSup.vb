﻿Option Explicit On
Option Strict On
Option Compare Text
Imports VI = Virtualia.Systeme.Constantes
Namespace WebAppli
    Namespace SelfV4
        Public Class DossierNewHeureSup
            Private WsParent As Virtualia.Net.WebAppli.SelfV4.DossierPersonne
            Private WsLstDetailHS As List(Of Virtualia.TablesObjet.ShemaPER.PER_DETAIL_HS)
            Private WsLstHeuresSup As List(Of Virtualia.TablesObjet.ShemaPER.PER_MENSUEL_HS)
            Private WsLstRecuperation As List(Of Virtualia.TablesObjet.ShemaPER.PER_ABSENCE)
            Private WsLstPlanning As List(Of Virtualia.TablesObjet.ShemaPER.PER_TRAVAIL)
            Private WsLstBadgeages As List(Of Virtualia.TablesObjet.ShemaPER.PER_POINTAGE)
            Private WsLstSemaines As List(Of Virtualia.TablesObjet.ShemaPER.PER_CET_HEBDO)
            Private WsFicheDomicile As Virtualia.TablesObjet.ShemaPER.PER_DOMICILE = Nothing
            Private WsLstDroits As List(Of Virtualia.TablesObjet.ShemaPER.PER_DROIT_CONGES)
            Private WsLstHSRemunerees As List(Of Virtualia.TablesObjet.ShemaPER.PER_HEURESUP)
            '
            Private WsSoldeAnterieur As Double
            Private WsTotalHeuresSup As Double
            Private WsTotalRemunerees As Double
            Private WsTotalCompensables As Double
            Private WsTotalRecuperations As Double
            Private WsTotalRecupEnCours As Double
            Private WsSolde As Double

            Public Sub CreerDetailJournalier(ByVal Annee As Integer, ByVal Mois As Integer)
                Dim Fiche_DetailLue As Virtualia.TablesObjet.ShemaPER.PER_DETAIL_HS
                Dim Fiche_Badgeage As Virtualia.TablesObjet.ShemaPER.PER_POINTAGE
                Dim Fiche_Semaine As Virtualia.TablesObjet.ShemaPER.PER_CET_HEBDO
                Dim LstBadgeage As List(Of Virtualia.TablesObjet.ShemaPER.PER_POINTAGE)
                Dim LstSemaine As List(Of Virtualia.TablesObjet.ShemaPER.PER_CET_HEBDO)
                Dim LstDetail As List(Of Virtualia.TablesObjet.ShemaPER.PER_DETAIL_HS) = ListeDetaille_HS(Annee, Mois)
                Dim HeureBadgee As System.DateTime
                Dim HeureLissee As System.DateTime
                Dim HeureBornee As System.DateTime
                Dim IndiceJ As Integer
                Dim IndiceP As Integer
                Dim DateW As String

                '** 1) A partir des badgeages
                LstBadgeage = ListeDesBadgeages(Annee, Mois)
                If LstBadgeage IsNot Nothing Then
                    For Each Fiche_Badgeage In LstBadgeage
                        'MsgBox(Fiche_Badgeage.Date_de_Valeur)
                        Fiche_DetailLue = Nothing
                        HeureLissee = WsParent.VParent.VirRhDates.HeureTypee(Fiche_Badgeage.Date_de_Valeur, "")
                        HeureBadgee = HeureLissee
                        If WsParent.VParent.VirRhDates.SiJourOuvre(Fiche_Badgeage.Date_de_Valeur, True) = True Then
                            '**Heures Supp
                            If Fiche_Badgeage.Lissage_N4 <> "" Then
                                HeureLissee = Fiche_Badgeage.V_HeureLissee(4)
                                HeureBadgee = Fiche_Badgeage.V_HeureBadgee(4)
                            ElseIf Fiche_Badgeage.Lissage_N2 <> "" Then
                                HeureLissee = Fiche_Badgeage.V_HeureLissee(2)
                                HeureBadgee = Fiche_Badgeage.V_HeureBadgee(2)
                            End If
                            '*********AKR Protection bug Intranet V3 qui remet à blanc les 4 horaires lissés de PER_POINTAGE quand les Horaires ont été modifiés par le manager
                            HeureBornee = BorneTypee(Fiche_Badgeage.Date_de_Valeur)
                            If Fiche_Badgeage.Lissage_N1 = "" And Fiche_Badgeage.Lissage_N2 = "" _
                               And Fiche_Badgeage.Lissage_N3 = "" And Fiche_Badgeage.Lissage_N4 = "" Then
                                If Fiche_Badgeage.Badgeage_N4 <> "" Then
                                    HeureLissee = HeureBornee
                                    HeureBadgee = Fiche_Badgeage.V_HeureBadgee(4)
                                ElseIf Fiche_Badgeage.Badgeage_N3 <> "" Then
                                    HeureLissee = HeureBornee
                                    HeureBadgee = Fiche_Badgeage.V_HeureBadgee(3)
                                ElseIf Fiche_Badgeage.Badgeage_N2 <> "" Then
                                    HeureLissee = HeureBornee
                                    HeureBadgee = Fiche_Badgeage.V_HeureBadgee(2)
                                ElseIf Fiche_Badgeage.Badgeage_N1 <> "" Then
                                    HeureLissee = HeureBornee
                                    HeureBadgee = Fiche_Badgeage.V_HeureBadgee(1)
                                End If
                            End If
                        Else
                            '** Astreinte
                            HeureLissee = Fiche_Badgeage.V_HeureBadgee(1)
                            If Fiche_Badgeage.Lissage_N4 <> "" Then
                                HeureBadgee = Fiche_Badgeage.V_HeureBadgee(4)
                            ElseIf Fiche_Badgeage.Lissage_N2 <> "" Then
                                HeureBadgee = Fiche_Badgeage.V_HeureBadgee(2)
                            End If
                        End If
                        If HeureBadgee > HeureLissee Then
                            If LstDetail IsNot Nothing Then
                                Fiche_DetailLue = LstDetail.Find(Function(Recherche) Recherche.Date_de_Valeur = Fiche_Badgeage.Date_de_Valeur)
                            End If
                            Call ConstruireFicheDetail_HS(Fiche_DetailLue, Fiche_Badgeage.Date_de_Valeur, HeureBadgee, HeureLissee)
                        End If
                    Next
                End If
                '** 2) Eventuellement compléter par Semaines constatées
                LstSemaine = ListeDesSemainesTravaillees(Annee, Mois)
                If LstSemaine Is Nothing Then
                    Exit Sub
                End If
                For Each Fiche_Semaine In LstSemaine
                    HeureBornee = BorneTypee(Fiche_Semaine.Date_de_Valeur)
                    For IndiceJ = 0 To 6
                        DateW = Fiche_Semaine.Date_Valeur_ToDate.AddDays(IndiceJ).ToShortDateString
                        HeureLissee = HeureBornee.AddDays(IndiceJ)
                        If WsParent.VParent.VirRhDates.SiJourOuvre(DateW, True) = False Then
                            '**Astreinte
                            HeureLissee = Fiche_Semaine.V_HeureDeDebutPlage(IndiceJ, 0)
                        End If
                        Fiche_DetailLue = Nothing
                        HeureBadgee = WsParent.VParent.VirRhDates.HeureTypee(DateW, "")
                        For IndiceP = 7 To 0 Step -1
                            If Fiche_Semaine.HeureDeFinPlage(IndiceJ, IndiceP) <> "" Then
                                HeureBadgee = Fiche_Semaine.V_HeureDeFinPlage(IndiceJ, IndiceP)
                                Exit For
                            End If
                        Next IndiceP
                        If HeureBadgee > HeureLissee Then
                            If LstDetail IsNot Nothing Then
                                Fiche_DetailLue = LstDetail.Find(Function(Recherche) Recherche.Date_de_Valeur = DateW)
                            End If
                            Call ConstruireFicheDetail_HS(Fiche_DetailLue, DateW, HeureBadgee, HeureLissee)
                        End If
                    Next
                Next
                '** Mettre à jour la situation mensuelle
                Call MettreAjourSituationMensuelle(Annee, Mois)
            End Sub

            Public Sub MettreAjourSituationMensuelle(ByVal Annee As Integer, ByVal Mois As Integer)
                Dim LstDetail As List(Of Virtualia.TablesObjet.ShemaPER.PER_DETAIL_HS)
                Dim FicheMois As Virtualia.TablesObjet.ShemaPER.PER_MENSUEL_HS = FicheMensuel_HS(Annee, Mois)
                Dim Total_Jour_Jusqua20 As Double
                Dim Total_Jour_De20a22 As Double
                Dim Total_Nuit As Double
                Dim Total_HS_Trajet As Double
                Dim Total_JF_Jusqua20 As Double
                Dim Total_JF_De20a22 As Double
                Dim Total_JF_Nuit As Double
                Dim Total_JF_Trajet As Double
                Dim CodeMaj As String = "M"
                Dim SiOK As Boolean

                LstDetail = ListeDetaille_HS_Validee(Annee, Mois)
                If LstDetail Is Nothing OrElse LstDetail.Count = 0 Then
                    If FicheMois IsNot Nothing Then
                        SiOK = WsParent.VParent.VirServiceServeur.MiseAjour_Fiche(WsParent.VParent.VirNomUtilisateur, VI.PointdeVue.PVueApplicatif,
                                                                   VI.ObjetPer.ObaMensuelHS, WsParent.Ide_Dossier, "S", FicheMois.ContenuTable, "", True)
                        If SiOK = True AndAlso WsLstHeuresSup IsNot Nothing Then
                            Try
                                WsLstHeuresSup.Remove(FicheMois)
                            Catch ex As Exception
                                Exit Try
                            End Try
                        End If
                    End If
                    Exit Sub
                End If
                For Each Fiche_DetailLue In LstDetail
                    If Fiche_DetailLue.FaitGenerateur <> "" AndAlso Fiche_DetailLue.FaitGenerateur.Contains("Aucun") = False Then
                        Select Case Fiche_DetailLue.SiJourOuvrable
                            Case True
                                Total_Jour_Jusqua20 += Fiche_DetailLue.Nbheures_Jusqua20h
                                Total_Jour_De20a22 += Fiche_DetailLue.Nbheures_De20ha22h
                                Total_Nuit += Fiche_DetailLue.Nbheures_Nuit
                                Total_HS_Trajet += Fiche_DetailLue.Nbheures_Trajet
                            Case False
                                Total_JF_Jusqua20 += Fiche_DetailLue.Nbheures_Jusqua20h
                                Total_JF_De20a22 += Fiche_DetailLue.Nbheures_De20ha22h
                                Total_JF_Nuit += Fiche_DetailLue.Nbheures_Nuit
                                Total_JF_Trajet += Fiche_DetailLue.Nbheures_Trajet
                        End Select
                    End If
                Next
                If FicheMois Is Nothing Then
                    FicheMois = New Virtualia.TablesObjet.ShemaPER.PER_MENSUEL_HS
                    FicheMois.Ide_Dossier = WsParent.Ide_Dossier
                    FicheMois.Date_de_Valeur = "01/" & Strings.Format(Mois, "00") & "/" & Annee
                    CodeMaj = "C"
                ElseIf FicheMois.FicheLue = "" Then
                    FicheMois.ContenuTable = FicheMois.Ide_Dossier & VI.Tild & FicheMois.ContenuTable
                End If
                FicheMois.TotalHeures_Jour_Jusqua20h = Total_Jour_Jusqua20
                FicheMois.TotalHeures_Jour_de20a22h = Total_Jour_De20a22
                FicheMois.TotalHeures_Nuit = Total_Nuit
                FicheMois.TotalHeures_Trajet_HS = Total_HS_Trajet
                FicheMois.TotalHeures_Astreinte_Jusqua20h = Total_JF_Jusqua20
                FicheMois.TotalHeures_Astreinte_de20a22h = Total_JF_De20a22
                FicheMois.TotalHeures_Astreinte_Nuit = Total_JF_Nuit
                FicheMois.TotalHeures_Trajet_Astreinte = Total_JF_Trajet
                If CodeMaj = "C" Then
                    SiOK = WsParent.VParent.VirServiceServeur.MiseAjour_Fiche(WsParent.VParent.VirNomUtilisateur, VI.PointdeVue.PVueApplicatif,
                                                                   VI.ObjetPer.ObaMensuelHS, WsParent.Ide_Dossier, "C", "", FicheMois.ContenuTable, True)
                    If SiOK = True Then
                        If WsLstHeuresSup Is Nothing Then
                            WsLstHeuresSup = New List(Of Virtualia.TablesObjet.ShemaPER.PER_MENSUEL_HS)
                        End If
                        WsLstHeuresSup.Add(FicheMois)
                    End If
                ElseIf (FicheMois.FicheLue <> FicheMois.ContenuTable) Then
                    SiOK = WsParent.VParent.VirServiceServeur.MiseAjour_Fiche(WsParent.VParent.VirNomUtilisateur, VI.PointdeVue.PVueApplicatif,
                                                                    VI.ObjetPer.ObaMensuelHS, WsParent.Ide_Dossier, "M", FicheMois.FicheLue, FicheMois.ContenuTable, True)
                End If
            End Sub

            Public ReadOnly Property TotalCreditHeuresSup(ByVal Annee As Integer) As String
                Get
                    If WsLstHeuresSup Is Nothing OrElse WsLstHeuresSup.Count = 0 Then
                        Return ""
                    End If
                    Dim TotalHSAnte As Double = 0
                    Dim TotalRecupAnte As Double = 0
                    Dim FichePERDroits As Virtualia.TablesObjet.ShemaPER.PER_DROIT_CONGES
                    Dim SiOK As Boolean
                    Dim AnneeReprise As Integer = 2016
                    If System.Configuration.ConfigurationManager.AppSettings("HS_AnneeReprise") IsNot Nothing AndAlso IsNumeric(System.Configuration.ConfigurationManager.AppSettings("HS_AnneeReprise")) = True Then
                        AnneeReprise = CInt(System.Configuration.ConfigurationManager.AppSettings("HS_AnneeReprise"))
                    End If
                    WsTotalHeuresSup = 0
                    WsTotalRemunerees = 0
                    WsTotalRecuperations = 0
                    WsTotalCompensables = 0
                    WsTotalRecupEnCours = 0
                    WsSolde = 0
                    WsSoldeAnterieur = 0

                    FichePERDroits = FicheDroitsConge(AnneeReprise)
                    If FichePERDroits IsNot Nothing AndAlso FichePERDroits.Droit_ReposCompensateur <> "" Then
                        Try
                            TotalHSAnte = CInt(WsParent.VParent.VirRhDates.CalcHeure(FichePERDroits.Droit_ReposCompensateur, "", 1))
                        Catch ex As Exception
                            TotalHSAnte = 0
                        End Try
                    End If

                    For Each ElementMensuel In WsLstHeuresSup
                        If ElementMensuel.Date_Valeur_ToDate.Year < Annee Then
                            TotalHSAnte += ElementMensuel.TotalHeuresCompensables(False) * 60
                        End If
                        If ElementMensuel.Date_Valeur_ToDate.Year = Annee Then
                            WsTotalHeuresSup += ElementMensuel.TotalHeuresCompensables(False) * 60
                            WsTotalRemunerees += ElementMensuel.TotalHeuresRemunerees(False) * 60
                        End If
                        If ElementMensuel.Date_Valeur_ToDate.AddMonths(1).Month = System.DateTime.Now.Month Or
                            ElementMensuel.Date_Valeur_ToDate.AddMonths(2).Month = System.DateTime.Now.Month Then
                            WsTotalCompensables += ElementMensuel.TotalHeuresCompensables(True) * 60
                        End If
                    Next
                    If WsLstRecuperation IsNot Nothing Then
                        For Each FichePER In WsLstRecuperation
                            If FichePER.Date_Valeur_ToDate.Year < Annee Then
                                TotalRecupAnte += FichePER.JoursOuvres * HoraireJournalier(FichePER.Date_de_Valeur)
                            End If
                            If FichePER.Date_Valeur_ToDate.Year = Annee Then
                                WsTotalRecuperations += FichePER.JoursOuvres * HoraireJournalier(FichePER.Date_de_Valeur)
                                If FichePER.Date_Valeur_ToDate.AddMonths(1).Month = System.DateTime.Now.Month Or
                                    FichePER.Date_Valeur_ToDate.AddMonths(2).Month = System.DateTime.Now.Month Then
                                    WsTotalRecupEnCours += FichePER.JoursOuvres * HoraireJournalier(FichePER.Date_de_Valeur)
                                End If
                            End If
                        Next
                    End If
                    WsSoldeAnterieur = TotalHSAnte - TotalRecupAnte
                    Select Case System.DateTime.Now.Month
                        Case 1, 2
                            WsSolde = WsTotalCompensables - WsTotalRecupEnCours + WsSoldeAnterieur
                        Case Else
                            WsSolde = WsTotalCompensables - WsTotalRecupEnCours
                    End Select

                    FichePERDroits = FicheDroitsConge(Annee)
                    If FichePERDroits Is Nothing Then
                        Return WsParent.VParent.VirRhDates.CalcHeure(CStr(CInt(WsTotalHeuresSup)), "", 2)
                    End If
                    If FichePERDroits.Droit_ReposCompensateur <> WsParent.VParent.VirRhDates.CalcHeure(CStr(CInt(WsSolde)), "", 2) Then
                        If WsSolde > 0 Then
                            FichePERDroits.Droit_ReposCompensateur = WsParent.VParent.VirRhDates.CalcHeure(CStr(CInt(WsSolde)), "", 2)
                        Else
                            FichePERDroits.Droit_ReposCompensateur = ""
                        End If
                        SiOK = WsParent.VParent.VirServiceServeur.MiseAjour_Fiche(WsParent.VParent.VirNomUtilisateur, VI.PointdeVue.PVueApplicatif,
                                                                    VI.ObjetPer.ObaDroits, WsParent.Ide_Dossier, "M", FichePERDroits.FicheLue, FichePERDroits.ContenuTable, True)
                    End If

                    Return WsParent.VParent.VirRhDates.CalcHeure(CStr(CInt(WsTotalHeuresSup)), "", 2)
                End Get
            End Property

            Public ReadOnly Property TotalRecuperations As String
                Get
                    If WsTotalRecuperations <= 0 Then
                        Return ""
                    End If
                    Return WsParent.VParent.VirRhDates.CalcHeure(CStr(CInt(WsTotalRecuperations)), "", 2)
                End Get
            End Property

            Public ReadOnly Property RecuperationsCourantes As String
                Get
                    If WsTotalRecupEnCours <= 0 Then
                        Return ""
                    End If
                    Return WsParent.VParent.VirRhDates.CalcHeure(CStr(CInt(WsTotalRecupEnCours)), "", 2)
                End Get
            End Property
            Public ReadOnly Property Solde_HS As String
                Get
                    If WsSolde <= 0 Then
                        Return ""
                    End If
                    Return WsParent.VParent.VirRhDates.CalcHeure(CStr(CInt(WsSolde)), "", 2)
                End Get
            End Property

            Public ReadOnly Property Solde_HS_Anterieur As String
                Get
                    If WsSoldeAnterieur <= 0 Then
                        Return ""
                    End If
                    Return WsParent.VParent.VirRhDates.CalcHeure(CStr(CInt(WsSoldeAnterieur)), "", 2)
                End Get
            End Property

            Public ReadOnly Property TotalRemunerees As String
                Get
                    If WsTotalRemunerees <= 0 Then
                        Return ""
                    End If
                    Return WsParent.VParent.VirRhDates.CalcHeure(CStr(CInt(WsTotalRemunerees)), "", 2)
                End Get
            End Property

            Public ReadOnly Property TotalCompensables As String
                Get
                    If WsTotalCompensables <= 0 Then
                        Return ""
                    End If
                    Return WsParent.VParent.VirRhDates.CalcHeure(CStr(CInt(WsTotalCompensables)), "", 2)
                End Get
            End Property

            Public ReadOnly Property ListeDetaille_HS(ByVal Annee As Integer, ByVal Mois As Integer) As List(Of Virtualia.TablesObjet.ShemaPER.PER_DETAIL_HS)
                Get
                    If WsLstDetailHS Is Nothing Then
                        Return Nothing
                    End If
                    Dim SousListe As List(Of Virtualia.TablesObjet.ShemaPER.PER_DETAIL_HS)
                    SousListe = (From FichePER In WsLstDetailHS Where FichePER.Date_Valeur_ToDate.Year = Annee _
                                  And FichePER.Date_Valeur_ToDate.Month = Mois Order By FichePER.Date_Valeur_ToDate Ascending).ToList
                    If SousListe Is Nothing OrElse SousListe.Count = 0 Then
                        Return Nothing
                    End If
                    Return SousListe
                End Get
            End Property

            Public ReadOnly Property ListeDetaille_HS(ByVal Annee As Integer) As List(Of Virtualia.TablesObjet.ShemaPER.PER_DETAIL_HS)
                Get
                    If WsLstDetailHS Is Nothing Then
                        Return Nothing
                    End If
                    Dim SousListe As List(Of Virtualia.TablesObjet.ShemaPER.PER_DETAIL_HS)
                    SousListe = (From FichePER In WsLstDetailHS Where FichePER.Date_Valeur_ToDate.Year = Annee Order By FichePER.Date_Valeur_ToDate Ascending).ToList
                    If SousListe Is Nothing OrElse SousListe.Count = 0 Then
                        Return Nothing
                    End If
                    Return SousListe
                End Get
            End Property

            Public ReadOnly Property ListeDetaille_HS_Validee(ByVal Annee As Integer, ByVal Mois As Integer) As List(Of Virtualia.TablesObjet.ShemaPER.PER_DETAIL_HS)
                Get
                    If WsLstDetailHS Is Nothing Then
                        Return Nothing
                    End If
                    Dim SousListe As List(Of Virtualia.TablesObjet.ShemaPER.PER_DETAIL_HS)
                    SousListe = (From FichePER In WsLstDetailHS Where FichePER.Date_Valeur_ToDate.Year = Annee _
                                  And FichePER.Date_Valeur_ToDate.Month = Mois And FichePER.FaitGenerateur <> "" And FichePER.FaitGenerateur.Contains("Aucun") = False _
                                  And FichePER.Date_Validation <> "" Order By FichePER.Date_Valeur_ToDate Ascending).ToList
                    If SousListe Is Nothing OrElse SousListe.Count = 0 Then
                        Return Nothing
                    End If
                    Return SousListe
                End Get
            End Property

            Public ReadOnly Property FicheDetail_HS(ByVal DateValeur As String) As Virtualia.TablesObjet.ShemaPER.PER_DETAIL_HS
                Get
                    If WsLstDetailHS Is Nothing OrElse WsLstDetailHS Is Nothing Then
                        Return Nothing
                    End If
                    Try
                        Return (From FichePER In WsLstDetailHS Where FichePER.Date_de_Valeur = DateValeur).First
                    Catch ex As Exception
                        Return Nothing
                    End Try
                End Get
            End Property

            Public Property Donnee_FicheMensuel_HS(ByVal NumInfo As Integer, ByVal DateValeur As String) As String
                Get
                    If WsLstHeuresSup Is Nothing OrElse WsLstHeuresSup.Count = 0 Then
                        Return ""
                    End If
                    Dim FichePER As Virtualia.TablesObjet.ShemaPER.PER_MENSUEL_HS
                    Try
                        FichePER = (From instance In WsLstHeuresSup Where instance.Date_de_Valeur = DateValeur).First
                    Catch ex As Exception
                        Return ""
                    End Try
                    Return FichePER.V_TableauData(NumInfo).ToString
                End Get
                Set(value As String)
                    Dim FichePER As Virtualia.TablesObjet.ShemaPER.PER_MENSUEL_HS = Nothing
                    Dim ConstructeurFiche As Virtualia.TablesObjet.ShemaPER.VConstructeur
                    Dim TableauData As ArrayList
                    If WsLstHeuresSup Is Nothing OrElse WsLstHeuresSup.Count = 0 Then
                        FichePER = Nothing
                        WsLstHeuresSup = New List(Of Virtualia.TablesObjet.ShemaPER.PER_MENSUEL_HS)
                    Else
                        Try
                            FichePER = (From instance In WsLstHeuresSup Where instance.Date_de_Valeur = DateValeur).First
                        Catch ex As Exception
                            FichePER = Nothing
                        End Try
                    End If
                    If FichePER Is Nothing Then
                        ConstructeurFiche = New Virtualia.TablesObjet.ShemaPER.VConstructeur(WsParent.VParent.VirModele.InstanceProduit.ClefModele)
                        FichePER = CType(ConstructeurFiche.V_NouvelleFiche(VI.ObjetPer.ObaMensuelHS, WsParent.Ide_Dossier), Virtualia.TablesObjet.ShemaPER.PER_MENSUEL_HS)
                        WsLstHeuresSup.Add(FichePER)
                    End If
                    TableauData = FichePER.V_TableauData
                    TableauData(NumInfo) = value
                    FichePER.V_TableauData = TableauData
                End Set
            End Property

            Public ReadOnly Property FicheMensuel_HS(ByVal Annee As Integer, ByVal Mois As Integer) As Virtualia.TablesObjet.ShemaPER.PER_MENSUEL_HS
                Get
                    If WsLstHeuresSup Is Nothing Then
                        Return Nothing
                    End If
                    Try
                        Return (From FichePER In WsLstHeuresSup Where FichePER.Date_Valeur_ToDate.Year = Annee And FichePER.Date_Valeur_ToDate.Month = Mois).First
                    Catch ex As Exception
                        Return Nothing
                    End Try
                End Get
            End Property

            Public ReadOnly Property ListeMensuel_HS(ByVal Annee As Integer) As List(Of Virtualia.TablesObjet.ShemaPER.PER_MENSUEL_HS)
                Get
                    If WsLstHeuresSup Is Nothing Then
                        Return Nothing
                    End If
                    Dim SousListe As List(Of Virtualia.TablesObjet.ShemaPER.PER_MENSUEL_HS)
                    SousListe = (From FichePER In WsLstHeuresSup Where FichePER.Date_Valeur_ToDate.Year = Annee Order By FichePER.Date_Valeur_ToDate Ascending).ToList
                    If SousListe Is Nothing OrElse SousListe.Count = 0 Then
                        Return Nothing
                    End If
                    Return SousListe
                End Get
            End Property

            Public ReadOnly Property FicheHs_Remuneree(ByVal Annee As Integer, ByVal Mois As Integer) As Virtualia.TablesObjet.ShemaPER.PER_HEURESUP
                Get
                    If WsLstHSRemunerees Is Nothing Then
                        Return Nothing
                    End If
                    Try
                        Return (From FichePER In WsLstHSRemunerees Where FichePER.Date_Valeur_ToDate.Year = Annee And FichePER.Date_Valeur_ToDate.Month = Mois).First
                    Catch ex As Exception
                        Return Nothing
                    End Try
                End Get
            End Property

            Public ReadOnly Property ListeHS_Remunerees(ByVal Annee As Integer) As List(Of Virtualia.TablesObjet.ShemaPER.PER_HEURESUP)
                Get
                    If WsLstHSRemunerees Is Nothing Then
                        Return Nothing
                    End If
                    Dim SousListe As List(Of Virtualia.TablesObjet.ShemaPER.PER_HEURESUP)
                    SousListe = (From FichePER In WsLstHSRemunerees Where FichePER.Date_Valeur_ToDate.Year = Annee Order By FichePER.Date_Valeur_ToDate Ascending).ToList
                    If SousListe Is Nothing OrElse SousListe.Count = 0 Then
                        Return Nothing
                    End If
                    Return SousListe
                End Get
            End Property

            Public ReadOnly Property ListeRecuperations(ByVal Annee As Integer, ByVal Mois As Integer) As List(Of Virtualia.TablesObjet.ShemaPER.PER_ABSENCE)
                Get
                    If WsLstRecuperation Is Nothing Then
                        Return Nothing
                    End If
                    Dim DateFin As Date
                    Dim DateDebut As Date
                    Dim SousListe As List(Of Virtualia.TablesObjet.ShemaPER.PER_ABSENCE)
                    DateFin = WsParent.VParent.VirRhDates.DateTypee(WsParent.VParent.VirRhDates.DateSaisieVerifiee("31/" & Strings.Format(Mois, "00") & "/" & Annee))
                    If Mois = 1 Then
                        Annee -= 1
                        Mois = 12
                    Else
                        Mois -= 1
                    End If
                    DateDebut = WsParent.VParent.VirRhDates.DateTypee("01/" & Strings.Format(Mois, "00") & "/" & Annee)
                    SousListe = (From FichePER In WsLstRecuperation Where (FichePER.Date_Valeur_ToDate >= DateDebut And FichePER.Date_Valeur_ToDate <= DateFin) _
                                Or (FichePER.Date_Fin_ToDate >= DateDebut And FichePER.Date_Fin_ToDate <= DateFin) Order By FichePER.Date_Valeur_ToDate Ascending).ToList
                    If SousListe Is Nothing OrElse SousListe.Count = 0 Then
                        Return Nothing
                    End If
                    Return SousListe
                End Get
            End Property

            Public ReadOnly Property ListeRecuperations(ByVal Annee As Integer) As List(Of Virtualia.TablesObjet.ShemaPER.PER_ABSENCE)
                Get
                    If WsLstRecuperation Is Nothing Then
                        Return Nothing
                    End If
                    Dim SousListe As List(Of Virtualia.TablesObjet.ShemaPER.PER_ABSENCE)
                    SousListe = (From FichePER In WsLstRecuperation Where FichePER.Date_Valeur_ToDate.Year = Annee Order By FichePER.Date_Valeur_ToDate Ascending).ToList
                    If SousListe Is Nothing OrElse SousListe.Count = 0 Then
                        Return Nothing
                    End If
                    Return SousListe
                End Get
            End Property

            Public ReadOnly Property ListeDesBadgeages(ByVal Annee As Integer, ByVal Mois As Integer) As List(Of Virtualia.TablesObjet.ShemaPER.PER_POINTAGE)
                Get
                    If WsLstBadgeages Is Nothing Then
                        Return Nothing
                    End If
                    Dim SousListe As List(Of Virtualia.TablesObjet.ShemaPER.PER_POINTAGE)
                    SousListe = (From FichePER In WsLstBadgeages Where FichePER.Date_Valeur_ToDate.Year = Annee _
                                              And FichePER.Date_Valeur_ToDate.Month = Mois Order By FichePER.Date_Valeur_ToDate Ascending).ToList
                    If SousListe Is Nothing OrElse SousListe.Count = 0 Then
                        Return Nothing
                    End If
                    Return SousListe
                End Get
            End Property

            Public ReadOnly Property ListeDesBadgeages(ByVal Annee As Integer) As List(Of Virtualia.TablesObjet.ShemaPER.PER_POINTAGE)
                Get
                    If WsLstBadgeages Is Nothing Then
                        Return Nothing
                    End If
                    Dim SousListe As List(Of Virtualia.TablesObjet.ShemaPER.PER_POINTAGE)
                    SousListe = (From FichePER In WsLstBadgeages Where FichePER.Date_Valeur_ToDate.Year = Annee Order By FichePER.Date_Valeur_ToDate Ascending).ToList
                    If SousListe Is Nothing OrElse SousListe.Count = 0 Then
                        Return Nothing
                    End If
                    Return SousListe
                End Get
            End Property

            Public ReadOnly Property ListeDesSemainesTravaillees(ByVal Annee As Integer, ByVal Mois As Integer) As List(Of Virtualia.TablesObjet.ShemaPER.PER_CET_HEBDO)
                Get
                    If WsLstSemaines Is Nothing Then
                        Return Nothing
                    End If
                    Dim SousListe As List(Of Virtualia.TablesObjet.ShemaPER.PER_CET_HEBDO)
                    SousListe = (From FichePER In WsLstSemaines Where (FichePER.Date_Valeur_ToDate.Year = Annee Or FichePER.Date_Fin_ToDate.Year = Annee) And
                                   (FichePER.Date_Valeur_ToDate.Month = Mois Or FichePER.Date_Fin_ToDate.Month = Mois)
                                 Order By FichePER.Date_Valeur_ToDate Ascending).ToList
                    If SousListe Is Nothing OrElse SousListe.Count = 0 Then
                        Return Nothing
                    End If
                    Return SousListe
                End Get
            End Property

            Public ReadOnly Property ListeDesSemainesTravaillees(ByVal Annee As Integer) As List(Of Virtualia.TablesObjet.ShemaPER.PER_CET_HEBDO)
                Get
                    If WsLstSemaines Is Nothing Then
                        Return Nothing
                    End If
                    Dim SousListe As List(Of Virtualia.TablesObjet.ShemaPER.PER_CET_HEBDO)
                    SousListe = (From FichePER In WsLstSemaines Where FichePER.Date_Valeur_ToDate.Year = Annee Order By FichePER.Date_Valeur_ToDate Ascending).ToList
                    If SousListe Is Nothing OrElse SousListe.Count = 0 Then
                        Return Nothing
                    End If
                    Return SousListe
                End Get
            End Property

            Public ReadOnly Property FicheDroitsConge(ByVal Annee As Integer) As Virtualia.TablesObjet.ShemaPER.PER_DROIT_CONGES
                Get
                    If WsLstDroits Is Nothing Then
                        Return Nothing
                    End If
                    Try
                        Return (From FichePER In WsLstDroits Where FichePER.Date_Valeur_ToDate.Year = Annee).First
                    Catch ex As Exception
                        Return Nothing
                    End Try
                End Get
            End Property

            Public ReadOnly Property PointeurCycle(ByVal ArgumentDate As String) As Virtualia.TablesObjet.ShemaPER.PER_TRAVAIL
                Get
                    If WsLstPlanning Is Nothing OrElse WsLstPlanning.Count = 0 Then
                        Return Nothing
                    End If
                    For Each FichePER In WsLstPlanning
                        Select Case WsParent.VParent.VirRhDates.ComparerDates(FichePER.Date_d_effet, ArgumentDate)
                            Case VI.ComparaisonDates.Egalite, VI.ComparaisonDates.PlusPetit
                                If FichePER.VDate_de_Fin = "" Then
                                    Return FichePER
                                End If
                                Select Case WsParent.VParent.VirRhDates.ComparerDates(FichePER.VDate_de_Fin, ArgumentDate)
                                    Case VI.ComparaisonDates.Egalite, VI.ComparaisonDates.PlusGrand
                                        Return FichePER
                                End Select
                                Exit For
                        End Select
                    Next
                    Return Nothing
                End Get
            End Property

            Public ReadOnly Property HoraireJournalier(ByVal ArgumentDate As String) As Double
                Get
                    If WsLstPlanning Is Nothing OrElse WsLstPlanning.Count = 0 Then
                        Return 420
                    End If
                    For Each FicheTRA In WsLstPlanning
                        Select Case WsParent.VParent.VirRhDates.ComparerDates(FicheTRA.Date_de_Valeur, ArgumentDate)
                            Case VI.ComparaisonDates.Egalite, VI.ComparaisonDates.PlusPetit
                                Return WsParent.VParent.VirRhFonction.ConversionDouble(WsParent.VParent.VirRhDates.CalcHeure(FicheTRA.HoraireJournalier, "", 1))
                        End Select
                    Next
                    Return 420
                End Get
            End Property

            Public ReadOnly Property SiHsAutomatique(ByVal ArgumentDate As String) As Boolean
                Get
                    Dim FichePER As Virtualia.TablesObjet.ShemaPER.PER_AFFECTATION
                    Dim SiOK As Boolean

                    FichePER = WsParent.VFiche_Affectation(ArgumentDate)
                    If FichePER Is Nothing Then
                        Return False
                    End If
                    If FichePER.Fonction_exercee = "" Or FichePER.Secteureconomique = "" Then
                        Return False
                    End If
                    If WsParent.VParent.VirListeSecteursHS Is Nothing Then
                        Return False
                    End If
                    SiOK = False
                    For Each Element In WsParent.VParent.VirListeSecteursHS
                        If Element = FichePER.Secteureconomique Then
                            SiOK = True
                            Exit For
                        End If
                    Next
                    If SiOK = False Then
                        Return False
                    End If
                    If WsParent.VParent.VirListeFonctionsHS Is Nothing Then
                        Return False
                    End If
                    SiOK = False
                    For Each Element In WsParent.VParent.VirListeFonctionsHS
                        If Element = FichePER.Fonction_exercee Then
                            SiOK = True
                            Exit For
                        End If
                    Next
                    Return SiOK
                End Get
            End Property

            Public ReadOnly Property DureeTrajet As Double
                Get
                    If WsFicheDomicile Is Nothing Then
                        Return 0.5
                    End If
                    If WsFicheDomicile.Temps_Trajet = "" Then
                        Return 0.5
                    End If
                    Dim Zcalc As String = WsParent.VParent.VirRhDates.ConvHeuresEnCentieme(WsFicheDomicile.Temps_Trajet)
                    If Zcalc = "" Then
                        Return 0.5
                    End If
                    Return WsParent.VParent.VirRhFonction.ConversionDouble(Zcalc, 2)
                End Get
            End Property

            Public ReadOnly Property BorneTypee(ByVal ArgumentDate As String) As System.DateTime
                Get
                    Dim FicheCOLL As Virtualia.TablesObjet.ShemaPER.PER_COLLECTIVITE
                    Dim LstCycles As List(Of Virtualia.TablesObjet.ShemaREF.TRA_IDENTIFICATION)
                    Dim Fiche_REF As Virtualia.TablesObjet.ShemaREF.TRA_IDENTIFICATION = Nothing
                    Dim Fiche_ETA As Virtualia.TablesObjet.ShemaREF.ETA_IDENTITE = Nothing
                    Dim Fiche_AccordRTT As Virtualia.TablesObjet.ShemaREF.ACCORD_RTT = Nothing
                    Dim FichePER As Virtualia.TablesObjet.ShemaPER.PER_TRAVAIL = PointeurCycle(ArgumentDate)
                    Dim HHNonRenseignee As System.DateTime

                    HHNonRenseignee = WsParent.VParent.VirRhDates.HeureTypee(ArgumentDate, "23:59")
                    FicheCOLL = WsParent.VFiche_Etablissement(ArgumentDate)
                    If FicheCOLL Is Nothing Then
                        Return HHNonRenseignee
                    End If
                    FichePER = PointeurCycle(ArgumentDate)
                    If FichePER Is Nothing Then
                        Return HHNonRenseignee
                    End If
                    If FichePER.SiHoraireFixe = True Then
                        Return WsParent.VParent.VirReferentiel.HeureFinCycle(ArgumentDate, FichePER.CycledeTravail, FichePER.Date_de_Valeur)
                    End If
                    LstCycles = WsParent.VParent.VirReferentiel.RessourceCycles.ListeDesCycles("")
                    If LstCycles IsNot Nothing Then
                        Try
                            Fiche_REF = (From CycleW In LstCycles Where CycleW.Intitule = FichePER.CycledeTravail).First
                        Catch ex1 As Exception
                            Exit Try
                        End Try
                    End If
                    If Fiche_REF IsNot Nothing Then
                        Fiche_AccordRTT = WsParent.VParent.VirReferentiel.FicheAccordRTT(VI.PointdeVue.PVueCycle, Fiche_REF.Ide_Dossier, FichePER.Date_Valeur_ToDate)
                    End If
                    If Fiche_AccordRTT Is Nothing Then
                        Fiche_ETA = WsParent.VParent.VirReferentiel.FicheEtablissement(FicheCOLL.Administration)
                        If Fiche_ETA IsNot Nothing Then
                            Fiche_AccordRTT = WsParent.VParent.VirReferentiel.FicheAccordRTT(VI.PointdeVue.PVueEtablissement, Fiche_ETA.Ide_Dossier, FichePER.Date_Valeur_ToDate)
                        End If
                    End If
                    If Fiche_AccordRTT Is Nothing Then
                        Return HHNonRenseignee
                    End If
                    If Fiche_AccordRTT.Heure_Fin_PlageVariable <> "" Then
                        Return WsParent.VParent.VirRhDates.HeureTypee(ArgumentDate, Fiche_AccordRTT.Heure_Fin_PlageVariable)
                    End If
                    Return HHNonRenseignee
                End Get
            End Property

            Private Sub ConstruireFicheDetail_HS(ByVal FicheSource As Virtualia.TablesObjet.ShemaPER.PER_DETAIL_HS, ByVal ArgumentDate As String,
                                                 ByVal HeureBadgee As System.DateTime, ByVal HeureLissee As System.DateTime, Optional ByVal SiMatin As Boolean = False)
                Dim Fiche_Detail As Virtualia.TablesObjet.ShemaPER.PER_DETAIL_HS = Nothing
                Dim SiOK As Boolean
                Dim HeureDebutHS As String = Strings.Format(HeureLissee.Hour, "00") & ":" & Strings.Format(HeureLissee.Minute, "00")
                Dim HeureFinHS As String = Strings.Format(HeureBadgee.Hour, "00") & ":" & Strings.Format(HeureBadgee.Minute, "00")

                If FicheSource IsNot Nothing Then
                    If FicheSource.FaitGenerateur <> "" And FicheSource.Date_Validation <> "" Then
                        Exit Sub
                    End If
                End If
                If FicheSource Is Nothing Then
                    Fiche_Detail = New Virtualia.TablesObjet.ShemaPER.PER_DETAIL_HS
                    Fiche_Detail.ModuleOrigine = "SelfV4"
                    Fiche_Detail.FaitGenerateur = ""
                    Fiche_Detail.Observations = ""
                    Fiche_Detail.Date_Validation = ""
                    Fiche_Detail.Signataire = ""
                Else
                    Fiche_Detail = FicheSource
                End If
                Fiche_Detail.Ide_Dossier = WsParent.Ide_Dossier
                Fiche_Detail.Date_de_Valeur = ArgumentDate
                Fiche_Detail.Nbheures_Trajet = DureeTrajet
                Fiche_Detail.Nature = "Heure supplémentaire"
                If WsParent.VParent.VirRhDates.SiJourOuvre(ArgumentDate, True) = False Then
                    Fiche_Detail.Nature = "Astreinte"
                    Fiche_Detail.Nbheures_Trajet = DureeTrajet * 2
                End If
                Fiche_Detail.Heure_Borne = HeureDebutHS
                Fiche_Detail.Heure_Fin = HeureFinHS
                If SiHsAutomatique(ArgumentDate) = True Then
                    Fiche_Detail.FaitGenerateur = WsParent.VFiche_Affectation(ArgumentDate).Secteureconomique
                    Fiche_Detail.Date_Validation = WsParent.VParent.VirRhDates.DateduJour
                    Fiche_Detail.Signataire = "Automatique"
                    Fiche_Detail.Observations = ""
                End If
                If FicheSource Is Nothing Then
                    If WsLstDetailHS Is Nothing Then
                        WsLstDetailHS = New List(Of Virtualia.TablesObjet.ShemaPER.PER_DETAIL_HS)
                    End If
                    SiOK = WsParent.VParent.VirServiceServeur.MiseAjour_Fiche(WsParent.VParent.VirNomUtilisateur, VI.PointdeVue.PVueApplicatif,
                                                                   VI.ObjetPer.ObaDetailHS, WsParent.Ide_Dossier, "C", "", Fiche_Detail.ContenuTable, True)
                    If SiOK = True Then
                        WsLstDetailHS.Add(Fiche_Detail)
                    End If
                ElseIf (Fiche_Detail.Heure_Fin <> FicheSource.Heure_Fin) Or (Fiche_Detail.Heure_Borne <> FicheSource.Heure_Borne) Then
                    SiOK = WsParent.VParent.VirServiceServeur.MiseAjour_Fiche(WsParent.VParent.VirNomUtilisateur, VI.PointdeVue.PVueApplicatif,
                                                                    VI.ObjetPer.ObaDetailHS, WsParent.Ide_Dossier, "M", Fiche_Detail.FicheLue, Fiche_Detail.ContenuTable, True)
                End If
            End Sub

            Private WriteOnly Property ListedesFiches As List(Of Virtualia.Systeme.MetaModele.VIR_FICHE)
                Set(ByVal value As List(Of Virtualia.Systeme.MetaModele.VIR_FICHE))
                    Dim Predicat As Virtualia.Systeme.MetaModele.Predicats.PredicateFiche
                    Dim TabObjet As List(Of Integer)
                    Dim LstFiches As List(Of Virtualia.Systeme.MetaModele.VIR_FICHE)
                    Dim Annee As Integer = Year(Now) - 2

                    Predicat = New Virtualia.Systeme.MetaModele.Predicats.PredicateFiche(VI.PointdeVue.PVueApplicatif, VI.ObjetPer.ObaAdresse)
                    If value.FirstOrDefault(Function(recherche) recherche.NumeroObjet = VI.ObjetPer.ObaAdresse) IsNot Nothing Then
                        WsFicheDomicile = CType(value.FirstOrDefault(Function(recherche) recherche.NumeroObjet = VI.ObjetPer.ObaAdresse), Virtualia.TablesObjet.ShemaPER.PER_DOMICILE)
                    End If

                    Predicat = New Virtualia.Systeme.MetaModele.Predicats.PredicateFiche(VI.ObjetPer.ObaDroits, "01/01/" & Annee, WsParent.VParent.VirRhDates.DateduJour)
                    LstFiches = value.FindAll(AddressOf Predicat.SiFicheEvtDansPeriode)
                    If LstFiches IsNot Nothing AndAlso LstFiches.Count > 0 Then
                        WsLstDroits = WsParent.VParent.VirRhFonction.ConvertisseurListeFiches(Of Virtualia.TablesObjet.ShemaPER.PER_DROIT_CONGES)(LstFiches)
                    Else
                        WsLstDroits = Nothing
                    End If

                    Predicat = New Virtualia.Systeme.MetaModele.Predicats.PredicateFiche(VI.ObjetPer.ObaHebdoCET, "01/01/" & Annee, WsParent.VParent.VirRhDates.DateduJour)
                    LstFiches = value.FindAll(AddressOf Predicat.SiFicheEvtDansPeriode)
                    If LstFiches IsNot Nothing AndAlso LstFiches.Count > 0 Then
                        WsLstSemaines = WsParent.VParent.VirRhFonction.ConvertisseurListeFiches(Of Virtualia.TablesObjet.ShemaPER.PER_CET_HEBDO)(LstFiches)
                    Else
                        WsLstSemaines = Nothing
                    End If

                    Predicat = New Virtualia.Systeme.MetaModele.Predicats.PredicateFiche(VI.ObjetPer.ObaDetailHS, "01/01/" & Annee, WsParent.VParent.VirRhDates.DateduJour)
                    LstFiches = value.FindAll(AddressOf Predicat.SiFicheEvtDansPeriode)
                    If LstFiches IsNot Nothing AndAlso LstFiches.Count > 0 Then
                        WsLstDetailHS = WsParent.VParent.VirRhFonction.ConvertisseurListeFiches(Of Virtualia.TablesObjet.ShemaPER.PER_DETAIL_HS)(LstFiches)
                    Else
                        WsLstDetailHS = Nothing
                    End If

                    Predicat = New Virtualia.Systeme.MetaModele.Predicats.PredicateFiche(VI.ObjetPer.ObaMensuelHS, "01/01/" & Annee - 1, WsParent.VParent.VirRhDates.DateduJour)
                    LstFiches = value.FindAll(AddressOf Predicat.SiFicheEvtDansPeriode)
                    If LstFiches IsNot Nothing AndAlso LstFiches.Count > 0 Then
                        WsLstHeuresSup = WsParent.VParent.VirRhFonction.ConvertisseurListeFiches(Of Virtualia.TablesObjet.ShemaPER.PER_MENSUEL_HS)(LstFiches)
                    Else
                        WsLstHeuresSup = Nothing
                    End If

                    Predicat = New Virtualia.Systeme.MetaModele.Predicats.PredicateFiche(VI.ObjetPer.ObaAbsence, "01/01/" & Annee - 1, WsParent.VParent.VirRhDates.DateduJour)
                    LstFiches = value.FindAll(AddressOf Predicat.SiFicheEvtDansPeriode)
                    If LstFiches IsNot Nothing AndAlso LstFiches.Count > 0 Then
                        WsLstRecuperation = New List(Of Virtualia.TablesObjet.ShemaPER.PER_ABSENCE)
                        For Each FichePER In LstFiches
                            If CType(FichePER, Virtualia.TablesObjet.ShemaPER.PER_ABSENCE).Nature = "Crédit Heures supplémentaires" Then
                                WsLstRecuperation.Add(CType(FichePER, Virtualia.TablesObjet.ShemaPER.PER_ABSENCE))
                            End If
                        Next
                        If WsLstRecuperation.Count = 0 Then
                            WsLstRecuperation = Nothing
                        End If
                    Else
                        WsLstRecuperation = Nothing
                    End If

                    TabObjet = New List(Of Integer)
                    TabObjet.Add(VI.ObjetPer.ObaPresence)
                    Predicat = New Virtualia.Systeme.MetaModele.Predicats.PredicateFiche(TabObjet)
                    LstFiches = value.FindAll(AddressOf Predicat.SiFicheDansListeObjets)
                    If LstFiches IsNot Nothing AndAlso LstFiches.Count > 0 Then
                        WsLstPlanning = WsParent.VParent.VirRhFonction.ConvertisseurListeFiches(Of Virtualia.TablesObjet.ShemaPER.PER_TRAVAIL)(LstFiches)
                    Else
                        WsLstPlanning = Nothing
                    End If

                    Predicat = New Virtualia.Systeme.MetaModele.Predicats.PredicateFiche(VI.ObjetPer.ObaPointage, "01/01/" & Annee, WsParent.VParent.VirRhDates.DateduJour)
                    LstFiches = value.FindAll(AddressOf Predicat.SiFicheEvtDansPeriode)
                    If LstFiches IsNot Nothing AndAlso LstFiches.Count > 0 Then
                        WsLstBadgeages = WsParent.VParent.VirRhFonction.ConvertisseurListeFiches(Of Virtualia.TablesObjet.ShemaPER.PER_POINTAGE)(LstFiches)
                    Else
                        WsLstBadgeages = Nothing
                    End If

                    Predicat = New Virtualia.Systeme.MetaModele.Predicats.PredicateFiche(VI.ObjetPer.ObaHeuresSup, "01/01/" & Annee - 1, WsParent.VParent.VirRhDates.DateduJour)
                    LstFiches = value.FindAll(AddressOf Predicat.SiFicheEvtDansPeriode)
                    If LstFiches IsNot Nothing AndAlso LstFiches.Count > 0 Then
                        WsLstHSRemunerees = WsParent.VParent.VirRhFonction.ConvertisseurListeFiches(Of Virtualia.TablesObjet.ShemaPER.PER_HEURESUP)(LstFiches)
                    Else
                        WsLstHSRemunerees = Nothing
                    End If
                End Set
            End Property

            Private Sub LireFiches()
                '** Infos complémentaires
                Dim TableObjet As List(Of Integer)
                Dim TabIde As List(Of Integer)
                Dim ToutUnObjet As Virtualia.Ressources.Datas.EnsembleFiches
                Dim ListeAllFiches As List(Of Virtualia.Systeme.MetaModele.VIR_FICHE)

                TableObjet = New List(Of Integer)
                TableObjet.Add(VI.ObjetPer.ObaAdresse)
                TableObjet.Add(VI.ObjetPer.ObaDroits)
                TableObjet.Add(VI.ObjetPer.ObaDetailHS)
                TableObjet.Add(VI.ObjetPer.ObaMensuelHS)
                TableObjet.Add(VI.ObjetPer.ObaAbsence)
                TableObjet.Add(VI.ObjetPer.ObaPresence)
                TableObjet.Add(VI.ObjetPer.ObaPointage)
                TableObjet.Add(VI.ObjetPer.ObaHebdoCET)
                TableObjet.Add(VI.ObjetPer.ObaHeuresSup)

                TabIde = New List(Of Integer)
                TabIde.Add(WsParent.Ide_Dossier)
                ListeAllFiches = New List(Of Virtualia.Systeme.MetaModele.VIR_FICHE)
                For IndiceI = 0 To TableObjet.Count - 1
                    ToutUnObjet = New Virtualia.Ressources.Datas.EnsembleFiches(WsParent.VParent.VirNomUtilisateur, VI.PointdeVue.PVueApplicatif)
                    ToutUnObjet.NumeroObjet(TabIde, True) = TableObjet(IndiceI)
                    ListeAllFiches.AddRange(ToutUnObjet.ListedesFiches)
                Next IndiceI
                ListedesFiches = ListeAllFiches
            End Sub

            Public ReadOnly Property VParent As Virtualia.Net.WebAppli.SelfV4.DossierPersonne
                Get
                    Return WsParent
                End Get
            End Property

            Public Sub New(ByVal host As Virtualia.Net.WebAppli.SelfV4.DossierPersonne)
                WsParent = host
                Call LireFiches()
            End Sub

        End Class
    End Namespace
End Namespace
