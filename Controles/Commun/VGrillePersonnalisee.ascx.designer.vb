﻿'------------------------------------------------------------------------------
' <généré automatiquement>
'     Ce code a été généré par un outil.
'
'     Les modifications apportées à ce fichier peuvent provoquer un comportement incorrect et seront perdues si
'     le code est régénéré.
' </généré automatiquement>
'------------------------------------------------------------------------------

Option Strict On
Option Explicit On


Partial Public Class VGrillePersonnalisee
    
    '''<summary>
    '''Contrôle PanelGrille.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents PanelGrille As Global.System.Web.UI.WebControls.Panel
    
    '''<summary>
    '''Contrôle CadreGrille.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CadreGrille As Global.System.Web.UI.WebControls.Table
    
    '''<summary>
    '''Contrôle CadreEtiquettes.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CadreEtiquettes As Global.System.Web.UI.WebControls.Table
    
    '''<summary>
    '''Contrôle CellNeutre.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CellNeutre As Global.System.Web.UI.WebControls.TableCell
    
    '''<summary>
    '''Contrôle EtiNeutre.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents EtiNeutre As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle CellEti1.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CellEti1 As Global.System.Web.UI.WebControls.TableCell
    
    '''<summary>
    '''Contrôle EtiCol1.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents EtiCol1 As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle CellEti2.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CellEti2 As Global.System.Web.UI.WebControls.TableCell
    
    '''<summary>
    '''Contrôle EtiCol2.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents EtiCol2 As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle CellEti3.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CellEti3 As Global.System.Web.UI.WebControls.TableCell
    
    '''<summary>
    '''Contrôle EtiCol3.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents EtiCol3 As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle CellEti4.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CellEti4 As Global.System.Web.UI.WebControls.TableCell
    
    '''<summary>
    '''Contrôle EtiCol4.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents EtiCol4 As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle CellEti5.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CellEti5 As Global.System.Web.UI.WebControls.TableCell
    
    '''<summary>
    '''Contrôle EtiCol5.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents EtiCol5 As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle CellEti6.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CellEti6 As Global.System.Web.UI.WebControls.TableCell
    
    '''<summary>
    '''Contrôle EtiCol6.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents EtiCol6 As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle CellEti7.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CellEti7 As Global.System.Web.UI.WebControls.TableCell
    
    '''<summary>
    '''Contrôle EtiCol7.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents EtiCol7 As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle CellEti8.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CellEti8 As Global.System.Web.UI.WebControls.TableCell
    
    '''<summary>
    '''Contrôle EtiCol8.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents EtiCol8 As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle CellEti9.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CellEti9 As Global.System.Web.UI.WebControls.TableCell
    
    '''<summary>
    '''Contrôle EtiCol9.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents EtiCol9 As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle Ligne01.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Ligne01 As Global.System.Web.UI.WebControls.Table
    
    '''<summary>
    '''Contrôle CellCmd_L01.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CellCmd_L01 As Global.System.Web.UI.WebControls.TableCell
    
    '''<summary>
    '''Contrôle CmdCol_L01.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CmdCol_L01 As Global.System.Web.UI.WebControls.ImageButton
    
    '''<summary>
    '''Contrôle CellDon_L01_1.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CellDon_L01_1 As Global.System.Web.UI.WebControls.TableCell
    
    '''<summary>
    '''Contrôle DonCol_L01_1.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents DonCol_L01_1 As Global.System.Web.UI.WebControls.TextBox
    
    '''<summary>
    '''Contrôle CellDon_L01_2.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CellDon_L01_2 As Global.System.Web.UI.WebControls.TableCell
    
    '''<summary>
    '''Contrôle DonCol_L01_2.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents DonCol_L01_2 As Global.System.Web.UI.WebControls.TextBox
    
    '''<summary>
    '''Contrôle CellDon_L01_3.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CellDon_L01_3 As Global.System.Web.UI.WebControls.TableCell
    
    '''<summary>
    '''Contrôle DonCol_L01_3.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents DonCol_L01_3 As Global.System.Web.UI.WebControls.TextBox
    
    '''<summary>
    '''Contrôle CellDon_L01_4.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CellDon_L01_4 As Global.System.Web.UI.WebControls.TableCell
    
    '''<summary>
    '''Contrôle DonCol_L01_4.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents DonCol_L01_4 As Global.System.Web.UI.WebControls.TextBox
    
    '''<summary>
    '''Contrôle CellDon_L01_5.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CellDon_L01_5 As Global.System.Web.UI.WebControls.TableCell
    
    '''<summary>
    '''Contrôle DonCol_L01_5.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents DonCol_L01_5 As Global.System.Web.UI.WebControls.TextBox
    
    '''<summary>
    '''Contrôle CellDon_L01_6.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CellDon_L01_6 As Global.System.Web.UI.WebControls.TableCell
    
    '''<summary>
    '''Contrôle DonCol_L01_6.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents DonCol_L01_6 As Global.System.Web.UI.WebControls.TextBox
    
    '''<summary>
    '''Contrôle CellDon_L01_7.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CellDon_L01_7 As Global.System.Web.UI.WebControls.TableCell
    
    '''<summary>
    '''Contrôle DonCol_L01_7.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents DonCol_L01_7 As Global.System.Web.UI.WebControls.TextBox
    
    '''<summary>
    '''Contrôle CellDon_L01_8.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CellDon_L01_8 As Global.System.Web.UI.WebControls.TableCell
    
    '''<summary>
    '''Contrôle DonCol_L01_8.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents DonCol_L01_8 As Global.System.Web.UI.WebControls.TextBox
    
    '''<summary>
    '''Contrôle CellDon_L01_9.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CellDon_L01_9 As Global.System.Web.UI.WebControls.TableCell
    
    '''<summary>
    '''Contrôle DonCol_L01_9.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents DonCol_L01_9 As Global.System.Web.UI.WebControls.TextBox
End Class
