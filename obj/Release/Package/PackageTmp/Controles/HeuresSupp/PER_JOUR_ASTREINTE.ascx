﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="PER_JOUR_ASTREINTE.ascx.vb" Inherits="Virtualia.Net.PER_JOUR_ASTREINTE" %>

<%@ Register src="~/Controles/Saisies/VCoupleEtiDate.ascx" tagname="VCoupleEtiDate" tagprefix="Virtualia" %>
<%@ Register src="~/Controles/Saisies/VCoupleEtiDonnee.ascx" tagname="VCoupleEtiDonnee" tagprefix="Virtualia" %>
<%@ Register src="~/Controles/Saisies/VCoupleVerticalEtiDonnee.ascx" tagname="VCoupleVerticalEtiDonnee" tagprefix="Virtualia" %>
<%@ Register src="~/Controles/Saisies/VTrioHorizontalRadio.ascx" tagname="VTrioHorizontalRadio" tagprefix="Virtualia" %>
<%@ Register src="~/Controles/Commun/VListeGrid.ascx" tagname="VListeGrid" tagprefix="Virtualia" %>

<asp:Table ID="CadreInfo" runat="server" BorderStyle="None" BorderWidth="2px" Visible="true" BackColor="#D7FAF3" BorderColor="#B0E0D7" Width="1050px" HorizontalAlign="Center" style="margin-top: 1px;">
    <asp:TableRow>
        <asp:TableCell Width="330px" HorizontalAlign="Right">
            <asp:Button ID="CommandeHS" runat="server" BackColor="#1C5150" Width="300px" Height="22px" 
                BorderStyle="Outset" BorderWidth="2px" BorderColor="WhiteSmoke" 
                Font-Names="Trebuchet MS" Font-Size="Small" ForeColor="#B0E0D7" Font-Bold="false"
                Font-Underline="true" Font-Italic="true"  Visible="True"
                Text="Retour au suivi des heures Supplémentaires" Tooltip=""
                style="margin-bottom: 10px;margin-right:5px;text-align:center">
           </asp:Button>
        </asp:TableCell>
    </asp:TableRow>
     <asp:TableRow> 
         <asp:TableCell> 
           <Virtualia:VListeGrid ID="ListeGrille" runat="server" CadreWidth="630px" SiPagination="true" TaillePage="10" SiColonneSelect="true" SiCaseAcocher="false" />
         </asp:TableCell>
   </asp:TableRow>
     <asp:TableRow>
          <asp:TableCell>
            <asp:Table ID="CadreCommandes" runat="server" Height="22px" CellPadding="0" CellSpacing="0"
                Width="244px" HorizontalAlign="Right" style="margin-top: 3px;margin-right:  3px">
                <asp:TableRow>
                    <asp:TableCell VerticalAlign="Bottom">
                        <asp:Table ID="CadreCmdNewSupp" runat="server" BackImageUrl="~/Images/Boutons/NewSupp_Std.bmp" 
                            Width="160px" CellPadding="0" CellSpacing="0"
                            BorderWidth="2px" BorderStyle="Outset" BorderColor="#B0E0D7" ForeColor="#D7FAF3">
                            <asp:TableRow>
                                <asp:TableCell>
                                    <asp:Button ID="CommandeNew" runat="server" Text="Nouveau" Width="68px" Height="20px"
                                        BackColor="Transparent" BorderColor="#B0E0D7" ForeColor="#D7FAF3"
                                        Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small" Font-Italic="true"
                                        BorderStyle="None" style=" margin-left: 10px; text-align: right;"
                                        Tooltip="Nouvelle fiche">
                                    </asp:Button>
                                </asp:TableCell>
                                <asp:TableCell>
                                    <asp:Button ID="CommandeSupp" runat="server" Text="Supprimer" Width="68px" Height="20px"
                                        BackColor="Transparent" BorderColor="#B0E0D7" ForeColor="#D7FAF3"
                                        Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small" Font-Italic="true"
                                        BorderStyle="None" style=" margin-left: 10px; text-align: right;"
                                        Tooltip="Supprimer la fiche">
                                    </asp:Button>
                                </asp:TableCell>
                            </asp:TableRow>
                        </asp:Table>    
                    </asp:TableCell>
                    <asp:TableCell VerticalAlign="Bottom">
                        <asp:Table ID="CadreCmdOK" runat="server" BackImageUrl="~/Images/Boutons/OK_Std.bmp" Width="70px"
                            BorderWidth="2px" BorderStyle="Outset" BorderColor="#B0E0D7" ForeColor="#D7FAF3"
                            CellPadding="0" CellSpacing="0" Visible="false">
                            <asp:TableRow>
                                <asp:TableCell>
                                    <asp:Button ID="CommandeOK" runat="server" Text="Valider" Width="65px" Height="20px"
                                        BackColor="Transparent" BorderColor="#B0E0D7" ForeColor="#D7FAF3" 
                                        Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small" Font-Italic="true"
                                        BorderStyle="None" style=" margin-left: 6px; text-align: right;">
                                        </asp:Button>
                                </asp:TableCell>
                            </asp:TableRow>
                        </asp:Table>  
                    </asp:TableCell>
                </asp:TableRow>
              </asp:Table>
          </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell>
             <asp:Table ID="CadreOperation" runat="server" CellPadding="0" Width="460px" 
                CellSpacing="0" HorizontalAlign="Center" BorderStyle="None" BorderWidth="1px" Visible="true" BorderColor="#2FA49B">
                <asp:TableRow>
                    <asp:TableCell>
                        <Virtualia:VCoupleEtiDate ID="InfoD00" runat="server" TypeCalendrier="Standard" EtiWidth="135px" DonWidth="100px"
                                        V_SiDonneeDico="true" V_PointdeVue="1" V_Objet="171" V_Information="0" DonTabIndex="1"
                                        Etistyle="text-align:center;"/>
                    </asp:TableCell>
                    <asp:TableCell>
                        <Virtualia:VCoupleEtiDonnee ID="InfoH01" runat="server" V_PointdeVue="1" V_Objet="171" V_Information="1" V_SiDonneeDico="true" V_SiEnLectureSeule="True"
                            Etivisible="False" DonWidth="150px" DonHeight="20px" DonTabIndex="2" DonBorderWidth="1px" Donstyle="text-align:center;"/>     
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center" ColumnSpan="2">
                        <Virtualia:VCoupleVerticalEtiDonnee ID="InfoV03" runat="server" DonTextMode="true"
                            V_PointdeVue="1" V_Objet="171" V_Information="3" V_SiDonneeDico="true"
                            EtiWidth="445px" DonWidth="445px" DonHeight="40px" EtiHeight="20px" DonTabIndex="4" 
                            EtiText="Observations éventuelles" Etivisible="True"
                            Donstyle="margin-left: 5px;" DonBorderWidth="1px"
                            Etistyle="margin-left: 5px; text-align: center; text-indent: 0px;"/>            
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                   <asp:TableCell Height="15px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center" ColumnSpan="2">
                        <asp:Label ID="EtiValidation" runat="server" Height="20px" Width="440px" Text="Validation du paiement"
                            BackColor="#B0E0D7" BorderColor="#B0E0D7"  BorderStyle="Outset"
                            BorderWidth="2px" ForeColor="#142425" Font-Italic="true"
                            Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Medium"
                            style="margin-top:0px; margin-left:0px; margin-bottom:0px;text-indent:0px;text-align:center;">
                        </asp:Label>          
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center" ColumnSpan="2">
                        <Virtualia:VTrioHorizontalRadio ID="RadioH04" runat="server" V_Groupe="ValidationAstreinte"
                            V_PointdeVue="1" V_Objet="171" V_Information="4" V_SiDonneeDico="true"
                            RadioGaucheWidth="90px" RadioCentreWidth="90px" RadioDroiteWidth="5px"  
                            RadioGaucheText="Oui" RadioCentreText="Non" RadioDroiteText="" RadioDroiteVisible="false" 
                            RadioGaucheHeight="20px" RadioCentreHeight="20px" RadioDroiteHeight="20px"
                            RadioCentreStyle="text-align:center;" RadioGaucheStyle="text-align:center;" RadioDroiteStyle="text-align:center;" 
			                Visible="true"/>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center" ColumnSpan="2">
                        <Virtualia:VCoupleEtiDonnee ID="InfoH05" runat="server" V_SiEnLectureSeule="True"
                            V_PointdeVue="1" V_Objet="171" V_Information="5" V_SiDonneeDico="true"
                            DonWidth="105px" DonHeight="20px" DonBorderWidth="1px" DonTabIndex="7" 
                            Donstyle="margin-left: 0px; text-align: center;" 
                            Etivisible="True" EtiWidth="330px" EtiForeColor="#2FA49B" EtiText="Date de validation de la Direction Générale"  
                            Etistyle="margin-left:0px;text-align:center;text-indent:2px;"/>
                    </asp:TableCell>          
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center" ColumnSpan="2">
                        <Virtualia:VCoupleVerticalEtiDonnee ID="InfoV06" runat="server" DonTextMode="true"
                            V_PointdeVue="1" V_Objet="171" V_Information="3" V_SiDonneeDico="true" V_SiEnLectureSeule="True"
                            EtiWidth="445px" DonWidth="445px" DonHeight="40px" EtiHeight="20px" DonTabIndex="4" 
                            EtiText="Observations éventuelles de la Direction Générale" Etivisible="True"
                            Donstyle="margin-left: 5px;" DonBorderWidth="1px"
                            Etistyle="margin-left: 5px; text-align: center; text-indent: 0px;"/>            
                    </asp:TableCell>
                </asp:TableRow>
             </asp:Table>
        </asp:TableCell>
    </asp:TableRow>
</asp:Table>
