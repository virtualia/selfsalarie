﻿Option Strict On
Option Explicit On
Option Compare Text
Imports Virtualia.OutilsVisu.Formation
'Imports Virtualia.Metier.Formation
Imports Virtualia.Net.Session
Imports System.Drawing
Imports System.Web.UI.DataVisualization.Charting
Imports System.IO

Public Class CtrlGraphique


    Inherits UserControl
    Implements IControlBase
    Implements ICtrlExport
    'Private WsCtrlGestion As CtrlGestionTableauBord
    Private WsCouleur_Benef As Color = VisuHelper.ConvertiCouleur("#4CCBC1") 'BackColor Control Gestion
    Private WsCouleur_Effectif As Color = VisuHelper.ConvertiCouleur("#2A716B") 'BorderColor LABEL
    Private WsCouleur_Cout As Color = VisuHelper.ConvertiCouleur("#08625B") 'BackColor LABEL
    Private WsCouleur_Jour As Color = VisuHelper.ConvertiCouleur("#19968D") 'BackColor Control graphique
    Private WsCouleur_Btn_Normal As Color = VisuHelper.ConvertiCouleur("#124545")
    Private WsCouleur_Btn_Sel As Color = VisuHelper.ConvertiCouleur("#19968D")
    '** RP Decembre 2014
    Private WebFct As Virtualia.Net.Controles.WebFonctions
    '***
    Public Property Repartition_Courante As String
        Get
            If (Not ViewState.KeyExiste("Repartition_Courante")) Then
                ViewState.AjouteValeur("Repartition_Courante", "SEXE")
            End If
            Return DirectCast(ViewState("Repartition_Courante"), String)
        End Get
        Set(value As String)
            ViewState.AjouteValeur("Repartition_Courante", value)
        End Set
    End Property

    Public Property ChargeOnLoad As Boolean
        Get
            If (Not ViewState.KeyExiste("ChargeOnLoad")) Then
                ViewState.AjouteValeur("ChargeOnLoad", False)
            End If
            Return DirectCast(ViewState("ChargeOnLoad"), Boolean)
        End Get
        Set(value As Boolean)
            ViewState.AjouteValeur("ChargeOnLoad", value)
        End Set
    End Property

    Public ReadOnly Property NomControl As String Implements IControlBase.NomControl
        Get
            Return ID
        End Get
    End Property

    Protected Overrides Sub OnLoad(e As EventArgs)
        MyBase.OnLoad(e)
        If Not (ChargeOnLoad) Then
            Return
        End If
        'If (WsCtrlGestion Is Nothing OrElse WsCtrlGestion.VCache Is Nothing OrElse WsCtrlGestion.VCache.Statistiques.Count <= 0) Then
        '    Return
        'End If
    End Sub

    Public Sub Charge(items As Object) Implements IControlBase.Charge
        Repartition_Courante = "SEXE"
        ChargeOnLoad = True
        OnLoad(Nothing)
    End Sub



    Private Sub RempliSerie(grp As Chart, numserie As Integer, dtps As List(Of DataPoint))
        Dim quetooltip As Boolean = (dtps.Count >= 15)

        grp.Series(numserie).Points.Clear()
        dtps.ForEach(Sub(dtp)
                         If (quetooltip) Then
                             dtp.Label = ""
                         Else
                             dtp.ToolTip = ""
                         End If
                         grp.Series(numserie).Points.Add(dtp)
                     End Sub)
    End Sub

    Private Sub BtnSelec(btn As Button, selectionne As Boolean)
        If (selectionne) Then
            btn.BorderStyle = BorderStyle.Inset
            btn.BackColor = WsCouleur_Btn_Sel
            Exit Sub
        End If
        btn.BorderStyle = BorderStyle.None
        btn.BackColor = WsCouleur_Btn_Normal
    End Sub

    Public Function GetExport() As ExportPdfInfo Implements ICtrlExport.GetExport
        Dim sw As StringWriter = New StringWriter()
        Dim htw As HtmlTextWriter = New HtmlTextWriter(sw)

        Dim CRetour As ExportPdfInfo = New ExportPdfInfo()
        CRetour.NomFichier = "Graphique_" & Repartition_Courante

        Dim tb As Table = Nothing

        Try
            Select Case (Repartition_Courante)
                Case "SEXE"
                    tb = TB_SEXE
            End Select
            tb.RenderControl(htw)
            CRetour.ContenuHtml = htw.InnerWriter.ToString()
        Catch ex As Exception
            CRetour.EstOk = False
        End Try
        Return CRetour
    End Function

    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load

    End Sub
    Protected Sub BtnVue_Click(sender As Object, e As EventArgs)
        If (Repartition_Courante = DirectCast(sender, Control).ID.Replace("Btn", "")) Then
            Return
        End If
        Repartition_Courante = DirectCast(sender, Control).ID.Replace("Btn", "")
        OnLoad(Nothing)
    End Sub
End Class