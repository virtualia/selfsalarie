﻿Option Explicit On
Option Strict On
Option Compare Text
Imports VI = Virtualia.Systeme.Constantes
Namespace WebAppli
    Namespace SelfV4
        Public Class DossierPersonne
            Private WsParent As Virtualia.Net.Session.ObjetGlobal
            Private WsIde_Dossier As Integer
            Private WsLogin_AD As String
            Private WsNom As String
            Private WsPrenom As String
            Private WsCivilite As String
            Private WsEMail As String

            Private WsIde_Manager As Integer
            Private WsNiveau_Manager As Integer
            Private WsNomPrenom_Manager As String
            Private WsMail_Manager As String
            Private WsIde_Delegue As Integer
            Private WsNomPrenom_Delegue As String
            Private WsMail_Delegue As String

            Private WsLstEtablissement As List(Of Virtualia.TablesObjet.ShemaPER.PER_COLLECTIVITE) = Nothing
            Private WsLstAffectation As List(Of Virtualia.TablesObjet.ShemaPER.PER_AFFECTATION) = Nothing
            Private WsLstAffectation2nd As List(Of Virtualia.TablesObjet.ShemaPER.PER_AFFECTATION_SECOND) = Nothing
            Private WsLstPosition As List(Of Virtualia.TablesObjet.ShemaPER.PER_POSITION) = Nothing
            Private WsLstGrades As List(Of Virtualia.TablesObjet.ShemaPER.PER_GRADE) = Nothing
            Private WsLstCyclesTravail As List(Of Virtualia.TablesObjet.ShemaPER.PER_TRAVAIL) = Nothing
            Private WsLstAjustements As List(Of Virtualia.TablesObjet.ShemaPER.PER_MODIF_PLANNING) = Nothing

            Private WsDossier_Calendrier As List(Of Virtualia.Net.Controles.ItemJourCalendrier) = Nothing

            Private WsDossier_Bulletin As Virtualia.Net.WebAppli.SelfV4.DossierBulletin = Nothing  'FP Etat
            Private WsDossier_HeuresSup As Virtualia.Net.WebAppli.SelfV4.DossierHeuresSup = Nothing 'FP Etat Cned,ENM
            Private WsDossier_NewHeuresSup As Virtualia.Net.WebAppli.SelfV4.DossierNewHeureSup = Nothing 'Nouveau Standard
            '*******AKR 
            Private WsLstJourAstreinte As List(Of Virtualia.TablesObjet.ShemaPER.PER_JOUR_ASTREINTE) = Nothing
            Private WsAnneeSelection As Integer
            Private WsMoisSelection As Integer
            Private WsListeFiches As List(Of String)

            Public Property AnneeSelection As Integer
                Get
                    Return WsAnneeSelection
                End Get
                Set(ByVal value As Integer)
                    WsAnneeSelection = value
                End Set
            End Property
            Public Property MoisSelection As Integer
                Get
                    Return WsMoisSelection
                End Get
                Set(ByVal value As Integer)
                    WsMoisSelection = value
                End Set
            End Property

            Public Property V_ListeFiches As List(Of String)
                Get
                    Return WsListeFiches
                End Get
                Set(ByVal value As List(Of String))
                    WsListeFiches = value
                End Set
            End Property
            ''******** AKR  Fin
            Public ReadOnly Property VParent As Virtualia.Net.Session.ObjetGlobal
                Get
                    Return WsParent
                End Get
            End Property

            Public Sub Initialiser()
                WsLstEtablissement = Nothing
                WsLstAffectation = Nothing
                WsLstPosition = Nothing
                WsLstCyclesTravail = Nothing
                WsLstAjustements = Nothing
                WsDossier_Calendrier = Nothing
                WsDossier_Bulletin = Nothing
                WsDossier_HeuresSup = Nothing
                WsDossier_NewHeuresSup = Nothing
                WsLstJourAstreinte = Nothing
            End Sub

            Public ReadOnly Property VFiche_Etablissement(ByVal DateValeur As String) As Virtualia.TablesObjet.ShemaPER.PER_COLLECTIVITE
                Get
                    If WsLstEtablissement Is Nothing Then
                        Dim LstFiches As List(Of Virtualia.Systeme.MetaModele.VIR_FICHE)

                        LstFiches = WsParent.VirServiceServeur.LectureObjet_ToFiches(WsParent.VirNomUtilisateur, VI.PointdeVue.PVueApplicatif, VI.ObjetPer.ObaSociete, WsIde_Dossier, True)
                        If LstFiches Is Nothing Then
                            Return Nothing
                        End If
                        WsLstEtablissement = New List(Of Virtualia.TablesObjet.ShemaPER.PER_COLLECTIVITE)
                        For Each FichePER As Virtualia.Systeme.MetaModele.VIR_FICHE In LstFiches
                            WsLstEtablissement.Add(DirectCast(FichePER, Virtualia.TablesObjet.ShemaPER.PER_COLLECTIVITE))
                        Next
                    End If
                    If WsLstEtablissement Is Nothing OrElse WsLstEtablissement.Count = 0 Then
                        Return Nothing
                    End If
                    For Each FicheEta As Virtualia.TablesObjet.ShemaPER.PER_COLLECTIVITE In WsLstEtablissement
                        Select Case WsParent.VirRhDates.ComparerDates(FicheEta.Date_de_Valeur, DateValeur)
                            Case VI.ComparaisonDates.Egalite, VI.ComparaisonDates.PlusPetit
                                Return FicheEta
                        End Select
                    Next
                    Return Nothing
                End Get
            End Property

            Public ReadOnly Property VFiche_Affectation(ByVal DateValeur As String) As Virtualia.TablesObjet.ShemaPER.PER_AFFECTATION
                Get
                    If WsLstAffectation Is Nothing Then
                        Dim LstFiches As List(Of Virtualia.Systeme.MetaModele.VIR_FICHE)

                        LstFiches = WsParent.VirServiceServeur.LectureObjet_ToFiches(WsParent.VirNomUtilisateur, VI.PointdeVue.PVueApplicatif, VI.ObjetPer.ObaOrganigramme, WsIde_Dossier, True)
                        If LstFiches Is Nothing Then
                            Return Nothing
                        End If
                        WsLstAffectation = New List(Of Virtualia.TablesObjet.ShemaPER.PER_AFFECTATION)
                        For Each FichePER As Virtualia.Systeme.MetaModele.VIR_FICHE In LstFiches
                            WsLstAffectation.Add(DirectCast(FichePER, Virtualia.TablesObjet.ShemaPER.PER_AFFECTATION))
                        Next
                    End If
                    If WsLstAffectation Is Nothing OrElse WsLstAffectation.Count = 0 Then
                        Return Nothing
                    End If
                    For Each FicheAffectation As Virtualia.TablesObjet.ShemaPER.PER_AFFECTATION In WsLstAffectation
                        Select Case WsParent.VirRhDates.ComparerDates(FicheAffectation.Date_de_Valeur, DateValeur)
                            Case VI.ComparaisonDates.Egalite, VI.ComparaisonDates.PlusPetit
                                Return FicheAffectation
                        End Select
                    Next
                    Return Nothing
                End Get
            End Property

            Public ReadOnly Property VFiche_Affectation2nd(ByVal DateValeur As String) As Virtualia.TablesObjet.ShemaPER.PER_AFFECTATION_SECOND
                Get
                    If WsLstAffectation2nd Is Nothing Then
                        Dim LstFiches As List(Of Virtualia.Systeme.MetaModele.VIR_FICHE)

                        LstFiches = WsParent.VirServiceServeur.LectureObjet_ToFiches(WsParent.VirNomUtilisateur, VI.PointdeVue.PVueApplicatif, VI.ObjetPer.ObaAffectation2nd, WsIde_Dossier, True)
                        If LstFiches Is Nothing Then
                            Return Nothing
                        End If
                        WsLstAffectation2nd = New List(Of Virtualia.TablesObjet.ShemaPER.PER_AFFECTATION_SECOND)
                        For Each FichePER As Virtualia.Systeme.MetaModele.VIR_FICHE In LstFiches
                            WsLstAffectation2nd.Add(DirectCast(FichePER, Virtualia.TablesObjet.ShemaPER.PER_AFFECTATION_SECOND))
                        Next
                    End If
                    If WsLstAffectation2nd Is Nothing OrElse WsLstAffectation2nd.Count = 0 Then
                        Return Nothing
                    End If
                    For Each FicheAffectation As Virtualia.TablesObjet.ShemaPER.PER_AFFECTATION_SECOND In WsLstAffectation2nd
                        Select Case WsParent.VirRhDates.ComparerDates(FicheAffectation.Date_de_Valeur, DateValeur)
                            Case VI.ComparaisonDates.Egalite, VI.ComparaisonDates.PlusPetit
                                Return FicheAffectation
                        End Select
                    Next
                    Return Nothing
                End Get
            End Property

            Public ReadOnly Property VFiche_Position(ByVal DateValeur As String) As Virtualia.TablesObjet.ShemaPER.PER_POSITION
                Get
                    If WsLstPosition Is Nothing Then
                        Dim LstFiches As List(Of Virtualia.Systeme.MetaModele.VIR_FICHE)

                        LstFiches = WsParent.VirServiceServeur.LectureObjet_ToFiches(WsParent.VirNomUtilisateur, VI.PointdeVue.PVueApplicatif, VI.ObjetPer.ObaActivite, WsIde_Dossier, True)
                        If LstFiches Is Nothing Then
                            Return Nothing
                        End If
                        WsLstPosition = New List(Of Virtualia.TablesObjet.ShemaPER.PER_POSITION)
                        For Each FichePER As Virtualia.Systeme.MetaModele.VIR_FICHE In LstFiches
                            WsLstPosition.Add(DirectCast(FichePER, Virtualia.TablesObjet.ShemaPER.PER_POSITION))
                        Next
                    End If
                    If WsLstPosition Is Nothing OrElse WsLstPosition.Count = 0 Then
                        Return Nothing
                    End If
                    For Each FichePER As Virtualia.TablesObjet.ShemaPER.PER_POSITION In WsLstPosition
                        Select Case WsParent.VirRhDates.ComparerDates(FichePER.Date_de_Valeur, DateValeur)
                            Case VI.ComparaisonDates.Egalite, VI.ComparaisonDates.PlusPetit
                                Return FichePER
                        End Select
                    Next
                    Return Nothing
                End Get
            End Property

            Public ReadOnly Property VFiche_Grade(ByVal DateValeur As String) As Virtualia.TablesObjet.ShemaPER.PER_GRADE
                Get
                    If WsLstGrades Is Nothing Then
                        Dim LstFiches As List(Of Virtualia.Systeme.MetaModele.VIR_FICHE)

                        LstFiches = WsParent.VirServiceServeur.LectureObjet_ToFiches(WsParent.VirNomUtilisateur, VI.PointdeVue.PVueApplicatif, VI.ObjetPer.ObaGrade, WsIde_Dossier, True)
                        If LstFiches Is Nothing Then
                            Return Nothing
                        End If
                        WsLstGrades = New List(Of Virtualia.TablesObjet.ShemaPER.PER_GRADE)
                        For Each FichePER As Virtualia.Systeme.MetaModele.VIR_FICHE In LstFiches
                            WsLstGrades.Add(DirectCast(FichePER, Virtualia.TablesObjet.ShemaPER.PER_GRADE))
                        Next
                    End If
                    If WsLstGrades Is Nothing OrElse WsLstGrades.Count = 0 Then
                        Return Nothing
                    End If
                    For Each FichePER As Virtualia.TablesObjet.ShemaPER.PER_GRADE In WsLstGrades
                        Select Case WsParent.VirRhDates.ComparerDates(FichePER.Date_de_Valeur, DateValeur)
                            Case VI.ComparaisonDates.Egalite, VI.ComparaisonDates.PlusPetit
                                Return FichePER
                        End Select
                    Next
                    Return Nothing
                End Get
            End Property

            Public ReadOnly Property VDossier_Bulletin As Virtualia.Net.WebAppli.SelfV4.DossierBulletin
                Get
                    If WsDossier_Bulletin Is Nothing Then
                        WsDossier_Bulletin = New Virtualia.Net.WebAppli.SelfV4.DossierBulletin(Me)
                    End If
                    Return WsDossier_Bulletin
                End Get
            End Property

            Public ReadOnly Property VListeActivites(ByVal DateDebut As String, ByVal DateFin As String) As List(Of Virtualia.TablesObjet.ShemaPER.PER_ACTIVITE_JOUR)
                Get
                    Dim LstRes As List(Of Virtualia.Systeme.MetaModele.VIR_FICHE)
                    Dim LstIde As List(Of Integer)
                    LstIde = New List(Of Integer)
                    LstIde.Add(Ide_Dossier)

                    LstRes = WsParent.SelectionObjetDate(LstIde, DateDebut, DateFin, 112)
                    If LstRes Is Nothing Then
                        Return Nothing
                    End If
                    Return WsParent.VirRhFonction.ConvertisseurListeFiches(Of Virtualia.TablesObjet.ShemaPER.PER_ACTIVITE_JOUR)(LstRes)
                End Get
            End Property

            '**** Heures Supplémentaires - FPETAT CNED,ENM ou Nouveau Standard ****************************************************
            Public ReadOnly Property VDossier_HeuresSup As Virtualia.Net.WebAppli.SelfV4.DossierHeuresSup
                Get
                    If WsDossier_HeuresSup Is Nothing Then
                        WsDossier_HeuresSup = New Virtualia.Net.WebAppli.SelfV4.DossierHeuresSup(Me)
                    End If
                    Return WsDossier_HeuresSup
                End Get
            End Property

            Public ReadOnly Property VDossier_NewHeuresSup As Virtualia.Net.WebAppli.SelfV4.DossierNewHeureSup
                Get
                    If WsDossier_NewHeuresSup Is Nothing Then
                        WsDossier_NewHeuresSup = New Virtualia.Net.WebAppli.SelfV4.DossierNewHeureSup(Me)
                    End If
                    Return WsDossier_NewHeuresSup
                End Get
            End Property
            '*************AKR
            Public ReadOnly Property VListeJourAstreinte(ByVal DateDebut As String, ByVal DateFin As String) As List(Of Virtualia.TablesObjet.ShemaPER.PER_JOUR_ASTREINTE)
                Get
                    Dim LstRes As List(Of Virtualia.Systeme.MetaModele.VIR_FICHE)
                    Dim LstIde As List(Of Integer)
                    LstIde = New List(Of Integer)
                    LstIde.Add(Ide_Dossier)

                    LstRes = WsParent.SelectionObjetDate(LstIde, DateDebut, DateFin, 171)
                    If LstRes Is Nothing Then
                        Return Nothing
                    End If
                    Return WsParent.VirRhFonction.ConvertisseurListeFiches(Of Virtualia.TablesObjet.ShemaPER.PER_JOUR_ASTREINTE)(LstRes)
                End Get
            End Property
            '********************************************************************************************
            Public ReadOnly Property VDossier_Calendrier(ByVal DateDebut As String, ByVal DateFin As String) As List(Of Virtualia.Net.Controles.ItemJourCalendrier)
                Get
                    Dim LstPlanifie As List(Of Virtualia.Net.Controles.ItemJourCalendrier)
                    Dim LstAbs As List(Of Virtualia.Net.Controles.ItemJourCalendrier)
                    LstPlanifie = VDossier_TempsPartiel_Ajuste(DateDebut, DateFin)
                    LstAbs = VDossier_CalendrierAbs
                    If LstPlanifie Is Nothing OrElse LstPlanifie.Count = 0 Then
                        If LstAbs Is Nothing OrElse LstAbs.Count = 0 Then
                            Return Nothing
                        Else
                            LstPlanifie = LstAbs
                        End If
                    ElseIf LstAbs IsNot Nothing AndAlso LstAbs.Count > 0 Then
                        LstPlanifie.AddRange(LstAbs)
                    End If
                    Return LstPlanifie
                End Get
            End Property
            Public ReadOnly Property VDossier_CalendrierAbs As List(Of Controles.ItemJourCalendrier)
                Get
                    If WsDossier_Calendrier IsNot Nothing Then
                        Return WsDossier_Calendrier
                    End If
                    Dim IndiceI As Integer
                    Dim LstRes As List(Of Virtualia.Systeme.MetaModele.VIR_FICHE)
                    Dim TabRes As List(Of String)
                    Dim DateDebut As String = WsParent.VirRhDates.CalcDateMoinsJour(WsParent.VirRhDates.DateduJour, "0", "366")
                    Dim DateFin As String = WsParent.VirRhDates.CalcDateMoinsJour(WsParent.VirRhDates.DateduJour, "366", "0")
                    Dim FicheCal As Virtualia.Net.Controles.ItemJourCalendrier
                    Dim DateW As String

                    WsDossier_Calendrier = New List(Of Virtualia.Net.Controles.ItemJourCalendrier)
                    Dim LstIde As List(Of Integer)
                    LstIde = New List(Of Integer)
                    LstIde.Add(Ide_Dossier)

                    LstRes = WsParent.SelectionObjetDate(LstIde, DateDebut, DateFin, VI.ObjetPer.ObaAbsence)
                    If LstRes IsNot Nothing Then
                        For Each FichePER As Virtualia.TablesObjet.ShemaPER.PER_ABSENCE In LstRes
                            DateW = FichePER.Date_de_Valeur
                            Do
                                FicheCal = New Virtualia.Net.Controles.ItemJourCalendrier(DateW, FichePER.Nature, "", FichePER.VTypeJour(DateW), VI.ObjetPer.ObaAbsence)
                                WsDossier_Calendrier.Add(FicheCal)
                                DateW = WsParent.VirRhDates.CalcDateMoinsJour(DateW, "1", "0")
                                If WsParent.VirRhDates.DateTypee(DateW) > FichePER.Date_Fin_ToDate Then
                                    Exit Do
                                End If
                            Loop
                        Next
                    End If

                    LstRes = WsParent.SelectionObjetDate(LstIde, DateDebut, DateFin, VI.ObjetPer.ObaFormation)
                    If LstRes IsNot Nothing Then
                        For Each FichePER As Virtualia.TablesObjet.ShemaPER.PER_SESSION In LstRes
                            DateW = FichePER.Date_de_Valeur
                            Do
                                FicheCal = New Virtualia.Net.Controles.ItemJourCalendrier(DateW, FichePER.Intitule, FichePER.Intitule_de_la_session, VI.NumeroPlage.Jour_Formation, VI.ObjetPer.ObaFormation)
                                WsDossier_Calendrier.Add(FicheCal)
                                DateW = WsParent.VirRhDates.CalcDateMoinsJour(DateW, "1", "0")
                                If WsParent.VirRhDates.DateTypee(DateW) > FichePER.Date_Fin_ToDate Then
                                    Exit Do
                                End If
                            Loop
                        Next
                    End If

                    TabRes = WsParent.VirServiceServeur.RequeteSql_ToListeChar(WsParent.VirNomUtilisateur,
                                                        VI.PointdeVue.PVueApplicatif, VI.ObjetPer.ObaSaisieIntranet, RequeteSaisiesIntranet)
                    If TabRes IsNot Nothing Then
                        Dim FicheINTRA As Virtualia.TablesObjet.ShemaPER.PER_MAJINTRANET
                        Dim FichePER As Virtualia.TablesObjet.ShemaPER.PER_ABSENCE
                        For IndiceI = 0 To TabRes.Count - 1
                            FicheINTRA = New Virtualia.TablesObjet.ShemaPER.PER_MAJINTRANET
                            FicheINTRA.ContenuTable = TabRes.Item(IndiceI).ToString
                            FichePER = FicheINTRA.PointeurAbsence
                            If FichePER IsNot Nothing Then
                                DateW = FichePER.Date_de_Valeur
                                Do
                                    FicheCal = New Virtualia.Net.Controles.ItemJourCalendrier(DateW, FichePER.Nature, "", FichePER.VTypeJour(DateW), VI.ObjetPer.ObaAbsence)
                                    WsDossier_Calendrier.Add(FicheCal)
                                    DateW = WsParent.VirRhDates.CalcDateMoinsJour(DateW, "1", "0")
                                    If WsParent.VirRhDates.DateTypee(DateW) > FichePER.Date_Fin_ToDate Then
                                        Exit Do
                                    End If
                                Loop
                            End If
                        Next IndiceI
                    End If

                    Return WsDossier_Calendrier
                End Get
            End Property

            Private ReadOnly Property VDossier_TempsPartiel_Ajuste(ByVal DateDebut As String, ByVal DateFin As String) As List(Of Virtualia.Net.Controles.ItemJourCalendrier)
                Get
                    If DateDebut = "" Then
                        Return Nothing
                    End If
                    Dim LstFiches As List(Of Virtualia.Systeme.MetaModele.VIR_FICHE)
                    If WsLstCyclesTravail Is Nothing Then
                        LstFiches = WsParent.VirServiceServeur.LectureObjet_ToFiches(WsParent.VirNomUtilisateur, VI.PointdeVue.PVueApplicatif, VI.ObjetPer.ObaPresence, WsIde_Dossier, True)
                        WsLstCyclesTravail = New List(Of Virtualia.TablesObjet.ShemaPER.PER_TRAVAIL)
                        If LstFiches IsNot Nothing Then
                            For Each FichePER As Virtualia.Systeme.MetaModele.VIR_FICHE In LstFiches
                                WsLstCyclesTravail.Add(DirectCast(FichePER, Virtualia.TablesObjet.ShemaPER.PER_TRAVAIL))
                            Next
                        End If
                    End If
                    If WsLstAjustements Is Nothing Then
                        LstFiches = WsParent.VirServiceServeur.LectureObjet_ToFiches(WsParent.VirNomUtilisateur, VI.PointdeVue.PVueApplicatif, VI.ObjetPer.ObaModifPlanning, WsIde_Dossier, True)
                        WsLstAjustements = New List(Of Virtualia.TablesObjet.ShemaPER.PER_MODIF_PLANNING)
                        If LstFiches IsNot Nothing Then
                            For Each FichePER As Virtualia.Systeme.MetaModele.VIR_FICHE In LstFiches
                                WsLstAjustements.Add(DirectCast(FichePER, Virtualia.TablesObjet.ShemaPER.PER_MODIF_PLANNING))
                            Next
                        End If
                    End If
                    If WsLstCyclesTravail.Count = 0 Then
                        Return Nothing
                    End If
                    Dim LstRes As List(Of Virtualia.Net.Controles.ItemJourCalendrier)
                    Dim FicheTravail As Virtualia.TablesObjet.ShemaPER.PER_TRAVAIL
                    Dim FicheAjustement As Virtualia.TablesObjet.ShemaPER.PER_MODIF_PLANNING
                    Dim FicheCal As Virtualia.Net.Controles.ItemJourCalendrier
                    Dim DateW As String = DateDebut
                    Dim SiAfaire As Boolean
                    LstRes = New List(Of Virtualia.Net.Controles.ItemJourCalendrier)
                    Do
                        FicheCal = Nothing
                        SiAfaire = False
                        For Each FicheTravail In WsLstCyclesTravail
                            Select Case WsParent.VirRhDates.ComparerDates(FicheTravail.Date_de_Valeur, DateW)
                                Case VI.ComparaisonDates.Egalite, VI.ComparaisonDates.PlusPetit
                                    If FicheTravail.Date_de_Fin = "" Then
                                        SiAfaire = True
                                    Else
                                        Select Case WsParent.VirRhDates.ComparerDates(FicheTravail.Date_de_Fin, DateW)
                                            Case VI.ComparaisonDates.Egalite, VI.ComparaisonDates.PlusGrand
                                                SiAfaire = True
                                        End Select
                                    End If
                                    If SiAfaire = True Then
                                        FicheCal = WsParent.VirReferentiel.CyclePrevisionnel(DateW, FicheTravail.CycledeTravail, FicheTravail.Date_de_Valeur)
                                    End If
                                    Exit For
                            End Select
                        Next
                        If FicheCal IsNot Nothing Then
                            LstRes.Add(FicheCal)
                        End If
                        If WsLstAjustements.Count > 0 Then
                            For Each FicheAjustement In WsLstAjustements
                                Select Case WsParent.VirRhDates.ComparerDates(FicheAjustement.Date_de_Valeur, DateW)
                                    Case VI.ComparaisonDates.Egalite, VI.ComparaisonDates.PlusPetit
                                        Select Case WsParent.VirRhDates.ComparerDates(FicheAjustement.Date_de_Fin, DateW)
                                            Case VI.ComparaisonDates.Egalite, VI.ComparaisonDates.PlusGrand
                                                FicheCal = WsParent.VirReferentiel.CyclePrevisionnel(DateW, FicheAjustement.CycledeTravail, FicheAjustement.Date_de_Valeur)
                                        End Select
                                        Exit For
                                End Select
                            Next
                            If FicheCal IsNot Nothing Then
                                LstRes.Add(FicheCal)
                            End If
                        End If
                        DateW = WsParent.VirRhDates.CalcDateMoinsJour(DateW, "1", "0")
                        If WsParent.VirRhDates.DateTypee(DateW) > WsParent.VirRhDates.DateTypee(DateFin) Then
                            Exit Do
                        End If
                    Loop
                    If LstRes.Count = 0 Then
                        LstRes = Nothing
                    End If
                    Return LstRes
                End Get
            End Property

            Public Property Ide_Dossier() As Integer
                Get
                    Return WsIde_Dossier
                End Get
                Set(ByVal value As Integer)
                    WsIde_Dossier = value
                End Set
            End Property

            Public Property Login_AD() As String
                Get
                    Return WsLogin_AD
                End Get
                Set(ByVal value As String)
                    WsLogin_AD = value
                End Set
            End Property

            Public Property Nom() As String
                Get
                    Return WsNom
                End Get
                Set(ByVal value As String)
                    WsNom = value
                End Set
            End Property

            Public Property Prenom() As String
                Get
                    Return WsPrenom
                End Get
                Set(ByVal value As String)
                    WsPrenom = value
                End Set
            End Property

            Public Property Civilite() As String
                Get
                    Return WsCivilite
                End Get
                Set(ByVal value As String)
                    WsCivilite = value
                End Set
            End Property

            Public Property EMail() As String
                Get
                    Return WsEMail
                End Get
                Set(ByVal value As String)
                    WsEMail = value
                End Set
            End Property

            Public Property Ide_Manager() As Integer
                Get
                    If WsIde_Manager = 0 Then
                        Dim FichePER As Virtualia.TablesObjet.ShemaPER.PER_AFFECTATION = VFiche_Affectation(WsParent.VirRhDates.DateduJour)
                        Dim ManagerPer As Virtualia.Net.WebAppli.SelfV4.DossierPersonne = Nothing
                        Dim IndiceI As Integer
                        Dim StructureAff As String = ""
                        Dim IdeChef As Integer
                        If FichePER Is Nothing Then
                            Return 0
                        End If
                        For IndiceI = 5 To 1 Step -1
                            Select Case IndiceI
                                Case 1
                                    StructureAff = FichePER.Structure_de_1er_niveau
                                Case 2
                                    StructureAff = FichePER.Structure_de_2e_niveau
                                Case 3
                                    StructureAff = FichePER.Structure_de_3e_niveau
                                Case 4
                                    StructureAff = FichePER.Structure_de_4e_niveau
                                Case 5
                                    StructureAff = FichePER.Structure_de_5e_niveau
                            End Select
                            If StructureAff <> "" Then
                                IdeChef = WsParent.VirIdentifiantManager(WsIde_Dossier, IndiceI)
                                If IdeChef > 0 AndAlso IdeChef <> Ide_Dossier Then
                                    ManagerPer = WsParent.Dossier(IdeChef)
                                    If ManagerPer IsNot Nothing Then
                                        WsIde_Manager = IdeChef
                                        WsNiveau_Manager = IndiceI
                                        Nom_Prenom_Manager = ManagerPer.Nom & Strings.Space(1) & ManagerPer.Prenom
                                        EMail_Manager = ManagerPer.EMail
                                        Exit For
                                    End If
                                End If
                            End If
                        Next IndiceI
                    End If
                    Return WsIde_Manager
                End Get
                Set(ByVal value As Integer)
                    WsIde_Manager = value
                End Set
            End Property

            Public Property Nom_Prenom_Manager() As String
                Get
                    Return WsNomPrenom_Manager
                End Get
                Set(ByVal value As String)
                    If value.Length > 50 Then
                        WsNomPrenom_Manager = Strings.Left(value, 50)
                    Else
                        WsNomPrenom_Manager = value
                    End If
                End Set
            End Property

            Public Property EMail_Manager() As String
                Get
                    Return WsMail_Manager
                End Get
                Set(ByVal value As String)
                    WsMail_Manager = value
                End Set
            End Property

            Public ReadOnly Property Niveau_Manager As Integer
                Get
                    Return WsNiveau_Manager
                End Get
            End Property

            Public Property Ide_Delegue() As Integer
                Get
                    If WsIde_Delegue = 0 Then
                        Dim FichePER As Virtualia.TablesObjet.ShemaPER.PER_AFFECTATION = VFiche_Affectation(WsParent.VirRhDates.DateduJour)
                        Dim DeleguePer As Virtualia.Net.WebAppli.SelfV4.DossierPersonne = Nothing
                        Dim IndiceI As Integer
                        Dim StructureAff As String = ""
                        Dim IdeDelegue As Integer
                        If FichePER Is Nothing Then
                            Return 0
                        End If
                        For IndiceI = 5 To 1 Step -1
                            Select Case IndiceI
                                Case 1
                                    StructureAff = FichePER.Structure_de_1er_niveau
                                Case 2
                                    StructureAff = FichePER.Structure_de_2e_niveau
                                Case 3
                                    StructureAff = FichePER.Structure_de_3e_niveau
                                Case 4
                                    StructureAff = FichePER.Structure_de_4e_niveau
                                Case 5
                                    StructureAff = FichePER.Structure_de_5e_niveau
                            End Select
                            If StructureAff <> "" Then
                                IdeDelegue = WsParent.VirIdentifiantDelegue(WsIde_Dossier)
                                If IdeDelegue > 0 AndAlso IdeDelegue <> Ide_Dossier Then
                                    DeleguePer = WsParent.Dossier(IdeDelegue)
                                    If DeleguePer IsNot Nothing Then
                                        WsIde_Delegue = IdeDelegue
                                        Nom_Prenom_Delegue = DeleguePer.Nom & Strings.Space(1) & DeleguePer.Prenom
                                        EMail_Delegue = DeleguePer.EMail
                                        Exit For
                                    End If
                                End If
                            End If
                        Next IndiceI
                    End If
                    Return WsIde_Delegue
                End Get
                Set(ByVal value As Integer)
                    WsIde_Delegue = value
                End Set
            End Property

            Public Property Nom_Prenom_Delegue() As String
                Get
                    Return WsNomPrenom_Delegue
                End Get
                Set(ByVal value As String)
                    If value.Length > 50 Then
                        WsNomPrenom_Delegue = Strings.Left(value, 50)
                    Else
                        WsNomPrenom_Delegue = value
                    End If
                End Set
            End Property

            Public Property EMail_Delegue() As String
                Get
                    Return WsMail_Delegue
                End Get
                Set(ByVal value As String)
                    WsMail_Delegue = value
                End Set
            End Property

            Public ReadOnly Property Structure_Niveau1 As String
                Get
                    Dim FicheAff As Virtualia.TablesObjet.ShemaPER.PER_AFFECTATION = VFiche_Affectation(WsParent.VirRhDates.DateduJour)
                    If FicheAff Is Nothing Then
                        Return ""
                    End If
                    Return FicheAff.Structure_de_1er_niveau
                End Get
            End Property

            Public ReadOnly Property StructureSecondaire_Niveau1 As String
                Get
                    Dim FicheAff As Virtualia.TablesObjet.ShemaPER.PER_AFFECTATION_SECOND = VFiche_Affectation2nd(WsParent.VirRhDates.DateduJour)
                    If FicheAff Is Nothing Then
                        Return ""
                    End If
                    Return FicheAff.Structure_de_1er_niveau
                End Get
            End Property

            Private Function RequeteSaisiesIntranet() As String
                Dim Constructeur As Virtualia.Systeme.Sgbd.Sql.SqlInterne
                Dim ChaineSql As String
                Dim IndiceI As Integer

                Constructeur = New Virtualia.Systeme.Sgbd.Sql.SqlInterne(WsParent.VirModele, WsParent.VirInstanceBd)
                Constructeur.NombredeRequetes(VI.PointdeVue.PVueApplicatif, "01/01/2001", WsParent.VirRhDates.DateduJour, VI.Operateurs.ET) = 2
                Constructeur.SiPasdeTriSurIdeDossier = False
                Constructeur.SiHistoriquedeSituation = True
                Constructeur.IdentifiantDossier = WsIde_Dossier
                Constructeur.NoInfoSelection(0, VI.ObjetPer.ObaSaisieIntranet) = 15 'Objet
                Constructeur.ValeuraComparer(0, VI.Operateurs.OU, VI.Operateurs.Egalite, False) = CStr(VI.ObjetPer.ObaAbsence)
                Constructeur.NoInfoSelection(1, VI.ObjetPer.ObaSaisieIntranet) = 10 'Statut
                Constructeur.ValeuraComparer(1, VI.Operateurs.OU, VI.Operateurs.Egalite, False) = CStr(VI.DemandeIntranet.EnAttente)
                For IndiceI = 0 To 15
                    Constructeur.InfoExtraite(IndiceI, VI.ObjetPer.ObaSaisieIntranet, 0) = IndiceI + 1
                Next IndiceI
                ChaineSql = Constructeur.OrdreSqlDynamique
                Return ChaineSql
            End Function

            Public Sub New(ByVal Host As Virtualia.Net.Session.ObjetGlobal, ByVal Valeurs As String)
                WsParent = Host
                Dim TableauData(0) As String
                TableauData = Strings.Split(Valeurs, VI.Tild, -1)
                If TableauData.Count < 6 Then
                    Exit Sub
                End If
                Ide_Dossier = CInt(TableauData(0))
                Login_AD = TableauData(1)
                EMail = TableauData(2)
                Nom = TableauData(3)
                Prenom = TableauData(4)
                Civilite = TableauData(5)
            End Sub

        End Class
    End Namespace
End Namespace