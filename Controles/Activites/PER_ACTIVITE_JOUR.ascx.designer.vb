﻿'------------------------------------------------------------------------------
' <généré automatiquement>
'     Ce code a été généré par un outil.
'
'     Les modifications apportées à ce fichier peuvent provoquer un comportement incorrect et seront perdues si
'     le code est régénéré.
' </généré automatiquement>
'------------------------------------------------------------------------------

Option Strict On
Option Explicit On


Partial Public Class PER_ACTIVITE_JOUR
    
    '''<summary>
    '''Contrôle CadreInfo.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CadreInfo As Global.System.Web.UI.WebControls.Table
    
    '''<summary>
    '''Contrôle CadreTitre.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CadreTitre As Global.System.Web.UI.WebControls.Table
    
    '''<summary>
    '''Contrôle CellRetour.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CellRetour As Global.System.Web.UI.WebControls.TableCell
    
    '''<summary>
    '''Contrôle CommandeRetour.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CommandeRetour As Global.System.Web.UI.WebControls.ImageButton
    
    '''<summary>
    '''Contrôle Etiquette.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Etiquette As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle CellModeSaisie.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CellModeSaisie As Global.System.Web.UI.WebControls.TableCell
    
    '''<summary>
    '''Contrôle CadreSaisieSemaine.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CadreSaisieSemaine As Global.System.Web.UI.WebControls.Table
    
    '''<summary>
    '''Contrôle CadreListeSemaines.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CadreListeSemaines As Global.System.Web.UI.WebControls.Table
    
    '''<summary>
    '''Contrôle LabelListeSemaines.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LabelListeSemaines As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle VComboSemaines.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents VComboSemaines As Global.Virtualia.Net.Controles_VListeCombo

    '''<summary>
    '''Contrôle RadioSemaine.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents RadioSemaine As Global.Virtualia.Net.Controles_VTrioHorizontalRadio

    '''<summary>
    '''Contrôle CadrageActivite.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CadrageActivite As Global.System.Web.UI.WebControls.Table
    
    '''<summary>
    '''Contrôle LigneTotalActivite.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LigneTotalActivite As Global.System.Web.UI.WebControls.TableRow
    
    '''<summary>
    '''Contrôle TitreTotalAct.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents TitreTotalAct As Global.System.Web.UI.WebControls.TableCell
    
    '''<summary>
    '''Contrôle LabelTotalAct.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LabelTotalAct As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle CadreCommandes.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CadreCommandes As Global.System.Web.UI.WebControls.Table
    
    '''<summary>
    '''Contrôle CadreCmdNew.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CadreCmdNew As Global.System.Web.UI.WebControls.Table
    
    '''<summary>
    '''Contrôle CommandeNew.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CommandeNew As Global.System.Web.UI.WebControls.Button
    
    '''<summary>
    '''Contrôle CellCmdOK.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CellCmdOK As Global.System.Web.UI.WebControls.TableCell
    
    '''<summary>
    '''Contrôle CadreCmdOK.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CadreCmdOK As Global.System.Web.UI.WebControls.Table
    
    '''<summary>
    '''Contrôle CommandeOK.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CommandeOK As Global.System.Web.UI.WebControls.Button
    
    '''<summary>
    '''Contrôle CellCmdSigner.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CellCmdSigner As Global.System.Web.UI.WebControls.TableCell
    
    '''<summary>
    '''Contrôle CadreCmdSigner.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CadreCmdSigner As Global.System.Web.UI.WebControls.Table
    
    '''<summary>
    '''Contrôle CommandeSigner.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CommandeSigner As Global.System.Web.UI.WebControls.Button
    
    '''<summary>
    '''Contrôle CellModeControle.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CellModeControle As Global.System.Web.UI.WebControls.TableCell
    
    '''<summary>
    '''Contrôle CadreControle.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CadreControle As Global.System.Web.UI.WebControls.Table
    
    '''<summary>
    '''Contrôle CadreListeControles.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CadreListeControles As Global.System.Web.UI.WebControls.Table
    
    '''<summary>
    '''Contrôle VComboDatesDebut.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents VComboDatesDebut As Global.Virtualia.Net.Controles_VListeCombo
    
    '''<summary>
    '''Contrôle VComboDatesFin.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents VComboDatesFin As Global.Virtualia.Net.Controles_VListeCombo
    
    '''<summary>
    '''Contrôle CadreMode.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CadreMode As Global.System.Web.UI.WebControls.Table
    
    '''<summary>
    '''Contrôle CellCmdMode.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CellCmdMode As Global.System.Web.UI.WebControls.TableCell
    
    '''<summary>
    '''Contrôle CommandeMode.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CommandeMode As Global.System.Web.UI.WebControls.ImageButton
    
    '''<summary>
    '''Contrôle CommandePDF.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CommandePDF As Global.System.Web.UI.WebControls.ImageButton
    
    '''<summary>
    '''Contrôle CadreEdition.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CadreEdition As Global.System.Web.UI.WebControls.Table
    
    '''<summary>
    '''Contrôle CellEditionActivite.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CellEditionActivite As Global.System.Web.UI.WebControls.TableCell
    
    '''<summary>
    '''Contrôle CadreEditionActivite.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CadreEditionActivite As Global.System.Web.UI.WebControls.Table
    
    '''<summary>
    '''Contrôle CadreEnteteActivite.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CadreEnteteActivite As Global.System.Web.UI.WebControls.Table
    
    '''<summary>
    '''Contrôle LigneControleJourActivite.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LigneControleJourActivite As Global.System.Web.UI.WebControls.TableRow
    
    '''<summary>
    '''Contrôle CellDossier.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CellDossier As Global.System.Web.UI.WebControls.TableCell
    
    '''<summary>
    '''Contrôle EtiDossier.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents EtiDossier As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle ControleLundiActivite.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents ControleLundiActivite As Global.System.Web.UI.WebControls.TableCell
    
    '''<summary>
    '''Contrôle EtiTotal00.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents EtiTotal00 As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle ControleMardiActivite.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents ControleMardiActivite As Global.System.Web.UI.WebControls.TableCell
    
    '''<summary>
    '''Contrôle EtiTotal01.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents EtiTotal01 As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle ControleMercrediActivite.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents ControleMercrediActivite As Global.System.Web.UI.WebControls.TableCell
    
    '''<summary>
    '''Contrôle EtiTotal03.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents EtiTotal03 As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle ControleJeudiActivite.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents ControleJeudiActivite As Global.System.Web.UI.WebControls.TableCell
    
    '''<summary>
    '''Contrôle EtiTotal04.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents EtiTotal04 As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle ControleVendrediActivite.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents ControleVendrediActivite As Global.System.Web.UI.WebControls.TableCell
    
    '''<summary>
    '''Contrôle EtiTotal05.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents EtiTotal05 As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle ControleSamediActivite.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents ControleSamediActivite As Global.System.Web.UI.WebControls.TableCell
    
    '''<summary>
    '''Contrôle EtiTotal06.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents EtiTotal06 As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle ControleDimancheActivite.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents ControleDimancheActivite As Global.System.Web.UI.WebControls.TableCell
    
    '''<summary>
    '''Contrôle EtiTotal07.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents EtiTotal07 As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle ControleSemaineActivite.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents ControleSemaineActivite As Global.System.Web.UI.WebControls.TableCell
    
    '''<summary>
    '''Contrôle LabelSemaineControle.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LabelSemaineControle As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle LigneEnteteJourActivite.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LigneEnteteJourActivite As Global.System.Web.UI.WebControls.TableRow
    
    '''<summary>
    '''Contrôle CellTitreSemaine.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CellTitreSemaine As Global.System.Web.UI.WebControls.TableCell
    
    '''<summary>
    '''Contrôle LabelTitreSemaine.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LabelTitreSemaine As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle CellLundiActivite.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CellLundiActivite As Global.System.Web.UI.WebControls.TableCell
    
    '''<summary>
    '''Contrôle LabelLundiActivite.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LabelLundiActivite As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle CellMardiActivite.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CellMardiActivite As Global.System.Web.UI.WebControls.TableCell
    
    '''<summary>
    '''Contrôle LabelMardiActivite.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LabelMardiActivite As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle CellMercrediActivite.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CellMercrediActivite As Global.System.Web.UI.WebControls.TableCell
    
    '''<summary>
    '''Contrôle LabelMercrediActivite.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LabelMercrediActivite As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle CellJeudiActivite.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CellJeudiActivite As Global.System.Web.UI.WebControls.TableCell
    
    '''<summary>
    '''Contrôle LabelJeudiActivite.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LabelJeudiActivite As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle CellVendrediActivite.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CellVendrediActivite As Global.System.Web.UI.WebControls.TableCell
    
    '''<summary>
    '''Contrôle LabelVendrediActivite.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LabelVendrediActivite As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle CellSamediActivite.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CellSamediActivite As Global.System.Web.UI.WebControls.TableCell
    
    '''<summary>
    '''Contrôle LabelSamediActivite.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LabelSamediActivite As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle CellDimancheActivite.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CellDimancheActivite As Global.System.Web.UI.WebControls.TableCell
    
    '''<summary>
    '''Contrôle LabelDimancheActivite.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LabelDimancheActivite As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle CellSemaineActivite.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CellSemaineActivite As Global.System.Web.UI.WebControls.TableCell
    
    '''<summary>
    '''Contrôle LabelSemaineActivite.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LabelSemaineActivite As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle LigneEnteteDateActivite.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LigneEnteteDateActivite As Global.System.Web.UI.WebControls.TableRow
    
    '''<summary>
    '''Contrôle CellDateActivite.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CellDateActivite As Global.System.Web.UI.WebControls.TableCell
    
    '''<summary>
    '''Contrôle LabelDateActivite.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LabelDateActivite As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle CellDateLundiActivite.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CellDateLundiActivite As Global.System.Web.UI.WebControls.TableCell
    
    '''<summary>
    '''Contrôle LabelDateLundiActivite.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LabelDateLundiActivite As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle CellDateMardiActivite.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CellDateMardiActivite As Global.System.Web.UI.WebControls.TableCell
    
    '''<summary>
    '''Contrôle LabelDateMardiActivite.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LabelDateMardiActivite As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle CellDateMercrediActivite.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CellDateMercrediActivite As Global.System.Web.UI.WebControls.TableCell
    
    '''<summary>
    '''Contrôle LabelDateMercrediActivite.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LabelDateMercrediActivite As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle CellDateJeudiActivite.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CellDateJeudiActivite As Global.System.Web.UI.WebControls.TableCell
    
    '''<summary>
    '''Contrôle LabelDateJeudiActivite.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LabelDateJeudiActivite As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle CellDateVendrediActivite.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CellDateVendrediActivite As Global.System.Web.UI.WebControls.TableCell
    
    '''<summary>
    '''Contrôle LabelDateVendrediActivite.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LabelDateVendrediActivite As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle CellDateSamediActivite.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CellDateSamediActivite As Global.System.Web.UI.WebControls.TableCell
    
    '''<summary>
    '''Contrôle LabelDateSamediActivite.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LabelDateSamediActivite As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle CellDateDimancheActivite.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CellDateDimancheActivite As Global.System.Web.UI.WebControls.TableCell
    
    '''<summary>
    '''Contrôle LabelDateDimancheActivite.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LabelDateDimancheActivite As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle CellDateSemaineActivite.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CellDateSemaineActivite As Global.System.Web.UI.WebControls.TableCell
    
    '''<summary>
    '''Contrôle LabelDateSemaineActivite.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LabelDateSemaineActivite As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle LigneEnteteDatePlanningAM.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LigneEnteteDatePlanningAM As Global.System.Web.UI.WebControls.TableRow
    
    '''<summary>
    '''Contrôle CellPlanningAM.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CellPlanningAM As Global.System.Web.UI.WebControls.TableCell
    
    '''<summary>
    '''Contrôle LabelPlanningAM.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LabelPlanningAM As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle CellLundiAM.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CellLundiAM As Global.System.Web.UI.WebControls.TableCell
    
    '''<summary>
    '''Contrôle CalAM00.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CalAM00 As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle CellMardiAM.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CellMardiAM As Global.System.Web.UI.WebControls.TableCell
    
    '''<summary>
    '''Contrôle CalAM01.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CalAM01 As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle CellMercrediAM.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CellMercrediAM As Global.System.Web.UI.WebControls.TableCell
    
    '''<summary>
    '''Contrôle CalAM02.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CalAM02 As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle CellJeudiAM.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CellJeudiAM As Global.System.Web.UI.WebControls.TableCell
    
    '''<summary>
    '''Contrôle CalAM03.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CalAM03 As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle CellVendrediAM.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CellVendrediAM As Global.System.Web.UI.WebControls.TableCell
    
    '''<summary>
    '''Contrôle CalAM04.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CalAM04 As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle CellSamediAM.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CellSamediAM As Global.System.Web.UI.WebControls.TableCell
    
    '''<summary>
    '''Contrôle CalAM05.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CalAM05 As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle CellDimancheAM.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CellDimancheAM As Global.System.Web.UI.WebControls.TableCell
    
    '''<summary>
    '''Contrôle CalAM06.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CalAM06 As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle CellDateSemainePlanningAM.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CellDateSemainePlanningAM As Global.System.Web.UI.WebControls.TableCell
    
    '''<summary>
    '''Contrôle LabelDateSemainePlanningAM.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LabelDateSemainePlanningAM As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle LigneEnteteDatePlanningPM.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LigneEnteteDatePlanningPM As Global.System.Web.UI.WebControls.TableRow
    
    '''<summary>
    '''Contrôle CellPlanningPM.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CellPlanningPM As Global.System.Web.UI.WebControls.TableCell
    
    '''<summary>
    '''Contrôle LabelPlanningPM.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LabelPlanningPM As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle CellLundiPM.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CellLundiPM As Global.System.Web.UI.WebControls.TableCell
    
    '''<summary>
    '''Contrôle CalPM00.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CalPM00 As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle CellMardiPM.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CellMardiPM As Global.System.Web.UI.WebControls.TableCell
    
    '''<summary>
    '''Contrôle CalPM01.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CalPM01 As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle CellMercrediPM.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CellMercrediPM As Global.System.Web.UI.WebControls.TableCell
    
    '''<summary>
    '''Contrôle CalPM02.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CalPM02 As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle CellJeudiPM.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CellJeudiPM As Global.System.Web.UI.WebControls.TableCell
    
    '''<summary>
    '''Contrôle CalPM03.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CalPM03 As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle CellVendrediPM.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CellVendrediPM As Global.System.Web.UI.WebControls.TableCell
    
    '''<summary>
    '''Contrôle CalPM04.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CalPM04 As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle CellSamediPM.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CellSamediPM As Global.System.Web.UI.WebControls.TableCell
    
    '''<summary>
    '''Contrôle CalPM05.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CalPM05 As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle CellDimanchePM.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CellDimanchePM As Global.System.Web.UI.WebControls.TableCell
    
    '''<summary>
    '''Contrôle CalPM06.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CalPM06 As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle CellDateSemainePlanningPM.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CellDateSemainePlanningPM As Global.System.Web.UI.WebControls.TableCell
    
    '''<summary>
    '''Contrôle LabelDateSemainePlanningPM.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LabelDateSemainePlanningPM As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle CellSaisie.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CellSaisie As Global.System.Web.UI.WebControls.TableCell
    
    '''<summary>
    '''Contrôle CadreSaisie.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CadreSaisie As Global.Virtualia.Net.CtlSaisieActivite
    
    '''<summary>
    '''Contrôle CellLecture.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CellLecture As Global.System.Web.UI.WebControls.TableCell
    
    '''<summary>
    '''Contrôle CadreLecture.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CadreLecture As Global.Virtualia.Net.CtlLectureActivite
    
    '''<summary>
    '''Contrôle CellEditionControle.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CellEditionControle As Global.System.Web.UI.WebControls.TableCell
    
    '''<summary>
    '''Contrôle ListeStatutSemaine.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents ListeStatutSemaine As Global.Virtualia.Net.Controles_VListeGrid
    
    '''<summary>
    '''Contrôle ListeRecap.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents ListeRecap As Global.Virtualia.Net.Controles_VListeGrid
End Class
