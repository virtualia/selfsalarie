﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="CtrlExportGraphique1.ascx.vb" Inherits="Virtualia.Net.CtrlExportGraphique1" %>

<asp:Table ID="TB1" runat="server" Width="768px" Font-Names="Trebuchet MS" Font-Size="Small" Font-Bold="false" BackColor="White">

    <asp:TableRow VerticalAlign="Top">
        <asp:TableCell HorizontalAlign="Center">
            <asp:Table ID="Table2" runat="server" HorizontalAlign="Center">
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center" ColumnSpan="3">
                        <asp:Label ID="Label3" runat="server" Text="Répartition par statut" Height="22px" Width="500px" BackColor="White" BorderColor="Black" BorderStyle="Notset" BorderWidth="2px" ForeColor="#111111" Style="font-style: oblique; text-indent: 5px; text-align: center;" />
                    </asp:TableCell>
                </asp:TableRow>

                <asp:TableRow>
                    <asp:TableCell>
                        <asp:Table ID="Table3" runat="server" HorizontalAlign="Center" BorderStyle="Ridge" BorderWidth="2px" BorderColor="Black">
                            <asp:TableRow>
                                <asp:TableCell ID="Statut_NB" HorizontalAlign="Center">
                                    <asp:Image ID="IMG_Statut_NB" runat="server" Height="226px" Width="340px" ImageUrl="http://localhost/Graphiques/Test.jpg"/>
                                    <%--<img  alt="" src="http://localhost/Graphiques/Test.jpg" style="vertical-align: central; height: 226px; width: 340px" />--%>
                                </asp:TableCell>
                                <asp:TableCell ID="Statut_Cout" HorizontalAlign="Center">
                                    <asp:Image ID="IMG_Statut_Cout" runat="server" Height="226px" Width="340px" ImageUrl="http://localhost/Graphiques/Test.jpg"/>
                                    <%--<img  alt="" src="http://localhost/Graphiques/Test.jpg" style="vertical-align: central; height: 226px; width: 340px" />--%>
                                </asp:TableCell>
                                <asp:TableCell ID="Statut_Jour" HorizontalAlign="Center">
                                    <asp:Image ID="IMG_Statut_Jour" runat="server" Height="226px" Width="340px" ImageUrl="http://localhost/Graphiques/Test.jpg"/>
                                    <%--<img  alt="" src="http://localhost/Graphiques/Test.jpg" style="vertical-align: central; height: 226px; width: 340px" />--%>
                                </asp:TableCell>
                            </asp:TableRow>

                        </asp:Table>
                    </asp:TableCell>
                </asp:TableRow>
            </asp:Table>
        </asp:TableCell>
    </asp:TableRow>

    <asp:TableRow VerticalAlign="Top">
        <asp:TableCell HorizontalAlign="Center">
            <asp:Table ID="Table4" runat="server" HorizontalAlign="Center">
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center" ColumnSpan="3">
                        <asp:Label ID="Label5" runat="server" Text="Répartition par domaine" Height="22px" Width="500px" BackColor="White" BorderColor="Black" BorderStyle="Notset" BorderWidth="2px" ForeColor="#111111" Style="font-style: oblique; text-indent: 5px; text-align: center;" />
                    </asp:TableCell>
                </asp:TableRow>

                <asp:TableRow>
                    <asp:TableCell>
                        <asp:Table ID="Table5" runat="server" HorizontalAlign="Center" BorderStyle="Ridge" BorderWidth="2px" BorderColor="Black">
                            <asp:TableRow>
                                <asp:TableCell ID="Domaine_NB" HorizontalAlign="Center">
                                    <asp:Image ID="IMG_Domaine_NB" runat="server" Height="226px" Width="340px" ImageUrl="http://localhost/Graphiques/Test.jpg"/>
                                    <%--<img  alt="" src="http://localhost/Graphiques/Test.jpg" style="vertical-align: central; height: 226px; width: 340px" />--%>
                                </asp:TableCell>
                                <asp:TableCell ID="Domaine_Cout" HorizontalAlign="Center">
                                    <asp:Image ID="IMG_Domaine_Cout" runat="server" Height="226px" Width="340px" ImageUrl="http://localhost/Graphiques/Test.jpg"/>
                                    <%--<img  alt="" src="http://localhost/Graphiques/Test.jpg" style="vertical-align: central; height: 226px; width: 340px" />--%>
                                </asp:TableCell>
                                <asp:TableCell ID="Domaine_Jour" HorizontalAlign="Center">
                                    <asp:Image ID="IMG_Domaine_Jour" runat="server" Height="226px" Width="340px" ImageUrl="http://localhost/Graphiques/Test.jpg"/>
                                    <%--<img  alt="" src="http://localhost/Graphiques/Test.jpg" style="vertical-align: central; height: 226px; width: 340px" />--%>
                                </asp:TableCell>
                            </asp:TableRow>
                        </asp:Table>
                    </asp:TableCell>
                </asp:TableRow>
            </asp:Table>
        </asp:TableCell>
    </asp:TableRow>

    <asp:TableRow VerticalAlign="Bottom">
        <asp:TableCell HorizontalAlign="Right">
            <asp:Label ID="lblNumPage" runat="server" Text="Virtualia.net Page " Width="500px" BackColor="White" BorderStyle="None" ForeColor="LightGray" Style="font-style: oblique; text-indent: 5px; text-align: right; margin-right: 10px" />
        </asp:TableCell>
    </asp:TableRow>

</asp:Table>