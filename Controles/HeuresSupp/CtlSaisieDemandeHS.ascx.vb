﻿Option Strict On
Option Explicit On
Option Compare Text
Imports VI = Virtualia.Systeme.Constantes
Public Class CtlSaisieDemandeHS
    Inherits System.Web.UI.UserControl
    Public Delegate Sub Retour_MsgEventHandler(ByVal sender As Object, ByVal e As Virtualia.Systeme.Evenements.DonneeChangeEventArgs)
    Public Event ValeurRetour As Retour_MsgEventHandler
    Private WebFct As Virtualia.Net.Controles.WebFonctions
    Private WsDossierHS As Virtualia.Net.WebAppli.SelfV4.DossierNewHeureSup

    Protected Overridable Sub ReponseRetour(ByVal e As Virtualia.Systeme.Evenements.DonneeChangeEventArgs)
        RaiseEvent ValeurRetour(Me, e)
    End Sub

    Public Property V_DateValeur As String
        Get
            Return EtiMoisValeur.ToolTip
        End Get
        Set(value As String)
            EtiMoisValeur.ToolTip = value
            EtiMoisValeur.Text = "Mois de - " & Strings.Format(V_WebFonction.ViRhDates.MoisEnClair(CDate(value).Month)) & Strings.Space(1) & CDate(value).Year
            Call LireLaFiche()
        End Set
    End Property

    Private ReadOnly Property V_WebFonction As Virtualia.Net.Controles.WebFonctions
        Get
            If WebFct Is Nothing Then
                WebFct = New Virtualia.Net.Controles.WebFonctions(Me, 0)
            End If
            Return WebFct
        End Get
    End Property

    Private ReadOnly Property V_UtiSession As Virtualia.Net.Session.SessionModule
        Get
            Return CType(V_WebFonction.PointeurUtilisateur.V_PointeurSessionModule, Virtualia.Net.Session.SessionModule)
        End Get
    End Property

    Private Sub LireLaFiche()
        Dim NumObjet As Integer
        Dim NumInfo As Integer
        Dim Ctl As Control
        Dim IndiceI As Integer = 0

        Dim VirControle As Controles_VCoupleEtiDonnee
        Do
            Ctl = V_WebFonction.VirWebControle(Me.CadreInfo, "InfoH", IndiceI)
            If Ctl Is Nothing Then
                Exit Do
            End If
            NumInfo = CInt(Strings.Right(Ctl.ID, 2))
            VirControle = CType(Ctl, Controles_VCoupleEtiDonnee)
            NumObjet = VirControle.V_Objet
            If VirControle.V_SiDonneeDico = True Then
                Select Case NumInfo
                    Case 20 To 25
                        VirControle.DonTooltip = ValeurLue(NumInfo)
                        VirControle.DonText = ConvertCentiemeEnHeureMinute(V_WebFonction.ViRhFonction.ConversionDouble(ValeurLue(NumInfo)))
                    Case Else
                        VirControle.DonText = ValeurLue(NumInfo)
                End Select
            Else
                VirControle.DonText = ""
            End If
            IndiceI += 1
        Loop
        If V_UtiSession Is Nothing Then
            Exit Sub
        End If
        CellSelf.Visible = True
        CellManager.Visible = False
        CellDRH.Visible = False
        If V_UtiSession.SiAccesGRH = True Then
            CellDRH.Visible = True
            CellSelf.Visible = False
            CellManager.Visible = False
        ElseIf V_UtiSession.SiAccesManager = True Then
            If Session.Item("Manager") IsNot Nothing Then
                CellManager.Visible = True
                CellSelf.Visible = False
                CellDRH.Visible = False
            End If
        End If
    End Sub

    Protected Sub InfoH_ValeurChange(ByVal sender As Object, ByVal e As Virtualia.Systeme.Evenements.DonneeChangeEventArgs) Handles InfoH10.ValeurChange,
        InfoH14.ValeurChange, InfoH18.ValeurChange, InfoH19.ValeurChange

        If V_UtiSession Is Nothing Then
            Exit Sub
        End If
        WsDossierHS = V_UtiSession.DossierPER.VDossier_NewHeuresSup
        If WsDossierHS Is Nothing Then
            Exit Sub
        End If
        Dim NumInfo As Integer = CInt(Strings.Right(CType(sender, Controles_VCoupleEtiDonnee).ID, 2))
        If WsDossierHS.Donnee_FicheMensuel_HS(NumInfo, EtiMoisValeur.ToolTip) <> e.Valeur Then
            '** Controles de cohérence
            '**
            CType(sender, Controles_VCoupleEtiDonnee).DonBackColor = V_WebFonction.CouleurMaj
            If NumInfo = 19 Then 'Nombre Heures demandées totales
                Call CalculerDecomposition(WsDossierHS.ListeDetaille_HS_Validee(CDate(EtiMoisValeur.ToolTip).Year, CDate(EtiMoisValeur.ToolTip).Month), V_WebFonction.ViRhFonction.ConversionDouble(e.Valeur, 2))
                WsDossierHS.Donnee_FicheMensuel_HS(NumInfo, EtiMoisValeur.ToolTip) = InfoH19.DonText
            Else
                WsDossierHS.Donnee_FicheMensuel_HS(NumInfo, EtiMoisValeur.ToolTip) = e.Valeur
            End If
        End If
    End Sub

    Private Sub CmdCancel_Click(sender As Object, e As EventArgs) Handles CmdCancel.Click
        Dim Evenement As Virtualia.Systeme.Evenements.DonneeChangeEventArgs
        Evenement = New Virtualia.Systeme.Evenements.DonneeChangeEventArgs("")
        ReponseRetour(Evenement)
    End Sub

    Private Sub CommandeOK_Click(sender As Object, e As EventArgs) Handles CommandeOK.Click
        Dim Evenement As Virtualia.Systeme.Evenements.DonneeChangeEventArgs
        Dim SiOK As Boolean = False
        Dim SiSelf As Boolean = True

        Evenement = New Virtualia.Systeme.Evenements.DonneeChangeEventArgs("")
        WsDossierHS = V_UtiSession.DossierPER.VDossier_NewHeuresSup
        If WsDossierHS Is Nothing Then
            ReponseRetour(Evenement)
            Exit Sub
        End If
        Dim UtiSession As Virtualia.Net.Session.SessionModule = CType(V_WebFonction.PointeurUtilisateur.V_PointeurSessionModule, Virtualia.Net.Session.SessionModule)
        If UtiSession Is Nothing Then
            ReponseRetour(Evenement)
            Exit Sub
        End If
        Dim FicheMensuel As Virtualia.TablesObjet.ShemaPER.PER_MENSUEL_HS = UtiSession.DossierPER.VDossier_NewHeuresSup.FicheMensuel_HS(CDate(EtiMoisValeur.ToolTip).Year, CDate(EtiMoisValeur.ToolTip).Month)
        If FicheMensuel Is Nothing Then
            ReponseRetour(Evenement)
            Exit Sub
        End If
        FicheMensuel.NombreHeures_Totales_DemandeAPayer = V_WebFonction.ViRhFonction.ConversionDouble(InfoH19.DonText, 2)
        Call CalculerDecomposition(WsDossierHS.ListeDetaille_HS_Validee(CDate(EtiMoisValeur.ToolTip).Year, CDate(EtiMoisValeur.ToolTip).Month), V_WebFonction.ViRhFonction.ConversionDouble(InfoH19.DonText, 2))
        FicheMensuel.NombreHeures_DemandeAPayer_Jour_T1 = V_WebFonction.ViRhFonction.ConversionDouble(InfoH20.DonTooltip, 2)
        FicheMensuel.NombreHeures_DemandeAPayer_Jour_T2 = V_WebFonction.ViRhFonction.ConversionDouble(InfoH23.DonTooltip, 2)
        FicheMensuel.NombreHeures_DemandeAPayer_Nuit_T1 = V_WebFonction.ViRhFonction.ConversionDouble(InfoH21.DonTooltip, 2)
        FicheMensuel.NombreHeures_DemandeAPayer_Nuit_T2 = V_WebFonction.ViRhFonction.ConversionDouble(InfoH24.DonTooltip, 2)
        FicheMensuel.NombreHeures_DemandeAPayer_JF_T1 = V_WebFonction.ViRhFonction.ConversionDouble(InfoH22.DonTooltip, 2)
        FicheMensuel.NombreHeures_DemandeAPayer_JF_T2 = V_WebFonction.ViRhFonction.ConversionDouble(InfoH25.DonTooltip, 2)
        If UtiSession.SiAccesGRH = True Then
            FicheMensuel.Date_Visa_GRH = V_WebFonction.ViRhDates.DateduJour
            FicheMensuel.Signataire_GRH = UtiSession.VParent.V_NomdeConnexion
            FicheMensuel.Observations_GRH = InfoH18.DonText
            If OptionVisaDRH.RadioGaucheCheck = True Then
                FicheMensuel.Nature_Visa_GRH = "Accord"
            Else
                FicheMensuel.Nature_Visa_GRH = "Refus"
            End If
            SiSelf = False
        ElseIf UtiSession.SiAccesManager = True Then
            If Session.Item("Manager") IsNot Nothing Then
                FicheMensuel.Date_Visa_Manager = V_WebFonction.ViRhDates.DateduJour
                FicheMensuel.Signataire_Manager = UtiSession.VParent.V_NomdeConnexion
                FicheMensuel.Observations_Manager = InfoH14.DonText
                If OptionManager.RadioGaucheCheck = True Then
                    FicheMensuel.Nature_Visa_Manager = "Accord"
                Else
                    FicheMensuel.Nature_Visa_Manager = "Refus"
                End If
                SiSelf = False
            End If
        End If
        If SiSelf = True Then
            FicheMensuel.Date_Demande = V_WebFonction.ViRhDates.DateduJour
            FicheMensuel.Observations_Demande = InfoH10.DonText
        End If
        SiOK = V_WebFonction.PointeurGlobal.VirServiceServeur.MiseAjour_Fiche(V_WebFonction.PointeurGlobal.VirNomUtilisateur, VI.PointdeVue.PVueApplicatif,
                       VI.ObjetPer.ObaMensuelHS, FicheMensuel.Ide_Dossier, "M", FicheMensuel.FicheLue, FicheMensuel.ContenuTable, True)

        If SiOK = True And SiSelf = True Then
            Call EnvoyerMail_Emanager(FicheMensuel.NombreHeures_Totales_DemandeAPayer)
        End If
        If UtiSession.SiAccesGRH = False Then
            ReponseRetour(Evenement)
            Exit Sub
        End If

        If SiOK = True And FicheMensuel.Nature_Visa_GRH = "Accord" Then
            Dim FicheRemu As Virtualia.TablesObjet.ShemaPER.PER_HEURESUP
            Dim CodeMaj As String = "C"
            Dim ChainagePrecedent As String = ""

            FicheRemu = UtiSession.DossierPER.VDossier_NewHeuresSup.FicheHs_Remuneree(CDate(EtiMoisValeur.ToolTip).Year, CDate(EtiMoisValeur.ToolTip).Month)
            If FicheRemu Is Nothing OrElse FicheRemu.Date_de_Valeur <> EtiMoisValeur.ToolTip Then
                FicheRemu = New Virtualia.TablesObjet.ShemaPER.PER_HEURESUP
            Else
                CodeMaj = "M"
                ChainagePrecedent = FicheRemu.Certification
            End If
            FicheRemu.Date_de_Valeur = EtiMoisValeur.ToolTip
            FicheRemu.Date_de_Fin = V_WebFonction.ViRhDates.DateSaisieVerifiee("31" & Strings.Right(EtiMoisValeur.ToolTip, 8))
            FicheRemu.Ide_Dossier = FicheMensuel.Ide_Dossier
            FicheRemu.Tranche1_Nombre = FicheMensuel.NombreHeures_DemandeAPayer_Jour_T1
            FicheRemu.Tranche2_Nombre = FicheMensuel.NombreHeures_DemandeAPayer_Jour_T2
            FicheRemu.Tranche3_Nombre = FicheMensuel.NombreHeures_DemandeAPayer_JF_T1
            FicheRemu.Tranche4_Nombre = FicheMensuel.NombreHeures_DemandeAPayer_Nuit_T1
            FicheRemu.Tranche5_Nombre = FicheMensuel.NombreHeures_DemandeAPayer_JF_T2
            FicheRemu.Tranche6_Nombre = FicheMensuel.NombreHeures_DemandeAPayer_Nuit_T2
            FicheRemu.Date_Certification = V_WebFonction.ViRhDates.DateduJour
            FicheRemu.Commentaire = "Visa DRH = " & FicheMensuel.Signataire_GRH
            '*** Calcul Taux et Montant
            If UtiSession.DossierPER.VFiche_Grade(EtiMoisValeur.ToolTip) IsNot Nothing AndAlso UtiSession.DossierPER.VFiche_Grade(EtiMoisValeur.ToolTip).Indice_majore <> "" Then
                Dim ValPoint As Double = CType(V_WebFonction.PointeurGlobal, Virtualia.Net.Session.ObjetGlobal).VirReferentiel.ValeurAnnuelleI100(EtiMoisValeur.ToolTip)
                Dim TauxResid As Double = CType(V_WebFonction.PointeurGlobal, Virtualia.Net.Session.ObjetGlobal).VirReferentiel.TauxZoneResidence
                Dim IPlancher As Integer = CType(V_WebFonction.PointeurGlobal, Virtualia.Net.Session.ObjetGlobal).VirReferentiel.IndicePlancherResidence
                FicheRemu.Indice_SecteurPublic(ValPoint, TauxResid, IPlancher) = CInt(UtiSession.DossierPER.VFiche_Grade(EtiMoisValeur.ToolTip).Indice_majore)
            End If
            '** Certification
            Dim ObjetCertification As Virtualia.Net.Session.ObjetTransaction
            Dim ChaineCryptee As String
            ObjetCertification = New Virtualia.Net.Session.ObjetTransaction(V_WebFonction.PointeurGlobal)
            ObjetCertification.ChainageOrigine(Session.SessionID, "SelfV4.CtlSaisieDemandeHS") = ChainagePrecedent
            ChaineCryptee = ObjetCertification.CreerTransactionCertifiee(VI.ObjetPer.ObaHeuresSup, FicheRemu.Ide_Dossier, FicheRemu.V_Donnees_Significatives)
            FicheRemu.Certification = ChaineCryptee
            '***************
            SiOK = V_WebFonction.PointeurGlobal.VirServiceServeur.MiseAjour_Fiche(V_WebFonction.PointeurGlobal.VirNomUtilisateur, VI.PointdeVue.PVueApplicatif,
                           VI.ObjetPer.ObaHeuresSup, FicheRemu.Ide_Dossier, CodeMaj, FicheRemu.FicheLue, FicheRemu.ContenuTable, True)
        End If
        Call EnvoyerMail_Salarie(FicheMensuel.NombreHeures_Totales_DemandeAPayer, FicheMensuel.Nature_Visa_GRH)
        ReponseRetour(Evenement)
    End Sub

    Private ReadOnly Property ValeurLue(ByVal NoInfo As Integer) As String
        Get
            If V_UtiSession Is Nothing Then
                Return ""
            End If
            WsDossierHS = V_UtiSession.DossierPER.VDossier_NewHeuresSup
            If WsDossierHS Is Nothing Then
                Return ""
            End If
            Return WsDossierHS.Donnee_FicheMensuel_HS(NoInfo, EtiMoisValeur.ToolTip)
        End Get
    End Property

    Private Sub CalculerDecomposition(ByVal LstMensuel As List(Of Virtualia.TablesObjet.ShemaPER.PER_DETAIL_HS), ByVal NbTotal As Double)
        If LstMensuel Is Nothing OrElse LstMensuel.Count = 0 Then
            Exit Sub
        End If
        InfoH20.DonTooltip = ""
        InfoH21.DonTooltip = ""
        InfoH22.DonTooltip = ""
        InfoH23.DonTooltip = ""
        InfoH24.DonTooltip = ""
        InfoH25.DonTooltip = ""
        Dim TotalListe As Double = 0
        Dim TotalCourant As Double = 0
        Dim NbJourJusqua14 As Double = 0
        Dim NbNuitJusqua14 As Double = 0
        Dim NbDJFJusqua14 As Double = 0
        Dim NbJourJusqua25 As Double = 0
        Dim NbNuitJusqua25 As Double = 0
        Dim NbDJFJusqua25 As Double = 0
        For Each FichePER In LstMensuel
            TotalCourant = FichePER.Nbheures_Jusqua20h + FichePER.Nbheures_De20ha22h
            Select Case FichePER.SiJourOuvrable
                Case False
                    TotalCourant += FichePER.Nbheures_Trajet
            End Select
            If TotalListe + TotalCourant > NbTotal Then
                TotalCourant = Math.Round(NbTotal - TotalListe, 2)
            End If
            Select Case FichePER.SiJourOuvrable
                Case True
                    Select Case NbJourJusqua14
                        Case < 14
                            NbJourJusqua14 += TotalCourant
                            If NbJourJusqua14 > 14 Then
                                NbJourJusqua25 = NbJourJusqua14 - 14
                                NbJourJusqua14 = 14
                            End If
                        Case 14 To 25
                            NbJourJusqua25 += TotalCourant
                            If NbJourJusqua25 > 11 Then
                                NbJourJusqua25 = 11
                            End If
                    End Select
                Case False
                    Select Case NbDJFJusqua14
                        Case < 14
                            NbDJFJusqua14 += TotalCourant
                            If NbDJFJusqua14 > 14 Then
                                NbDJFJusqua25 = NbDJFJusqua14 - 14
                                NbDJFJusqua14 = 14
                            End If
                        Case 14 To 25
                            NbDJFJusqua25 += TotalCourant
                            If NbDJFJusqua25 > 11 Then
                                NbDJFJusqua25 = 11
                            End If
                    End Select
            End Select
            TotalListe += TotalCourant
            TotalCourant = FichePER.Nbheures_Nuit
            If TotalListe + TotalCourant > NbTotal Then
                TotalCourant = Math.Round(NbTotal - TotalListe, 2)
            End If
            Select Case NbNuitJusqua14
                Case < 14
                    NbNuitJusqua14 += TotalCourant
                    If NbNuitJusqua14 > 14 Then
                        NbNuitJusqua25 = NbNuitJusqua14 - 14
                        NbNuitJusqua14 = 14
                    End If
                Case 14 To 25
                    NbNuitJusqua25 += TotalCourant
                    If NbNuitJusqua25 > 11 Then
                        NbNuitJusqua25 = 11
                    End If
            End Select
            TotalListe += TotalCourant
            If TotalListe >= NbTotal Then
                Exit For
            End If
        Next
        InfoH20.DonTooltip = Strings.Format(NbJourJusqua14, "#.##")
        InfoH21.DonTooltip = Strings.Format(NbNuitJusqua14, "#.##")
        InfoH22.DonTooltip = Strings.Format(NbDJFJusqua14, "#.##")
        InfoH23.DonTooltip = Strings.Format(NbJourJusqua25, "#.##")
        InfoH24.DonTooltip = Strings.Format(NbNuitJusqua25, "#.##")
        InfoH25.DonTooltip = Strings.Format(NbDJFJusqua25, "#.##")
        InfoH20.DonText = ConvertCentiemeEnHeureMinute(NbJourJusqua14)
        InfoH21.DonText = ConvertCentiemeEnHeureMinute(NbNuitJusqua14)
        InfoH22.DonText = ConvertCentiemeEnHeureMinute(NbDJFJusqua14)
        InfoH23.DonText = ConvertCentiemeEnHeureMinute(NbJourJusqua25)
        InfoH24.DonText = ConvertCentiemeEnHeureMinute(NbNuitJusqua25)
        InfoH25.DonText = ConvertCentiemeEnHeureMinute(NbDJFJusqua25)
        If TotalListe < NbTotal Then
            InfoH19.DonText = Strings.Format(TotalListe, "#.##")
        End If
    End Sub

    Private Sub EnvoyerMail_Emanager(ByVal NbHeures As Double)
        Dim DossierManager As Virtualia.Net.WebAppli.SelfV4.DossierPersonne = CType(V_WebFonction.PointeurGlobal, Virtualia.Net.Session.ObjetGlobal).Dossier(CType(V_WebFonction.PointeurUtilisateur.V_PointeurSessionModule, Virtualia.Net.Session.SessionModule).DossierPER.Ide_Manager)
        If DossierManager Is Nothing OrElse DossierManager.EMail = "" Then
            Exit Sub
        End If
        Dim Sujet As String = "Demande de rémunération d'heures supplémentaires"
        Dim Msg As String = "Bonjour, " & vbCrLf
        Msg &= "Pour votre information, " & CType(V_WebFonction.PointeurUtilisateur.V_PointeurSessionModule, Virtualia.Net.Session.SessionModule).DossierPER.Prenom & Strings.Space(1) & CType(V_WebFonction.PointeurUtilisateur.V_PointeurSessionModule, Virtualia.Net.Session.SessionModule).DossierPER.Nom
        Msg &= " a déposé une demande de rémunération de " & NbHeures & " heures supplémentaires." & vbCrLf
        Msg &= "Une réponse de votre part est attendue dans Virtualia.net (" & System.Configuration.ConfigurationManager.AppSettings("UrlSelfV3") & ")." & vbCrLf
        Msg &= "Ce message est généré automatiquement par l’application Virtualia.net à la suite d’une demande de rémunération."
        Call CType(V_WebFonction.PointeurGlobal, Virtualia.Net.Session.ObjetGlobal).EnvoyerEmail(Sujet, "", DossierManager.EMail, Msg)
        If DossierManager.Ide_Delegue > 0 AndAlso DossierManager.EMail_Delegue <> "" Then
            Call CType(V_WebFonction.PointeurGlobal, Virtualia.Net.Session.ObjetGlobal).EnvoyerEmail(Sujet, "", DossierManager.EMail_Delegue, Msg)
        End If
    End Sub

    Private Sub EnvoyerMail_Salarie(ByVal NbHeures As Double, ByVal Motif As String)
        If WsDossierHS Is Nothing Then
            Exit Sub
        End If
        If WsDossierHS.VParent.EMail = "" Then
            Exit Sub
        End If
        Dim Sujet As String = "Demande de rémunération d'heures supplémentaires"
        Dim Msg As String = "Bonjour, " & vbCrLf
        Msg &= "Pour votre information, vous avez déposé une demande de rémunération de " & NbHeures & " heures supplémentaires." & vbCrLf
        Select Case Motif
            Case "Accord"
                Msg &= "Cette demande a reçue l'accord de la Direction Générale." & vbCrLf
            Case Else
                Msg &= "Cette demande n'a pas reçue l'accord de la Direction Générale." & vbCrLf
        End Select
        Msg &= "Ce message est généré automatiquement par l’application Virtualia.net à la suite d’une demande de rémunération."
        Call CType(V_WebFonction.PointeurGlobal, Virtualia.Net.Session.ObjetGlobal).EnvoyerEmail(Sujet, "", WsDossierHS.VParent.EMail, Msg)
    End Sub

    Private Function ConvertCentiemeEnHeureMinute(ByVal NbCentiemes As Double) As String
        If NbCentiemes = 0 Then
            Return ""
        End If
        Dim NbMinutes As Double = NbCentiemes * 60
        Dim NBHeures As Long = CInt(NbMinutes) \ 60
        NbMinutes -= NBHeures * 60
        Return NBHeures & " h " & Strings.Format(NbMinutes, "00")
    End Function
End Class