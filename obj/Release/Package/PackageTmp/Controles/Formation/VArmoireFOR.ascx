﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="VArmoireFOR.ascx.vb" Inherits="Virtualia.Net.VArmoireFOR" %>

<asp:Panel ID="PanelArmoire" runat="server" BackColor="#1C5151" HorizontalAlign="Center" Width="720px" BorderStyle="Solid" BorderWidth="1px" BorderColor="#B0E0D7" 
     Font-Names="Trebuchet MS" Font-Italic="true" Style="margin-top:5px; margin-bottom:5px">
    <asp:Table ID="CadreArmoire" runat="server">
        <asp:TableRow>
            <asp:TableCell HorizontalAlign="Center">
               <asp:Table ID="TableTypeArmoire" runat="server" CellPadding="0" CellSpacing="0" BackColor="#6D9092" Width="710px" Height="40px">
                    <asp:TableRow>
                        <asp:TableCell VerticalAlign="Middle" HorizontalAlign="Left">
                            <asp:Label ID="EtiPlan" runat="server" Text="Filtre sur Plan" Width="100px" BackColor="Transparent" ForeColor="White" BorderStyle="None"
                                 Font-Italic="true" Style="text-align:left;margin-left:5px" />
                        </asp:TableCell>
                        <asp:TableCell VerticalAlign="Middle" HorizontalAlign="Left">
                            <asp:DropDownList ID="LstFiltrePlan" runat="server" Height="22px" Width="300px" AutoPostBack="True" 
                                BackColor="#A8BBB8" ForeColor="#124545" Style="text-align:left;margin-left:5px"/>
                        </asp:TableCell>
                    </asp:TableRow>
                </asp:Table>
            </asp:TableCell>
        </asp:TableRow>
        <asp:TableRow>
            <asp:TableCell>
                <asp:Table ID="CadreListe" runat="server" Style="margin-top: 10px; width: 640px; background-attachment: inherit; display: table-cell;">
                    <asp:TableRow>
                        <asp:TableCell ID="CelluleLettre" runat="server" Width="650px" Visible="true">
                            <asp:Table ID="CadreLettre" runat="server" CellPadding="0" CellSpacing="0" HorizontalAlign="Left" Width="640px">
                                <asp:TableRow>
                                    <asp:TableCell>
                                        <asp:ImageButton ID="ButtonA" runat="server" BorderStyle="None" ImageAlign="Middle" ImageUrl="~/Images/Lettres/A.bmp" />
                                    </asp:TableCell>
                                    <asp:TableCell>
                                        <asp:ImageButton ID="ButtonB" runat="server" BorderStyle="None" ImageAlign="Middle" ImageUrl="~/Images/Lettres/B.bmp" />
                                    </asp:TableCell>
                                    <asp:TableCell>
                                        <asp:ImageButton ID="ButtonC" runat="server" BorderStyle="None" ImageAlign="Middle" ImageUrl="~/Images/Lettres/C.bmp" />
                                    </asp:TableCell>
                                    <asp:TableCell>
                                        <asp:ImageButton ID="ButtonD" runat="server" BorderStyle="None" ImageAlign="Middle" ImageUrl="~/Images/Lettres/D.bmp" />
                                    </asp:TableCell>
                                    <asp:TableCell>
                                        <asp:ImageButton ID="ButtonE" runat="server" BorderStyle="None" ImageAlign="Middle" ImageUrl="~/Images/Lettres/E.bmp" />
                                    </asp:TableCell>
                                    <asp:TableCell>
                                        <asp:ImageButton ID="ButtonF" runat="server" BorderStyle="None" ImageAlign="Middle" ImageUrl="~/Images/Lettres/F.bmp" />
                                    </asp:TableCell>
                                    <asp:TableCell>
                                        <asp:ImageButton ID="ButtonG" runat="server" BorderStyle="None" ImageAlign="Middle" ImageUrl="~/Images/Lettres/G.bmp" />
                                    </asp:TableCell>
                                    <asp:TableCell>
                                        <asp:ImageButton ID="ButtonH" runat="server" BorderStyle="None" ImageAlign="Middle" ImageUrl="~/Images/Lettres/H.bmp" />
                                    </asp:TableCell>
                                    <asp:TableCell>
                                        <asp:ImageButton ID="ButtonI" runat="server" BorderStyle="None" ImageAlign="Middle" ImageUrl="~/Images/Lettres/I.bmp" />
                                    </asp:TableCell>
                                    <asp:TableCell>
                                        <asp:ImageButton ID="ButtonJ" runat="server" BorderStyle="None" ImageAlign="Middle" ImageUrl="~/Images/Lettres/J.bmp" />
                                    </asp:TableCell>
                                    <asp:TableCell>
                                        <asp:ImageButton ID="ButtonK" runat="server" BorderStyle="None" ImageAlign="Middle" ImageUrl="~/Images/Lettres/K.bmp" />
                                    </asp:TableCell>
                                    <asp:TableCell>
                                        <asp:ImageButton ID="ButtonL" runat="server" BorderStyle="None" ImageAlign="Middle" ImageUrl="~/Images/Lettres/L.bmp" />
                                    </asp:TableCell>
                                    <asp:TableCell>
                                        <asp:ImageButton ID="ButtonM" runat="server" BorderStyle="None" ImageAlign="Middle" ImageUrl="~/Images/Lettres/M.bmp" />
                                    </asp:TableCell>
                                    <asp:TableCell>
                                        <asp:ImageButton ID="ButtonN" runat="server" BorderStyle="None" ImageAlign="Middle" ImageUrl="~/Images/Lettres/N.bmp" />
                                    </asp:TableCell>
                                    <asp:TableCell>
                                        <asp:ImageButton ID="ButtonO" runat="server" BorderStyle="None" ImageAlign="Middle" ImageUrl="~/Images/Lettres/O.bmp" />
                                    </asp:TableCell>
                                    <asp:TableCell>
                                        <asp:ImageButton ID="ButtonP" runat="server" BorderStyle="None" ImageAlign="Middle" ImageUrl="~/Images/Lettres/P.bmp" />
                                    </asp:TableCell>
                                    <asp:TableCell>
                                        <asp:ImageButton ID="ButtonQ" runat="server" BorderStyle="None" ImageAlign="Middle" ImageUrl="~/Images/Lettres/Q.bmp" />
                                    </asp:TableCell>
                                    <asp:TableCell>
                                        <asp:ImageButton ID="ButtonR" runat="server" BorderStyle="None" ImageAlign="Middle" ImageUrl="~/Images/Lettres/R.bmp" />
                                    </asp:TableCell>
                                    <asp:TableCell>
                                        <asp:ImageButton ID="ButtonS" runat="server" BorderStyle="None" ImageAlign="Middle" ImageUrl="~/Images/Lettres/S.bmp" />
                                    </asp:TableCell>
                                    <asp:TableCell>
                                        <asp:ImageButton ID="ButtonT" runat="server" BorderStyle="None" ImageAlign="Middle" ImageUrl="~/Images/Lettres/T.bmp" />
                                    </asp:TableCell>
                                    <asp:TableCell>
                                        <asp:ImageButton ID="ButtonU" runat="server" BorderStyle="None" ImageAlign="Middle" ImageUrl="~/Images/Lettres/U.bmp" />
                                    </asp:TableCell>
                                    <asp:TableCell>
                                        <asp:ImageButton ID="ButtonV" runat="server" BorderStyle="None" ImageAlign="Middle" ImageUrl="~/Images/Lettres/V.bmp" />
                                    </asp:TableCell>
                                    <asp:TableCell>
                                        <asp:ImageButton ID="ButtonW" runat="server" BorderStyle="None" ImageAlign="Middle" ImageUrl="~/Images/Lettres/W.bmp" />
                                    </asp:TableCell>
                                    <asp:TableCell>
                                        <asp:ImageButton ID="ButtonX" runat="server" BorderStyle="None" ImageAlign="Middle" ImageUrl="~/Images/Lettres/X.bmp" />
                                    </asp:TableCell>
                                    <asp:TableCell>
                                        <asp:ImageButton ID="ButtonY" runat="server" BorderStyle="None" ImageAlign="Middle" ImageUrl="~/Images/Lettres/Y.bmp" />
                                    </asp:TableCell>
                                    <asp:TableCell>
                                        <asp:ImageButton ID="ButtonZ" runat="server" BorderStyle="None" ImageAlign="Middle" ImageUrl="~/Images/Lettres/Z.bmp" />
                                    </asp:TableCell>
                                    <asp:TableCell>
                                        <asp:ImageButton ID="ButtonAll" runat="server" BorderStyle="None" ImageAlign="Middle" ImageUrl="~/Images/Lettres/arobase_Sel.bmp" />
                                    </asp:TableCell>
                                </asp:TableRow>
                                <asp:TableRow>
                                    <asp:TableCell HorizontalAlign="Left" ColumnSpan="19">
                                        <asp:Label ID="EtiRecherche" runat="server" Text="Recherche par intitulé (Contenant)" Height="16px" Width="420px" BackColor="Transparent" ForeColor="#D7FAF3" BorderStyle="None" Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="Small" Style="margin-top: 0px; margin-right: 5px; font-style: oblique; text-indent: 5px; text-align: right;">
                                        </asp:Label>
                                    </asp:TableCell>
                                    <asp:TableCell HorizontalAlign="Left" ColumnSpan="8">
                                        <asp:TextBox ID="DonRecherche" runat="server" Text="" Visible="true" AutoPostBack="true" BackColor="White" BorderColor="#B0E0D7" BorderStyle="InSet" BorderWidth="2px" ForeColor="#124545" Height="16px" Width="220px" MaxLength="35" Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="Small" Style="margin-top: 0px; margin-left: 2px; font-style: normal; text-indent: 1px; text-align: left" />
                                    </asp:TableCell>
                                </asp:TableRow>
                            </asp:Table>
                        </asp:TableCell>
                    </asp:TableRow>
                    <asp:TableRow>
                        <asp:TableCell>
                            <asp:Panel ID="PanelTree" runat="server" ScrollBars="Vertical" Width="710px" Height="550px" BackColor="Snow" Wrap="true" Style="margin-top: 0px; vertical-align: top; overflow: hidden; text-align: left">
                                <asp:TreeView ID="TreeListeDossier" runat="server" MaxDataBindDepth="2" BorderStyle="None" BorderWidth="2px" BorderColor="Snow" ForeColor="#142425" ShowCheckBoxes="None" Width="700px" Height="550px" Font-Bold="True" NodeIndent="10" BackColor="Snow" LeafNodeStyle-HorizontalPadding="8px" RootNodeStyle-Font-Bold="True" RootNodeStyle-ImageUrl="~/Images/Armoire/FicheBleue.bmp" RootNodeStyle-Font-Italic="False">
                                    <SelectedNodeStyle BackColor="#6C9690" BorderColor="#E3924F" BorderStyle="Solid" BorderWidth="1px" ForeColor="White" />
                                </asp:TreeView>
                            </asp:Panel>
                        </asp:TableCell>
                    </asp:TableRow>
                    <asp:TableRow>
                        <asp:TableCell Width="620px" HorizontalAlign="Center">
                            <asp:Label ID="EtiStatus1" runat="server" Height="15px" Width="150px" BackColor="#6D9092" Visible="true" BorderColor="#CCFFFF" BorderStyle="None" BorderWidth="2px" ForeColor="White" Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small" Style="margin-left: 0px; font-style: oblique; text-align: center" />
                        </asp:TableCell>
                    </asp:TableRow>
                </asp:Table>
            </asp:TableCell>
        </asp:TableRow>
        <asp:TableRow>
            <asp:TableCell>
                <asp:HiddenField ID="HSelLettre" runat="server" Value="" />
            </asp:TableCell>
        </asp:TableRow>
    </asp:Table>
</asp:Panel>