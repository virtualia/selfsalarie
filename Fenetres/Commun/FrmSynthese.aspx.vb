﻿Option Strict On
Option Explicit On
Option Compare Text
Public Class FrmSynthese
    Inherits System.Web.UI.Page
    Private WebFct As Virtualia.Net.Controles.WebFonctions
    Private WsCtl_Cache As WebAppli.SelfV4.CacheSynthese
    'Private WsNomStateUrl As String = "Synthese"
    Private WsNomStateCache As String = "VSelSy"
    Private SelIndex As Integer = 0
    Private Enum Ivue As Integer
        ISynthese = 0
        IArmoire = 1
    End Enum
    Private ReadOnly Property V_WebFonction As Virtualia.Net.Controles.WebFonctions
        Get
            If WebFct Is Nothing Then
                WebFct = New Virtualia.Net.Controles.WebFonctions(Me, 1)
            End If
            Return WebFct
        End Get
    End Property
    Private Sub Page_Init(sender As Object, e As EventArgs) Handles Me.Init
        Dim Msg As String = ""
        Dim NumSession As String = Server.HtmlDecode(Request.QueryString("IDVirtualia"))
        If NumSession <> Session.SessionID Then
            Msg = "Erreur lors de l'identification."
            Response.Redirect("~/Fenetres/Commun/ErreurApplication.aspx?Msg=" & Msg)
            Exit Sub
        End If
        WebFct = New Virtualia.Net.Controles.WebFonctions(Me, 1)
        If WebFct.PointeurUtilisateur Is Nothing Then
            Msg = "Erreur de serveur. Vous avez été déconnecté(e)."
            Response.Redirect("~/Fenetres/Commun/ErreurApplication.aspx?Msg=" & Msg)
            Exit Sub
        End If
        If WebFct.VerifierCookie(Me, Session.SessionID) = False Then
            Msg = "Erreur lors de l'identification du dossier accédé. Session invalide."
            Response.Redirect("~/Fenetres/Commun/ErreurApplication.aspx?Msg=" & Msg)
            Exit Sub
        End If
    End Sub

    Private Property CacheVirControle As WebAppli.SelfV4.CacheSynthese
        'Get
        '    If Me.ViewState(WsNomStateUrl) IsNot Nothing And WsCtl_Cache IsNot Nothing Then
        '        Return WsCtl_Cache
        '    End If

        '    Dim NewCache As WebAppli.SelfV4.CacheSynthese = New WebAppli.SelfV4.CacheSynthese
        '    Return NewCache
        'End Get
        'Set(value As WebAppli.SelfV4.CacheSynthese)
        '    If value Is Nothing Then
        '        Exit Property
        '    End If
        '    If Me.ViewState(WsNomStateUrl) IsNot Nothing Then
        '        Me.ViewState.Remove(WsNomStateUrl)
        '    End If
        '    Me.ViewState.Add(WsNomStateUrl, value)
        'End Set

        Get
            If Me.ViewState(WsNomStateCache) IsNot Nothing Then
                Return CType(Me.ViewState(WsNomStateCache), WebAppli.SelfV4.CacheSynthese)
            End If
            Dim NewCache As WebAppli.SelfV4.CacheSynthese = New WebAppli.SelfV4.CacheSynthese
            Return NewCache
        End Get
        Set(value As WebAppli.SelfV4.CacheSynthese)
            If value Is Nothing Then
                Exit Property
            End If
            If Me.ViewState(WsNomStateCache) IsNot Nothing Then
                Me.ViewState.Remove(WsNomStateCache)
            End If
            Me.ViewState.Add(WsNomStateCache, value)
        End Set
    End Property

    Private Sub FrmPFRSynthese_PreRender(sender As Object, e As EventArgs) Handles Me.PreRender
        Dim UtiSession As Virtualia.Net.Session.SessionModule = CType(V_WebFonction.PointeurUtilisateur.V_PointeurSessionModule, Virtualia.Net.Session.SessionModule)
        If UtiSession Is Nothing Then
            Exit Sub
        End If
        WsCtl_Cache = CacheVirControle
        Select Case WsCtl_Cache.Index_VueActive
            Case Ivue.ISynthese
                Call FaireListeChoix()
        End Select
    End Sub

    Private Sub FrmPFRSynthese_PreRenderComplete(sender As Object, e As EventArgs) Handles Me.PreRenderComplete
        Dim FichierHtml As String
        FichierHtml = V_WebFonction.PointeurGlobal.VirRepertoireTemporaire & "\" & Session.SessionID & ".html"
        Response.ContentEncoding = System.Text.Encoding.UTF8
        Response.Filter = New Virtualia.Systeme.Outils.FiltreReponseParMemoryStream(Response.Filter, FichierHtml)
    End Sub

    Private Sub Armoire_Dossier_Click(ByVal sender As Object, ByVal e As Systeme.Evenements.DossierClickEventArgs) Handles ArmoirePER.Dossier_Click
        If e.Identifiant = 0 Then
            Exit Sub
        End If
        Dim Dossier As Virtualia.Net.WebAppli.SelfV4.DossierPersonne = CType(V_WebFonction.PointeurGlobal, Virtualia.Net.Session.ObjetGlobal).Dossier(e.Identifiant)
        If Dossier Is Nothing Then
            Exit Sub
        End If
        Dossier.Nom_Prenom_Manager = V_WebFonction.PointeurUtilisateur.V_NomdeConnexion
        CType(V_WebFonction.PointeurUtilisateur.V_PointeurSessionModule, Virtualia.Net.Session.SessionModule).SiAccesManager = True
        CType(V_WebFonction.PointeurUtilisateur.V_PointeurSessionModule, Virtualia.Net.Session.SessionModule).DossierPER = Dossier
        WsCtl_Cache = CacheVirControle
        WsCtl_Cache.Ide_Dossier = e.Identifiant
        WsCtl_Cache.Index_VueActive = Ivue.ISynthese
        CacheVirControle = WsCtl_Cache
        DropDownMoisAnnee.Items.Clear()
    End Sub
    Private Sub FaireListeChoix()
        Dim UtiSession As Virtualia.Net.Session.SessionModule = CType(V_WebFonction.PointeurUtilisateur.V_PointeurSessionModule, Virtualia.Net.Session.SessionModule)
        If UtiSession Is Nothing Then
            Exit Sub
        End If
        WsCtl_Cache = CacheVirControle
        If DropDownArmoire.Items.Count = 0 Then
            DropDownArmoire.Items.Add("Liste des dossiers ayant des heures supplémentaires à payer")
            DropDownArmoire.Items.Add("Liste des dossiers ayant des astreintes à payer")
        End If
        If DropDownMoisAnnee.Items.Count = 0 Then
            Dim AnneeDebut As Integer = Year(Now)
            Dim MoisDebut As Integer = Month(Now) - 1
            Dim Mois As Integer
            If MoisDebut = 0 Then
                AnneeDebut -= 1
                MoisDebut = 12
            End If
            WsCtl_Cache.AnneeSelection = AnneeDebut
            WsCtl_Cache.MoisSelection = MoisDebut
            ' WsCtl_Cache.Ide_Dossier = UtiSession.DossierPER.Ide_Dossier
            WsCtl_Cache.Index_VueActive = Ivue.ISynthese
            For Mois = MoisDebut To 1 Step -1
                DropDownMoisAnnee.Items.Add(New ListItem(V_WebFonction.ViRhDates.MoisEnClair(Mois) & Strings.Space(1) & AnneeDebut, AnneeDebut & Strings.Format(Mois, "00")))
            Next
            For Mois = 12 To 1 Step -1
                DropDownMoisAnnee.Items.Add(New ListItem(V_WebFonction.ViRhDates.MoisEnClair(Mois) & Strings.Space(1) & (AnneeDebut - 1), (AnneeDebut - 1) & Strings.Format(Mois, "00")))
            Next
            CacheVirControle = WsCtl_Cache
        End If
    End Sub

    Private Sub DropDown_SelectedIndexChanged(sender As Object, e As EventArgs) Handles DropDownMoisAnnee.SelectedIndexChanged
        Dim UtiSession As Virtualia.Net.Session.SessionModule = CType(V_WebFonction.PointeurUtilisateur.V_PointeurSessionModule, Virtualia.Net.Session.SessionModule)
        If UtiSession Is Nothing Then
            Exit Sub
        End If
        Dim Annee As Integer = CInt(Strings.Left(CType(sender, System.Web.UI.WebControls.DropDownList).SelectedItem.Value, 4))
        Dim Mois As Integer = CInt(Strings.Right(CType(sender, System.Web.UI.WebControls.DropDownList).SelectedItem.Value, 2))
        WsCtl_Cache = CacheVirControle
        WsCtl_Cache.AnneeSelection = Annee
        WsCtl_Cache.MoisSelection = Mois
        CacheVirControle = WsCtl_Cache
    End Sub
    Private Sub CommandeRetour_Click(sender As Object, e As ImageClickEventArgs) Handles CommandeRetour.Click
        Dim UtiSession As Virtualia.Net.Session.SessionModule = CType(V_WebFonction.PointeurUtilisateur.V_PointeurSessionModule, Virtualia.Net.Session.SessionModule)
        If UtiSession Is Nothing Then
            Exit Sub
        End If
        Response.Redirect("~/Fenetres/Commun/FrmHeuresSupplementaires.aspx?IDVirtualia=" & UtiSession.VParent.V_IDSession)
    End Sub

    Private Sub ExecuterListe()
        Dim LstVuesDyna As List(Of Virtualia.Net.ServiceServeur.VirRequeteType) = Nothing
        Dim ObjetdelaListe As String = ""

        ' ObjetdelaListe = WsNomStateUrl
        ObjetdelaListe = WsNomStateCache
        WsCtl_Cache = CacheVirControle
        WsCtl_Cache.AnneeSelection = CInt(Strings.Left(DropDownMoisAnnee.SelectedItem.Value, 4))
        WsCtl_Cache.MoisSelection = CInt(Strings.Right(DropDownMoisAnnee.SelectedItem.Value, 2))

        SelIndex = CInt(CType(DropDownArmoire(), System.Web.UI.WebControls.DropDownList).SelectedIndex)

        Select Case SelIndex
            Case 0 To 1
                If ObjetdelaListe = "VSelSy" Then
                    Dim Requeteur As ObjetRequetes = New ObjetRequetes(V_WebFonction.PointeurUtilisateur)
                    Select Case SelIndex
                        Case 0
                            LstVuesDyna = Requeteur.LesHeuresSupplementaires_APayer(WebFct.ViRhDates.DateSaisieVerifiee("01" & "/" & WsCtl_Cache.MoisSelection & "/" & WsCtl_Cache.AnneeSelection))
                        Case 1
                            LstVuesDyna = Requeteur.LesAstreintes_APayer(WebFct.ViRhDates.DateSaisieVerifiee("01" & "/" & WsCtl_Cache.MoisSelection & "/" & WsCtl_Cache.AnneeSelection))
                    End Select
                End If
        End Select

        V_WebFonction.PointeurUtilisateur.PointeurListeCourantePER = LstVuesDyna

    End Sub

    Private Sub DropDownArmoire_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles DropDownArmoire.SelectedIndexChanged

        SelIndex = CInt(CType(sender, System.Web.UI.WebControls.DropDownList).SelectedIndex)

        If SelIndex = 0 Or SelIndex = 1 Or SelIndex > 1 Then
            If V_WebFonction.PointeurUtilisateur Is Nothing Then
                Exit Sub
            End If
        End If
        ''Call ExecuterListe()
    End Sub

    Private Sub CommandeCsv_Click(sender As Object, e As System.EventArgs) Handles CommandeCsv.Click
        Dim UtiSession As Virtualia.Net.Session.SessionModule = CType(V_WebFonction.PointeurUtilisateur.V_PointeurSessionModule, Virtualia.Net.Session.SessionModule)
        If UtiSession Is Nothing Then
            Exit Sub
        End If
        WsCtl_Cache = CacheVirControle
        Dim CarSep As String = Strings.Chr(9)
        Dim Suffixe As String = "." & Strings.Right(CType(sender, System.Web.UI.WebControls.Button).ID, 3)
        Dim CodeIso As System.Text.Encoding = System.Text.Encoding.UTF8
        Dim CodeUnicode As System.Text.Encoding = System.Text.Encoding.Unicode
        Dim FicStream As System.IO.FileStream
        Dim FicWriter As System.IO.StreamWriter
        Dim ChemindeBase As String
        Dim NomRepertoire As String
        Dim NomFichier As String
        Dim Chaine As String
        Dim FluxTeleChargement As Byte() = Nothing
        Dim IndiceI As Integer
        Dim IndiceK As Integer

        Dim LstVuesDyna As List(Of Virtualia.Net.ServiceServeur.VirRequeteType) = Nothing
        ExecuterListe()
        LstVuesDyna = V_WebFonction.PointeurUtilisateur.PointeurListeCourantePER
        If LstVuesDyna Is Nothing Then
            Exit Sub
        End If
        If LstVuesDyna.Count = 0 Then
            Exit Sub
        End If

        '*******AKR
        Dim Dossier As Virtualia.Net.WebAppli.SelfV4.DossierPersonne = Nothing
        Dim LstHeures As List(Of Virtualia.TablesObjet.ShemaPER.PER_DETAIL_HS) = Nothing
        Dim LstJourAstreinte As List(Of Virtualia.TablesObjet.ShemaPER.PER_JOUR_ASTREINTE) = Nothing
        Dim ChaineD As String
        '***********AKR
        WsCtl_Cache.AnneeSelection = CInt(Strings.Left(DropDownMoisAnnee.SelectedItem.Value, 4))
        WsCtl_Cache.MoisSelection = CInt(Strings.Right(DropDownMoisAnnee.SelectedItem.Value, 2))
        Dossier = CType(V_WebFonction.PointeurGlobal, Virtualia.Net.Session.ObjetGlobal).Dossier(LstVuesDyna.Item(0).Ide_Dossier)
        LstHeures = Dossier.VDossier_NewHeuresSup.ListeDetaille_HS_Validee(WsCtl_Cache.AnneeSelection, WsCtl_Cache.MoisSelection)
        LstJourAstreinte = Dossier.VListeJourAstreinte(WebFct.ViRhDates.DateSaisieVerifiee("01" & "/" & WsCtl_Cache.MoisSelection & "/" & WsCtl_Cache.AnneeSelection), WebFct.ViRhDates.DateSaisieVerifiee("31" & "/" & WsCtl_Cache.MoisSelection & "/" & WsCtl_Cache.AnneeSelection))

        Select Case DropDownArmoire.SelectedIndex
            Case 0
                If LstHeures Is Nothing Then
                    Exit Sub
                End If
            Case 1
                If LstJourAstreinte Is Nothing Then
                    Exit Sub
                End If
            Case Else
                Exit Sub
        End Select



        ChemindeBase = Request.PhysicalApplicationPath
        If Strings.Right(ChemindeBase, 1) <> "\" Then
            ChemindeBase &= "\"
        End If
        NomRepertoire = V_WebFonction.PointeurGlobal.VirRepertoireTemporaire & "\" & Session.SessionID
        If My.Computer.FileSystem.DirectoryExists(NomRepertoire) = False Then
            My.Computer.FileSystem.CreateDirectory(NomRepertoire)
        End If
        NomFichier = "Armoire_" & V_WebFonction.PointeurUtilisateur.Param_Annee & "_" & V_WebFonction.ViRhFonction.ChaineSansAccent(V_WebFonction.PointeurUtilisateur.Param_Etablissement, False) & Suffixe

        FicStream = New System.IO.FileStream(NomRepertoire & "\" & NomFichier, IO.FileMode.Create, IO.FileAccess.Write)
        FicWriter = New System.IO.StreamWriter(FicStream, CodeUnicode)

        ''***AKR Entête du fichier csv
        Chaine = ""
        Chaine &= "N° de dossier" & CarSep
        Chaine &= "Nom" & CarSep
        Chaine &= "Prénom" & CarSep
        Chaine &= "Structure de rattachement" & CarSep
        Chaine &= "Structure d'affectation" & CarSep
        Chaine &= "Structure de 3e niveau" & CarSep
        Chaine &= "Fonction exercée" & CarSep
        Select Case DropDownArmoire.SelectedIndex
            Case 0
                Chaine &= "Date de Valeur" & CarSep
                Chaine &= "Nature" & CarSep
                Chaine &= "Fait Generateur" & CarSep
                Chaine &= "Heure de début" & CarSep
                Chaine &= "Heure de Fin" & CarSep
                Chaine &= "NbHeures Jusquà 20h" & CarSep
                Chaine &= "NbHeures de 20h à 22h" & CarSep
                Chaine &= "NbHeures de Nuit" & CarSep
                Chaine &= "NbHeures de Trajet" & CarSep
                Chaine &= "Date de Valeur" & CarSep
                FicWriter.WriteLine(Chaine)
            Case 1
                ''Chaine &= "Date de Valeur" & CarSep
                Chaine &= "Date de Valeur" & CarSep
                Chaine &= "Nature" & CarSep
                Chaine &= "Validation direction" & CarSep
                Chaine &= "Date de validation" & CarSep
                Chaine &= "Montant" & CarSep
                Chaine &= "Commentaire de l'agent" & CarSep
                Chaine &= "Commentaire de la direction" & CarSep
                FicWriter.WriteLine(Chaine)

        End Select
        ''****

        For IndiceI = 0 To LstVuesDyna.Count - 1
            Dossier = CType(V_WebFonction.PointeurGlobal, Virtualia.Net.Session.ObjetGlobal).Dossier(LstVuesDyna.Item(IndiceI).Ide_Dossier)
            LstHeures = Dossier.VDossier_NewHeuresSup.ListeDetaille_HS_Validee(WsCtl_Cache.AnneeSelection, WsCtl_Cache.MoisSelection)
            LstJourAstreinte = Dossier.VListeJourAstreinte(WebFct.ViRhDates.DateSaisieVerifiee("01" & "/" & WsCtl_Cache.MoisSelection & "/" & WsCtl_Cache.AnneeSelection), WebFct.ViRhDates.DateSaisieVerifiee("31" & "/" & WsCtl_Cache.MoisSelection & "/" & WsCtl_Cache.AnneeSelection))
            Chaine = ""
            Chaine &= LstVuesDyna.Item(IndiceI).Ide_Dossier & CarSep
            Chaine &= Dossier.Nom & CarSep
            Chaine &= Dossier.Prenom & CarSep
            Chaine &= Dossier.VFiche_Affectation(V_WebFonction.ViRhDates.DateduJour).Structure_de_rattachement & CarSep
            Chaine &= Dossier.VFiche_Affectation(V_WebFonction.ViRhDates.DateduJour).Structure_d_affectation & CarSep
            Chaine &= Dossier.VFiche_Affectation(V_WebFonction.ViRhDates.DateduJour).Structure_de_3e_niveau & CarSep
            Chaine &= Dossier.VFiche_Affectation(V_WebFonction.ViRhDates.DateduJour).Fonction_exercee & CarSep

            Select Case DropDownArmoire.SelectedIndex
                Case 0
                    IndiceK = 0
                    ChaineD = ""
                    If LstHeures IsNot Nothing Then
                        For Each LigneDetail In LstHeures
                            ChaineD = V_WebFonction.ViRhDates.ClairDate(LigneDetail.Date_de_Valeur, True) & CarSep
                            ChaineD &= LigneDetail.Nature & CarSep
                            ChaineD &= LigneDetail.FaitGenerateur & CarSep
                            ChaineD &= LigneDetail.Heure_Borne & CarSep
                            ChaineD &= LigneDetail.Heure_Fin & CarSep
                            ChaineD &= LigneDetail.V_NbHeures_Jusqua20h & CarSep
                            ChaineD &= LigneDetail.V_NbHeures_de20ha22h & CarSep
                            ChaineD &= LigneDetail.V_NbHeures_deNuit & CarSep
                            ChaineD &= LigneDetail.V_NbHeures_Trajet & CarSep
                            ChaineD &= LigneDetail.Date_de_Valeur
                            FicWriter.WriteLine(Chaine & ChaineD)
                            IndiceK += 1
                        Next
                    End If
                Case 1
                        IndiceK = 0
                        ChaineD = ""
                    If LstJourAstreinte IsNot Nothing Then
                        For Each LigneDetail In LstJourAstreinte
                            Dim MontantTauxAstreinte As Double = 0
                            Dim LstDecision = CType(V_WebFonction.PointeurGlobal, Virtualia.Net.Session.ObjetGlobal).ListeTablesDecision(50)
                            If LstDecision IsNot Nothing Then
                                For Each OutDecision As Virtualia.TablesObjet.ShemaREF.OUT_DECISION In LstDecision
                                    If Year(V_WebFonction.ViRhDates.DateTypee(OutDecision.ValeurInterne(0))).ToString = Year(Now).ToString Then
                                        MontantTauxAstreinte = V_WebFonction.ViRhFonction.ConversionDouble(OutDecision.CodeExterne(0))
                                    End If
                                Next
                            End If
                            ''ChaineD = V_WebFonction.ViRhDates.ClairDate(LigneDetail.Date_de_Valeur, True) & CarSep
                            ChaineD = LigneDetail.Date_de_Valeur & CarSep
                            ChaineD &= LigneDetail.Nature & CarSep
                            ChaineD &= LigneDetail.SiValide_DRH & CarSep
                            ChaineD &= LigneDetail.Date_Validation & CarSep
                            If LigneDetail.SiValide_DRH = "Oui" Then
                                ChaineD &= MontantTauxAstreinte & CarSep
                            Else
                                ChaineD &= "" & CarSep
                            End If

                            ChaineD &= LigneDetail.Commentaire_Demandeur & CarSep
                            ChaineD &= LigneDetail.Commentaire_DRH
                            FicWriter.WriteLine(Chaine & ChaineD)
                            IndiceK += 1
                        Next
                    End If
            End Select
        Next IndiceI

        FicWriter.Flush()
        FicWriter.Close()
        FluxTeleChargement = My.Computer.FileSystem.ReadAllBytes(NomRepertoire & "\" & NomFichier)
        If LstVuesDyna IsNot Nothing Then
            Dim response As System.Web.HttpResponse = System.Web.HttpContext.Current.Response
            response.Clear()
            response.AddHeader("Content-Type", "binary/octet-stream")
            response.AddHeader("Content-Disposition", "attachment; filename=" & NomFichier & "; size=" & FluxTeleChargement.Length.ToString())
            response.Flush()
            response.BinaryWrite(FluxTeleChargement)
            response.Flush()
            response.End()
        End If
    End Sub

End Class