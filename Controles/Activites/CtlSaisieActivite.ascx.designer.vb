﻿'------------------------------------------------------------------------------
' <généré automatiquement>
'     Ce code a été généré par un outil.
'
'     Les modifications apportées à ce fichier peuvent provoquer un comportement incorrect et seront perdues si
'     le code est régénéré.
' </généré automatiquement>
'------------------------------------------------------------------------------

Option Strict On
Option Explicit On


Partial Public Class CtlSaisieActivite
    
    '''<summary>
    '''Contrôle CadreSemaine.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CadreSemaine As Global.System.Web.UI.WebControls.Table
    
    '''<summary>
    '''Contrôle LigneActivite.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LigneActivite As Global.System.Web.UI.WebControls.TableRow
    
    '''<summary>
    '''Contrôle CadreActivite.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CadreActivite As Global.System.Web.UI.WebControls.Table
    
    '''<summary>
    '''Contrôle CellEti_Activite.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CellEti_Activite As Global.System.Web.UI.WebControls.TableCell
    
    '''<summary>
    '''Contrôle Eti_Activite.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Eti_Activite As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle CellDon_Activite.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CellDon_Activite As Global.System.Web.UI.WebControls.TableCell
    
    '''<summary>
    '''Contrôle VComboActivite.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents VComboActivite As Global.Virtualia.Net.Controles_VListeCombo
    
    '''<summary>
    '''Contrôle CellEti_Etape.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CellEti_Etape As Global.System.Web.UI.WebControls.TableCell
    
    '''<summary>
    '''Contrôle Eti_Etape.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Eti_Etape As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle CellDon_Etape.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CellDon_Etape As Global.System.Web.UI.WebControls.TableCell
    
    '''<summary>
    '''Contrôle VComboEtape.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents VComboEtape As Global.Virtualia.Net.Controles_VListeCombo
    
    '''<summary>
    '''Contrôle CellEti_Precision.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CellEti_Precision As Global.System.Web.UI.WebControls.TableCell
    
    '''<summary>
    '''Contrôle Eti_Precision.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Eti_Precision As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle CellDon_Precision.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CellDon_Precision As Global.System.Web.UI.WebControls.TableCell
    
    '''<summary>
    '''Contrôle Don_Precision.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Don_Precision As Global.System.Web.UI.WebControls.TextBox
    
    '''<summary>
    '''Contrôle Donnee00.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Donnee00 As Global.System.Web.UI.WebControls.TextBox
    
    '''<summary>
    '''Contrôle Donnee01.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Donnee01 As Global.System.Web.UI.WebControls.TextBox
    
    '''<summary>
    '''Contrôle Donnee02.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Donnee02 As Global.System.Web.UI.WebControls.TextBox
    
    '''<summary>
    '''Contrôle Donnee03.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Donnee03 As Global.System.Web.UI.WebControls.TextBox
    
    '''<summary>
    '''Contrôle Donnee04.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Donnee04 As Global.System.Web.UI.WebControls.TextBox
    
    '''<summary>
    '''Contrôle Donnee05.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Donnee05 As Global.System.Web.UI.WebControls.TextBox
    
    '''<summary>
    '''Contrôle Donnee06.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Donnee06 As Global.System.Web.UI.WebControls.TextBox
    
    '''<summary>
    '''Contrôle CellSemaineCadrage.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CellSemaineCadrage As Global.System.Web.UI.WebControls.TableCell
    
    '''<summary>
    '''Contrôle EtiSemaineCadrage.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents EtiSemaineCadrage As Global.System.Web.UI.WebControls.Label
End Class
