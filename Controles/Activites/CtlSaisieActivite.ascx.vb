﻿Option Strict On
Option Explicit On
Option Compare Text
Imports VI = Virtualia.Systeme.Constantes
Public Class CtlSaisieActivite
    Inherits System.Web.UI.UserControl
    Private WebFct As Virtualia.Net.Controles.WebFonctions
    Private WsNomStateCache As String = "VSaisieActivite"
    Private WsCtl_Cache As Virtualia.Net.Controles.CacheControleActivite
    Private WsNumObjet As Integer = 112
    Private WsNomTable As String = "PER_ACTIVITE_JOUR"
    Private WsFiche As Virtualia.TablesObjet.ShemaPER.PER_ACTIVITE_JOUR

    Public Property Date_Valeur As String
        Get
            WsCtl_Cache = CacheVirControle
            If WsCtl_Cache.DateValeur <> "" Then
                Return WsCtl_Cache.DateValeur
            Else
                Return V_WebFonction.ViRhDates.Semaine_Date_Debut(V_WebFonction.ViRhDates.DateduJour)
            End If
        End Get
        Set(value As String)
            WsCtl_Cache = CacheVirControle
            WsCtl_Cache.DateValeur = value
            CacheVirControle = WsCtl_Cache
        End Set
    End Property

    Public Property Clef_Selectionnee As String
        Get
            WsCtl_Cache = CacheVirControle
            Return WsCtl_Cache.ValeurSelection
        End Get
        Set(value As String)
            WsCtl_Cache = CacheVirControle
            WsCtl_Cache.ValeurSelection = value
            WsCtl_Cache.Clef = ""
            WsCtl_Cache.Tag = value
            CacheVirControle = WsCtl_Cache
            If value = "" Then
                VComboActivite.LstText = "(Aucun)"
                VComboActivite.LstToolTip = ""
                VComboEtape.V_Liste("") = ""
                VComboEtape.LstText = "(Aucun)"
                Don_Precision.Text = ""
            End If
        End Set
    End Property

    Private ReadOnly Property V_WebFonction As Virtualia.Net.Controles.WebFonctions
        Get
            If WebFct Is Nothing Then
                WebFct = New Virtualia.Net.Controles.WebFonctions(Me, 0)
            End If
            Return WebFct
        End Get
    End Property

    Public Function ValiderLaSaisie(ByVal SiEffacer As Boolean) As Integer
        '*** Gestion du Code Retour *************************************
        '0 = Aucune Opération effectuée
        '1 = OK
        '2 = % > 100% et SiEffacer = False
        '3 = Doublon Activite / Tache
        '****************************************************************
        WsCtl_Cache = CacheVirControle
        If WsCtl_Cache.DateValeur = "" Then
            Return 0
        End If
        For Each Element As String In WsCtl_Cache.ListeSelection
            Select Case Element
                Case "", "(Aucun)"
                    Return 0
            End Select
        Next
        Dim ListeActivites As List(Of Virtualia.TablesObjet.ShemaPER.PER_ACTIVITE_JOUR) = Nothing
        Dim ListeJours As List(Of Virtualia.TablesObjet.ShemaPER.PER_ACTIVITE_JOUR) = Nothing
        Dim FichePERLue As Virtualia.TablesObjet.ShemaPER.PER_ACTIVITE_JOUR
        Dim IndiceJ As Integer
        Dim SiOK As Boolean
        Dim ClefSel As String = WsCtl_Cache.Clef
        Dim DateW As String
        Dim ZTotal As Double
        Dim Rang As Integer
        Dim Maxi As Integer

        If ClefSel = "" Then
            ClefSel = WsCtl_Cache.ListeSelection.Item(0)
            ClefSel &= WsCtl_Cache.ListeSelection.Item(1)
            ClefSel &= Don_Precision.Text
        End If
        ListeActivites = CType(V_WebFonction.PointeurUtilisateur.V_PointeurSessionModule, Virtualia.Net.Session.SessionModule).DossierPER.VListeActivites(WsCtl_Cache.DateValeur, V_WebFonction.ViRhDates.CalcDateMoinsJour(WsCtl_Cache.DateValeur, "6", "0"))
        '** Controle doublon Activite / Tache
        If ListeActivites IsNot Nothing AndAlso WsCtl_Cache.Tag = "" AndAlso SiEffacer = False Then
            ZTotal = 0
            ListeJours = ListeActivites.FindAll(Function(Recherche) Recherche.Activite = WsCtl_Cache.ListeSelection.Item(0) And
                                                    Recherche.Tache_Etape = WsCtl_Cache.ListeSelection.Item(1))
            If ListeJours IsNot Nothing AndAlso ListeJours.Count > 0 Then
                For Each Fiche As Virtualia.TablesObjet.ShemaPER.PER_ACTIVITE_JOUR In ListeJours
                    ZTotal += Fiche.Pourcentage_Repartition
                Next
            End If
            If ZTotal > 0 Then
                Return 3
            End If
        End If
        '************************************
        For IndiceJ = 0 To 6
            DateW = V_WebFonction.ViRhDates.CalcDateMoinsJour(WsCtl_Cache.DateValeur, CStr(IndiceJ), "0")
            FichePERLue = Nothing
            ListeJours = Nothing
            ZTotal = 0
            Rang = 1
            Maxi = 100
            If SiDemie_Journee(IndiceJ) = True Then
                Maxi = 50
            End If
            If ListeActivites IsNot Nothing Then
                ListeJours = ListeActivites.FindAll(Function(Recherche) Recherche.Date_Valeur_ToDate = CDate(DateW))
                If ListeJours IsNot Nothing Then
                    For Each Fiche As Virtualia.TablesObjet.ShemaPER.PER_ACTIVITE_JOUR In ListeJours
                        ZTotal += Fiche.Pourcentage_Repartition
                        If CInt(Fiche.Clef) >= Rang Then
                            Rang = CInt(Fiche.Clef) + 1
                        End If
                    Next
                End If
                If ListeJours IsNot Nothing Then
                    FichePERLue = ListeJours.Find(Function(Recherche) Recherche.ClefConcatenee = ClefSel)
                    If FichePERLue IsNot Nothing Then
                        ZTotal -= FichePERLue.Pourcentage_Repartition
                    End If
                End If
            End If
            If VText(IndiceJ) <> "" AndAlso IsNumeric(VText(IndiceJ)) Then
                ZTotal += CInt(VText(IndiceJ))
                If ZTotal <= Maxi Then
                    WsFiche = New Virtualia.TablesObjet.ShemaPER.PER_ACTIVITE_JOUR
                    WsFiche.Ide_Dossier = CType(V_WebFonction.PointeurUtilisateur.V_PointeurSessionModule, Virtualia.Net.Session.SessionModule).DossierPER.Ide_Dossier
                    WsFiche.Date_de_Valeur = DateW
                    WsFiche.Clef = CStr(Rang)
                    WsFiche.Activite = WsCtl_Cache.ListeSelection.Item(0)
                    WsFiche.Tache_Etape = WsCtl_Cache.ListeSelection.Item(1)
                    WsFiche.Precision = Don_Precision.Text
                    WsFiche.Pourcentage_Repartition = CInt(VText(IndiceJ))
                    WsFiche.Date_Saisie = V_WebFonction.ViRhDates.DateduJour
                    If FichePERLue Is Nothing Then
                        SiOK = V_WebFonction.PointeurGlobal.VirServiceServeur.MiseAjour_Fiche(V_WebFonction.PointeurGlobal.VirNomUtilisateur, VI.PointdeVue.PVueApplicatif,
                                                                                       WsNumObjet, WsFiche.Ide_Dossier, "C", "", WsFiche.ContenuTable, True)
                    ElseIf FichePERLue.FicheLue <> WsFiche.ContenuTable & VI.tild Then
                        WsFiche.Clef = FichePERLue.Clef
                        SiOK = V_WebFonction.PointeurGlobal.VirServiceServeur.MiseAjour_Fiche(V_WebFonction.PointeurGlobal.VirNomUtilisateur, VI.PointdeVue.PVueApplicatif,
                                                                                       WsNumObjet, WsFiche.Ide_Dossier, "M", FichePERLue.FicheLue, WsFiche.ContenuTable, True)
                    End If
                Else
                    Select Case SiEffacer
                        Case False
                            Return 2
                    End Select
                End If
            ElseIf FichePERLue IsNot Nothing Then
                SiOK = V_WebFonction.PointeurGlobal.VirServiceServeur.MiseAjour_Fiche(V_WebFonction.PointeurGlobal.VirNomUtilisateur, VI.PointdeVue.PVueApplicatif,
                                                               WsNumObjet, FichePERLue.Ide_Dossier, "S", FichePERLue.FicheLue, "", True)
            End If
        Next IndiceJ
        Return 1
    End Function

    Private Property CacheVirControle As Virtualia.Net.Controles.CacheControleActivite
        Get
            If Me.ViewState(WsNomStateCache) IsNot Nothing Then
                Return CType(Me.ViewState(WsNomStateCache), Virtualia.Net.Controles.CacheControleActivite)
            End If
            Dim NewCache As Virtualia.Net.Controles.CacheControleActivite
            NewCache = New Virtualia.Net.Controles.CacheControleActivite
            Return NewCache
        End Get
        Set(value As Virtualia.Net.Controles.CacheControleActivite)
            If value Is Nothing Then
                Exit Property
            End If
            If Me.ViewState(WsNomStateCache) IsNot Nothing Then
                Me.ViewState.Remove(WsNomStateCache)
            End If
            Me.ViewState.Add(WsNomStateCache, value)
        End Set
    End Property

    Private Sub Page_PreRender(sender As Object, e As EventArgs) Handles Me.PreRender
        WsCtl_Cache = CacheVirControle
        If WsCtl_Cache.ListeSelection Is Nothing Then
            Call FaireListes()
        End If
        If WsCtl_Cache.DateValeur <> "" Then
            Call FairePlanning(WsCtl_Cache.DateValeur, WsCtl_Cache.ValeurSelection)
        End If
    End Sub

    Public Sub FairePlanning(ByVal DateDebut As String, ByVal ClefSel As String)
        Dim LstCalend As List(Of Virtualia.Net.Controles.ItemJourCalendrier) = Nothing
        Dim LstAbs As List(Of Virtualia.Net.Controles.ItemJourCalendrier) = Nothing
        Dim FicheAbs As Virtualia.Net.Controles.ItemJourCalendrier = Nothing
        Dim DateW As String
        Dim IndiceJ As Integer
        Dim Couleur As Drawing.Color
        Dim ListeActivites As List(Of Virtualia.TablesObjet.ShemaPER.PER_ACTIVITE_JOUR) = Nothing
        Dim SiDemiJour As Boolean

        LstCalend = CType(V_WebFonction.PointeurUtilisateur.V_PointeurSessionModule, Virtualia.Net.Session.SessionModule).DossierPER.VDossier_Calendrier(DateDebut, V_WebFonction.ViRhDates.CalcDateMoinsJour(DateDebut, "4", "0"))
        If ClefSel <> "" Then
            ListeActivites = CType(V_WebFonction.PointeurUtilisateur.V_PointeurSessionModule, Virtualia.Net.Session.SessionModule).DossierPER.VListeActivites(DateDebut, V_WebFonction.ViRhDates.CalcDateMoinsJour(DateDebut, "6", "0"))
            If ListeActivites IsNot Nothing Then
                WsFiche = ListeActivites.FirstOrDefault(Function(Recherche) Recherche.ClefConcatenee = ClefSel)
                If WsFiche IsNot Nothing Then
                    If WsFiche.ClefConcatenee = ClefSel Then
                        WsCtl_Cache = CacheVirControle
                        WsCtl_Cache.Clef = ClefSel
                        WsCtl_Cache.ValeurSelection = ""
                        WsCtl_Cache.ListeSelection.Item(0) = WsFiche.Activite
                        WsCtl_Cache.ListeSelection.Item(1) = WsFiche.Tache_Etape
                        CacheVirControle = WsCtl_Cache
                        VComboActivite.LstText = WsFiche.Activite
                        If WsFiche.Tache_Etape <> VComboEtape.LstText Then
                            Call ActualiserListeEtapes(WsFiche.Activite, WsFiche.Tache_Etape)
                        End If
                        VComboEtape.LstText = WsFiche.Tache_Etape
                        Don_Precision.Text = WsFiche.Precision
                    End If
                End If
            End If
        End If
        IndiceJ = 0
        DateW = DateDebut
        Do
            SiDemiJour = False
            VSiReadonly(IndiceJ) = False
            VJourToolTip(IndiceJ) = ""
            VBackColor(IndiceJ) = Drawing.Color.White
            VBorderColor(IndiceJ) = V_WebFonction.ConvertCouleur("#B0E0D7")
            FicheAbs = Nothing
            If ListeActivites IsNot Nothing Then
                WsFiche = ListeActivites.Find(Function(Recherche) Recherche.Date_Valeur_ToDate = CDate(DateW) And Recherche.ClefConcatenee = ClefSel)
            Else
                WsFiche = Nothing
            End If
            If WsFiche IsNot Nothing Then
                If WsFiche.ClefConcatenee = ClefSel Then
                    VText(IndiceJ) = WsFiche.Pourcentage_Repartition.ToString
                End If
            End If
            '** Inclure Le prévisionnbel ajusté et les absences
            If LstCalend IsNot Nothing Then
                LstAbs = (From FicheCal In LstCalend Where FicheCal.Date_Valeur_ToDate = V_WebFonction.ViRhDates.DateTypee(DateW)).ToList
                If LstAbs IsNot Nothing Then
                    For Each FicheAbs In LstAbs
                        Select Case FicheAbs.Caracteristique
                            Case VI.NumeroPlage.Jour_Formation, VI.NumeroPlage.Plage1_Formation, VI.NumeroPlage.Plage2_Formation
                                Couleur = V_WebFonction.PointeurGlobal.InstanceCouleur.Item(3).Couleur_Web
                            Case Else
                                Couleur = V_WebFonction.PointeurGlobal.InstanceCouleur.Item(V_WebFonction.PointeurGlobal.InstanceCouleur.IndexCouleurPlanning(FicheAbs.Intitule)).Couleur_Web
                        End Select
                        Select Case FicheAbs.Caracteristique
                            Case VI.NumeroPlage.Jour_Absence, VI.NumeroPlage.Jour_Formation
                                VBackColor(IndiceJ) = Couleur
                                VJourToolTip(IndiceJ) = FicheAbs.Intitule
                                If FicheAbs.Intitule.StartsWith("Mission") = False Then
                                    VSiReadonly(IndiceJ) = True
                                End If
                                SiDemiJour = False
                            Case VI.NumeroPlage.Plage1_Absence, VI.NumeroPlage.Plage1_Formation
                                VJourToolTip(IndiceJ) = FicheAbs.Intitule
                                If SiDemiJour = False Then
                                    SiDemiJour = True
                                Else
                                    VSiReadonly(IndiceJ) = True
                                    SiDemiJour = False
                                End If
                            Case VI.NumeroPlage.Plage2_Absence, VI.NumeroPlage.Plage2_Formation
                                VJourToolTip(IndiceJ) = FicheAbs.Intitule
                                If SiDemiJour = False Then
                                    SiDemiJour = True
                                Else
                                    VSiReadonly(IndiceJ) = True
                                    SiDemiJour = False
                                End If
                        End Select
                    Next
                End If
            End If

            If FicheAbs IsNot Nothing Then
                Select Case FicheAbs.Caracteristique
                    Case VI.NumeroPlage.Jour_Formation, VI.NumeroPlage.Plage1_Formation, VI.NumeroPlage.Plage2_Formation
                        Couleur = V_WebFonction.PointeurGlobal.InstanceCouleur.Item(3).Couleur_Web
                    Case Else
                        Couleur = V_WebFonction.PointeurGlobal.InstanceCouleur.Item(V_WebFonction.PointeurGlobal.InstanceCouleur.IndexCouleurPlanning(FicheAbs.Intitule)).Couleur_Web
                End Select
                Select Case FicheAbs.Caracteristique
                    Case VI.NumeroPlage.Jour_Absence, VI.NumeroPlage.Jour_Formation
                        VBackColor(IndiceJ) = Couleur
                        VJourToolTip(IndiceJ) = FicheAbs.Intitule
                        If FicheAbs.Intitule.StartsWith("Mission") = False Then
                            VSiReadonly(IndiceJ) = True
                        End If
                    Case VI.NumeroPlage.Plage1_Absence, VI.NumeroPlage.Plage1_Formation
                        VJourToolTip(IndiceJ) = FicheAbs.Intitule
                    Case VI.NumeroPlage.Plage2_Absence, VI.NumeroPlage.Plage2_Formation
                        VJourToolTip(IndiceJ) = FicheAbs.Intitule
                End Select
            End If
            If V_WebFonction.ViRhDates.SiJourOuvre(DateW, True) = False Then
                VBackColor(IndiceJ) = V_WebFonction.PointeurGlobal.InstanceCouleur.Item(1).Couleur_Web
            End If
            If V_WebFonction.ViRhDates.SiJourFerie(DateW) = True Then
                VBorderColor(IndiceJ) = Drawing.Color.Red
            End If
            DateW = V_WebFonction.ViRhDates.CalcDateMoinsJour(DateW, "1", "0")
            IndiceJ += 1
            If IndiceJ > 6 Then
                Exit Do
            End If
        Loop
    End Sub

    Private Sub FaireListes()
        Dim Chaine As String
        Dim LstSel As List(Of String)

        LstSel = CType(V_WebFonction.PointeurGlobal, Virtualia.Net.Session.ObjetGlobal).VirReferentiel.ListeActivites
        Chaine = "(Aucun)" & VI.Tild
        For Each Element In LstSel
            If Element <> "" Then
                Chaine &= Element & VI.Tild
            End If
        Next
        If Chaine <> "" Then
            VComboActivite.V_Liste("") = Chaine
        End If
        VComboActivite.LstText = "(Aucun)"
        VComboEtape.V_Liste("") = ""
        LstSel = New List(Of String)
        LstSel.Add("(Aucun)")
        LstSel.Add("(Aucun)")

        WsCtl_Cache = CacheVirControle
        WsCtl_Cache.ListeSelection = LstSel
        CacheVirControle = WsCtl_Cache
    End Sub

    Private Sub VComboActivite_ValeurChange(sender As Object, e As Systeme.Evenements.DonneeChangeEventArgs) Handles VComboActivite.ValeurChange
        WsCtl_Cache = CacheVirControle
        WsCtl_Cache.ListeSelection.Item(0) = e.Valeur
        CacheVirControle = WsCtl_Cache

        Call ActualiserListeEtapes(e.Valeur, "")
    End Sub

    Private Sub VComboEtape_ValeurChange(sender As Object, e As Systeme.Evenements.DonneeChangeEventArgs) Handles VComboEtape.ValeurChange
        WsCtl_Cache = CacheVirControle
        WsCtl_Cache.ListeSelection.Item(1) = e.Valeur
        CacheVirControle = WsCtl_Cache
    End Sub

    Public Property VText(ByVal NoJour As Integer) As String
        Get
            Dim Ctl As Control
            Dim VirControle As System.Web.UI.WebControls.TextBox
            Ctl = V_WebFonction.VirWebControle(Me.CadreSemaine, "Donnee", NoJour)
            If Ctl Is Nothing Then
                Return ""
            End If
            VirControle = CType(Ctl, System.Web.UI.WebControls.TextBox)
            Return VirControle.Text
        End Get
        Set(ByVal value As String)
            Dim Ctl As Control
            Dim VirControle As System.Web.UI.WebControls.TextBox
            Ctl = V_WebFonction.VirWebControle(Me.CadreSemaine, "Donnee", NoJour)
            If Ctl Is Nothing Then
                Exit Property
            End If
            VirControle = CType(Ctl, System.Web.UI.WebControls.TextBox)
            VirControle.Text = value
        End Set
    End Property

    Public Property VSiReadonly(ByVal NoJour As Integer) As Boolean
        Get
            Dim Ctl As Control
            Ctl = V_WebFonction.VirWebControle(Me.CadreSemaine, "Donnee", NoJour)
            If Ctl Is Nothing Then
                Return False
            End If
            Return CType(Ctl, System.Web.UI.WebControls.TextBox).ReadOnly
        End Get
        Set(ByVal value As Boolean)
            Dim Ctl As Control
            Dim VirControle As System.Web.UI.WebControls.TextBox
            Ctl = V_WebFonction.VirWebControle(Me.CadreSemaine, "Donnee", NoJour)
            If Ctl Is Nothing Then
                Exit Property
            End If
            VirControle = CType(Ctl, System.Web.UI.WebControls.TextBox)
            VirControle.ReadOnly = value
        End Set
    End Property

    Private WriteOnly Property VBackColor(ByVal NoJour As Integer) As System.Drawing.Color
        Set(ByVal value As System.Drawing.Color)
            Dim Ctl As Control
            Dim VirControle As System.Web.UI.WebControls.TextBox
            Ctl = V_WebFonction.VirWebControle(Me.CadreSemaine, "Donnee", NoJour)
            If Ctl Is Nothing Then
                Exit Property
            End If
            VirControle = CType(Ctl, System.Web.UI.WebControls.TextBox)
            VirControle.BackColor = value
            If value.GetBrightness > 0.55 Then
                VirControle.ForeColor = Drawing.Color.Black
            Else
                VirControle.ForeColor = Drawing.Color.White
            End If
        End Set
    End Property

    Private WriteOnly Property VBorderColor(ByVal NoJour As Integer) As System.Drawing.Color
        Set(ByVal value As System.Drawing.Color)
            Dim Ctl As Control
            Dim VirControle As System.Web.UI.WebControls.TextBox
            Ctl = V_WebFonction.VirWebControle(Me.CadreSemaine, "Donnee", NoJour)
            If Ctl Is Nothing Then
                Exit Property
            End If
            VirControle = CType(Ctl, System.Web.UI.WebControls.TextBox)
            VirControle.BorderColor = value
        End Set
    End Property

    Public Property VJourToolTip(ByVal NoJour As Integer) As String
        Get
            Dim Ctl As Control
            Ctl = V_WebFonction.VirWebControle(Me.CadreSemaine, "Donnee", NoJour)
            If Ctl Is Nothing Then
                Return ""
            End If
            Return CType(Ctl, System.Web.UI.WebControls.TextBox).ToolTip
        End Get
        Set(ByVal value As String)
            Dim Ctl As Control
            Dim VirControle As System.Web.UI.WebControls.TextBox
            Ctl = V_WebFonction.VirWebControle(Me.CadreSemaine, "Donnee", NoJour)
            If Ctl Is Nothing Then
                Exit Property
            End If
            VirControle = CType(Ctl, System.Web.UI.WebControls.TextBox)
            VirControle.ToolTip = value
        End Set
    End Property

    Private Sub Donnee_TextChanged(sender As Object, e As System.EventArgs) Handles Donnee00.TextChanged, Donnee01.TextChanged, Donnee02.TextChanged, _
                                                    Donnee03.TextChanged, Donnee04.TextChanged, Donnee05.TextChanged, Donnee06.TextChanged
        Dim VirControle As System.Web.UI.WebControls.TextBox
        VirControle = CType(sender, System.Web.UI.WebControls.TextBox)
        If IsNumeric(VirControle.Text) = False Then
            VirControle.Text = ""
            Exit Sub
        End If
        If CInt(VirControle.Text) > 100 Then
            VirControle.Text = "100"
            Exit Sub
        End If
        If CInt(VirControle.Text) = 0 Then
            VirControle.Text = ""
            Exit Sub
        End If
        VirControle.Text = CInt(VirControle.Text).ToString
    End Sub

    Private Function SiDemie_Journee(ByVal NoJour As Integer) As Boolean
        If VSiReadonly(NoJour) = True Then
            Return False
        End If
        If VJourToolTip(NoJour) <> "" Then
            Return True
        End If
        Return False
    End Function

    Private Sub ActualiserListeEtapes(ByVal Activite As String, ByVal Valeur As String)
        Dim Chaine As String
        Dim LstSel As List(Of KeyValuePair(Of String, String))
        Dim DescriptifToolTip As String = ""

        LstSel = CType(V_WebFonction.PointeurGlobal, Virtualia.Net.Session.ObjetGlobal).VirReferentiel.ListeActivites(Activite)
        Chaine = "(Aucun)" & VI.Tild
        For Each Element In LstSel
            If DescriptifToolTip = "" Then
                DescriptifToolTip = Element.Value
            End If
            Chaine &= Element.Key & VI.Tild
        Next
        If Chaine <> "" Then
            VComboEtape.V_Liste("") = Chaine
        End If
        If Valeur = "" Then
            Valeur = "(Aucun)"
        End If
        VComboActivite.LstToolTip = DescriptifToolTip
        VComboEtape.LstText = Valeur
    End Sub
End Class