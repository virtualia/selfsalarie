﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="PER_ACOMPTE_HONDA.ascx.vb" Inherits="Virtualia.Net.PER_ACOMPTE_HONDA" %>

<%@ Register src="~/Controles/Saisies/VCoupleEtiDonnee.ascx" tagname="VCoupleEtiDonnee" tagprefix="Virtualia" %>
<%@ Register src="~/Controles/Saisies/VCoupleVerticalEtiDonnee.ascx" tagname="VCoupleVerticalEtiDonnee" tagprefix="Virtualia" %>
<%@ Register src="~/Controles/Saisies/VTrioHorizontalRadio.ascx" tagname="VTrioHorizontalRadio" tagprefix="Virtualia" %>
<%@ Register src="~/Controles/Commun/VListeGrid.ascx" tagname="VListeGrid" tagprefix="Virtualia" %>

 <asp:Table ID="CadreInfo" runat="server" BorderStyle="None" BorderWidth="2px" Visible="true"
    BorderColor="#B0E0D7" Width="750px" HorizontalAlign="Center" style="margin-top: 1px;">
     <asp:TableRow> 
         <asp:TableCell> 
           <Virtualia:VListeGrid ID="ListeGrille" runat="server" CadreWidth="630px" SiColonneSelect="true"
                                            SiCaseAcocher="false" />
         </asp:TableCell>
   </asp:TableRow>
     <asp:TableRow>
          <asp:TableCell>
            <asp:Table ID="CadreCommandes" runat="server" Height="22px" CellPadding="0" CellSpacing="0"
                Width="244px" HorizontalAlign="Right" style="margin-top: 3px;margin-right:  3px">
                <asp:TableRow>
                    <asp:TableCell VerticalAlign="Bottom">
                        <asp:Table ID="CadreCmdNewSupp" runat="server" BackImageUrl="~/Images/Boutons/NewSupp_Std.bmp" 
                            Width="160px" CellPadding="0" CellSpacing="0"
                            BorderWidth="2px" BorderStyle="Outset" BorderColor="#B0E0D7" ForeColor="#D7FAF3">
                            <asp:TableRow>
                                <asp:TableCell>
                                    <asp:Button ID="CommandeNew" runat="server" Text="Nouveau" Width="68px" Height="20px"
                                        BackColor="Transparent" BorderColor="#B0E0D7" ForeColor="#D7FAF3"
                                        Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small" Font-Italic="true"
                                        BorderStyle="None" style=" margin-left: 10px; text-align: right;"
                                        Tooltip="Nouvelle fiche">
                                    </asp:Button>
                                </asp:TableCell>
                                <asp:TableCell>
                                    <asp:Button ID="CommandeSupp" runat="server" Text="Supprimer" Width="68px" Height="20px"
                                        BackColor="Transparent" BorderColor="#B0E0D7" ForeColor="#D7FAF3"
                                        Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small" Font-Italic="true"
                                        BorderStyle="None" style=" margin-left: 10px; text-align: right;"
                                        Tooltip="Supprimer la fiche">
                                    </asp:Button>
                                </asp:TableCell>
                            </asp:TableRow>
                        </asp:Table>    
                    </asp:TableCell>
                    <asp:TableCell VerticalAlign="Bottom">
                        <asp:Table ID="CadreCmdOK" runat="server" BackImageUrl="~/Images/Boutons/OK_Std.bmp" Width="70px"
                            BorderWidth="2px" BorderStyle="Outset" BorderColor="#B0E0D7" ForeColor="#D7FAF3"
                            CellPadding="0" CellSpacing="0" Visible="false">
                            <asp:TableRow>
                                <asp:TableCell>
                                    <asp:Button ID="CommandeOK" runat="server" Text="Valider" Width="65px" Height="20px"
                                        BackColor="Transparent" BorderColor="#B0E0D7" ForeColor="#D7FAF3" 
                                        Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small" Font-Italic="true"
                                        BorderStyle="None" style=" margin-left: 6px; text-align: right;">
                                        </asp:Button>
                                </asp:TableCell>
                            </asp:TableRow>
                        </asp:Table>  
                    </asp:TableCell>
                </asp:TableRow>
              </asp:Table>
          </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell>
            <asp:Table ID="CadreEnteteActivite" runat="server" Height="75px" CellPadding="0" Width="750px" 
                CellSpacing="0" HorizontalAlign="Center" BorderStyle="Notset" BorderWidth="1px" Visible="true" BorderColor="#2FA49B">
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center" ColumnSpan="4">
                        <asp:Label ID="Etiquette" runat="server" Text="DEMANDE D'ACOMPTE<BR>(Attention le montant de l'acompte ne peut être supérieur à 50% du salaire brut mensuel)" Height="50px" Width="746px"
                            BackColor="Transparent" BorderColor="#B0E0D7" BorderStyle="None"
                            BorderWidth="1px" ForeColor="#0E5F5C"
                            Font-Bold="True" Font-Names="Trebuchet MS" Font-Size="Medium"
                            style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px; padding-top: 10px;
                            font-style: normal; text-indent: 0px; text-align: center;">
                        </asp:Label>          
                    </asp:TableCell>
                </asp:TableRow>
            </asp:Table>
       </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
       <asp:TableCell Height="8px"></asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
       <asp:TableCell> 
          <asp:Table ID="CadreOperation" runat="server" CellPadding="0" Width="750px" 
            CellSpacing="0" HorizontalAlign="Center" BorderStyle="None" BorderWidth="1px" Visible="true" BorderColor="#2FA49B">
            <asp:TableRow>
                <asp:TableCell HorizontalAlign="Center" ColumnSpan="3">
                    <Virtualia:VCoupleEtiDonnee ID="InfoH01" runat="server"
                        V_PointdeVue="1" V_Objet="170" V_Information="1" V_SiDonneeDico="true"
                        Etivisible="True" DonWidth="100px" DonHeight="22px" DonTabIndex="1" Donstyle="text-align: center;"
                        Etitext="Mois / année" EtiWidth="100px"
                        DonBorderWidth="1px"/>             
                </asp:TableCell>
            </asp:TableRow>
            <asp:TableRow>
                <asp:TableCell HorizontalAlign="Center">
                    <asp:Label ID="LabelCadrageAcompteo" runat="server" Height="22px" Width="200px"
                        BackColor="Transparent" BorderStyle="None" Text=""
                        BorderWidth="1px" ForeColor="#2FA49B" Font-Italic="False"
                        Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Medium">
                    </asp:Label>          
                </asp:TableCell>
                <asp:TableCell HorizontalAlign="Center">
                    <Virtualia:VCoupleEtiDonnee ID="InfoH02" runat="server" 
                        V_PointdeVue="1" V_Objet="170" V_Information="2" V_SiDonneeDico="true"
                        Etivisible="True" DonWidth="120px" DonHeight="22px" DonTabIndex="2"
                        Etitext="Montant de l'acompte" EtiWidth="150px"
                        DonBorderWidth="1px"/>             
                </asp:TableCell>
                <asp:TableCell HorizontalAlign="Center">
                    <asp:Label ID="LabelAcompteEuro" runat="server" Height="22px" Width="230px"
                        BackColor="Transparent" BorderStyle="None" Text="€"
                        BorderWidth="1px" ForeColor="#2FA49B" Font-Italic="False"
                        Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Medium"
                        style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px; padding-top: 0px;
                        font-style: normal; text-indent: 0px; text-align:  left;">
                    </asp:Label>          
                </asp:TableCell>
            </asp:TableRow>
            <asp:TableRow>
                <asp:TableCell Height="3px" ColumnSpan="3"></asp:TableCell>
            </asp:TableRow>
            <asp:TableRow>
                <asp:TableCell HorizontalAlign="Center" ColumnSpan="3">
                    <Virtualia:VCoupleVerticalEtiDonnee ID="InfoV03" runat="server" DonTextMode="true"
                        V_PointdeVue="1" V_Objet="170" V_Information="3" V_SiDonneeDico="true"
                        EtiWidth="500px" DonWidth="500px" DonHeight="80px" EtiHeight="20px" DonTabIndex="4" 
                        EtiText="Observations" Etivisible="True"
                        Donstyle="margin-left: 5px;" DonBorderWidth="1px"
                        Etistyle="margin-left: 5px; text-align: center; text-indent: 0px;"/>
                </asp:TableCell>
            </asp:TableRow>
          </asp:Table>
       </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
       <asp:TableCell Height="8px"></asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
       <asp:TableCell> 
          <asp:Table ID="CadreValidation" runat="server" HorizontalAlign="Center" BorderStyle="None" BorderWidth="1px" Visible="true">
            <asp:TableRow>
              <asp:TableCell>
                <asp:Table ID="CadreValidationCollaborateur" runat="server" Height="190px" CellPadding="0" Width="270px" 
                    CellSpacing="0" HorizontalAlign="Center" BorderStyle="None" BorderWidth="1px" Visible="true" BorderColor="#8DA8A3" BackColor="#E9FDF9">
                    <asp:TableRow>
                       <asp:TableCell HorizontalAlign="Center" VerticalAlign="Top">
                          <asp:Label ID="LabelCollaborateur" runat="server" Height="22px" Width="270px"
                                BackColor="#CAEBE4" BorderColor="#8DA8A3" BorderStyle="NotSet" Text="Collaborateur"
                                BorderWidth="1px" ForeColor="#0E5F5C" Font-Italic="False"
                                Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Medium"
                                style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px; padding-top: 2px;
                                font-style: normal; text-indent: 0px; text-align:  center;">
                          </asp:Label>          
                       </asp:TableCell> 
                    </asp:TableRow>
                    <asp:TableRow>
                       <asp:TableCell HorizontalAlign="Center" VerticalAlign="Top">
                          <Virtualia:VCoupleEtiDonnee ID="InfoH00" runat="server"
                            V_PointdeVue="1" V_Objet="170" V_Information="0" V_SiDonneeDico="true"
                            DonWidth="100px" DonHeight="20px" DonBorderWidth="1px" DonTabIndex="6" 
                            Donstyle="margin-left: 0px; text-align: center;" 
                            Etivisible="True" EtiWidth="120px" EtiBackColor="Transparent" EtiBorderStyle="None" EtiForeColor="#2FA49B" EtiText="Demandé le"  
                            Etistyle="margin-left: 0px; text-align: left; text-indent: 2px;"/>
                       </asp:TableCell>          
                    </asp:TableRow>
                </asp:Table>
              </asp:TableCell>
              <asp:TableCell>
                  <asp:Table ID="CadreValidationManager" runat="server" Height="190px" CellPadding="0" Width="400px" 
                        CellSpacing="0" HorizontalAlign="Center" BorderStyle="None" BorderWidth="1px" Visible="true" BorderColor="#8DA8A3" BackColor="#E9FDF9">
                        <asp:TableRow>
                           <asp:TableCell HorizontalAlign="Center" VerticalAlign="Top">
                              <asp:Label ID="LabelManager" runat="server" Height="22px" Width="400px"
                                    BackColor="#CAEBE4" BorderColor="#8DA8A3" BorderStyle="NotSet" Text="DRH"
                                    BorderWidth="1px" ForeColor="#0E5F5C" Font-Italic="False"
                                    Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Medium"
                                    style="margin-top: 1px; margin-left: 0px; margin-bottom: 10px; padding-top: 2px;
                                    font-style: normal; text-indent: 0px; text-align:  center;">
                              </asp:Label>          
                           </asp:TableCell> 
                        </asp:TableRow>
                        <asp:TableRow>
                           <asp:TableCell HorizontalAlign="Center">
                              <Virtualia:VTrioHorizontalRadio ID="RadioH04" runat="server" V_Groupe="ValidationAcompte"
                                   V_PointdeVue="1" V_Objet="170" V_Information="4" V_SiDonneeDico="true"
                                   RadioGaucheWidth="5px" RadioCentreWidth="90px" RadioDroiteWidth="90px"  
                                   RadioGaucheText="" RadioCentreText="Oui" RadioDroiteText="Non" RadioGaucheVisible="false" 
                                   RadioGaucheHeight="20px" RadioCentreHeight="20px" RadioDroiteHeight="20px"
                                   RadioCentreStyle="text-align: left;" RadioGaucheStyle="text-align: left;" RadioDroiteStyle="text-align: left;" 
			                       Visible="true"/>
                           </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow>
                           <asp:TableCell Height="5px"></asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow>
                           <asp:TableCell HorizontalAlign="Center">
                              <Virtualia:VCoupleEtiDonnee ID="InfoH05" runat="server"
                                    V_PointdeVue="1" V_Objet="170" V_Information="5" V_SiDonneeDico="true"
                                    DonWidth="100px" DonHeight="20px" DonBorderWidth="1px" DonTabIndex="7" 
                                    Donstyle="margin-left: 0px; text-align: center;" 
                                    Etivisible="True" EtiWidth="30px" EtiBackColor="Transparent" EtiBorderStyle="None" EtiForeColor="#2FA49B" EtiText="Le"  
                                    Etistyle="margin-left: 0px; text-align: left; text-indent: 2px;"/>
                           </asp:TableCell>          
                        </asp:TableRow>
                        <asp:TableRow>
                           <asp:TableCell Height="5px"></asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow>
                           <asp:TableCell HorizontalAlign="Left">
                              <Virtualia:VCoupleVerticalEtiDonnee ID="InfoV06" runat="server" DonTextMode="true"
                                    V_PointdeVue="1" V_Objet="170" V_Information="6" V_SiDonneeDico="true"
                                    EtiWidth="393px" DonWidth="393px" DonHeight="50px" EtiHeight="20px" DonTabIndex="8" 
                                    EtiText="Commentaire" Etivisible="True"
                                    Donstyle="margin-left: 2px;" DonBorderWidth="1px"
                                    Etistyle="margin-left: 2px; text-align: center; text-indent: 0px;"/>
                           </asp:TableCell>
                        </asp:TableRow>
                  </asp:Table>
              </asp:TableCell>
            </asp:TableRow>
          </asp:Table>
       </asp:TableCell>
    </asp:TableRow>

 </asp:Table>