﻿Option Strict On
Option Explicit On
Option Compare Text
Imports Virtualia.Systeme.Evenements
Imports VI = Virtualia.Systeme.Constantes
Public Class FrmSemaineActivite
    Inherits System.Web.UI.Page
    Private WebFct As Virtualia.Net.Controles.WebFonctions
    Private WsNomStateCache As String = "VActivite"
    Private WsCtl_Cache As Virtualia.Net.VCaches.CacheFenetreFrm
    Private WsLstActivites As List(Of Virtualia.TablesObjet.ShemaPER.PER_ACTIVITE_JOUR)

    Private Enum Ivue As Integer
        IActivite = 0
        IArmoire = 1
        IEdition = 2
        IAdministration = 3
        IMessage = 4
    End Enum

    Private Property CacheVirControle As Virtualia.Net.VCaches.CacheFenetreFrm
        Get
            If Me.ViewState(WsNomStateCache) IsNot Nothing Then
                Return CType(Me.ViewState(WsNomStateCache), Virtualia.Net.VCaches.CacheFenetreFrm)
            End If
            Dim NewCache As Virtualia.Net.VCaches.CacheFenetreFrm
            NewCache = New Virtualia.Net.VCaches.CacheFenetreFrm
            Return NewCache
        End Get
        Set(value As Virtualia.Net.VCaches.CacheFenetreFrm)
            If value Is Nothing Then
                Exit Property
            End If
            If Me.ViewState(WsNomStateCache) IsNot Nothing Then
                Me.ViewState.Remove(WsNomStateCache)
            End If
            Me.ViewState.Add(WsNomStateCache, value)
        End Set
    End Property

    Private Sub FrmSemaineActivite_Init(sender As Object, e As EventArgs) Handles Me.Init
        Dim Msg As String = ""
        Dim NumSession As String = Server.HtmlDecode(Request.QueryString("IDVirtualia"))
        If NumSession <> Session.SessionID Then
            Msg = "Erreur lors de l'identification."
            Response.Redirect("~/Fenetres/Commun/ErreurApplication.aspx?Msg=" & Msg)
            Exit Sub
        End If
        WebFct = New Virtualia.Net.Controles.WebFonctions(Me, 1)
        If WebFct.PointeurUtilisateur Is Nothing Then
            Msg = "Erreur lors de l'identification. Vous avez été déconnecté(e)."
            Response.Redirect("~/Fenetres/Commun/ErreurApplication.aspx?Msg=" & Msg)
            Exit Sub
        End If
        If WebFct.VerifierCookie(Me, Session.SessionID) = False Then
            Msg = "Erreur lors de l'identification du dossier accédé. Session invalide."
            Response.Redirect("~/Fenetres/Commun/ErreurApplication.aspx?Msg=" & Msg)
            Exit Sub
        End If
    End Sub

    Private Sub FrmSemaineActivite_PreRender(sender As Object, e As EventArgs) Handles Me.PreRender
        If WebFct.PointeurUtilisateur Is Nothing Then
            Exit Sub
        End If
        WsCtl_Cache = CacheVirControle
        If CType(WebFct.PointeurUtilisateur.V_PointeurSessionModule, Virtualia.Net.Session.SessionModule).SiAccesGRH = True Then
            If WsCtl_Cache.Ide_Dossier = 0 Then
                MultiVues.ActiveViewIndex = Ivue.IArmoire
                Exit Sub
            End If
        End If
        MultiVues.ActiveViewIndex = WsCtl_Cache.Index_VueActive
    End Sub

    Private Sub FrmSemaineActivite_PreRenderComplete(sender As Object, e As EventArgs) Handles Me.PreRenderComplete
        Dim FichierHtml As String
        FichierHtml = WebFct.PointeurGlobal.VirRepertoireTemporaire & "\" & Session.SessionID & ".html"
        Response.ContentEncoding = System.Text.Encoding.UTF8
        Response.Filter = New Virtualia.Systeme.Outils.FiltreReponseParMemoryStream(Response.Filter, FichierHtml)
    End Sub

    Private Sub MsgVirtualia_ValeurRetour(sender As Object, e As Systeme.Evenements.MessageRetourEventArgs) Handles MsgVirtualia.ValeurRetour
        WsCtl_Cache = CacheVirControle
        Select Case e.Emetteur
            Case "Figer"
                If e.ReponseMsg = "Oui" Then
                    Call CtlActivite.ViserLaSemaine()
                End If
                WsCtl_Cache.Index_VueActive = Ivue.IActivite
            Case "Anomalie"
                If e.ReponseMsg = "Oui" Then
                    Call CtlActivite.ValiderSaisieEtape()
                End If
                WsCtl_Cache.Index_VueActive = Ivue.IActivite
            Case "REFActivite"
                WsCtl_Cache.Index_VueActive = Ivue.IAdministration
        End Select
        CacheVirControle = WsCtl_Cache
        Select Case e.Emetteur
            Case "REFActivite"
                MultiVues.SetActiveView(VueAdministration)
            Case Else
                MultiVues.SetActiveView(VueActivite)
        End Select
    End Sub

    Private Sub Ctl_MessageDialogue(sender As Object, e As Systeme.Evenements.MessageSaisieEventArgs) Handles CtlActivite.MessageDialogue, MajReferentiel.MessageDialogue
        'HPopupMsg.Value = "1"
        'CellMessage.Visible = True
        MsgVirtualia.AfficherMessage = e
        WsCtl_Cache = CacheVirControle
        WsCtl_Cache.Index_VueActive = Ivue.IMessage
        CacheVirControle = WsCtl_Cache
        'PopupMsg.Show()
    End Sub

    Private Sub CtlActivite_ValeurRetour(sender As Object, e As Systeme.Evenements.MessageRetourEventArgs) Handles CtlActivite.ValeurRetour
        'HPopupMsg.Value = "0"
        'CellMessage.Visible = False
        WsCtl_Cache = CacheVirControle
        WsCtl_Cache.Ide_Dossier = 0
        WsCtl_Cache.Index_VueActive = Ivue.IArmoire
        CacheVirControle = WsCtl_Cache
    End Sub

    Private Sub CommandeEdit_Click(sender As Object, e As EventArgs) Handles CommandeEdit.Click
        WsCtl_Cache = CacheVirControle
        WsCtl_Cache.Ide_Dossier = 999999999
        WsCtl_Cache.Index_VueActive = Ivue.IEdition
        CacheVirControle = WsCtl_Cache
    End Sub

    Private Sub Ctl_ValeurRetour(sender As Object, e As Systeme.Evenements.MessageRetourEventArgs) Handles EtatMensuel.ValeurRetour, MajReferentiel.ValeurRetour
        WsCtl_Cache = CacheVirControle
        WsCtl_Cache.Ide_Dossier = 0
        WsCtl_Cache.Index_VueActive = Ivue.IArmoire
        CacheVirControle = WsCtl_Cache
    End Sub

    Private Sub Armoire_Dossier_Click(ByVal sender As Object, ByVal e As Systeme.Evenements.DossierClickEventArgs) Handles ArmoirePER.Dossier_Click
        If e.Identifiant = 0 Then
            Exit Sub
        End If
        If WebFct Is Nothing Then
            WebFct = New Virtualia.Net.Controles.WebFonctions(Me, 1)
        End If
        Dim Dossier As Virtualia.Net.WebAppli.SelfV4.DossierPersonne = CType(WebFct.PointeurGlobal, Virtualia.Net.Session.ObjetGlobal).Dossier(e.Identifiant)
        If Dossier Is Nothing Then
            Exit Sub
        End If
        Dossier.Nom_Prenom_Manager = WebFct.PointeurUtilisateur.V_NomdeConnexion
        CType(WebFct.PointeurUtilisateur.V_PointeurSessionModule, Virtualia.Net.Session.SessionModule).SiAccesManager = True
        CType(WebFct.PointeurUtilisateur.V_PointeurSessionModule, Virtualia.Net.Session.SessionModule).DossierPER = Dossier
        WsCtl_Cache = CacheVirControle
        WsCtl_Cache.Ide_Dossier = e.Identifiant
        WsCtl_Cache.Index_VueActive = Ivue.IActivite
        CacheVirControle = WsCtl_Cache
        CtlActivite.Initialiser()
        MultiVues.SetActiveView(VueActivite)
    End Sub

    Private Sub CommandeAdm_Click(sender As Object, e As EventArgs) Handles CommandeAdm.Click
        WsCtl_Cache = CacheVirControle
        WsCtl_Cache.Ide_Dossier = 1
        WsCtl_Cache.Index_VueActive = Ivue.IAdministration
        CacheVirControle = WsCtl_Cache
        MajReferentiel.Identifiant = 1
    End Sub

End Class