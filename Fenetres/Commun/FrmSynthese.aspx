﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/PagesMaitres/VirtualiaMain.Master" CodeBehind="FrmSynthese.aspx.vb"
    Inherits="Virtualia.Net.FrmSynthese" MaintainScrollPositionOnPostback="True" %>

<%@ MasterType VirtualPath="~/PagesMaitres/VirtualiaMain.master" %>
<%@ Register Src="~/Controles/HeuresSupp/Tableaux/CtrlGraphique.ascx" TagName="VTableau" TagPrefix="HeuresSupp" %>
<%@ Register Src="~/Controles/Commun/VListeGrid.ascx" TagName="VListeGrid" TagPrefix="Virtualia" %>
<%@ Register Src="~/Controles/Commun/VListeCombo.ascx" TagName="VListeCombo" TagPrefix="Virtualia" %>
<%@ Register src="~/Controles/Commun/VArmoirePER.ascx" tagname="VArmoire" tagprefix="Virtualia" %>

<%--<asp:Content ID="LigneAnnee" ContentPlaceHolderID="HautdePage" runat="server">  </asp:Content>--%>
<%--<asp:Content ID="CadreSynthese" runat="server" ContentPlaceHolderID="Principal">--%>
<asp:Content ID="Corps" ContentPlaceHolderID="Principal" runat="server">
    <asp:Table ID="CadreGeneral" runat="server" HorizontalAlign="Center" Width="1050px">
        <asp:TableRow>
            <asp:TableCell HorizontalAlign="Center" VerticalAlign="Top">
                <asp:MultiView ID="MultiVues" runat="server" ActiveViewIndex="0">
                    <asp:View ID="VueSynthese" runat="server">
                      <asp:Panel ID="CadreAffichage" runat="server" BackColor="Transparent" HorizontalAlign="Center" Width="1050px"
                            BackImageUrl="~/Images/Fonds/Fond_Saisie.jpg" BorderStyle="none">
    <asp:Table ID="CadreAnnee" runat="server" HorizontalAlign="Center" Width="1050px">
        <asp:TableRow>
            <asp:TableCell ID="CellRetour" Width="150px" VerticalAlign="Top" HorizontalAlign="Left">
                <asp:ImageButton ID="CommandeRetour" runat="server" Width="32px" Height="32px" Visible="true"
                    BorderStyle="None" ImageUrl="~/Images/Boutons/FlecheRetourContexte.jpg"
                    ImageAlign="Middle" Style="margin-left: 0px; margin-bottom: 10px;"></asp:ImageButton>
            </asp:TableCell>
        </asp:TableRow>
        <asp:TableRow>
            <asp:TableCell VerticalAlign="Top" HorizontalAlign="Left">
                <asp:Label ID="EtiSelMoisAnnee" runat="server" Text="Période mensuelle"
                    Height="20px" Width="150px" BackColor="#225C59" ForeColor="#D7FAF3"
                    BorderColor="#B6C7E2" BorderStyle="Ridge" BorderWidth="2px"
                    Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small" Font-Italic="true"
                    Style="margin-left: 0px; text-align: center">
                </asp:Label>
            </asp:TableCell>
            <asp:TableCell VerticalAlign="Top" HorizontalAlign="Left">

                <asp:DropDownList ID="DropDownMoisAnnee" runat="server" AutoPostBack="True"
                    Height="22px" Width="150px" BackColor="#A8BBB8" ForeColor="#124545" Font-Bold="False" 
                    Style="margin-top: 0px; margin-left: 5px; border-spacing: 2px; font-family: Trebuchet MS;
                    font-size: small; font-style: normal; text-indent: 5px; text-align: left">
                </asp:DropDownList>
            </asp:TableCell>
            <asp:TableCell VerticalAlign="Middle" HorizontalAlign="Center">
                <asp:Label ID="EtiTypeArmoire" runat="server" Height="20px" Width="150px" 
                    BackColor="#225C59" ForeColor="#D7FAF3" BorderStyle="Ridge" BorderColor="#B6C7E2" 
                    BorderWidth="2px" Text="Choix du type de liste" Font-Bold="False" Font-Names="Trebuchet MS" 
                    Font-Size="Small" Font-Italic="true" Style="margin-top: 0px; margin-left: 20px; text-align: center" />
            </asp:TableCell><asp:TableCell VerticalAlign="Middle" HorizontalAlign="Center">
                <asp:DropDownList ID="DropDownArmoire" runat="server" AutoPostBack="True" Height="22px" 
                    Width="400px" BackColor="#A8BBB8" ForeColor="#124545" Font-Bold="False" 
                    Font-Names="Trebuchet MS" Font-Size="Small" Font-Italic="false" 
                    Style="margin-top: 0px; margin-left: 5px; border-spacing: 2px; text-indent: 5px; text-align: left" >
                </asp:DropDownList>
            </asp:TableCell><asp:TableCell>
                <asp:Button ID="CommandeCsv" NavigateUrl="~/Fenetres/Commun/FrmSynthese.aspx" runat="server" BackColor="#1C5150" Width="80px" Height="22px" AutoPostBack="True"
                    BorderStyle="Outset" BorderWidth="2px" BorderColor="WhiteSmoke"
                    Font-Names="Trebuchet MS" Font-Size="Small" ForeColor="#B0E0D7" Font-Bold="false"
                    Font-Underline="true" Font-Italic="true" Visible="True"
                    Text="Fichier Csv" ToolTip="Fichier Csv"
                    Style="margin-top: 0px; margin-left: 5px;  text-align: center"></asp:Button>
            </asp:TableCell></asp:TableRow><%--        <asp:TableRow>
           <asp:TableCell HorizontalAlign="Center">
              <HeuresSupp:VTableau ID="CtlTableaux" runat="server" />
           </asp:TableCell>
        </asp:TableRow>  --%></asp:Table>
                             </asp:Panel>               
                    </asp:View>
                     <asp:View ID="VueArmoire" runat="server">
                        <Virtualia:VArmoire ID="ArmoirePER" runat="server" V_Appelant="HSUPP" />
                    </asp:View>
                    </asp:MultiView>  
                </asp:TableCell>
            </asp:TableRow>
            </asp:Table>
</asp:Content> 