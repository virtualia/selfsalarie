﻿Option Explicit On
Option Strict On
Option Compare Text
Imports VI = Virtualia.Systeme.Constantes
Namespace Session
    Public Class ObjetGlobal
        Inherits Virtualia.Net.Session.ObjetGlobalBase
        Private WsReferentiel As Virtualia.Net.WebAppli.ObjetReferentiel = Nothing
        Private WsLstDossiers As List(Of Virtualia.Net.WebAppli.SelfV4.DossierPersonne)

        '** Armoire pour Gestion des dossiers PER
        Private WsListeArmoires As List(Of Virtualia.Net.Datas.ObjetArmoire)
        '** Paramètres Heure Supp
        Private WsLstHsSecteurs As List(Of String) = Nothing
        Private WsLstHsFonctions As List(Of String) = Nothing
        Public ReadOnly Property Dossier(ByVal Ide As Integer) As Virtualia.Net.WebAppli.SelfV4.DossierPersonne
            Get
                Try
                    Return WsLstDossiers.Find(Function(recherche) recherche.Ide_Dossier = Ide)
                Catch ex As Exception
                    Return Nothing
                End Try
            End Get
        End Property

        Public ReadOnly Property SiEstAussiManager(ByVal Ide As Integer) As Boolean
            Get
                Dim Constructeur As Virtualia.Systeme.Sgbd.Sql.SqlInterne
                Dim LstResultat As List(Of String)
                Dim OrdreSql As String

                Constructeur = New Virtualia.Systeme.Sgbd.Sql.SqlInterne(VirModele, VirInstanceBd)
                'Constructeur.NombredeRequetes(VI.PointdeVue.PVueVueLogique, "", "", VI.Operateurs.ET) = 1
                Constructeur.NombredeRequetes(VI.PointdeVue.PVueDirection, "", "", VI.Operateurs.ET) = 1
                Constructeur.SiPasdeTriSurIdeDossier = True
                Constructeur.NoInfoSelection(0, 2) = 4

                Constructeur.ValeuraComparer(0, VI.Operateurs.OU, VI.Operateurs.Egalite, False) = CStr(Ide)
                Constructeur.InfoExtraite(0, 2, 0) = 4

                OrdreSql = Constructeur.OrdreSqlDynamique

                'ChaineLue = Proxy.SelectionSql(WsPointeurUtiGlobal.Nom, VI.PointdeVue.PVueVueLogique, 1, OrdreSql, 1)
                LstResultat = VirServiceServeur.RequeteSql_ToListeChar(VirNomUtilisateur, VI.PointdeVue.PVueDirection, 1, OrdreSql)

                If LstResultat Is Nothing OrElse LstResultat.Count = 0 Then
                    Return False
                End If
                Return True
            End Get
        End Property
        Public ReadOnly Property SiEstAussiDG(ByVal Ide As Integer) As Boolean
            Get
                Dim Constructeur As Virtualia.Systeme.Sgbd.Sql.SqlInterne
                Dim LstResultat As List(Of Virtualia.Net.ServiceServeur.VirRequeteType)
                Dim OrdreSql As String

                Constructeur = New Virtualia.Systeme.Sgbd.Sql.SqlInterne(VirModele, VirInstanceBd)
                'Constructeur.NombredeRequetes(VI.PointdeVue.PVueVueLogique, "", "", VI.Operateurs.ET) = 1
                Constructeur.NombredeRequetes(VI.PointdeVue.PVueDirection, "", "", VI.Operateurs.ET) = 1
                Constructeur.SiPasdeTriSurIdeDossier = True
                Constructeur.NoInfoSelection(0, 2) = 4

                Constructeur.ValeuraComparer(0, VI.Operateurs.OU, VI.Operateurs.Egalite, False) = CStr(Ide)
                Constructeur.InfoExtraite(0, 2, 0) = 2

                OrdreSql = Constructeur.OrdreSqlDynamique

                'ChaineLue = Proxy.SelectionSql(WsPointeurUtiGlobal.Nom, VI.PointdeVue.PVueVueLogique, 1, OrdreSql, 1)
                LstResultat = VirServiceServeur.RequeteSql_ToListeType(VirNomUtilisateur, VI.PointdeVue.PVueDirection, 1, OrdreSql)

                If LstResultat Is Nothing OrElse LstResultat.Count = 0 Then
                    Return False
                End If
                If LstResultat.Item(0).Valeurs(0) = "1" Then
                    Return True
                Else
                    Return False
                End If
            End Get
        End Property

        Public ReadOnly Property LesStructures_N1() As String
            Get
                Dim Constructeur As Virtualia.Systeme.Sgbd.Sql.SqlInterne
                Dim ChaineSql As String
                Dim ResultatPage As List(Of String)
                Dim TableauData(0) As String
                Dim IndiceI As Integer
                Dim ChaineResultat As System.Text.StringBuilder

                Constructeur = New Virtualia.Systeme.Sgbd.Sql.SqlInterne(VirModele, VirInstanceBd)
                Constructeur.NombredeRequetes(VI.PointdeVue.PVueDirection, "", "", VI.Operateurs.ET) = 1
                Constructeur.SiPasdeTriSurIdeDossier = True
                Constructeur.NoInfoSelection(0, 1) = 1
                Constructeur.InfoExtraite(0, 1, 1) = 1
                ChaineSql = Constructeur.OrdreSqlDynamique

                ResultatPage = VirServiceServeur.RequeteSql_ToListeChar(VirNomUtilisateur, VI.PointdeVue.PVueDirection, 1, ChaineSql)

                ChaineResultat = New System.Text.StringBuilder
                For IndiceI = 0 To ResultatPage.Count - 1
                    If ResultatPage.Item(IndiceI) = "" Then
                        Exit For
                    End If
                    TableauData = Split(ResultatPage.Item(IndiceI), VI.Tild, -1)
                    ChaineResultat.Append(TableauData(1) & VI.Tild)
                Next IndiceI
                Return ChaineResultat.ToString
            End Get
        End Property
        Public ReadOnly Property ListeDirecteurs_N1(ByVal Ide As Integer) As List(Of Integer)
            Get
                Dim Constructeur As Virtualia.Systeme.Sgbd.Sql.SqlInterne
                Dim LstResultat As List(Of Virtualia.Net.ServiceServeur.VirRequeteType)
                Dim LstPresel As List(Of Integer)
                Dim OrdreSql As String
                Dim IndiceI As Integer
                Dim NiveauSel As Integer

                LstPresel = New List(Of Integer)
                For IndiceI = 0 To 10
                    LstPresel.Add((IndiceI * 10) + 1)
                Next IndiceI

                Constructeur = New Virtualia.Systeme.Sgbd.Sql.SqlInterne(VirModele, VirInstanceBd)
                'Constructeur.NombredeRequetes(VI.PointdeVue.PVueVueLogique, "", "", VI.Operateurs.ET) = 1
                Constructeur.NombredeRequetes(VI.PointdeVue.PVueDirection, "", "", VI.Operateurs.ET) = 1
                Constructeur.SiPasdeTriSurIdeDossier = False
                Constructeur.PreselectiondIdentifiants = LstPresel
                Constructeur.NoInfoSelection(0, 2) = 4
                Constructeur.InfoExtraite(0, 2, 0) = 4

                OrdreSql = Constructeur.OrdreSqlDynamique

                'ChaineLue = Proxy.SelectionSql(WsPointeurUtiGlobal.Nom, VI.PointdeVue.PVueVueLogique, 1, OrdreSql, 1)
                LstResultat = VirServiceServeur.RequeteSql_ToListeType(VirNomUtilisateur, VI.PointdeVue.PVueDirection, 1, OrdreSql)

                If LstResultat Is Nothing OrElse LstResultat.Count = 0 Then
                    Return Nothing
                End If
                For Each Res As Virtualia.Net.ServiceServeur.VirRequeteType In LstResultat
                    If IsNumeric(Res.Valeurs(0)) AndAlso CInt(Res.Valeurs(0)) = Ide Then
                        NiveauSel = Res.Ide_Dossier
                        Exit For
                    End If
                Next
                LstPresel = New List(Of Integer)
                For Each Res As Virtualia.Net.ServiceServeur.VirRequeteType In LstResultat
                    If Res.Ide_Dossier = NiveauSel Then
                        If IsNumeric(Res.Valeurs(0)) AndAlso CInt(Res.Valeurs(0)) <> Ide Then
                            LstPresel.Add(CInt(Res.Valeurs(0)))
                        End If
                    End If
                Next
                Return LstPresel
            End Get
        End Property

        Public ReadOnly Property SiMaxiDemandeHS(ByVal DateValeur As Date) As Boolean
            Get
                Dim DateDebut As Date = VirRhDates.DateTypee("01" & Strings.Right(VirRhDates.DateduJour, 8))
                If DateValeur.AddMonths(1) < DateDebut Then
                    Return False
                End If
                Dim ChaineConfig As String = System.Configuration.ConfigurationManager.AppSettings("HS_DelaiDemande")
                Dim NBJours As Integer = 15
                If ChaineConfig Is Nothing OrElse ChaineConfig = "" Then
                    NBJours = 15
                ElseIf IsNumeric(ChaineConfig) Then
                    NBJours = CInt(ChaineConfig)
                End If
                If Now.Day <= NBJours Then
                    Return True
                End If
                Return False
            End Get
        End Property

        Public ReadOnly Property VirReferentiel As Virtualia.Net.WebAppli.ObjetReferentiel
            Get
                If WsReferentiel Is Nothing Then
                    WsReferentiel = New Virtualia.Net.WebAppli.ObjetReferentiel(Me)
                End If
                Return WsReferentiel
            End Get
        End Property

        Public ReadOnly Property VirObjetPresence As Virtualia.Ressources.Datas.ObjetPresence
            Get
                If WsReferentiel Is Nothing Then
                    WsReferentiel = New Virtualia.Net.WebAppli.ObjetReferentiel(Me)
                End If
                Return WsReferentiel.RessourcePresence
            End Get
        End Property

        Public ReadOnly Property VirClauseFiltre(ByVal Ide As Integer, ByVal DateDebut As String, ByVal DateFin As String, ByVal NoObjet As Integer) As String
            Get
                Dim Constructeur As Virtualia.Systeme.Sgbd.Sql.SqlInterne
                Dim ChaineSql As String

                Constructeur = New Virtualia.Systeme.Sgbd.Sql.SqlInterne(VirModele, VirInstanceBd)
                Constructeur.NombredeRequetes(VI.PointdeVue.PVueApplicatif, DateDebut, DateFin, VI.Operateurs.ET) = 1
                Constructeur.SiPasdeTriSurIdeDossier = True
                Constructeur.SiHistoriquedeSituation = False
                Constructeur.IdentifiantDossier = Ide
                Constructeur.NoInfoSelection(0, NoObjet) = 0
                Constructeur.ValeuraComparer(0, VI.Operateurs.OU, VI.Operateurs.Inclu, False) = DateDebut & VI.PointVirgule & DateFin

                ChaineSql = Constructeur.OrdreSqlDynamique
                Constructeur = Nothing

                Return ChaineSql
            End Get
        End Property

        Public ReadOnly Property VirListeDiffusionDRH As List(Of String)
            Get
                Dim ChaineConfig As String = System.Configuration.ConfigurationManager.AppSettings("MailsDRH")
                If ChaineConfig Is Nothing OrElse ChaineConfig = "" Then
                    Return Nothing
                End If
                Dim TableauData(0) As String
                TableauData = Strings.Split(ChaineConfig, VI.PointVirgule, -1)
                Return TableauData.ToList
            End Get
        End Property

        Public ReadOnly Property VirListeSecteursHS As List(Of String)
            Get
                If WsLstHsSecteurs IsNot Nothing AndAlso WsLstHsSecteurs.Count > 0 Then
                    Return WsLstHsSecteurs
                End If
                Dim ChaineConfig As String = System.Configuration.ConfigurationManager.AppSettings("HS_Secteur_Auto")
                If ChaineConfig Is Nothing OrElse ChaineConfig = "" Then
                    Return Nothing
                End If
                Dim TableauData(0) As String
                TableauData = Strings.Split(ChaineConfig, VI.PointVirgule, -1)
                WsLstHsSecteurs = TableauData.ToList
                Return WsLstHsSecteurs
            End Get
        End Property

        Public ReadOnly Property VirListeFonctionsHS As List(Of String)
            Get
                If WsLstHsFonctions IsNot Nothing AndAlso WsLstHsFonctions.Count > 0 Then
                    Return WsLstHsFonctions
                End If
                Dim ChaineConfig As String = System.Configuration.ConfigurationManager.AppSettings("HS_Fonction_Auto")
                If ChaineConfig Is Nothing OrElse ChaineConfig = "" Then
                    Return Nothing
                End If
                Dim TableauData(0) As String
                TableauData = Strings.Split(ChaineConfig, VI.PointVirgule, -1)
                WsLstHsFonctions = TableauData.ToList
                Return WsLstHsFonctions
            End Get
        End Property

        Public ReadOnly Property ListeTablesDecision(ByVal Ide As Integer) As List(Of Virtualia.TablesObjet.ShemaREF.OUT_DECISION)
            Get
                If WsReferentiel Is Nothing Then
                    WsReferentiel = New Virtualia.Net.WebAppli.ObjetReferentiel(Me)
                End If
                Return WsReferentiel.ListeOutDecision(Ide)
            End Get
        End Property
        Public ReadOnly Property FicheRubrique(ByVal CodeRubrique As String) As Virtualia.TablesObjet.ShemaREF.OUT_RUBRIQUES
            Get
                If WsReferentiel Is Nothing Then
                    WsReferentiel = New Virtualia.Net.WebAppli.ObjetReferentiel(Me)
                End If
                Return WsReferentiel.OutRubrique(CodeRubrique)
            End Get
        End Property
        Private Function NombreDossiersArmoire(ByVal TypeArmoireActive As Integer) As Integer
            Const BaseComplete As Integer = 0
            Const EnActivite As Integer = 1
            Const Personnalisee As Integer = 1000
            Const LePanel As Integer = 1001

            Dim NouvelleArmoire As Virtualia.Net.Datas.ObjetArmoire
            Dim NbDossiers As Integer = 0
            NouvelleArmoire = New Virtualia.Net.Datas.ObjetArmoire(Me, VI.PointdeVue.PVueApplicatif)
            NouvelleArmoire.InfoIconeArmoire(0, False) = 0
            NouvelleArmoire.SiArmoireStandard = True
            Select Case TypeArmoireActive
                Case BaseComplete
                    NouvelleArmoire.NomdelArmoire = "Tous les dossiers"
                    NbDossiers = NouvelleArmoire.ArmoireComplete(VirRhDates.DateduJour)
                Case EnActivite
                    NouvelleArmoire.NomdelArmoire = "Les dossiers en activité"
                    NbDossiers = NouvelleArmoire.SelectionDynamique(EnActivite, VI.ObjetPer.ObaActivite, 1, VI.Operateurs.OU, VI.Operateurs.Inclu, LesPositionsdActivite, "", VirRhDates.DateduJour)
                Case Personnalisee
                    NouvelleArmoire.NomdelArmoire = "Armoire personnalisée"
                    NouvelleArmoire.SiArmoireStandard = False
                Case LePanel
                    NouvelleArmoire.NomdelArmoire = "Le panel issu de la recherche"
                    NouvelleArmoire.SiArmoireStandard = False
            End Select
            NouvelleArmoire.TypeArmoire = TypeArmoireActive
            If WsListeArmoires Is Nothing Then
                WsListeArmoires = New List(Of Virtualia.Net.Datas.ObjetArmoire)
            End If
            WsListeArmoires.Add(NouvelleArmoire)
            Select Case TypeArmoireActive
                Case Is > 999
                    Return NouvelleArmoire.NouvelEnsemble.Count
                Case Else
                    Return NbDossiers
            End Select
        End Function

        Public ReadOnly Property ItemArmoire(ByVal TypeArmoireActive As Integer) As Virtualia.Net.Datas.ObjetArmoire
            Get
                If WsListeArmoires Is Nothing Then
                    Dim Nb As Integer = NombreDossiersArmoire(TypeArmoireActive)
                End If
                Dim IndiceA As Integer
                For IndiceA = 0 To WsListeArmoires.Count - 1
                    Select Case WsListeArmoires.Item(IndiceA).TypeArmoire
                        Case Is = TypeArmoireActive
                            Return WsListeArmoires.Item(IndiceA)
                    End Select
                Next IndiceA
                Return Nothing
            End Get
        End Property

        Private ReadOnly Property LesPositionsdActivite() As String
            Get
                Dim Constructeur As Virtualia.Systeme.Sgbd.Sql.SqlInterne
                Dim ChaineSql As String
                Dim ResultatPage As List(Of String)
                Dim TableauData(0) As String
                Dim IndiceI As Integer
                Dim ChaineActivite As System.Text.StringBuilder

                Constructeur = New Virtualia.Systeme.Sgbd.Sql.SqlInterne(VirModele, VirInstanceBd)
                Constructeur.NombredeRequetes(VI.PointdeVue.PVuePosition, "", "", VI.Operateurs.ET) = 1
                Constructeur.SiPasdeTriSurIdeDossier = False
                Constructeur.NoInfoSelection(0, 1) = 8
                Constructeur.ValeuraComparer(0, VI.Operateurs.OU, VI.Operateurs.Egalite, False) = "Oui;"
                Constructeur.InfoExtraite(0, 1, 0) = 1
                ChaineSql = Constructeur.OrdreSqlDynamique

                ResultatPage = VirServiceServeur.RequeteSql_ToListeChar(VirNomUtilisateur, VI.PointdeVue.PVuePosition, 1, ChaineSql)

                ChaineActivite = New System.Text.StringBuilder
                For IndiceI = 0 To ResultatPage.Count - 1
                    If ResultatPage.Item(IndiceI) = "" Then
                        Exit For
                    End If
                    TableauData = Split(ResultatPage.Item(IndiceI), VI.Tild, -1)
                    ChaineActivite.Append(TableauData(1) & VI.PointVirgule)
                Next IndiceI
                Return ChaineActivite.ToString
            End Get
        End Property

        Public Sub EnvoyerEmail(ByVal Sujet As String, ByVal Emetteur As String, ByVal Destinataire As String, ByVal Contenu As String)
            Dim ProxyOutil As Virtualia.Net.WebService.VirtualiaOutils
            Dim CodeRetour As Boolean
            Try
                If MyBase.UrlWebOutil = "" Then
                    Exit Sub
                End If
            Catch ex As Exception
                Exit Sub
            End Try
            ProxyOutil = New Virtualia.Net.WebService.VirtualiaOutils(MyBase.UrlWebOutil)
            Select Case Sujet
                Case ""
                    Try
                        CodeRetour = ProxyOutil.EnvoyerEmail(Emetteur, Destinataire, Contenu, False)
                    Catch ex As Exception
                        Exit Try
                    End Try
                Case Else
                    Try
                        CodeRetour = ProxyOutil.EnvoyerEmail2(Sujet, Emetteur, Destinataire, Contenu, False)
                    Catch ex As Exception
                        Exit Try
                    End Try
            End Select

        End Sub

        Private Sub FaireListeDossiers()
            Dim Constructeur As Virtualia.Systeme.Sgbd.Sql.SqlInterne
            Dim Dossier As Virtualia.Net.WebAppli.SelfV4.DossierPersonne
            Dim ChaineSql As String
            Dim TabRes As List(Of String)
            Dim TableauData(0) As String
            Dim IndiceI As Integer
            Dim DateFin As String = "31/12/" & Year(Now).ToString

            DateFin = ""
            'Extraction Infos Obligatoirement présentes
            Constructeur = New Virtualia.Systeme.Sgbd.Sql.SqlInterne(VirModele, VirInstanceBd)
            Constructeur.NombredeRequetes(VI.PointdeVue.PVueApplicatif, "", DateFin, VI.Operateurs.ET) = 1
            Constructeur.SiPasdeTriSurIdeDossier = False
            Constructeur.SiHistoriquedeSituation = False
            Constructeur.SiForcerClauseDistinct = False

            Constructeur.NoInfoSelection(0, VI.ObjetPer.ObaCivil) = 2 'le nom
            Constructeur.InfoExtraite(0, VI.ObjetPer.ObaAdrPro, 0) = 11 'login AD
            Constructeur.InfoExtraite(1, VI.ObjetPer.ObaAdrPro, 0) = 7 'EMail
            Constructeur.InfoExtraite(2, VI.ObjetPer.ObaCivil, 0) = 2 'le nom
            Constructeur.InfoExtraite(3, VI.ObjetPer.ObaCivil, 0) = 3 'le prénom
            Constructeur.InfoExtraite(4, VI.ObjetPer.ObaCivil, 0) = 1 'Civilité

            ChaineSql = Constructeur.OrdreSqlDynamique

            TabRes = VirServiceServeur.RequeteSql_ToListeChar(VirNomUtilisateur, VI.PointdeVue.PVueApplicatif, VI.ObjetPer.ObaAdrPro, ChaineSql)
            WsLstDossiers = New List(Of Virtualia.Net.WebAppli.SelfV4.DossierPersonne)

            For IndiceI = 0 To TabRes.Count - 1
                Dossier = New Virtualia.Net.WebAppli.SelfV4.DossierPersonne(Me, TabRes(IndiceI).ToString)
                WsLstDossiers.Add(Dossier)
            Next IndiceI
        End Sub

        Private Sub PurgerTemporaire()
            Dim RepTemp As String = VirRepertoireTemporaire
            If My.Computer.FileSystem.DirectoryExists(RepTemp) = True Then
                Try
                    My.Computer.FileSystem.DeleteDirectory(RepTemp, FileIO.DeleteDirectoryOption.DeleteAllContents)
                Catch ex As Exception
                    Exit Try
                End Try
            End If
        End Sub

        Private Sub ChangerBaseCourante()
            Dim CodeRetour As Boolean
            CodeRetour = VirServiceServeur.ChangerBasedeDonneesCourante(VirNomUtilisateur, VirNoBd)
        End Sub

        Public Sub New(ByVal NomUti As String, ByVal NoBd As Integer)
            MyBase.New(NomUti, NoBd)
            MyBase.VirNomModule = "SelfV4"

            Call FaireListeDossiers()
        End Sub

        Public Sub New()
            MyBase.New()
        End Sub
    End Class
End Namespace