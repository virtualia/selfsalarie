﻿'------------------------------------------------------------------------------
' <généré automatiquement>
'     Ce code a été généré par un outil.
'
'     Les modifications apportées à ce fichier peuvent provoquer un comportement incorrect et seront perdues si
'     le code est régénéré.
' </généré automatiquement>
'------------------------------------------------------------------------------

Option Strict On
Option Explicit On


Partial Public Class FrmPFR
    
    '''<summary>
    '''Contrôle CadreAnnee.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CadreAnnee As Global.System.Web.UI.WebControls.Table
    
    '''<summary>
    '''Contrôle ComboSelAnnee.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents ComboSelAnnee As Global.Virtualia.Net.Controles_VListeCombo
    
    '''<summary>
    '''Contrôle CadreGeneral.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CadreGeneral As Global.System.Web.UI.WebControls.Table
    
    '''<summary>
    '''Contrôle TableOnglets.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents TableOnglets As Global.System.Web.UI.WebControls.Table
    
    '''<summary>
    '''Contrôle MenuChoix.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents MenuChoix As Global.System.Web.UI.WebControls.Menu
    
    '''<summary>
    '''Contrôle LienSynthese.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LienSynthese As Global.System.Web.UI.WebControls.HyperLink
    
    '''<summary>
    '''Contrôle LienNotification.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LienNotification As Global.System.Web.UI.WebControls.HyperLink
    
    '''<summary>
    '''Contrôle LienAdmin.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents LienAdmin As Global.System.Web.UI.WebControls.HyperLink
    
    '''<summary>
    '''Contrôle CadreAffichage.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CadreAffichage As Global.System.Web.UI.WebControls.Panel
    
    '''<summary>
    '''Contrôle CadreSelection.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CadreSelection As Global.System.Web.UI.WebControls.Table

    '''<summary>
    '''Contrôle RadioSelRegime.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents RadioSelRegime As Global.Virtualia.Net.Controles_VTrioHorizontalRadio

    '''<summary>
    '''Contrôle EtiTotal.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents EtiTotal As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle ComboSelTri.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents ComboSelTri As Global.Virtualia.Net.Controles_VListeCombo
    
    '''<summary>
    '''Contrôle CadreCsv.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CadreCsv As Global.System.Web.UI.WebControls.Table
    
    '''<summary>
    '''Contrôle CommandeCsv.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CommandeCsv As Global.System.Web.UI.WebControls.Button
    
    '''<summary>
    '''Contrôle CadreFiger.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CadreFiger As Global.System.Web.UI.WebControls.Table
    
    '''<summary>
    '''Contrôle CommandeFiger.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CommandeFiger As Global.System.Web.UI.WebControls.Button
    
    '''<summary>
    '''Contrôle CadreFiltre.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CadreFiltre As Global.System.Web.UI.WebControls.Table
    
    '''<summary>
    '''Contrôle ComboSelCritere.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents ComboSelCritere As Global.Virtualia.Net.Controles_VListeCombo
    
    '''<summary>
    '''Contrôle ComboSelValeur.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents ComboSelValeur As Global.Virtualia.Net.Controles_VListeCombo
    
    '''<summary>
    '''Contrôle EtiRechercheNom.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents EtiRechercheNom As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle DonNom.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents DonNom As Global.System.Web.UI.WebControls.TextBox
    
    '''<summary>
    '''Contrôle CadreActivite.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CadreActivite As Global.System.Web.UI.WebControls.Table
    
    '''<summary>
    '''Contrôle MultiVues.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents MultiVues As Global.System.Web.UI.WebControls.MultiView
    
    '''<summary>
    '''Contrôle VueBase100.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents VueBase100 As Global.System.Web.UI.WebControls.View
    
    '''<summary>
    '''Contrôle ListeBase100.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents ListeBase100 As Global.Virtualia.Net.Controles_VListeGrid
    
    '''<summary>
    '''Contrôle VueModulation.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents VueModulation As Global.System.Web.UI.WebControls.View
    
    '''<summary>
    '''Contrôle ListeModulation.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents ListeModulation As Global.Virtualia.Net.Controles_VListeGrid
    
    '''<summary>
    '''Contrôle VueProrata.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents VueProrata As Global.System.Web.UI.WebControls.View
    
    '''<summary>
    '''Contrôle ListeProrata.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents ListeProrata As Global.Virtualia.Net.Controles_VListeGrid
    
    '''<summary>
    '''Contrôle VueAffectation.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents VueAffectation As Global.System.Web.UI.WebControls.View
    
    '''<summary>
    '''Contrôle ListeAffectation.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents ListeAffectation As Global.Virtualia.Net.Controles_VListeGrid
    
    '''<summary>
    '''Contrôle VueEnveloppe.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents VueEnveloppe As Global.System.Web.UI.WebControls.View
    
    '''<summary>
    '''Contrôle ListeEnveloppe.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents ListeEnveloppe As Global.Virtualia.Net.Controles_VListeGrid
    
    '''<summary>
    '''Contrôle VuePaye.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents VuePaye As Global.System.Web.UI.WebControls.View
    
    '''<summary>
    '''Contrôle ListePaye.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents ListePaye As Global.Virtualia.Net.Controles_VListeGrid
    
    '''<summary>
    '''Contrôle VuePartielle.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents VuePartielle As Global.System.Web.UI.WebControls.View
    
    '''<summary>
    '''Contrôle SaisieLigne.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents SaisieLigne As Global.Virtualia.Net.CtlSaisiePartielle
    
    '''<summary>
    '''Contrôle VueAttente.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents VueAttente As Global.System.Web.UI.WebControls.View
    
    '''<summary>
    '''Contrôle CadreAttente.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CadreAttente As Global.System.Web.UI.WebControls.Table
    
    '''<summary>
    '''Contrôle EtiAttente.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents EtiAttente As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle UpdateAttente.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents UpdateAttente As Global.System.Web.UI.UpdateProgress
    
    '''<summary>
    '''Contrôle CellMessage.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CellMessage As Global.System.Web.UI.WebControls.TableCell
    
    '''<summary>
    '''Contrôle PopupMsg.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents PopupMsg As Global.AjaxControlToolkit.ModalPopupExtender
    
    '''<summary>
    '''Contrôle PanelMsgPopup.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents PanelMsgPopup As Global.System.Web.UI.WebControls.Panel
    
    '''<summary>
    '''Contrôle MsgVirtualia.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents MsgVirtualia As Global.Virtualia.Net.Controles_VMessage
    
    '''<summary>
    '''Contrôle HPopupMsg.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents HPopupMsg As Global.System.Web.UI.WebControls.HiddenField
    
    '''<summary>
    '''Propriété Master.
    '''</summary>
    '''<remarks>
    '''Propriété générée automatiquement.
    '''</remarks>
    Public Shadows ReadOnly Property Master() As Virtualia.Net.VirtualiaMain
        Get
            Return CType(MyBase.Master,Virtualia.Net.VirtualiaMain)
        End Get
    End Property
End Class
