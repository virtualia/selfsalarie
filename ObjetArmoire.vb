﻿Option Strict On
Option Explicit On
Option Compare Text
Imports VI = Virtualia.Systeme.Constantes
Namespace Datas
    Public Class ObjetArmoire
        Implements IDisposable
        Private disposedValue As Boolean = False        ' Pour détecter les appels redondants
        Private WsPointdeVue As Integer
        Private WsSiArmoireStandard As Boolean = True
        Private WsNomArmoire As String
        Private WsEnsembleDossier As List(Of Virtualia.Ressources.Datas.ObjetDossierPER)
        Private WsIndexCourant As Integer = -1
        '
        Private WsObjetIcone As Integer
        Private WsInfoIcone As Integer
        Private WsSiDateIcone As Boolean
        '
        Private WsAppObjetGlobal As Virtualia.Net.Session.ObjetGlobal
        Private WsTypeArmoire As Integer

        Public ReadOnly Property TabIdentifiants() As Integer()
            Get
                If WsEnsembleDossier Is Nothing Then
                    Return Nothing
                End If
                If WsEnsembleDossier.Count = 0 Then
                    Return Nothing
                End If
                Dim TabIde As Integer()
                TabIde = (From instance In WsEnsembleDossier Select instance.V_Identifiant _
                          Order By V_Identifiant Ascending).ToArray
                Return TabIde
            End Get
        End Property

        Public ReadOnly Property ListeIdentifiants() As List(Of Integer)
            Get
                If WsEnsembleDossier Is Nothing Then
                    Return Nothing
                End If
                If WsEnsembleDossier.Count = 0 Then
                    Return Nothing
                End If
                Dim TabIde As List(Of Integer)
                TabIde = (From instance In WsEnsembleDossier Select instance.V_Identifiant _
                          Order By V_Identifiant Ascending).ToList
                Return TabIde
            End Get
        End Property

        Public Property NomdelArmoire() As String
            Get
                Return WsNomArmoire
            End Get
            Set(ByVal value As String)
                WsNomArmoire = value
            End Set
        End Property

        Public Property SiArmoireStandard() As Boolean
            Get
                Return WsSiArmoireStandard
            End Get
            Set(ByVal value As Boolean)
                WsSiArmoireStandard = value
            End Set
        End Property

        Public Property TypeArmoire() As Integer
            Get
                Return WsTypeArmoire
            End Get
            Set(ByVal value As Integer)
                WsTypeArmoire = value
            End Set
        End Property

        Public Property EnsembleDossiers() As List(Of Virtualia.Ressources.Datas.ObjetDossierPER)
            Get
                If WsEnsembleDossier Is Nothing Then
                    If TypeArmoire > 999 Then
                        Return NouvelEnsemble
                    End If
                End If
                Return WsEnsembleDossier
            End Get
            Set(ByVal value As List(Of Virtualia.Ressources.Datas.ObjetDossierPER))
                WsEnsembleDossier = value
            End Set
        End Property

        Public ReadOnly Property NombredeDossiers() As Integer
            Get
                If WsEnsembleDossier IsNot Nothing Then
                    Return WsEnsembleDossier.Count
                Else
                    Return 0
                End If
            End Get
        End Property

        Public ReadOnly Property NouvelEnsemble() As List(Of Virtualia.Ressources.Datas.ObjetDossierPER)
            Get
                If WsEnsembleDossier IsNot Nothing Then
                    WsEnsembleDossier.Clear()
                    WsEnsembleDossier = Nothing
                End If
                WsEnsembleDossier = New List(Of Virtualia.Ressources.Datas.ObjetDossierPER)
                Return WsEnsembleDossier
            End Get
        End Property

        Public WriteOnly Property InfoIconeArmoire(ByVal NoObjet As Integer, ByVal SiDate As Boolean) As Integer
            Set(ByVal value As Integer)
                WsObjetIcone = NoObjet
                WsSiDateIcone = SiDate
                WsInfoIcone = value
            End Set
        End Property

        Public ReadOnly Property SelectionDynamique(ByVal NoArmoire As Integer, ByVal NoObjet As Integer, ByVal NoInfo As Integer, ByVal OpeLiaison As Integer, ByVal OpeComparaison As Integer, ByVal ConditionValeurs As String, ByVal DateDebut As String, ByVal DateFin As String) As Integer
            Get
                Dim Constructeur As Virtualia.Systeme.Sgbd.Sql.SqlInterne
                Dim LstRes As List(Of String)
                Dim OrdreSql As String
                Dim TableauData(0) As String

                Constructeur = New Virtualia.Systeme.Sgbd.Sql.SqlInterne(WsAppObjetGlobal.VirModele, WsAppObjetGlobal.VirInstanceBd)
                Constructeur.NombredeRequetes(WsPointdeVue, DateDebut, DateFin, VI.Operateurs.ET) = 1
                Constructeur.SiPasdeTriSurIdeDossier = False
                Constructeur.NoInfoSelection(0, NoObjet) = NoInfo
                Select Case ConditionValeurs
                    Case Is <> ""
                        Constructeur.ValeuraComparer(0, OpeLiaison, OpeComparaison, False) = ConditionValeurs
                End Select
                Constructeur.InfoExtraite(0, NoObjet, 0) = NoInfo
                OrdreSql = Constructeur.OrdreSqlDynamique
                Constructeur = Nothing

                LstRes = WsAppObjetGlobal.VirServiceServeur.RequeteSql_ToListeChar(WsAppObjetGlobal.VirNomUtilisateur, WsPointdeVue, NoObjet, OrdreSql)

                Dim PerDossier As Virtualia.Ressources.Datas.ObjetDossierPER
                Dim PointeurClone As Virtualia.Ressources.Datas.ObjetDossierPER
                Dim ListeTout As List(Of Virtualia.Ressources.Datas.ObjetDossierPER)
                Dim ListeRes As New List(Of Virtualia.Ressources.Datas.ObjetDossierPER)
                Dim IdePer As Integer

                ListeTout = WsAppObjetGlobal.ItemArmoire(0).EnsembleDossiers

                For Each Resultat In LstRes
                    TableauData = Strings.Split(Resultat, VI.Tild, -1)
                    IdePer = CInt(TableauData(0))
                    PerDossier = ListeTout.Find(Function(Recherche) Recherche.V_Identifiant = IdePer)
                    If PerDossier IsNot Nothing Then
                        PerDossier.ChampSelectionne(NoArmoire) = TableauData(1)
                        PointeurClone = New Virtualia.Ressources.Datas.ObjetDossierPER(WsAppObjetGlobal.VirModele)
                        PointeurClone = PerDossier
                        ListeRes.Add(PointeurClone)
                    End If
                Next

                WsEnsembleDossier = New List(Of Virtualia.Ressources.Datas.ObjetDossierPER)
                WsEnsembleDossier = (From instance In ListeRes Select instance Where instance.V_Identifiant > 0 Order By instance.Nom Ascending, instance.Prenom Ascending).ToList
                Return WsEnsembleDossier.Count

            End Get
        End Property

        Public ReadOnly Property ArmoireComplete(ByVal DateFin As String) As Integer
            Get
                Dim Constructeur As Virtualia.Systeme.Sgbd.Sql.SqlInterne
                Dim LstRes As List(Of String)
                Dim OrdreSql As String
                Dim TableauData(0) As String

                Constructeur = New Virtualia.Systeme.Sgbd.Sql.SqlInterne(WsAppObjetGlobal.VirModele, WsAppObjetGlobal.VirInstanceBd)
                Constructeur.NombredeRequetes(WsPointdeVue, "", "", VI.Operateurs.ET) = 1
                Constructeur.SiPasdeTriSurIdeDossier = False
                Constructeur.SiHistoriquedeSituation = False

                Constructeur.NoInfoSelection(0, 1) = 2
                Constructeur.InfoExtraite(0, 1, 0) = 2
                Constructeur.InfoExtraite(1, 1, 0) = 3
                Constructeur.InfoExtraite(2, 1, 0) = 4

                OrdreSql = Constructeur.OrdreSqlDynamique

                Constructeur = Nothing
                LstRes = WsAppObjetGlobal.VirServiceServeur.RequeteSql_ToListeChar(WsAppObjetGlobal.VirNomUtilisateur, WsPointdeVue, 1, OrdreSql)

                Dim PerDossier As Virtualia.Ressources.Datas.ObjetDossierPER
                Dim LstObjet1 As New List(Of Virtualia.Ressources.Datas.ObjetDossierPER)

                For Each Resultat In LstRes
                    TableauData = Strings.Split(Resultat, VI.Tild, -1)
                    PerDossier = New Virtualia.Ressources.Datas.ObjetDossierPER(WsAppObjetGlobal.VirModele, CInt(TableauData(0)))
                    PerDossier.Nom = TableauData(1)
                    PerDossier.Prenom = TableauData(2)
                    PerDossier.Date_de_Naissance = TableauData(3)
                    LstObjet1.Add(PerDossier)
                Next

                WsEnsembleDossier = New List(Of Virtualia.Ressources.Datas.ObjetDossierPER)
                WsEnsembleDossier = (From instance In LstObjet1 Select instance Where instance.V_Identifiant > 0 Order By instance.Nom Ascending, instance.Prenom Ascending).ToList
                Return WsEnsembleDossier.Count
            End Get
        End Property

        Public ReadOnly Property NouveauDossier(ByVal Nom As String, ByVal Prenom As String, ByVal DateNai As String) As Virtualia.Ressources.Datas.ObjetDossierPER
            Get
                Dim PerDossier As Virtualia.Ressources.Datas.ObjetDossierPER
                Dim Fiche As TablesObjet.ShemaPER.PER_ETATCIVIL
                Dim Ide As Integer
                Dim ListeTout As List(Of Virtualia.Ressources.Datas.ObjetDossierPER)

                Try
                    ListeTout = WsAppObjetGlobal.ItemArmoire(0).EnsembleDossiers
                    If ListeTout Is Nothing Then
                        ListeTout = New List(Of Virtualia.Ressources.Datas.ObjetDossierPER)
                    End If
                    Ide = WsAppObjetGlobal.NouvelIdentifiant("PER_ETATCIVIL")
                    PerDossier = New Virtualia.Ressources.Datas.ObjetDossierPER(WsAppObjetGlobal.VirModele, Ide)
                    PerDossier.Nom = Nom
                    PerDossier.Prenom = Prenom
                    PerDossier.Date_de_Naissance = DateNai

                    Fiche = CType(WsAppObjetGlobal.VirRhShemaPER.V_NouvelleFiche(1, PerDossier.V_Identifiant), TablesObjet.ShemaPER.PER_ETATCIVIL)
                    PerDossier.V_ListeDesFiches.Add(Fiche)
                    Fiche.Nom = Nom
                    Fiche.Prenom = Prenom
                    Fiche.Date_de_naissance = DateNai

                    ListeTout.Add(PerDossier)

                    Return PerDossier
                Catch ex As Exception
                    Return Nothing
                End Try
            End Get
        End Property

        Public Sub RetirerDossier(ByVal PerDossier As Virtualia.Ressources.Datas.ObjetDossierPER)
            Dim ListeTout As List(Of Virtualia.Ressources.Datas.ObjetDossierPER)

            ListeTout = WsAppObjetGlobal.ItemArmoire(0).EnsembleDossiers
            If ListeTout IsNot Nothing Then
                Try
                    ListeTout.Remove(PerDossier)
                Catch ex As Exception
                    Exit Try
                End Try
            End If
            If WsEnsembleDossier IsNot Nothing Then
                Try
                    WsEnsembleDossier.Remove(PerDossier)
                Catch ex As Exception
                    Exit Try
                End Try
            End If
        End Sub

        Public Sub New(ByVal Host As Virtualia.Net.Session.ObjetGlobal, ByVal PointdeVue As Integer)
            MyBase.New()
            WsAppObjetGlobal = Host
            WsPointdeVue = PointdeVue
        End Sub

        Public Overloads Sub Dispose() Implements IDisposable.Dispose
            Dispose(True)
            GC.SuppressFinalize(Me)
        End Sub

        Protected Overridable Overloads Sub Dispose(ByVal disposing As Boolean)
            If Not Me.disposedValue Then
                If disposing Then
                    WsEnsembleDossier.Clear()
                End If
            End If
            Me.disposedValue = True
        End Sub
    End Class
End Namespace
