﻿Option Strict On
Option Explicit On
Option Compare Text
Imports Virtualia.Systeme.Evenements
Imports VI = Virtualia.Systeme.Constantes
Public Class FrmPFRSynthese
    Inherits System.Web.UI.Page
    Private WebFct As Virtualia.Net.Controles.WebFonctions
    Private WsNomStateUrl As String = "PFRSynthese"

    Private Property V_UrlRetour As String
        Get
            Dim VCache As String

            VCache = CStr(Me.ViewState(WsNomStateUrl))
            If VCache Is Nothing Then
                Return ""
            End If
            Return VCache
        End Get
        Set(ByVal value As String)
            If Me.ViewState(WsNomStateUrl) IsNot Nothing Then
                Me.ViewState.Remove(WsNomStateUrl)
            End If
            Me.ViewState.Add(WsNomStateUrl, value)
        End Set
    End Property

    Private Sub CommandeRetour_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles CommandeRetour.Click
        Response.Redirect(V_UrlRetour & "?IDVirtualia=" & Server.HtmlEncode(Session.SessionID))
    End Sub

    Private Sub Page_Init(sender As Object, e As EventArgs) Handles Me.Init
        Dim Msg As String = ""
        Dim NumSession As String = Server.HtmlDecode(Request.QueryString("IDVirtualia"))
        If NumSession <> Session.SessionID Then
            Msg = "Erreur lors de l'identification."
            Response.Redirect("~/Fenetres/Commun/ErreurApplication.aspx?Msg=" & Msg)
            Exit Sub
        End If
        WebFct = New Virtualia.Net.Controles.WebFonctions(Me, 1)
        If WebFct.PointeurUtilisateur Is Nothing Then
            Msg = "Erreur de serveur. Vous avez été déconnecté(e)."
            Response.Redirect("~/Fenetres/Commun/ErreurApplication.aspx?Msg=" & Msg)
            Exit Sub
        End If
        If WebFct.VerifierCookie(Me, Session.SessionID) = False Then
            Msg = "Erreur lors de l'identification du dossier accédé. Session invalide."
            Response.Redirect("~/Fenetres/Commun/ErreurApplication.aspx?Msg=" & Msg)
            Exit Sub
        End If
        V_UrlRetour = Server.HtmlDecode(Request.QueryString("UrlRetour"))
    End Sub

    Private Sub FrmPFRSynthese_PreRender(sender As Object, e As EventArgs) Handles Me.PreRender
        Call FaireListeAnnee()
    End Sub

    Private Sub FaireListeAnnee()
        Dim Chaine As System.Text.StringBuilder
        Dim Libel As String
        Dim Annee As Integer = Year(Now)

        Libel = "Aucun" & VI.Tild & "Une possibilité" & VI.Tild & "possibilités"
        Chaine = New System.Text.StringBuilder
        Do
            Chaine.Append(Annee.ToString & VI.Tild)
            Annee -= 1
            If Annee < 2014 Then
                Exit Do
            End If
        Loop
        ComboSelAnnee.V_Liste(Libel) = Chaine.ToString
        If Session.Item("ANNEE") IsNot Nothing Then
            Annee = CInt(Session.Item("ANNEE"))
        Else
            Annee = Year(Now)
        End If
        ComboSelAnnee.LstText = Annee.ToString
        If Annee = Year(Now) Then
            CadreEDition.BackColor = WebFct.ConvertCouleur("#2D8781")
        Else
            CadreEDition.BackColor = Drawing.Color.Firebrick
        End If
    End Sub

    Private Sub CmdDotation_Click(sender As Object, e As EventArgs) Handles CmdDotation.Click
        Dim LstDotations As List(Of PFR.SelfV4.LigneDotation)
        Dim Rupture As String = ""
        Dim MontantDotation As Double
        Dim TotalDotation As Double
        Dim MontantBase100 As Double
        Dim MontantModulation As Double
        Dim MontantComplement As Double
        Dim TotalBase100 As Double
        Dim TotalModulation As Double
        Dim TotalComplement As Double
        Dim Colonnes As List(Of String)
        Dim Chaine As String
        Dim LstRes As List(Of String)
        Dim IndiceI As Integer
        Dim LstDecision As List(Of Virtualia.TablesObjet.ShemaREF.OUT_DECISION)

        If WebFct Is Nothing Then
            WebFct = New Virtualia.Net.Controles.WebFonctions(Me, 1)
        End If
        Colonnes = New List(Of String)

        Colonnes.Add("Régime indemnitaire")
        Colonnes.Add("Dotation budgétaire")
        Colonnes.Add("Montant en base 100")
        Colonnes.Add("Montant de la modulation")
        Colonnes.Add("Total des primes")
        Colonnes.Add("Solde sur dotation")
        Colonnes.Add("Mt Modulation complémentaire")
        Colonnes.Add("Clef")
        ListeSynthese.V_LibelColonne = Colonnes
        For IndiceI = 0 To 6
            Select Case IndiceI
                Case Is > 0
                    ListeSynthese.Centrage_Colonne(IndiceI) = 2
                Case Else
                    ListeSynthese.Centrage_Colonne(IndiceI) = 0
            End Select
        Next IndiceI

        LstDotations = ConstituerListeSynthese("Dotation")
        If LstDotations Is Nothing Then
            ListeSynthese.V_Liste = Nothing
            Exit Sub
        End If

        LstRes = New List(Of String)
        TotalDotation = 0
        TotalBase100 = 0
        TotalModulation = 0
        TotalComplement = 0
        MontantBase100 = 0
        MontantModulation = 0
        MontantComplement = 0
        LstDecision = CType(WebFct.PointeurGlobal, Virtualia.Net.Session.ObjetGlobal).ListeTablesDecision(33)
        For IndiceI = 0 To LstDotations.Count - 1
            If LstDotations.Item(IndiceI).Regime_Categorie <> Rupture AndAlso Rupture <> "" Then
                Chaine = Rupture & VI.Tild
                MontantDotation = 0
                For Each OutDecision As Virtualia.TablesObjet.ShemaREF.OUT_DECISION In LstDecision
                    If OutDecision.ValeurInterne(0) = Year(Now).ToString And OutDecision.ValeurInterne(1) = Rupture Then
                        MontantDotation = WebFct.ViRhFonction.ConversionDouble(OutDecision.CodeExterne(0))
                    End If
                Next
                TotalDotation += MontantDotation
                Chaine &= Strings.Format(MontantDotation, "# ### ### ##0") & VI.Tild
                Chaine &= Strings.Format(MontantBase100, "# ### ### ##0") & VI.Tild
                Chaine &= Strings.Format(MontantModulation, "# ### ### ##0") & VI.Tild
                'Total Primes
                Chaine &= Strings.Format(MontantBase100 + MontantModulation, "# ### ### ##0") & VI.Tild
                '** Solde
                Chaine &= Strings.Format(MontantDotation - MontantBase100 - MontantModulation, "# ### ### ##0") & VI.Tild
                '** Complément de la modulation
                Chaine &= Strings.Format(MontantComplement, "# ### ### ##0") & VI.Tild
                Chaine &= IndiceI.ToString
                LstRes.Add(Chaine)
                MontantBase100 = 0
                MontantModulation = 0
                MontantComplement = 0
            End If
            Rupture = LstDotations.Item(IndiceI).Regime_Categorie
            TotalBase100 += LstDotations.Item(IndiceI).Montant_Base100
            TotalModulation += LstDotations.Item(IndiceI).Montant_Modulation
            TotalComplement += LstDotations.Item(IndiceI).Montant_Complement_Modulation
            MontantBase100 += LstDotations.Item(IndiceI).Montant_Base100
            MontantModulation += LstDotations.Item(IndiceI).Montant_Modulation
            MontantComplement += LstDotations.Item(IndiceI).Montant_Complement_Modulation
        Next IndiceI
        '**** Dernière ligne
        Chaine = Rupture & VI.Tild
        MontantDotation = 0
        For Each OutDecision As Virtualia.TablesObjet.ShemaREF.OUT_DECISION In LstDecision
            If OutDecision.ValeurInterne(0) = Year(Now).ToString And OutDecision.ValeurInterne(1) = Rupture Then
                MontantDotation = WebFct.ViRhFonction.ConversionDouble(OutDecision.CodeExterne(0))
            End If
        Next
        TotalDotation += MontantDotation
        Chaine &= Strings.Format(MontantDotation, "# ### ### ##0") & VI.Tild
        Chaine &= Strings.Format(MontantBase100, "# ### ### ##0") & VI.Tild
        Chaine &= Strings.Format(MontantModulation, "# ### ### ##0") & VI.Tild
        Chaine &= Strings.Format(MontantBase100 + MontantModulation, "# ### ### ##0") & VI.Tild
        Chaine &= Strings.Format(MontantDotation - MontantBase100 - MontantModulation, "# ### ### ##0") & VI.Tild
        Chaine &= Strings.Format(MontantComplement, "# ### ### ##0") & VI.Tild
        Chaine &= IndiceI.ToString
        LstRes.Add(Chaine)
        '*** Total ***
        LstRes.Add(StrDup(6, VI.Tild))
        Chaine = "TOTAL Régimes indemnitaires" & VI.Tild
        Chaine &= Strings.Format(TotalDotation, "# ### ### ##0") & VI.Tild
        Chaine &= Strings.Format(TotalBase100, "# ### ### ##0") & VI.Tild
        Chaine &= Strings.Format(TotalModulation, "# ### ### ##0") & VI.Tild
        Chaine &= Strings.Format(TotalBase100 + TotalModulation, "# ### ### ##0") & VI.Tild
        Chaine &= Strings.Format(TotalDotation - TotalBase100 - TotalModulation, "# ### ### ##0") & VI.Tild
        Chaine &= Strings.Format(TotalComplement, "# ### ### ##0") & VI.Tild
        Chaine &= IndiceI.ToString
        ListeSynthese.V_Liste = LstRes
    End Sub

    Private Sub CmdEnveloppe_Click(sender As Object, e As EventArgs) Handles CmdEnveloppe.Click
        Dim LstDotations As List(Of PFR.SelfV4.LigneDotation)
        Dim RuptureRegime As String = ""
        Dim RuptureService As String = ""
        Dim MontantBase100 As Double
        Dim MontantEnveloppe As Double
        Dim NbAgents As Integer
        Dim MontantModulation As Double
        Dim CoeffModulation As Double
        Dim TotalBase100 As Double
        Dim TotalEnveloppe As Double
        Dim TotalAgents As Integer
        Dim TotalModulation As Double
        Dim LibColonnes As List(Of String)
        Dim LstRes As List(Of String)
        Dim Chaine As String
        Dim IndiceI As Integer

        If WebFct Is Nothing Then
            WebFct = New Virtualia.Net.Controles.WebFonctions(Me, 1)
        End If
        LibColonnes = New List(Of String)
        LibColonnes.Add("Direction")
        LibColonnes.Add("Montant Base 100")
        LibColonnes.Add("Enveloppe modulation")
        LibColonnes.Add("Nombre agents")
        LibColonnes.Add("Modulation demandée")
        LibColonnes.Add("Ecarts constatés")
        LibColonnes.Add("Modulation moyenne")
        LibColonnes.Add("Clef")
        ListeSynthese.V_LibelColonne = LibColonnes
        For IndiceI = 0 To 8
            Select Case IndiceI
                Case Is > 0
                    ListeSynthese.Centrage_Colonne(IndiceI) = 2
                Case Else
                    ListeSynthese.Centrage_Colonne(IndiceI) = 0
            End Select
        Next IndiceI

        LstDotations = ConstituerListeSynthese("Enveloppe")
        If LstDotations Is Nothing Then
            ListeSynthese.V_Liste = Nothing
            Exit Sub
        End If

        LstRes = New List(Of String)
        TotalBase100 = 0
        TotalEnveloppe = 0
        TotalAgents = 0
        TotalModulation = 0
        MontantBase100 = 0
        MontantEnveloppe = 0
        NbAgents = 0
        MontantModulation = 0
        CoeffModulation = 0

        RuptureRegime = LstDotations.Item(0).Regime_Categorie
        RuptureService = LstDotations.Item(0).Rupture_Modulation
        LstRes.Add(RuptureRegime & VI.Tild & StrDup(6, VI.Tild))
        For IndiceI = 0 To LstDotations.Count - 1
            If LstDotations.Item(IndiceI).Rupture_Modulation <> RuptureService Then
                Chaine = LstDotations.Item(IndiceI - 1).Direction_Modulation & VI.Tild
                Chaine &= Strings.Format(MontantBase100, "# ### ### ##0") & VI.Tild
                Chaine &= Strings.Format(MontantEnveloppe, "# ### ### ##0") & VI.Tild
                Chaine &= Strings.Format(NbAgents, "# ##0") & VI.Tild
                Chaine &= Strings.Format(MontantModulation, "# ### ### ##0") & VI.Tild
                'Ecarts
                Chaine &= Strings.Format(MontantModulation - MontantEnveloppe, "# ### ### ##0") & VI.Tild
                'Modulation moyenne
                Chaine &= Strings.Format((CoeffModulation / NbAgents), "# ##0.00") & VI.Tild
                Chaine &= IndiceI.ToString
                LstRes.Add(Chaine)

                MontantBase100 = 0
                MontantEnveloppe = 0
                NbAgents = 0
                MontantModulation = 0
                CoeffModulation = 0
            End If
            If LstDotations.Item(IndiceI).Regime_Categorie <> RuptureRegime Then
                If LstDotations.Item(IndiceI).Rupture_Modulation = RuptureService Then
                    '**** Dernière ligne
                    Chaine = LstDotations.Item(IndiceI - 1).Direction_Modulation & VI.Tild
                    Chaine &= Strings.Format(MontantBase100, "# ### ### ##0") & VI.Tild
                    Chaine &= Strings.Format(MontantEnveloppe, "# ### ### ##0") & VI.Tild
                    Chaine &= Strings.Format(NbAgents, "# ##0") & VI.Tild
                    Chaine &= Strings.Format(MontantModulation, "# ### ### ##0") & VI.Tild
                    Chaine &= Strings.Format(MontantModulation - MontantEnveloppe, "# ### ### ##0") & VI.Tild
                    Chaine &= Strings.Format((CoeffModulation / NbAgents), "# ##0.00") & VI.Tild
                    Chaine &= IndiceI.ToString
                    LstRes.Add(Chaine)

                    MontantBase100 = 0
                    MontantEnveloppe = 0
                    NbAgents = 0
                    MontantModulation = 0
                    CoeffModulation = 0
                End If
                LstRes.Add(StrDup(7, VI.Tild))
                Chaine = "TOTAL " & RuptureRegime & VI.Tild
                Chaine &= Strings.Format(TotalBase100, "# ### ### ##0") & VI.Tild
                Chaine &= Strings.Format(TotalEnveloppe, "# ### ### ##0") & VI.Tild
                Chaine &= Strings.Format(TotalAgents, "# ##0") & VI.Tild
                Chaine &= Strings.Format(TotalModulation, "# ### ### ##0") & VI.Tild
                'Ecarts
                Chaine &= Strings.Format(TotalModulation - TotalEnveloppe, "# ### ### ##0") & VI.Tild
                Chaine &= VI.Tild
                Chaine &= IndiceI.ToString
                LstRes.Add(Chaine)
                LstRes.Add(StrDup(7, VI.Tild))
                LstRes.Add(LstDotations.Item(IndiceI).Regime_Categorie & VI.Tild & StrDup(6, VI.Tild))

                TotalBase100 = 0
                TotalEnveloppe = 0
                TotalAgents = 0
                TotalModulation = 0
            End If
            RuptureRegime = LstDotations.Item(IndiceI).Regime_Categorie
            RuptureService = LstDotations.Item(IndiceI).Rupture_Modulation

            TotalBase100 += LstDotations.Item(IndiceI).Montant_Base100
            TotalEnveloppe += LstDotations.Item(IndiceI).Enveloppe_Modulation
            TotalAgents += 1
            TotalModulation += LstDotations.Item(IndiceI).Montant_Modulation

            MontantBase100 += LstDotations.Item(IndiceI).Montant_Base100
            MontantEnveloppe += LstDotations.Item(IndiceI).Enveloppe_Modulation
            NbAgents += 1
            MontantModulation += LstDotations.Item(IndiceI).Montant_Modulation
            CoeffModulation += LstDotations.Item(IndiceI).Coefficient_Modulation

        Next IndiceI
        '**** Dernière ligne
        Chaine = LstDotations.Item(IndiceI - 1).Direction_Modulation & VI.Tild
        Chaine &= Strings.Format(MontantBase100, "# ### ### ##0") & VI.Tild
        Chaine &= Strings.Format(MontantEnveloppe, "# ### ### ##0") & VI.Tild
        Chaine &= Strings.Format(NbAgents, "# ##0") & VI.Tild
        Chaine &= Strings.Format(MontantModulation, "# ### ### ##0") & VI.Tild
        Chaine &= Strings.Format(MontantModulation - MontantEnveloppe, "# ### ### ##0") & VI.Tild
        Chaine &= Strings.Format((CoeffModulation / NbAgents), "# ##0.00") & VI.Tild
        Chaine &= IndiceI.ToString
        LstRes.Add(Chaine)

        '**** Dernier Total
        LstRes.Add(StrDup(7, VI.Tild))
        Chaine = "TOTAL " & RuptureRegime & VI.Tild
        Chaine &= Strings.Format(TotalBase100, "# ### ### ##0") & VI.Tild
        Chaine &= Strings.Format(TotalEnveloppe, "# ### ### ##0") & VI.Tild
        Chaine &= Strings.Format(TotalAgents, "# ##0") & VI.Tild
        Chaine &= Strings.Format(TotalModulation, "# ### ### ##0") & VI.Tild
        Chaine &= Strings.Format(TotalModulation - TotalEnveloppe, "# ### ### ##0") & VI.Tild
        Chaine &= VI.Tild
        Chaine &= IndiceI.ToString
        LstRes.Add(Chaine)
        LstRes.Add(StrDup(7, VI.Tild))

        '** Total Global par Direction
        Dim LstEnveloppes As List(Of PFR.SelfV4.LigneDotation)
        LstEnveloppes = (From instance In LstDotations Order By instance.SiegeOuRegion, instance.Direction, instance.Service).ToList
        TotalBase100 = 0
        TotalEnveloppe = 0
        TotalAgents = 0
        TotalModulation = 0
        MontantBase100 = 0
        MontantEnveloppe = 0
        NbAgents = 0
        MontantModulation = 0
        CoeffModulation = 0

        RuptureService = LstEnveloppes.Item(0).Rupture_Modulation
        LstRes.Add("TOTAL GLOBALISE" & VI.Tild & StrDup(6, VI.Tild))
        LstRes.Add(StrDup(7, VI.Tild))
        For IndiceI = 0 To LstEnveloppes.Count - 1
            If LstEnveloppes.Item(IndiceI).Rupture_Modulation <> RuptureService Then
                Chaine = LstEnveloppes.Item(IndiceI - 1).Direction_Modulation & VI.Tild
                Chaine &= Strings.Format(MontantBase100, "# ### ### ##0") & VI.Tild
                Chaine &= Strings.Format(MontantEnveloppe, "# ### ### ##0") & VI.Tild
                Chaine &= Strings.Format(NbAgents, "# ##0") & VI.Tild
                Chaine &= Strings.Format(MontantModulation, "# ### ### ##0") & VI.Tild
                Chaine &= Strings.Format(MontantModulation - MontantEnveloppe, "# ### ### ##0") & VI.Tild
                Chaine &= Strings.Format((CoeffModulation / NbAgents), "# ##0.00") & VI.Tild
                Chaine &= IndiceI.ToString
                LstRes.Add(Chaine)

                MontantBase100 = 0
                MontantEnveloppe = 0
                NbAgents = 0
                MontantModulation = 0
                CoeffModulation = 0
            End If
            RuptureService = LstEnveloppes.Item(IndiceI).Rupture_Modulation

            TotalBase100 += LstEnveloppes.Item(IndiceI).Montant_Base100
            TotalEnveloppe += LstEnveloppes.Item(IndiceI).Enveloppe_Modulation
            TotalAgents += 1
            TotalModulation += LstEnveloppes.Item(IndiceI).Montant_Modulation

            MontantBase100 += LstEnveloppes.Item(IndiceI).Montant_Base100
            MontantEnveloppe += LstEnveloppes.Item(IndiceI).Enveloppe_Modulation
            NbAgents += 1
            MontantModulation += LstEnveloppes.Item(IndiceI).Montant_Modulation
            CoeffModulation += LstEnveloppes.Item(IndiceI).Coefficient_Modulation

        Next IndiceI
        '**** Dernière ligne
        Chaine = LstEnveloppes.Item(IndiceI - 1).Direction_Modulation & VI.Tild
        Chaine &= Strings.Format(MontantBase100, "# ### ### ##0") & VI.Tild
        Chaine &= Strings.Format(MontantEnveloppe, "# ### ### ##0") & VI.Tild
        Chaine &= Strings.Format(NbAgents, "# ##0") & VI.Tild
        Chaine &= Strings.Format(MontantModulation, "# ### ### ##0") & VI.Tild
        Chaine &= Strings.Format(MontantModulation - MontantEnveloppe, "# ### ### ##0") & VI.Tild
        Chaine &= Strings.Format((CoeffModulation / NbAgents), "# ##0.00") & VI.Tild
        Chaine &= IndiceI.ToString
        LstRes.Add(Chaine)
        '**** Dernier Total
        LstRes.Add(StrDup(7, VI.Tild))
        Chaine = "TOTAL GENERAL" & VI.Tild
        Chaine &= Strings.Format(TotalBase100, "# ### ### ##0") & VI.Tild
        Chaine &= Strings.Format(TotalEnveloppe, "# ### ### ##0") & VI.Tild
        Chaine &= Strings.Format(TotalAgents, "# ##0") & VI.Tild
        Chaine &= Strings.Format(TotalModulation, "# ### ### ##0") & VI.Tild
        Chaine &= Strings.Format(TotalModulation - TotalEnveloppe, "# ### ### ##0") & VI.Tild
        Chaine &= VI.Tild
        Chaine &= IndiceI.ToString
        LstRes.Add(Chaine)
        LstRes.Add(StrDup(7, VI.Tild))

        ListeSynthese.V_Liste = LstRes
    End Sub

    Private Function ConstituerListeSynthese(ByVal TypeSynthese As String) As List(Of PFR.SelfV4.LigneDotation)
        Dim LstVues As List(Of Virtualia.TablesObjet.ShemaVUE.VUE_ATTRIBUTAIRE_PFR)
        Dim LstLignes As List(Of PFR.SelfV4.LigneDotation)
        Dim Ligne As PFR.SelfV4.LigneDotation
        Dim Annee As Integer = Year(Now)

        If Session.Item("ANNEE") IsNot Nothing Then
            Annee = CInt(Session.Item("ANNEE"))
        End If
        LstLignes = New List(Of PFR.SelfV4.LigneDotation)
        LstVues = CType(WebFct.PointeurUtilisateur.V_PointeurSessionModule, Virtualia.Net.Session.SessionModule).ListeVues_PFR(Annee, "PFR", "")
        If LstVues IsNot Nothing Then
            For Each VUE As Virtualia.TablesObjet.ShemaVUE.VUE_ATTRIBUTAIRE_PFR In LstVues
                Ligne = New PFR.SelfV4.LigneDotation
                Ligne.Regime = VUE.Regime_Prime
                Ligne.Categorie = VUE.Categorie
                Select Case TypeSynthese
                    Case "Dotation"
                        Ligne.SiegeOuRegion = VUE.Type_Affectation
                    Case "Enveloppe"
                        Ligne.SiegeOuRegion = AffectationEnveloppe(VUE)
                End Select
                Ligne.Direction = VUE.Structure_N1
                Ligne.Service = VUE.Structure_N2
                Ligne.Montant_Base100 = VUE.Montant_Total_Base100_Hors_IS_et_CIT
                Ligne.Montant_Modulation = VUE.Montant_Modulation_Apres_Prorata
                Ligne.Montant_Complement_Modulation = VUE.Montant_Modulation_Exception_Apres_Prorata
                Ligne.Enveloppe_Modulation = VUE.Enveloppe_PFR_Modulation
                Ligne.Coefficient_Modulation = VUE.Coefficient_Modulation_R
                LstLignes.Add(Ligne)
            Next
        End If
        LstVues = CType(WebFct.PointeurUtilisateur.V_PointeurSessionModule, Virtualia.Net.Session.SessionModule).ListeVues_PFR(Annee, "PS", "")
        If LstVues IsNot Nothing Then
            For Each VUE As Virtualia.TablesObjet.ShemaVUE.VUE_ATTRIBUTAIRE_PFR In LstVues
                Ligne = New PFR.SelfV4.LigneDotation
                Ligne.Regime = VUE.Regime_Prime
                Ligne.Categorie = VUE.Categorie
                Select Case TypeSynthese
                    Case "Dotation"
                        Ligne.SiegeOuRegion = VUE.Type_Affectation
                    Case "Enveloppe"
                        Ligne.SiegeOuRegion = AffectationEnveloppe(VUE)
                End Select
                Ligne.Direction = VUE.Structure_N1
                Ligne.Service = VUE.Structure_N2
                Ligne.Montant_Base100 = VUE.Montant_PS_Base100_Proratise
                Ligne.Montant_Modulation = VUE.Montant_Modulation_PS_Proratise
                Ligne.Montant_Complement_Modulation = VUE.Montant_PS_Complement
                Ligne.Enveloppe_Modulation = VUE.Enveloppe_PS_Modulation
                Ligne.Coefficient_Modulation = VUE.Coefficient_Modulation_R
                LstLignes.Add(Ligne)
            Next
        End If
        LstVues = CType(WebFct.PointeurUtilisateur.V_PointeurSessionModule, Virtualia.Net.Session.SessionModule).ListeVues_PFR(Annee, "F_R", "")
        If LstVues IsNot Nothing Then
            For Each VUE As Virtualia.TablesObjet.ShemaVUE.VUE_ATTRIBUTAIRE_PFR In LstVues
                Ligne = New PFR.SelfV4.LigneDotation
                Ligne.Regime = VUE.Regime_Prime
                Ligne.Categorie = VUE.Categorie
                Select Case TypeSynthese
                    Case "Dotation"
                        Ligne.SiegeOuRegion = VUE.Type_Affectation
                    Case "Enveloppe"
                        Ligne.SiegeOuRegion = AffectationEnveloppe(VUE)
                End Select
                Ligne.Direction = VUE.Structure_N1
                Ligne.Service = VUE.Structure_N2
                Ligne.Montant_Base100 = VUE.Montant_Total_Base100_Hors_IS_et_CIT
                Ligne.Montant_Modulation = VUE.Montant_Modulation_Apres_Prorata
                Ligne.Montant_Complement_Modulation = VUE.Montant_Modulation_Exception_Apres_Prorata
                Ligne.Enveloppe_Modulation = VUE.Enveloppe_PFR_Modulation
                Ligne.Coefficient_Modulation = VUE.Coefficient_Modulation_R
                LstLignes.Add(Ligne)
            Next
        End If
        If LstLignes.Count = 0 Then
            Return Nothing
        End If
        Select Case TypeSynthese
            Case "Dotation"
                Return (From instance In LstLignes Order By instance.Regime_Categorie).ToList
            Case "Enveloppe"
                Return (From instance In LstLignes Order By instance.Regime_Categorie, instance.SiegeOuRegion, instance.Direction, instance.Service).ToList
            Case Else
                Return Nothing
        End Select
    End Function

    Private ReadOnly Property AffectationEnveloppe(ByVal VueModulation As Virtualia.TablesObjet.ShemaVUE.VUE_ATTRIBUTAIRE_PFR) As String
        Get
            If VueModulation.Structure_N1.StartsWith("FAM") Then
                Return "Région"
            End If
            Return "Siège"
        End Get
    End Property

    Private Sub CommandeCsv_Click(sender As Object, e As System.EventArgs) Handles CommandeCsv.Click
        Dim UtiSession As Virtualia.Net.Session.SessionModule = CType(WebFct.PointeurUtilisateur.V_PointeurSessionModule, Virtualia.Net.Session.SessionModule)
        Dim NomFichier As String = ""
        If UtiSession Is Nothing Then
            Exit Sub
        End If
        If UtiSession.VParent.V_NomdeConnexion <> "" Then
            NomFichier = WebFct.PointeurGlobal.VirRepertoireTemporaire & "\" & UtiSession.VParent.V_NomdeConnexion & ".csv"
        ElseIf UtiSession.DossierPER IsNot Nothing Then
            NomFichier = WebFct.PointeurGlobal.VirRepertoireTemporaire & "\" & UtiSession.DossierPER.Nom & ".csv"
        Else
            Exit Sub
        End If
        Dim FluxTeleChargement As Byte()

        Call EcrireFichierCsv(NomFichier)
        If My.Computer.FileSystem.FileExists(NomFichier) = False Then
            Exit Sub
        End If
        FluxTeleChargement = My.Computer.FileSystem.ReadAllBytes(NomFichier)
        If FluxTeleChargement IsNot Nothing Then
            Dim response As System.Web.HttpResponse = System.Web.HttpContext.Current.Response
            response.Clear()
            response.AddHeader("Content-Type", "binary/octet-stream")
            response.AddHeader("Content-Disposition", "attachment; filename=" & NomFichier & "; size=" & FluxTeleChargement.Length.ToString())
            response.Flush()
            response.BinaryWrite(FluxTeleChargement)
            response.Flush()
            response.End()
        End If
    End Sub

    Private Sub EcrireFichierCsv(ByVal Nom_Fichier As String)
        Dim FicStream As System.IO.FileStream
        Dim FicWriter As System.IO.StreamWriter
        Dim SysCodeIso As System.Text.Encoding = System.Text.Encoding.GetEncoding(1252)
        Dim TableauLignes As List(Of String)
        Dim TableauColonnes As List(Of String)
        Dim TableauData(0) As String
        Dim IndiceK As Integer
        Dim PosClef As Integer
        Dim Chaine As System.Text.StringBuilder

        If WebFct Is Nothing Then
            WebFct = New Virtualia.Net.Controles.WebFonctions(Me, 1)
        End If
        TableauLignes = ListeSynthese.V_Liste
        If TableauLignes Is Nothing OrElse TableauLignes.Count = 0 Then
            Exit Sub
        End If
        FicStream = New System.IO.FileStream(Nom_Fichier, IO.FileMode.Create, IO.FileAccess.Write)
        FicWriter = New System.IO.StreamWriter(FicStream, SysCodeIso)
        TableauColonnes = ListeSynthese.V_LibelColonne
        For IndiceK = 0 To TableauColonnes.Count - 1
            If TableauColonnes(IndiceK) = "Clef" Then
                PosClef = IndiceK
                Exit For
            End If
        Next
        Chaine = New System.Text.StringBuilder
        For Each ligne As String In TableauLignes
            TableauData = Strings.Split(ligne, VI.Tild, -1)
            If TableauData.Count >= PosClef Then
                For IndiceK = 0 To PosClef - 1
                    Chaine.Append(TableauData(IndiceK) & VI.PointVirgule)
                Next
                FicWriter.WriteLine(Chaine.ToString)
                Chaine.Clear()
            Else
                FicWriter.WriteLine("")
            End If
        Next
        FicWriter.Flush()
        FicWriter.Close()
    End Sub

    Private Sub ComboSelAnnee_ValeurChange(sender As Object, e As DonneeChangeEventArgs) Handles ComboSelAnnee.ValeurChange
        If Session.Item("ANNEE") IsNot Nothing Then
            Session.Item("ANNEE") = e.Valeur
        Else
            Session.Add("ANNEE", e.Valeur)
        End If
        ListeSynthese.V_Liste = Nothing
    End Sub

End Class