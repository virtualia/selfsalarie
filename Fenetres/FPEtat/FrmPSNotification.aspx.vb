﻿Option Strict On
Option Explicit On
Option Compare Text
Imports VI = Virtualia.Systeme.Constantes
Public Class FrmPSNotification
    Inherits System.Web.UI.Page
    Private WebFct As Virtualia.Net.Controles.WebFonctions
    Private WsNomStateUrl As String = "PFRNotification"

    Private Property V_UrlRetour As String
        Get
            Dim VCache As String

            VCache = CStr(Me.ViewState(WsNomStateUrl))
            If VCache Is Nothing Then
                Return ""
            End If
            Return VCache
        End Get
        Set(ByVal value As String)
            If Me.ViewState(WsNomStateUrl) IsNot Nothing Then
                Me.ViewState.Remove(WsNomStateUrl)
            End If
            Me.ViewState.Add(WsNomStateUrl, value)
        End Set
    End Property

    Private Sub Page_Init(sender As Object, e As EventArgs) Handles Me.Init
        Dim Msg As String = ""
        Dim NumSession As String = Server.HtmlDecode(Request.QueryString("IDVirtualia"))
        If NumSession <> Session.SessionID Then
            Msg = "Erreur lors de l'identification."
            Response.Redirect("~/Fenetres/Commun/ErreurApplication.aspx?Msg=" & Msg)
            Exit Sub
        End If
        WebFct = New Virtualia.Net.Controles.WebFonctions(Me, 1)
        If WebFct.PointeurUtilisateur Is Nothing Then
            Msg = "Erreur de serveur. Vous avez été déconnecté(e)."
            Response.Redirect("~/Fenetres/Commun/ErreurApplication.aspx?Msg=" & Msg)
            Exit Sub
        End If
        If WebFct.VerifierCookie(Me, Session.SessionID) = False Then
            Msg = "Erreur lors de l'identification du dossier accédé. Session invalide."
            Response.Redirect("~/Fenetres/Commun/ErreurApplication.aspx?Msg=" & Msg)
            Exit Sub
        End If
        V_UrlRetour = Server.HtmlDecode(Request.QueryString("UrlRetour"))
    End Sub

    Private Sub CreerLesElements()
        Dim Ctl As Control
        Dim VirControle As Virtualia.Net.VNotificationPS
        Dim IndiceI As Integer
        Dim LstVues As List(Of Virtualia.TablesObjet.ShemaVUE.VUE_ATTRIBUTAIRE_PFR)

        If WebFct Is Nothing Then
            WebFct = New Virtualia.Net.Controles.WebFonctions(Me, 1)
        End If
        LstVues = CType(WebFct.PointeurUtilisateur.V_PointeurSessionModule, Virtualia.Net.Session.SessionModule).ExeListePFR
        IndiceI = 0
        Do
            Ctl = WebFct.VirWebControle(Me.CadreGeneral, "Notification_", IndiceI)
            If Ctl Is Nothing Then
                Exit Do
            End If
            VirControle = CType(Ctl, Virtualia.Net.VNotificationPS)
            VirControle.Visible = False
            If LstVues IsNot Nothing AndAlso IndiceI < LstVues.Count Then
                VirControle.Fiche = CType(LstVues.Item(IndiceI), Virtualia.TablesObjet.ShemaVUE.VUE_ATTRIBUTAIRE_PFR)
                VirControle.Visible = True
            End If
            IndiceI += 1
        Loop

    End Sub

    Private Sub FrmPSNotification_PreRender(sender As Object, e As EventArgs) Handles Me.PreRender
        Call CreerLesElements()
    End Sub

    Private Sub CommandeRetour_Click(sender As Object, e As EventArgs) Handles CommandeRetour.Click
        Response.Redirect(V_UrlRetour & "?IDVirtualia=" & Server.HtmlEncode(Session.SessionID))
    End Sub

    Private Sub CommandePdf_Click(sender As Object, e As EventArgs) Handles CommandePdf.Click
        Dim LstVues As List(Of Virtualia.TablesObjet.ShemaVUE.VUE_ATTRIBUTAIRE_PFR)
        Dim FichierHtml As String
        Dim Util As Virtualia.Systeme.Outils.Utilitaires
        Dim FluxHtml As String
        Dim FluxPDF As Byte()

        If WebFct Is Nothing Then
            WebFct = New Virtualia.Net.Controles.WebFonctions(Me, 1)
        End If
        LstVues = CType(WebFct.PointeurUtilisateur.V_PointeurSessionModule, Virtualia.Net.Session.SessionModule).ExeListePFR
        If LstVues Is Nothing Then
            Exit Sub
        End If
        Util = New Virtualia.Systeme.Outils.Utilitaires
        FichierHtml = WebFct.PointeurGlobal.VirRepertoireTemporaire & "\" & Session.SessionID & ".html"
        Call Util.SourceHtmlEditee(FichierHtml, WebFct.VirSourceHtml(Me.CadreGeneral))

        '*** Spécifique Proxy FranceAgrimer **********************************************************
        Dim ChaineLue As String
        Dim ChaineReecrite As String
        Dim Tableaudata(0) As String
        Dim ChaineHote As String
        Dim TableauHote(0) As String
        Tableaudata = Strings.Split(Request.Url.AbsoluteUri, "/Fenetres/", -1)
        ChaineHote = Strings.Right(Tableaudata(0), Tableaudata(0).Length - 7)
        TableauHote = Strings.Split(ChaineHote, "/", -1)
        If TableauHote.Count = 2 Then
            ChaineLue = My.Computer.FileSystem.ReadAllText(FichierHtml)
            ChaineReecrite = ChaineLue.Replace(Tableaudata(0) & "/Images/Specifique/SignatureFAM.jpg", "http://localhost/" & TableauHote(1) & "/Images/Specifique/SignatureFAM.jpg")
            My.Computer.FileSystem.WriteAllText(FichierHtml, ChaineReecrite, False)
        End If
        '***********************************************************************************************
        Try
            Dim WseOutil As New Virtualia.Net.WebService.VirtualiaOutils(System.Configuration.ConfigurationManager.AppSettings("WebService.VirtualiaUtils"))
            FluxHtml = My.Computer.FileSystem.ReadAllText(FichierHtml, System.Text.Encoding.GetEncoding(1252))
            FluxPDF = WseOutil.ConversionPDF(FluxHtml, False)
            WseOutil.Dispose()

            If FluxPDF IsNot Nothing Then
                Dim response As System.Web.HttpResponse = System.Web.HttpContext.Current.Response
                response.Clear()
                response.AddHeader("Content-Type", "binary/octet-stream")
                response.AddHeader("Content-Disposition", "attachment; filename=" & "Notifications.pdf" & "; size=" & FluxPDF.Length.ToString())
                response.Flush()
                response.BinaryWrite(FluxPDF)
                response.Flush()
                response.End()
            End If
        Catch ex As Exception
            Exit Try
        End Try
    End Sub
End Class