﻿Option Explicit On
Option Strict On
Option Compare Text
Imports Virtualia.Systeme.Evenements
Public Class FrmDemandeFormation
    Inherits System.Web.UI.Page
    Private WebFct As Virtualia.Net.Controles.WebFonctions

    Private ReadOnly Property V_WebFonction As Virtualia.Net.Controles.WebFonctions
        Get
            If WebFct Is Nothing Then
                WebFct = New Virtualia.Net.Controles.WebFonctions(Me, 1)
            End If
            Return WebFct
        End Get
    End Property

    Private Sub FrmDemandeFormation_Init(sender As Object, e As EventArgs) Handles Me.Init
        Demande.Act_CocheChange = AddressOf ActDemande_CocheChange
    End Sub

    Private Sub FrmDemandeFormation_PreRender(sender As Object, e As EventArgs) Handles Me.PreRender
        Call FaireListeChoix()
    End Sub

    Private Sub FaireListeChoix()
        If LstTypeArmoire.Items.Count > 0 Then
            Exit Sub
        End If
        LstTypeArmoire.Items.Add(New ListItem("Le calendrier des sessions à venir", "SESSION_A_VENIR"))
        LstTypeArmoire.Items.Add(New ListItem("Liste alphabétique de tous les stages ayant des sessions à venir", "ALPHA_A_VENIR"))
        LstTypeArmoire.Items.Add(New ListItem("Liste alphabétique de tous les stages", "ALPHA"))
        LstTypeArmoire.Items.Add(New ListItem("Liste organisée par domaine de tous les stages ayant des sessions à venir", "DOMAINE_A_VENIR"))
        LstTypeArmoire.Items.Add(New ListItem("Liste organisée par plan, domaine, thème de tous les stages ayant des sessions à venir", "PLAN_A_VENIR"))
    End Sub

    Private Sub LstTypeArmoire_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles LstTypeArmoire.SelectedIndexChanged
        Dim SelValue As String = CType(sender, System.Web.UI.WebControls.DropDownList).SelectedValue
        If SelValue.StartsWith("SESSION") Then
            MultiOnglets.SetActiveView(VueCalendrier)
        Else
            Armoire.V_TypeArmoire = SelValue
            MultiOnglets.SetActiveView(VueArmoire)
        End If
    End Sub

    Private Sub Armoire_Dossier_Click(sender As Object, e As DossierClickEventArgs) Handles Armoire.Dossier_Click
        Dim FicheFOR As Virtualia.Net.WebAppli.SelfV4.DossierFormation
        FicheFOR = CType(V_WebFonction.PointeurUtilisateur.V_PointeurSessionModule, Virtualia.Net.Session.SessionModule).PointeurArmoireFOR.FicheSession(e.Identifiant, e.Valeur)
        If FicheFOR IsNot Nothing Then
            Demande.IntituleStage = FicheFOR.IntituleStage
            Demande.DateDuStage = FicheFOR.Date_Debut
            Demande.DateDeFinStage = FicheFOR.Date_Fin
            Demande.Organisme = FicheFOR.Organisme
            Demande.LieuDuStage = FicheFOR.Lieu
            Demande.UrlDuStage = FicheFOR.Lien_Url
        Else
            Demande.IntituleStage = ""
            Demande.DateDuStage = e.Valeur
            Demande.DateDeFinStage = ""
            Demande.Organisme = ""
            Demande.LieuDuStage = ""
            Demande.UrlDuStage = ""
        End If
    End Sub

    Private Sub CalendrierDyna_ValeurChange(sender As Object, e As DonneeChangeEventArgs) Handles CalendrierDyna.ValeurChange
        If IsNumeric(e.Parametre) = False Then
            Exit Sub
        End If
        Dim FicheFOR As Virtualia.Net.WebAppli.SelfV4.DossierFormation
        FicheFOR = CType(V_WebFonction.PointeurUtilisateur.V_PointeurSessionModule, Virtualia.Net.Session.SessionModule).PointeurArmoireFOR.FicheSession(CInt(e.Parametre), e.Valeur)
        If FicheFOR IsNot Nothing Then
            Demande.IntituleStage = FicheFOR.IntituleStage
            Demande.DateDuStage = FicheFOR.Date_Debut
            Demande.DateDeFinStage = FicheFOR.Date_Fin
            Demande.Organisme = FicheFOR.Organisme
            Demande.LieuDuStage = FicheFOR.Lieu
            Demande.UrlDuStage = FicheFOR.Lien_Url
        Else
            Demande.IntituleStage = ""
            Demande.DateDuStage = e.Valeur
            Demande.DateDeFinStage = ""
            Demande.Organisme = ""
            Demande.LieuDuStage = ""
            Demande.UrlDuStage = ""
        End If
    End Sub

    Private Sub ActDemande_CocheChange(ByVal Valeur As Boolean)
        CellTypeArmoire.Visible = Not (Valeur)
        ConteneurVues.Visible = Not (Valeur)
    End Sub

    Private Sub Msg_MessageDialogue(sender As Object, e As MessageSaisieEventArgs) Handles Demande.MessageDialogue
        HPopupMsg.Value = "1"
        CellMessage.Visible = True
        MsgVirtualia.AfficherMessage = e
        PopupMsg.Show()
    End Sub

    Private Sub MsgVirtualia_ValeurRetour(sender As Object, e As MessageRetourEventArgs) Handles MsgVirtualia.ValeurRetour
        HPopupMsg.Value = "0"
        CellMessage.Visible = False
    End Sub
End Class