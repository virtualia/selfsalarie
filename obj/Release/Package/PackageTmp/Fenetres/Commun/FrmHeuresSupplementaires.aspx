﻿<%@ Page Language="vb" AutoEventWireup="false" MasterPageFile="~/PagesMaitres/VirtualiaMain.Master" CodeBehind="FrmHeuresSupplementaires.aspx.vb" 
    Inherits="Virtualia.Net.FrmHeuresSupplementaires" MaintainScrollPositionOnPostback="True" %>

<%@ MasterType VirtualPath="~/PagesMaitres/VirtualiaMain.master" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<%@ Register src="~/Controles/HeuresSupp/CtlPerHeureSupMensuel.ascx" tagname="HS_MENSUEL" tagprefix="Virtualia" %>
<%@ Register src="~/Controles/HeuresSupp/CtlSaisieFaitGenerateur.ascx" tagname="VSaisieOri" tagprefix="Virtualia" %>
<%@ Register src="~/Controles/HeuresSupp/CtlSaisieDemandeHS.ascx" tagname="VSaisieHS" tagprefix="Virtualia" %>
<%@ Register src="~/Controles/HeuresSupp/PER_JOUR_ASTREINTE.ascx" tagname="VAstreinte" tagprefix="Virtualia" %>
<%@ Register src="~/Controles/Commun/VListeGrid.ascx" tagname="VListeGrid" tagprefix="Virtualia" %>
<%@ Register src="~/Controles/Commun/VGrillePersonnalisee.ascx" tagname="VGrillePerso" tagprefix="Virtualia" %>
<%@ Register src="~/Controles/Commun/VArmoirePER.ascx" tagname="VArmoire" tagprefix="Virtualia" %>

<asp:Content ID="Corps" ContentPlaceHolderID="Principal" runat="server">
    <asp:Table ID="CadreGeneral" runat="server" HorizontalAlign="Center" Width="1050px">
        <asp:TableRow>
            <asp:TableCell HorizontalAlign="Center" VerticalAlign="Top">
                <asp:MultiView ID="MultiVues" runat="server" ActiveViewIndex="0">
                    <asp:View ID="VueHeureSup" runat="server">
                         <asp:Panel ID="CadreAffichage" runat="server" BackColor="Transparent" HorizontalAlign="Center" Width="1050px"
                            BackImageUrl="~/Images/Fonds/Fond_Saisie.jpg" BorderStyle="none">
                            <asp:Table ID="CadreHSupp" runat="server" Width="1040px" HorizontalAlign="Center" BorderStyle="none">
                                 <asp:TableRow Height="40px">
                                     <asp:TableCell HorizontalAlign="Center">
                                         <asp:Table ID="CadreIdentite" runat="server">
                                             <asp:TableRow>
                                                 <asp:TableCell ID="CellRetour" Width="150px" VerticalAlign="Top" HorizontalAlign="Left">
                                                    <asp:ImageButton ID="CommandeRetour" runat="server" Width="32px" Height="32px" Visible="false"
                                                        BorderStyle="None" ImageUrl="~/Images/Boutons/FlecheRetourContexte.jpg" 
                                                        ImageAlign="Middle" style="margin-left:1px; margin-bottom:10px;">
                                                    </asp:ImageButton>
                                                </asp:TableCell>
                                                <asp:TableCell VerticalAlign="Middle" HorizontalAlign="Center">
                                                    <asp:Label ID="EtiIdentité" runat="server" Height="20px" Width="500px" Text=""
                                                        BackColor="#124545" BorderColor="#B6C7E2"  BorderStyle="Groove" BorderWidth="2px" ForeColor="#D7FAF3"
                                                        Font-Bold="True" Font-Names="Trebuchet MS" Font-Size="Small"
                                                        style="margin-bottom: 10px;text-indent:5px; text-align: center;" >
                                                    </asp:Label>          
                                                </asp:TableCell>
                                                <asp:TableCell Width="370px" HorizontalAlign="Right">
                                                    <asp:Button ID="CommandeAstreinte" runat="server" BackColor="#1C5150" Width="275px" Height="22px" 
                                                        BorderStyle="Outset" BorderWidth="2px" BorderColor="WhiteSmoke" 
                                                        Font-Names="Trebuchet MS" Font-Size="Small" ForeColor="#B0E0D7" Font-Bold="false"
                                                        Font-Underline="true" Font-Italic="true"  Visible="True"
                                                        Text="Déclaration des journées d'astreinte" Tooltip="Déclaration des journées d'astreinte en vue de leur paiement"
                                                        style="margin-bottom: 10px;margin-right:5px;text-align:center">
                                                    </asp:Button>
                                                </asp:TableCell>
                                             </asp:TableRow>
                                         </asp:Table>
                                     </asp:TableCell>
                                </asp:TableRow>
                                <asp:TableRow>
                                    <asp:TableCell VerticalAlign="Top" HorizontalAlign="Center">
                                        <asp:Table ID="CadreSituationHS" runat="server">
                                            <asp:TableRow>
                                                <asp:TableCell HorizontalAlign="Center">
                                                    <asp:Table ID="CadreSoldeAnnuel" runat="server" HorizontalAlign="Center" BorderStyle="Ridge" BorderWidth="2px" BorderColor="#B0E0D7">
                                                        <asp:TableRow>
                                                            <asp:TableCell VerticalAlign="Middle" HorizontalAlign="Center" ColumnSpan="7">
                                                                <asp:Label ID="EtiTitreSolde" runat="server" Height="22px" Width="840px" Text="Situation de l'année"
                                                                    BackColor="#124545" BorderColor="#B6C7E2"  BorderStyle="Groove" BorderWidth="2px" ForeColor="#D7FAF3"
                                                                    Font-Bold="True" Font-Names="Trebuchet MS" Font-Size="Small"
                                                                    style="margin-top:5px; text-indent:5px; text-align:center;" >
                                                                </asp:Label>          
                                                            </asp:TableCell>
                                                        </asp:TableRow>
                                                        <asp:TableRow>
                                                            <asp:TableCell>
                                                                <asp:Label ID="EtiSoldeAnte" runat="server" Height="36px" Width="120px" Text="Situation en début d'année"
                                                                    BackColor="#225C59" BorderColor="#B6C7E2"  BorderStyle="Groove" BorderWidth="2px" ForeColor="#D7FAF3"
                                                                    Font-Bold="True" Font-Names="Trebuchet MS" Font-Size="Small"
                                                                    style="text-indent:5px;text-align:center;" >
                                                                </asp:Label>
                                                            </asp:TableCell> 
                                                            <asp:TableCell>
                                                                <asp:Label ID="EtiTotalHS" runat="server" Height="36px" Width="120px" Text="Total des heures compensables"
                                                                    BackColor="#225C59" BorderColor="#B6C7E2"  BorderStyle="Groove" BorderWidth="2px" ForeColor="#D7FAF3"
                                                                    Font-Bold="True" Font-Names="Trebuchet MS" Font-Size="Small"
                                                                    style="text-indent:5px;text-align:center;" >
                                                                </asp:Label>          
                                                            </asp:TableCell>
                                                            <asp:TableCell>
                                                                <asp:Label ID="EtiTotalRemu" runat="server" Height="36px" Width="120px" Text="Total des heures rémunéréees"
                                                                    BackColor="#225C59" BorderColor="#B6C7E2"  BorderStyle="Groove" BorderWidth="2px" ForeColor="#D7FAF3"
                                                                    Font-Bold="True" Font-Names="Trebuchet MS" Font-Size="Small"
                                                                    style="text-indent:5px;text-align:center;" >
                                                                </asp:Label>          
                                                            </asp:TableCell>
                                                            <asp:TableCell>
                                                                <asp:Label ID="EtiTotalRecup" runat="server" Height="36px" Width="120px" Text="Total des heures récupérées"
                                                                    BackColor="#225C59" BorderColor="#B6C7E2"  BorderStyle="Groove" BorderWidth="2px" ForeColor="#D7FAF3"
                                                                    Font-Bold="True" Font-Names="Trebuchet MS" Font-Size="Small"
                                                                    style="text-indent:5px; text-align:center;" >
                                                                </asp:Label>          
                                                            </asp:TableCell> 
                                                            <asp:TableCell>
                                                                <asp:Label ID="Eti2MoisCompensable" runat="server" Height="36px" Width="120px" Text="HS compensables depuis 2 mois"
                                                                    BackColor="#225C59" BorderColor="#B6C7E2"  BorderStyle="Groove" BorderWidth="2px" ForeColor="#D7FAF3"
                                                                    Font-Bold="True" Font-Names="Trebuchet MS" Font-Size="Small" ToolTip="Total des heures compensables des deux derniers mois"
                                                                    style="text-indent:5px; text-align:center;" >
                                                                </asp:Label>          
                                                            </asp:TableCell>
                                                            <asp:TableCell>
                                                                <asp:Label ID="Eti2MoisRecup" runat="server" Height="36px" Width="120px" Text="HS récupérées depuis 2 mois"
                                                                    BackColor="#225C59" BorderColor="#B6C7E2"  BorderStyle="Groove" BorderWidth="2px" ForeColor="#D7FAF3"
                                                                    Font-Bold="True" Font-Names="Trebuchet MS" Font-Size="Small"
                                                                    style="text-indent:5px; text-align:center;" >
                                                                </asp:Label>          
                                                            </asp:TableCell>
                                                            <asp:TableCell>
                                                                <asp:Label ID="EtiSoldeHS" runat="server" Height="36px" Width="120px" Text="Solde courant"
                                                                    BackColor="#225C59" BorderColor="#B6C7E2"  BorderStyle="Groove" BorderWidth="2px" ForeColor="#D7FAF3"
                                                                    Font-Bold="True" Font-Names="Trebuchet MS" Font-Size="Small"
                                                                    style="text-indent:5px;text-align:center;" >
                                                                </asp:Label>          
                                                            </asp:TableCell>
                                                        </asp:TableRow>
                                                        <asp:TableRow>
                                                            <asp:TableCell>
                                                                <asp:Label ID="DonSoldeAnte" runat="server" Height="20px" Width="120px" Text="0,00"
                                                                    BackColor="White" BorderColor="#B6C7E2"  BorderStyle="Inset" BorderWidth="2px" ForeColor="#124545"
                                                                    Font-Bold="True" Font-Names="Trebuchet MS" Font-Size="Small"
                                                                    style="text-indent:5px;text-align:center;" >
                                                                </asp:Label>          
                                                            </asp:TableCell>
                                                            <asp:TableCell>
                                                                <asp:Label ID="DonTotalHS" runat="server" Height="20px" Width="120px" Text="0,00"
                                                                    BackColor="White" BorderColor="#B6C7E2"  BorderStyle="Inset" BorderWidth="2px" ForeColor="#124545"
                                                                    Font-Bold="True" Font-Names="Trebuchet MS" Font-Size="Small"
                                                                    style="text-indent:5px;text-align:center;" >
                                                                </asp:Label>          
                                                            </asp:TableCell>
                                                            <asp:TableCell>
                                                                <asp:Label ID="DonTotalRemu" runat="server" Height="20px" Width="120px" Text="0,00"
                                                                    BackColor="White" BorderColor="#B6C7E2"  BorderStyle="Inset" BorderWidth="2px" ForeColor="#124545"
                                                                    Font-Bold="True" Font-Names="Trebuchet MS" Font-Size="Small"
                                                                    style="text-indent:5px;text-align:center;" >
                                                                </asp:Label>          
                                                            </asp:TableCell>
                                                            <asp:TableCell>
                                                                <asp:Label ID="DonTotalRecup" runat="server" Height="20px" Width="120px" Text="0,00"
                                                                    BackColor="White" BorderColor="#B6C7E2"  BorderStyle="Inset" BorderWidth="2px" ForeColor="#C71F23"
                                                                    Font-Bold="True" Font-Names="Trebuchet MS" Font-Size="Small"
                                                                    style="text-indent:5px;text-align:center;" >
                                                                </asp:Label>          
                                                            </asp:TableCell>
                                                            <asp:TableCell>
                                                                <asp:Label ID="Don2MoisCompensable" runat="server" Height="20px" Width="120px" Text="0,00"
                                                                    BackColor="White" BorderColor="#B6C7E2"  BorderStyle="Inset" BorderWidth="2px" ForeColor="#124545"
                                                                    Font-Bold="True" Font-Names="Trebuchet MS" Font-Size="Small"
                                                                    style="text-indent:5px;text-align:center;" >
                                                                </asp:Label>          
                                                            </asp:TableCell>
                                                            <asp:TableCell>
                                                                <asp:Label ID="Don2MoisRecup" runat="server" Height="20px" Width="120px" Text="0,00"
                                                                    BackColor="White" BorderColor="#B6C7E2"  BorderStyle="Inset" BorderWidth="2px" ForeColor="#C71F23"
                                                                    Font-Bold="True" Font-Names="Trebuchet MS" Font-Size="Small"
                                                                    style="text-indent:5px;text-align:center;" >
                                                                </asp:Label>          
                                                            </asp:TableCell>
                                                            <asp:TableCell>
                                                                <asp:Label ID="DonSoldeHS" runat="server" Height="20px" Width="120px" Text="0,00"
                                                                    BackColor="White" BorderColor="#B6C7E2"  BorderStyle="Inset" BorderWidth="2px" ForeColor="#124545"
                                                                    Font-Bold="True" Font-Names="Trebuchet MS" Font-Size="Small"
                                                                    style="text-indent:5px;text-align:center;" >
                                                                </asp:Label>          
                                                            </asp:TableCell>
                                                        </asp:TableRow>
                                                    </asp:Table>
                                                </asp:TableCell>
                                            </asp:TableRow>
                                            <asp:TableRow>
                                                <asp:TableCell HorizontalAlign="Center">
                                                    <asp:Table ID="CadreDetailMensuel" runat="server">
                                                        <asp:TableRow>
                                                            <asp:TableCell VerticalAlign="Top" HorizontalAlign="Left">
                                                                <asp:Label ID="EtiSelMoisAnnee" runat="server" Text="Période mensuelle"
                                                                            Height="20px" Width="150px" BackColor="#225C59" ForeColor="#D7FAF3"
                                                                            BorderColor="#B6C7E2"  BorderStyle="Ridge" BorderWidth="2px"
                                                                            Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small" Font-Italic="true"
                                                                            style="margin-left:5px;text-align:center">
                                                                </asp:Label>
                                                            </asp:TableCell>
                                                            <asp:TableCell VerticalAlign="Top" HorizontalAlign="Left">
                                                                <asp:DropDownList ID="DropDownMoisAnnee" runat="server" AutoPostBack="True" 
                                                                    Height="22px" Width="150px" BackColor="White" ForeColor="#124545"
                                                                    style="margin-top:0px; margin-left:5px; border-spacing:2px; font-family:Trebuchet MS; 
                                                                            font-size:small; font-weight:bold; font-style:normal; text-indent:5px; text-align:left">
                                                                </asp:DropDownList>
                                                            </asp:TableCell>
                                                            <asp:TableCell Width="750px"></asp:TableCell>
                                                        </asp:TableRow>
                                                        <asp:TableRow>
                                                            <asp:TableCell HorizontalAlign="Center" ColumnSpan="3">
                                                                <Virtualia:HS_MENSUEL ID="HsMensuel" runat="server" ></Virtualia:HS_MENSUEL>
                                                            </asp:TableCell>
                                                        </asp:TableRow>
                                                    </asp:Table>
                                                </asp:TableCell>
                                            </asp:TableRow>
                                        </asp:Table>
                                    </asp:TableCell>
                                </asp:TableRow>
                                <asp:TableRow>
                                    <asp:TableCell VerticalAlign="Top">
                                        <asp:Label ID="EtiListeHS" runat="server" Height="20px" Width="500px" Text="Détail des heures supplémentaires du mois"
                                            BackColor="#124545" BorderColor="#B6C7E2"  BorderStyle="Groove" BorderWidth="2px" ForeColor="#D7FAF3"
                                            Font-Bold="True" Font-Names="Trebuchet MS" Font-Size="Small"
                                            style="margin-top: 5px; margin-left: 4px; margin-bottom: 10px; text-indent: 5px; text-align: center;" >
                                        </asp:Label>          
                                    </asp:TableCell>
                                </asp:TableRow>
                               <asp:TableRow>
                                    <asp:TableCell VerticalAlign="Top">
                                        <Virtualia:VGrillePerso ID="ListeDetailHS" runat="server" BackColorCadre="Transparent"
                                             Nombre_Colonnes="9" SiStyleAlterne="true" SiColonneCommande="true" Nombre_Lignes="31" ParticulariteLigne="WEJF"
                                             Taille_Colonne_1="180px" Taille_Colonne_2="150px" Taille_Colonne_3="150px" Taille_Colonne_4="90px"
                                             Taille_Colonne_5="90px" Taille_Colonne_6="90px" Taille_Colonne_7="90px" Taille_Colonne_8="80px" Taille_Colonne_9="70px"
                                             Style_Colonne_2="Text-align:center" Style_Colonne_3="Text-align:center"
                                             Style_Colonne_4="Text-align:center" Style_Colonne_5="Text-align:center" Style_Colonne_6="Text-align:center"
                                             Style_Colonne_7="Text-align:center" Style_Colonne_8="Text-align:center" Style_Colonne_9="Text-align:center" 
                                             ToolTipImageCommande="Saisir le fait générateur" NumColonneSiCmdVisible="2" />
                                    </asp:TableCell>
                               </asp:TableRow>
                               <asp:TableRow>
                                    <asp:TableCell VerticalAlign="Top">
                                        <asp:Label ID="EtiListeRecup" runat="server" Height="20px" Width="500px" Text="Détail des récupérations des deux derniers mois"
                                            BackColor="#124545" BorderColor="#B6C7E2"  BorderStyle="Groove" BorderWidth="2px" ForeColor="#D7FAF3"
                                            Font-Bold="True" Font-Names="Trebuchet MS" Font-Size="Small"
                                            style="margin-top: 8px; margin-left: 4px; margin-bottom: 3px; text-indent: 5px; text-align: center;" >
                                        </asp:Label>          
                                    </asp:TableCell>
                                </asp:TableRow>
                               <asp:TableRow>
                                    <asp:TableCell VerticalAlign="Top">
                                        <Virtualia:VListeGrid ID="ListeRecup" runat="server" CadreWidth="500px"
                                                SiCaptionVisible="false" SiPagination="false" SiCaseAcocher="false"
                                                SiColonneSelect="false" TaillePage="200" />
                                    </asp:TableCell>
                               </asp:TableRow>
                            </asp:Table>
                        </asp:Panel>               
                    </asp:View>
                    <asp:View ID="VueAstreinte" runat="server">
                        <Virtualia:VAstreinte ID="PER_ASTREINTE_171" runat="server" />
                    </asp:View>
                    <asp:View ID="VueArmoire" runat="server">
                        <Virtualia:VArmoire ID="ArmoirePER" runat="server" V_Appelant="HSUPP" />
                    </asp:View>
                </asp:MultiView>
            </asp:TableCell>
        </asp:TableRow>
        <asp:TableRow>
             <asp:TableCell ID="CellSaisieOri" Visible="false">
                <ajaxToolkit:ModalPopupExtender ID="PopupSaisie" runat="server" TargetControlID="HPopupSaisie" PopupControlID="PanelSaisiePopup" BackgroundCssClass="modalBackground" />
                <asp:Panel ID="PanelSaisiePopup" runat="server">
                    <Virtualia:VSaisieOri id="SaisieFaitGen" runat="server" />
                </asp:Panel>
                <asp:HiddenField ID="HPopupSaisie" runat="server" Value="0" />
            </asp:TableCell>
        </asp:TableRow>
        <asp:TableRow>
             <asp:TableCell ID="CellSaisieHS" Visible="false">
                <ajaxToolkit:ModalPopupExtender ID="PopupDemande" runat="server" TargetControlID="HPopupDemande" PopupControlID="PanelDemandePopup" BackgroundCssClass="modalBackground" />
                <asp:Panel ID="PanelDemandePopup" runat="server">
                    <Virtualia:VSaisieHS id="SaisieDemande" runat="server" />
                </asp:Panel>
                <asp:HiddenField ID="HPopupDemande" runat="server" Value="0" />
            </asp:TableCell>
        </asp:TableRow>
    </asp:Table>
</asp:Content>



