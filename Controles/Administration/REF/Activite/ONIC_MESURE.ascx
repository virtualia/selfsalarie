﻿<%@ Control Language="VB" AutoEventWireup="false" Inherits="Virtualia.Net.Fenetre_ONIC_MESURE" CodeBehind="ONIC_MESURE.ascx.vb" %>

<%@ Register Src="~/Controles/Saisies/VCoupleEtiDonnee.ascx" TagName="VCoupleEtiDonnee" TagPrefix="Virtualia" %>
<%@ Register Src="~/Controles/Saisies/VCoupleVerticalEtiDonnee.ascx" TagName="VCoupleVerticalEtiDonnee" TagPrefix="Virtualia" %>

<asp:Table ID="CadreControle" runat="server" Width="1050px" CellPadding="0" CellSpacing="0" HorizontalAlign="Center" BackColor="#D7FAF3">
    <asp:TableRow>
        <asp:TableCell HorizontalAlign="Left">
            <asp:ImageButton ID="CommandeRetour" runat="server" Width="32px" Height="32px" 
                    BorderStyle="None" ImageUrl="~/Images/Boutons/FlecheRetourContexte.jpg" 
                    ImageAlign="Middle" style="margin-left: 1px;">
            </asp:ImageButton>
        </asp:TableCell>
        <asp:TableCell HorizontalAlign="Right">
            <asp:Table ID="CadreCmdFichier" runat="server" Height="20px" CellPadding="0" CellSpacing="0" BackImageUrl="~/Images/Icones/Cmd_Std.bmp"
                BorderWidth="2px" BorderStyle="Outset" BorderColor="#B6C7E2" ForeColor="#D7FAF3" Width="150px" HorizontalAlign="Right" style="margin-top: 6px; margin-right: 20px">
                <asp:TableRow VerticalAlign="Top">
                    <asp:TableCell VerticalAlign="Top">
                        <asp:Button ID="CommandeCsv" runat="server" Text="Fichier CSV" Width="135px" Height="20px"
                            BackColor="Transparent" BorderColor="#B6C7E2" ForeColor="#D7FAF3"
                            Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small" Font-Italic="true"
                            BorderStyle="None" style=" margin-left: 12px; text-align: left;"
                            Tooltip="Crée un fichier au format Csv (Séparateur Point-virgule)">
                        </asp:Button>
                    </asp:TableCell>
                </asp:TableRow>
            </asp:Table>
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell>
            <asp:Label ID="Eti_Activite" runat="server" Height="23px" Width="100px"
                BackColor="#B0E0D7" BorderColor="#D2D2D2" BorderStyle="None" Text="Projet"
                BorderWidth="1px" ForeColor="#142425" Font-Italic="False" Font-Underline="false"
                Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Smaller"
                style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px;
                font-style: normal; text-indent: 5px; text-align: left; padding-top: 4px;">
            </asp:Label>      
        </asp:TableCell>
        <asp:TableCell>
            <asp:DropDownList ID="VListeActivites" runat="server" Height="22px" Width="700px"
                    AutoPostBack="true" BackColor="#B0E0D7" ForeColor="#142425"
                    style="border-color:#7EC8BE;border-width:2px;border-style:inset;display:table-cell;font-style:oblique;" >
            </asp:DropDownList>        
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell>
            <asp:Label ID="Eti_Etape" runat="server" Height="23px" Width="100px"
                BackColor="#B0E0D7" BorderColor="#FFDDA1" BorderStyle="None" Text="Etape"
                BorderWidth="1px" ForeColor="#142425" Font-Italic="False" Font-Underline="false"
                Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Smaller"
                style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px;
                font-style: normal; text-indent: 5px; text-align: left; padding-top: 4px;">
            </asp:Label>    
        </asp:TableCell>
        <asp:TableCell>
            <asp:DropDownList ID="VListeEtapes" runat="server" Height="22px" Width="900px"
                    AutoPostBack="true" BackColor="#B0E0D7" ForeColor="#142425"
                    style="border-color:#7EC8BE;border-width:2px;border-style:inset;display:table-cell;font-style:oblique;" >
            </asp:DropDownList>        
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell ColumnSpan="2" Height="10px"></asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell ColumnSpan="2">
            <asp:Table ID="CadreInfo" runat="server" BorderStyle="Ridge" BorderWidth="2px" BorderColor="#FFEBC8" Height="500px" Width="1020px" HorizontalAlign="Center">
                <asp:TableRow>
                    <asp:TableCell>
                        <asp:Table ID="CadreCommandes" runat="server" Height="22px" CellPadding="0" CellSpacing="0"
                            Width="244px" HorizontalAlign="Right" style="margin-top: 3px;margin-right: 3px">
                            <asp:TableRow>
                                <asp:TableCell VerticalAlign="Bottom">
                                    <asp:Table ID="CadreCmdNewSupp" runat="server" BackImageUrl="~/Images/Boutons/NewSupp_Std.bmp" 
                                        Width="160px" CellPadding="0" CellSpacing="0"
                                        BorderWidth="2px" BorderStyle="Outset" BorderColor="#B0E0D7" ForeColor="#D7FAF3">
                                        <asp:TableRow>
                                            <asp:TableCell>
                                                <asp:Button ID="CommandeNew" runat="server" Text="Nouveau" Width="68px" Height="20px"
                                                    BackColor="Transparent" BorderColor="#B0E0D7" ForeColor="#D7FAF3"
                                                    Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small" Font-Italic="true"
                                                    BorderStyle="None" style=" margin-left: 10px; text-align: right;"
                                                    Tooltip="Nouvelle fiche" >
                                                </asp:Button>
                                            </asp:TableCell>
                                            <asp:TableCell>
                                                <asp:Button ID="CommandeSupp" runat="server" Text="Supprimer" Width="68px" Height="20px"
                                                    BackColor="Transparent" BorderColor="#B0E0D7" ForeColor="#D7FAF3"
                                                    Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small" Font-Italic="true"
                                                    BorderStyle="None" style=" margin-left: 10px; text-align: right;"
                                                    Tooltip="Supprimer la fiche" >
                                                </asp:Button>
                                            </asp:TableCell>
                                        </asp:TableRow>
                                    </asp:Table>    
                                </asp:TableCell>
                                <asp:TableCell VerticalAlign="Bottom">
                                    <asp:Table ID="CadreCmdOK" runat="server" BackImageUrl="~/Images/Boutons/OK_Std.bmp" Width="70px"
                                        BorderWidth="2px" BorderStyle="Outset" BorderColor="#B0E0D7" ForeColor="#D7FAF3"
                                        CellPadding="0" CellSpacing="0" Visible="false" >
                                        <asp:TableRow>
                                            <asp:TableCell>
                                                <asp:Button ID="CommandeOK" runat="server" Text="Valider" Width="65px" Height="20px"
                                                    BackColor="Transparent" BorderColor="#B0E0D7" ForeColor="#D7FAF3" 
                                                    Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small" Font-Italic="true"
                                                    BorderStyle="None" style=" margin-left: 6px; text-align: right;">
                                                    </asp:Button>
                                            </asp:TableCell>
                                        </asp:TableRow>
                                    </asp:Table>  
                                </asp:TableCell>
                            </asp:TableRow>
                         </asp:Table>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell>
                        <asp:Table ID="CadreTitre" runat="server" Height="40px" CellPadding="0" CellSpacing="0" HorizontalAlign="Center">
                            <asp:TableRow>
                                <asp:TableCell>
                                    <asp:Label ID="Etiquette" runat="server" Text="Paramétrage des activités" Height="20px" Width="350px" BackColor="#124545" ForeColor="White"
                                        BorderColor="#FFEBC8" BorderStyle="Groove" BorderWidth="2px" Font-Bold="True" Font-Names="Trebuchet MS" Font-Size="Small" 
                                        Style="margin-top: 15px; margin-left: 4px; margin-bottom: 10px; font-style: oblique; text-indent: 5px; text-align: center;" />
                                </asp:TableCell>
                            </asp:TableRow>
                        </asp:Table>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell>
                        <asp:Table ID="CadreDon" runat="server" Height="25px" CellPadding="0" CellSpacing="0" Width="610px">
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Left">
                                    <Virtualia:VCoupleEtiDonnee ID="InfoH01" runat="server" V_PointdeVue="48" V_Objet="2" V_Information="1" V_SiDonneeDico="true" 
                                        EtiWidth="100px" DonWidth="50px" DonTabIndex="1" V_SiEnLectureSeule="true" />
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Left">
                                    <Virtualia:VCoupleEtiDonnee ID="InfoH02" runat="server" V_PointdeVue="48" V_Objet="2" V_Information="2" V_SiDonneeDico="true" 
                                        EtiWidth="100px" DonWidth="700px" DonTabIndex="2" V_SiAutoPostBack="true" EtiText="Projet" />
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Left">
                                    <Virtualia:VCoupleEtiDonnee ID="InfoH03" runat="server" V_PointdeVue="48" V_Objet="2" V_Information="3" V_SiDonneeDico="true" 
                                        EtiWidth="100px" DonWidth="900px" DonTabIndex="3" V_SiAutoPostBack="true" EtiText="Etape"  />
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell Height="10px" />
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Center">
                                    <Virtualia:VCoupleVerticalEtiDonnee ID="InfoV04" runat="server" DonTextMode="true" V_PointdeVue="48" V_Objet="2" V_Information="4" 
                                        V_SiDonneeDico="true" EtiWidth="720px" DonWidth="720px" DonHeight="160px" DonTabIndex="4" Etistyle="text-align:center;" 
                                        V_SiAutoPostBack="true"/>
                                </asp:TableCell>
                            </asp:TableRow>
                        </asp:Table>
                    </asp:TableCell>
                </asp:TableRow>
            </asp:Table>
        </asp:TableCell>
    </asp:TableRow>
</asp:Table>
