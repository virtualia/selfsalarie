﻿Option Explicit On
Option Strict On
Option Compare Text
Imports VI = Virtualia.Systeme.Constantes
Namespace Session
    Public Class SessionModule
        Private WsParent As Virtualia.Net.Session.ObjetSession
        Private WsAppGlobal As Virtualia.Net.Session.ObjetGlobal
        Private WsUtiV3 As Virtualia.Version3.Utilisateur.UtilisateurV3 = Nothing
        Private WsDossierPER As Virtualia.Net.WebAppli.SelfV4.DossierPersonne

        Private WsCourantArmoireFOR As Virtualia.Net.WebAppli.SelfV4.PlanFormation

        Private WsSiManager As Boolean = False
        Private WsSiGRH As Boolean = False
        Private WsSiDG As Boolean = False
        Private WsVueActiveApha As String = ""
        Private WsSiEnExecution As Boolean
        Private WsNiveauManager As Integer = 0
        Private WsNiveauBlocage As Integer = 9

        Private WsExeListeFiches As List(Of Virtualia.Systeme.MetaModele.VIR_FICHE) = Nothing
        Private WsExeListeMoisActivite As List(Of Virtualia.Net.WebAppli.SelfV4.MoisActivite) = Nothing
        Private WsExeImportPFR As Virtualia.Net.Execution.ImportPFR = Nothing
        Private WsExeListePFR As List(Of Virtualia.TablesObjet.ShemaVUE.VUE_ATTRIBUTAIRE_PFR) = Nothing
        Private WsExeFichier As String = ""

        Public ReadOnly Property VParent As Virtualia.Net.Session.ObjetSession
            Get
                Return WsParent
            End Get
        End Property

        Public Property DossierPER() As Virtualia.Net.WebAppli.SelfV4.DossierPersonne
            Get
                Return WsDossierPER
            End Get
            Set(value As Virtualia.Net.WebAppli.SelfV4.DossierPersonne)
                WsDossierPER = value
            End Set
        End Property

        Public ReadOnly Property VPointeurGlobal As Virtualia.Net.Session.ObjetGlobal
            Get
                Return WsAppGlobal
            End Get
        End Property

        Public ReadOnly Property PointeurArmoireFOR As Virtualia.Net.WebAppli.SelfV4.PlanFormation
            Get
                If WsCourantArmoireFOR Is Nothing Then
                    WsCourantArmoireFOR = New Virtualia.Net.WebAppli.SelfV4.PlanFormation(Me)
                End If
                Return WsCourantArmoireFOR
            End Get
        End Property

        Public Property ExeListePFR As List(Of Virtualia.TablesObjet.ShemaVUE.VUE_ATTRIBUTAIRE_PFR)
            Get
                Return WsExeListePFR
            End Get
            Set(value As List(Of Virtualia.TablesObjet.ShemaVUE.VUE_ATTRIBUTAIRE_PFR))
                WsExeListePFR = value
            End Set
        End Property

        Public Property ExeListeFiches As List(Of Virtualia.Systeme.MetaModele.VIR_FICHE)
            Get
                Return WsExeListeFiches
            End Get
            Set(value As List(Of Virtualia.Systeme.MetaModele.VIR_FICHE))
                WsExeListeFiches = value
            End Set
        End Property

        Public Property ExeListeMoisActivite As List(Of Virtualia.Net.WebAppli.SelfV4.MoisActivite)
            Get
                Return WsExeListeMoisActivite
            End Get
            Set(value As List(Of Virtualia.Net.WebAppli.SelfV4.MoisActivite))
                WsExeListeMoisActivite = value
            End Set
        End Property

        Public Property ExeImportPFR As Virtualia.Net.Execution.ImportPFR
            Get
                Return WsExeImportPFR
            End Get
            Set(value As Virtualia.Net.Execution.ImportPFR)
                WsExeImportPFR = value
            End Set
        End Property

        Public Property VueActiveAlPha As String
            Get
                Return WsVueActiveApha
            End Get
            Set(value As String)
                WsVueActiveApha = value
            End Set
        End Property

        Public Property SiEnExecution As Boolean
            Get
                Return WsSiEnExecution
            End Get
            Set(value As Boolean)
                WsSiEnExecution = value
            End Set
        End Property

        Public Property SiAccesManager As Boolean
            Get
                Return WsSiManager
            End Get
            Set(value As Boolean)
                WsSiManager = value
            End Set
        End Property

        Public ReadOnly Property NiveauManager As Integer
            Get
                Return WsNiveauManager
            End Get
        End Property

        Public ReadOnly Property SiDG As Boolean
            Get
                Return WsSiDG
            End Get
        End Property

        Public ReadOnly Property SiBoutonFigeVisible(ByVal Annee As Integer) As Boolean
            Get
                If SiAccesGRH = True Then
                    Return False
                End If
                If WsNiveauManager = 5 Then
                    Return False
                End If
                If WsNiveauBlocage = 9 Then
                    If ListeVues_PFR(Annee, "", "") Is Nothing Then
                        Return False
                    End If
                End If
                If WsNiveauBlocage = 0 Then
                    Return True
                End If
                If WsNiveauManager + 1 >= WsNiveauBlocage Then
                    Return False
                End If
                Return True
            End Get
        End Property

        Public ReadOnly Property SiFige(ByVal Annee As Integer) As Boolean
            Get
                If SiAccesGRH = True Then
                    Return False
                End If
                If WsNiveauBlocage = 9 Then
                    If ListeVues_PFR(Annee, "", "") Is Nothing Then
                        Return True
                    End If
                End If
                If WsNiveauBlocage = 0 Then
                    Return False
                End If
                If WsNiveauManager >= WsNiveauBlocage Then
                    Return True
                End If
                Return False
            End Get
        End Property

        Public ReadOnly Property InfosAccesManager() As Boolean
            Get
                Dim ManagerPer As Virtualia.Net.WebAppli.SelfV4.DossierPersonne = Nothing
                Dim FichePER As Virtualia.TablesObjet.ShemaPER.PER_AFFECTATION = Nothing
                Dim IndiceI As Integer = 0
                Dim StructureAff As String = ""
                Dim IdeManager As Integer

                FichePER = WsDossierPER.VFiche_Affectation(WsAppGlobal.VirRhDates.DateduJour)
                If FichePER Is Nothing Then
                    Return Nothing
                End If
                For IndiceI = 5 To 1 Step -1
                    Select Case IndiceI
                        Case 1
                            StructureAff = FichePER.Structure_de_1er_niveau
                        Case 2
                            StructureAff = FichePER.Structure_de_2e_niveau
                        Case 3
                            StructureAff = FichePER.Structure_de_3e_niveau
                        Case 4
                            StructureAff = FichePER.Structure_de_4e_niveau
                        Case 5
                            StructureAff = FichePER.Structure_de_5e_niveau
                    End Select
                    If StructureAff <> "" Then
                        IdeManager = WsAppGlobal.VirIdentifiantManager(FichePER.Ide_Dossier, IndiceI)
                        If IdeManager > 0 AndAlso IdeManager <> WsDossierPER.Ide_Dossier Then
                            ManagerPer = WsAppGlobal.Dossier(IdeManager)
                            If ManagerPer IsNot Nothing Then
                                WsDossierPER.Ide_Manager = IdeManager
                                WsDossierPER.Nom_Prenom_Manager = ManagerPer.Nom & Strings.Space(1) & ManagerPer.Prenom
                                WsDossierPER.EMail_Manager = ManagerPer.EMail
                                Return True
                            End If
                        End If
                    End If
                Next IndiceI
                Return False
            End Get
        End Property

        Public ReadOnly Property SiAccesManagerVerifie(ByVal Ide As Integer, ByVal SiDelegation As Boolean) As Boolean
            Get
                If WsSiManager = False Then
                    Return False
                End If
                Dim FichePER As Virtualia.TablesObjet.ShemaPER.PER_AFFECTATION = Nothing
                Dim IdeVerifie As Integer
                Dim IndiceI As Integer = 0
                Dim StructureAff As String = ""
                FichePER = WsDossierPER.VFiche_Affectation(WsAppGlobal.VirRhDates.DateduJour)
                If FichePER Is Nothing Then
                    WsSiManager = False
                    Return False
                End If
                Do
                    IdeVerifie = 0
                    IndiceI += 1
                    If IndiceI > 5 Then
                        Exit Do
                    End If
                    Select Case IndiceI
                        Case 1
                            StructureAff = FichePER.Structure_de_1er_niveau
                        Case 2
                            StructureAff = FichePER.Structure_de_2e_niveau
                        Case 3
                            StructureAff = FichePER.Structure_de_3e_niveau
                        Case 4
                            StructureAff = FichePER.Structure_de_4e_niveau
                        Case 5
                            StructureAff = FichePER.Structure_de_5e_niveau
                    End Select
                    If StructureAff = "" Then
                        Exit Do
                    End If
                    IdeVerifie = WsAppGlobal.VirIdentifiantManager(FichePER.Ide_Dossier, IndiceI)
                    If IdeVerifie = Ide Then
                        WsNiveauManager = IndiceI
                        Exit Do
                    End If
                    If SiDelegation = True Then
                        IdeVerifie = WsAppGlobal.VirIdentifiantManager(FichePER.Ide_Dossier, IndiceI)
                        If IdeVerifie = Ide Then
                            WsNiveauManager = IndiceI
                            Exit Do
                        End If
                    End If
                Loop
                If IdeVerifie <> Ide Then
                    WsSiManager = False
                End If
                If IndiceI = 1 Then 'Vérification Si Numéro 1
                    WsSiDG = WsAppGlobal.SiEstAussiDG(Ide)
                End If
                Return WsSiManager
            End Get
        End Property

        Public Property SiAccesGRH As Boolean
            Get
                Return WsSiGRH
            End Get
            Set(value As Boolean)
                WsSiGRH = value
            End Set
        End Property

        Public Property UtilisateurV3 As Virtualia.Version3.Utilisateur.UtilisateurV3
            Get
                Return WsUtiV3
            End Get
            Set(value As Virtualia.Version3.Utilisateur.UtilisateurV3)
                WsUtiV3 = value
                If WsUtiV3 Is Nothing Then
                    Exit Property
                End If
                VParent.ActualiserLaConnexion(value.Nom, value.FiltreEtablissementr, RequeteFiltreV3)
            End Set
        End Property

        Private ReadOnly Property RequeteFiltreV3 As String
            Get
                If WsUtiV3 Is Nothing Then
                    Return ""
                End If
                If WsUtiV3.ListeDatabase(0).Filtre_Type(0) = "Sql" Then
                    Return WsUtiV3.ListeDatabase(0).Liste_Filtre_Contenu(0)
                End If
                If WsUtiV3.ListeDatabase(0).Filtre_Objet = 0 Then
                    Return ""
                End If
                Dim Constructeur As Virtualia.Systeme.Sgbd.Sql.SqlInterne
                Dim ChaineSql As String
                Dim Valeurs As System.Text.StringBuilder
                Dim IndiceK As Integer
                Dim DateFin = WsAppGlobal.VirRhDates.DateduJour

                Constructeur = New Virtualia.Systeme.Sgbd.Sql.SqlInterne(WsAppGlobal.VirModele, WsAppGlobal.VirInstanceBd)
                Constructeur.NombredeRequetes(VI.PointdeVue.PVueApplicatif, "", DateFin, VI.Operateurs.ET) = 1
                Constructeur.LettreAlias = "fi"
                Constructeur.SiPasdeTriSurIdeDossier = True
                Constructeur.SiHistoriquedeSituation = False
                Constructeur.SiForcerClauseDistinct = False
                Constructeur.NoInfoSelection(0, CShort(WsUtiV3.ListeDatabase(0).Filtre_Objet)) = CShort(WsUtiV3.ListeDatabase(0).Filtre_Information)
                Valeurs = New System.Text.StringBuilder
                For IndiceK = 0 To WsUtiV3.ListeDatabase(0).Liste_Filtre_Contenu.Count - 1
                    Valeurs.Append(WsUtiV3.ListeDatabase(0).Liste_Filtre_Contenu(IndiceK) & VI.PointVirgule)
                Next IndiceK
                Select Case WsUtiV3.ListeDatabase(0).Filtre_Sens
                    Case "0"
                        Constructeur.ValeuraComparer(0, VI.Operateurs.OU, VI.Operateurs.Egalite, False) = Valeurs.ToString
                    Case "1"
                        Constructeur.ValeuraComparer(0, VI.Operateurs.ET, VI.Operateurs.Egalite, False) = Valeurs.ToString
                End Select

                ChaineSql = Constructeur.OrdreSqlDynamique
                Constructeur = Nothing
                Return ChaineSql
            End Get
        End Property

        Public ReadOnly Property ListeVues_PFR(ByVal AnneeTraitee As Integer, ByVal Regime As String, ByVal CritereTri As String) As List(Of Virtualia.TablesObjet.ShemaVUE.VUE_ATTRIBUTAIRE_PFR)
            Get
                Dim LstVues As List(Of Virtualia.TablesObjet.ShemaVUE.VUE_ATTRIBUTAIRE_PFR) = Nothing
                Dim FichePER As Virtualia.TablesObjet.ShemaPER.PER_AFFECTATION = Nothing
                Dim FicheVUE As Virtualia.TablesObjet.ShemaVUE.VUE_ATTRIBUTAIRE_PFR
                Dim LstRes As List(Of Virtualia.Systeme.MetaModele.VIR_FICHE)
                Dim Constructeur As Virtualia.Systeme.Sgbd.Sql.SqlInterne
                Dim ChaineSql As String
                Dim IndiceI As Integer = 1
                Dim LimiteObjet As Integer

                WsNiveauBlocage = 0
                If SiAccesGRH = False Then
                    IndiceI += WsNiveauManager
                End If

                Constructeur = New Virtualia.Systeme.Sgbd.Sql.SqlInterne(WsAppGlobal.VirModele, WsAppGlobal.VirInstanceBd)
                Constructeur.NombredeRequetes(VI.PointdeVue.PVueVueLogique, "01/01/" & AnneeTraitee, "31/12/" & AnneeTraitee, VI.Operateurs.ET) = IndiceI
                Constructeur.SiPasdeTriSurIdeDossier = False
                Constructeur.SiHistoriquedeSituation = True
                LimiteObjet = 38

                Constructeur.NoInfoSelection(0, 7) = 0
                Constructeur.ValeuraComparer(0, VI.Operateurs.ET, VI.Operateurs.Inclu, False) = "01/01/" & AnneeTraitee & VI.PointVirgule & "31/12/" & AnneeTraitee
                IndiceI = 1

                If SiAccesGRH = False And WsNiveauManager > 0 Then
                    FichePER = WsDossierPER.VFiche_Affectation(WsAppGlobal.VirRhDates.DateduJour)
                End If
                If FichePER IsNot Nothing Then
                    Constructeur.NoInfoSelection(IndiceI, 7) = 9
                    Constructeur.ValeuraComparer(IndiceI, VI.Operateurs.ET, VI.Operateurs.Egalite, False) = FichePER.Structure_de_1er_niveau
                    If FichePER.Structure_de_2e_niveau <> "" And WsNiveauManager > 1 Then
                        IndiceI += 1
                        Constructeur.NoInfoSelection(IndiceI, 7) = 10
                        Constructeur.ValeuraComparer(IndiceI, VI.Operateurs.ET, VI.Operateurs.Egalite, False) = FichePER.Structure_de_2e_niveau
                    End If
                    If FichePER.Structure_de_3e_niveau <> "" And WsNiveauManager > 2 Then
                        IndiceI += 1
                        Constructeur.NoInfoSelection(IndiceI, 7) = 11
                        Constructeur.ValeuraComparer(IndiceI, VI.Operateurs.ET, VI.Operateurs.Egalite, False) = FichePER.Structure_de_3e_niveau
                    End If
                    If FichePER.Structure_de_4e_niveau <> "" And WsNiveauManager > 3 Then
                        IndiceI += 1
                        Constructeur.NoInfoSelection(IndiceI, 7) = 12
                        Constructeur.ValeuraComparer(IndiceI, VI.Operateurs.ET, VI.Operateurs.Egalite, False) = FichePER.Structure_de_4e_niveau
                    End If
                    If FichePER.Structure_de_5e_niveau <> "" And WsNiveauManager > 4 Then
                        IndiceI += 1
                        Constructeur.NoInfoSelection(IndiceI, 7) = 12
                        Constructeur.ValeuraComparer(IndiceI, VI.Operateurs.ET, VI.Operateurs.Egalite, False) = FichePER.Structure_de_5e_niveau
                    End If
                End If
                For IndiceI = 0 To LimiteObjet
                    Constructeur.InfoExtraite(IndiceI, 7, 0) = IndiceI
                Next IndiceI
                ChaineSql = Constructeur.OrdreSqlDynamique
                Constructeur = Nothing

                LstRes = WsAppGlobal.VirServiceServeur.RequeteSql_ToFiches(WsAppGlobal.VirNomUtilisateur, VI.PointdeVue.PVueVueLogique, 7, ChaineSql)
                If LstRes IsNot Nothing Then
                    LstVues = New List(Of Virtualia.TablesObjet.ShemaVUE.VUE_ATTRIBUTAIRE_PFR)
                    For Each VirFiche As Virtualia.Systeme.MetaModele.VIR_FICHE In LstRes
                        FicheVUE = DirectCast(VirFiche, Virtualia.TablesObjet.ShemaVUE.VUE_ATTRIBUTAIRE_PFR)
                        '*** Provisoire ****************************
                        If FicheVUE.Regime_Prime = "PS" Then
                            FicheVUE.TauxMoyen_de_Modulation = 0.04
                        Else
                            FicheVUE.TauxMoyen_de_Modulation = 0.5
                        End If
                        '********************************************
                        If SiAccesGRH = True Then
                            LstVues.Add(FicheVUE)
                        ElseIf WsDossierPER IsNot Nothing AndAlso FicheVUE.Ide_Dossier <> WsDossierPER.Ide_Dossier Then
                            LstVues.Add(FicheVUE)
                        End If
                    Next
                End If
                '*** Si DG ******
                If SiDG = True Then
                    Dim LstDirecteurs As List(Of Integer) = WsAppGlobal.ListeDirecteurs_N1(WsDossierPER.Ide_Dossier)
                    LstRes = WsAppGlobal.SelectionObjetDate(VI.PointdeVue.PVueVueLogique, 7, "01/01/" & AnneeTraitee, "31/12/" & AnneeTraitee, LstDirecteurs)
                    If LstRes IsNot Nothing Then
                        If LstVues Is Nothing Then
                            LstVues = New List(Of Virtualia.TablesObjet.ShemaVUE.VUE_ATTRIBUTAIRE_PFR)
                        End If
                        For Each VirFiche As Virtualia.Systeme.MetaModele.VIR_FICHE In LstRes
                            LstVues.Add(CType(VirFiche, Virtualia.TablesObjet.ShemaVUE.VUE_ATTRIBUTAIRE_PFR))
                        Next
                    End If
                End If
                '****************
                If LstVues Is Nothing Then
                    Return Nothing
                End If
                If SiAccesGRH = False Then
                    Dim LstFiges As List(Of Integer)
                    LstFiges = (From instance In LstVues Order By instance.Niveau_Blocage Descending Select instance.Niveau_Blocage).Distinct.ToList
                    WsNiveauBlocage = LstFiges.Item(0)
                End If
                If Regime = "" Then
                    Regime = "PFR"
                End If
                Select Case CritereTri
                    Case "Par affectation"
                        Return (From instance In LstVues Select instance Where instance.Regime_Prime = Regime
                                Order By instance.Structure_N1, instance.Structure_N2, instance.Structure_N3,
                                                              instance.Structure_N4, instance.Structure_N5,
                                                              instance.Nom Ascending, instance.Prenom Ascending).ToList
                    Case "Par grade"
                        Return (From instance In LstVues Select instance Where instance.Regime_Prime = Regime
                                Order By instance.Grade, instance.Nom Ascending, instance.Prenom Ascending).ToList
                    Case Else
                        Return (From instance In LstVues Select instance Where instance.Regime_Prime = Regime
                                Order By instance.Nom Ascending, instance.Prenom Ascending).ToList
                End Select
            End Get
        End Property

        Public Sub New(ByVal PointeurSession As Virtualia.Net.Session.ObjetSession)
            WsParent = PointeurSession
            WsAppGlobal = CType(PointeurSession.V_PointeurGlobal, Virtualia.Net.Session.ObjetGlobal)
            WsDossierPER = Nothing
        End Sub

    End Class
End Namespace

