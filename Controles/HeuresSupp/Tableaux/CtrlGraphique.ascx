﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="CtrlGraphique.ascx.vb" Inherits="Virtualia.Net.CtrlGraphique" %>

<%--<asp:UpdatePanel ID="eeee" runat="server">
    <ContentTemplate>--%>
<%@ Register Assembly="System.Web.DataVisualization, Version=4.0.0.0,  
   Culture=neutral, PublicKeyToken=31bf3856ad364e35" 
   Namespace="System.Web.UI.DataVisualization.Charting" TagPrefix="asp" %>

<asp:Table ID="TB1" runat="server" Visible="true" Width="1005px" Font-Names="Trebuchet MS" Font-Size="Small" Font-Bold="false" BorderStyle="Ridge" BorderWidth="2px" BorderColor="#B0E0D7" BackColor="#A8BBB8">

    <asp:TableRow>
        <asp:TableCell Height="5px" ColumnSpan="2" />
    </asp:TableRow>

    <asp:TableRow>
        <asp:TableCell HorizontalAlign="Center" ColumnSpan="2">
            <asp:Table ID="TB2" runat="server" Width="700px" HorizontalAlign="Center">
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left" Width="100px" />
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="EtiTitre" runat="server" Text="Graphiques" Height="22px" Width="600px" BackColor="#124545" BorderColor="#B0E0D7" BorderStyle="Notset" BorderWidth="2px" ForeColor="#B0E0D7" Style="font-style: oblique; text-indent: 5px; text-align: center;" />
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Right" Width="100px" />
                </asp:TableRow>
            </asp:Table>
        </asp:TableCell>
    </asp:TableRow>

    <asp:TableRow>
        <asp:TableCell Height="5px" ColumnSpan="2" />
    </asp:TableRow>

    <asp:TableRow>
        <asp:TableCell VerticalAlign="Top">

            <asp:MultiView ID="MultiOnglets" runat="server" ActiveViewIndex="0">
                <asp:View ID="View1" runat="server">
                    <asp:Table ID="TB_SEXE" runat="server" HorizontalAlign="Center" Width="980px">
                        <asp:TableRow>
                            <asp:TableCell BorderStyle="Ridge" BorderWidth="2px" BorderColor="#B0E0D7">
                                <asp:Table ID="TB5" runat="server" >
                                    <asp:TableRow>
                                        <asp:TableCell HorizontalAlign="Center">
                                            <asp:Chart ID="Sexe_NB" runat="server" Palette="SeaGreen" BackColor="WhiteSmoke" Height="250px" Width="250px" BorderlineDashStyle="Solid" BackSecondaryColor="White" BackGradientStyle="TopBottom" BorderWidth="2" BorderColor="26, 59, 105">
                                                <Titles>
                                                    <asp:Title Name="Title2" ShadowColor="32, 0, 0, 0" Font="Trebuchet MS, 10pt, style=Bold" ShadowOffset="3" Text="Bénéficiaires" ForeColor="26, 59, 105" />
                                                </Titles>
                                                <Legends>
                                                    <asp:Legend Name="Default" BackColor="Transparent" Alignment="Center" Docking="Bottom" Font="Trebuchet MS, 8.25pt, style=Bold" IsTextAutoFit="False" LegendStyle="Row" />
                                                </Legends>
                                                <Series>
                                                    <asp:Series Name="Default" ChartType="Doughnut" BorderColor="180, 26, 59, 105" />
                                                </Series>
                                                <ChartAreas>
                                                    <asp:ChartArea Name="ChartArea1" BorderColor="64, 64, 64, 64" BackSecondaryColor="Transparent" BackColor="Transparent" ShadowColor="Transparent" BorderWidth="0">
                                                        <Area3DStyle Rotation="10" />
                                                    </asp:ChartArea>
                                                </ChartAreas>
                                            </asp:Chart>
                                        </asp:TableCell>
                                    </asp:TableRow>
                                </asp:Table>
                            </asp:TableCell>
                        </asp:TableRow>
                    </asp:Table>
                </asp:View>        

            </asp:MultiView>

        </asp:TableCell>
    </asp:TableRow>

</asp:Table>
<%--</ContentTemplate>
</asp:UpdatePanel>--%>
