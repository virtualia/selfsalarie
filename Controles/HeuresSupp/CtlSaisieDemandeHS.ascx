﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="CtlSaisieDemandeHS.ascx.vb" Inherits="Virtualia.Net.CtlSaisieDemandeHS" %>

<%@ Register src="~/Controles/Saisies/VCoupleEtiDonnee.ascx" tagname="VCoupleEtiDonnee" tagprefix="Virtualia" %>
<%@ Register src="~/Controles/Saisies/VTrioHorizontalRadio.ascx" tagname="VOption" tagprefix="Virtualia" %>

<asp:Table ID="CadreControle" runat="server" Height="30px" Width="585px" CellPadding="0" CellSpacing="0" HorizontalAlign="Center">
   <asp:TableRow>
     <asp:TableCell>
       <asp:Table ID="CadreInfo" runat="server" BackColor="#98D4CA" BorderStyle="Ridge" BorderWidth="4px" BorderColor="#B0E0D7" HorizontalAlign="Center" style="margin-top: 3px;">
        <asp:TableRow>
          <asp:TableCell>
            <asp:Table ID="CadreCommandes" runat="server" Height="22px" CellPadding="0" CellSpacing="0"
                Width="122px" HorizontalAlign="Right" style="margin-top: 3px;margin-right: 3px" Visible="true">
                <asp:TableRow>
                    <asp:TableCell VerticalAlign="Bottom">
                        <asp:Table ID="CadreCmdOK" runat="server" BackImageUrl="~/Images/Boutons/OK_Std.bmp" Width="70px"
                            BorderWidth="2px" BorderStyle="Outset" BorderColor="#B0E0D7" ForeColor="#D7FAF3"
                            CellPadding="0" CellSpacing="0" Visible="True">
                            <asp:TableRow>
                                <asp:TableCell>
                                    <asp:Button ID="CommandeOK" runat="server" Text="Valider" Width="65px" Height="20px"
                                        BackColor="Transparent" BorderColor="#B0E0D7" ForeColor="#D7FAF3" 
                                        Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small" Font-Italic="true"
                                        BorderStyle="None" style=" margin-left: 6px; text-align: right;">
                                        </asp:Button>
                                </asp:TableCell>
                            </asp:TableRow>
                        </asp:Table>  
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="right">
                        <asp:Button ID="CmdCancel" runat="server" Height="20px" Width="20px" BackColor="Transparent" ForeColor="Black"
                            BorderStyle="None" BorderColor="#216B68" BorderWidth="1px" Text=" x" ToolTip="Fermer"
                            Font-Bold="true" Font-Names="Trebuchet MS" Font-Size="Medium" Font-Italic="false"
                            style="margin-top: 0px; text-indent:0px; text-align: center; vertical-align: middle" />
                    </asp:TableCell>
                </asp:TableRow>
              </asp:Table>
          </asp:TableCell>
        </asp:TableRow>
        <asp:TableRow>
          <asp:TableCell HorizontalAlign="Center">
              <asp:Label ID="EtiTitreHS" runat="server" Text="Demande de rémunération d'heures supplémentaires" Height="30px" Width="380px"
                    BackColor="#124545" BorderColor="#B0E0D7" BorderStyle="Groove"
                    BorderWidth="2px" ForeColor="#D7FAF3"
                    Font-Bold="True" Font-Names="Trebuchet MS" Font-Size="Small"
                    style="margin-top: 5px; margin-left: 4px; margin-bottom: 20px;
                    font-style: oblique; text-indent: 5px; text-align: center;">
              </asp:Label>   
          </asp:TableCell>
        </asp:TableRow>
        <asp:TableRow>
            <asp:TableCell HorizontalAlign="Center">
                <asp:Label ID="EtiMoisValeur" runat="server" Height="20px" Width="350px" Text="Mois de"
                    BackColor="#B0E0D7" BorderColor="#B0E0D7"  BorderStyle="Outset"
                    BorderWidth="2px" ForeColor="#142425" Font-Italic="true"
                    Font-Bold="True" Font-Names="Trebuchet MS" Font-Size="Small"
                    style="margin-top: 2px; text-indent: 5px; text-align: center">
                </asp:Label>
            </asp:TableCell>
        </asp:TableRow>
        <asp:TableRow>
          <asp:TableCell HorizontalAlign="Center">
             <asp:Table ID="CadreDemande" runat="server" CellPadding="0" CellSpacing="0">
                 <asp:TableRow>
                     <asp:TableCell ColumnSpan="2">
                        <asp:Label ID="EtiChoixRemu" runat="server" Text="Nombre d'heures totales demandées" Height="20px" Width="360px"
                            BackColor="#225C59" BorderColor="#B0E0D7" BorderStyle="Groove"
                            BorderWidth="2px" ForeColor="#D7FAF3" Font-Bold="True" Font-Names="Trebuchet MS" Font-Size="Small"
                            style="margin-top:2px;margin-left:4px;font-style:oblique;text-indent:5px;text-align:center;">
                        </asp:Label>          
                    </asp:TableCell>
                    <asp:TableCell>
                       <Virtualia:VCoupleEtiDonnee ID="InfoH19" runat="server"
                            V_PointdeVue="1" V_Objet="131" V_Information="19" V_SiDonneeDico="true" V_SiAutoPostBack="true"
                            EtiVisible="false" DonWidth="75px" DonTabIndex="3" Donstyle="margin-left:22px;text-align:center;"/>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                     <asp:TableCell HorizontalAlign="Center" ColumnSpan="3">
                        <asp:Label ID="EtiDEcomposition" runat="server" Text="Décomposition déduite du choix effectué" Height="20px" Width="350px"
                            BackColor="#B0E0D7" BorderColor="#B0E0D7" BorderStyle="Outset"
                            BorderWidth="2px" ForeColor="#124545" Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                            style="margin-top:2px;margin-left:4px;text-indent:5px;text-align:center;">
                        </asp:Label>          
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Width="230px"></asp:TableCell>
                    <asp:TableCell>
                        <asp:Label ID="EtiChoixRemu14" runat="server" Text="14 premières heures" Height="20px" Width="140px"
                            BackColor="#B0E0D7" BorderColor="#B0E0D7" BorderStyle="Outset"
                            BorderWidth="2px" ForeColor="#124545" Font-Bold="True" Font-Names="Trebuchet MS" Font-Size="Small"
                            style="margin-top:2px;margin-left:4px;font-style:oblique;text-indent:5px;text-align:center;">
                        </asp:Label>          
                    </asp:TableCell>
                    <asp:TableCell>
                        <asp:Label ID="EtiChoixRemuAuDela14" runat="server" Text="au delà de 14" Height="20px" Width="140px"
                            BackColor="#B0E0D7" BorderColor="#B0E0D7" BorderStyle="Outset"
                            BorderWidth="2px" ForeColor="#124545" Font-Bold="True" Font-Names="Trebuchet MS" Font-Size="Small"
                            style="margin-top:2px;margin-left:2px;font-style:oblique;text-indent:5px;text-align:center;">
                        </asp:Label>          
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell>
                       <asp:Label ID="EtiTotalJour" runat="server" Height="20px" Width="230px" Text="Heures de jour"
                            BackColor="#B0E0D7" BorderColor="#B0E0D7"  BorderStyle="Outset"
                            BorderWidth="2px" ForeColor="#142425" Font-Italic="true"
                            Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                            style="margin-top:2px;margin-left:2px;text-indent:5px;text-align:left">
                       </asp:Label>
                    </asp:TableCell>
                    <asp:TableCell>
                       <Virtualia:VCoupleEtiDonnee ID="InfoH20" runat="server" 
                            V_PointdeVue="1" V_Objet="131" V_Information="20" V_SiDonneeDico="true" V_SiEnLectureSeule="true"
                            EtiVisible="false" DonWidth="75px" DonTabIndex="1" Donstyle="margin-left:22px;text-align:center;"/>
                    </asp:TableCell>
                    <asp:TableCell>
                       <Virtualia:VCoupleEtiDonnee ID="InfoH23" runat="server"
                            V_PointdeVue="1" V_Objet="131" V_Information="23" V_SiDonneeDico="true" V_SiEnLectureSeule="true"
                            EtiVisible="false" DonWidth="75px" DonTabIndex="2" Donstyle="margin-left:22px;text-align:center;"/>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell>
                       <asp:Label ID="EtiTotalNuit" runat="server" Height="20px" Width="230px" Text="Heures de nuit"
                            BackColor="#B0E0D7" BorderColor="#B0E0D7"  BorderStyle="Outset"
                            BorderWidth="2px" ForeColor="#142425" Font-Italic="true"
                            Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                            style="margin-top:2px;margin-left:2px;text-indent:5px;text-align:left">
                       </asp:Label>
                    </asp:TableCell>
                    <asp:TableCell>
                       <Virtualia:VCoupleEtiDonnee ID="InfoH21" runat="server"
                            V_PointdeVue="1" V_Objet="131" V_Information="21" V_SiDonneeDico="true" V_SiEnLectureSeule="true"
                            EtiVisible="false" DonWidth="75px" DonTabIndex="3" Donstyle="margin-left:22px;text-align:center;"/>
                    </asp:TableCell>
                    <asp:TableCell>
                       <Virtualia:VCoupleEtiDonnee ID="InfoH24" runat="server"
                            V_PointdeVue="1" V_Objet="131" V_Information="24" V_SiDonneeDico="true" V_SiEnLectureSeule="true"
                            EtiVisible="false" DonWidth="75px" DonTabIndex="4" Donstyle="margin-left:22px;text-align:center;"/>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell>
                       <asp:Label ID="EtiTotalWeJf" runat="server" Height="20px" Width="230px" Text="Heures de dimanches et jours fériés"
                            BackColor="#B0E0D7" BorderColor="#B0E0D7"  BorderStyle="Outset"
                            BorderWidth="2px" ForeColor="#142425" Font-Italic="true"
                            Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                            style="margin-top:2px;margin-left:2px;text-indent:5px;text-align:left">
                       </asp:Label>
                    </asp:TableCell>
                    <asp:TableCell>
                       <Virtualia:VCoupleEtiDonnee ID="InfoH22" runat="server"
                            V_PointdeVue="1" V_Objet="131" V_Information="22" V_SiDonneeDico="true" V_SiEnLectureSeule="true"
                            EtiVisible="false" DonWidth="75px" DonTabIndex="5" Donstyle="margin-left:22px;text-align:center;"/>
                    </asp:TableCell>
                    <asp:TableCell>
                       <Virtualia:VCoupleEtiDonnee ID="InfoH25" runat="server"
                            V_PointdeVue="1" V_Objet="131" V_Information="25" V_SiDonneeDico="true" V_SiEnLectureSeule="true"
                            EtiVisible="false" DonWidth="75px" DonTabIndex="6" Donstyle="margin-left:22px;text-align:center;"/>
                    </asp:TableCell>
                </asp:TableRow>
                 <asp:TableRow>
                    <asp:TableCell ID="CellSelf" HorizontalAlign="Center" ColumnSpan="3">
                        <asp:Table ID="CadreSelf" runat="server">
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Center">
                                    <asp:Label ID="EtiObsSelf" runat="server" Height="20px" Width="450px" Text="Observations éventuelles"
                                        BackColor="#B0E0D7" BorderColor="#B0E0D7"  BorderStyle="Outset"
                                        BorderWidth="2px" ForeColor="#142425" Font-Italic="true"
                                        Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                        style="margin-top:2px;margin-left:2px;text-indent:5px;text-align:center">
                                   </asp:Label>
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell>
                                    <Virtualia:VCoupleEtiDonnee ID="InfoH10" runat="server"
                                        V_PointdeVue="1" V_Objet="131" V_Information="10" V_SiDonneeDico="true"
                                        EtiVisible="false" DonWidth="450px" DonTabIndex="10" Donstyle="margin-left:2px;text-align:left;"/>
                                </asp:TableCell>
                            </asp:TableRow>
                        </asp:Table>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell ID="CellManager" HorizontalAlign="Center" ColumnSpan="3">
                        <asp:Table ID="CadreManager" runat="server">
                            <asp:TableRow>
                                <asp:TableCell>
                                    <Virtualia:VOption ID="OptionManager" runat="server" RadioDroiteVisible="false"
                                        RadioGaucheText="Accord" RadioCentreText="Refus" RadioGaucheCheck="true" />
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Center">
                                    <asp:Label ID="EtiObsManager" runat="server" Height="20px" Width="450px" Text="Observations éventuelles"
                                        BackColor="#B0E0D7" BorderColor="#B0E0D7"  BorderStyle="Outset"
                                        BorderWidth="2px" ForeColor="#142425" Font-Italic="true"
                                        Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                        style="margin-top:2px;margin-left:2px;text-indent:5px;text-align:center">
                                   </asp:Label>
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell>
                                    <Virtualia:VCoupleEtiDonnee ID="InfoH14" runat="server"
                                        V_PointdeVue="1" V_Objet="131" V_Information="14" V_SiDonneeDico="true"
                                        EtiVisible="false" DonWidth="450px" DonTabIndex="11" Donstyle="margin-left:2px;text-align:left;"/>
                                </asp:TableCell>
                            </asp:TableRow>
                        </asp:Table>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell ID="CellDRH" ColumnSpan="3">
                        <asp:Table ID="CadreDRH" HorizontalAlign="Center" runat="server">
                            <asp:TableRow>
                                <asp:TableCell>
                                    <Virtualia:VOption ID="OptionVisaDRH" runat="server" RadioDroiteVisible="false"
                                        RadioGaucheText="Accord" RadioCentreText="Refus" RadioGaucheCheck="true" />
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Center">
                                    <asp:Label ID="EtiObsDRH" runat="server" Height="20px" Width="450px" Text="Observations éventuelles"
                                        BackColor="#B0E0D7" BorderColor="#B0E0D7"  BorderStyle="Outset"
                                        BorderWidth="2px" ForeColor="#142425" Font-Italic="true"
                                        Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                        style="margin-top:2px;margin-left:2px;text-indent:5px;text-align:center">
                                   </asp:Label>
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell>
                                    <Virtualia:VCoupleEtiDonnee ID="InfoH18" runat="server"
                                        V_PointdeVue="1" V_Objet="131" V_Information="18" V_SiDonneeDico="true"
                                        EtiVisible="false" DonWidth="450px" DonTabIndex="12" Donstyle="margin-left:2px;text-align:left;"/>
                                </asp:TableCell>
                            </asp:TableRow>
                        </asp:Table>
                    </asp:TableCell>
                </asp:TableRow>
            </asp:Table>
          </asp:TableCell>
        </asp:TableRow>
        <asp:TableRow>
            <asp:TableCell Height="10px"></asp:TableCell>
        </asp:TableRow>
       </asp:Table>
     </asp:TableCell>
   </asp:TableRow>
 </asp:Table>

