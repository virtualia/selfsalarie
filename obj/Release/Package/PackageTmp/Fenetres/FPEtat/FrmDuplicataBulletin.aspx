﻿<%@ Page Language="vb" AutoEventWireup="false" MasterPageFile="~/PagesMaitres/VirtualiaMain.Master"  
    CodeBehind="FrmDuplicataBulletin.aspx.vb" Inherits="Virtualia.Net.FrmDuplicataBulletin" MaintainScrollPositionOnPostback="True" %>

<%@ MasterType VirtualPath="~/PagesMaitres/VirtualiaMain.master"  %>
<%@ Register src="~/Controles/FPEtat/VBulletinPaie.ascx" tagname="VBulPaie" tagprefix="Virtualia" %>

<asp:Content ID="Corps" ContentPlaceHolderID="Principal" runat="server">
    <asp:Table ID="CadreGeneral" runat="server" HorizontalAlign="Center" Width="1050px">
         <asp:TableRow>
            <asp:TableCell>
                <asp:Panel ID="CadreAffichage" runat="server" BackColor="Transparent" HorizontalAlign="Center" Width="1050px"
                     BackImageUrl="~/Images/Fonds/Fond_Saisie.jpg" BorderStyle="None">
                    <asp:Table ID="CadreBulPaie" runat="server" Width="1040px" HorizontalAlign="Center" borderStyle="None">
                        <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Center" VerticalAlign="Top">
                                <Virtualia:VBulPaie ID="Bulletin" runat="server" />
                            </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow Height="40px">
                            <asp:TableCell VerticalAlign="Top" ColumnSpan="2">
                                <asp:Label ID="EtiObservation" runat="server" Height="20px" Width="1050px" 
                                    Text="Ce document est un fac-similé. Le bulletin de paie est reconstitué par Virtualia.Net à partir des données numériques ayant permis l'édition du bulletin original."
                                    BackColor="Transparent" BorderColor="#B6C7E2"  BorderStyle="None" ForeColor="#124545"
                                    Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Smaller"
                                    style="margin-left: 4px; margin-bottom: 10px; text-align: left;" >
                                </asp:Label>          
                            </asp:TableCell>
                        </asp:TableRow>
                    </asp:Table>
                </asp:Panel>
            </asp:TableCell>
        </asp:TableRow>
    </asp:Table>
</asp:Content>