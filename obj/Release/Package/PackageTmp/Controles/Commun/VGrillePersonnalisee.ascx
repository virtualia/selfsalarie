﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="VGrillePersonnalisee.ascx.vb" Inherits="Virtualia.Net.VGrillePersonnalisee" %>

<style type="text/css">
    .LigneTable
    {
        border-style:none;
        margin-top:0px;
        position:static;
        z-index:99;
    }

    .CellEtiquette
    {
        background-color:#225C59;
        border-color:#124545;
        border-style:solid;
        border-width:1px;
    }
    .Etiquette
    {
        background-color:transparent;
        border-style:none;
        color:white;
        height:35px;
        font-family:'Trebuchet MS';
        font-size:small;
        font-style:italic;
        font-weight:bold;
        margin-top:1px;
        padding-top: 2px;
        text-indent:2px;
        text-align:center;
        word-wrap:normal;
    }
     .CellDonnee
    {
        background-color:white;
        border-color:#124545;
        border-bottom-style:solid;
        border-left-style:solid;
        border-right-style:solid;
        border-top-style:none;
        border-width:1px;
        height:20px;
        margin-bottom:0px;
        margin-top:0px;
        vertical-align:top;
    }
    .Donnee
    {
        background-color:transparent;
        border-style:none;
        color:black;
        height:16px;
        font-family:'Trebuchet MS';
        font-size:small;
        font-style:normal;
        padding-top:2px;
        text-indent:2px;
        text-align:left;
    }
</style>
<asp:Panel ID="PanelGrille" runat="server" Width="1050px" ScrollBars="Auto" HorizontalAlign="Center" BackColor="#B0E0D7">
    <asp:Table ID="CadreGrille" runat="server" HorizontalAlign="Center" CellPadding="0" CellSpacing="0">
        <asp:TableRow>
           <asp:TableCell>
               <asp:Table ID="CadreEtiquettes" runat="server" HorizontalAlign="Center" CellPadding="0" CellSpacing="0">
                   <asp:TableRow>
                        <asp:TableCell ID="CellNeutre" Width="24px" Height="20px" HorizontalAlign="Center">
                            <asp:Label ID="EtiNeutre" runat="server" Width="22px" Text="" />
                        </asp:TableCell>
                        <asp:TableCell ID="CellEti1" Width="141px" CssClass="CellEtiquette">
                            <asp:Label ID="EtiCol1" runat="server" CssClass="Etiquette" Width="130px" Text="Virtualia" />
                        </asp:TableCell>
                       <asp:TableCell ID="CellEti2" Width="140px" CssClass="CellEtiquette">
                            <asp:Label ID="EtiCol2" runat="server" CssClass="Etiquette" Width="130px" Text="Virtualia" />
                        </asp:TableCell>
                       <asp:TableCell ID="CellEti3" Width="140px" CssClass="CellEtiquette">
                            <asp:Label ID="EtiCol3" runat="server" CssClass="Etiquette" Width="130px" Text="Virtualia" />
                        </asp:TableCell>
                       <asp:TableCell ID="CellEti4" Width="100px" CssClass="CellEtiquette">
                            <asp:Label ID="EtiCol4" runat="server" CssClass="Etiquette" Width="90px" Text="Virtualia" />
                        </asp:TableCell>
                       <asp:TableCell ID="CellEti5" Width="100px" CssClass="CellEtiquette">
                            <asp:Label ID="EtiCol5" runat="server" CssClass="Etiquette" Width="90px" Text="Virtualia" />
                        </asp:TableCell>
                       <asp:TableCell ID="CellEti6" Width="100px" CssClass="CellEtiquette">
                            <asp:Label ID="EtiCol6" runat="server" CssClass="Etiquette" Width="90px" Text="Virtualia" />
                        </asp:TableCell>
                        <asp:TableCell ID="CellEti7" Width="100px" CssClass="CellEtiquette">
                            <asp:Label ID="EtiCol7" runat="server" CssClass="Etiquette" Width="90px" Text="Virtualia" />
                        </asp:TableCell>
                       <asp:TableCell ID="CellEti8" Width="100px" CssClass="CellEtiquette">
                            <asp:Label ID="EtiCol8" runat="server" CssClass="Etiquette" Width="90px" Text="Virtualia" />
                        </asp:TableCell>
                        <asp:TableCell ID="CellEti9" Width="100px" CssClass="CellEtiquette">
                            <asp:Label ID="EtiCol9" runat="server" CssClass="Etiquette" Width="90px" Text="Virtualia" />
                        </asp:TableCell>
                   </asp:TableRow>
               </asp:Table>
           </asp:TableCell>
       </asp:TableRow>
       <asp:TableRow>
           <asp:TableCell>
               <asp:Table ID="Ligne01" runat="server" CssClass="LigneTable" HorizontalAlign="Center" CellPadding="0" CellSpacing="0">
                   <asp:TableRow VerticalAlign="Top">
                        <asp:TableCell ID="CellCmd_L01" Width="24px" CssClass="CellDonnee">
                            <asp:ImageButton ID="CmdCol_L01" runat="server" Width="22px" Height="19px" 
                                 ImageUrl="~/Images/General/CarreUniversel.bmp" ImageAlign="Middle" style="margin-left: 1px;" Tooltip="Mettre à jour la fiche" />
                        </asp:TableCell>
                        <asp:TableCell ID="CellDon_L01_1" Width="140px" CssClass="CellDonnee">
                            <asp:TextBox ID="DonCol_L01_1" runat="server" CssClass="Donnee" Width="130px" Text="Affichage" ToolTip=""
                                ReadOnly="true" AutoPostBack="false" TextMode="SingleLine" > 
                            </asp:TextBox>
                        </asp:TableCell>
                        <asp:TableCell ID="CellDon_L01_2" Width="140px" CssClass="CellDonnee">
                            <asp:TextBox ID="DonCol_L01_2" runat="server" CssClass="Donnee" Width="130px" Text="Affichage" ToolTip=""
                                ReadOnly="true" AutoPostBack="false" TextMode="SingleLine" >  
                            </asp:TextBox>
                        </asp:TableCell>
                        <asp:TableCell ID="CellDon_L01_3" Width="140px" CssClass="CellDonnee">
                            <asp:TextBox ID="DonCol_L01_3" runat="server" CssClass="Donnee" Width="130px" Text="Affichage" ToolTip=""
                                ReadOnly="true" AutoPostBack="false" TextMode="SingleLine" > 
                            </asp:TextBox>
                        </asp:TableCell>
                        <asp:TableCell ID="CellDon_L01_4" Width="100px" CssClass="CellDonnee">
                            <asp:TextBox ID="DonCol_L01_4" runat="server" CssClass="Donnee" Width="90px" Text="Affichage" ToolTip=""
                                ReadOnly="true" AutoPostBack="false" TextMode="SingleLine" >  
                            </asp:TextBox>
                        </asp:TableCell>
                        <asp:TableCell ID="CellDon_L01_5" Width="100px" CssClass="CellDonnee">
                            <asp:TextBox ID="DonCol_L01_5" runat="server" CssClass="Donnee" Width="90px" Text="Affichage" ToolTip=""
                                ReadOnly="true" AutoPostBack="false" TextMode="SingleLine" > 
                            </asp:TextBox>
                        </asp:TableCell>
                        <asp:TableCell ID="CellDon_L01_6" Width="100px" CssClass="CellDonnee">
                            <asp:TextBox ID="DonCol_L01_6" runat="server" CssClass="Donnee" Width="90px" Text="Affichage" ToolTip=""
                                ReadOnly="true" AutoPostBack="false" TextMode="SingleLine" > 
                            </asp:TextBox>
                        </asp:TableCell>
                       <asp:TableCell ID="CellDon_L01_7" Width="100px" CssClass="CellDonnee">
                            <asp:TextBox ID="DonCol_L01_7" runat="server" CssClass="Donnee" Width="90px" Text="Affichage" ToolTip=""
                                ReadOnly="true" AutoPostBack="false" TextMode="SingleLine" > 
                            </asp:TextBox>
                        </asp:TableCell>
                        <asp:TableCell ID="CellDon_L01_8" Width="100px" CssClass="CellDonnee">
                            <asp:TextBox ID="DonCol_L01_8" runat="server" CssClass="Donnee" Width="90px" Text="Affichage" ToolTip=""
                                ReadOnly="true" AutoPostBack="false" TextMode="SingleLine" > 
                            </asp:TextBox>
                        </asp:TableCell>
                        <asp:TableCell ID="CellDon_L01_9" Width="100px" CssClass="CellDonnee">
                            <asp:TextBox ID="DonCol_L01_9" runat="server" CssClass="Donnee" Width="90px" Text="Affichage" ToolTip=""
                                ReadOnly="true" AutoPostBack="false" TextMode="SingleLine" > 
                            </asp:TextBox>
                        </asp:TableCell>
                    </asp:TableRow>
               </asp:Table>
           </asp:TableCell>
       </asp:TableRow>
        
    </asp:Table>
</asp:Panel>