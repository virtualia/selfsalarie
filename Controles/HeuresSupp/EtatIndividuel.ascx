﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="EtatIndividuel.ascx.vb" Inherits="Virtualia.Net.EtatIndividuel" %>

<%@ Register Src="~/Controles/Saisies/VCoupleEtiDonnee.ascx" TagName="VCoupleEtiDonnee" TagPrefix="Virtualia" %>
<%@ Register Src="~/Controles/Commun/VListeGrid.ascx" TagName="VListeGrid" TagPrefix="Virtualia" %>

<style type="text/css">
    .Gen_Titre {
        background-color: #216B68;
        color: #D7FAF3;
        border-style: none;
        font-family: 'Trebuchet MS';
        font-size: medium;
        font-style: normal;
        font-weight: bold;
        text-indent: 0px;
        text-align: center;
        margin-top: 1px;
        margin-left: 0px;
        margin-bottom: 0px;
        word-wrap: initial;
        height: 25px;
        width: 746px;
    }

    .Gen_TitreChapitre {
        background-color: #CAEBE4;
        color: #124545;
        border-style: none;
        font-family: 'Trebuchet MS';
        font-size: medium;
        font-style: normal;
        font-weight: normal;
        text-indent: 0px;
        text-align: center;
        margin-top: 5px;
        margin-left: 0px;
        margin-bottom: 0px;
        word-wrap: normal;
        height: 28px;
        width: 746px;
    }

    .Gen_EtiInfo {
        background-color: #E2FDF7;
        color: #124545;
        border-style: unset;
        border-width: 1px;
        font-family: 'Trebuchet MS';
        font-size: small;
        font-style: italic;
        font-weight: normal;
        text-indent: 5px;
        text-align: center;
        margin-top: 1px;
        margin-left: 0px;
        margin-bottom: 5px;
        word-wrap: normal;
        height: 18px;
        width: 710px;
    }
</style>

<asp:Table ID="CadreInfo" runat="server" BorderStyle="None" BorderWidth="2px" Visible="true"
    BorderColor="#B0E0D7" Width="750px" HorizontalAlign="Center" Style="margin-top: 1px;">
    <asp:TableRow>
        <asp:TableCell>
            <asp:Table ID="CadreTitreGeneral" runat="server" Height="50px" CellPadding="0" Width="750px"
                CellSpacing="0" HorizontalAlign="Center" BorderStyle="None" BorderWidth="1px" Visible="true" BorderColor="#124545">
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="EtiTitre" runat="server" Text="ETAT MENSUEL DES ASTREINTES PAR AGENT" Height="25px" Width="746px" CssClass="Gen_Titre">
                        </asp:Label>
                    </asp:TableCell>
                </asp:TableRow>
            </asp:Table>
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell>
            <asp:Table ID="CadreEtatCivil" runat="server" BackColor="#E2F5F1" CellPadding="0" CellSpacing="0" HorizontalAlign="Center">
                <asp:TableRow>
                    <asp:TableCell Width="710px" HorizontalAlign="Left" ColumnSpan="1">
                        <asp:Label ID="EtiMois" runat="server" Height="20px" Width="350px" Text="Mois de :"
                            BackColor="#B0E0D7" BorderColor="#B0E0D7" BorderStyle="Outset"
                            BorderWidth="2px" ForeColor="#142425" Font-Italic="true"
                            Font-Bold="True" Font-Names="Trebuchet MS" Font-Size="Small"
                            Style="margin-top: 2px; text-indent: 5px; text-align: center">
                        </asp:Label>
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:TextBox ID="Mois" runat="server" CssClass="Std_Donnee" Width="180px" TextMode="SingleLine"
                            Style="text-indent: 5px; text-align: center">
                        </asp:TextBox>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Width="710px" HorizontalAlign="Left" ColumnSpan="1">
                        <asp:Label ID="EtiJuridiction" runat="server" Height="20px" Width="350px" Text="Juridiction :"
                            BackColor="#B0E0D7" BorderColor="#B0E0D7" BorderStyle="Outset"
                            BorderWidth="2px" ForeColor="#142425" Font-Italic="true"
                            Font-Bold="True" Font-Names="Trebuchet MS" Font-Size="Small"
                            Style="margin-top: 2px; text-indent: 5px; text-align: center">
                        </asp:Label>
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:TextBox ID="Juridiction" runat="server" CssClass="Std_Donnee" Width="180px" TextMode="SingleLine"
                            Style=" text-indent: 5px; text-align: center">
                        </asp:TextBox>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Width="710px" HorizontalAlign="Left" ColumnSpan="1">
                        <asp:Label ID="EtiNomEtPrenom" runat="server" Height="20px" Width="350px" Text="NOM - Prénom :"
                            BackColor="#B0E0D7" BorderColor="#B0E0D7" BorderStyle="Outset"
                            BorderWidth="2px" ForeColor="#142425" Font-Italic="true"
                            Font-Bold="True" Font-Names="Trebuchet MS" Font-Size="Small"
                            Style="margin-top: 2px; text-indent: 5px; text-align: center">
                        </asp:Label>
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:TextBox ID="NomEtPrenom" runat="server" CssClass="Std_Donnee" Width="180px" TextMode="SingleLine"
                            Style=" text-indent: 5px; text-align: center">
                        </asp:TextBox>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Width="710px" HorizontalAlign="Left" ColumnSpan="1">
                        <asp:Label ID="EtiFonctions" runat="server" Height="20px" Width="350px" Text="Fonctions :"
                            BackColor="#B0E0D7" BorderColor="#B0E0D7" BorderStyle="Outset"
                            BorderWidth="2px" ForeColor="#142425" Font-Italic="true"
                            Font-Bold="True" Font-Names="Trebuchet MS" Font-Size="Small"
                            Style="margin-top: 2px; text-indent: 5px; text-align: center">
                        </asp:Label>
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:TextBox ID="Fonctions" runat="server" CssClass="Std_Donnee" Width="180px" TextMode="SingleLine"
                            Style=" text-indent: 5px; text-align: center">
                        </asp:TextBox>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Width="710px" HorizontalAlign="Left" ColumnSpan="1">
                        <asp:Label ID="EtiServiceAstreinte" runat="server" Height="20px" Width="350px" Text="Service d'astreinte :"
                            BackColor="#B0E0D7" BorderColor="#B0E0D7" BorderStyle="Outset"
                            BorderWidth="2px" ForeColor="#142425" Font-Italic="true"
                            Font-Bold="True" Font-Names="Trebuchet MS" Font-Size="Small"
                            Style="margin-top: 2px; text-indent: 5px; text-align: center">
                        </asp:Label>
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:TextBox ID="ServiceAstreinte" runat="server" CssClass="Std_Donnee" Width="180px" TextMode="SingleLine"
                            Style=" text-indent: 5px; text-align: center">
                        </asp:TextBox>
                    </asp:TableCell>
                </asp:TableRow>
            </asp:Table>
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell>
            <asp:Table ID="CadreTitreChapitre" runat="server" Height="50px" CellPadding="0" Width="750px"
                CellSpacing="0" HorizontalAlign="Center" BorderStyle="None" BorderWidth="1px" Visible="true" BorderColor="#124545">
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="EtiTitreChapitre" runat="server" Text="JOUR ET DATE D'ASTREINTES" Height="25px" Width="746px" CssClass="Gen_TitreChapitre">
                        </asp:Label>
                    </asp:TableCell>
                </asp:TableRow>
            </asp:Table>
        </asp:TableCell>
    </asp:TableRow>
                        <asp:TableRow>
                    <asp:TableCell>
                        <Virtualia:VListeGrid ID="ListeGrille" runat="server" CadreWidth="630px" SiPagination="true" TaillePage="10" SiColonneSelect="true" SiCaseAcocher="false" />
                    </asp:TableCell>
                </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell>
            <asp:Table ID="CadreAstreinte" runat="server" BackColor="#E2F5F1" CellPadding="0" CellSpacing="0" HorizontalAlign="Center">
                <asp:TableRow>
                    <asp:TableCell Width="710px" HorizontalAlign="Left" ColumnSpan="1">
                        <asp:Label ID="EtiNombreAstreinte" runat="server" Height="20px" Width="350px" Text="Nombre d'astreintes réalisées dans le mois :"
                            BackColor="#B0E0D7" BorderColor="#B0E0D7" BorderStyle="Outset"
                            BorderWidth="2px" ForeColor="#142425" Font-Italic="true"
                            Font-Bold="True" Font-Names="Trebuchet MS" Font-Size="Small"
                            Style="margin-top: 2px; text-indent: 5px; text-align: center">
                        </asp:Label>
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:TextBox ID="NombreAstreinte" runat="server" CssClass="Std_Donnee" Width="180px" TextMode="SingleLine"
                            Style=" text-indent: 5px; text-align: center">
                        </asp:TextBox>
                    </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell Width="710px" HorizontalAlign="Left" ColumnSpan="1">
                        <asp:Label ID="EtiRemuneration" runat="server" Height="36px" Width="350px" Text="Rémunération pour le mois :(50 euros par astreinte, plafond de 500 euros par mois)"
                            BackColor="#B0E0D7" BorderColor="#B0E0D7" BorderStyle="Outset"
                            BorderWidth="2px" ForeColor="#142425" Font-Italic="true"
                            Font-Bold="True" Font-Names="Trebuchet MS" Font-Size="Small"
                            Style="margin-top: 2px; text-indent: 5px; text-align: center">
                        </asp:Label>
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Left">
                        <asp:TextBox ID="Remuneration" runat="server" CssClass="Std_Donnee" Width="180px" TextMode="SingleLine"
                            Style=" text-indent: 5px; text-align: center">
                        </asp:TextBox>
                    </asp:TableCell>
                </asp:TableRow>
            </asp:Table>
        </asp:TableCell>
    </asp:TableRow>
        <asp:TableRow>
        <asp:TableCell Height="5px" ColumnSpan="1" />
            </asp:TableRow>
                    <asp:TableRow>
                    <asp:TableCell Width="710px" HorizontalAlign="Left" ColumnSpan="1">
                        <asp:Label ID="DateEtat" runat="server" Height="20px" Width="350px" Text="Etat dressé le :"
                            BackColor="#B0E0D7" BorderColor="#B0E0D7" BorderStyle="Outset"
                            BorderWidth="2px" ForeColor="#142425" Font-Italic="true"
                            Font-Bold="True" Font-Names="Trebuchet MS" Font-Size="Small"
                            Style="margin-top: 2px; text-indent: 5px; text-align: center">
                        </asp:Label>
                    </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow>
                    <asp:TableCell Width="710px" HorizontalAlign="Left" ColumnSpan="1">
                        <asp:Label ID="DirecteurGreffe" runat="server" Height="20px" Width="350px" Text="Le directeur/la directrice de greffe :"
                            BackColor="#B0E0D7" BorderColor="#B0E0D7" BorderStyle="Outset"
                            BorderWidth="2px" ForeColor="#142425" Font-Italic="true"
                            Font-Bold="True" Font-Names="Trebuchet MS" Font-Size="Small"
                            Style="margin-top: 2px; text-indent: 5px; text-align: center">
                        </asp:Label>
                        <asp:Button ID ="PDF" runat="server" Text ="AperçuPDF" /> 
                    </asp:TableCell>

                        </asp:TableRow>

</asp:Table>
