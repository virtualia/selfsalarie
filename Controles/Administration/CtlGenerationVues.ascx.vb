﻿Option Strict On
Option Explicit On
Option Compare Text
Imports VI = Virtualia.Systeme.Constantes
Public Class CtlGenerationVues
    Inherits System.Web.UI.UserControl
    Private WebFct As Virtualia.Net.Controles.WebFonctions
    Private WsNomStateCmd As String = "GenererCmd"
    Private WsNomStateIde As String = "GenererIde"

    Public WriteOnly Property V_Perimetre As String
        Set(value As String)
            Dim Cpt As Integer = 0
            Select Case Strings.Left(value, 1)
                Case "1"
                    CommandeN1.Visible = True
                    Cpt += 1
                Case Else
                    CommandeN1.Visible = False
            End Select
            Select Case Strings.Mid(value, 2, 1)
                Case "1"
                    CommandeN2.Visible = True
                    Cpt += 1
                Case Else
                    CommandeN2.Visible = False
            End Select
            Select Case Strings.Mid(value, 3, 1)
                Case "1"
                    CommandeN3.Visible = True
                    Cpt += 1
                Case Else
                    CommandeN3.Visible = False
            End Select
            CadreCmdStd.Width = New Unit(150 * Cpt)
        End Set
    End Property

    Private Property V_Commande As String
        Get
            Dim VCache As String

            VCache = CStr(Me.ViewState(WsNomStateCmd))
            If VCache Is Nothing Then
                Return ""
            End If
            Return VCache
        End Get
        Set(ByVal value As String)
            If Me.ViewState(WsNomStateCmd) IsNot Nothing Then
                Me.ViewState.Remove(WsNomStateCmd)
            End If
            Me.ViewState.Add(WsNomStateCmd, value)
        End Set
    End Property

    Private Property V_ListeIde As List(Of Integer)
        Get
            Dim VCache As List(Of Integer)

            VCache = CType(Me.ViewState(WsNomStateIde), List(Of Integer))
            If VCache Is Nothing Then
                Return Nothing
            End If
            Return VCache
        End Get
        Set(ByVal value As List(Of Integer))
            If Me.ViewState(WsNomStateIde) IsNot Nothing Then
                Me.ViewState.Remove(WsNomStateIde)
            End If
            Me.ViewState.Add(WsNomStateIde, value)
        End Set
    End Property

    Private Sub Page_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init
        WebFct = New Virtualia.Net.Controles.WebFonctions(Me, 0)
    End Sub

    Private Sub Page_PreRender(sender As Object, e As System.EventArgs) Handles Me.PreRender
        If CInt(HNumPage.Value) = 0 Then
            Exit Sub
        End If
        Select Case V_Commande
            Case "FIC"
                Call ExecuterImport_PFR()
            Case "PFR"
                Call ExecuterPrepa_PFR()
            Case "ORG"
                Call ExecuterPrepa_ORG()
            Case "CAR"
                Call ExecuterPrepa_CAR()
        End Select
    End Sub

    Private Sub ExecuterPrepa_PFR()
        Dim WseMetier As Virtualia.Net.WebService.VirtualiaWcfMetier
        Dim Cretour As Boolean
        Dim HeureDeb As Date
        Dim HeureFin As Date

        If WebFct Is Nothing Then
            WebFct = New Virtualia.Net.Controles.WebFonctions(Me, 0)
        End If
        If CType(WebFct.PointeurUtilisateur.V_PointeurSessionModule, Virtualia.Net.Session.SessionModule).SiEnExecution = True Then
            Exit Sub
        End If
        CType(WebFct.PointeurUtilisateur.V_PointeurSessionModule, Virtualia.Net.Session.SessionModule).SiEnExecution = True
        Select Case CInt(HNumPage.Value)
            Case 1
                HNumPage.Value = "2"
                HeureDeb = System.DateTime.Now
                EtiTraitement.Text = "Exécution de la préparation des VUES PFR - Obtention des dossiers concernés"
                EtiHDebut.Text = HeureDeb.ToString("dd/MM/yyyy hh:mm:ss")

                WseMetier = New Virtualia.Net.WebService.VirtualiaWcfMetier
                Cretour = WseMetier.Prime.PreparationVUES_PFR_Lecture(Year(Now))

            Case 2
                HNumPage.Value = "3"
                HeureDeb = System.DateTime.Now
                EtiTraitement.Text = "Exécution de la préparation des VUES PFR - Suppression Vues"
                EtiHDebut.Text = HeureDeb.ToString("dd/MM/yyyy hh:mm:ss")

                WseMetier = New Virtualia.Net.WebService.VirtualiaWcfMetier
                Cretour = WseMetier.Prime.PreparationVUES_PFR_Supprimer

            Case 3
                HNumPage.Value = "4"
                EtiTraitement.Text = "Exécution de la préparation des VUES PFR - Générer"

                WseMetier = New Virtualia.Net.WebService.VirtualiaWcfMetier
                V_ListeIde = WseMetier.Prime.PreparationVUES_PFR_ListeDossiers

            Case 4 To 100
                Dim LstIde As List(Of Integer) = V_ListeIde
                Dim TabIde As List(Of Integer)
                Dim IndiceK As Integer
                If (CInt(HNumPage.Value) - 4) * 200 > LstIde.Count - 1 Then
                    HNumPage.Value = "101"
                    EtiTraitement.Text = "Exécution de la préparation des VUES PFR - Traitement terminé"
                    CType(WebFct.PointeurUtilisateur.V_PointeurSessionModule, Virtualia.Net.Session.SessionModule).SiEnExecution = False
                    Exit Sub
                End If
                TabIde = New List(Of Integer)
                For IndiceK = (CInt(HNumPage.Value) - 4) * 200 To ((CInt(HNumPage.Value) - 3) * 200) - 1
                    If IndiceK > LstIde.Count - 1 Then
                        Exit For
                    End If
                    TabIde.Add(LstIde(IndiceK))
                Next IndiceK
                HNumPage.Value = CStr(CInt(HNumPage.Value) + 1)
                EtiTraitement.Text = "Exécution de la préparation des VUES PFR - Générer - " & CStr((CInt(HNumPage.Value) - 4) * 200)

                WseMetier = New Virtualia.Net.WebService.VirtualiaWcfMetier
                Cretour = WseMetier.Prime.PreparationVUES_PFR_Generer(TabIde)

            Case 101
                HNumPage.Value = "999"

                WseMetier = New Virtualia.Net.WebService.VirtualiaWcfMetier
                Cretour = WseMetier.Prime.PreparationVUES_Finaliser

                HeureFin = System.DateTime.Now
                EtiHFin.Text = HeureFin.ToString("dd/MM/yyyy hh:mm:ss")
            Case Else
                HNumPage.Value = "0"
                TimerExec.Enabled = False
        End Select
        CType(WebFct.PointeurUtilisateur.V_PointeurSessionModule, Virtualia.Net.Session.SessionModule).SiEnExecution = False
    End Sub

    Private Sub ExecuterPrepa_ORG()
        Dim WseMetier As Virtualia.Net.WebService.VirtualiaWcfMetier
        Dim Cretour As Boolean
        Dim HeureDeb As Date
        Dim HeureFin As Date

        If WebFct Is Nothing Then
            WebFct = New Virtualia.Net.Controles.WebFonctions(Me, 0)
        End If
        If CType(WebFct.PointeurUtilisateur.V_PointeurSessionModule, Virtualia.Net.Session.SessionModule).SiEnExecution = True Then
            Exit Sub
        End If
        CType(WebFct.PointeurUtilisateur.V_PointeurSessionModule, Virtualia.Net.Session.SessionModule).SiEnExecution = True
        Select Case CInt(HNumPage.Value)
            Case 1
                HNumPage.Value = "2"
                HeureDeb = System.DateTime.Now
                EtiTraitement.Text = "Exécution de la préparation des VUES Annuaire - Lecture Affectations"
                EtiHDebut.Text = HeureDeb.ToString("dd/MM/yyyy hh:mm:ss")

                WseMetier = New Virtualia.Net.WebService.VirtualiaWcfMetier
                Cretour = WseMetier.Annuaire.PreparationVUESAnnuaire_Lecture("A")

            Case 2
                HNumPage.Value = "3"
                EtiTraitement.Text = "Exécution de la préparation des VUES Annuaire - Suppression des vues"

                WseMetier = New Virtualia.Net.WebService.VirtualiaWcfMetier
                Cretour = WseMetier.Annuaire.PreparationVUESAnnuaire_Lecture("B")

            Case 3
                HNumPage.Value = "4"
                EtiTraitement.Text = "Exécution de la préparation des VUES Annuaire - Obtention des dossiers concernés"

                WseMetier = New Virtualia.Net.WebService.VirtualiaWcfMetier
                Cretour = WseMetier.Annuaire.PreparationVUESAnnuaire_Supprimer

            Case 4
                HNumPage.Value = "5"
                EtiTraitement.Text = "Exécution de la préparation des VUES Annuaire - Générer - Début"

                WseMetier = New Virtualia.Net.WebService.VirtualiaWcfMetier
                V_ListeIde = WseMetier.Annuaire.PreparationVUESAnnuaire_ListeDossiers

            Case 5 To 100
                Dim LstIde As List(Of Integer) = V_ListeIde
                Dim TabIde As List(Of Integer)
                Dim IndiceK As Integer
                If (CInt(HNumPage.Value) - 5) * 200 > LstIde.Count - 1 Then
                    HNumPage.Value = "101"
                    EtiTraitement.Text = "Exécution de la préparation des VUES Annuaire - Traitement terminé"
                    CType(WebFct.PointeurUtilisateur.V_PointeurSessionModule, Virtualia.Net.Session.SessionModule).SiEnExecution = False
                    Exit Sub
                End If
                TabIde = New List(Of Integer)
                For IndiceK = (CInt(HNumPage.Value) - 5) * 200 To ((CInt(HNumPage.Value) - 4) * 200) - 1
                    If IndiceK > LstIde.Count - 1 Then
                        Exit For
                    End If
                    TabIde.Add(LstIde(IndiceK))
                Next IndiceK
                HNumPage.Value = CStr(CInt(HNumPage.Value) + 1)
                EtiTraitement.Text = "Exécution de la préparation des VUES Annuaire - Générer - " & CStr((CInt(HNumPage.Value) - 5) * 200)

                WseMetier = New Virtualia.Net.WebService.VirtualiaWcfMetier
                Cretour = WseMetier.Annuaire.PreparationVUESAnnuaire_Generer(TabIde)

            Case 101
                HNumPage.Value = "999"

                WseMetier = New Virtualia.Net.WebService.VirtualiaWcfMetier
                Cretour = WseMetier.Annuaire.PreparationVUESAnnuaire_Finaliser

                HeureFin = System.DateTime.Now
                EtiHFin.Text = HeureFin.ToString("dd/MM/yyyy hh:mm:ss")
            Case Else
                HNumPage.Value = "0"
                TimerExec.Enabled = False
        End Select
        CType(WebFct.PointeurUtilisateur.V_PointeurSessionModule, Virtualia.Net.Session.SessionModule).SiEnExecution = False
    End Sub

    Private Sub ExecuterPrepa_CAR()
        Dim WseMetier As Virtualia.Net.WebService.VirtualiaWcfMetier
        Dim Cretour As Boolean
        Dim HeureDeb As Date
        Dim HeureFin As Date

        If WebFct Is Nothing Then
            WebFct = New Virtualia.Net.Controles.WebFonctions(Me, 0)
        End If
        If CType(WebFct.PointeurUtilisateur.V_PointeurSessionModule, Virtualia.Net.Session.SessionModule).SiEnExecution = True Then
            Exit Sub
        End If
        CType(WebFct.PointeurUtilisateur.V_PointeurSessionModule, Virtualia.Net.Session.SessionModule).SiEnExecution = True
        Select Case CInt(HNumPage.Value)
            Case 1
                HeureDeb = System.DateTime.Now
                EtiTraitement.Text = "Exécution de la préparation des VUES - CARRIERE Lecture Situations de détachement"
                EtiHDebut.Text = HeureDeb.ToString("dd/MM/yyyy hh:mm:ss")
                HNumPage.Value = "2"

                WseMetier = New Virtualia.Net.WebService.VirtualiaWcfMetier
                Cretour = WseMetier.Carriere.PreparationVUES_Lecture("V")

            Case 2
                HNumPage.Value = "3"
                EtiTraitement.Text = "Exécution de la préparation des VUES - CARRIERE Lecture Contextes de Paie"

                WseMetier = New Virtualia.Net.WebService.VirtualiaWcfMetier
                Cretour = WseMetier.Carriere.PreparationVUES_Lecture("D")

            Case 3
                HNumPage.Value = "4"
                EtiTraitement.Text = "Exécution de la préparation des VUES - CARRIERE Obtention des dossiers concernés"

                WseMetier = New Virtualia.Net.WebService.VirtualiaWcfMetier
                Cretour = WseMetier.Carriere.PreparationVUES_Lecture("P")

            Case 4
                HNumPage.Value = "5"
                EtiTraitement.Text = "Exécution de la préparation des VUES CARRIERE - Générer - Début"

                WseMetier = New Virtualia.Net.WebService.VirtualiaWcfMetier
                V_ListeIde = WseMetier.Carriere.PreparationVUES_ListeDossiers

            Case 5 To 100
                Dim LstIde As List(Of Integer) = V_ListeIde
                Dim TabIde As List(Of Integer)
                Dim IndiceK As Integer

                If (CInt(HNumPage.Value) - 5) * 200 > LstIde.Count - 1 Then
                    WseMetier = New Virtualia.Net.WebService.VirtualiaWcfMetier
                    Cretour = WseMetier.Carriere.PreparationVUES_Finaliser
                    HNumPage.Value = "999"
                    EtiTraitement.Text = "Exécution de la préparation des VUES CARRIERE - Traitement terminé"
                    CType(WebFct.PointeurUtilisateur.V_PointeurSessionModule, Virtualia.Net.Session.SessionModule).SiEnExecution = False
                    HeureFin = System.DateTime.Now
                    EtiHFin.Text = HeureFin.ToString("dd/MM/yyyy hh:mm:ss")
                    Exit Sub
                End If

                TabIde = New List(Of Integer)
                For IndiceK = (CInt(HNumPage.Value) - 5) * 200 To ((CInt(HNumPage.Value) - 4) * 200) - 1
                    If IndiceK > LstIde.Count - 1 Then
                        Exit For
                    End If
                    TabIde.Add(LstIde(IndiceK))
                Next IndiceK
                WseMetier = New Virtualia.Net.WebService.VirtualiaWcfMetier
                Cretour = WseMetier.Carriere.PreparationVUES_Generer(TabIde)

                HNumPage.Value = CStr(CInt(HNumPage.Value) + 1)
                EtiTraitement.Text = "Exécution de la préparation des VUES CARRIERE - Générer - " & CStr((CInt(HNumPage.Value) - 5) * 200)
            Case Else
                HNumPage.Value = "0"
                TimerExec.Enabled = False
        End Select
        CType(WebFct.PointeurUtilisateur.V_PointeurSessionModule, Virtualia.Net.Session.SessionModule).SiEnExecution = False
    End Sub

    Private Sub ExecuterImport_PFR()
        Dim HeureDeb As Date
        Dim HeureFin As Date
        Dim Limite As Integer

        If WebFct Is Nothing Then
            WebFct = New Virtualia.Net.Controles.WebFonctions(Me, 0)
        End If
        If CType(WebFct.PointeurUtilisateur.V_PointeurSessionModule, Virtualia.Net.Session.SessionModule).SiEnExecution = True Then
            Exit Sub
        End If
        CType(WebFct.PointeurUtilisateur.V_PointeurSessionModule, Virtualia.Net.Session.SessionModule).SiEnExecution = True
        If CType(WebFct.PointeurUtilisateur.V_PointeurSessionModule, Virtualia.Net.Session.SessionModule).ExeImportPFR Is Nothing OrElse CType(WebFct.PointeurUtilisateur.V_PointeurSessionModule, Virtualia.Net.Session.SessionModule).ExeImportPFR.ListeImport Is Nothing Then
            Exit Sub
        End If
        Limite = CType(WebFct.PointeurUtilisateur.V_PointeurSessionModule, Virtualia.Net.Session.SessionModule).ExeImportPFR.Nombre_Enregistrements + 1
        Select Case CInt(HNumPage.Value)
            Case 1
                HeureDeb = System.DateTime.Now
                EtiHDebut.Text = HeureDeb.ToString("dd/MM/yyyy hh:mm:ss")
                TimerExec.Interval = 200
            Case Is > Limite
                HNumPage.Value = "0"
                TimerExec.Enabled = False
                HeureFin = System.DateTime.Now
                EtiHFin.Text = HeureFin.ToString("dd/MM/yyyy hh:mm:ss")
                CType(WebFct.PointeurUtilisateur.V_PointeurSessionModule, Virtualia.Net.Session.SessionModule).SiEnExecution = False
                CType(WebFct.PointeurUtilisateur.V_PointeurSessionModule, Virtualia.Net.Session.SessionModule).ExeImportPFR = Nothing
                Exit Sub
        End Select
        EtiTraitement.Text = "Ligne n° " & HNumPage.Value & " - " & CType(WebFct.PointeurUtilisateur.V_PointeurSessionModule, Virtualia.Net.Session.SessionModule).ExeImportPFR.TraiterDossierCourant(CInt(HNumPage.Value) - 1)
        HNumPage.Value = CStr(CInt(HNumPage.Value) + 1)
        CType(WebFct.PointeurUtilisateur.V_PointeurSessionModule, Virtualia.Net.Session.SessionModule).SiEnExecution = False
    End Sub

    Private Sub CmdExe_Click(sender As Object, e As EventArgs) Handles CommandeN1.Click, CommandeN2.Click, CommandeN3.Click, CommndeImport.Click
        Select Case CType(sender, System.Web.UI.WebControls.Button).ID
            Case "CommndeImport"
                V_Commande = "FIC"
                EtiTraitement.Text = "Exécution de l'import des acomptes - Lecture"
            Case "CommandeN1"
                V_Commande = "PFR"
                EtiTraitement.Text = "Exécution de la préparation des VUES PFR - Lecture"
            Case "CommandeN2"
                V_Commande = "ORG"
                EtiTraitement.Text = "Exécution de la préparation des VUES Annuaire - Lecture Fonds du dossier"
            Case "CommandeN3"
                V_Commande = "CAR"
                EtiTraitement.Text = "Exécution de la préparation des VUES - CARRIERE Lecture Situations administratives"
        End Select

        HNumPage.Value = "1"
        TimerExec.Interval = 2000
        TimerExec.Enabled = True
    End Sub

    Private Sub CmdUpload_Click(sender As Object, e As EventArgs) Handles CmdUpload.Click
        Dim NomRepertoire As String
        Dim NomFichier As String = ""
        Dim Extension As String
        Dim IndiceI As Integer
        Dim VEntete As List(Of String)
        Dim VTableau As List(Of String)
        Dim ExeImport As Virtualia.Net.Execution.ImportPFR
        Dim LstImport As List(Of Virtualia.Import.Externe.PFR.AcomptePFR)

        If WebFct Is Nothing Then
            WebFct = New Virtualia.Net.Controles.WebFonctions(Me, 0)
        End If

        If VUploadCarte.HasFile Then
            NomRepertoire = System.Configuration.ConfigurationManager.AppSettings("RepertoireImport")
            NomFichier = Server.HtmlEncode(VUploadCarte.FileName)
            Extension = System.IO.Path.GetExtension(NomFichier)
            If Extension = ".txt" Or Extension = ".csv" Then
                If My.Computer.FileSystem.DirectoryExists(NomRepertoire & "\Fichiers_PFR") = False Then
                    My.Computer.FileSystem.CreateDirectory(NomRepertoire & "\Fichiers_PFR")
                End If
                VUploadCarte.SaveAs(NomRepertoire & "\Fichiers_PFR\" & NomFichier)
                ExeImport = New Virtualia.Net.Execution.ImportPFR(WebFct.PointeurUtilisateur, NomRepertoire & "\Fichiers_PFR\" & NomFichier)
                LstImport = ExeImport.ListeImport
                If LstImport IsNot Nothing Then
                    CType(WebFct.PointeurUtilisateur.V_PointeurSessionModule, Virtualia.Net.Session.SessionModule).ExeImportPFR = ExeImport
                    VEntete = LstImport.Item(0).VEntete_Liste
                    VEntete.Add("CLEF")
                    VTableau = New List(Of String)
                    For Each Enreg In LstImport
                        VTableau.Add(Enreg.VLigne(VI.Tild))
                    Next
                    For IndiceI = 3 To 12
                        ListeGrille.Centrage_Colonne(IndiceI) = 1
                    Next IndiceI
                    ListeGrille.V_LibelColonne = VEntete
                    ListeGrille.V_Liste = VTableau
                End If
            End If
        End If
        CType(WebFct.PointeurUtilisateur.V_PointeurSessionModule, Virtualia.Net.Session.SessionModule).SiEnExecution = False
        HNumPage.Value = "0"
        TimerExec.Enabled = False
    End Sub
End Class