﻿Option Explicit On
Option Strict On
Option Compare Text
Namespace WebAppli
    Namespace SelfV4
        <Serializable>
        Public Class CacheSynthese
            Private WsAnnee As Integer
            Private WsMois As Integer
            Private WsIde_Dossier As Integer
            Private WsIndex_VueActive As Integer

            Public Property AnneeSelection As Integer
                Get
                    Return WsAnnee
                End Get
                Set(value As Integer)
                    WsAnnee = value
                End Set
            End Property

            Public Property MoisSelection As Integer
                Get
                    Return WsMois
                End Get
                Set(value As Integer)
                    WsMois = value
                End Set
            End Property

            Public Property Ide_Dossier As Integer
                Get
                    Return WsIde_Dossier
                End Get
                Set(value As Integer)
                    WsIde_Dossier = value
                End Set
            End Property

            Public Property Index_VueActive As Integer
                Get
                    Return WsIndex_VueActive
                End Get
                Set(value As Integer)
                    WsIndex_VueActive = value
                End Set
            End Property
        End Class
    End Namespace
End Namespace