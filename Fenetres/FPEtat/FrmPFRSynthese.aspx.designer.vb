﻿'------------------------------------------------------------------------------
' <généré automatiquement>
'     Ce code a été généré par un outil.
'
'     Les modifications apportées à ce fichier peuvent provoquer un comportement incorrect et seront perdues si
'     le code est régénéré.
' </généré automatiquement>
'------------------------------------------------------------------------------

Option Strict On
Option Explicit On


Partial Public Class FrmPFRSynthese
    
    '''<summary>
    '''Contrôle CadreAnnee.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CadreAnnee As Global.System.Web.UI.WebControls.Table
    
    '''<summary>
    '''Contrôle ComboSelAnnee.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents ComboSelAnnee As Global.Virtualia.Net.Controles_VListeCombo
    
    '''<summary>
    '''Contrôle CadreEDition.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CadreEDition As Global.System.Web.UI.WebControls.Table
    
    '''<summary>
    '''Contrôle CadreCmd.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CadreCmd As Global.System.Web.UI.WebControls.TableCell
    
    '''<summary>
    '''Contrôle PanelCommande.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents PanelCommande As Global.System.Web.UI.WebControls.Panel
    
    '''<summary>
    '''Contrôle CadreCmdStd.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CadreCmdStd As Global.System.Web.UI.WebControls.Table
    
    '''<summary>
    '''Contrôle CmdDotation.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CmdDotation As Global.System.Web.UI.WebControls.Button
    
    '''<summary>
    '''Contrôle CmdEnveloppe.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CmdEnveloppe As Global.System.Web.UI.WebControls.Button
    
    '''<summary>
    '''Contrôle CommandeCsv.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CommandeCsv As Global.System.Web.UI.WebControls.Button
    
    '''<summary>
    '''Contrôle CommandeRetour.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CommandeRetour As Global.System.Web.UI.WebControls.Button
    
    '''<summary>
    '''Contrôle CadreVues.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CadreVues As Global.System.Web.UI.WebControls.Table
    
    '''<summary>
    '''Contrôle MultiVues.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents MultiVues As Global.System.Web.UI.WebControls.MultiView
    
    '''<summary>
    '''Contrôle VueEdition.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents VueEdition As Global.System.Web.UI.WebControls.View
    
    '''<summary>
    '''Contrôle ListeSynthese.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents ListeSynthese As Global.Virtualia.Net.Controles_VListeGrid
    
    '''<summary>
    '''Contrôle CtlTableaux.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CtlTableaux As Global.Virtualia.Net.CtrlGraphique
    
    '''<summary>
    '''Propriété Master.
    '''</summary>
    '''<remarks>
    '''Propriété générée automatiquement.
    '''</remarks>
    Public Shadows ReadOnly Property Master() As Virtualia.Net.VirtualiaMain
        Get
            Return CType(MyBase.Master,Virtualia.Net.VirtualiaMain)
        End Get
    End Property
End Class
