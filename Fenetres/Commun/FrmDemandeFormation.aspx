﻿<%@ Page Language="vb" MasterPageFile="~/PagesMaitres/VirtualiaMain.Master" AutoEventWireup="false" CodeBehind="FrmDemandeFormation.aspx.vb" 
    Inherits="Virtualia.Net.FrmDemandeFormation" MaintainScrollPositionOnPostback="true" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<%@ MasterType VirtualPath="~/PagesMaitres/VirtualiaMain.Master" %>
<%@ Register src="~/Controles/Commun/VMessage.ascx" tagname="VMessage" tagprefix="Virtualia" %>
<%@ Register src="~/Controles/Formation/VArmoireFOR.ascx" tagname="VArmoireFOR" tagprefix="Virtualia" %>
<%@ Register src="~/Controles/Formation/CtlListeCalendrier.ascx" tagname="VListeCal" tagprefix="Virtualia" %>
<%@ Register src="~/Controles/Formation/DemandeFormation.ascx" tagname="VDemandeFOR" tagprefix="Virtualia" %>

<asp:Content ID="CadreGeneral" runat="server" ContentPlaceHolderID="Principal">
    <asp:Table ID="CadreArmoire" runat="server" HorizontalAlign="Center" Font-Names="Trebuchet MS" Font-Size="Small" Font-Bold="false">
        <asp:TableRow>
            <asp:TableCell Height="5px"/>
        </asp:TableRow>
        <asp:TableRow>
            <asp:TableCell>
                <Virtualia:VDemandeFOR ID="Demande" runat="server" />
            </asp:TableCell>
        </asp:TableRow>
        <asp:TableRow>
             <asp:TableCell ID="CellTypeArmoire" HorizontalAlign="Center" VerticalAlign="Middle" BackColor="#6D9092" Height="40px" Width="720px">
                    <asp:Table ID="TableTypeArmoire" runat="server" CellPadding="0" CellSpacing="0" HorizontalAlign="Center" Width="720px">
                        <asp:TableRow>
                            <asp:TableCell VerticalAlign="Middle" HorizontalAlign="Center" Height="30px">
                                <asp:Label ID="EtiTypeArmoire" runat="server" Text="Catalogue" BackColor="Transparent" ForeColor="White" Width="120px" BorderStyle="None" Font-Italic="true" />
                            </asp:TableCell>
                            <asp:TableCell VerticalAlign="Middle" HorizontalAlign="Center" Height="30px">
                                <asp:DropDownList ID="LstTypeArmoire" runat="server" Height="22px" Width="580px" AutoPostBack="True" BackColor="#A8BBB8" ForeColor="#124545" Style="text-align:left" />
                            </asp:TableCell>
                        </asp:TableRow>
                    </asp:Table>
            </asp:TableCell>
        </asp:TableRow>
        <asp:TableRow>
                <asp:TableCell ID="ConteneurVues" VerticalAlign="Top" HorizontalAlign="Center">
                    <asp:MultiView ID="MultiOnglets" runat="server" ActiveViewIndex="0">
                        <asp:View ID="VueCalendrier" runat="server">
                            <Virtualia:VListeCal ID="CalendrierDyna" runat="server" />
                        </asp:View>
                        <asp:View ID="VueArmoire" runat="server">
                            <Virtualia:VArmoireFOR ID="Armoire" runat="server" IDAppelant="FOR" />
                        </asp:View>
                    </asp:MultiView>
                </asp:TableCell>
        </asp:TableRow>
        <asp:TableRow>
             <asp:TableCell ID="CellMessage" Visible="false">
                <ajaxToolkit:ModalPopupExtender ID="PopupMsg" runat="server" TargetControlID="HPopupMsg" PopupControlID="PanelMsgPopup" BackgroundCssClass="modalBackground" />
                <asp:Panel ID="PanelMsgPopup" runat="server">
                    <Virtualia:VMessage id="MsgVirtualia" runat="server" />
                </asp:Panel>
                <asp:HiddenField ID="HPopupMsg" runat="server" Value="0" />
            </asp:TableCell>
        </asp:TableRow>
    </asp:Table>
</asp:Content>