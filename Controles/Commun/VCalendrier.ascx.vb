﻿Option Strict On
Option Explicit On
Option Compare Text
Imports VI = Virtualia.Systeme.Constantes
Partial Class Controles_VCalendrier
    Inherits System.Web.UI.UserControl
    Public Delegate Sub Valeur_ChangeEventHandler(ByVal sender As Object, ByVal e As Virtualia.Systeme.Evenements.DonneeChangeEventArgs)
    Public Event ValeurChange As Valeur_ChangeEventHandler
    Private WebFct As Virtualia.Net.Controles.WebFonctions
    Private WsTypeCalend As String = "SD" 'SD=Standard Saisie date début, ID=Individuel date début
    'SF=Standard Saisie date fin, IF=Individuel date fin
    Private WsNomStateIde As String = "VCalendrier"
    Private WsCouleurSelection As System.Drawing.Color
    Private WsForeColorSelection As System.Drawing.Color

    Protected Overridable Sub Valeur_Change(ByVal e As Virtualia.Systeme.Evenements.DonneeChangeEventArgs)
        RaiseEvent ValeurChange(Me, e)
    End Sub

    Public Property Identifiant() As Integer
        Get
            Dim CacheIde As Integer
            If Me.ViewState(WsNomStateIde) IsNot Nothing Then
                CacheIde = CType(Me.ViewState(WsNomStateIde), Integer)
                Return CacheIde
            End If
            Return 0
        End Get
        Set(ByVal value As Integer)
            If value = 0 Then
                Exit Property
            End If
            Dim CacheIde As Integer
            If Me.ViewState(WsNomStateIde) IsNot Nothing Then
                CacheIde = CType(Me.ViewState(WsNomStateIde), Integer)
                If value = CacheIde Then
                    Exit Property
                End If
                CacheIde = value
                Me.ViewState.Remove(WsNomStateIde)
            End If
            CacheIde = value
            Me.ViewState.Add(WsNomStateIde, CacheIde)
            Dim UtiSession As Virtualia.Net.Session.ObjetSession = WebFct.PointeurUtilisateur
            If UtiSession Is Nothing Then
                Exit Property
            End If

        End Set
    End Property

    Public Property TypeCalendrier() As String
        Get
            Return WsTypeCalend
        End Get
        Set(ByVal value As String)
            WsTypeCalend = value
        End Set
    End Property

    Public Property SiEtiDateSelVisible() As Boolean
        Get
            Return CalDateSel00.Visible
        End Get
        Set(ByVal value As Boolean)
            CalDateSel00.Visible = value
            CalDateSel01.Visible = value
        End Set
    End Property

    Public Property MoisSelectionne() As String
        Get
            Return HSelMois.Value
        End Get
        Set(ByVal value As String)
            HSelMois.Value = value
        End Set
    End Property

    Public Property DateSelectionnee(ByVal Index As Integer) As String
        Get
            Select Case Index
                Case 0
                    Return CalDateSel00.Text
                Case 1
                    Return CalDateSel01.Text
                Case Else
                    Return ""
            End Select
        End Get
        Set(ByVal value As String)
            Select Case Index
                Case 0
                    CalDateSel00.Text = value
                Case 1
                    CalDateSel01.Text = value
            End Select
            HSelDate.Value = value
        End Set
    End Property

    Public WriteOnly Property SiJourVisible(ByVal NoJour As Integer, ByVal Prefixe As String) As Boolean
        Set(ByVal value As Boolean)
            Dim Ctl As Control
            Dim VirControle As System.Web.UI.WebControls.Button
            Ctl = WebFct.VirWebControle(Me, Prefixe, NoJour)
            If Ctl Is Nothing Then
                Exit Property
            End If
            VirControle = CType(Ctl, System.Web.UI.WebControls.Button)
            Select Case value
                Case True
                    VirControle.BackColor = Drawing.Color.LightGray
                    VirControle.BorderStyle = BorderStyle.Solid
                    Select Case WsTypeCalend
                        Case Is = "ID", "IF"
                            VJourStyle(NoJour, "CalPM") = "solid"
                    End Select
                Case False
                    VirControle.BackColor = Drawing.Color.Transparent
                    VirControle.BorderStyle = BorderStyle.None
                    VirControle.Text = ""
                    VJourStyle(NoJour, Prefixe) = "none"
            End Select
        End Set
    End Property

    Public WriteOnly Property SiJourEnable(ByVal NoJour As Integer, ByVal Prefixe As String) As Boolean
        Set(ByVal value As Boolean)
            Dim Ctl As Control
            Dim VirControle As System.Web.UI.WebControls.Button
            Ctl = WebFct.VirWebControle(Me, Prefixe, NoJour)
            If Ctl Is Nothing Then
                Exit Property
            End If
            VirControle = CType(Ctl, System.Web.UI.WebControls.Button)
            VirControle.Enabled = value
        End Set
    End Property

    Public WriteOnly Property TitreText() As String
        Set(ByVal value As String)
            EtiTitre.Text = value
        End Set
    End Property

    Public WriteOnly Property SiCocheVisible() As Boolean
        Set(ByVal value As Boolean)
            CalCoche.V_Visible = value
        End Set
    End Property

    Public WriteOnly Property CocheText() As String
        Set(ByVal value As String)
            CalCoche.V_Text = value
        End Set
    End Property

    Public WriteOnly Property VText(ByVal NoJour As Integer, ByVal Prefixe As String) As String
        Set(ByVal value As String)
            Dim Ctl As Control
            Dim VirControle As System.Web.UI.WebControls.Button
            Ctl = WebFct.VirWebControle(Me, Prefixe, NoJour)
            If Ctl Is Nothing Then
                Exit Property
            End If
            VirControle = CType(Ctl, System.Web.UI.WebControls.Button)
            VirControle.Text = value
        End Set
    End Property

    Public WriteOnly Property VDemiJourToolTip(ByVal NoJour As Integer, ByVal Prefixe As String) As String
        Set(ByVal value As String)
            Dim Ctl As Control
            Dim VirControle As System.Web.UI.WebControls.Button
            Ctl = WebFct.VirWebControle(Me, Prefixe, NoJour)
            If Ctl Is Nothing Then
                Exit Property
            End If
            VirControle = CType(Ctl, System.Web.UI.WebControls.Button)
            VirControle.ToolTip = value
        End Set
    End Property

    Public WriteOnly Property VBorduresColor(ByVal Prefixe As String) As System.Drawing.Color
        Set(ByVal value As System.Drawing.Color)
            Dim IndiceI As Integer
            If WebFct Is Nothing Then
                WebFct = New Virtualia.Net.Controles.WebFonctions(Me, 0)
            End If
            For IndiceI = 0 To 36
                VBorderColor(IndiceI, Prefixe) = value
            Next IndiceI
        End Set
    End Property

    Public WriteOnly Property VBackColor(ByVal NoJour As Integer, ByVal Prefixe As String) As System.Drawing.Color
        Set(ByVal value As System.Drawing.Color)
            Dim Ctl As Control
            Dim VirControle As System.Web.UI.WebControls.Button
            Ctl = WebFct.VirWebControle(Me, Prefixe, NoJour)
            If Ctl Is Nothing Then
                Exit Property
            End If
            VirControle = CType(Ctl, System.Web.UI.WebControls.Button)
            VirControle.BackColor = value
        End Set
    End Property

    Public WriteOnly Property VBorderColor(ByVal NoJour As Integer, ByVal Prefixe As String) As System.Drawing.Color
        Set(ByVal value As System.Drawing.Color)
            Dim Ctl As Control
            Dim VirControle As System.Web.UI.WebControls.Button
            Ctl = WebFct.VirWebControle(Me, Prefixe, NoJour)
            If Ctl Is Nothing Then
                Exit Property
            End If
            VirControle = CType(Ctl, System.Web.UI.WebControls.Button)
            VirControle.BorderColor = value
        End Set
    End Property

    Public WriteOnly Property VSelectionColor() As String
        Set(ByVal value As String)
            If WebFct Is Nothing Then
                WebFct = New Virtualia.Net.Controles.WebFonctions(Me, 0)
            End If
            WsCouleurSelection = WebFct.ConvertCouleur(value)
            If WsCouleurSelection.GetBrightness > 0.35 Then
                WsForeColorSelection = Drawing.Color.Black
            Else
                WsForeColorSelection = Drawing.Color.White
            End If
            CalDateSel00.BackColor = WsCouleurSelection
            CalDateSel01.BackColor = WsCouleurSelection
            CalDateSel00.ForeColor = WsForeColorSelection
            CalDateSel01.ForeColor = WsForeColorSelection
        End Set
    End Property

    Public WriteOnly Property VJoursStyle() As String
        Set(ByVal value As String)
            Dim IndiceI As Integer
            If WebFct Is Nothing Then
                WebFct = New Virtualia.Net.Controles.WebFonctions(Me, 0)
            End If
            For IndiceI = 0 To 36
                VJourStyle(IndiceI, "CalPM") = value
            Next IndiceI
        End Set
    End Property

    Public WriteOnly Property VJourStyle(ByVal NoJour As Integer, ByVal Prefixe As String) As String
        Set(ByVal value As String)
            Dim Ctl As Control
            Dim VirControle As System.Web.UI.WebControls.Button
            Dim StyleBordure As String = "border-top-style"
            Dim NumBouton As Integer

            If Prefixe = "CalAM" Then
                StyleBordure = "border-bottom-style"
            End If

            Ctl = WebFct.VirWebControle(Me, Prefixe, NoJour)
            If Ctl Is Nothing Then
                Exit Property
            End If
            VirControle = CType(Ctl, System.Web.UI.WebControls.Button)
            NumBouton = CInt(Strings.Right(VirControle.ID, 2))
            If NumBouton = NoJour Then
                VirControle.Style.Remove(StyleBordure)
                VirControle.Style.Add(StyleBordure, value)
            End If
        End Set
    End Property

    Public WriteOnly Property VFontSize(ByVal NoJour As Integer, ByVal Prefixe As String) As System.Web.UI.WebControls.FontUnit
        Set(ByVal value As System.Web.UI.WebControls.FontUnit)
            Dim Ctl As Control
            Dim VirControle As System.Web.UI.WebControls.Button
            Ctl = WebFct.VirWebControle(Me, Prefixe, NoJour)
            If Ctl Is Nothing Then
                Exit Property
            End If
            VirControle = CType(Ctl, System.Web.UI.WebControls.Button)
            VirControle.Font.Size = value
        End Set
    End Property

    Public WriteOnly Property VFontBold(ByVal NoJour As Integer, ByVal Prefixe As String) As Boolean
        Set(ByVal value As Boolean)
            Dim Ctl As Control
            Dim VirControle As System.Web.UI.WebControls.Button
            Ctl = WebFct.VirWebControle(Me, Prefixe, NoJour)
            If Ctl Is Nothing Then
                Exit Property
            End If
            VirControle = CType(Ctl, System.Web.UI.WebControls.Button)
            VirControle.Font.Bold = value
        End Set
    End Property

    Private Sub FaireCalendrier()
        Dim TableauW(0) As String
        Dim Mois As Integer
        Dim Annee As Integer
        Dim DateW As String
        Dim DateFin As String
        Dim NoJour As Integer
        Dim IndiceJ As Integer
        Dim IndiceCtl As Integer
        Dim LstAbs As List(Of Virtualia.Net.Controles.ItemJourCalendrier) = Nothing
        Dim Couleur As Drawing.Color

        For IndiceCtl = 0 To 36
            SiJourEnable(IndiceCtl, "CalAM") = True
            SiJourEnable(IndiceCtl, "CalPM") = True
        Next IndiceCtl

        If HSelMois.Value = "" Then
            Annee = CInt(Strings.Right(WebFct.ViRhDates.DateduJour, 4))
            Mois = CInt(Strings.Mid(WebFct.ViRhDates.DateduJour, 4, 2))
        Else
            TableauW = Strings.Split(HSelMois.Value, Strings.Space(1), -1)
            Annee = CInt(TableauW(1))
            Mois = WebFct.ViRhDates.IndexduMois(TableauW(0))
        End If
        Select Case TypeCalendrier
            Case "ID", "IF"
                If WebFct.PointeurUtilisateur IsNot Nothing Then
                    LstAbs = CType(WebFct.PointeurUtilisateur.V_PointeurSessionModule, Virtualia.Net.Session.SessionModule).DossierPER.VDossier_CalendrierAbs
                End If
        End Select

        DateW = "01/" & Strings.Format(Mois, "00") & "/" & Annee.ToString
        DateFin = WebFct.ViRhDates.DateSaisieVerifiee("31" & "/" & Strings.Format(Mois, "00") & "/" & Annee.ToString)

        NoJour = Weekday(DateValue(DateW), Microsoft.VisualBasic.FirstDayOfWeek.Monday) - 1
        For IndiceCtl = 0 To NoJour - 1
            SiJourVisible(IndiceCtl, "CalAM") = False
            SiJourVisible(IndiceCtl, "CalPM") = False
        Next IndiceCtl
        IndiceJ = NoJour

        Do 'Traitement des jours du mois
            '** Calendrier Standard
            SiJourVisible(IndiceJ, "CalAM") = True
            SiJourVisible(IndiceJ, "CalPM") = True
            VBorderColor(IndiceJ, "CalAM") = WebFct.ConvertCouleur("#216B68")
            VBorderColor(IndiceJ, "CalPM") = WebFct.ConvertCouleur("#216B68")
            VText(IndiceJ, "CalAM") = Strings.Left(DateW, 2)
            VDemiJourToolTip(IndiceJ, "CalAM") = WebFct.ViRhDates.ClairDate(DateW, True)
            VDemiJourToolTip(IndiceJ, "CalPM") = WebFct.ViRhDates.ClairDate(DateW, True)
            If WebFct.ViRhDates.SiJourOuvre(DateW, True) = True Then
                VBackColor(IndiceJ, "CalAM") = WebFct.PointeurGlobal.InstanceCouleur.Item(0).Couleur_Web
                VBackColor(IndiceJ, "CalPM") = WebFct.PointeurGlobal.InstanceCouleur.Item(0).Couleur_Web
            Else
                VBackColor(IndiceJ, "CalAM") = WebFct.PointeurGlobal.InstanceCouleur.Item(1).Couleur_Web
                VBackColor(IndiceJ, "CalPM") = WebFct.PointeurGlobal.InstanceCouleur.Item(1).Couleur_Web
                VJourStyle(IndiceJ, "CalPM") = "none"
                If WebFct.ViRhDates.SiJourFerie(DateW) = True Then
                    VBorderColor(IndiceJ, "CalAM") = Drawing.Color.Red
                    VBorderColor(IndiceJ, "CalPM") = Drawing.Color.Red
                End If
            End If
            If DateW = HSelDate.Value Then
                If WsCouleurSelection.IsEmpty Then
                    WsCouleurSelection = WebFct.PointeurGlobal.InstanceCouleur.Item(4).Couleur_Web
                End If
                VBackColor(IndiceJ, "CalAM") = WsCouleurSelection
                VBackColor(IndiceJ, "CalPM") = WsCouleurSelection
            End If
            '** Inclure les absences
            Select Case TypeCalendrier
                Case "ID", "IF"
                    Dim FicheAbs As Virtualia.Net.Controles.ItemJourCalendrier = Nothing
                    If LstAbs IsNot Nothing Then
                        FicheAbs = LstAbs.Find(Function(Recherche) Recherche.Date_Valeur_ToDate = CDate(DateW))
                    End If
                    If FicheAbs IsNot Nothing Then
                        Select Case FicheAbs.Caracteristique
                            Case VI.NumeroPlage.Jour_Formation, VI.NumeroPlage.Plage1_Formation, VI.NumeroPlage.Plage2_Formation
                                Couleur = WebFct.PointeurGlobal.InstanceCouleur.Item(3).Couleur_Web
                            Case Else
                                Couleur = WebFct.PointeurGlobal.InstanceCouleur.Item(WebFct.PointeurGlobal.InstanceCouleur.IndexCouleurPlanning(FicheAbs.Intitule)).Couleur_Web
                        End Select
                        Select Case FicheAbs.Caracteristique
                            Case VI.NumeroPlage.Jour_Absence, VI.NumeroPlage.Jour_Formation
                                VJourStyle(IndiceJ, "CalAM") = "none"
                                VBackColor(IndiceJ, "CalAM") = Couleur
                                VDemiJourToolTip(IndiceJ, "CalAM") = FicheAbs.Intitule
                                VBackColor(IndiceJ, "CalPM") = Couleur
                                VDemiJourToolTip(IndiceJ, "CalPM") = FicheAbs.Intitule
                                SiJourEnable(IndiceJ, "CalAM") = False
                                SiJourEnable(IndiceJ, "CalPM") = False
                            Case VI.NumeroPlage.Plage1_Absence, VI.NumeroPlage.Plage1_Formation
                                VJourStyle(IndiceJ, "CalAM") = "solid"
                                VBackColor(IndiceJ, "CalAM") = Couleur
                                VDemiJourToolTip(IndiceJ, "CalAM") = FicheAbs.Intitule
                                SiJourEnable(IndiceJ, "CalAM") = False
                            Case VI.NumeroPlage.Plage2_Absence, VI.NumeroPlage.Plage2_Formation
                                VJourStyle(IndiceJ, "CalAM") = "solid"
                                VBackColor(IndiceJ, "CalPM") = Couleur
                                VDemiJourToolTip(IndiceJ, "CalPM") = FicheAbs.Intitule
                                SiJourEnable(IndiceJ, "CalPM") = False
                        End Select
                    End If
            End Select

            DateW = WebFct.ViRhDates.CalcDateMoinsJour(DateW, "1", "0")
            If CInt(Strings.Mid(DateW, 4, 2)) <> Mois Then
                For IndiceCtl = IndiceJ + 1 To 36
                    SiJourVisible(IndiceCtl, "CalAM") = False
                    SiJourVisible(IndiceCtl, "CalPM") = False
                Next IndiceCtl
                Exit Do
            End If
            NoJour = Weekday(DateValue(DateW), Microsoft.VisualBasic.FirstDayOfWeek.Monday) - 1
            IndiceJ += 1
        Loop

    End Sub

    Protected Sub Page_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init
        WebFct = New Virtualia.Net.Controles.WebFonctions(Me, 0)
    End Sub

    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        Call FaireListeMois()
        Call FaireCalendrier()
    End Sub

    Private Sub FaireListeMois()
        Dim Chaine As New System.Text.StringBuilder
        Dim Libel As String = "Aucun mois" & VI.Tild & "Un mois" & VI.Tild & "possibilités"
        Dim Annee As Integer
        Dim Mois As Integer
        Dim Limite As Integer
        Dim IndiceI As Integer
        Dim IndiceA As Integer
        Annee = CInt(Strings.Right(WebFct.ViRhDates.DateduJour, 4))
        Mois = CInt(Strings.Mid(WebFct.ViRhDates.DateduJour, 4, 2))

        Select Case WsTypeCalend
            Case "IF", "SF"
                If HSelDate.Value <> "" Then
                    Annee = CInt(Strings.Right(HSelDate.Value, 4))
                    Mois = CInt(Strings.Mid(HSelDate.Value, 4, 2))
                End If
                IndiceA = Annee
                Limite = 12
                Do
                    For IndiceI = Mois To Limite
                        Chaine.Append(WebFct.ViRhDates.MoisEnClair(IndiceI) & Strings.Space(1) & IndiceA.ToString & VI.Tild)
                    Next IndiceI
                    IndiceA += 1
                    If IndiceA - Annee > 1 Then
                        Exit Do
                    End If
                    Limite -= Mois + 1
                    Mois = 1
                Loop
            Case "ID"
                IndiceA = Annee
                Limite = 12
                Do
                    For IndiceI = Mois To Limite
                        Chaine.Append(WebFct.ViRhDates.MoisEnClair(IndiceI) & Strings.Space(1) & IndiceA.ToString & VI.Tild)
                    Next IndiceI
                    IndiceA += 1
                    If IndiceA - Annee > 1 Then
                        Exit Do
                    End If
                    Limite -= Mois + 1
                    Mois = 1
                Loop
            Case "SD"
                IndiceA = Annee + 1
                Do
                    For IndiceI = 12 To 1 Step -1
                        Chaine.Append(WebFct.ViRhDates.MoisEnClair(IndiceI) & Strings.Space(1) & IndiceA.ToString & VI.Tild)
                    Next IndiceI
                    IndiceA -= 1
                    If IndiceA < Annee - 3 Then
                        Exit Do
                    End If
                Loop
        End Select
        CalLstMois.V_Liste(Libel) = Chaine.ToString
        Chaine.Clear()

        If HSelMois.Value = "" Then
            HSelMois.Value = WebFct.ViRhDates.MoisEnClair(CInt(Strings.Mid(WebFct.ViRhDates.DateduJour, 4, 2))) & Strings.Space(1) & Year(Now)
        End If
        CalLstMois.LstText = HSelMois.Value
    End Sub

    Protected Sub CalLstMois_ValeurChange(ByVal sender As Object, ByVal e As Virtualia.Systeme.Evenements.DonneeChangeEventArgs) Handles CalLstMois.ValeurChange
        HSelMois.Value = e.Valeur
    End Sub

    Protected Sub CalAM_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles CalAM00.Click, CalAM01.Click,
    CalAM02.Click, CalAM03.Click, CalAM04.Click, CalAM05.Click, CalAM06.Click, CalAM07.Click, CalAM08.Click, CalAM09.Click,
    CalAM10.Click, CalAM11.Click, CalAM12.Click, CalAM13.Click, CalAM14.Click, CalAM15.Click, CalAM08.Click, CalAM16.Click,
    CalAM17.Click, CalAM18.Click, CalAM19.Click, CalAM20.Click, CalAM21.Click, CalAM22.Click, CalAM23.Click, CalAM24.Click,
    CalAM25.Click, CalAM26.Click, CalAM27.Click, CalAM28.Click, CalAM29.Click, CalAM30.Click, CalAM31.Click, CalAM32.Click,
    CalAM33.Click, CalAM34.Click, CalAM35.Click, CalAM36.Click

        If HSelMois.Value = "" Then
            Exit Sub
        End If
        Dim TableauW(0) As String
        Dim Mois As Integer
        Dim Annee As Integer
        Dim Jour As Integer
        Dim DateW As String

        TableauW = Strings.Split(HSelMois.Value, Strings.Space(1), -1)
        Annee = CInt(TableauW(1))
        Mois = WebFct.ViRhDates.IndexduMois(TableauW(0))
        Jour = CInt(CType(sender, System.Web.UI.WebControls.Button).Text)
        DateW = Strings.Format(Jour, "00") & VI.Slash & Strings.Format(Mois, "00") & VI.Slash & Annee.ToString

        Select Case WsTypeCalend
            Case Is = "IF", "SF"
                If HSelDate.Value <> "" Then
                    Select Case WebFct.ViRhDates.ComparerDates(DateW, HSelDate.Value)
                        Case VI.ComparaisonDates.PlusPetit
                            Exit Sub
                    End Select
                End If
        End Select

        HSelDate.Value = DateW
        CalDateSel01.Text = ""
        CalDateSel00.Text = DateW

        Dim Evenement As Virtualia.Systeme.Evenements.DonneeChangeEventArgs
        Evenement = New Virtualia.Systeme.Evenements.DonneeChangeEventArgs("Jour", DateW)
        Valeur_Change(Evenement)
    End Sub

    Protected Sub CalPM_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles CalPM00.Click, CalPM01.Click,
    CalPM02.Click, CalPM03.Click, CalPM04.Click, CalPM05.Click, CalPM06.Click, CalPM07.Click, CalPM08.Click, CalPM09.Click,
    CalPM10.Click, CalPM11.Click, CalPM12.Click, CalPM13.Click, CalPM14.Click, CalPM15.Click, CalPM08.Click, CalPM16.Click,
    CalPM17.Click, CalPM18.Click, CalPM19.Click, CalPM20.Click, CalPM21.Click, CalPM22.Click, CalPM23.Click, CalPM24.Click,
    CalPM25.Click, CalPM26.Click, CalPM27.Click, CalPM28.Click, CalPM29.Click, CalPM30.Click, CalPM31.Click, CalPM32.Click,
    CalPM33.Click, CalPM34.Click, CalPM35.Click, CalPM36.Click

        If HSelMois.Value Is Nothing Then
            Exit Sub
        End If
        Dim TableauW(0) As String
        Dim Mois As Integer
        Dim Annee As Integer
        Dim Jour As Integer
        Dim DateW As String
        Dim Ctl As Control
        Dim VirControle As System.Web.UI.WebControls.Button
        Jour = CInt(Strings.Right(CType(sender, System.Web.UI.WebControls.Button).ID, 2))
        Ctl = WebFct.VirWebControle(Me, "CalAM", Jour)
        If Ctl Is Nothing Then
            Exit Sub
        End If
        VirControle = CType(Ctl, System.Web.UI.WebControls.Button)
        Jour = CInt(VirControle.Text)

        TableauW = Strings.Split(HSelMois.Value, Strings.Space(1), -1)
        Annee = CInt(TableauW(1))
        Mois = WebFct.ViRhDates.IndexduMois(TableauW(0))
        DateW = Strings.Format(Jour, "00") & VI.Slash & Strings.Format(Mois, "00") & VI.Slash & Annee.ToString

        Select Case WsTypeCalend
            Case Is = "IF", "SF"
                If HSelDate.Value <> "" Then
                    Select Case WebFct.ViRhDates.ComparerDates(DateW, HSelDate.Value)
                        Case VI.ComparaisonDates.PlusPetit
                            Exit Sub
                    End Select
                End If
        End Select

        HSelDate.Value = DateW
        CalDateSel00.Text = ""
        CalDateSel01.Text = DateW

        Dim Evenement As Virtualia.Systeme.Evenements.DonneeChangeEventArgs
        Evenement = New Virtualia.Systeme.Evenements.DonneeChangeEventArgs("Jour", DateW)
        Valeur_Change(Evenement)
    End Sub

    Protected Sub CalCoche_ValeurChange(ByVal sender As Object, ByVal e As Virtualia.Systeme.Evenements.DonneeChangeEventArgs) Handles CalCoche.ValeurChange
        HSelCoche.Value = e.Valeur
        Dim Evenement As Virtualia.Systeme.Evenements.DonneeChangeEventArgs
        Evenement = New Virtualia.Systeme.Evenements.DonneeChangeEventArgs("Coche", e.Valeur)
        Valeur_Change(Evenement)
    End Sub
End Class
