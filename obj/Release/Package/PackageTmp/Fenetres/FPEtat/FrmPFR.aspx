﻿<%@ Page Language="vb" AutoEventWireup="false" MasterPageFile="~/PagesMaitres/VirtualiaMain.Master" CodeBehind="FrmPFR.aspx.vb" 
    Inherits="Virtualia.Net.FrmPFR" MaintainScrollPositionOnPostback="True" %>

<%@ MasterType VirtualPath="~/PagesMaitres/VirtualiaMain.master" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit" %>
<%@ Register src="~/Controles/Commun/VMessage.ascx" tagname="VMessage" tagprefix="Virtualia" %>
<%@ Register src="~/Controles/Commun/VListeGrid.ascx" tagname="VListeGrid" tagprefix="Virtualia" %>
<%@ Register src="~/Controles/Commun/VListeCombo.ascx" tagname="VListeCombo" tagprefix="Virtualia" %>
<%@ Register src="~/Controles/FPEtat/CtlSaisiePartielle.ascx" tagname="VSaisie" tagprefix="Virtualia" %>
<%@ Register src="~/Controles/Saisies/VTrioHorizontalRadio.ascx" tagname="VTrioHorizontalRadio" tagprefix="Virtualia" %>

<asp:Content ID="LigneAnnee" ContentPlaceHolderID="HautdePage" runat="server">
    <asp:Table ID="CadreAnnee" runat="server" HorizontalAlign="Center" Width="1050px">
        <asp:TableRow>
            <asp:TableCell HorizontalAlign="Left">
                <Virtualia:VListeCombo ID="ComboSelAnnee" runat="server" EtiVisible="true" EtiText="ANNEE COURANTE" EtiWidth="150px"
                        V_NomTable="LstAnnee" LstWidth="100px" LstBorderColor="#B0E0D7" LstBackColor="#EEECFD" LstStyle="font-weight:bold"
                        EtiStyle="text-align:center;font-weight:bold;margin-left:1px" EtiBackColor="#0E5F5C" EtiForeColor="White" />
            </asp:TableCell>
        </asp:TableRow>
    </asp:Table>
</asp:Content>

<asp:Content ID="Corps" ContentPlaceHolderID="Principal" runat="server">
    <asp:Table ID="CadreGeneral" runat="server" HorizontalAlign="Center" Width="1050px">
         <asp:TableRow>
            <asp:TableCell>
                <asp:Table ID="TableOnglets" runat="server" Height="35px" Width="1050px" HorizontalAlign="Center" BackColor="Transparent" Style="margin-top: 2px">
                    <asp:TableRow>
                        <asp:TableCell HorizontalAlign="Left">
                            <asp:Menu ID="MenuChoix" runat="server" DynamicHorizontalOffset="10" Font-Names="Trebuchet MS" Font-Size="Small" 
                                BackColor="Transparent" Font-Italic="false" ForeColor="White" Height="32px" Orientation="Horizontal" 
                                StaticSubMenuIndent="20px" Font-Bold="True" BorderWidth="2px" Style="margin-left: 1px;" 
                                StaticEnableDefaultPopOutImage="False" BorderColor="#B0E0D7" BorderStyle="None">
                                <StaticSelectedStyle BackColor="#B0E0D7" BorderStyle="Solid" Font-Bold="True" Font-Names="Trebuchet MS" Font-Size="Small" ForeColor="#000066" />
                                <StaticMenuItemStyle VerticalPadding="2px" HorizontalPadding="10px" Height="24px" BackColor="#0E5F5C" BorderColor="#B0E0D7" BorderStyle="Solid" Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small" ForeColor="White" />
                                <StaticHoverStyle BackColor="#7EC8BE" ForeColor="White" BorderColor="#B0E0D7" BorderStyle="Solid" />
                                <Items>
                                    <asp:MenuItem Text="Base 100" Value="Base100" ToolTip="Base 100" />
                                    <asp:MenuItem Text="Modulation" Value="Modulation" ToolTip="Modulation" />
                                    <asp:MenuItem Text="Taux de proratisation" Value="Prorata" ToolTip="" />
                                    <asp:MenuItem Text="Affectation et Position" Value="Affectation" ToolTip="Affectation et Position administrative" />
                                    <asp:MenuItem Text="Calcul Enveloppe" Value="Enveloppe" ToolTip="Enveloppe globale de modulation" />
                                </Items>
                            </asp:Menu>
                        </asp:TableCell>
                        <asp:TableCell HorizontalAlign="Left">
                            <asp:HyperLink ID="LienSynthese" runat="server" NavigateUrl="~/Fenetres/FPEtat/FrmPFRSynthese.aspx" Text="Synthèses"
                                BackColor="Transparent" BorderColor="Transparent" BorderStyle="None"  Visible="false"
                                ForeColor="#2F5FBD" Font-Italic="True" Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Small"
                                style="text-align:center;" Height="17px" Width="80px">
                            </asp:HyperLink> 
                        </asp:TableCell>
                        <asp:TableCell HorizontalAlign="Left">
                            <asp:HyperLink ID="LienNotification" runat="server" NavigateUrl="~/Fenetres/FPEtat/FrmPFRNotification.aspx" Text="Notifications"
                                BackColor="Transparent" BorderColor="Transparent" BorderStyle="None"  Visible="false"
                                ForeColor="#2F5FBD" Font-Italic="True" Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Small"
                                style="text-align:center;" Height="17px" Width="80px">
                            </asp:HyperLink> 
                        </asp:TableCell>
                        <asp:TableCell HorizontalAlign="Left">
                            <asp:HyperLink ID="LienAdmin" runat="server" NavigateUrl="~/Fenetres/Commun/FrmTachesAdmin.aspx" Text="Administrer"
                                BackColor="Transparent" BorderColor="Transparent" BorderStyle="None"  Visible="false"
                                ForeColor="#2F5FBD" Font-Italic="True" Font-Bold="False" Font-Names="Trebuchet MS" Font-Size= "Small"
                                style="text-align:center;" Height="17px" Width="80px">
                            </asp:HyperLink> 
                        </asp:TableCell>
                    </asp:TableRow>
                </asp:Table>
            </asp:TableCell>
        </asp:TableRow>
        <asp:TableRow>
            <asp:TableCell>
                <asp:Panel ID="CadreAffichage" runat="server" BackColor="Transparent" HorizontalAlign="Center" Width="1050px"
                     BackImageUrl="~/Images/Fonds/Fond_Saisie.jpg" BorderStyle="NotSet" BorderWidth="4px" BorderColor="#B0E0D7" ScrollBars="Auto">
                    <asp:Table ID="CadreSelection" runat="server" BackColor="Transparent" HorizontalAlign="Center" Width="1000px"
                        borderStyle="None">
                        <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Left">
                                <Virtualia:VTrioHorizontalRadio ID="RadioSelRegime" runat="server" V_Groupe="Regime" V_SiAutoPostBack="true"
                                        V_SiDonneeDico="false" RadioGaucheWidth="90px" RadioCentreWidth="90px" RadioDroiteWidth="90px"  
                                        RadioGaucheText="PFR" RadioCentreText="PS" RadioDroiteText="F_R" 
                                        RadioGaucheHeight="20px" RadioCentreHeight="20px" RadioDroiteHeight="20px"
                                        RadioCentreStyle="text-align: center;" RadioGaucheStyle="text-align: center;" RadioDroiteStyle="text-align: center;" 
			                            RadioGaucheCheck="true" Visible="true"/>
                            </asp:TableCell>
                            <asp:TableCell HorizontalAlign="Left">
                                <asp:Label ID="EtiTotal" runat="server" Height="20px" Width="200px"
                                        BackColor="#B0E0D7" BorderColor="#B0E0D7"  BorderStyle="Outset"
                                        BorderWidth="2px" ForeColor="#142425" Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                        style="margin-top: 0px; margin-left: 4px; font-style: oblique;
                                        text-indent: 5px; text-align: left"></asp:Label>
                            </asp:TableCell>
                            <asp:TableCell HorizontalAlign="Left">
                                <Virtualia:VListeCombo ID="ComboSelTri" runat="server" EtiVisible="true" EtiText="Tri" EtiWidth="65px"
                                     V_NomTable="Tri" LstWidth="150px" LstBorderColor="#B0E0D7" LstBackColor="#EEECFD"
                                     EtiStyle="text-align:center" EtiBackColor="#8DA8A3" EtiForeColor="#E9FDF9" />
                            </asp:TableCell>
                            <asp:TableCell>
                                <asp:Table ID="CadreCsv" runat="server" BackImageUrl="~/Images/Icones/Cmd_Std.bmp" Width="110px"
                                    BorderWidth="2px" BorderStyle="Outset" BorderColor="#B0E0D7" ForeColor="#D7FAF3"
                                    CellPadding="0" CellSpacing="0" Visible="true">
                                    <asp:TableRow>
                                        <asp:TableCell>
                                            <asp:Button ID="CommandeCsv" runat="server" Text="Fichier Csv" Width="95px" Height="20px"
                                                BackColor="Transparent" BorderColor="#B0E0D7" ForeColor="#D7FAF3"  ToolTip="Fichier Excel complet"
                                                Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small" Font-Italic="true"
                                                BorderStyle="None" style=" margin-left: 15px; text-align: left;">
                                            </asp:Button>
                                       </asp:TableCell>
                                    </asp:TableRow>
                                </asp:Table>
                            </asp:TableCell>
                            <asp:TableCell Width="110px">
                                <asp:Table ID="CadreFiger" runat="server" BackImageUrl="~/Images/Icones/Cmd_Std.bmp" Width="110px"
                                    BorderWidth="2px" BorderStyle="Outset" BorderColor="#B0E0D7" ForeColor="#D7FAF3"
                                    CellPadding="0" CellSpacing="0" Visible="false">
                                    <asp:TableRow>
                                        <asp:TableCell>
                                            <asp:Button ID="CommandeFiger" runat="server" Text="Figer la saisie" Width="95px" Height="20px"
                                                BackColor="Transparent" BorderColor="#B0E0D7" ForeColor="#D7FAF3"  ToolTip="Opération consistant à figer les saisies pour le niveau hiérarchique infèrieur"
                                                Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small" Font-Italic="true"
                                                BorderStyle="None" style=" margin-left: 15px; text-align: left;">
                                            </asp:Button>
                                       </asp:TableCell>
                                    </asp:TableRow>
                                </asp:Table>  
                            </asp:TableCell>
                        </asp:TableRow>
                    </asp:Table>
                    <asp:Table ID="CadreFiltre" runat="server" BackColor="Transparent" HorizontalAlign="Center" Width="990px"
                        borderStyle="None">
                        <asp:TableRow>
                            <asp:TableCell>
                                <Virtualia:VListeCombo ID="ComboSelCritere" runat="server" EtiVisible="true" EtiText="Filtre" EtiWidth="80px"
                                     V_NomTable="Filtre" LstWidth="150px" LstBorderColor="#B0E0D7" LstBackColor="#EEECFD"
                                     EtiStyle="text-align:center" EtiBackColor="#8DA8A3" EtiForeColor="#E9FDF9" />
                            </asp:TableCell>
                            <asp:TableCell>
                                <Virtualia:VListeCombo ID="ComboSelValeur" runat="server" EtiVisible="true" EtiText="Valeur du filtre" EtiWidth="120px"
                                     V_NomTable="Valeur" LstWidth="300px" LstBorderColor="#B0E0D7" LstBackColor="#EEECFD"
                                     EtiStyle="text-align:center" EtiBackColor="#8DA8A3" EtiForeColor="#E9FDF9" />
                            </asp:TableCell>
                            <asp:TableCell HorizontalAlign="Left">
                                <asp:Label ID="EtiRechercheNom" runat="server" Height="20px" Width="150px" Text="Recherche sur nom"
                                        BackColor="#B0E0D7" BorderColor="#B0E0D7"  BorderStyle="Outset" Tooltip="Recherche fonctionnant par régime de prime (Bouton radio) sur le pricipe du nom débutant par ..."
                                        BorderWidth="2px" ForeColor="#142425" Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                        style="margin-top: 0px; margin-left: 4px; font-style: oblique;
                                        text-indent: 5px; text-align: left"></asp:Label>
                            </asp:TableCell>
                            <asp:TableCell>
                                <asp:TextBox ID="DonNom" runat="server" Height="16px" Width="120px" BackColor="White" ForeColor="Black"
                                    Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small" AutoPostBack="true"
                                    style="margin-top: 0px; margin-left: 0px; font-style: normal; text-indent: 5px; text-align: left" >
                                </asp:TextBox>
                            </asp:TableCell>
                        </asp:TableRow>
                    </asp:Table>
                    <asp:Table ID="CadreActivite" runat="server" HorizontalAlign="Center" borderStyle="None">
                        <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Center" VerticalAlign="Top">
                                <asp:MultiView ID="MultiVues" runat="server" ActiveViewIndex="0">
                                    <asp:View ID="VueBase100" runat="server">
                                        <Virtualia:VListeGrid ID="ListeBase100" runat="server" CadreWidth="2000px" SiColonneSelect="true"
                                            SiCaseAcocher="false" SiPagination="true" TaillePage="25" />
                                    </asp:View>
                                    <asp:View ID="VueModulation" runat="server">
                                        <Virtualia:VListeGrid ID="ListeModulation" runat="server" CadreWidth="1600px" SiColonneSelect="true"
                                            SiCaseAcocher="false" SiPagination="true" TaillePage="25" />
                                    </asp:View>
                                    <asp:View ID="VueProrata" runat="server">
                                        <Virtualia:VListeGrid ID="ListeProrata" runat="server" CadreWidth="1000px" SiColonneSelect="true"
                                            SiCaseAcocher="false" SiPagination="true" TaillePage="25" />
                                    </asp:View>
                                    <asp:View ID="VueAffectation" runat="server">
                                        <Virtualia:VListeGrid ID="ListeAffectation" runat="server" CadreWidth="1400px" SiColonneSelect="true"
                                            SiCaseAcocher="false"  SiPagination="true" TaillePage="25" />
                                    </asp:View>
                                    <asp:View ID="VueEnveloppe" runat="server">
                                        <Virtualia:VListeGrid ID="ListeEnveloppe" runat="server" CadreWidth="1000px" SiColonneSelect="true"
                                            SiCaseAcocher="false" SiPagination="true" TaillePage="25" />
                                    </asp:View>
                                    <asp:View ID="VuePaye" runat="server">
                                        <Virtualia:VListeGrid ID="ListePaye" runat="server" CadreWidth="2000px" SiColonneSelect="true"
                                            SiCaseAcocher="false" SiPagination="true" TaillePage="25" />
                                    </asp:View>
                                    <asp:View ID="VuePartielle" runat="server">
                                        <Virtualia:VSaisie ID="SaisieLigne" runat="server"/>
                                    </asp:View>
                                    <asp:View ID="VueAttente" runat="server">
                                        <asp:Table ID="CadreAttente" runat="server" BackColor="Transparent">
                                            <asp:TableRow Height="60px">
                                                <asp:TableCell>
                                                    <asp:Label ID="EtiAttente" runat="server" Height="30px" Width="800px" 
                                                                Font-Bold="True" Font-Names="Trebuchet MS" Font-Size="Medium"
                                                                ForeColor="Black" Text="" style="text-align: center">
                                                    </asp:Label>
                                                    <asp:UpdateProgress ID="UpdateAttente" runat="server">
                                                        <ProgressTemplate>
                                                            <img alt="" src="../Images/General/Attente.gif" 
                                                                    style="vertical-align:top; text-align:center; font-family: 'Trebuchet MS'; height: 12px" />
                                                                    ... Veuillez patienter ... 
                                                        </ProgressTemplate>
                                                    </asp:UpdateProgress>
                                                </asp:TableCell>
                                            </asp:TableRow>
                                        </asp:Table>
                                    </asp:View>
                                </asp:MultiView>
                            </asp:TableCell>
                        </asp:TableRow>
                    </asp:Table>
                </asp:Panel>
            </asp:TableCell>
        </asp:TableRow>
        <asp:TableRow>
             <asp:TableCell ID="CellMessage" Visible="false">
                <ajaxToolkit:ModalPopupExtender ID="PopupMsg" runat="server" TargetControlID="HPopupMsg" PopupControlID="PanelMsgPopup" BackgroundCssClass="modalBackground" />
                <asp:Panel ID="PanelMsgPopup" runat="server">
                    <Virtualia:VMessage id="MsgVirtualia" runat="server" />
                </asp:Panel>
                <asp:HiddenField ID="HPopupMsg" runat="server" Value="0" />
            </asp:TableCell>
        </asp:TableRow>
    </asp:Table>
</asp:Content>
