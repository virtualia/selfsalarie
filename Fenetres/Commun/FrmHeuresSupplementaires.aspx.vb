﻿Option Strict On
Option Explicit On
Option Compare Text
Imports Virtualia.Systeme.Evenements
Imports VI = Virtualia.Systeme.Constantes
Public Class FrmHeuresSupplementaires
    Inherits System.Web.UI.Page
    Private WebFct As Virtualia.Net.Controles.WebFonctions
    Private WsNomStateCache As String = "VSelHS"
    Private WsCtl_Cache As WebAppli.SelfV4.CacheHeureSupp
    '************AKR
    Private WsRhDate As Virtualia.Systeme.Fonctions.CalculDates = Nothing
    Private WsDate_Demarrage As String
    'Public WsMoisSelection As Integer

    Private Enum Ivue As Integer
        IHeureSup = 0
        IAstreinte = 1
        IArmoire = 2
    End Enum

    Private ReadOnly Property V_WebFonction As Virtualia.Net.Controles.WebFonctions
        Get
            If WebFct Is Nothing Then
                WebFct = New Virtualia.Net.Controles.WebFonctions(Me, 1)
            End If
            Return WebFct
        End Get
    End Property

    Private Sub FrmHeuresSupplementaires_Init(sender As Object, e As EventArgs) Handles Me.Init
        Dim Msg As String = ""
        Dim NumSession As String = Server.HtmlDecode(Request.QueryString("IDVirtualia"))
        If NumSession <> Session.SessionID Then
            Msg = "Erreur lors de l'identification."
            Response.Redirect("~/Fenetres/Commun/ErreurApplication.aspx?Msg=" & Msg)
            Exit Sub
        End If
        If V_WebFonction.PointeurUtilisateur Is Nothing Then
            Msg = "Erreur de serveur. Vous avez été déconnecté(e)."
            Response.Redirect("~/Fenetres/Commun/ErreurApplication.aspx?Msg=" & Msg)
            Exit Sub
        End If
        If V_WebFonction.VerifierCookie(Me, Session.SessionID) = False Then
            Msg = "Erreur lors de l'identification du dossier accédé. Session invalide."
            Response.Redirect("~/Fenetres/Commun/ErreurApplication.aspx?Msg=" & Msg)
            Exit Sub
        End If
    End Sub

    Private Property CacheVirControle As WebAppli.SelfV4.CacheHeureSupp
        Get
            If Me.ViewState(WsNomStateCache) IsNot Nothing Then
                Return CType(Me.ViewState(WsNomStateCache), WebAppli.SelfV4.CacheHeureSupp)
            End If
            Dim NewCache As WebAppli.SelfV4.CacheHeureSupp = New WebAppli.SelfV4.CacheHeureSupp
            Return NewCache
        End Get
        Set(value As WebAppli.SelfV4.CacheHeureSupp)
            If value Is Nothing Then
                Exit Property
            End If
            If Me.ViewState(WsNomStateCache) IsNot Nothing Then
                Me.ViewState.Remove(WsNomStateCache)
            End If
            Me.ViewState.Add(WsNomStateCache, value)
        End Set
    End Property

    Private Sub FrmHeuresSupplementaires_PreRender(sender As Object, e As EventArgs) Handles Me.PreRender
        Dim UtiSession As Virtualia.Net.Session.SessionModule = CType(V_WebFonction.PointeurUtilisateur.V_PointeurSessionModule, Virtualia.Net.Session.SessionModule)
        If UtiSession Is Nothing Then
            Exit Sub
        End If
        If HPopupSaisie.Value = "1" Then
            PopupSaisie.Show()
            Exit Sub
        End If
        If HPopupDemande.Value = "1" Then
            PopupDemande.Show()
            Exit Sub
        End If
        WsCtl_Cache = CacheVirControle
        If UtiSession.SiAccesGRH = True Then
            CommandeRetour.Visible = True
            CommandeAstreinte.Visible = True
            If WsCtl_Cache.Ide_Dossier = 0 Then
                MultiVues.ActiveViewIndex = Ivue.IArmoire
                Exit Sub
            End If
        ElseIf UtiSession.SiAccesManager = True Then
            CommandeRetour.Visible = False
            CommandeAstreinte.Visible = False
        Else
            CommandeRetour.Visible = False
            CommandeAstreinte.Visible = True
        End If
        Select Case WsCtl_Cache.Index_VueActive
            Case Ivue.IHeureSup
                Call FaireListeChoix()
            Case Ivue.IAstreinte
                PER_ASTREINTE_171.Identifiant = UtiSession.DossierPER.Ide_Dossier
                ''Exit Sub
        End Select
        MultiVues.ActiveViewIndex = WsCtl_Cache.Index_VueActive
    End Sub

    Private Sub FrmHeuresSupplementaires_PreRenderComplete(sender As Object, e As EventArgs) Handles Me.PreRenderComplete
        Dim FichierHtml As String
        FichierHtml = V_WebFonction.PointeurGlobal.VirRepertoireTemporaire & "\" & Session.SessionID & ".html"
        Response.ContentEncoding = System.Text.Encoding.UTF8
        Response.Filter = New Virtualia.Systeme.Outils.FiltreReponseParMemoryStream(Response.Filter, FichierHtml)
    End Sub

    Private Sub Armoire_Dossier_Click(ByVal sender As Object, ByVal e As Systeme.Evenements.DossierClickEventArgs) Handles ArmoirePER.Dossier_Click
        If e.Identifiant = 0 Then
            Exit Sub
        End If
        Dim Dossier As Virtualia.Net.WebAppli.SelfV4.DossierPersonne = CType(V_WebFonction.PointeurGlobal, Virtualia.Net.Session.ObjetGlobal).Dossier(e.Identifiant)
        If Dossier Is Nothing Then
            Exit Sub
        End If
        Dossier.Nom_Prenom_Manager = V_WebFonction.PointeurUtilisateur.V_NomdeConnexion
        CType(V_WebFonction.PointeurUtilisateur.V_PointeurSessionModule, Virtualia.Net.Session.SessionModule).SiAccesManager = True
        CType(V_WebFonction.PointeurUtilisateur.V_PointeurSessionModule, Virtualia.Net.Session.SessionModule).DossierPER = Dossier
        WsCtl_Cache = CacheVirControle
        WsCtl_Cache.Ide_Dossier = e.Identifiant
        WsCtl_Cache.Index_VueActive = Ivue.IHeureSup
        CacheVirControle = WsCtl_Cache
        DropDownMoisAnnee.Items.Clear()
    End Sub


    Private Sub FaireListeChoix()
        Dim UtiSession As Virtualia.Net.Session.SessionModule = CType(V_WebFonction.PointeurUtilisateur.V_PointeurSessionModule, Virtualia.Net.Session.SessionModule)
        If UtiSession Is Nothing Then
            Exit Sub
        End If
        WsCtl_Cache = CacheVirControle
        If DropDownMoisAnnee.Items.Count = 0 Then
            Dim AnneeDebut As Integer = Year(Now)
            '******AKR
            Dim MoisDebut As Integer = Month(Now)

            'Dim MoisDebut As Integer = Month(Now) - 1 
            Dim Mois As Integer
            If MoisDebut = 0 Then
                AnneeDebut -= 1
                MoisDebut = 12
            End If
            WsCtl_Cache.AnneeSelection = AnneeDebut
            WsCtl_Cache.MoisSelection = MoisDebut
            WsCtl_Cache.Ide_Dossier = UtiSession.DossierPER.Ide_Dossier
            WsCtl_Cache.Index_VueActive = Ivue.IHeureSup
            For Mois = MoisDebut To 1 Step -1
                DropDownMoisAnnee.Items.Add(New ListItem(V_WebFonction.ViRhDates.MoisEnClair(Mois) & Strings.Space(1) & AnneeDebut, AnneeDebut & Strings.Format(Mois, "00")))
            Next
            For Mois = 12 To 1 Step -1
                DropDownMoisAnnee.Items.Add(New ListItem(V_WebFonction.ViRhDates.MoisEnClair(Mois) & Strings.Space(1) & (AnneeDebut - 1), (AnneeDebut - 1) & Strings.Format(Mois, "00")))
            Next
            'For Mois = 12 To 1 Step -1
            '    DropDownMoisAnnee.Items.Add(New ListItem(V_WebFonction.ViRhDates.MoisEnClair(Mois) & Strings.Space(1) & (AnneeDebut - 2), (AnneeDebut - 2) & Strings.Format(Mois, "00")))
            'Next
            CacheVirControle = WsCtl_Cache
            UtiSession.DossierPER.VDossier_NewHeuresSup.CreerDetailJournalier(AnneeDebut, MoisDebut)
        End If

        '*********************AKR*****************
        ' Controle pour afficher ou masquer la situation des heures supp de l'année, pour un établissement et pour une date de démarrage 

        If System.Configuration.ConfigurationManager.AppSettings("HS_DemarrageEtablissement") IsNot Nothing Then
            Dim ChaineConfig As String = System.Configuration.ConfigurationManager.AppSettings("HS_DemarrageEtablissement")
            Dim TableauEtabDate(0) As String
            TableauEtabDate = Strings.Split(ChaineConfig, VI.AntiSlash, -1)

            For IndiceT = 0 To TableauEtabDate.Count - 1
                Dim TableauDemarrageEtab(0) As String
                TableauDemarrageEtab = Strings.Split(TableauEtabDate(IndiceT), VI.PointVirgule, -1)
                If TableauDemarrageEtab(0) = UtiSession.DossierPER.VFiche_Etablissement(VirRhDate.DateduJour).Administration Then
                    If TableauDemarrageEtab(1) <> "" Then
                        WsDate_Demarrage = TableauDemarrageEtab(1)
                    Else
                        WsDate_Demarrage = VirRhDate.DateduJour
                    End If
                End If
            Next IndiceT


        End If

        Select Case VirRhDate.ComparerDates(VirRhDate.DateduJour, WsDate_Demarrage)
            Case VI.ComparaisonDates.PlusGrand, VI.ComparaisonDates.Egalite
                EtiIdentité.Text = UtiSession.DossierPER.Civilite & Strings.Space(1) & UtiSession.DossierPER.Nom & Strings.Space(1) & UtiSession.DossierPER.Prenom
                EtiTitreSolde.Text = "Situation des heures supplémentaires de l'année " & WsCtl_Cache.AnneeSelection
                DonSoldeAnte.Text = UtiSession.DossierPER.VDossier_NewHeuresSup.Solde_HS_Anterieur
                DonTotalHS.Text = UtiSession.DossierPER.VDossier_NewHeuresSup.TotalCreditHeuresSup(WsCtl_Cache.AnneeSelection)
                DonTotalRemu.Text = UtiSession.DossierPER.VDossier_NewHeuresSup.TotalRemunerees
                DonTotalRecup.Text = UtiSession.DossierPER.VDossier_NewHeuresSup.TotalRecuperations
                Don2MoisCompensable.Text = UtiSession.DossierPER.VDossier_NewHeuresSup.TotalCompensables
                Don2MoisRecup.Text = UtiSession.DossierPER.VDossier_NewHeuresSup.RecuperationsCourantes
                DonSoldeHS.Text = UtiSession.DossierPER.VDossier_NewHeuresSup.Solde_HS
            Case Else
                EtiIdentité.Text = UtiSession.DossierPER.Civilite & Strings.Space(1) & UtiSession.DossierPER.Nom & Strings.Space(1) & UtiSession.DossierPER.Prenom
                EtiTitreSolde.Text = "Situation des heures supplémentaires de l'année " & WsCtl_Cache.AnneeSelection
                DonSoldeAnte.Text = ""
                DonTotalHS.Text = ""
                DonTotalRemu.Text = ""
                DonTotalRecup.Text = ""
                Don2MoisCompensable.Text = ""
                Don2MoisRecup.Text = ""
                DonSoldeHS.Text = ""
        End Select
        '****************


        Dim LstHeures As List(Of Virtualia.TablesObjet.ShemaPER.PER_DETAIL_HS) = UtiSession.DossierPER.VDossier_NewHeuresSup.ListeDetaille_HS(WsCtl_Cache.AnneeSelection, WsCtl_Cache.MoisSelection)
        Dim LstRecups As List(Of Virtualia.TablesObjet.ShemaPER.PER_ABSENCE)
        Dim LstChaineF As List(Of String)
        Dim LstChaineTip As List(Of String)
        Dim Entete As List(Of String)
        Dim ChaineF As String
        Dim ChaineTip As String
        Dim IndiceI As Integer
        Dim ZCalcul As Double

        '*** Liste détaillée des heures sup du mois sélectionné Grille Personnalisée ************************
        Entete = New List(Of String)
        Entete.Add("Date")
        Entete.Add("Nature")
        Entete.Add("Fait générateur")
        Entete.Add("Heure début")
        Entete.Add("Heure fin")
        Entete.Add("Heures jusqu'à 20h")
        Entete.Add("Heures de 20h à 22h")
        Entete.Add("Heures Nuit")
        Entete.Add("Trajet")
        Entete.Add("CLEF")
        ListeDetailHS.V_LibelColonne = Entete

        IndiceI = 0
        LstChaineF = New List(Of String)
        LstChaineTip = New List(Of String)
        If LstHeures IsNot Nothing Then
            For Each LigneDetail In LstHeures
                ChaineF = V_WebFonction.ViRhDates.ClairDate(LigneDetail.Date_de_Valeur, True) & VI.Tild
                ChaineTip = LigneDetail.Date_de_Valeur & StrDup(2, VI.Tild)
                ChaineF &= LigneDetail.Nature & VI.Tild
                ChaineF &= LigneDetail.FaitGenerateur & VI.Tild
                If LigneDetail.FaitGenerateur <> "" Then
                    ChaineTip &= " certifié par " & LigneDetail.Signataire & " le " & LigneDetail.Date_Validation & VI.Tild
                Else
                    ChaineTip &= VI.Tild
                End If
                ChaineTip &= StrDup(2, VI.Tild)
                ChaineF &= LigneDetail.Heure_Borne & VI.Tild
                ChaineF &= LigneDetail.Heure_Fin & VI.Tild
                ChaineF &= LigneDetail.V_NbHeures_Jusqua20h & VI.Tild
                If LigneDetail.Nbheures_Jusqua20h > 0 Then
                    ChaineTip &= " soit " & LigneDetail.Nbheures_Jusqua20h & VI.Tild
                Else
                    ChaineTip &= VI.Tild
                End If
                ChaineF &= LigneDetail.V_NbHeures_de20ha22h & VI.Tild
                If LigneDetail.Nbheures_De20ha22h > 0 Then
                    ChaineTip &= " soit " & LigneDetail.Nbheures_De20ha22h & VI.Tild
                Else
                    ChaineTip &= VI.Tild
                End If
                ChaineF &= LigneDetail.V_NbHeures_deNuit & VI.Tild
                If LigneDetail.Nbheures_Nuit > 0 Then
                    ChaineTip &= " soit " & LigneDetail.Nbheures_Nuit & VI.Tild
                Else
                    ChaineTip &= VI.Tild
                End If
                ChaineF &= LigneDetail.V_NbHeures_Trajet & VI.Tild
                'ChaineTip &= LigneDetail.Nbheures_Trajet & VI.Tild & VI.Tild
                ChaineF &= LigneDetail.Date_de_Valeur
                '************************AKR********************
                ChaineTip &= LigneDetail.Nbheures_Trajet & VI.Tild
                ChaineTip &= LigneDetail.SiValide_Par_Manager
                '***********************************************
                LstChaineF.Add(ChaineF)
                LstChaineTip.Add(ChaineTip)

                IndiceI += 1
            Next
        End If
        ListeDetailHS.V_Liste = LstChaineF
        ListeDetailHS.V_ListeToolTip = LstChaineTip
        If CDate("01/" & Strings.Format(WsCtl_Cache.MoisSelection, "00") & VI.Slash & WsCtl_Cache.AnneeSelection).AddMonths(1) <V_WebFonction.ViRhDates.DateTypee("01" & Strings.Right(V_WebFonction.ViRhDates.DateduJour, 8)) Then
            ListeDetailHS.NumColonneSiCmdVisible = 2
        ElseIf UtiSession.SiAccesGRH = True Or UtiSession.SiAccesManager = True Then
            ListeDetailHS.NumColonneSiCmdVisible = -1
        End If
        '*************************************************************************
        LstRecups = UtiSession.DossierPER.VDossier_NewHeuresSup.ListeRecuperations(WsCtl_Cache.AnneeSelection, WsCtl_Cache.MoisSelection)

        Entete = New List(Of String)
        Entete.Add("Date")
        Entete.Add("Durée en jours")
        Entete.Add("Nombre d'heures")
        Entete.Add("CLEF")
        ListeRecup.V_LibelColonne = Entete
        For IndiceI = 0 To 3
            ListeRecup.Centrage_Colonne(IndiceI) = 1
        Next IndiceI
        LstChaineF = New List(Of String)
        If LstRecups IsNot Nothing Then
            For Each PerAbsence In LstRecups
                ChaineF = PerAbsence.Date_de_Valeur & VI.Tild
                ChaineF &= Strings.Format(PerAbsence.JoursOuvres, "0.00") & VI.Tild
                ZCalcul = PerAbsence.JoursOuvres * UtiSession.DossierPER.VDossier_HeuresSup.HoraireJournalier(PerAbsence.Date_de_Valeur)
                ChaineF &= V_WebFonction.ViRhDates.CalcHeure(CStr(CInt(ZCalcul)), "", 2) & VI.Tild
                ChaineF &= PerAbsence.Date_de_Valeur
                LstChaineF.Add(ChaineF)
            Next
        End If
        ListeRecup.V_Liste = LstChaineF

        HsMensuel.V_DateValeur = "01/" & Strings.Format(WsCtl_Cache.MoisSelection, "00") & VI.Slash & WsCtl_Cache.AnneeSelection
    End Sub

    Private Sub DropDown_SelectedIndexChanged(sender As Object, e As EventArgs) Handles DropDownMoisAnnee.SelectedIndexChanged
        Dim UtiSession As Virtualia.Net.Session.SessionModule = CType(V_WebFonction.PointeurUtilisateur.V_PointeurSessionModule, Virtualia.Net.Session.SessionModule)
        If UtiSession Is Nothing Then
            Exit Sub
        End If
        Dim Annee As Integer = CInt(Strings.Left(CType(sender, System.Web.UI.WebControls.DropDownList).SelectedItem.Value, 4))
        Dim Mois As Integer = CInt(Strings.Right(CType(sender, System.Web.UI.WebControls.DropDownList).SelectedItem.Value, 2))
        UtiSession.DossierPER.VDossier_NewHeuresSup.CreerDetailJournalier(Annee, Mois)
        WsCtl_Cache = CacheVirControle
        WsCtl_Cache.AnneeSelection = Annee
        WsCtl_Cache.MoisSelection = Mois
        CacheVirControle = WsCtl_Cache
    End Sub

    Private Sub ListeDetailHS_ValeurChange(sender As Object, e As DonneeChangeEventArgs) Handles ListeDetailHS.ValeurChange
        HPopupSaisie.Value = "1"
        CellSaisieOri.Visible = True
        SaisieFaitGen.AfficherDialogue = e
        PopupSaisie.Show()
    End Sub

    Private Sub SaisieFaitGen_ValeurRetour(sender As Object, e As DonneeChangeEventArgs) Handles SaisieFaitGen.ValeurRetour
        HPopupSaisie.Value = "0"
        CellSaisieOri.Visible = False
    End Sub

    Private Sub CommandeRetour_Click(sender As Object, e As ImageClickEventArgs) Handles CommandeRetour.Click
        WsCtl_Cache = CacheVirControle
        WsCtl_Cache.Index_VueActive = Ivue.IArmoire
        CacheVirControle = WsCtl_Cache
    End Sub

    Private Sub HsMensuel_DemandePaieHS(sender As Object, e As DonneeChangeEventArgs) Handles HsMensuel.DemandePaieHS
        WsCtl_Cache = CacheVirControle
        HPopupDemande.Value = "1"
        CellSaisieHS.Visible = True
        SaisieDemande.V_DateValeur = "01/" & Strings.Format(WsCtl_Cache.MoisSelection, "00") & VI.Slash & WsCtl_Cache.AnneeSelection
        PopupDemande.Show()
    End Sub

    Private Sub SaisieDemande_ValeurRetour(sender As Object, e As DonneeChangeEventArgs) Handles SaisieDemande.ValeurRetour
        HPopupDemande.Value = "0"
        CellSaisieHS.Visible = False
    End Sub

    Private Sub CommandeAstreinte_Click(sender As Object, e As EventArgs) Handles CommandeAstreinte.Click
        WsCtl_Cache = CacheVirControle
        WsCtl_Cache.Index_VueActive = Ivue.IAstreinte
        CacheVirControle = WsCtl_Cache
    End Sub

    Private Sub PER_ASTREINTE_171_ValeurRetour(sender As Object, e As DonneeChangeEventArgs) Handles PER_ASTREINTE_171.ValeurRetour
        WsCtl_Cache = CacheVirControle
        WsCtl_Cache.Index_VueActive = Ivue.IHeureSup
        CacheVirControle = WsCtl_Cache
    End Sub
    '*******************AKR
    Public ReadOnly Property VirRhDate() As Virtualia.Systeme.Fonctions.CalculDates
        Get
            If WsRhDate Is Nothing Then
                WsRhDate = New Virtualia.Systeme.Fonctions.CalculDates
            End If
            Return WsRhDate
        End Get
    End Property
End Class