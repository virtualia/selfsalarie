﻿Option Strict On
Option Explicit On
Option Compare Text
Imports VI = Virtualia.Systeme.Constantes
Public Class VBulletinPaie
    Inherits System.Web.UI.UserControl
    Private WebFct As Virtualia.Net.Controles.WebFonctions
    Private WsNomState As String = "Bulletin"

    Public Property V_Identifiant As Integer
        Get
            Dim VCache As ArrayList
            If Me.ViewState(WsNomState) IsNot Nothing Then
                VCache = CType(Me.ViewState(WsNomState), ArrayList)
                Return CInt(VCache(0))
            End If
            Return 0
        End Get
        Set(ByVal value As Integer)
            Dim VCache As ArrayList
            If Me.ViewState(WsNomState) IsNot Nothing Then
                VCache = CType(Me.ViewState(WsNomState), ArrayList)
                If CInt(VCache(0)) = value Then
                    Exit Property
                End If
                VCache(0) = value
                VCache(1) = ""
                VCache(2) = ""
                VCache(3) = ""
                Me.ViewState.Remove(WsNomState)
            Else
                VCache = New ArrayList
                VCache.Add(value) 'Identifiant
                VCache.Add("") 'Année
                VCache.Add("") 'Mois
                VCache.Add("") 'NumDos
            End If
            Me.ViewState.Add(WsNomState, VCache)
        End Set
    End Property

    Public Property V_Annee As String
        Get
            Dim VCache As ArrayList
            If Me.ViewState(WsNomState) IsNot Nothing Then
                VCache = CType(Me.ViewState(WsNomState), ArrayList)
                Return VCache(1).ToString
            End If
            Return ""
        End Get
        Set(ByVal value As String)
            Dim VCache As ArrayList
            If Me.ViewState(WsNomState) IsNot Nothing Then
                VCache = CType(Me.ViewState(WsNomState), ArrayList)
                VCache(1) = value
                Me.ViewState.Remove(WsNomState)
                Me.ViewState.Add(WsNomState, VCache)
            End If
        End Set
    End Property

    Public Property V_Mois As String
        Get
            Dim VCache As ArrayList
            If Me.ViewState(WsNomState) IsNot Nothing Then
                VCache = CType(Me.ViewState(WsNomState), ArrayList)
                Return VCache(2).ToString
            End If
            Return ""
        End Get
        Set(ByVal value As String)
            Dim VCache As ArrayList
            If Me.ViewState(WsNomState) IsNot Nothing Then
                VCache = CType(Me.ViewState(WsNomState), ArrayList)
                VCache(2) = value
                Me.ViewState.Remove(WsNomState)
                Me.ViewState.Add(WsNomState, VCache)
            End If
        End Set
    End Property

    Public Property V_NumDos As String
        Get
            Dim VCache As ArrayList
            If Me.ViewState(WsNomState) IsNot Nothing Then
                VCache = CType(Me.ViewState(WsNomState), ArrayList)
                Return VCache(3).ToString
            End If
            Return ""
        End Get
        Set(ByVal value As String)
            Dim VCache As ArrayList
            If Me.ViewState(WsNomState) IsNot Nothing Then
                VCache = CType(Me.ViewState(WsNomState), ArrayList)
                VCache(3) = value
                Me.ViewState.Remove(WsNomState)
                Me.ViewState.Add(WsNomState, VCache)
            End If
        End Set
    End Property

    Private Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        Call AfficherBulletin()
    End Sub

    Private Sub AfficherBulletin()
        If V_Identifiant = 0 Then
            Call Initialiser()
            Exit Sub
        End If
        WebFct = New Virtualia.Net.Controles.WebFonctions(Me, 0)
        Dim UtiSession As Virtualia.Net.Session.SessionModule = CType(WebFct.PointeurUtilisateur.V_PointeurSessionModule, Virtualia.Net.Session.SessionModule)
        If UtiSession Is Nothing Then
            Exit Sub
        End If
        Dim Dossier As Virtualia.Net.WebAppli.SelfV4.DossierPersonne = UtiSession.DossierPER
        Dim FicheCTX As Virtualia.TablesObjet.ShemaPER.PER_CONTEXTEPAIE = Nothing
        Dim LstCtxPaie As List(Of Virtualia.TablesObjet.ShemaPER.PER_CONTEXTEPAIE)
        Dim Mois As Integer
        Dim Annee As Integer
        Dim NumDos As String
        Dim DateDebut As String
        Dim DateFin As String

        If Dossier Is Nothing Then
            Call Initialiser()
            Exit Sub
        End If
        EtiIdentite.Text = Dossier.Nom & Strings.Space(1) & Dossier.Prenom

        LstCtxPaie = Dossier.VDossier_Bulletin.ListeContextes
        If LstCtxPaie Is Nothing Then
            Call Initialiser()
            Exit Sub
        End If

        If IsNumeric(V_Annee) And IsNumeric(V_Mois) Then
            Annee = CInt(V_Annee)
            Mois = CInt(V_Mois)
            NumDos = V_NumDos
        Else
            Call FaireListeMois(LstCtxPaie)
            Annee = CInt(Strings.Right(LstCtxPaie.Item(0).Date_de_Valeur, 4))
            Mois = CInt(Strings.Mid(LstCtxPaie.Item(0).Date_de_Valeur, 4, 2))
            NumDos = LstCtxPaie.Item(LstCtxPaie.Count - 1).Contexte23
        End If
        DateDebut = "01/" & Strings.Format(Mois, "00") & "/" & Annee.ToString
        DateFin = WebFct.ViRhDates.DateSaisieVerifiee("31" & Strings.Right(DateDebut, 8))

        For Each CTXPaie In LstCtxPaie
            Select Case NumDos
                Case ""
                    If CTXPaie.Date_Valeur_ToDate >= CDate(DateDebut) And CTXPaie.Date_Valeur_ToDate < CDate(DateFin) Then
                        FicheCTX = CTXPaie
                        Exit For
                    End If
                Case Else
                    If CTXPaie.Date_Valeur_ToDate >= CDate(DateDebut) And CTXPaie.Date_Valeur_ToDate < CDate(DateFin) And CTXPaie.Contexte23 = NumDos Then
                        FicheCTX = CTXPaie
                        Exit For
                    End If
            End Select
        Next
        If FicheCTX Is Nothing Then
            Call Initialiser()
            Exit Sub
        End If
        DonGestion.Text = FicheCTX.Contexte22 & FicheCTX.Etablissement
        DonCodePoste.Text = FicheCTX.Contexte20
        DonLibellePoste.Text = FicheCTX.V_Emploi_Poste
        DonMisEnPaiement.Text = FicheCTX.Contexte06

        DonNIR.Text = Strings.Left(FicheCTX.V_NIRComplet, 13)
        DonClefNIR.Text = Strings.Right(FicheCTX.V_NIRComplet, 2)
        DonNumDos.Text = FicheCTX.Contexte23
        DonGrade.Text = FicheCTX.V_Grade
        DonEchelon.Text = FicheCTX.V_Echelon
        DonIndice.Text = FicheCTX.V_IndiceTraitement.ToString
        DonNBI.Text = FicheCTX.V_NBI
        DonEnfants.Text = FicheCTX.V_NbEnfants_ACharge
        DonQuotite.Text = FicheCTX.V_Taux_Activite.ToString

        Dim Colonnes As New List(Of String)

        Colonnes.Add("Code")
        Colonnes.Add("Rubrique")
        Colonnes.Add("Gain")
        Colonnes.Add("Retenue")
        Colonnes.Add("Employeur")
        Colonnes.Add("Clef")
        GridBulletin.V_LibelColonne = Colonnes

        Dim IndiceI As Integer
        Dim Chaine As String
        Dim LstRes As List(Of String)
        Dim LstBulAnnee As List(Of Virtualia.TablesObjet.ShemaPER.PER_BULLETIN)
        Dim LstBulMois As List(Of Virtualia.TablesObjet.ShemaPER.PER_BULLETIN) = Nothing
        Dim FicheRef As Virtualia.TablesObjet.ShemaREF.OUT_RUBRIQUES
        Dim SiaFaire As Boolean
        Dim MontantNum As Double = 0
        Dim MontantAlpha As String
        Dim TotalBrut As Double = 0
        Dim TotalPO As Double = 0
        Dim TotalPP As Double = 0
        Dim TotalIMPOT As Double = 0
        Dim NetAPayer As Double = 0
        Dim Imposable As Double = 0

        LstBulAnnee = Dossier.VDossier_Bulletin.ListeBulletins(Annee.ToString)

        LstBulMois = (From Ligne In LstBulAnnee Where Ligne.Date_Valeur_ToDate = FicheCTX.Date_Valeur_ToDate
                      Order By Ligne.V_TriCategorie Ascending).ToList

        If LstBulMois Is Nothing Then
            Call Initialiser()
            Exit Sub
        End If

        LstRes = New List(Of String)

        For IndiceI = 0 To LstBulMois.Count - 1
            SiaFaire = True
            Select Case LstBulMois.Item(IndiceI).Categorie
                Case Is < 5
                    SiaFaire = False
                    If LstBulMois.Item(IndiceI).Categorie = 4 Then
                        NetAPayer = LstBulMois.Item(IndiceI).Montant
                    End If
                Case 50 To 52
                    SiaFaire = False
                    If LstBulMois.Item(IndiceI).Categorie = 50 Then
                        Imposable = LstBulMois.Item(IndiceI).Montant
                    End If
            End Select
            If SiaFaire = True Then
                MontantAlpha = ""
                MontantNum = 0
                If LstBulMois.Item(IndiceI).Montant <> 0 Then
                    MontantNum = LstBulMois.Item(IndiceI).Montant
                    Select Case LstBulMois.Item(IndiceI).Categorie
                        Case VI.CategoriePaie.Précompte_Retenues, VI.CategoriePaie.Mutuelles, VI.CategoriePaie.Prets, VI.CategoriePaie.Compensation_Charges
                            If FicheCTX.Ide_paie = VI.IdentifiantPaie.GAPAIE Then
                                MontantNum = MontantNum * -1
                            End If
                    End Select
                    MontantAlpha = Strings.Format(MontantNum, "0.00")
                End If

                Chaine = LstBulMois.Item(IndiceI).Rubrique & VI.Tild
                FicheRef = UtiSession.VPointeurGlobal.FicheRubrique(LstBulMois.Item(IndiceI).Rubrique)
                If FicheRef IsNot Nothing Then
                    Chaine &= FicheRef.Intitule & VI.Tild
                Else
                    Chaine &= VI.Tild
                End If
                Select Case LstBulMois.Item(IndiceI).Categorie
                    Case 5 'Retenues diverses
                        Chaine &= VI.Tild
                        Chaine &= MontantAlpha & Strings.StrDup(2, VI.Tild)
                    Case 6 'Acompte
                        Chaine &= MontantAlpha & Strings.StrDup(3, VI.Tild)
                    Case 10 To 19 'Brut
                        Chaine &= MontantAlpha & Strings.StrDup(3, VI.Tild)
                        TotalBrut += MontantNum
                    Case 20 To 29 'PO
                        Chaine &= VI.Tild
                        Chaine &= MontantAlpha & Strings.StrDup(2, VI.Tild)
                        TotalPO += MontantNum
                    Case 30 To 39 'PP
                        Chaine &= Strings.StrDup(2, VI.Tild)
                        Chaine &= MontantAlpha & VI.Tild
                        TotalPP += MontantNum
                    Case 53 'Prêts
                        Chaine &= VI.Tild
                        Chaine &= MontantAlpha & Strings.StrDup(3, VI.Tild)
                    Case 54 'Mutuelle
                        Chaine &= VI.Tild
                        Chaine &= MontantAlpha & Strings.StrDup(3, VI.Tild)
                    Case Else 'Idem PP
                        Chaine &= Strings.StrDup(2, VI.Tild)
                        Chaine &= MontantAlpha & VI.Tild
                        TotalPP += MontantNum
                End Select
                Chaine &= LstBulMois.Item(IndiceI).Rubrique
                LstRes.Add(Chaine)
            End If
        Next IndiceI
        '****
        LstRes.Add("NET" & Strings.StrDup(5, VI.Tild) & "NET")
        Chaine = "Net à Payer" & Strings.StrDup(2, VI.Tild) & Strings.Format(NetAPayer, "0.00")
        Chaine &= Strings.StrDup(3, VI.Tild) & "Net_Paie"
        LstRes.Add(Chaine)
        Chaine = "Montant imposable" & Strings.StrDup(2, VI.Tild) & Strings.Format(Imposable, "0.00")
        Chaine &= Strings.StrDup(3, VI.Tild) & "Imposable"
        LstRes.Add(Chaine)
        '*** Totaux
        Chaine = "TOTAUX" & Strings.StrDup(5, VI.Tild) & "TOTAUX"
        LstRes.Add(Chaine)
        Chaine = "Total Brut" & Strings.StrDup(2, VI.Tild) & Strings.Format(TotalBrut, "0.00")
        Chaine &= Strings.StrDup(3, VI.Tild) & "Total_Brut" & VI.SigneBarre
        LstRes.Add(Chaine)

        Chaine = "Total Parts Ouvrières" & Strings.StrDup(3, VI.Tild)
        Chaine &= Strings.Format(TotalPO, "0.00") & VI.Tild & VI.Tild & "Total_PO"
        LstRes.Add(Chaine)

        Chaine = "Total Parts Patronales" & Strings.StrDup(4, VI.Tild)
        Chaine &= Strings.Format(TotalPP, "0.00") & VI.Tild & "Total_PP"
        LstRes.Add(Chaine)

        Chaine = "Coût budgétaire" & Strings.StrDup(4, VI.Tild)
        Chaine &= Strings.Format(TotalBrut + TotalPP, "0.00") & VI.Tild & "Cout_Bud"
        LstRes.Add(Chaine)
        '*** Cumuls
        Chaine = "CUMULS" & Strings.StrDup(5, VI.Tild) & "CUMULS"
        LstRes.Add(Chaine)
        MontantAlpha = ""
        If FicheCTX.Cumul_Brut > 0 Then
            TotalBrut = FicheCTX.Cumul_Brut
        Else
            TotalBrut = Dossier.VDossier_Bulletin.Cumuls_Virtualia(DateFin, "BRUT")
        End If
        MontantAlpha = Strings.Format(TotalBrut, "0.00")
        Chaine = "Cumul Brut" & Strings.StrDup(2, VI.Tild) & MontantAlpha
        Chaine &= Strings.StrDup(3, VI.Tild) & "Cumul_Brut" & VI.SigneBarre
        LstRes.Add(Chaine)

        TotalIMPOT = Dossier.VDossier_Bulletin.Cumuls_Virtualia(DateFin, "IMPOT")
        Chaine = "Cumul Imposable" & Strings.StrDup(2, VI.Tild) & Strings.Format(TotalIMPOT, "0.00")
        Chaine &= Strings.StrDup(3, VI.Tild) & "Cumul_Impot"
        LstRes.Add(Chaine)

        '** Ajout de cumuls calculés
        TotalPO = Dossier.VDossier_Bulletin.Cumuls_Virtualia(DateFin, "PO")
        Chaine = "Cumul Parts Ouvrières" & Strings.StrDup(3, VI.Tild)
        Chaine &= Strings.Format(TotalPO, "0.00") & VI.Tild & VI.Tild & "Cumul_VPO"
        LstRes.Add(Chaine)

        TotalPP = Dossier.VDossier_Bulletin.Cumuls_Virtualia(DateFin, "PP")
        Chaine = "Cumul Parts Patronales" & Strings.StrDup(4, VI.Tild)
        Chaine &= Strings.Format(TotalPP, "0.00") & VI.Tild & "Cumul_VPP"
        LstRes.Add(Chaine)

        Chaine = "Cumul Coûts budgétaires" & Strings.StrDup(4, VI.Tild)
        Chaine &= Strings.Format(TotalBrut + TotalPP, "0.00") & VI.Tild & "Cumul_VBud"
        LstRes.Add(Chaine)

        For IndiceI = 0 To 5
            Select Case IndiceI
                Case Is > 1
                    GridBulletin.Centrage_Colonne(IndiceI) = 2
                Case Else
                    GridBulletin.Centrage_Colonne(IndiceI) = 0
            End Select
        Next IndiceI

        GridBulletin.V_Liste = LstRes
    End Sub

    Protected Sub LstLibelMois_ValeurChange(ByVal sender As Object, ByVal e As Virtualia.Systeme.Evenements.DonneeChangeEventArgs) Handles LstLibelMois.ValeurChange
        Dim TableauW(0) As String
        If WebFct Is Nothing Then
            WebFct = New Virtualia.Net.Controles.WebFonctions(Me, 0)
        End If
        EtiSelMois.Text = e.Valeur
        TableauW = Strings.Split(e.Valeur, Strings.Space(1), -1)
        V_Annee = TableauW(1)
        V_Mois = WebFct.ViRhDates.IndexduMois(TableauW(0)).ToString
        If TableauW.Count > 3 Then
            V_NumDos = TableauW(3)
        Else
            V_NumDos = ""
        End If
    End Sub

    Private Sub FaireListeMois(ByVal LstContexte As List(Of Virtualia.TablesObjet.ShemaPER.PER_CONTEXTEPAIE))
        If LstContexte Is Nothing Then
            Exit Sub
        End If
        Dim Chaine As New System.Text.StringBuilder
        Dim Libel As String = "Aucune paie" & VI.Tild & "Une paie" & VI.Tild & "paies"
        Dim LstCtxTrie As List(Of Virtualia.TablesObjet.ShemaPER.PER_CONTEXTEPAIE)
        Dim ListeDyna As New List(Of String)
        Dim MoisPaie As String
        Dim Rupture As String = ""
        Dim RuptNumDos As String = ""

        If WebFct Is Nothing Then
            WebFct = New Virtualia.Net.Controles.WebFonctions(Me, 0)
        End If
        LstCtxTrie = (From CtxPaie In LstContexte Order By CtxPaie.Date_Valeur_ToDate Descending).ToList
        For Each Ctx In LstContexte
            MoisPaie = WebFct.ViRhDates.MoisEnClair(CShort(Ctx.Date_Valeur_ToDate.Month)) & Strings.Space(1) & Ctx.Date_Valeur_ToDate.Year
            Select Case MoisPaie
                Case = Rupture
                    ListeDyna(ListeDyna.Count - 1) = MoisPaie & " Dossier " & RuptNumDos
                    MoisPaie &= " Dossier " & Ctx.Contexte23
            End Select
            ListeDyna.Add(MoisPaie)
            Rupture = WebFct.ViRhDates.MoisEnClair(CShort(Ctx.Date_Valeur_ToDate.Month)) & Strings.Space(1) & Ctx.Date_Valeur_ToDate.Year
            RuptNumDos = Ctx.Contexte23
        Next
        For Each Element In ListeDyna
            Chaine.Append(Element & VI.Tild)
        Next
        LstLibelMois.V_Liste(Libel) = Chaine.ToString
        LstLibelMois.LstText = WebFct.ViRhDates.MoisEnClair(CInt(Strings.Mid(LstContexte.Item(0).Date_de_Valeur, 4, 2))) & Strings.Space(1) _
                          & Strings.Right(LstContexte.Item(0).Date_de_Valeur, 4)
        EtiSelMois.Text = WebFct.ViRhDates.MoisEnClair(CInt(Strings.Mid(LstContexte.Item(0).Date_de_Valeur, 4, 2))) & Strings.Space(1) _
                          & Strings.Right(LstContexte.Item(0).Date_de_Valeur, 4)

        V_Annee = Strings.Right(LstContexte.Item(0).Date_de_Valeur, 4)
        V_Mois = Strings.Mid(LstContexte.Item(0).Date_de_Valeur, 4, 2)
        V_NumDos = LstContexte.Item(0).Contexte23
    End Sub

    Private Sub Initialiser()
        DonGestion.Text = ""
        DonCodePoste.Text = ""
        DonLibellePoste.Text = ""
        DonMisEnPaiement.Text = ""
        DonNIR.Text = ""
        DonClefNIR.Text = ""
        DonNumDos.Text = ""
        DonGrade.Text = ""
        DonEchelon.Text = ""
        DonIndice.Text = ""
        DonNBI.Text = ""
        DonEnfants.Text = ""
        DonQuotite.Text = ""
        GridBulletin.V_Liste = Nothing
    End Sub
End Class