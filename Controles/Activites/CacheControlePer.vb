﻿Option Explicit On
Option Strict Off
Option Compare Text
Namespace Controles
    <Serializable>
    Public Class CacheControleActivite
        Private WsValeurSelection As String = ""
        Private WsClef As String = ""
        Private WsDateValeur As String = ""
        Private WsListeSelection As List(Of String) = Nothing
        Private WsTag As String = ""
        Private WsChoix As String

        Public Property ValeurSelection As String
            Get
                Return WsValeurSelection
            End Get
            Set(value As String)
                WsValeurSelection = value
            End Set
        End Property

        Public Property Clef As String
            Get
                Return WsClef
            End Get
            Set(value As String)
                WsClef = value
            End Set
        End Property

        Public Property DateValeur As String
            Get
                Return WsDateValeur
            End Get
            Set(value As String)
                WsDateValeur = value
            End Set
        End Property

        Public ReadOnly Property DateValeur_ToDate As Date
            Get
                If WsDateValeur = "" Then
                    Return Nothing
                End If
                Return CDate(DateValeur)
            End Get
        End Property

        Public Property ListeSelection As List(Of String)
            Get
                Return WsListeSelection
            End Get
            Set(value As List(Of String))
                WsListeSelection = value
            End Set
        End Property

        Public Property Tag As String
            Get
                Return WsTag
            End Get
            Set(value As String)
                WsTag = value
            End Set
        End Property

        Public Property Choix As String
            Get
                Return WsChoix
            End Get
            Set(value As String)
                WsChoix = value
            End Set
        End Property
    End Class
End Namespace