﻿Option Strict On
Option Explicit On
Option Compare Text
Imports VI = Virtualia.Systeme.Constantes
Public Class PER_ACTIVITE_HONDA
    Inherits Virtualia.Net.Controles.ObjetWebControle
    Private WsNumObjet As Integer = 110
    Private WsNomTable As String = "PER_ACTIVITE_HONDA"
    Private WsFiche As Virtualia.TablesObjet.ShemaPER.PER_ACTIVITE_HONDA

    Public ReadOnly Property MonCadre() As System.Web.UI.Control
        Get
            Return Me.CadreInfo
        End Get
    End Property

    Public WriteOnly Property CadreStyle() As String
        Set(ByVal value As String)
            Dim TableauData(0) As String
            Dim TableauW(0) As String
            Dim IndiceI As Integer
            TableauData = Strings.Split(value, ";")
            For IndiceI = 0 To TableauData.Count - 1
                If TableauData(IndiceI) = "" Then
                    Exit For
                End If
                TableauW = Strings.Split(TableauData(IndiceI), ":")
                CadreInfo.Style.Remove(Strings.Trim(TableauW(0)))
                CadreInfo.Style.Add(Strings.Trim(TableauW(0)), Strings.Trim(TableauW(1)))
            Next IndiceI
        End Set
    End Property

    Public WriteOnly Property Identifiant() As Integer
        Set(ByVal value As Integer)
            If V_Identifiant <> value Then

                Dim ColHisto As New List(Of Integer)
                ColHisto.Add(0)
                ColHisto.Add(1)
                ColHisto.Add(12)
                ColHisto.Add(13)
                ColHisto.Add(8)
                V_CacheColHisto = ColHisto

                V_Identifiant = value
                Call ActualiserListe()
            End If
        End Set
    End Property

    Private Sub ActualiserListe()
        CadreCmdOK.Visible = False
        Dim LstLibels As New List(Of String)
        LstLibels.Add("Aucune activité")
        LstLibels.Add("Une activité")
        LstLibels.Add("activités")
        ListeGrille.V_LibelCaption = LstLibels

        Dim LstColonnes As New List(Of String)
        LstColonnes.Add("date de valeur")
        LstColonnes.Add("nature")
        LstColonnes.Add("montant")
        LstColonnes.Add("Droit à récupération")
        LstColonnes.Add("Validation")
        LstColonnes.Add("Clef")
        ListeGrille.V_LibelColonne = LstColonnes

        If V_Identifiant > 0 Then
            Call InitialiserControles()
            If V_IndexFiche = -1 Then
                ListeGrille.V_Liste = Nothing
                Exit Sub
            End If
            ListeGrille.V_Liste = V_ListeFiches
        End If
    End Sub

    Protected Sub ListeGrille_ValeurChange(ByVal sender As Object, ByVal e As Virtualia.Systeme.Evenements.DonneeChangeEventArgs) Handles ListeGrille.ValeurChange
        If e.Valeur = "" Then
            Exit Sub
        End If
        V_Occurence = e.Valeur
    End Sub

    Private Sub LireLaFiche()
        Dim CacheDonnee As List(Of String) = V_CacheMaj
        If CacheDonnee Is Nothing Then
            Exit Sub
        End If
        Dim NumInfo As Integer
        Dim Ctl As Control
        Dim VirControle As Controles_VCoupleEtiDonnee
        Dim IndiceI As Integer = 0
        Dim SiEnLecture As Boolean = False
        Dim SiManager As Boolean = CType(V_WebFonction.PointeurUtilisateur.V_PointeurSessionModule, Virtualia.Net.Session.SessionModule).SiAccesManager
        Dim DateLimite As String

        DateLimite = V_WebFonction.ViRhDates.CalcDateMoinsJour(V_WebFonction.ViRhDates.DateduJour, "0", "180")
        Calendrier.Visible = True
        If CacheDonnee(10) IsNot Nothing Then
            If CacheDonnee(10).ToString <> "" Then
                If V_WebFonction.ViRhDates.ComparerDates(CacheDonnee(10).ToString, DateLimite) = VI.ComparaisonDates.PlusPetit Then
                    Calendrier.Visible = False
                    SiEnLecture = True
                End If
            End If
        End If
        Do
            Ctl = V_WebFonction.VirWebControle(Me.CadreInfo, "InfoH", IndiceI)
            If Ctl Is Nothing Then
                Exit Do
            End If
            NumInfo = CInt(Strings.Right(Ctl.ID, 2))
            VirControle = CType(Ctl, Controles_VCoupleEtiDonnee)
            If CacheDonnee(NumInfo) Is Nothing Then
                VirControle.DonText = ""
            Else
                VirControle.DonText = CacheDonnee(NumInfo).ToString
            End If
            VirControle.V_SiEnLectureSeule = SiEnLecture
            VirControle.V_SiAutoPostBack = Not (CadreCmdOK.Visible)
            Select Case NumInfo
                Case 0, 6, 10
                    VirControle.V_SiEnLectureSeule = True
            End Select
            IndiceI += 1
        Loop

        IndiceI = 0
        Dim VirDonneeVerticale As Controles_VCoupleVerticalEtiDonnee
        Do
            Ctl = V_WebFonction.VirWebControle(Me.CadreInfo, "InfoV", IndiceI)
            If Ctl Is Nothing Then
                Exit Do
            End If
            NumInfo = CInt(Strings.Right(Ctl.ID, 2))
            VirDonneeVerticale = CType(Ctl, Controles_VCoupleVerticalEtiDonnee)
            If CacheDonnee(NumInfo) Is Nothing Then
                VirDonneeVerticale.DonText = ""
            Else
                VirDonneeVerticale.DonText = CacheDonnee(NumInfo).ToString
            End If
            VirDonneeVerticale.V_SiEnLectureSeule = SiEnLecture
            If NumInfo = 5 Then
                Select Case SiManager
                    Case True
                        VirDonneeVerticale.V_SiEnLectureSeule = True
                    Case False
                        Select Case SiEnLecture
                            Case True
                                If CacheDonnee(8).ToString = "Accord préalable" Then
                                    VirDonneeVerticale.V_SiEnLectureSeule = False
                                    CadreCmdOK.Visible = True
                                Else
                                    CadreCmdOK.Visible = False
                                End If
                        End Select
                End Select
            End If
            If NumInfo = 9 And SiManager = False Then
                VirDonneeVerticale.V_SiEnLectureSeule = True
            End If
            IndiceI += 1
        Loop

        Dim VirRadioV As VirtualiaControle_VTrioVerticalRadio
        IndiceI = 0
        Do
            Ctl = V_WebFonction.VirWebControle(Me.CadreInfo, "RadioV", IndiceI)
            If Ctl Is Nothing Then
                Exit Do
            End If
            NumInfo = CInt(Strings.Right(Ctl.ID, 2))
            VirRadioV = CType(Ctl, VirtualiaControle_VTrioVerticalRadio)
            VirRadioV.V_SiAutoPostBack = Not (SiEnLecture)
            If CacheDonnee(NumInfo) IsNot Nothing Then
                If V_IndexFiche <> -1 And CacheDonnee(NumInfo).ToString <> "" Then
                    Select Case CacheDonnee(NumInfo).ToString
                        Case Is = VirRadioV.RadioGaucheText
                            VirRadioV.RadioGaucheCheck = True
                        Case Is = VirRadioV.RadioCentreText
                            VirRadioV.RadioCentreCheck = True
                        Case Is = VirRadioV.RadioDroiteText
                            VirRadioV.RadioDroiteCheck = True
                    End Select
                End If
            End If
            VirRadioV.V_SiEnLectureSeule = SiEnLecture
            IndiceI += 1
        Loop

        Dim VirRadioH As Controles_VTrioHorizontalRadio
        IndiceI = 0
        Do
            Ctl = V_WebFonction.VirWebControle(Me.CadreInfo, "RadioH", IndiceI)
            If Ctl Is Nothing Then
                Exit Do
            End If
            NumInfo = CInt(Strings.Right(Ctl.ID, 2))
            VirRadioH = CType(Ctl, Controles_VTrioHorizontalRadio)
            VirRadioH.V_SiAutoPostBack = Not (SiEnLecture)
            If CacheDonnee(NumInfo) IsNot Nothing Then
                If V_IndexFiche <> -1 And CacheDonnee(NumInfo).ToString <> "" Then
                    Select Case CacheDonnee(NumInfo).ToString
                        Case Is = VirRadioH.RadioGaucheText
                            VirRadioH.RadioGaucheCheck = True
                        Case Is = VirRadioH.RadioCentreText
                            VirRadioH.RadioCentreCheck = True
                        Case Is = VirRadioH.RadioDroiteText
                            VirRadioH.RadioDroiteCheck = True
                    End Select
                End If
            End If
            VirRadioH.V_SiEnLectureSeule = SiEnLecture
            If NumInfo = 8 And SiManager = False Then
                VirRadioH.V_SiEnLectureSeule = True
            End If
            IndiceI += 1
        Loop

    End Sub

    Protected Sub CommandeNew_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles CommandeNew.Click
        Call InitialiserControles()
        Call V_CommandeNewFiche()
        CadreCmdOK.Visible = True
    End Sub

    Protected Sub CommandeSupp_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles CommandeSupp.Click
        Dim CacheDonnee As List(Of String) = V_CacheMaj
        If CacheDonnee Is Nothing Then
            Exit Sub
        End If
        If CacheDonnee(10) IsNot Nothing Then
            If CacheDonnee(10).ToString <> "" Then
                If V_WebFonction.ViRhDates.ComparerDates(CacheDonnee(10).ToString, V_WebFonction.ViRhDates.DateduJour) = VI.ComparaisonDates.PlusPetit Then
                    Exit Sub
                End If
            End If
        End If
        Call V_CommandeSuppFiche()
        '*** Vérification Compteur Recup ********************************
        'Call VerifierDroitsRecuperation(CacheDonnee(0).ToString)
    End Sub

    Public WriteOnly Property RetourDialogueSupp(ByVal Cmd As String) As String
        Set(ByVal value As String)
            V_RetourDialogueSupp(Cmd) = value
            Call ActualiserListe()
        End Set
    End Property

    Protected Sub CommandeOK_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles CommandeOK.Click
        Dim CacheDonnee As List(Of String) = V_CacheMaj
        If CacheDonnee Is Nothing Then
            Exit Sub
        End If
        If CacheDonnee(10) IsNot Nothing Then
            If CacheDonnee(10).ToString <> "" AndAlso CacheDonnee(8).ToString <> "Accord préalable" Then
                If V_WebFonction.ViRhDates.ComparerDates(CacheDonnee(10).ToString, V_WebFonction.ViRhDates.DateduJour) = VI.ComparaisonDates.PlusPetit Then
                    Exit Sub
                End If
            End If
        End If
        Call V_MajFiche()
        Call ActualiserListe()
        CadreCmdOK.Visible = False

        '*** Vérification Compteur Recup ********************************
        'If V_WebFonction.PointeurUtilisateur.SiAccesManager = True Then
        '    Call VerifierDroitsRecuperation(CacheDonnee(0).ToString)
        'End If

        '** Envoi d'un mail systématique *********************
        If CType(V_WebFonction.PointeurUtilisateur.V_PointeurSessionModule, Virtualia.Net.Session.SessionModule).DossierPER Is Nothing Then
            Exit Sub
        End If
        If CType(V_WebFonction.PointeurUtilisateur.V_PointeurSessionModule, Virtualia.Net.Session.SessionModule).DossierPER.EMail = "" Then
            Exit Sub
        End If
        Dim Msg As String
        Dim DossierManager As Virtualia.Net.WebAppli.SelfV4.DossierPersonne = CType(V_WebFonction.PointeurGlobal, Virtualia.Net.Session.ObjetGlobal).Dossier(CType(V_WebFonction.PointeurUtilisateur.V_PointeurSessionModule, Virtualia.Net.Session.SessionModule).DossierPER.Ide_Manager)
        If DossierManager Is Nothing Then
            Exit Sub
        End If
        If DossierManager.EMail = "" Then
            Exit Sub
        End If
        If CType(V_WebFonction.PointeurUtilisateur.V_PointeurSessionModule, Virtualia.Net.Session.SessionModule).SiAccesManager = True Then
            Msg = "Vous venez de déclarer une activité spécifique sur virtualia en date du " & CacheDonnee(0).ToString
            Msg &= ". Elle a été visée par votre manager avec la mention : " & CacheDonnee(8).ToString & ". Vous pouvez la consulter dans Virtualia.Net "
            Msg &= vbCrLf & System.Configuration.ConfigurationManager.AppSettings("UrlSelfV3") & vbCrLf
            Msg &= "Nous vous demandons de bien vouloir poser les jours de repos nécessaires sur virtualia de manière "
            Msg &= "à ne pas travailler plus de 6 jours consécutifs par semaine. (Article L3132-1 du Code du Travail). "
            Call CType(V_WebFonction.PointeurGlobal, Virtualia.Net.Session.ObjetGlobal).EnvoyerEmail("", DossierManager.EMail, CType(V_WebFonction.PointeurUtilisateur.V_PointeurSessionModule, Virtualia.Net.Session.SessionModule).DossierPER.EMail, Msg)
        Else
            Msg = "Votre collaborateur " & CType(V_WebFonction.PointeurUtilisateur.V_PointeurSessionModule, Virtualia.Net.Session.SessionModule).DossierPER.Civilite
            Msg &= Strings.Space(1) & CType(V_WebFonction.PointeurUtilisateur.V_PointeurSessionModule, Virtualia.Net.Session.SessionModule).DossierPER.Nom & Strings.Space(1) & CType(V_WebFonction.PointeurUtilisateur.V_PointeurSessionModule, Virtualia.Net.Session.SessionModule).DossierPER.Prenom & Strings.Space(1)
            Msg &= " vient de déclarer une activité spécifique à valider sur virtualia." & vbCrLf
            Msg &= " Vous pouvez la superviser dans Virtualia.Net "
            Msg &= vbCrLf & System.Configuration.ConfigurationManager.AppSettings("UrlSelfV3") & vbCrLf
            Msg &= "Nous vous rappelons qu'en cas de travail rendu nécessaire le samedi ou le dimanche ou les jours fériés "
            Msg &= "pour raison de salon ou autres activités spécifiques, l’organisation du travail devra tenir compte de l’obligation "
            Msg &= "de ne pas occuper plus de 6 jours par semaine un même salarié (Article L3132-1 du Code du Travail). " & vbCrLf
            Msg &= "Il est donc de votre responsabilité de vérifier que votre collaborateur  ne travaille pas plus de 6 jours par semaine."
            Call CType(V_WebFonction.PointeurGlobal, Virtualia.Net.Session.ObjetGlobal).EnvoyerEmail("", CType(V_WebFonction.PointeurUtilisateur.V_PointeurSessionModule, Virtualia.Net.Session.SessionModule).DossierPER.EMail, DossierManager.EMail, Msg)
        End If
    End Sub

    Protected Sub InfoVerticale_ValeurChange(ByVal sender As Object, ByVal e As Systeme.Evenements.DonneeChangeEventArgs) Handles InfoV05.ValeurChange, InfoV09.ValeurChange

        Dim CacheDonnee As List(Of String) = V_CacheMaj
        Dim NumInfo As Integer = CInt(Strings.Right(CType(sender, Controles_VCoupleVerticalEtiDonnee).ID, 2))
        If CacheDonnee IsNot Nothing Then
            If CacheDonnee(NumInfo) Is Nothing Then
                CType(sender, Controles_VCoupleVerticalEtiDonnee).DonBackColor = V_WebFonction.CouleurMaj
                CadreCmdOK.Visible = True
            Else
                If CacheDonnee(NumInfo).ToString <> e.Valeur Then
                    CType(sender, Controles_VCoupleVerticalEtiDonnee).DonBackColor = V_WebFonction.CouleurMaj
                    CadreCmdOK.Visible = True
                End If
            End If
            Call V_ValeurChange(NumInfo, e.Valeur)
        End If
    End Sub

    Private Sub InfoH_ValeurChange(ByVal sender As Object, ByVal e As Systeme.Evenements.DonneeChangeEventArgs) Handles InfoH02.ValeurChange,
        InfoH03.ValeurChange, InfoH04.ValeurChange, InfoH10.ValeurChange

        Dim CacheDonnee As List(Of String) = V_CacheMaj
        Dim NumInfo As Integer = CInt(Strings.Right(CType(sender, Controles_VCoupleEtiDonnee).ID, 2))
        If CacheDonnee IsNot Nothing Then
            If CacheDonnee(NumInfo) Is Nothing Then
                CType(sender, Controles_VCoupleEtiDonnee).DonBackColor = V_WebFonction.CouleurMaj
                CadreCmdOK.Visible = True
            Else
                If CacheDonnee(NumInfo).ToString <> e.Valeur Then
                    CType(sender, Controles_VCoupleEtiDonnee).DonBackColor = V_WebFonction.CouleurMaj
                    CadreCmdOK.Visible = True
                End If
            End If
            Call V_ValeurChange(NumInfo, e.Valeur)
        End If

    End Sub

    Protected Sub RadioValeurChange(ByVal sender As Object, ByVal e As Systeme.Evenements.DonneeChangeEventArgs) Handles RadioV01.ValeurChange
        Dim CacheDonnee As List(Of String) = V_CacheMaj
        Dim NumInfo As Integer = CInt(Strings.Right(CType(sender, VirtualiaControle_VTrioVerticalRadio).ID, 2))
        If CacheDonnee IsNot Nothing Then
            CadreCmdOK.Visible = True
            Call V_ValeurChange(NumInfo, e.Valeur)
            Call V_ValeurChange(7, Identifiant_Manager.ToString)
            Call SpecifiqueFiche()
        End If
    End Sub

    Protected Sub RadioHValeurChange(ByVal sender As Object, ByVal e As Systeme.Evenements.DonneeChangeEventArgs) Handles RadioH08.ValeurChange
        Dim CacheDonnee As List(Of String) = V_CacheMaj
        Dim NumInfo As Integer = CInt(Strings.Right(CType(sender, Controles_VTrioHorizontalRadio).ID, 2))
        If CacheDonnee IsNot Nothing Then
            CadreCmdOK.Visible = True
            Call V_ValeurChange(NumInfo, e.Valeur)
            Call V_ValeurChange(10, V_WebFonction.ViRhDates.DateduJour) 'Date de validation
        End If
    End Sub

    Protected Overrides Sub Page_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init
        V_Objet(TypeListe.SiListeGrid) = WsNumObjet
        V_NomTableSgbd = WsNomTable
        V_Fiche = WsFiche
        ListeGrille.Centrage_Colonne(0) = 1
        ListeGrille.Centrage_Colonne(1) = 0
        ListeGrille.Centrage_Colonne(2) = 1
        ListeGrille.Centrage_Colonne(3) = 1
    End Sub

    Protected Overrides Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        If CType(V_WebFonction.PointeurUtilisateur.V_PointeurSessionModule, Virtualia.Net.Session.SessionModule).SiAccesManager = True Then
            CadreCmdNewSupp.Visible = False
        End If
        Call LireLaFiche()
        Call SpecifiqueFiche()

        CadreInfo.BackColor = V_WebFonction.CouleurCharte(VI.PointdeVue.PVueApplicatif, "Cadre")
        Etiquette.BackColor = V_WebFonction.CouleurCharte(VI.PointdeVue.PVueApplicatif, "Titre")
        Etiquette.ForeColor = V_WebFonction.CouleurCharte(VI.PointdeVue.PVueApplicatif, "Police_Claire")
        Etiquette.BorderColor = V_WebFonction.CouleurCharte(VI.PointdeVue.PVueApplicatif, "Bordure")

    End Sub

    Private Sub InitialiserControles()
        Dim Ctl As Control
        Dim VirControle As Controles_VCoupleEtiDonnee
        Dim IndiceI As Integer = 0
        Do
            Ctl = V_WebFonction.VirWebControle(Me.CadreInfo, "InfoH", IndiceI)
            If Ctl Is Nothing Then
                Exit Do
            End If
            VirControle = CType(Ctl, Controles_VCoupleEtiDonnee)
            VirControle.DonBackColor = Drawing.Color.White
            VirControle.DonText = ""
            IndiceI += 1
        Loop

        Dim VirVertical As Controles_VCoupleVerticalEtiDonnee
        IndiceI = 0
        Do
            Ctl = V_WebFonction.VirWebControle(Me.CadreInfo, "InfoV", IndiceI)
            If Ctl Is Nothing Then
                Exit Do
            End If
            VirVertical = CType(Ctl, Controles_VCoupleVerticalEtiDonnee)
            VirVertical.DonBackColor = Drawing.Color.White
            VirVertical.DonText = ""
            IndiceI += 1
        Loop
        LabelJour.Text = ""
        Calendrier.DateSelectionnee(0) = ""
        LabelMontantPrime.Text = ""
        LabelRecuperation.Text = ""

    End Sub

    Private Sub Calendrier_ValeurChange(sender As Object, e As Systeme.Evenements.DonneeChangeEventArgs) Handles Calendrier.ValeurChange
        If e.Parametre <> "Jour" Then
            Exit Sub
        End If
        Dim CacheDonnee As List(Of String) = V_CacheMaj
        If CacheDonnee IsNot Nothing Then
            If CacheDonnee(0) Is Nothing Then
                CadreCmdOK.Visible = True
            Else
                If CacheDonnee(0).ToString <> e.Valeur Then
                    CadreCmdOK.Visible = True
                End If
            End If
            Call V_ValeurChange(0, e.Valeur) 'Date de l'activité
            Call V_ValeurChange(6, V_WebFonction.ViRhDates.DateduJour) 'Date de déclaration
            If CacheDonnee(1) IsNot Nothing AndAlso CacheDonnee(1).ToString = "" Then
                CacheDonnee(1) = "Activité extérieure"
            End If
            Call SpecifiqueFiche()
        End If
    End Sub

    Private Sub SpecifiqueFiche()
        Dim LstDecision As List(Of Virtualia.TablesObjet.ShemaREF.OUT_DECISION)
        Dim CacheDonnee As List(Of String) = V_CacheMaj
        Dim IdeTableDec As Integer = 81
        Dim Periode As String = "SE"
        LabelJour.Text = "Jour de semaine"
        Dim Montant As Double = 0
        Dim SiRecup As String = "Non"

        If CacheDonnee Is Nothing Then
            Exit Sub
        End If
        If CacheDonnee(0) Is Nothing OrElse CacheDonnee(0).ToString = "" Then
            Exit Sub
        End If
        Calendrier.DateSelectionnee(0) = CacheDonnee(0).ToString

        If CacheDonnee(7) Is Nothing OrElse CacheDonnee(7).ToString = "" Then
            Call V_ValeurChange(7, Identifiant_Manager.ToString)
        End If
        If V_WebFonction.ViRhDates.SiJourFerie(CacheDonnee(0).ToString) = True Then
            Periode = "JF"
            LabelJour.Text = "Jour férié"
        Else
            If CDate(CacheDonnee(0).ToString).DayOfWeek = DayOfWeek.Saturday Then
                Periode = "SA"
                LabelJour.Text = "Samedi"
            ElseIf CDate(CacheDonnee(0).ToString).DayOfWeek = DayOfWeek.Sunday Then
                Periode = "DI"
                LabelJour.Text = "Dimanche"
            End If
        End If
        Call V_ValeurChange(11, Periode)
        If CacheDonnee(1) IsNot Nothing Then
            Select Case CacheDonnee(1).ToString
                Case "Activité Racing"
                    IdeTableDec = 80
                Case "Activité extérieure"
                    IdeTableDec = 81
            End Select
        End If

        LstDecision = CType(V_WebFonction.PointeurGlobal, Virtualia.Net.Session.ObjetGlobal).ListeTablesDecision(IdeTableDec)
        If LstDecision Is Nothing Then
            Exit Sub
        End If
        For Each TableDecision In LstDecision
            If TableDecision.ValeurInterne(0) = Periode Then
                Montant = V_WebFonction.ViRhFonction.ConversionDouble(TableDecision.ValeurInterne(1))
                SiRecup = TableDecision.ValeurInterne(2)
                If CType(V_WebFonction.PointeurGlobal, Virtualia.Net.Session.ObjetGlobal).SiEstAussiManager(CType(V_WebFonction.PointeurUtilisateur.V_PointeurSessionModule, Virtualia.Net.Session.SessionModule).DossierPER.Ide_Dossier) = True Then
                    Montant = 0
                End If
                Exit For
            End If
        Next
        'Si Numéro RINGI non saisi
        If CacheDonnee(3) Is Nothing OrElse CacheDonnee(3).ToString = "" Then
            Montant = 0
        End If
        Call V_ValeurChange(12, Montant.ToString)
        Call V_ValeurChange(13, SiRecup)

        LabelMontantPrime.Text = Montant.ToString & " €"
        If SiRecup = "Oui" Then
            LabelRecuperation.Text = "1 jour"
        Else
            LabelRecuperation.Text = ""
        End If
    End Sub

    Private ReadOnly Property Identifiant_Manager As Integer
        Get
            Dim UtiSession As Virtualia.Net.Session.SessionModule = CType(V_WebFonction.PointeurUtilisateur.V_PointeurSessionModule, Virtualia.Net.Session.SessionModule)
            If UtiSession Is Nothing Then
                Return 0
            End If
            Return UtiSession.DossierPER.Ide_Manager
        End Get
    End Property

    Private Sub VerifierDroitsRecuperation(ByVal ArgumentDate As String)
        Dim LstFichesDroit As List(Of Virtualia.Systeme.MetaModele.VIR_FICHE)
        Dim LstFichesActivite As List(Of Virtualia.Systeme.MetaModele.VIR_FICHE)
        Dim FicheDroit As Virtualia.TablesObjet.ShemaPER.PER_DROIT_CONGES = Nothing
        Dim FicheActivite As Virtualia.TablesObjet.ShemaPER.PER_ACTIVITE_HONDA
        Dim Ide As Integer = CType(V_WebFonction.PointeurUtilisateur.V_PointeurSessionModule, Virtualia.Net.Session.SessionModule).DossierPER.Ide_Dossier
        Dim CptRecup As Integer
        Dim Siok As Boolean

        Dim LstIde As List(Of Integer)
        LstIde = New List(Of Integer)
        LstIde.Add(Ide)

        LstFichesDroit = V_WebFonction.PointeurGlobal.SelectionObjetDate(LstIde, "01/01/" & CDate(ArgumentDate).Year - 1, "31/12/" & CDate(ArgumentDate).Year + 1, VI.ObjetPer.ObaDroits)
        If LstFichesDroit Is Nothing Then
            Exit Sub
        End If
        For Each FichePER As Virtualia.Systeme.MetaModele.VIR_FICHE In LstFichesDroit
            If FichePER.Date_Valeur_ToDate.Year = CDate(ArgumentDate).Year Then
                FicheDroit = DirectCast(FichePER, Virtualia.TablesObjet.ShemaPER.PER_DROIT_CONGES)
                Exit For
            End If
        Next
        If FicheDroit Is Nothing Then
            Exit Sub
        End If

        LstFichesActivite = V_WebFonction.PointeurGlobal.SelectionObjetDate(LstIde, "01/01/" & CDate(ArgumentDate).Year - 1, "31/12/" & CDate(ArgumentDate).Year + 1, WsNumObjet)
        If LstFichesActivite Is Nothing Then
            Exit Sub
        End If
        CptRecup = 0
        For Each FichePER As Virtualia.Systeme.MetaModele.VIR_FICHE In LstFichesActivite
            Select Case V_WebFonction.ViRhDates.ComparerDates(FichePER.Date_de_Valeur, FicheDroit.Date_de_Valeur)
                Case VI.ComparaisonDates.Egalite, VI.ComparaisonDates.PlusGrand
                    Select Case V_WebFonction.ViRhDates.ComparerDates(FichePER.Date_de_Fin, FicheDroit.Date_de_Fin)
                        Case VI.ComparaisonDates.Egalite, VI.ComparaisonDates.PlusPetit
                            FicheActivite = DirectCast(FichePER, Virtualia.TablesObjet.ShemaPER.PER_ACTIVITE_HONDA)
                            If FicheActivite.SiDroit_Recuperation = "Oui" And FicheActivite.Validation_Manager = "Validé" Then
                                CptRecup += 1
                            End If
                    End Select
            End Select
        Next
        FicheDroit.Droit_CompteurN2 = CptRecup
        Siok = V_WebFonction.PointeurGlobal.VirServiceServeur.MiseAjour_Fiche(V_WebFonction.PointeurGlobal.VirNomUtilisateur, _
                                    VI.PointdeVue.PVueApplicatif, VI.ObjetPer.ObaDroits, Ide, "M", FicheDroit.FicheLue, FicheDroit.ContenuTable, True)
    End Sub
End Class