﻿Option Strict On
Option Explicit On
Option Compare Text
Public Class VNotificationPS
    Inherits System.Web.UI.UserControl
    Private WebFct As Virtualia.Net.Controles.WebFonctions

    Public WriteOnly Property Fiche As Virtualia.TablesObjet.ShemaVUE.VUE_ATTRIBUTAIRE_PFR
        Set(value As Virtualia.TablesObjet.ShemaVUE.VUE_ATTRIBUTAIRE_PFR)
            WebFct = New Virtualia.Net.Controles.WebFonctions(Me, 0)

            PS_LieuDate.Text = "Montreuil, le " & Now.ToShortDateString
            PS_AnneeEntretien.Text = CStr(value.Date_Valeur_ToDate.Year)
            PS_Matricule.Text = Strings.Format(value.Ide_Dossier, "000000")
            PS_Nom.Text = value.Nom
            PS_Prenom.Text = value.Prenom
            PS_AffectationNiv1.Text = value.Structure_N1
            PS_AffectationNiv2.Text = value.Structure_N2
            PS_Grade.Text = value.Grade
            PS_Echelon.Text = value.Echelon

            PS_Col_18.Text = Strings.Format(value.Montant_Reference_PS, "### ##0.00")
            PS_Col_17.Text = Strings.Format(value.Pourcentage_Moyen_Proratisation, "### ##0.0000")
            PS_Col_19.Text = Strings.Format(value.Montant_PS_Base100_Proratise, "### ##0.00")
            PS_Col_20.Text = Strings.Format(value.Coefficient_Modulation_R, "### ##0.00")
            PS_Col_21.Text = Strings.Format(value.Montant_Modulation_PS_Proratise, "### ##0.00")
            PS_Col_21Bis.Text = Strings.Format(value.Montant_PS_Complement, "### ##0.00")
            PS_Col_22.Text = Strings.Format(value.Montant_PS_Base100_Proratise + value.Montant_Modulation_PS_Proratise, "### ##0.00")

            PS_Col_22Bis.Text = Strings.Format(value.Montant_Indemnite_Sujetion, "### ##0.00")
            PS_Col_22BisBis.Text = Strings.Format(value.Montant_Indemnite_Sujetion, "### ##0.00")

            PS_Col_22Ter.Text = Strings.Format(value.Montant_PS_Avec_IS, "### ##0.00")

            PS_L3_C1.Text = "Indemnisation<br/>des sujétions<br/>du 01/11/"
            PS_L3_C1.Text &= CStr(value.Date_Valeur_ToDate.Year - 1)
            PS_L3_C1.Text &= "<br/>au 31/10/" & CStr(value.Date_Valeur_ToDate.Year)
        End Set
    End Property

    Private Sub Page_Init(sender As Object, e As EventArgs) Handles Me.Init
        Dim Tableaudata(0) As String
        Tableaudata = Strings.Split(Request.Url.AbsoluteUri, "/Fenetres/", -1)
        PS_SignatureFAM.BackImageUrl = Tableaudata(0) & "/Images/Specifique/SignatureFAM.jpg"
    End Sub

End Class