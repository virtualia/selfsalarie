﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="CtlPerHeureSupMensuel.ascx.vb" Inherits="Virtualia.Net.CtlPerHeureSupMensuel" %>

<%@ Register src="~/Controles/Saisies/VCoupleEtiDonnee.ascx" tagname="VCoupleEtiDonnee" tagprefix="Virtualia" %>

<asp:Table ID="CadreControle" runat="server" BackColor="#D7FAF3" Height="30px" Width="585px" CellPadding="0" CellSpacing="0" HorizontalAlign="Center">
   <asp:TableRow>
     <asp:TableCell>
       <asp:Table ID="CadreInfo" runat="server" BorderStyle="Ridge" BorderWidth="2px" BorderColor="#B0E0D7" HorizontalAlign="Center" style="margin-top: 3px;">
         <asp:TableRow>
              <asp:TableCell>
                  <asp:Table ID="CadreTitre" runat="server" Height="22px" CellPadding="0" CellSpacing="0" HorizontalAlign="Center">
                    <asp:TableRow>
                        <asp:TableCell HorizontalAlign="Center">
                            <asp:Label ID="EtiDateValeur" runat="server" Text="Situation des heures supplémentaires" Height="22px" Width="680px"
                                BackColor="#124545" BorderColor="#B0E0D7" BorderStyle="Groove"
                                BorderWidth="2px" ForeColor="#D7FAF3"
                                Font-Bold="True" Font-Names="Trebuchet MS" Font-Size="Small"
                                style="margin-top:2px;margin-bottom:2px;text-indent:5px;text-align:center;">
                            </asp:Label>          
                        </asp:TableCell>      
                    </asp:TableRow>
                  </asp:Table>
              </asp:TableCell>
        </asp:TableRow>
        <asp:TableRow>
          <asp:TableCell>
             <asp:Table ID="TableauDonHeures" runat="server" CellPadding="0" CellSpacing="0" HorizontalAlign="Center">
               <asp:TableRow>
                   <asp:TableCell Width="230px"></asp:TableCell>
                   <asp:TableCell ColumnSpan="2">
                        <asp:Label ID="EtiTotalEffectue" runat="server" Text="Total des heures effectuées" Height="20px" Width="228px"
                            BackColor="Transparent" BorderColor="#B0E0D7" BorderStyle="None"
                            BorderWidth="2px" ForeColor="#124545" Font-Bold="True" Font-Names="Trebuchet MS" Font-Size="Small"
                            style="margin-top:1px;margin-left:4px;text-indent:5px;text-align:center;">
                        </asp:Label>          
                   </asp:TableCell>
                   <asp:TableCell ColumnSpan="2">
                        <asp:Label ID="EtiTotalRemunere" runat="server" Text="Heures choisies pour rémunération" Height="20px" Width="228px"
                            BackColor="Transparent" BorderColor="#B0E0D7" BorderStyle="None"
                            BorderWidth="2px" ForeColor="#124545" Font-Bold="True" Font-Names="Trebuchet MS" Font-Size="Small"
                            style="margin-top:1px;margin-left:4px;text-indent:5px;text-align:center;">
                        </asp:Label>          
                   </asp:TableCell>
                    <asp:TableCell ColumnSpan="2">
                        <asp:Label ID="EtiTotalCompense" runat="server" Text="Total des heures compensables" Height="20px" Width="228px"
                            BackColor="Transparent" BorderColor="#B0E0D7" BorderStyle="None"
                            BorderWidth="2px" ForeColor="#124545" Font-Bold="True" Font-Names="Trebuchet MS" Font-Size="Small"
                            style="margin-top:1px;margin-left:4px;text-indent:5px;text-align:center;">
                        </asp:Label>          
                   </asp:TableCell>
               </asp:TableRow>
               <asp:TableRow>
                    <asp:TableCell Width="230px"></asp:TableCell>
                    <asp:TableCell>
                        <asp:Label ID="Eti14Premieres" runat="server" Text="14 1ères heures" Height="20px" Width="120px"
                            BackColor="#225C59" BorderColor="#B0E0D7" BorderStyle="Groove"
                            BorderWidth="2px" ForeColor="#D7FAF3" Font-Bold="True" Font-Names="Trebuchet MS" Font-Size="Small"
                            style="margin-top:1px;margin-left:4px;font-style:oblique;text-indent:5px;text-align:center;">
                        </asp:Label>          
                    </asp:TableCell>
                    <asp:TableCell>
                        <asp:Label ID="EtiAuDela14" runat="server" Text="au delà de 14" Height="20px" Width="120px"
                            BackColor="#225C59" BorderColor="#B0E0D7" BorderStyle="Groove"
                            BorderWidth="2px" ForeColor="#D7FAF3" Font-Bold="True" Font-Names="Trebuchet MS" Font-Size="Small"
                            style="margin-top:1px;margin-left:2px;font-style:oblique;text-indent:5px;text-align:center;">
                        </asp:Label>          
                    </asp:TableCell>
                    <asp:TableCell>
                        <asp:Label ID="EtiChoixRemu14" runat="server" Text="14 1ères heures" Height="20px" Width="120px"
                            BackColor="#225C59" BorderColor="#B0E0D7" BorderStyle="Groove"
                            BorderWidth="2px" ForeColor="#D7FAF3" Font-Bold="True" Font-Names="Trebuchet MS" Font-Size="Small"
                            style="margin-top:1px;margin-left:4px;font-style:oblique;text-indent:5px;text-align:center;">
                        </asp:Label>          
                    </asp:TableCell>
                    <asp:TableCell>
                        <asp:Label ID="EtiChoixRemuAuDela14" runat="server" Text="au delà de 14" Height="20px" Width="120px"
                            BackColor="#225C59" BorderColor="#B0E0D7" BorderStyle="Groove"
                            BorderWidth="2px" ForeColor="#D7FAF3" Font-Bold="True" Font-Names="Trebuchet MS" Font-Size="Small"
                            style="margin-top:1px;margin-left:2px;font-style:oblique;text-indent:5px;text-align:center;">
                        </asp:Label>          
                    </asp:TableCell>
                    <asp:TableCell>
                        <asp:Label ID="EtiCompense25" runat="server" Text="25 1ères heures" Height="20px" Width="120px"
                            BackColor="#225C59" BorderColor="#B0E0D7" BorderStyle="Groove"
                            BorderWidth="2px" ForeColor="#D7FAF3"
                            Font-Bold="True" Font-Names="Trebuchet MS" Font-Size="Small"
                            style="margin-top:1px;margin-left:4px;font-style:oblique;text-indent:5px;text-align:center;">
                        </asp:Label>          
                    </asp:TableCell>
                    <asp:TableCell>
                        <asp:Label ID="EtiCompenseAuDela25" runat="server" Text="au delà de 25" Height="20px" Width="120px"
                            BackColor="#225C59" BorderColor="#B0E0D7" BorderStyle="Groove"
                            BorderWidth="2px" ForeColor="#D7FAF3"
                            Font-Bold="True" Font-Names="Trebuchet MS" Font-Size="Small"
                            style="margin-top:1px;margin-left:2px;font-style:oblique;text-indent:5px;text-align:center;">
                        </asp:Label>          
                    </asp:TableCell>        
               </asp:TableRow>
               <asp:TableRow>
                    <asp:TableCell>
                       <asp:Label ID="EtiTotalJour" runat="server" Height="20px" Width="230px" Text="Heures de jour"
                            BackColor="#98D4CA" BorderColor="#B0E0D7" BorderStyle="Outset"
                            BorderWidth="2px" ForeColor="#142425" Font-Italic="true"
                            Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                            style="margin-top:2px;margin-left:4px;text-indent:5px;text-align:left">
                       </asp:Label>
                    </asp:TableCell>
                    <asp:TableCell>
                       <Virtualia:VCoupleEtiDonnee ID="InfoJour14" runat="server" V_SiEnLectureSeule="true" V_SiAutoPostBack="false"
                            V_SiDonneeDico="false" EtiVisible="false" DonWidth="75px" DonTabIndex="1" Donstyle="margin-left:22px;text-align:center;"/>
                    </asp:TableCell>
                    <asp:TableCell>
                       <Virtualia:VCoupleEtiDonnee ID="InfoJour25" runat="server" V_SiEnLectureSeule="true" V_SiAutoPostBack="false"
                            V_SiDonneeDico="false" EtiVisible="false" DonWidth="75px" DonTabIndex="2" Donstyle="margin-left:22px;text-align:center;"/>
                    </asp:TableCell>
                   <asp:TableCell>
                       <Virtualia:VCoupleEtiDonnee ID="InfoH20" runat="server" V_SiEnLectureSeule="true" V_SiAutoPostBack="false"
                            V_PointdeVue="1" V_Objet="131" V_Information="20" V_SiDonneeDico="true"
                            EtiVisible="false" DonWidth="75px" DonTabIndex="3" Donstyle="margin-left:22px;text-align:center;"/>
                    </asp:TableCell>
                    <asp:TableCell>
                       <Virtualia:VCoupleEtiDonnee ID="InfoH23" runat="server" V_SiEnLectureSeule="true" V_SiAutoPostBack="false"
                            V_PointdeVue="1" V_Objet="131" V_Information="23" V_SiDonneeDico="true"
                            EtiVisible="false" DonWidth="75px" DonTabIndex="4" Donstyle="margin-left:22px;text-align:center;"/>
                    </asp:TableCell>
                    <asp:TableCell>
                       <Virtualia:VCoupleEtiDonnee ID="InfoJour40" runat="server" V_SiEnLectureSeule="true" V_SiAutoPostBack="false"
                            V_SiDonneeDico="false" EtiVisible="false" DonWidth="75px" DonTabIndex="5" DonBackColor="#E2FDD9"
                            Donstyle="margin-left:22px;text-align:center;"/>
                    </asp:TableCell>
                    <asp:TableCell>
                       <Virtualia:VCoupleEtiDonnee ID="InfoJour41" runat="server" V_SiEnLectureSeule="true" V_SiAutoPostBack="false"
                            V_SiDonneeDico="false" EtiVisible="false" DonWidth="75px" DonTabIndex="6" DonBackColor="#E2FDD9"
                            Donstyle="margin-left:22px;text-align:center;"/>
                    </asp:TableCell>
               </asp:TableRow>
               <asp:TableRow>
                    <asp:TableCell>
                       <asp:Label ID="EtiTotalNuit" runat="server" Height="20px" Width="230px" Text="Heures de nuit"
                            BackColor="#98D4CA" BorderColor="#B0E0D7" BorderStyle="Outset"
                            BorderWidth="2px" ForeColor="#142425" Font-Italic="true"
                            Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                            style="margin-top:2px;margin-left:4px;text-indent:5px;text-align:left">
                       </asp:Label>
                    </asp:TableCell>
                    <asp:TableCell>
                       <Virtualia:VCoupleEtiDonnee ID="InfoNuit14" runat="server" V_SiEnLectureSeule="true" V_SiAutoPostBack="false"
                            V_SiDonneeDico="false" EtiVisible="false" DonWidth="75px" DonTabIndex="7" Donstyle="margin-left:22px;text-align:center;"/>
                    </asp:TableCell>
                    <asp:TableCell>
                       <Virtualia:VCoupleEtiDonnee ID="InfoNuit25" runat="server" V_SiEnLectureSeule="true" V_SiAutoPostBack="false"
                            V_SiDonneeDico="false" EtiVisible="false" DonWidth="75px" DonTabIndex="8" Donstyle="margin-left:22px;text-align:center;"/>
                    </asp:TableCell>
                    <asp:TableCell>
                       <Virtualia:VCoupleEtiDonnee ID="InfoH21" runat="server" V_SiEnLectureSeule="true" V_SiAutoPostBack="false"
                            V_PointdeVue="1" V_Objet="131" V_Information="21" V_SiDonneeDico="true"
                            EtiVisible="false" DonWidth="75px" DonTabIndex="9" Donstyle="margin-left:22px;text-align:center;"/>
                    </asp:TableCell>
                    <asp:TableCell>
                       <Virtualia:VCoupleEtiDonnee ID="InfoH24" runat="server" V_SiEnLectureSeule="true" V_SiAutoPostBack="false"
                            V_PointdeVue="1" V_Objet="131" V_Information="24" V_SiDonneeDico="true"
                            EtiVisible="false" DonWidth="75px" DonTabIndex="10" Donstyle="margin-left:22px;text-align:center;"/>
                    </asp:TableCell>
                    <asp:TableCell>
                       <Virtualia:VCoupleEtiDonnee ID="InfoNuit40" runat="server" V_SiEnLectureSeule="true" V_SiAutoPostBack="false"
                            V_SiDonneeDico="false" EtiVisible="false" DonWidth="75px" DonTabIndex="11" DonBackColor="#DAE7FA"
                            Donstyle="margin-left:22px;text-align:center;"/>
                    </asp:TableCell>
                    <asp:TableCell>
                       <Virtualia:VCoupleEtiDonnee ID="InfoNuit41" runat="server" V_SiEnLectureSeule="true" V_SiAutoPostBack="false"
                            V_SiDonneeDico="false" EtiVisible="false" DonWidth="75px" DonTabIndex="12" DonBackColor="#DAE7FA"
                            Donstyle="margin-left:22px;text-align:center;"/>
                    </asp:TableCell>
               </asp:TableRow>
               <asp:TableRow>
                   <asp:TableCell>
                       <asp:Label ID="EtiTotalWeJf" runat="server" Height="20px" Width="230px" Text="Heures de dimanches et jours fériés"
                            BackColor="#98D4CA" BorderColor="#B0E0D7" BorderStyle="Outset"
                            BorderWidth="2px" ForeColor="#142425" Font-Italic="true"
                            Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                            style="margin-top:2px;margin-left:4px;text-indent:5px;text-align:left">
                       </asp:Label>
                    </asp:TableCell>
                    <asp:TableCell>
                       <Virtualia:VCoupleEtiDonnee ID="InfoJF14" runat="server" V_SiEnLectureSeule="true" V_SiAutoPostBack="false"
                            V_SiDonneeDico="false" EtiVisible="false" DonWidth="75px" DonTabIndex="14" Donstyle="margin-left:22px;text-align:center;"/>
                    </asp:TableCell>
                    <asp:TableCell>
                       <Virtualia:VCoupleEtiDonnee ID="InfoJF25" runat="server" V_SiEnLectureSeule="true" V_SiAutoPostBack="false"
                            V_SiDonneeDico="false" EtiVisible="false" DonWidth="75px" DonTabIndex="15" Donstyle="margin-left:22px;text-align:center;"/>
                    </asp:TableCell>
                    <asp:TableCell>
                       <Virtualia:VCoupleEtiDonnee ID="InfoH22" runat="server" V_SiEnLectureSeule="true" V_SiAutoPostBack="false"
                            V_PointdeVue="1" V_Objet="131" V_Information="22" V_SiDonneeDico="true"
                            EtiVisible="false" DonWidth="75px" DonTabIndex="16" Donstyle="margin-left:22px;text-align:center;"/>
                    </asp:TableCell>
                    <asp:TableCell>
                       <Virtualia:VCoupleEtiDonnee ID="InfoH25" runat="server" V_SiEnLectureSeule="true" V_SiAutoPostBack="false"
                            V_PointdeVue="1" V_Objet="131" V_Information="25" V_SiDonneeDico="true"
                            EtiVisible="false" DonWidth="75px" DonTabIndex="17" Donstyle="margin-left:22px;text-align:center;"/>
                    </asp:TableCell>
                    <asp:TableCell>
                       <Virtualia:VCoupleEtiDonnee ID="InfoJF40" runat="server" V_SiEnLectureSeule="true" V_SiAutoPostBack="false"
                            V_SiDonneeDico="false" EtiVisible="false" DonWidth="75px" DonTabIndex="18" DonBackColor="#FFF2DB"
                            Donstyle="margin-left:22px;text-align:center;"/>
                    </asp:TableCell>
                    <asp:TableCell>
                       <Virtualia:VCoupleEtiDonnee ID="InfoJF41" runat="server" V_SiEnLectureSeule="true" V_SiAutoPostBack="false"
                            V_SiDonneeDico="false" EtiVisible="false" DonWidth="75px" DonTabIndex="19" DonBackColor="#FFF2DB"
                            Donstyle="margin-left:22px;text-align:center;"/>
                    </asp:TableCell>
               </asp:TableRow>
               <asp:TableRow>
                 <asp:TableCell ColumnSpan="7" HorizontalAlign="Center">
                     <asp:Table ID="CadreDetailCompensation" runat="server">
                         <asp:TableRow>
                             <asp:TableCell ColumnSpan="2" HorizontalAlign="Center">
                                <asp:Label ID="EtiHsSEmaine" runat="server" Height="20px" Width="500px" Text="Calcul de la compensation des heures effectuées les jours ouvrables"
                                    BackColor="#225C59" BorderColor="#B0E0D7"  BorderStyle="Outset" BorderWidth="2px" 
                                    ForeColor="#D7FAF3" Font-Italic="true" Font-Bold="True" Font-Names="Trebuchet MS" Font-Size="Small"
                                    style="margin-top:5px;text-indent:5px;text-align:center">
                                </asp:Label>
                             </asp:TableCell>
                             <asp:TableCell ColumnSpan="2" HorizontalAlign="Center">
                                <asp:Label ID="EtiWEJF" runat="server" Height="20px" Width="500px" Text="Calcul de la compensation des heures de dimanches et jours fériés"
                                    BackColor="#225C59" BorderColor="#B0E0D7"  BorderStyle="Outset" BorderWidth="2px" 
                                    ForeColor="#D7FAF3" Font-Italic="true" Font-Bold="true" Font-Names="Trebuchet MS" Font-Size="Small"
                                    style="margin-top:5px;text-indent:5px;text-align:center">
                                </asp:Label>
                             </asp:TableCell>
                         </asp:TableRow>
                         <asp:TableRow>
                             <asp:TableCell ColumnSpan="2">
                                   <Virtualia:VCoupleEtiDonnee ID="InfoH01" runat="server" V_SiEnLectureSeule="true" V_SiAutoPostBack="false"
                                        V_PointdeVue="1" V_Objet="131" V_Information="1" V_SiDonneeDico="true" EtiText="Heures effectuées jusqu'à 20 h"
                                        EtiVisible="true" EtiWidth="230px" DonWidth="75px" DonTabIndex="21" DonBackColor="#E2FDD9"
                                        Donstyle="margin-left:184px;text-align:center;"/>
                             </asp:TableCell>
                             <asp:TableCell>
                                   <Virtualia:VCoupleEtiDonnee ID="InfoH05" runat="server" V_SiEnLectureSeule="true" V_SiAutoPostBack="false"
                                        V_PointdeVue="1" V_Objet="131" V_Information="5" V_SiDonneeDico="true" EtiText="Heures effectuées jusqu'à 20 h"
                                        EtiVisible="true" EtiWidth="230px" DonWidth="75px" DonTabIndex="21" Donstyle="margin-left:4px;text-align:center;"/>
                             </asp:TableCell>
                             <asp:TableCell>
                               <Virtualia:VCoupleEtiDonnee ID="InfoJFT1" runat="server" V_SiEnLectureSeule="true" V_SiAutoPostBack="false"
                                    V_SiDonneeDico="false" EtiVisible="True" EtiWidth="80px" EtiText=" x 1 h 15" Etistyle="text-align:center"
                                    DonWidth="75px" DonTabIndex="22" DonBackColor="#FFF2DB" Donstyle="margin-left:4px;text-align:center"/>
                            </asp:TableCell>
                         </asp:TableRow>
                         <asp:TableRow>
                             <asp:TableCell>
                                   <Virtualia:VCoupleEtiDonnee ID="InfoH02" runat="server" V_SiEnLectureSeule="true" V_SiAutoPostBack="false"
                                        V_PointdeVue="1" V_Objet="131" V_Information="2" V_SiDonneeDico="true" EtiText="Heures effectuées de 20 h à 22 h"
                                        EtiVisible="true" EtiWidth="230px" DonWidth="75px" DonTabIndex="24" Donstyle="margin-left:4px;text-align:center;"/>
                             </asp:TableCell>
                             <asp:TableCell>
                               <Virtualia:VCoupleEtiDonnee ID="InfoOuv2" runat="server" V_SiEnLectureSeule="true" V_SiAutoPostBack="false"
                                   V_SiDonneeDico="false" EtiVisible="True" EtiWidth="80px" EtiText=" x 1 h 30" Etistyle="text-align:center"
                                   DonWidth="75px" DonTabIndex="26" DonBackColor="#E2FDD9" Donstyle="margin-left:4px;text-align:center;"/>
                            </asp:TableCell>
                             <asp:TableCell>
                                   <Virtualia:VCoupleEtiDonnee ID="InfoH06" runat="server" V_SiEnLectureSeule="true" V_SiAutoPostBack="false"
                                        V_PointdeVue="1" V_Objet="131" V_Information="6" V_SiDonneeDico="true" EtiText="Heures effectuées de 20 h à 22 h"
                                        EtiVisible="true" EtiWidth="230px" DonWidth="75px" DonTabIndex="24" Donstyle="margin-left:4px;text-align:center;"/>
                             </asp:TableCell>
                             <asp:TableCell>
                               <Virtualia:VCoupleEtiDonnee ID="InfoJFT2" runat="server" V_SiEnLectureSeule="true" V_SiAutoPostBack="false"
                                   V_SiDonneeDico="false" EtiVisible="True" EtiWidth="80px" EtiText=" x 1 h 30" Etistyle="text-align:center"
                                   DonWidth="75px" DonTabIndex="26" DonBackColor="#FFF2DB" Donstyle="margin-left:4px;text-align:center"/>
                            </asp:TableCell>
                         </asp:TableRow>
                         <asp:TableRow>
                             <asp:TableCell>
                                   <Virtualia:VCoupleEtiDonnee ID="InfoH03" runat="server" V_SiEnLectureSeule="true" V_SiAutoPostBack="false"
                                        V_PointdeVue="1" V_Objet="131" V_Information="3" V_SiDonneeDico="true" EtiText="Heures effectuées de nuit"
                                        EtiVisible="true" EtiWidth="230px" DonWidth="75px" DonTabIndex="29" Donstyle="margin-left:4px;text-align:center;"/>
                             </asp:TableCell>
                             <asp:TableCell>
                               <Virtualia:VCoupleEtiDonnee ID="InfoOuv3" runat="server" V_SiEnLectureSeule="true" V_SiAutoPostBack="false"
                                   V_SiDonneeDico="false" EtiVisible="True" EtiWidth="80px" EtiText=" x 2 h" Etistyle="text-align:center"
                                   DonWidth="75px" DonTabIndex="30" DonBackColor="#DAE7FA" Donstyle="margin-left:4px;text-align:center"/>
                            </asp:TableCell>
                             <asp:TableCell>
                                   <Virtualia:VCoupleEtiDonnee ID="InfoH07" runat="server" V_SiEnLectureSeule="true" V_SiAutoPostBack="false"
                                        V_PointdeVue="1" V_Objet="131" V_Information="7" V_SiDonneeDico="true" EtiText="Heures effectuées de nuit"
                                        EtiVisible="true" EtiWidth="230px" DonWidth="75px" DonTabIndex="29" Donstyle="margin-left:4px;text-align:center;"/>
                             </asp:TableCell>
                             <asp:TableCell>
                               <Virtualia:VCoupleEtiDonnee ID="InfoJFT3" runat="server" V_SiEnLectureSeule="true" V_SiAutoPostBack="false"
                                   V_SiDonneeDico="false" EtiVisible="True" EtiWidth="80px" EtiText=" x 2 h" Etistyle="text-align:center"
                                   DonWidth="75px" DonTabIndex="30" DonBackColor="#DAE7FA" Donstyle="margin-left:4px;text-align:center"/>
                            </asp:TableCell>
                         </asp:TableRow>
                         <asp:TableRow>
                             <asp:TableCell ColumnSpan="2">
                                   <Virtualia:VCoupleEtiDonnee ID="InfoH04" runat="server" V_SiEnLectureSeule="true" V_SiAutoPostBack="false"
                                        V_PointdeVue="1" V_Objet="131" V_Information="4" V_SiDonneeDico="true" EtiText="Heures de trajet"
                                        EtiVisible="true" EtiWidth="230px" DonWidth="75px" DonTabIndex="33" DonBackColor="#E2FDD9"
                                        Donstyle="margin-left:184px;text-align:center;"/>
                             </asp:TableCell>
                             <asp:TableCell ColumnSpan="2">
                                   <Virtualia:VCoupleEtiDonnee ID="InfoH08" runat="server" V_SiEnLectureSeule="true" V_SiAutoPostBack="false"
                                        V_PointdeVue="1" V_Objet="131" V_Information="8" V_SiDonneeDico="true" EtiText="Heures de trajet aller-retour"
                                        EtiVisible="true" EtiWidth="230px" DonWidth="75px" DonTabIndex="33" DonBackColor="#FFF2DB"
                                        Donstyle="margin-left:184px;text-align:center;"/>
                             </asp:TableCell>
                         </asp:TableRow>
                     </asp:Table>
                 </asp:TableCell>
               </asp:TableRow>
               <asp:TableRow>
                    <asp:TableCell ColumnSpan="7" HorizontalAlign="Center">
                        <asp:Table ID="CadreDemande" runat="server">
                            <asp:TableRow>
                                <asp:TableCell ColumnSpan="4" HorizontalAlign="Center">
                                     <asp:Label ID="EtiVisas" runat="server" Height="20px" Width="480px" Text="Suivi de la demande des heures rémunérées"
                                        BackColor="#225C59" BorderColor="#B0E0D7"  BorderStyle="Outset"
                                        BorderWidth="2px" ForeColor="#D7FAF3" Font-Italic="true"
                                        Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                        style="margin-top:5px;text-indent:5px;text-align:center">
                                    </asp:Label>
                                 </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell>
                                    <Virtualia:VCoupleEtiDonnee ID="InfoH09" runat="server" V_SiEnLectureSeule="true" V_SiAutoPostBack="false"
                                        V_PointdeVue="1" V_Objet="131" V_Information="9" V_SiDonneeDico="true" EtiText="Date de la demande"
                                        EtiVisible="True" EtiWidth="150px" DonWidth="100px" DonTabIndex="23" Donstyle="margin-left:2px;text-align:center;"/>
                                </asp:TableCell>
                                <asp:TableCell HorizontalAlign="Center">
                                    <Virtualia:VCoupleEtiDonnee ID="InfoH11" runat="server" V_SiEnLectureSeule="true" V_SiAutoPostBack="false"
                                        V_PointdeVue="1" V_Objet="131" V_Information="11" V_SiDonneeDico="true" EtiText="Date du visa hiérarchie"
                                        EtiVisible="True" EtiWidth="150px" DonWidth="100px" DonTabIndex="27" Donstyle="margin-left:2px;text-align:center;"/>
                                </asp:TableCell>
                                <asp:TableCell>
                                    <Virtualia:VCoupleEtiDonnee ID="InfoH13" runat="server" V_SiEnLectureSeule="true" V_SiAutoPostBack="false"
                                        V_PointdeVue="1" V_Objet="131" V_Information="13" V_SiDonneeDico="true"
                                        EtiVisible="False" DonWidth="150px" DonTabIndex="28" Donstyle="margin-left:2px;text-align:center;"/>
                                </asp:TableCell>
                                <asp:TableCell Width="200px"></asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell Width="280px"></asp:TableCell>
                                <asp:TableCell HorizontalAlign="Center">
                                     <Virtualia:VCoupleEtiDonnee ID="InfoH15" runat="server" V_SiEnLectureSeule="true" V_SiAutoPostBack="false"
                                        V_PointdeVue="1" V_Objet="131" V_Information="15" V_SiDonneeDico="true" EtiText="Date du visa DG"
                                        EtiVisible="True" EtiWidth="150px" DonWidth="100px" DonTabIndex="31" Donstyle="margin-left:2px;text-align:center;"/>
                                </asp:TableCell>
                                <asp:TableCell>
                                    <Virtualia:VCoupleEtiDonnee ID="InfoH17" runat="server" V_SiEnLectureSeule="true" V_SiAutoPostBack="false"
                                        V_PointdeVue="1" V_Objet="131" V_Information="17" V_SiDonneeDico="true"
                                        EtiVisible="False" DonWidth="150px" DonTabIndex="32" Donstyle="margin-left:2px;text-align:center;"/>
                                </asp:TableCell>
                                <asp:TableCell HorizontalAlign="Right">
                                     <asp:Button ID="CmdDemande" runat="server" Text="Effectuer une demande" Width="204px" Height="24px"
                                        BackColor="#124545" BorderColor="#B6C7E2" ForeColor="White"
                                        Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small" Font-Italic="true"
                                        BorderStyle="Outset" BorderWidth="2px" style="margin-left:0px;text-align:center;"
                                        Tooltip="Effectuer une demande">
                                    </asp:Button>
                                 </asp:TableCell>
                            </asp:TableRow>
                        </asp:Table>
                    </asp:TableCell>
                </asp:TableRow>
            </asp:Table>
          </asp:TableCell>
        </asp:TableRow>
       </asp:Table>
     </asp:TableCell>
   </asp:TableRow>
 </asp:Table>
