﻿<%@ Page Language="vb" AutoEventWireup="false" MasterPageFile="~/PagesMaitres/VirtualiaMain.Master" CodeBehind="FrmSemaineActivite.aspx.vb" 
    Inherits="Virtualia.Net.FrmSemaineActivite" MaintainScrollPositionOnPostback="True" %>

<%@ MasterType VirtualPath="~/PagesMaitres/VirtualiaMain.master" %>

<%@ Register src="~/Controles/Commun/VMessage.ascx" tagname="VMessage" tagprefix="Virtualia" %>
<%@ Register src="~/Controles/Commun/VArmoirePER.ascx" tagname="VArmoire" tagprefix="Virtualia" %>
<%@ Register src="~/Controles/Activites/PER_ACTIVITE_JOUR.ascx" tagname="VActivite" tagprefix="Virtualia" %>
<%@ Register src="~/Controles/Activites/CtlEditionActivite.ascx" tagname="VEdition" tagprefix="Virtualia" %>
<%@ Register Src="~/Controles/Administration/REF/Activite/ONIC_MESURE.ascx" TagName="ONIC_MESURE" TagPrefix="Virtualia" %>

<asp:Content ID="Corps" ContentPlaceHolderID="Principal" runat="server">
    <asp:Table IDID="CadreVues"  runat="server" HorizontalAlign="Center" Width="1050px" CellSpacing="2" Style="margin-top: 1px">
         <asp:TableRow>
            <asp:TableCell>
                <asp:Table ID="CadreActivite" runat="server" HorizontalAlign="Center" borderStyle="None">
                        <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Center" VerticalAlign="Top">
                                <asp:MultiView ID="MultiVues" runat="server" ActiveViewIndex="0">
                                    <asp:View ID="VueActivite" runat="server">
                                        <Virtualia:VActivite ID="CtlActivite" runat="server"/>
                                    </asp:View>
                                    <asp:View ID="VueArmoire" runat="server">
                                        <asp:Table ID="CadreDRH" runat="server">
                                            <asp:TableRow>
                                                <asp:TableCell> 
                                                    <asp:Table ID="CadreCommande" runat="server" Height="20px" CellPadding="0" 
                                                            CellSpacing="0" BackImageUrl="~/Images/Icones/Cmd_Std.bmp"
                                                            BorderWidth="2px" BorderStyle="Outset" BorderColor="#B6C7E2" ForeColor="#D7FAF3"
                                                            Width="300px" HorizontalAlign="Right" style="margin-top: 6px; margin-right: 20px">
                                                            <asp:TableRow VerticalAlign="Top">
                                                                <asp:TableCell VerticalAlign="Top">
                                                                    <asp:Button ID="CommandeEdit" runat="server" Text="Editions" Width="135px" Height="20px"
                                                                        BackColor="Transparent" BorderColor="#B6C7E2" ForeColor="#D7FAF3"
                                                                        Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small" Font-Italic="true"
                                                                        BorderStyle="None" style=" margin-left: 12px; text-align: left;"
                                                                        Tooltip="Edition des états de sortie des activités cumulées d'un mois" >
                                                                    </asp:Button>
                                                                </asp:TableCell>
                                                                <asp:TableCell VerticalAlign="Top">
                                                                    <asp:Button ID="CommandeAdm" runat="server" Text="Administration" Width="135px" Height="20px"
                                                                        BackColor="Transparent" BorderColor="#B6C7E2" ForeColor="#D7FAF3"
                                                                        Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small" Font-Italic="true"
                                                                        BorderStyle="None" style=" margin-left: 12px; text-align: left;"
                                                                        Tooltip="Mise à jour des référentiels" >
                                                                    </asp:Button>
                                                                </asp:TableCell>
                                                            </asp:TableRow>
                                                     </asp:Table>     
                                                </asp:TableCell> 
                                            </asp:TableRow>
                                            <asp:TableRow>
                                                <asp:TableCell>
                                                    <Virtualia:VArmoire ID="ArmoirePER" runat="server" V_Appelant="STD" />
                                                </asp:TableCell>
                                            </asp:TableRow>
                                        </asp:Table>
                                    </asp:View>
                                    <asp:View ID="VueEdition" runat="server">
                                        <Virtualia:VEdition ID="EtatMensuel" runat="server" />
                                    </asp:View>
                                    <asp:View ID="VueAdministration" runat="server">
                                        <Virtualia:ONIC_MESURE ID="MajReferentiel" runat="server" />
                                    </asp:View>
                                    <asp:View ID="VueMessage" runat="server">
                                        <Virtualia:VMessage id="MsgVirtualia" runat="server" />
                                    </asp:View>
                                </asp:MultiView>
                            </asp:TableCell>
                        </asp:TableRow>
                    </asp:Table>
            </asp:TableCell>
         </asp:TableRow>
    </asp:Table>
</asp:Content>