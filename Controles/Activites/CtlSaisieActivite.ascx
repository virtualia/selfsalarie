﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="CtlSaisieActivite.ascx.vb" Inherits="Virtualia.Net.CtlSaisieActivite" %>

<%@ Register src="~/Controles/Commun/VListeCombo.ascx" tagname="VListeCombo" tagprefix="Virtualia" %>
<asp:Table ID="CadreSemaine" runat="server" CellPadding="0" CellSpacing="0" Width="950px" Backcolor="#FFDDA1"
      BorderStyle="Notset" BorderWidth="2px" BorderColor="#124545" HorizontalAlign="Left">
    <asp:TableRow ID="LigneActivite" Width="946px" Height="23px">
        <asp:TableCell>
            <asp:Table ID="CadreActivite" runat="server" Height="30px" CellPadding="0" Width="646px"  
                                CellSpacing="0" HorizontalAlign="Center" Visible="true" BorderStyle="None">
                <asp:TableRow>
                    <asp:TableCell ID="CellEti_Activite" BackColor="#FFDDA1" BorderColor="LightGray" BorderStyle="None" HorizontalAlign="Center" BorderWidth="1px"> 
                        <asp:Label ID="Eti_Activite" runat="server" Height="23px" Width="95px"
                            BackColor="#FFDDA1" BorderColor="#D2D2D2" BorderStyle="None" Text="Projet"
                            BorderWidth="1px" ForeColor="#142425" Font-Italic="False" Font-Underline="true"
                            Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Smaller"
                            style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px;
                            font-style: normal; text-indent: 5px; text-align: left; padding-top: 4px;">
                        </asp:Label>          
                    </asp:TableCell> 
                    <asp:TableCell ID="CellDon_Activite" BackColor="#FFDDA1" BorderColor= "#D2D2D2" BorderStyle="None" HorizontalAlign="Center" BorderWidth="1px"> 
                        <Virtualia:VListeCombo ID="VComboActivite" runat="server" EtiVisible="false" V_NomTable="" 
                            LstHeight="22px" LstWidth="565px" LstBorderColor="#7EC8BE" LstBackColor="White" LstForeColor="#142425"
                            LstStyle="border-color:#7EC8BE border-width:2px;border-style:inset;display:table-cell;font-style:oblique;font-size:10px" 
                            EtiBackColor="#8DA8A3" EtiForeColor="#E9FDF9" />
                    </asp:TableCell> 
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell ID="CellEti_Etape" BackColor="#FFDDA1" BorderColor="LightGray" BorderStyle="None" HorizontalAlign="Center" BorderWidth="1px"> 
                        <asp:Label ID="Eti_Etape" runat="server" Height="23px" Width="95px"
                            BackColor="#FFDDA1" BorderColor="#FFDDA1" BorderStyle="None" Text="Etape"
                            BorderWidth="1px" ForeColor="#142425" Font-Italic="False" Font-Underline="true"
                            Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Smaller"
                            style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px;
                            font-style: normal; text-indent: 5px; text-align: left; padding-top: 4px;">
                        </asp:Label>          
                    </asp:TableCell> 
                    <asp:TableCell ID="CellDon_Etape" BackColor="#FFDDA1" BorderColor= "#D2D2D2" BorderStyle="None" HorizontalAlign="Center" BorderWidth="1px"> 
                        <Virtualia:VListeCombo ID="VComboEtape" runat="server" EtiVisible="false" V_NomTable="" 
                            LstHeight="22px" LstWidth="565px" LstBorderColor="#7EC8BE" LstBackColor="White" LstForeColor="#142425"
                            LstStyle="border-color:#7EC8BE border-width:2px;border-style:inset;display:table-cell;font-style:oblique;font-size:10px"  
                            EtiBackColor="#8DA8A3" EtiForeColor="#E9FDF9" />
                    </asp:TableCell>     
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell ID="CellEti_Precision" BackColor="#FFDDA1" BorderColor="#FFDDA1" BorderStyle="None" HorizontalAlign="Center" BorderWidth="1px"> 
                        <asp:Label ID="Eti_Precision" runat="server" Height="23px" Width="95px"
                            BackColor="#FFDDA1" BorderColor="#FFDDA1" BorderStyle="None" Text="Précision"
                            BorderWidth="1px" ForeColor="#142425" Font-Italic="False" Font-Underline="true"
                            Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Smaller"
                            style="margin-top:1px;margin-left:0px;margin-bottom:5px;font-style:normal;text-indent:5px;text-align:left;padding-top:4px;">
                        </asp:Label>          
                    </asp:TableCell> 
                    <asp:TableCell ID="CellDon_Precision" BackColor="#FFDDA1" BorderColor= "#FFDDA1" BorderStyle="None" HorizontalAlign="Center" BorderWidth="1px"> 
                        <asp:TextBox ID="Don_Precision" runat="server" Height="20px" Width="558px" MaxLength="0"
                            BackColor="White" BorderColor="#B0E0D7" BorderStyle="Inset" Text=""
                            BorderWidth="2px" ForeColor="Black" Visible="true" AutoPostBack="false"
                            Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="Small" Font-Italic="true"
                            style="margin-top: 1px;margin-bottom: 5px;text-indent: 1px;text-align: left" TextMode="SingleLine"> 
                        </asp:TextBox>
                    </asp:TableCell>     
                </asp:TableRow>
            </asp:Table>
        </asp:TableCell>
        <asp:TableCell>
            <asp:TextBox ID="Donnee00" runat="server" Height="23px" Width="29px" MaxLength="0"
                BackColor="White" BorderColor="#B0E0D7" BorderStyle="Inset" Text=""
                BorderWidth="2px" ForeColor="Black" Visible="true" AutoPostBack="false" ToolTip="lundi"
                Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="Small" Font-Italic="false"
                style="margin-top: 1px; margin-left: 6px; text-indent: 1px; text-align: center" TextMode="SingleLine"> 
            </asp:TextBox>
        </asp:TableCell>
        <asp:TableCell>
            <asp:TextBox ID="Donnee01" runat="server" Height="23px" Width="29px" MaxLength="0"
                BackColor="White" BorderColor="#B0E0D7" BorderStyle="Inset"
                BorderWidth="2px" ForeColor="Black" Visible="true" AutoPostBack="false" ToolTip="mardi"
                Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="Small" Font-Italic="false"
                style="margin-top: 1px; text-indent: 1px; text-align: center" TextMode="SingleLine"> 
            </asp:TextBox>
        </asp:TableCell>
        <asp:TableCell>
            <asp:TextBox ID="Donnee02" runat="server" Height="23px" Width="29px" MaxLength="0"
                BackColor="White" BorderColor="#B0E0D7" BorderStyle="Inset"
                BorderWidth="2px" ForeColor="Black" Visible="true" AutoPostBack="false" ToolTip="mercredi"
                Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="Small" Font-Italic="false"
                style="margin-top: 1px; text-indent: 1px; text-align: center" TextMode="SingleLine"> 
            </asp:TextBox>
        </asp:TableCell>
        <asp:TableCell>
            <asp:TextBox ID="Donnee03" runat="server" Height="23px" Width="29px" MaxLength="0"
                BackColor="#B0E0D7" BorderColor="#B0E0D7" BorderStyle="Inset"
                BorderWidth="2px" ForeColor="Black" Visible="true" AutoPostBack="false" ToolTip="jeudi"
                Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="Small" Font-Italic="false"
                style="margin-top: 1px; text-indent: 1px; text-align: center" TextMode="SingleLine"> 
            </asp:TextBox>
        </asp:TableCell>
        <asp:TableCell>
            <asp:TextBox ID="Donnee04" runat="server" Height="23px" Width="29px" MaxLength="0"
                BackColor="#B0E0D7" BorderColor="#B0E0D7" BorderStyle="Inset"
                BorderWidth="2px" ForeColor="Black" Visible="true" AutoPostBack="false" ToolTip="vendredi"
                Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="Small" Font-Italic="false"
                style="margin-top: 1px; text-indent: 1px; text-align: center" TextMode="SingleLine"> 
            </asp:TextBox>
        </asp:TableCell>
        <asp:TableCell>
            <asp:TextBox ID="Donnee05" runat="server" Height="23px" Width="29px" MaxLength="0"
                BackColor="White" BorderColor="#B0E0D7" BorderStyle="Inset"
                BorderWidth="2px" ForeColor="Black" Visible="true" AutoPostBack="false" ToolTip="samedi"
                Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="Small" Font-Italic="false"
                style="margin-top: 1px; text-indent: 1px; text-align: center" TextMode="SingleLine"> 
            </asp:TextBox>
        </asp:TableCell>
        <asp:TableCell>
            <asp:TextBox ID="Donnee06" runat="server" Height="23px" Width="29px" MaxLength="0"
                BackColor="White" BorderColor="#B0E0D7" BorderStyle="Inset"
                BorderWidth="2px" ForeColor="Black" Visible="true" AutoPostBack="false" ToolTip="dimanche"
                Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="Small" Font-Italic="false"
                style="margin-top: 1px; text-indent: 1px; text-align: center" TextMode="SingleLine"> 
            </asp:TextBox>
        </asp:TableCell>
        <asp:TableCell ID="CellSemaineCadrage" BackColor="#FFDDA1" BorderColor="White" BorderStyle="None" BorderWidth="1px"> 
            <asp:Label ID="EtiSemaineCadrage" runat="server" Height="23px" Width="20px" 
                BackColor="#FFDDA1" BorderColor="#FFDDA1" BorderStyle="Solid" Text=""
                BorderWidth="1px" ForeColor="#FFDDA1" Font-Italic="False"
                Font-Bold="True" Font-Names="Trebuchet MS" Font-Size= "Smaller"
                style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px;
                font-style: normal; text-indent: 0px;  text-align:  center; padding-top: 2px;">
            </asp:Label>          
        </asp:TableCell>
    </asp:TableRow>
</asp:Table>
