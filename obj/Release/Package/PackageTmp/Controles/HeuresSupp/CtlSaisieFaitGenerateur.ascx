﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="CtlSaisieFaitGenerateur.ascx.vb" Inherits="Virtualia.Net.CtlSaisieFaitGenerateur" %>

<asp:Table ID="CadreControle" runat="server" Height="30px" Width="585px" CellPadding="0" CellSpacing="0" HorizontalAlign="Center">
   <asp:TableRow>
     <asp:TableCell>
       <asp:Table ID="CadreInfo" runat="server" BackColor="#98D4CA" BorderStyle="Ridge" BorderWidth="4px" BorderColor="#B0E0D7" HorizontalAlign="Center" style="margin-top: 3px;">
        <asp:TableRow>
          <asp:TableCell>
            <asp:Table ID="CadreCommandes" runat="server" Height="22px" CellPadding="0" CellSpacing="0"
                Width="122px" HorizontalAlign="Right" style="margin-top: 3px;margin-right: 3px" Visible="true">
                <asp:TableRow>
                    <asp:TableCell VerticalAlign="Bottom">
                        <asp:Table ID="CadreCmdOK" runat="server" BackImageUrl="~/Images/Boutons/OK_Std.bmp" Width="70px"
                            BorderWidth="2px" BorderStyle="Outset" BorderColor="#B0E0D7" ForeColor="#D7FAF3"
                            CellPadding="0" CellSpacing="0" Visible="True">
                            <asp:TableRow>
                                <asp:TableCell>
                                    <asp:Button ID="CommandeOK" runat="server" Text="Valider" Width="65px" Height="20px"
                                        BackColor="Transparent" BorderColor="#B0E0D7" ForeColor="#D7FAF3" 
                                        Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small" Font-Italic="true"
                                        BorderStyle="None" style=" margin-left: 6px; text-align: right;">
                                        </asp:Button>
                                </asp:TableCell>
                            </asp:TableRow>
                        </asp:Table>  
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="right">
                        <asp:Button ID="CmdCancel" runat="server" Height="20px" Width="20px" BackColor="Transparent" ForeColor="Black"
                            BorderStyle="None" BorderColor="#216B68" BorderWidth="1px" Text=" x" ToolTip="Fermer"
                            Font-Bold="true" Font-Names="Trebuchet MS" Font-Size="Medium" Font-Italic="false"
                            style="margin-top: 0px; text-indent:0px; text-align: center; vertical-align: middle" />
                    </asp:TableCell>
                </asp:TableRow>
              </asp:Table>
          </asp:TableCell>
        </asp:TableRow>
        <asp:TableRow>
          <asp:TableCell HorizontalAlign="Center">
              <asp:Label ID="EtiTitreHS" runat="server" Text="Saisie du fait générateur" Height="30px" Width="380px"
                    BackColor="#124545" BorderColor="#B0E0D7" BorderStyle="Groove"
                    BorderWidth="2px" ForeColor="#D7FAF3"
                    Font-Bold="True" Font-Names="Trebuchet MS" Font-Size="Small"
                    style="margin-top: 5px; margin-left: 4px; margin-bottom: 20px;
                    font-style: oblique; text-indent: 5px; text-align: center;">
              </asp:Label>   
          </asp:TableCell>
        </asp:TableRow>
        <asp:TableRow>
            <asp:TableCell HorizontalAlign="Center">
                <asp:Label ID="EtiDateValeur" runat="server" Height="20px" Width="350px" Text="Journée du"
                    BackColor="#B0E0D7" BorderColor="#B0E0D7"  BorderStyle="Outset"
                    BorderWidth="2px" ForeColor="#142425" Font-Italic="true"
                    Font-Bold="True" Font-Names="Trebuchet MS" Font-Size="Small"
                    style="margin-top: 2px; text-indent: 5px; text-align: center">
                </asp:Label>
            </asp:TableCell>
        </asp:TableRow>
        <asp:TableRow>
          <asp:TableCell HorizontalAlign="Center">
             <asp:Table ID="CadreReferentiel" runat="server" CellPadding="0" CellSpacing="0">
               <asp:TableRow>
                 <asp:TableCell>
                       <asp:Label ID="EtiVerification" runat="server" Height="160px" Width="350px" 
                            BackColor="LightGray" BorderColor="#B0E0D7"  BorderStyle="Outset"
                            BorderWidth="2px" ForeColor="DarkRed" Font-Italic="false"
                            Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                            style="margin-top: 5px; text-indent: 5px; text-align: center">
                       </asp:Label>
                 </asp:TableCell>
               </asp:TableRow>
               <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:ListBox ID="ListeGenerateur" runat="server" Height="200px" Width="350px"
                                AutoPostBack="true" BackColor="White" ForeColor="#142425"
                                Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small" style="margin-top: 2px;">
                        </asp:ListBox>
                    </asp:TableCell>      
               </asp:TableRow>
               <asp:TableRow>
                   <asp:TableCell Height="3px"></asp:TableCell>
               </asp:TableRow>
            </asp:Table>
          </asp:TableCell>
        </asp:TableRow>
       </asp:Table>
     </asp:TableCell>
   </asp:TableRow>
 </asp:Table>
