﻿Public Class FrmActiviteH
    Inherits System.Web.UI.Page
    Private WebFct As Virtualia.Net.Controles.WebFonctions

    Private Sub FrmActiviteH_Init(sender As Object, e As EventArgs) Handles Me.Init
        Dim Msg As String = ""
        Dim NumSession As String = Server.HtmlDecode(Request.QueryString("IDVirtualia"))
        If NumSession <> Session.SessionID Then
            Msg = "Erreur lors de l'identification."
            Response.Redirect("~/Fenetres/Commun/ErreurApplication.aspx?Msg=" & Msg)
            Exit Sub
        End If
        WebFct = New Virtualia.Net.Controles.WebFonctions(Me, 1)
        If WebFct.PointeurUtilisateur Is Nothing Then
            Msg = "Erreur de serveur. Vous avez été déconnecté(e)."
            Response.Redirect("~/Fenetres/Commun/ErreurApplication.aspx?Msg=" & Msg)
            Exit Sub
        End If
        If WebFct.VerifierCookie(Me, Session.SessionID) = False Then
            Msg = "Erreur lors de l'identification du dossier accédé. Session invalide."
            Response.Redirect("~/Fenetres/Commun/ErreurApplication.aspx?Msg=" & Msg)
            Exit Sub
        End If
        Call AfficherFenetrePER(CType(WebFct.PointeurUtilisateur.V_PointeurSessionModule, Virtualia.Net.Session.SessionModule).DossierPER.Ide_Dossier, CType(WebFct.PointeurUtilisateur.V_PointeurSessionModule, Virtualia.Net.Session.SessionModule).VueActiveAlPha)
    End Sub

    Private Sub AfficherFenetrePER(ByVal Ide As Integer, ByVal Vue As String)
        If Ide = 0 Then
            Exit Sub
        End If
        Select Case Vue
            Case "Activite"
                ActiviteHonda.Identifiant = Ide
                MultiHonda.SetActiveView(VueActivite)
            Case "Acompte"
                AcompteHonda.Identifiant = Ide
                MultiHonda.SetActiveView(VueAcompte)
        End Select
    End Sub

    Private Sub MsgVirtualia_ValeurRetour(sender As Object, e As Systeme.Evenements.MessageRetourEventArgs) Handles MsgVirtualia.ValeurRetour
        HPopupMsg.Value = "0"
        CellMessage.Visible = False
        Select Case e.Emetteur
            Case "SuppFiche"
                Select Case e.NumeroObjet
                    Case 110
                        ActiviteHonda.RetourDialogueSupp(e.ReponseMsg) = e.ChaineDatas
                        MultiHonda.SetActiveView(VueActivite)
                    Case 170
                        AcompteHonda.RetourDialogueSupp(e.ReponseMsg) = e.ChaineDatas
                        MultiHonda.SetActiveView(VueAcompte)
                End Select
            Case "MajDossier"
                Select Case e.NumeroObjet
                    Case 110
                        ActiviteHonda.V_RetourDialogueMaj(e.ReponseMsg) = e.ChaineDatas
                        MultiHonda.SetActiveView(VueActivite)
                    Case 170
                        AcompteHonda.V_RetourDialogueMaj(e.ReponseMsg) = e.ChaineDatas
                        MultiHonda.SetActiveView(VueAcompte)
                End Select
        End Select
    End Sub

    Protected Sub MessageDialogue(ByVal sender As Object, ByVal e As Virtualia.Systeme.Evenements.MessageSaisieEventArgs) Handles ActiviteHonda.MessageDialogue, AcompteHonda.MessageDialogue
        HPopupMsg.Value = "1"
        CellMessage.Visible = True
        MsgVirtualia.AfficherMessage = e
        PopupMsg.Show()
    End Sub

End Class