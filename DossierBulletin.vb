﻿Option Explicit On
Option Strict On
Option Compare Text
Imports VI = Virtualia.Systeme.Constantes
Namespace WebAppli
    Namespace SelfV4
        Public Class DossierBulletin
            Private WsParent As Virtualia.Net.WebAppli.SelfV4.DossierPersonne
            Private WsLstBulletinPaie As List(Of Virtualia.TablesObjet.ShemaPER.PER_BULLETIN)
            Private WsLstContextePaie As List(Of Virtualia.TablesObjet.ShemaPER.PER_CONTEXTEPAIE)

            Public ReadOnly Property VParent As Virtualia.Net.WebAppli.SelfV4.DossierPersonne
                Get
                    Return WsParent
                End Get
            End Property

            Public ReadOnly Property Cumuls_Virtualia(ByVal ArgumentDate As String, ByVal CodeRegroupement As String) As Double
                Get
                    Dim DateDebut As String = "01/01/" & Strings.Right(ArgumentDate, 4)
                    Dim DateFin As String = WsParent.VParent.VirRhDates.DateSaisieVerifiee("31" & Strings.Right(ArgumentDate, 8))
                    Dim LstBul As List(Of Virtualia.TablesObjet.ShemaPER.PER_BULLETIN)
                    Dim CatMini As Integer
                    Dim CatMaxi As Integer = 19
                    Dim MontantTotal As Double = 0

                    If WsLstBulletinPaie Is Nothing Then
                        Return 0
                    End If

                    Select Case CodeRegroupement
                        Case Is = "BRUT"
                            CatMini = 10
                            CatMaxi = 19
                        Case Is = "IMPOT"
                            CatMini = 50
                            CatMaxi = 50
                        Case Is = "PO"
                            CatMini = 20
                            CatMaxi = 29
                        Case Is = "PP"
                            CatMini = 30
                            CatMaxi = 39
                        Case Else
                            Return 0
                    End Select
                    LstBul = ListeBulletins(Strings.Right(ArgumentDate, 4))
                    LstBul = (From Ligne In WsLstBulletinPaie Where (Ligne.Date_Valeur_ToDate >= CDate(DateDebut) _
                                            And Ligne.Date_Valeur_ToDate <= CDate(DateFin)) And (Ligne.Categorie >= CatMini _
                                            And Ligne.Categorie <= CatMaxi)
                                                   Order By Ligne.Date_Valeur_ToDate Ascending).ToList

                    If LstBul Is Nothing OrElse LstBul.Count = 0 Then
                        Return 0
                    End If
                    For Each BulPaie In LstBul
                        MontantTotal += BulPaie.Montant
                    Next
                    Return MontantTotal
                End Get
            End Property

            Public ReadOnly Property Cumuls_Virtualia(ByVal ArgumentDate As String, ByVal Categorie As Integer) As Double
                Get
                    Dim DateDebut As String = "01/01/" & Strings.Right(ArgumentDate, 4)
                    Dim DateFin As String = WsParent.VParent.VirRhDates.DateSaisieVerifiee("31" & Strings.Right(ArgumentDate, 8))
                    Dim LstBul As List(Of Virtualia.TablesObjet.ShemaPER.PER_BULLETIN)
                    Dim MontantTotal As Double = 0

                    If WsLstBulletinPaie Is Nothing Then
                        Return 0
                    End If
                    LstBul = ListeBulletins(Strings.Right(ArgumentDate, 4))
                    LstBul = (From Ligne In WsLstBulletinPaie Where (Ligne.Date_Valeur_ToDate >= CDate(DateDebut) _
                                            And Ligne.Date_Valeur_ToDate <= CDate(DateFin)) And Ligne.Categorie = Categorie _
                                            Order By Ligne.Date_Valeur_ToDate Ascending).ToList

                    If LstBul Is Nothing OrElse LstBul.Count = 0 Then
                        Return 0
                    End If
                    For Each BulPaie In LstBul
                        MontantTotal += BulPaie.Montant
                    Next
                    Return MontantTotal
                End Get
            End Property

            Public ReadOnly Property ListeContextes() As List(Of Virtualia.TablesObjet.ShemaPER.PER_CONTEXTEPAIE)
                Get
                    If WsLstContextePaie Is Nothing Then
                        Dim TabRes As List(Of Virtualia.Systeme.MetaModele.VIR_FICHE)
                        Dim DateDebut As String = "01/01/" & Year(Now) - 3
                        Dim DateFin As String = "31/12/" & Year(Now)

                        Dim LstIde As List(Of Integer)
                        LstIde = New List(Of Integer)
                        LstIde.Add(WsParent.Ide_Dossier)

                        TabRes = WsParent.VParent.SelectionObjetDate(LstIde, DateDebut, DateFin, VI.ObjetPer.ObaContextePaie)
                        If TabRes Is Nothing Then
                            Return Nothing
                        End If
                        WsLstContextePaie = New List(Of Virtualia.TablesObjet.ShemaPER.PER_CONTEXTEPAIE)
                        For Each FichePER In TabRes
                            WsLstContextePaie.Add(CType(FichePER, Virtualia.TablesObjet.ShemaPER.PER_CONTEXTEPAIE))
                        Next
                    End If
                    Try
                        Return (From instance In WsLstContextePaie Select instance _
                                 Order By instance.Date_Valeur_ToDate Descending).ToList
                    Catch ex As Exception
                        Return Nothing
                    End Try
                End Get
            End Property

            Public ReadOnly Property FicheContexte_Valable(ByVal ArgumentDate As String) As Virtualia.TablesObjet.ShemaPER.PER_CONTEXTEPAIE
                Get
                    Dim IndiceF As Integer
                    Dim LstTriee As List(Of Virtualia.TablesObjet.ShemaPER.PER_CONTEXTEPAIE) = Nothing
                    Dim FichePER As Virtualia.TablesObjet.ShemaPER.PER_CONTEXTEPAIE

                    If WsLstContextePaie Is Nothing Then
                        Return Nothing
                    End If
                    LstTriee = (From instance In WsLstContextePaie Select instance _
                                  Order By instance.Date_Valeur_ToDate Descending).ToList

                    For IndiceF = 0 To LstTriee.Count - 1
                        FichePER = CType(LstTriee.Item(IndiceF), Virtualia.TablesObjet.ShemaPER.PER_CONTEXTEPAIE)
                        Select Case WsParent.VParent.VirRhDates.ComparerDates(FichePER.Date_de_Valeur, ArgumentDate)
                            Case VI.ComparaisonDates.Egalite, VI.ComparaisonDates.PlusPetit
                                Return FichePER
                        End Select
                    Next IndiceF
                    Return Nothing
                End Get
            End Property

            Public ReadOnly Property FicheContexte_Valable(ByVal DateDebut As String, ByVal DateFin As String) As Virtualia.TablesObjet.ShemaPER.PER_CONTEXTEPAIE
                Get
                    Dim LstFiches As List(Of Virtualia.TablesObjet.ShemaPER.PER_CONTEXTEPAIE) = Nothing

                    If WsLstContextePaie Is Nothing Then
                        Return Nothing
                    End If
                    Try
                        LstFiches = (From instance In WsLstContextePaie Select instance _
                                  Where instance.Date_Valeur_ToDate >= CDate(DateDebut) And _
                                  instance.Date_Valeur_ToDate <= CDate(DateFin)).ToList
                    Catch ex As Exception
                        Return Nothing
                    End Try
                    If LstFiches IsNot Nothing Then
                        If LstFiches.Count > 0 Then
                            Return LstFiches.Item(0)
                        End If
                    End If
                    Return Nothing
                End Get
            End Property

            Public ReadOnly Property ListeBulletins(ByVal Annee As String) As List(Of Virtualia.TablesObjet.ShemaPER.PER_BULLETIN)
                Get
                    Dim DateDebut As String = "01/01/" & Annee
                    Dim DateFin As String = "31/12/" & Annee
                    Dim TabRes As List(Of Virtualia.Systeme.MetaModele.VIR_FICHE)
                    Dim LignePaie As Virtualia.TablesObjet.ShemaPER.PER_BULLETIN = Nothing

                    If WsLstBulletinPaie IsNot Nothing Then
                        If WsLstBulletinPaie.Count > 0 Then
                            Try
                                LignePaie = (From Ligne In WsLstBulletinPaie Where Ligne.Date_Valeur_ToDate >= CDate(DateDebut) _
                                                And Ligne.Date_Valeur_ToDate <= CDate(DateFin)).First
                            Catch ex As Exception
                                LignePaie = Nothing
                                WsLstBulletinPaie = Nothing
                            End Try
                        End If
                    End If

                    If WsLstBulletinPaie Is Nothing Then

                        Dim LstIde As List(Of Integer)
                        LstIde = New List(Of Integer)
                        LstIde.Add(WsParent.Ide_Dossier)

                        TabRes = WsParent.VParent.SelectionObjetDate(LstIde, DateDebut, DateFin, VI.ObjetPer.ObaBulletin)
                        If TabRes Is Nothing Then
                            Return Nothing
                        End If
                        WsLstBulletinPaie = New List(Of Virtualia.TablesObjet.ShemaPER.PER_BULLETIN)
                        For Each Fiche In TabRes
                            WsLstBulletinPaie.Add(CType(Fiche, Virtualia.TablesObjet.ShemaPER.PER_BULLETIN))
                        Next
                    End If
                    Return (From Ligne In WsLstBulletinPaie Order By Ligne.Date_Valeur_ToDate Ascending).ToList
                End Get
            End Property

            Public Sub New(ByVal host As Virtualia.Net.WebAppli.SelfV4.DossierPersonne)
                WsParent = host
            End Sub
        End Class
    End Namespace
End Namespace