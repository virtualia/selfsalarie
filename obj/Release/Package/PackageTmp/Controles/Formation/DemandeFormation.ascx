﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="DemandeFormation.ascx.vb" Inherits="Virtualia.Net.DemandeFormation" %>

<%@ Register src="~/Controles/Saisies/VCoupleEtiDonnee.ascx" tagname="VCoupleEtiDonnee" tagprefix="Virtualia" %>
<%@ Register src="~/Controles/Saisies/VCoupleVerticalEtiDonnee.ascx" tagname="VCoupleVerticalEtiDonnee" tagprefix="Virtualia" %>
<%@ Register src="~/Controles/Saisies/VCocheSimple.ascx" tagname="VCocheSimple" tagprefix="Virtualia" %>
<%@ Register src="~/Controles/Commun/VListeCombo.ascx" tagname="VListeCombo" tagprefix="Virtualia" %>

 <asp:Table ID="CadreInfo" runat="server" BorderStyle="NotSet" BorderWidth="2px" Visible="true" BorderColor="#B0E0D7" Width="720px" 
        HorizontalAlign="Center" style="margin-top: 1px;">
     <asp:TableRow>
          <asp:TableCell HorizontalAlign="Left">
              <asp:Label ID="Etiquette" runat="server" Text="Demande en vue de suivre une formation" Height="20px" Width="700px"
                   BackColor="#2FA49B" BorderColor="#B0E0D7" BorderStyle="Groove" BorderWidth="2px" ForeColor="White"
                   Font-Bold="True" Font-Names="Trebuchet MS" Font-Size="Medium" Font-Italic="true"
                   style="margin-top: 5px;margin-bottom: 10px;text-indent: 5px;text-align: center;">
              </asp:Label>          
          </asp:TableCell>
     </asp:TableRow> 
     <asp:TableRow>
        <asp:TableCell HorizontalAlign="Left">
            <Virtualia:VCoupleEtiDonnee ID="InfoH00" runat="server" V_SiDonneeDico="false" V_SiEnLectureSeule="True"
                Etivisible="True" DonWidth="120px" DonHeight="22px" DonTabIndex="1" Donstyle="text-align:center;"
                Etitext="Date de la demande" EtiWidth="150px"  DonBorderWidth="1px" DonBackColor="#E2F5F1"/>             
        </asp:TableCell>
     </asp:TableRow>
     <asp:TableRow>
         <asp:TableCell>
            <asp:Table ID="CadreCoches" runat="server">
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Left">
                        <Virtualia:VCocheSimple ID="Coche04" runat="server" V_SiDonneeDico="false" V_Text="Si Souhait hors plan" V_SiAutoPostBack="true" /> 
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Left">
                        <Virtualia:VCocheSimple ID="Coche08" runat="server" V_SiDonneeDico="false" V_Text="A faire au titre du DIF" V_SiAutoPostBack="true" /> 
                    </asp:TableCell>
                </asp:TableRow>  
            </asp:Table>
         </asp:TableCell>
     </asp:TableRow>
     <asp:TableRow>
        <asp:TableCell HorizontalAlign="Left">
            <Virtualia:VCoupleEtiDonnee ID="InfoH01" runat="server" V_SiDonneeDico="false" V_SiEnLectureSeule="True" DonBackColor="#E2F5F1" V_Format="0" DonMaxLength="120"
                Etivisible="True" DonWidth="550px" DonHeight="22px" DonTabIndex="2" Etitext="Objet du stage" EtiWidth="150px"  DonBorderWidth="1px"/>             
        </asp:TableCell>
     </asp:TableRow>
     <asp:TableRow>
        <asp:TableCell HorizontalAlign="Left">
            <asp:Table ID="CadreDates" runat="server">
                <asp:TableRow>
                    <asp:TableCell>
                            <Virtualia:VCoupleEtiDonnee ID="InfoH02" runat="server" V_SiDonneeDico="false" V_SiEnLectureSeule="True" DonBackColor="#E2F5F1" V_Format="1"
                                Etivisible="True" DonWidth="120px" DonHeight="22px" DonTabIndex="3" Donstyle="text-align: center;"
                                Etitext="Date de la session" EtiWidth="150px"  DonBorderWidth="1px"/>             
                    </asp:TableCell>
                    <asp:TableCell>
                            <Virtualia:VCoupleEtiDonnee ID="InfoH07" runat="server" V_SiDonneeDico="false" V_SiEnLectureSeule="True" DonBackColor="#E2F5F1" V_Format="1"
                                Etivisible="True" DonWidth="120px" DonHeight="22px" DonTabIndex="3" Donstyle="text-align: center;"
                                Etitext="Date de fin de la session" EtiWidth="200px"  DonBorderWidth="1px"/>             
                    </asp:TableCell>
                </asp:TableRow>
            </asp:Table>
        </asp:TableCell>
     </asp:TableRow>
     <asp:TableRow>
        <asp:TableCell HorizontalAlign="Left">
            <Virtualia:VCoupleEtiDonnee ID="InfoH05" runat="server" V_SiDonneeDico="false" V_SiEnLectureSeule="True" DonBackColor="#E2F5F1" V_Format="0" DonMaxLength="120"
                Etivisible="True" DonWidth="550px" DonHeight="22px" DonTabIndex="2" Etitext="Organisme formateur" EtiWidth="150px"  DonBorderWidth="1px"/>             
        </asp:TableCell>
     </asp:TableRow>
     <asp:TableRow>
        <asp:TableCell HorizontalAlign="Left">
            <Virtualia:VCoupleEtiDonnee ID="InfoH06" runat="server" V_SiDonneeDico="false" V_SiEnLectureSeule="True" DonBackColor="#E2F5F1" V_Format="0"
                Etivisible="True" DonWidth="550px" DonHeight="22px" DonTabIndex="2" Etitext="Lieu de la formation" EtiWidth="150px"  DonBorderWidth="1px"/>             
        </asp:TableCell>
     </asp:TableRow>
     <asp:TableRow>
        <asp:TableCell HorizontalAlign="Left">
            <Virtualia:VListeCombo ID="ListeEcheance" runat="server" LstStyle="text-align: center;" EtiText="Echéance souhaitée" LstWidth="120px" LstHeight="22px" LstBackColor="White"/>             
        </asp:TableCell>
     </asp:TableRow>
     <asp:TableRow>
        <asp:TableCell HorizontalAlign="Left">
            <Virtualia:VCoupleVerticalEtiDonnee ID="InfoV03" runat="server" V_SiDonneeDico="false" Etivisible="True" DonWidth="700px" DonHeight="90px" DonMaxLength="500"
                DonTabIndex="4" Etitext="Observations (Motivez ici votre demande)" EtiWidth="700px" DonBorderWidth="1px"/>             
        </asp:TableCell>
     </asp:TableRow>
     <asp:TableRow>
         <asp:TableCell>
             <asp:HyperLink runat="server" ID="LienFormation" Target="_blank" Font-Names="Trebuchet MS" Text="" />
         </asp:TableCell>
     </asp:TableRow>
     <asp:TableRow>
         <asp:TableCell HorizontalAlign="Center">
            <asp:Table ID="CadreCmdOK" runat="server" BackImageUrl="~/Images/Boutons/OK_Std.bmp" Width="70px"
                BorderWidth="2px" BorderStyle="Outset" BorderColor="#B0E0D7" ForeColor="#D7FAF3" CellPadding="0" CellSpacing="0" Visible="true">
                <asp:TableRow>
                    <asp:TableCell>
                        <asp:Button ID="CommandeOK" runat="server" Text="Envoyer" Width="65px" Height="20px"
                            BackColor="Transparent" BorderColor="#B0E0D7" ForeColor="#D7FAF3" 
                            Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small" Font-Italic="true"
                            BorderStyle="None" style=" margin-left: 6px; text-align: right;">
                            </asp:Button>
                    </asp:TableCell>
                </asp:TableRow>
            </asp:Table>  
          </asp:TableCell>
     </asp:TableRow>
</asp:Table>
