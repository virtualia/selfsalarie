﻿Option Strict On
Option Explicit On
Option Compare Text
Imports System.Collections.Generic
Imports VI = Virtualia.Systeme.Constantes
Public Class CtlLectureActivite
    Inherits System.Web.UI.UserControl
    Public Delegate Sub Valeur_ChangeEventHandler(ByVal sender As Object, ByVal e As Virtualia.Systeme.Evenements.DonneeChangeEventArgs)
    Public Event Modifier As Valeur_ChangeEventHandler
    Private WebFct As Virtualia.Net.Controles.WebFonctions
    Private WsNomStateCache As String = "VLireActivite"
    Private WsCtl_Cache As Virtualia.Net.Controles.CacheControleActivite
    Private WsListeActivites As List(Of Virtualia.TablesObjet.ShemaPER.PER_ACTIVITE_JOUR)
    Private WsNbElements As Integer = 5

    Protected Overridable Sub Saisie_Change(ByVal e As Virtualia.Systeme.Evenements.DonneeChangeEventArgs)
        RaiseEvent Modifier(Me, e)
    End Sub

    Private ReadOnly Property V_WebFonction As Virtualia.Net.Controles.WebFonctions
        Get
            If WebFct Is Nothing Then
                WebFct = New Virtualia.Net.Controles.WebFonctions(Me, 0)
            End If
            Return WebFct
        End Get
    End Property

    Private Property CacheVirControle As Virtualia.Net.Controles.CacheControleActivite
        Get
            If Me.ViewState(WsNomStateCache) IsNot Nothing Then
                Return CType(Me.ViewState(WsNomStateCache), Virtualia.Net.Controles.CacheControleActivite)
            End If
            Dim NewCache As Virtualia.Net.Controles.CacheControleActivite
            NewCache = New Virtualia.Net.Controles.CacheControleActivite
            Return NewCache
        End Get
        Set(value As Virtualia.Net.Controles.CacheControleActivite)
            If value Is Nothing Then
                Exit Property
            End If
            If Me.ViewState(WsNomStateCache) IsNot Nothing Then
                Me.ViewState.Remove(WsNomStateCache)
            End If
            Me.ViewState.Add(WsNomStateCache, value)
        End Set
    End Property

    Public Property NombreElements As Integer
        Get
            Return WsNbElements
        End Get
        Set(value As Integer)
            WsNbElements = value
        End Set
    End Property

    Public Property Date_Valeur As String
        Get
            WsCtl_Cache = CacheVirControle
            If WsCtl_Cache.DateValeur <> "" Then
                Return WsCtl_Cache.DateValeur
            Else
                Return V_WebFonction.ViRhDates.Semaine_Date_Debut(V_WebFonction.ViRhDates.DateduJour)
            End If
        End Get
        Set(value As String)
            WsCtl_Cache = CacheVirControle
            WsCtl_Cache.DateValeur = value
            CacheVirControle = WsCtl_Cache
        End Set
    End Property

    Private Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        Call CreerLesElements()
    End Sub

    Private Sub Page_PreRender(sender As Object, e As EventArgs) Handles Me.PreRender
        Call FaireLibelles()
        Call FairePlanning()
    End Sub

    Private Sub FairePlanning()
        Dim LstCalend As List(Of Virtualia.Net.Controles.ItemJourCalendrier) = Nothing
        Dim ListeJours As List(Of Virtualia.TablesObjet.ShemaPER.PER_ACTIVITE_JOUR) = Nothing
        Dim FichePER As Virtualia.TablesObjet.ShemaPER.PER_ACTIVITE_JOUR
        Dim LstAbs As List(Of Virtualia.Net.Controles.ItemJourCalendrier) = Nothing
        Dim FicheAbs As Virtualia.Net.Controles.ItemJourCalendrier = Nothing
        Dim LstDescriptif As List(Of KeyValuePair(Of String, String))
        Dim DescriptifToolTip As String = ""
        Dim DateW As String
        Dim IndiceI As Integer
        Dim IndiceJ As Integer
        Dim Couleur As Drawing.Color
        Dim CadrePrincipal As Control
        Dim Ctl As Control
        Dim SiCmdEnabled As Boolean
        Dim DateDebut As String
        Dim TotalElements As Integer = 0

        If V_WebFonction.PointeurUtilisateur Is Nothing Then
            Exit Sub
        End If
        DateDebut = Date_Valeur
        WsListeActivites = CType(V_WebFonction.PointeurUtilisateur.V_PointeurSessionModule, Virtualia.Net.Session.SessionModule).DossierPER.VListeActivites(DateDebut, V_WebFonction.ViRhDates.CalcDateMoinsJour(DateDebut, "6", "0"))
        If WsListeActivites Is Nothing Then
            IndiceI = 0
            Do
                CadrePrincipal = V_WebFonction.VirWebControle(Me.CadreGlobal, "CadreElement", IndiceI)
                If CadrePrincipal Is Nothing Then
                    Exit Do
                End If
                CadrePrincipal.Visible = False
                IndiceI += 1
                If IndiceI > NombreElements - 1 Then
                    Exit Do
                End If
            Loop
            Exit Sub
        End If
        SiCmdEnabled = True
        If WsListeActivites.Item(0).Date_Visa <> "" Then
            SiCmdEnabled = CType(V_WebFonction.PointeurUtilisateur.V_PointeurSessionModule, Virtualia.Net.Session.SessionModule).SiAccesGRH
        End If

        Dim LstElements = (From instance In WsListeActivites Order By instance.Activite, instance.Tache_Etape, instance.Precision
                           Select instance.Activite, instance.Tache_Etape, instance.Precision).Distinct.ToList
        TotalElements = LstElements.Count
        IndiceI = 0
        Do
            CadrePrincipal = V_WebFonction.VirWebControle(Me.CadreGlobal, "CadreElement", IndiceI)
            If CadrePrincipal Is Nothing Then
                Exit Do
            End If
            If IndiceI < TotalElements Then
                CadrePrincipal.Visible = True
                Ctl = V_WebFonction.VirWebControle(CadrePrincipal, "CadreIntitule", 0)
                If Ctl IsNot Nothing Then
                    LstDescriptif = CType(V_WebFonction.PointeurGlobal, Virtualia.Net.Session.ObjetGlobal).VirReferentiel.ListeActivites(LstElements.Item(IndiceI).Activite)
                    DescriptifToolTip = ""
                    If LstDescriptif IsNot Nothing AndAlso LstDescriptif.Count > 0 Then
                        For Each Element In LstDescriptif
                            If Element.Key = LstElements.Item(IndiceI).Tache_Etape Then
                                DescriptifToolTip = Element.Value
                                Exit For
                            End If
                        Next
                    End If
                    VElement(Ctl, "DonActivite", DescriptifToolTip) = LstElements.Item(IndiceI).Activite
                    VElement(Ctl, "DonProduit", "") = LstElements.Item(IndiceI).Tache_Etape
                    VElement(Ctl, "DonPrecision") = LstElements.Item(IndiceI).Precision
                End If
            Else
                CadrePrincipal.Visible = False
            End If
            IndiceI += 1
            If IndiceI > NombreElements - 1 Then
                Exit Do
            End If
        Loop
        LstCalend = CType(V_WebFonction.PointeurUtilisateur.V_PointeurSessionModule, Virtualia.Net.Session.SessionModule).DossierPER.VDossier_Calendrier(DateDebut, V_WebFonction.ViRhDates.CalcDateMoinsJour(DateDebut, "4", "0"))
        IndiceI = 0
        Do
            CadrePrincipal = V_WebFonction.VirWebControle(Me.CadreGlobal, "CadreElement", IndiceI)
            If CadrePrincipal Is Nothing Then
                Exit Do
            End If
            Ctl = V_WebFonction.VirWebControle(CadrePrincipal, "CadreRepartition", 0)
            If Ctl Is Nothing Then
                Exit Do
            End If
            If CadrePrincipal.Visible = False Then
                Exit Do
            End If
            IndiceJ = 0
            DateW = DateDebut
            Do
                VCmdModifierVisible(Ctl) = SiCmdEnabled
                FichePER = Nothing
                If WsListeActivites IsNot Nothing Then
                    ListeJours = WsListeActivites.FindAll(Function(Recherche) Recherche.Date_Valeur_ToDate = V_WebFonction.ViRhDates.DateTypee(DateW))
                    If ListeJours IsNot Nothing Then
                        FichePER = ListeJours.Find(Function(Recherche) Recherche.ClefConcatenee = VElementsConcatenes(IndiceI))
                    End If
                End If
                If FichePER IsNot Nothing Then
                    If FichePER.ClefConcatenee = VElementsConcatenes(IndiceI) Then
                        VText(Ctl, "Donnee_", IndiceJ) = FichePER.Pourcentage_Repartition.ToString
                    End If
                Else
                    VText(Ctl, "Donnee_", IndiceJ) = ""
                End If
                VJourToolTip(Ctl, "Donnee_", IndiceJ) = ""
                VBackColor(Ctl, "Donnee_", IndiceJ) = V_WebFonction.PointeurGlobal.InstanceCouleur.Item(0).Couleur_Web
                VBorderColor(Ctl, "Donnee_", IndiceJ) = V_WebFonction.ConvertCouleur("#B0E0D7")

                FicheAbs = Nothing
                '** Inclure Le prévisionnbel ajusté et les absences
                If LstCalend IsNot Nothing Then
                    LstAbs = (From FicheCal In LstCalend Where FicheCal.Date_Valeur_ToDate = V_WebFonction.ViRhDates.DateTypee(DateW)).ToList
                    If LstAbs IsNot Nothing AndAlso LstAbs.Count > 0 Then
                        Select Case LstAbs.Count
                            Case 1
                                FicheAbs = LstAbs.Item(0)
                            Case Else
                                For Each FicheAbs In LstAbs
                                    Select Case FicheAbs.Caracteristique
                                        Case VI.NumeroPlage.Plage2_Absence, VI.NumeroPlage.Plage2_Formation
                                            Exit For
                                    End Select
                                Next
                        End Select
                    End If
                End If
                If FicheAbs IsNot Nothing Then
                    Select Case FicheAbs.Caracteristique
                        Case VI.NumeroPlage.Jour_Formation, VI.NumeroPlage.Plage1_Formation, VI.NumeroPlage.Plage2_Formation
                            Couleur = V_WebFonction.PointeurGlobal.InstanceCouleur.Item(3).Couleur_Web
                        Case Else
                            Couleur = V_WebFonction.PointeurGlobal.InstanceCouleur.Item(V_WebFonction.PointeurGlobal.InstanceCouleur.IndexCouleurPlanning(FicheAbs.Intitule)).Couleur_Web
                    End Select
                    Select Case FicheAbs.Caracteristique
                        Case VI.NumeroPlage.Jour_Absence, VI.NumeroPlage.Jour_Formation
                            VBackColor(Ctl, "Donnee_", IndiceJ) = Couleur
                            VJourToolTip(Ctl, "Donnee_", IndiceJ) = FicheAbs.Intitule
                        Case VI.NumeroPlage.Plage1_Absence, VI.NumeroPlage.Plage1_Formation
                            VJourToolTip(Ctl, "Donnee_", IndiceJ) = FicheAbs.Intitule
                        Case VI.NumeroPlage.Plage2_Absence, VI.NumeroPlage.Plage2_Formation
                            VJourToolTip(Ctl, "Donnee_", IndiceJ) = FicheAbs.Intitule
                    End Select
                End If
                If V_WebFonction.ViRhDates.SiJourOuvre(DateW, True) = False Then
                    VBackColor(Ctl, "Donnee_", IndiceJ) = V_WebFonction.PointeurGlobal.InstanceCouleur.Item(1).Couleur_Web
                End If
                If V_WebFonction.ViRhDates.SiJourFerie(DateW) = True Then
                    VBorderColor(Ctl, "Donnee_", IndiceJ) = Drawing.Color.Red
                End If
                DateW = V_WebFonction.ViRhDates.CalcDateMoinsJour(DateW, "1", "0")
                IndiceJ += 1
                If IndiceJ > 6 Then
                    Exit Do
                End If
            Loop
            IndiceI += 1
        Loop
    End Sub

    Private Sub FaireLibelles()
        Dim LstLibels As List(Of String)
        Dim IndiceI As Integer
        Dim IndiceK As Integer
        Dim Ctl As Control
        Dim Etiquette As System.Web.UI.WebControls.Label
        Dim CadreEti As System.Web.UI.WebControls.Table
        Dim DateDebutSemaine As String

        If V_WebFonction.PointeurUtilisateur Is Nothing Then
            Exit Sub
        End If
        DateDebutSemaine = Date_Valeur

        LstLibels = New List(Of String)
        If DateDebutSemaine = "" Then
            For IndiceI = 1 To 7
                LstLibels.Add("")
            Next IndiceI
        Else
            LstLibels.Add(Strings.Left(DateDebutSemaine, 5))
            For IndiceI = 1 To 6
                LstLibels.Add(Strings.Left(V_WebFonction.ViRhDates.DateTypee(DateDebutSemaine).AddDays(IndiceI).ToShortDateString, 5))
            Next IndiceI
        End If

        IndiceI = 0
        Do
            Ctl = V_WebFonction.VirWebControle(Me.CadreGlobal, "CadreRepartition", IndiceI)
            If Ctl Is Nothing Then
                Exit Do
            End If
            CadreEti = CType(Ctl, System.Web.UI.WebControls.Table)
            IndiceK = 0
            Do
                Ctl = V_WebFonction.VirWebControle(CadreEti, "EtiDate", IndiceK)
                If Ctl Is Nothing Then
                    Exit Do
                End If
                Etiquette = CType(Ctl, System.Web.UI.WebControls.Label)
                Select Case Etiquette.ToolTip
                    Case "Lundi"
                        Etiquette.Text = LstLibels(0)
                    Case "Mardi"
                        Etiquette.Text = LstLibels(1)
                    Case "Mercredi"
                        Etiquette.Text = LstLibels(2)
                    Case "Jeudi"
                        Etiquette.Text = LstLibels(3)
                    Case "Vendredi"
                        Etiquette.Text = LstLibels(4)
                    Case "Samedi"
                        Etiquette.Text = LstLibels(5)
                    Case "Dimanche"
                        Etiquette.Text = LstLibels(6)
                End Select
                IndiceK += 1
            Loop
            IndiceI += 1
        Loop
    End Sub

    Private Sub CreerLesElements()
        Dim Ligne As System.Web.UI.WebControls.TableRow
        Dim Cellule As System.Web.UI.WebControls.TableCell
        Dim IndiceI As Integer

        For IndiceI = 2 To WsNbElements
            Ligne = New System.Web.UI.WebControls.TableRow
            Cellule = New System.Web.UI.WebControls.TableCell
            Cellule.Controls.Add(CreerUnElementDynamique(IndiceI))
            Ligne.Controls.Add(Cellule)
            CadreGlobal.Controls.Add(Ligne)
            '** Séparation de quelqes px
            Ligne = New System.Web.UI.WebControls.TableRow
            Cellule = New System.Web.UI.WebControls.TableCell
            Cellule.Height = New Unit(5)
            Ligne.Controls.Add(Cellule)
            CadreGlobal.Controls.Add(Ligne)
        Next IndiceI
    End Sub

    Private Function CreerUnElementDynamique(ByVal Index As Integer) As System.Web.UI.WebControls.Table
        Dim CadreDyna As System.Web.UI.WebControls.Table
        Dim Rangee As System.Web.UI.WebControls.TableRow
        Dim CellDyna As System.Web.UI.WebControls.TableCell

        CadreDyna = New System.Web.UI.WebControls.Table
        CadreDyna.ID = "CadreElement_" & Strings.Format(Index, "00")
        CadreDyna.BackColor = CadreElement_01.BackColor
        CadreDyna.BorderColor = CadreElement_01.BorderColor
        CadreDyna.BorderStyle = CadreElement_01.BorderStyle
        CadreDyna.BorderWidth = CadreElement_01.BorderWidth
        CadreDyna.CellPadding = CadreElement_01.CellPadding
        CadreDyna.CellSpacing = CadreElement_01.CellSpacing
        CadreDyna.HorizontalAlign = CadreElement_01.HorizontalAlign
        CadreDyna.Width = CadreElement_01.Width

        Rangee = New System.Web.UI.WebControls.TableRow
        Rangee.VerticalAlign = VerticalAlign.Top

        CellDyna = New System.Web.UI.WebControls.TableCell
        CellDyna.Controls.Add(CreerTableEntete(Index))
        Rangee.Controls.Add(CellDyna)

        CellDyna = New System.Web.UI.WebControls.TableCell
        CellDyna.Controls.Add(CreerTableRepartition(Index))
        Rangee.Controls.Add(CellDyna)

        CadreDyna.Controls.Add(Rangee)
        CadreDyna.Visible = False
        Return CadreDyna
    End Function

    Private Function CreerTableEntete(ByVal Index As Integer) As System.Web.UI.WebControls.Table
        Dim IndiceC As Integer
        Dim CadreTable As System.Web.UI.WebControls.Table
        Dim Rangee As System.Web.UI.WebControls.TableRow
        Dim Cellule As System.Web.UI.WebControls.TableCell
        Dim EtiDyna As System.Web.UI.WebControls.Label
        Dim DonneDyna As System.Web.UI.WebControls.TextBox

        CadreTable = New System.Web.UI.WebControls.Table
        CadreTable.ID = "CadreIntitule_" & Strings.Format(Index, "00")
        CadreTable.CssClass = CadreIntitule_01.CssClass
        CadreTable.CellPadding = CadreIntitule_01.CellPadding
        CadreTable.CellSpacing = CadreIntitule_01.CellSpacing
        CadreTable.Height = CadreIntitule_01.Height
        CadreTable.HorizontalAlign = CadreIntitule_01.HorizontalAlign
        CadreTable.Width = CadreIntitule_01.Width

        For IndiceC = 1 To 3
            Rangee = New System.Web.UI.WebControls.TableRow
            Rangee.VerticalAlign = VerticalAlign.Top
            Rangee.Visible = True
            Cellule = New System.Web.UI.WebControls.TableCell
            Select Case IndiceC
                Case 1
                    Cellule.ID = "CellEtiActivite_" & Strings.Format(Index, "00")
                Case 2
                    Cellule.ID = "CellEtiProduit_" & Strings.Format(Index, "00")
                Case 3
                    Cellule.ID = "CellEtiPrecision_" & Strings.Format(Index, "00")
            End Select
            Cellule.CssClass = "CellEtiIntitule"
            Cellule.Visible = True
            EtiDyna = New System.Web.UI.WebControls.Label
            Select Case IndiceC
                Case 1
                    EtiDyna.ID = "EtiActivite_" & Strings.Format(Index, "00")
                    EtiDyna.Text = "Projet"
                Case 2
                    EtiDyna.ID = "EtiProduit_" & Strings.Format(Index, "00")
                    EtiDyna.Text = "Etape"
                Case 3
                    EtiDyna.ID = "EtiPrecision_" & Strings.Format(Index, "00")
                    EtiDyna.Text = "Précision"
            End Select
            EtiDyna.CssClass = "EtiIntitule"
            EtiDyna.Visible = True
            Cellule.Controls.Add(EtiDyna)
            Rangee.Controls.Add(Cellule)

            Cellule = New System.Web.UI.WebControls.TableCell
            Select Case IndiceC
                Case 1
                    Cellule.ID = "CellDonActivite_" & Strings.Format(Index, "00")
                Case 2
                    Cellule.ID = "CellDonProduit_" & Strings.Format(Index, "00")
                Case 3
                    Cellule.ID = "CellDonPrecision_" & Strings.Format(Index, "00")
            End Select
            Cellule.CssClass = "CellEtiIntitule"
            Cellule.Visible = True
            DonneDyna = New System.Web.UI.WebControls.TextBox
            Select Case IndiceC
                Case 1
                    DonneDyna.ID = "DonActivite_" & Strings.Format(Index, "00")
                Case 2
                    DonneDyna.ID = "DonProduit_" & Strings.Format(Index, "00")
                Case 3
                    DonneDyna.ID = "DonPrecision_" & Strings.Format(Index, "00")
            End Select
            DonneDyna.CssClass = "DonIntitule"
            DonneDyna.ReadOnly = True
            DonneDyna.AutoPostBack = False
            DonneDyna.TextMode = TextBoxMode.SingleLine
            DonneDyna.Text = ""
            DonneDyna.Visible = True
            Cellule.Controls.Add(DonneDyna)
            Rangee.Controls.Add(Cellule)

            CadreTable.Controls.Add(Rangee)
        Next IndiceC

        CadreTable.Visible = True
        Return CadreTable
    End Function

    Private Function CreerTableRepartition(ByVal Index As Integer) As System.Web.UI.WebControls.Table
        Dim IndiceC As Integer
        Dim Libelle As String = ""
        Dim CadreTable As System.Web.UI.WebControls.Table
        Dim Rangee As System.Web.UI.WebControls.TableRow
        Dim Cellule As System.Web.UI.WebControls.TableCell
        Dim EtiDyna As System.Web.UI.WebControls.Label
        Dim DonneDyna As System.Web.UI.WebControls.TextBox
        Dim CmdDyna As System.Web.UI.WebControls.ImageButton

        CadreTable = New System.Web.UI.WebControls.Table
        CadreTable.ID = "CadreRepartition_" & Strings.Format(Index, "00")
        CadreTable.CssClass = CadreRepartition_01.CssClass
        CadreTable.CellPadding = CadreRepartition_01.CellPadding
        CadreTable.CellSpacing = CadreRepartition_01.CellSpacing
        CadreTable.Height = CadreRepartition_01.Height
        CadreTable.HorizontalAlign = CadreRepartition_01.HorizontalAlign
        CadreTable.Width = CadreRepartition_01.Width

        Rangee = New System.Web.UI.WebControls.TableRow
        Rangee.VerticalAlign = VerticalAlign.Top
        For IndiceC = 1 To 8
            Cellule = New System.Web.UI.WebControls.TableCell
            EtiDyna = New System.Web.UI.WebControls.Label
            Select Case IndiceC
                Case 1
                    Libelle = "Lundi"
                Case 2
                    Libelle = "Mardi"
                Case 3
                    Libelle = "Mercredi"
                Case 4
                    Libelle = "Jeudi"
                Case 5
                    Libelle = "Vendredi"
                Case 6
                    Libelle = "Samedi"
                Case 7
                    Libelle = "Dimanche"
                Case 8
                    Libelle = "Semaine"
            End Select
            Cellule.ID = "CellDate" & Libelle & "_" & Strings.Format(Index, "00")
            Cellule.CssClass = "CellEtiIntitule"
            Cellule.Visible = True
            EtiDyna.ID = "EtiDate" & Libelle & "_" & Strings.Format(Index, "00")
            EtiDyna.CssClass = "DateDonnee"
            EtiDyna.Text = ""
            EtiDyna.ToolTip = Libelle
            EtiDyna.Visible = True
            If IndiceC = 8 Then
                EtiDyna.Width = EtiDateSemaine_01.Width
                EtiDyna.ToolTip = ""
            End If
            Cellule.Controls.Add(EtiDyna)
            Rangee.Controls.Add(Cellule)

        Next IndiceC
        CadreTable.Controls.Add(Rangee)
        CadreTable.Visible = True

        Rangee = New System.Web.UI.WebControls.TableRow
        Rangee.VerticalAlign = VerticalAlign.Top
        For IndiceC = 1 To 8
            Cellule = New System.Web.UI.WebControls.TableCell
            EtiDyna = New System.Web.UI.WebControls.Label
            Select Case IndiceC
                Case 1
                    Libelle = "Lundi_" & Strings.Format(Index, "00")
                Case 2
                    Libelle = "Mardi_" & Strings.Format(Index, "00")
                Case 3
                    Libelle = "Mercredi_" & Strings.Format(Index, "00")
                Case 4
                    Libelle = "Jeudi_" & Strings.Format(Index, "00")
                Case 5
                    Libelle = "Vendredi_" & Strings.Format(Index, "00")
                Case 6
                    Libelle = "Samedi_" & Strings.Format(Index, "00")
                Case 7
                    Libelle = "Dimanche_" & Strings.Format(Index, "00")
                Case 8
                    Libelle = "Semaine_" & Strings.Format(Index, "00")
            End Select
            Cellule.ID = "Cell" & Libelle
            Cellule.CssClass = "CellEtiIntitule"
            Cellule.Visible = True
            EtiDyna.ID = "Eti" & Libelle
            EtiDyna.CssClass = "JourDonnee"
            EtiDyna.Text = Strings.Left(Libelle, 1)
            EtiDyna.Visible = True
            If IndiceC = 8 Then
                EtiDyna.Width = EtiSemaine_01.Width
                EtiDyna.Text = ""
            End If
            Cellule.Controls.Add(EtiDyna)
            Rangee.Controls.Add(Cellule)

        Next IndiceC
        CadreTable.Controls.Add(Rangee)
        CadreTable.Visible = True

        Rangee = New System.Web.UI.WebControls.TableRow
        Rangee.VerticalAlign = VerticalAlign.Top
        For IndiceC = 1 To 7
            Cellule = New System.Web.UI.WebControls.TableCell
            DonneDyna = New System.Web.UI.WebControls.TextBox
            Select Case IndiceC
                Case 1
                    Libelle = "Lundi"
                Case 2
                    Libelle = "Mardi"
                Case 3
                    Libelle = "Mercredi"
                Case 4
                    Libelle = "Jeudi"
                Case 5
                    Libelle = "Vendredi"
                Case 6
                    Libelle = "Samedi"
                Case 7
                    Libelle = "Dimanche"
            End Select
            DonneDyna.ID = "Donnee_" & Strings.Format(Index, "00") & "_" & Strings.Format(IndiceC - 1, "00")
            DonneDyna.CssClass = "Donnee"
            DonneDyna.ReadOnly = True
            DonneDyna.AutoPostBack = False
            DonneDyna.TextMode = TextBoxMode.SingleLine
            DonneDyna.Text = ""
            DonneDyna.ToolTip = Libelle
            DonneDyna.Visible = True

            Cellule.Controls.Add(DonneDyna)
            Rangee.Controls.Add(Cellule)

        Next IndiceC

        Cellule = New System.Web.UI.WebControls.TableCell
        Cellule.ID = "CellModifier" & "_" & Strings.Format(Index, "00")
        Cellule.CssClass = CellModifier_01.CssClass
        Cellule.Visible = True

        CmdDyna = New System.Web.UI.WebControls.ImageButton
        CmdDyna.ID = "CmdModifier" & "_" & Strings.Format(Index, "00")
        CmdDyna.CssClass = CmdModifier_01.CssClass
        CmdDyna.ImageAlign = CmdModifier_01.ImageAlign
        CmdDyna.ImageUrl = CmdModifier_01.ImageUrl
        CmdDyna.ToolTip = CmdModifier_01.ToolTip
        AddHandler CmdDyna.Click, Sub(s, ev)
                                      CmdModifier_Click(s, CType(ev, System.Web.UI.ImageClickEventArgs))
                                  End Sub
        CmdDyna.Visible = True
        Cellule.Controls.Add(CmdDyna)

        CmdDyna = New System.Web.UI.WebControls.ImageButton
        CmdDyna.ID = "CmdSupprimer" & "_" & Strings.Format(Index, "00")
        CmdDyna.CssClass = CmdSupprimer_01.CssClass
        CmdDyna.ImageAlign = CmdSupprimer_01.ImageAlign
        CmdDyna.ImageUrl = CmdSupprimer_01.ImageUrl
        CmdDyna.ToolTip = CmdSupprimer_01.ToolTip
        AddHandler CmdDyna.Click, Sub(s, ev)
                                      CmdSupprimer_Click(s, CType(ev, System.Web.UI.ImageClickEventArgs))
                                  End Sub
        CmdDyna.Visible = True
        Cellule.Controls.Add(CmdDyna)

        Rangee.Controls.Add(Cellule)

        CadreTable.Controls.Add(Rangee)
        CadreTable.Visible = True

        Return CadreTable
    End Function

    Private Sub CmdModifier_Click(sender As Object, e As System.Web.UI.ImageClickEventArgs) Handles CmdModifier_01.Click
        Dim NumLigne As Integer
        NumLigne = CInt(Strings.Right(CType(sender, System.Web.UI.WebControls.ImageButton).ID, 2))

        Dim Evenement As Virtualia.Systeme.Evenements.DonneeChangeEventArgs
        Evenement = New Virtualia.Systeme.Evenements.DonneeChangeEventArgs("MAJ", VElementsConcatenes(NumLigne - 1))
        Saisie_Change(Evenement)
    End Sub

    Private Sub CmdSupprimer_Click(sender As Object, e As System.Web.UI.ImageClickEventArgs) Handles CmdSupprimer_01.Click
        Dim NumLigne As Integer
        NumLigne = CInt(Strings.Right(CType(sender, System.Web.UI.WebControls.ImageButton).ID, 2))

        Dim Evenement As Virtualia.Systeme.Evenements.DonneeChangeEventArgs
        Evenement = New Virtualia.Systeme.Evenements.DonneeChangeEventArgs("SUP", VElementsConcatenes(NumLigne - 1))
        Saisie_Change(Evenement)
    End Sub

    Public WriteOnly Property VText(ByVal CtlCadre As Control, ByVal Prefixe As String, ByVal NoJour As Integer) As String
        Set(ByVal value As String)
            Dim Ctl As Control
            Dim VirControle As System.Web.UI.WebControls.TextBox
            Ctl = V_WebFonction.VirWebControle(CtlCadre, Prefixe, NoJour)
            If Ctl Is Nothing Then
                Exit Property
            End If
            VirControle = CType(Ctl, System.Web.UI.WebControls.TextBox)
            VirControle.Text = value
        End Set
    End Property

    Private WriteOnly Property VBackColor(ByVal CtlCadre As Control, ByVal Prefixe As String, ByVal NoJour As Integer) As System.Drawing.Color
        Set(ByVal value As System.Drawing.Color)
            Dim Ctl As Control
            Dim VirControle As System.Web.UI.WebControls.TextBox
            Ctl = V_WebFonction.VirWebControle(CtlCadre, Prefixe, NoJour)
            If Ctl Is Nothing Then
                Exit Property
            End If
            VirControle = CType(Ctl, System.Web.UI.WebControls.TextBox)
            VirControle.BackColor = value
            If value.GetBrightness > 0.55 Then
                VirControle.ForeColor = Drawing.Color.Black
            Else
                VirControle.ForeColor = Drawing.Color.White
            End If
        End Set
    End Property

    Private WriteOnly Property VBorderColor(ByVal CtlCadre As Control, ByVal Prefixe As String, ByVal NoJour As Integer) As System.Drawing.Color
        Set(ByVal value As System.Drawing.Color)
            Dim Ctl As Control
            Dim VirControle As System.Web.UI.WebControls.TextBox
            Ctl = V_WebFonction.VirWebControle(CtlCadre, Prefixe, NoJour)
            If Ctl Is Nothing Then
                Exit Property
            End If
            VirControle = CType(Ctl, System.Web.UI.WebControls.TextBox)
            VirControle.BorderColor = value
        End Set
    End Property

    Public WriteOnly Property VJourToolTip(ByVal CtlCadre As Control, ByVal Prefixe As String, ByVal NoJour As Integer) As String
        Set(ByVal value As String)
            Dim Ctl As Control
            Dim VirControle As System.Web.UI.WebControls.TextBox
            Ctl = V_WebFonction.VirWebControle(CtlCadre, Prefixe, NoJour)
            If Ctl Is Nothing Then
                Exit Property
            End If
            VirControle = CType(Ctl, System.Web.UI.WebControls.TextBox)
            VirControle.ToolTip = value
        End Set
    End Property

    Public WriteOnly Property VElement(ByVal CtlCadre As Control, ByVal Prefixe As String, Optional ByVal Observation As String = "") As String
        Set(ByVal value As String)
            Dim Ctl As Control
            Dim VirControle As System.Web.UI.WebControls.TextBox
            Ctl = V_WebFonction.VirWebControle(CtlCadre, Prefixe, 0)
            If Ctl Is Nothing Then
                Exit Property
            End If
            VirControle = CType(Ctl, System.Web.UI.WebControls.TextBox)
            VirControle.Text = value
            VirControle.ToolTip = Observation
        End Set
    End Property

    Public ReadOnly Property VElementsConcatenes(ByVal Index As Integer) As String
        Get
            Dim CtlCadre As Control
            Dim Ctl As Control
            Dim Chaine As String = ""
            CtlCadre = V_WebFonction.VirWebControle(Me.CadreGlobal, "CadreIntitule", Index)
            If CtlCadre Is Nothing Then
                Return ""
            End If
            Ctl = V_WebFonction.VirWebControle(CtlCadre, "DonActivite", 0)
            If Ctl IsNot Nothing Then
                Chaine = CType(Ctl, System.Web.UI.WebControls.TextBox).Text
            End If
            Ctl = V_WebFonction.VirWebControle(CtlCadre, "DonProduit", 0)
            If Ctl IsNot Nothing Then
                Chaine &= CType(Ctl, System.Web.UI.WebControls.TextBox).Text
            End If
            Ctl = V_WebFonction.VirWebControle(CtlCadre, "DonPrecision", 0)
            If Ctl IsNot Nothing Then
                Chaine &= CType(Ctl, System.Web.UI.WebControls.TextBox).Text
            End If
            Return Chaine
        End Get
    End Property

    Public WriteOnly Property VCmdModifierVisible(ByVal CtlCadre As Control) As Boolean
        Set(ByVal value As Boolean)
            Dim Ctl As Control
            Ctl = V_WebFonction.VirWebControle(CtlCadre, "CellModifier", 0)
            If Ctl Is Nothing Then
                Exit Property
            End If
            If value = False Then
                CType(Ctl, System.Web.UI.WebControls.TableCell).CssClass = "CellEtiIntitule"
            Else
                CType(Ctl, System.Web.UI.WebControls.TableCell).CssClass = "CellCmd"
            End If
            Ctl = V_WebFonction.VirWebControle(CtlCadre, "CmdModifier", 0)
            If Ctl Is Nothing Then
                Exit Property
            End If
            CType(Ctl, System.Web.UI.WebControls.ImageButton).Visible = value
            Ctl = V_WebFonction.VirWebControle(CtlCadre, "CmdSupprimer", 0)
            If Ctl Is Nothing Then
                Exit Property
            End If
            CType(Ctl, System.Web.UI.WebControls.ImageButton).Visible = value
        End Set
    End Property

End Class