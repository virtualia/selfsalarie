﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="FrmImpressionAsstreintes.aspx.vb" Inherits="Virtualia.Net.FrmImpressionAsstreintes" UICulture="Fr" %>

<%@ Register src="~/Impression/PER_EtatIndividuel_Astreintes.ascx" tagname="PER_ASSTR" tagprefix="Virtualia" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>ETAT MENSUEL DES ASTREINTES PAR AGENT</title>
</head>
<body>
    <form id="FrmAsstreintes" runat="server" style="max-width: 800px">
    <div style="margin-left : 10px">
        <asp:ImageButton ID="CommandePDF" runat="server" ImageUrl="~/Images/General/PDF_on.gif"
                     ToolTip="Editer le formulaire au format PDF" />
    </div>
    <div runat="server" style="width: 780px;margin-left: 20px">
        <asp:Table runat="server" ID="CadreApercu" HorizontalAlign="Center">
            <asp:TableRow>
                <asp:TableCell>
                    <Virtualia:PER_ASSTR ID="PER_ASSTREINTE" runat="server"/>
                </asp:TableCell>
            </asp:TableRow>
        </asp:Table>
        <asp:HiddenField ID="HSelIde" runat="server" Value="0" />
    </div>
    </form>
</body>
</html>