﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="CtlGenerationVues.ascx.vb" Inherits="Virtualia.Net.CtlGenerationVues" %>

<%@ Register src="~/Controles/Commun/VListeGrid.ascx" tagname="VListeGrid" tagprefix="Virtualia" %>

<asp:UpdatePanel ID="UpdatePanelHaut" runat="server">
   <ContentTemplate>
    <asp:Timer ID="TimerExec" runat="server" Interval="1000" Enabled="false">
    </asp:Timer>
    <asp:Panel ID="CadreGeneration" runat="server" BackColor="#5E9598" HorizontalAlign="Center" Width="1000px" Height="400px"
               Style="margin-top: 10px; border-color: #B0E0D7; border-style: solid; border-width: 2px; text-align: center;">
         <asp:Table ID="CadreExecution" runat="server" HorizontalAlign="Center" Height="395px">
            <asp:TableRow>
                <asp:TableCell ColumnSpan="2" VerticalAlign="Top">
                    <asp:Table ID="CadreCmdStd" runat="server" Height="20px" CellPadding="0" 
                            CellSpacing="0" BackImageUrl="~/Images/Icones/Cmd_Std.bmp"
                            BorderWidth="2px" BorderStyle="Outset" BorderColor="#B6C7E2" ForeColor="#D7FAF3"
                            Width="600px" HorizontalAlign="Right" style="margin-top: 6px; margin-right: 20px">
                            <asp:TableRow VerticalAlign="Top">
                                <asp:TableCell VerticalAlign="Top">
                                    <asp:Button ID="CommndeImport" runat="server" Text="Importer" Width="135px" Height="20px"
                                        BackColor="Transparent" BorderColor="#B6C7E2" ForeColor="#D7FAF3"
                                        Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small" Font-Italic="true"
                                        BorderStyle="None" style=" margin-left: 12px; text-align: left;"
                                        Tooltip="Exécute l'import du fichier Csv des acomptes versés" >
                                    </asp:Button>
                                </asp:TableCell>
                                <asp:TableCell VerticalAlign="Top">
                                    <asp:Button ID="CommandeN1" runat="server" Text="Vues PFR" Width="135px" Height="20px"
                                        BackColor="Transparent" BorderColor="#B6C7E2" ForeColor="#D7FAF3"
                                        Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small" Font-Italic="true"
                                        BorderStyle="None" style=" margin-left: 12px; text-align: left;"
                                        Tooltip="Exécute la préparation des éléments constitutifs des primes PFR, PS et F_R " >
                                    </asp:Button>
                                </asp:TableCell>
                                <asp:TableCell VerticalAlign="Top">
                                    <asp:Button ID="CommandeN2" runat="server" Text="Organigramme" Width="135px" Height="20px"
                                        BackColor="Transparent" BorderColor="#B6C7E2" ForeColor="#D7FAF3"
                                        Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small" Font-Italic="true"
                                        BorderStyle="None" style=" margin-left: 12px; text-align: left;"
                                        Tooltip="Exécute la préparation de la vue organigramme" >
                                    </asp:Button>
                                </asp:TableCell>
                                <asp:TableCell VerticalAlign="Top">
                                    <asp:Button ID="CommandeN3" runat="server" Text="Carrières" Width="135px" Height="20px"
                                        BackColor="Transparent" BorderColor="#B6C7E2" ForeColor="#D7FAF3"
                                        Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small" Font-Italic="true"
                                        BorderStyle="None" style=" margin-left: 12px; text-align: left;"
                                        Tooltip="Exécute la préparation de la synthèse des carrières" >
                                    </asp:Button>
                                </asp:TableCell>
                            </asp:TableRow>
                     </asp:Table>
                </asp:TableCell>
            </asp:TableRow>
            <asp:TableRow>
                <asp:TableCell VerticalAlign="Top">
                    <asp:Label ID="EtiHDebut" runat="server" Height="20px" Width="150px" BackColor="Transparent"
                                ForeColor="#491A1A" Text=""
                                style="text-align: center">
                    </asp:Label>
                </asp:TableCell>
                <asp:TableCell VerticalAlign="Top">
                    <asp:Label ID="EtiHFin" runat="server" Height="20px" Width="150px" BackColor="Transparent"
                                ForeColor="#491A1A" Text=""
                                style="text-align: center">
                    </asp:Label>
                </asp:TableCell>
            </asp:TableRow>
            <asp:TableRow>
                <asp:TableCell ColumnSpan="2" VerticalAlign="Top">
                    <asp:Label ID="EtiTraitement" runat="server" Height="20px" Width="800px" BackColor="Transparent"
                                ForeColor="#610D0D" Text=""
                                style="text-align: center">
                    </asp:Label>
                </asp:TableCell>
            </asp:TableRow>
            <asp:TableRow Height="60px">
                <asp:TableCell ColumnSpan="2" VerticalAlign="Top">
                    <asp:Label ID="EtiAttente" runat="server" Height="20px" Width="800px" BackColor="Transparent"
                                ForeColor="White" Text=""
                                style="text-align: center">
                    </asp:Label>
                    <asp:UpdateProgress ID="UpdateAttente" runat="server">
                        <ProgressTemplate>
                            <img alt="" src="../../Images/General/Attente.gif" 
                                    style="vertical-align:top; text-align:center; font-family: 'Trebuchet MS'; height: 12px" />
                                    ... Veuillez patienter ... 
                        </ProgressTemplate>
                    </asp:UpdateProgress>
                </asp:TableCell>
            </asp:TableRow>
            <asp:TableRow>
                <asp:TableCell>
                    <asp:HiddenField ID="HNumPage" runat="server" Value="0" />
                </asp:TableCell>
            </asp:TableRow>
         </asp:Table>
     </asp:Panel>
   </ContentTemplate>
</asp:UpdatePanel>
<asp:Panel ID="CadreUpload" runat="server" BackColor="#3FAFAB" HorizontalAlign="Center" Width="1000px" Height="150px"
        Style="margin-top: 10px; border-color: #B0E0D7; border-style: ridge; border-width: 4px; text-align: center;">
    <asp:Table ID="CadreFichier" runat="server" HorizontalAlign="Center">
        <asp:TableRow>
            <asp:TableCell HorizontalAlign="Center" Height="50px" ColumnSpan="2">
                <asp:Table ID="CadreCmdTele" runat="server" Height="20px" CellPadding="0" 
                            CellSpacing="0" BackImageUrl="~/Images/Icones/Cmd_Std.bmp"
                            BorderWidth="2px" BorderStyle="Outset" BorderColor="#B6C7E2" ForeColor="#D7FAF3"
                            Width="150px" HorizontalAlign="Center" style="margin-top: 6px;">
                    <asp:TableRow>
                        <asp:TableCell VerticalAlign="Top">
                            <asp:Button ID="CmdUpload" runat="server" Text="Télécharger le fichier" Width="145px" Height="20px"
                                BackColor="Transparent" BorderColor="#B6C7E2" ForeColor="#D7FAF3"
                                Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small" Font-Italic="true"
                                BorderStyle="None" style="text-align: left; text-indent:5px" >
                            </asp:Button>
                        </asp:TableCell>
                    </asp:TableRow>
                </asp:Table>
            </asp:TableCell>
        </asp:TableRow>
        <asp:TableRow>
            <asp:TableCell HorizontalAlign="Center">
                <asp:Label ID="EtiRechercheCarte" runat="server" Text="Nouveau fichier d'acomptes"
                            Height="20px" Width="300px" BackColor="#225C59" ForeColor="#D7FAF3"
                            BorderColor="#B6C7E2"  BorderStyle="Ridge" BorderWidth="2px"
                            Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small" Font-Italic="true">
                </asp:Label>
            </asp:TableCell>
            <asp:TableCell  HorizontalAlign="Center">
                <asp:FileUpload ID="VUploadCarte" runat="server" Width="400px" 
                        BorderWidth="2px" BorderStyle="Groove" />
            </asp:TableCell>
        </asp:TableRow>
    </asp:Table>
</asp:Panel>
<asp:Panel ID="CadreVisu" runat="server" BackColor="#3FAFAB" HorizontalAlign="Center" Width="1000px"
        Style="margin-top: 10px; border-color: #B0E0D7; border-style: ridge; border-width: 4px; text-align: center;">
        <Virtualia:VListeGrid ID="ListeGrille" runat="server" CadreWidth="1000px" 
            SiCaseAcocher="False" TexteCaseACocher="" NatureCaseACocher="0"
            SiPagination="True" SiColonneSelect="False" SiCaptionVisible="False"
            SiStyleBulletin="False" SiStyleReleve="True" TaillePage="50" />
</asp:Panel>