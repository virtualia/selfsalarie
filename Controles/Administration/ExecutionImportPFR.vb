﻿Option Explicit On
Option Strict On
Option Compare Text
Imports VI = Virtualia.Systeme.Constantes
Namespace Execution
    Public Class ImportPFR
        Private WsParent As Virtualia.Net.Session.ObjetSession
        Private WsNomFichier As String
        Private WsDateEffetIndice As String
        Private WsListeImport As List(Of Virtualia.Import.Externe.PFR.AcomptePFR) = Nothing
        Private WsListeVuesPS_SU As List(Of Virtualia.TablesObjet.ShemaVUE.VUE_ATTRIBUTAIRE_PFR) = Nothing
        Private WsListeVuesRIFSEEP As List(Of Virtualia.TablesObjet.ShemaVUE.VUE_ATTRIBUTAIRE_RIFSEEP) = Nothing
        '
        Private WsSiEnCours As Boolean = False
        Private WsIndexCourant As Integer = 0
        '
        Public ReadOnly Property SiEnCours As Boolean
            Get
                Return WsSiEnCours
            End Get
        End Property

        Public ReadOnly Property IndexCourant As Integer
            Get
                Return WsIndexCourant
            End Get
        End Property

        Public ReadOnly Property ListeImport As List(Of Virtualia.Import.Externe.PFR.AcomptePFR)
            Get
                Return WsListeImport
            End Get
        End Property

        Public ReadOnly Property Nombre_Enregistrements As Integer
            Get
                If WsListeImport Is Nothing Then
                    Return 0
                End If
                Return WsListeImport.Count
            End Get
        End Property

        Public Function TraiterDossierCourant(ByVal Index As Integer) As String
            WsSiEnCours = True

            Select Case Index
                Case Is > WsListeImport.Count - 1
                    Dim NomSauvegarde As String = System.Configuration.ConfigurationManager.AppSettings("RepertoireImport") & "\Fichiers_PFR\"
                    NomSauvegarde &= Now.Year & "_" & Strings.Format(Now.Month, "00") & "_" & Strings.Format(Now.Day) & ".sav"
                    WsIndexCourant = 0
                    WsSiEnCours = False
                    My.Computer.FileSystem.CopyFile(WsNomFichier, NomSauvegarde, True)
                    Return "Fin du traitement du fichier " & WsNomFichier & " Nombre de lignes traitées : " & Nombre_Enregistrements
            End Select

            WsIndexCourant = Index + 1
            WsSiEnCours = False

            Dim FicheAcompte As Virtualia.Import.Externe.PFR.AcomptePFR
            Dim FicheVUEPS_SU As Virtualia.TablesObjet.ShemaVUE.VUE_ATTRIBUTAIRE_PFR = Nothing
            Dim FicheVUERIFSEEP As Virtualia.TablesObjet.ShemaVUE.VUE_ATTRIBUTAIRE_RIFSEEP = Nothing
            Dim CRetour As Boolean

            FicheAcompte = WsListeImport.Item(Index)
            Select Case FicheAcompte.SiTypePS
                Case True
                    FicheVUEPS_SU = WsListeVuesPS_SU.Find(Function(Recherche) Recherche.Ide_Dossier = FicheAcompte.Matricule And Recherche.Regime_Prime = "PS")
                    If FicheVUEPS_SU Is Nothing Then
                        Return ""
                    End If
                Case False
                    FicheVUERIFSEEP = WsListeVuesRIFSEEP.Find(Function(Recherche) Recherche.Ide_Dossier = FicheAcompte.Matricule)
                    If FicheVUERIFSEEP Is Nothing Then
                        Return ""
                    End If
            End Select
            Select Case FicheAcompte.SiTypePS
                Case True
                    FicheVUEPS_SU.Montant_Indemnite_Sujetion = FicheAcompte.PS_MontantIS
                    FicheVUEPS_SU.Acompte_Indemnite_Sujetion = FicheAcompte.PS_AcompteIS
                    FicheVUEPS_SU.Acompte_Base100 = FicheAcompte.PS_AcomptePrime
                    If FicheVUEPS_SU.FicheLue <> FicheVUEPS_SU.ContenuTable & VI.Tild Then
                        CRetour = WsParent.V_PointeurGlobal.VirServiceServeur.MiseAjour_Fiche(WsParent.V_NomdUtilisateurSgbd, VI.PointdeVue.PVueVueLogique, 7,
                                                                          FicheVUEPS_SU.Ide_Dossier, "M", FicheVUEPS_SU.FicheLue, FicheVUEPS_SU.ContenuTable)
                    End If
                Case False
                    FicheVUERIFSEEP.Acompte_IFSE = FicheAcompte.RIFSEEP_AcompteIFSE
                    FicheVUERIFSEEP.Avance_CIA_Adeduire = FicheAcompte.RIFSEEP_AvanceCIA
                    FicheVUERIFSEEP.Acompte_Garantie = FicheAcompte.RIFSEEP_AcompteGarantie
                    If FicheVUERIFSEEP.FicheLue <> FicheVUERIFSEEP.ContenuTable & VI.Tild Then
                        CRetour = WsParent.V_PointeurGlobal.VirServiceServeur.MiseAjour_Fiche(WsParent.V_NomdUtilisateurSgbd, VI.PointdeVue.PVueVueLogique, 8,
                                                                          FicheVUERIFSEEP.Ide_Dossier, "M", FicheVUERIFSEEP.FicheLue, FicheVUERIFSEEP.ContenuTable)
                    End If
            End Select

            Return WsListeImport.Item(Index).Nom & Strings.Space(1) & WsListeImport.Item(Index).Prenom

        End Function

        Private Sub LireVuesPS_SU()
            Dim Constructeur As Virtualia.Systeme.Sgbd.Sql.SqlInterne
            Dim OrdreSql As String
            Dim LstRes As List(Of Virtualia.Systeme.MetaModele.VIR_FICHE)
            Dim IndiceI As Integer

            Constructeur = New Virtualia.Systeme.Sgbd.Sql.SqlInterne(WsParent.V_PointeurGlobal.VirModele, WsParent.V_PointeurGlobal.VirInstanceBd)
            Constructeur.NombredeRequetes(VI.PointdeVue.PVueVueLogique, "01/01/" & Year(Now), "31/12/" & Year(Now), VI.Operateurs.ET) = 1
            Constructeur.SiPasdeTriSurIdeDossier = False
            Constructeur.SiHistoriquedeSituation = True

            Constructeur.NoInfoSelection(0, 7) = 0
            Constructeur.ValeuraComparer(0, VI.Operateurs.ET, VI.Operateurs.Inclu, False) = "01/01/" & Year(Now) & VI.PointVirgule & "31/12/" & Year(Now)

            For IndiceI = 0 To 36
                Constructeur.InfoExtraite(IndiceI, 7, 0) = CInt(IndiceI)
            Next IndiceI
            OrdreSql = Constructeur.OrdreSqlDynamique
            Constructeur = Nothing

            LstRes = WsParent.V_PointeurGlobal.VirServiceServeur.RequeteSql_ToFiches(WsParent.V_NomdUtilisateurSgbd, VI.PointdeVue.PVueVueLogique, 7, OrdreSql)
            If LstRes Is Nothing Then
                WsListeVuesPS_SU = Nothing
                Exit Sub
            End If
            WsListeVuesPS_SU = WsParent.V_RhFonction.ConvertisseurListeFiches(Of Virtualia.TablesObjet.ShemaVUE.VUE_ATTRIBUTAIRE_PFR)(LstRes)

        End Sub

        Private Sub LireVuesRIFSEEP()
            Dim Constructeur As Virtualia.Systeme.Sgbd.Sql.SqlInterne
            Dim OrdreSql As String
            Dim LstRes As List(Of Virtualia.Systeme.MetaModele.VIR_FICHE)
            Dim IndiceI As Integer

            Constructeur = New Virtualia.Systeme.Sgbd.Sql.SqlInterne(WsParent.V_PointeurGlobal.VirModele, WsParent.V_PointeurGlobal.VirInstanceBd)
            Constructeur.NombredeRequetes(VI.PointdeVue.PVueVueLogique, "01/01/" & Year(Now), "31/12/" & Year(Now), VI.Operateurs.ET) = 1
            Constructeur.SiPasdeTriSurIdeDossier = False
            Constructeur.SiHistoriquedeSituation = True

            Constructeur.NoInfoSelection(0, 8) = 0
            Constructeur.ValeuraComparer(0, VI.Operateurs.ET, VI.Operateurs.Inclu, False) = "01/01/" & Year(Now) & VI.PointVirgule & "31/12/" & Year(Now)

            For IndiceI = 0 To 32
                Constructeur.InfoExtraite(IndiceI, 8, 0) = CInt(IndiceI)
            Next IndiceI
            OrdreSql = Constructeur.OrdreSqlDynamique
            Constructeur = Nothing

            LstRes = WsParent.V_PointeurGlobal.VirServiceServeur.RequeteSql_ToFiches(WsParent.V_NomdUtilisateurSgbd, VI.PointdeVue.PVueVueLogique, 8, OrdreSql)
            If LstRes Is Nothing Then
                WsListeVuesRIFSEEP = Nothing
                Exit Sub
            End If
            WsListeVuesRIFSEEP = WsParent.V_RhFonction.ConvertisseurListeFiches(Of Virtualia.TablesObjet.ShemaVUE.VUE_ATTRIBUTAIRE_RIFSEEP)(LstRes)

        End Sub

        Private Sub LireFichier()
            Dim CodeIso As System.Text.Encoding = System.Text.Encoding.GetEncoding(1252)
            Dim FLuxLecture As System.IO.FileStream
            Dim FicReader As System.IO.StreamReader
            Dim LigneAcompte As Virtualia.Import.Externe.PFR.AcomptePFR
            Dim Champlu As String
            Dim Sep As String = VI.PointVirgule
            If My.Computer.FileSystem.GetFileInfo(WsNomFichier).Extension = ".txt" Then
                Sep = Strings.Chr(9)
            End If
            WsListeImport = New List(Of Virtualia.Import.Externe.PFR.AcomptePFR)

            FLuxLecture = New System.IO.FileStream(WsNomFichier, IO.FileMode.Open, IO.FileAccess.Read)
            FicReader = New System.IO.StreamReader(FLuxLecture, CodeIso)
            Do Until FicReader.EndOfStream
                Champlu = FicReader.ReadLine
                LigneAcompte = New Virtualia.Import.Externe.PFR.AcomptePFR(Champlu, Sep)
                If LigneAcompte.Matricule > 0 Then
                    WsListeImport.Add(LigneAcompte)
                End If
            Loop
            FicReader.Close()
        End Sub

        Public Sub New(ByVal Pointeur As Virtualia.Net.Session.ObjetSession, ByVal NomFichier As String)
            WsParent = Pointeur
            WsNomFichier = NomFichier
            Call LireFichier()
            Call LireVuesPS_SU()
            Call LireVuesRIFSEEP()
        End Sub

    End Class
End Namespace
