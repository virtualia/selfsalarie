﻿Option Strict On
Option Explicit On
Option Compare Text
Public Class VNotificationPFR
    Inherits System.Web.UI.UserControl
    Private WebFct As Virtualia.Net.Controles.WebFonctions

    Public WriteOnly Property Fiche As Virtualia.TablesObjet.ShemaVUE.VUE_ATTRIBUTAIRE_PFR
        Set(value As Virtualia.TablesObjet.ShemaVUE.VUE_ATTRIBUTAIRE_PFR)
            WebFct = New Virtualia.Net.Controles.WebFonctions(Me, 0)

            PFR_LieuDate.Text = "Montreuil, le " & Now.ToShortDateString
            PFR_AnneeEntretien.Text = CStr(value.Date_Valeur_ToDate.Year)
            PFR_Matricule.Text = Strings.Format(value.Ide_Dossier, "000000")
            PFR_Nom.Text = value.Nom
            PFR_Prenom.Text = value.Prenom
            PFR_AffectationNiv1.Text = value.Structure_N1
            PFR_AffectationNiv2.Text = value.Structure_N2
            PFR_Grade.Text = value.Grade
            PFR_Coefficient.Text = CStr(value.Cotation_F)
            Select Case value.Regime_Prime
                Case "PFR"
                    PFR_EtiAnneeEntretien.Text = "Fonctionnaires - Année de référence : "
                    PFR_BasPage1.Visible = True
                Case "F_R"
                    PFR_EtiAnneeEntretien.Text = "Agents du statut unifié - Année de référence : "
                    PFR_BasPage1.Visible = False
            End Select

            PFR_Col_15.Text = Strings.Format(value.Cotation_Totale_F, "### ##0.00")
            PFR_Col_16.Text = Strings.Format(value.Montant_Reference_F, "### ##0.00")
            PFR_Col_31.Text = Strings.Format(value.Pourcentage_Moyen_Proratisation, "### ##0.0000")
            PFR_Col_32.Text = Strings.Format(value.Montant_Total_Base100_Hors_IS_et_CIT, "### ##0.00")
            PFR_Col_23.Text = Strings.Format(value.Coefficient_Modulation_Base100, "### ##0.00")
            PFR_Col_34.Text = Strings.Format(value.Montant_Total_Base100_Hors_IS_et_CIT + value.Montant_Modulation_Apres_Prorata, "### ##0.00")

            PFR_Col_18.Text = Strings.Format(value.Coefficient_R_Base100, "### ##0.00")
            PFR_Col_19.Text = Strings.Format(value.Montant_Reference_R, "### ##0.00")
            PFR_L3_C4.Text = ""
            PFR_L3_C5.Text = ""
            PFR_Col_33.Text = Strings.Format(value.Montant_Modulation_Apres_Prorata, "### ##0.00")
            PFR_L3_C7.Text = ""

            PFR_L4_C2.Text = ""
            PFR_L4_C3.Text = ""
            PFR_L4_C5.Text = ""
            PFR_Col_24Ter.Text = Strings.Format(value.Coefficient__CIT_R, "### ##0.00")
            PFR_Col_33Bis.Text = Strings.Format(value.Montant_CIT, "### ##0.00")

            PFR_L5_C1.Text = "Indemnisation<br/>des sujétions<br/>période du<br/>01/11/"
            PFR_L5_C1.Text &= CStr(value.Date_Valeur_ToDate.Year - 1)
            PFR_L5_C1.Text &= " au<br/>31/10/" & CStr(value.Date_Valeur_ToDate.Year)
            PFR_Col_17Ter.Text = Strings.Format(value.Surcote_Indemnite_Sujetion, "### ##0.00")
            PFR_L5_C3.Text = ""
            PFR_L5_C4.Text = ""
            PFR_L5_C5.Text = ""
            PFR_L5_C6.Text = ""
            PFR_Col_17Bis.Text = Strings.Format(value.Montant_Indemnite_Sujetion, "### ##0.00")

            PFR_Col_24a.Text = Strings.Format(value.Coefficient_PFR_Complement, "### ##0.00")
            PFR_L6_C3.Text = ""
            PFR_L6_C4.Text = ""
            PFR_L6_C5.Text = ""
            PFR_L6_C6.Text = ""
            PFR_Col_33a.Text = Strings.Format(value.Montant_Modulation_Exception_Apres_Prorata, "### ##0.00")

            PFR_L7_C2.Text = ""
            PFR_L7_C3.Text = ""
            PFR_L7_C4.Text = ""
            PFR_L7_C5.Text = ""
            PFR_L7_C6.Text = ""
            PFR_Col_34Bis.Text = Strings.Format(value.Montant_Du_Avec_Surcote_IS, "### ##0.00")

        End Set
    End Property

    Private Sub Page_Init(sender As Object, e As EventArgs) Handles Me.Init
        Dim Tableaudata(0) As String
        Tableaudata = Strings.Split(Request.Url.AbsoluteUri, "/Fenetres/", -1)
        PFR_SignatureFAM.BackImageUrl = Tableaudata(0) & "/Images/Specifique/SignatureFAM.jpg"
    End Sub

End Class