﻿Option Strict On
Option Explicit On
Option Compare Text
Imports VI = Virtualia.Systeme.Constantes
Public Class CtlSaisieFaitGenerateur
    Inherits System.Web.UI.UserControl
    Public Delegate Sub Retour_MsgEventHandler(ByVal sender As Object, ByVal e As Virtualia.Systeme.Evenements.DonneeChangeEventArgs)
    Public Event ValeurRetour As Retour_MsgEventHandler
    Private WebFct As Virtualia.Net.Controles.WebFonctions

    Protected Overridable Sub ReponseRetour(ByVal e As Virtualia.Systeme.Evenements.DonneeChangeEventArgs)
        RaiseEvent ValeurRetour(Me, e)
    End Sub

    Private ReadOnly Property V_WebFonction As Virtualia.Net.Controles.WebFonctions
        Get
            If WebFct Is Nothing Then
                WebFct = New Virtualia.Net.Controles.WebFonctions(Me, 0)
            End If
            Return WebFct
        End Get
    End Property

    Private Sub CtlSaisieFaitGenerateur_PreRender(sender As Object, e As EventArgs) Handles Me.PreRender
        If ListeGenerateur.Items.Count > 0 Then
            Exit Sub
        End If
        Dim UtiSession As Virtualia.Net.Session.SessionModule = CType(V_WebFonction.PointeurUtilisateur.V_PointeurSessionModule, Virtualia.Net.Session.SessionModule)
        If UtiSession Is Nothing Then
            Exit Sub
        End If
        ListeGenerateur.Items.Add("Aucun")
        If UtiSession.SiAccesGRH = False And UtiSession.SiAccesManager = False Then
            ListeGenerateur.Items.Add("Nécessité de service")
            ListeGenerateur.Items.Add("Tenue audience")
            Exit Sub
        End If
        Dim LstTable As List(Of ServiceServeur.VirRequeteType)
        LstTable = V_WebFonction.PointeurGlobal.LireTableGeneraleSimple("Fait générateur HS")
        If LstTable Is Nothing Then
            Exit Sub
        End If
        For Each Element In LstTable
            ListeGenerateur.Items.Add(Element.Valeurs(0))
        Next
    End Sub

    Private Sub CommandeOK_Click(sender As Object, e As EventArgs) Handles CommandeOK.Click
        Dim Evenement As Virtualia.Systeme.Evenements.DonneeChangeEventArgs
        Dim SiOk As Boolean

        Evenement = New Virtualia.Systeme.Evenements.DonneeChangeEventArgs("")
        If ListeGenerateur.SelectedItem Is Nothing Then
            ReponseRetour(Evenement)
            Exit Sub
        End If
        Dim UtiSession As Virtualia.Net.Session.SessionModule = CType(V_WebFonction.PointeurUtilisateur.V_PointeurSessionModule, Virtualia.Net.Session.SessionModule)
        If UtiSession Is Nothing Then
            ReponseRetour(Evenement)
            Exit Sub
        End If
        Dim FicheDetail As Virtualia.TablesObjet.ShemaPER.PER_DETAIL_HS = UtiSession.DossierPER.VDossier_NewHeuresSup.FicheDetail_HS(EtiDateValeur.ToolTip)
        If FicheDetail Is Nothing Then
            ReponseRetour(Evenement)
            Exit Sub
        End If
        If FicheDetail.FicheLue = "" Then
            FicheDetail.ContenuTable = FicheDetail.Ide_Dossier & VI.Tild & FicheDetail.ContenuTable
        End If
        If ListeGenerateur.SelectedItem.Text = "Aucun" Then
            FicheDetail.FaitGenerateur = ""
        Else
            FicheDetail.FaitGenerateur = ListeGenerateur.SelectedItem.Text
        End If
        FicheDetail.Date_Validation = V_WebFonction.ViRhDates.DateduJour
        FicheDetail.Signataire = V_WebFonction.PointeurUtilisateur.V_NomdeConnexion
        FicheDetail.ModuleOrigine = "SelfV4"
        If UtiSession.SiAccesGRH = True Then
            FicheDetail.ModuleOrigine = "DRHV4"
        ElseIf UtiSession.SiAccesManager = True Then
            If Session.Item("Manager") IsNot Nothing Then
                FicheDetail.ModuleOrigine = "ManagerV4"
            End If
        End If
        SiOk = V_WebFonction.PointeurGlobal.VirServiceServeur.MiseAjour_Fiche(V_WebFonction.PointeurGlobal.VirNomUtilisateur, VI.PointdeVue.PVueApplicatif,
                       VI.ObjetPer.ObaDetailHS, FicheDetail.Ide_Dossier, "M", FicheDetail.FicheLue, FicheDetail.ContenuTable, True)
        If SiOk = True Then
            UtiSession.DossierPER.VDossier_NewHeuresSup.MettreAjourSituationMensuelle(FicheDetail.Date_Valeur_ToDate.Year, FicheDetail.Date_Valeur_ToDate.Month)
        End If
        ReponseRetour(Evenement)
    End Sub

    Private Sub CmdCancel_Click(sender As Object, e As EventArgs) Handles CmdCancel.Click
        Dim Evenement As Virtualia.Systeme.Evenements.DonneeChangeEventArgs
        Evenement = New Virtualia.Systeme.Evenements.DonneeChangeEventArgs("")
        ReponseRetour(Evenement)
    End Sub

    Public WriteOnly Property AfficherDialogue() As Virtualia.Systeme.Evenements.DonneeChangeEventArgs
        Set(ByVal value As Virtualia.Systeme.Evenements.DonneeChangeEventArgs)
            EtiDateValeur.Text = "Journée du " & CDate(value.Valeur).ToLongDateString
            EtiDateValeur.ToolTip = value.Valeur
            Dim UtiSession As Virtualia.Net.Session.SessionModule = CType(V_WebFonction.PointeurUtilisateur.V_PointeurSessionModule, Virtualia.Net.Session.SessionModule)
            If UtiSession Is Nothing Then
                Exit Property
            End If
            Dim FicheDetail As Virtualia.TablesObjet.ShemaPER.PER_DETAIL_HS = UtiSession.DossierPER.VDossier_NewHeuresSup.FicheDetail_HS(value.Valeur)
            If FicheDetail Is Nothing Then
                Exit Property
            End If

            EtiVerification.Text = "Heures supplémentaires de " & FicheDetail.Heure_Borne & " à " & FicheDetail.Heure_Fin
            If FicheDetail.Nbheures_Jusqua20h > 0 Then
                EtiVerification.Text &= "<BR>" & FicheDetail.Nbheures_Jusqua20h & " heures comptablisées jusqu'à 20 heures,"
                EtiVerification.Text &= " soit " & FicheDetail.V_NbHeures_Jusqua20h
            End If
            If FicheDetail.Nbheures_De20ha22h > 0 Then
                EtiVerification.Text &= "<BR>" & FicheDetail.Nbheures_De20ha22h & " heures comptablisées de 20 heures à 22 heures,"
                EtiVerification.Text &= " soit " & FicheDetail.V_NbHeures_de20ha22h
            End If
            If FicheDetail.Nbheures_Nuit > 0 Then
                EtiVerification.Text &= "<BR>" & FicheDetail.Nbheures_Nuit & " heures comptablisées de nuit,"
                EtiVerification.Text &= " soit " & FicheDetail.V_NbHeures_deNuit
            End If
            If FicheDetail.Nbheures_Trajet > 0 Then
                EtiVerification.Text &= "<BR>" & FicheDetail.Nbheures_Trajet & " heures comptablisées de trajet,"
                EtiVerification.Text &= " soit " & FicheDetail.V_NbHeures_Trajet
            End If
            EtiVerification.Text &= "<BR>"
            EtiVerification.Text &= "<BR>" & "En validant le fait générateur vous confirmez la validité des heures supplémentaires comptabilisées."
        End Set
    End Property

End Class