﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="CtlEditionActivite.ascx.vb" Inherits="Virtualia.Net.CtlEditionActivite" %>

<%@ Register src="~/Controles/Commun/VListeCombo.ascx" tagname="VListeCombo" tagprefix="Virtualia" %>
<%@ Register src="~/Controles/Saisies/VTrioHorizontalRadio.ascx" tagname="VOption" tagprefix="Virtualia" %>

<asp:Panel ID="PanelGlobal" runat="server" HorizontalAlign="Center" Width="950px" Style="margin-top: 10px; text-align: center;">
    <asp:Table ID="CadreHaut" runat="server" HorizontalAlign="Center" Width="900px" Height="150px" BackColor="#5E9598"
         BorderStyle="Solid" BorderWidth="2px" BorderColor="#B0E0D7">
        <asp:TableRow>
            <asp:TableCell ID="CellRetour" HorizontalAlign="Left" VerticalAlign="Top">
                <asp:ImageButton ID="CommandeRetour" runat="server" Width="32px" Height="32px" 
                    BorderStyle="None" ImageUrl="~/Images/Boutons/FlecheRetourContexte.jpg" 
                    ImageAlign="Middle" style="margin-left: 1px;">
                </asp:ImageButton>
            </asp:TableCell>
        </asp:TableRow>
        <asp:TableRow>
            <asp:TableCell HorizontalAlign="Center" VerticalAlign="Top">
                    <asp:Label ID="Etiquette" runat="server" Text="Edition de l'état mensuel des activités" Height="20px" Width="350px"
                        BackColor="#216B68" BorderColor="#B0E0D7"  BorderStyle="Groove"
                        BorderWidth="2px" ForeColor="#D7FAF3"
                        Font-Bold="True" Font-Names="Trebuchet MS" Font-Size="Small"
                        style="margin-top: 5px; margin-left: 4px; margin-bottom: 4px;
                        font-style: oblique; text-indent: 5px; text-align: center;">
                    </asp:Label>          
            </asp:TableCell>      
        </asp:TableRow>
        <asp:TableRow>
            <asp:TableCell VerticalAlign="Top">
                <asp:Table ID="CadreCmdStd" runat="server" Height="20px" CellPadding="0" 
                        CellSpacing="0" BackImageUrl="~/Images/Icones/Cmd_Std.bmp"
                        BorderWidth="2px" BorderStyle="Outset" BorderColor="#B6C7E2" ForeColor="#D7FAF3"
                        Width="450px" HorizontalAlign="Right" style="margin-top: 6px; margin-right: 20px">
                        <asp:TableRow VerticalAlign="Top">
                            <asp:TableCell VerticalAlign="Top">
                                <asp:Button ID="CommandeCsv" runat="server" Text="Fichier CSV" Width="135px" Height="20px"
                                    BackColor="Transparent" BorderColor="#B6C7E2" ForeColor="#D7FAF3"
                                    Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small" Font-Italic="true"
                                    BorderStyle="None" style=" margin-left: 12px; text-align: left;"
                                    Tooltip="Crée un fichier au format Csv (Séparateur Point-virgule)">
                                </asp:Button>
                            </asp:TableCell>
                            <asp:TableCell VerticalAlign="Top">
                                <asp:Button ID="CommandeTxt" runat="server" Text="Fichier TXT" Width="135px" Height="20px"
                                    BackColor="Transparent" BorderColor="#B6C7E2" ForeColor="#D7FAF3"
                                    Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small" Font-Italic="true"
                                    BorderStyle="None" style=" margin-left: 12px; text-align: left;"
                                    Tooltip="Crée un fichier au format Txt (Séparateur Tabulation)" >
                                </asp:Button>
                            </asp:TableCell>
                            <asp:TableCell VerticalAlign="Top">
                                <asp:Button ID="CommandeExec" runat="server" Text="Exécuter" Width="135px" Height="20px"
                                    BackColor="Transparent" BorderColor="#B6C7E2" ForeColor="#D7FAF3"
                                    Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small" Font-Italic="true"
                                    BorderStyle="None" style=" margin-left: 12px; text-align: left;"
                                    Tooltip="Exécute l'édition de l'état de sortie des activités cumulées du mois sélectionné" >
                                </asp:Button>
                            </asp:TableCell>
                        </asp:TableRow>
                    </asp:Table>
            </asp:TableCell>
        </asp:TableRow>
    </asp:Table>
</asp:Panel>
<asp:UpdatePanel ID="UpdatePanelBas" runat="server">
    <Triggers>
        <asp:AsyncPostBackTrigger ControlID="HorlogeExec" EventName="Tick" />
    </Triggers>
    <ContentTemplate>
        <asp:Timer ID="HorlogeExec" runat="server" Interval="1000" Enabled="false"></asp:Timer>
        <asp:Table ID="CadreBas" runat="server" HorizontalAlign="Center" Width="900px" Height="300px" BackColor="#5E9598"
            BorderStyle="Solid" BorderWidth="2px" BorderColor="#B0E0D7">
            <asp:TableRow>
                <asp:TableCell ColumnSpan="2" HorizontalAlign="Left" VerticalAlign="Top" Height="30px" Width="700px">
                    <Virtualia:VOption ID="OptionVisa" runat="server" RadioDroiteVisible="false"
                         RadioGaucheText="Validées" RadioCentreText="Toutes" RadioGaucheCheck="true" />
                </asp:TableCell>
            </asp:TableRow>
            <asp:TableRow>
                <asp:TableCell HorizontalAlign="Left" VerticalAlign="Top" Height="30px">
                    <Virtualia:VListeCombo ID="VComboMois" runat="server" EtiVisible="true" V_NomTable="Mois" 
                            LstHeight="22px" LstWidth="200px" LstBorderColor="#7EC8BE" LstBackColor="White" LstForeColor="#142425"
                            LstStyle="border-color: #7EC8BE; border-width: 2px; border-style: inset;display :table-cell; font-style: oblique;" 
                            EtiWidth="120px" EtiText="Liste des mois" EtiBackColor="#CAEBE4" EtiForeColor="#124545" />
                </asp:TableCell>
                <asp:TableCell HorizontalAlign="Left" VerticalAlign="Top" Height="30px">
                    <Virtualia:VListeCombo ID="VComboStructureN1" runat="server" EtiVisible="true" V_NomTable="DR" 
                            LstHeight="22px" LstWidth="300px" LstBorderColor="#7EC8BE" LstBackColor="White" LstForeColor="#142425"
                            LstStyle="border-color: #7EC8BE; border-width: 2px; border-style: inset;display :table-cell; font-style: oblique;" 
                            EtiWidth="200px" EtiText="Liste des affectations" EtiBackColor="#CAEBE4" EtiForeColor="#124545" />
                </asp:TableCell> 
            </asp:TableRow>
            <asp:TableRow>
                <asp:TableCell VerticalAlign="Top" Height="30px">
                    <asp:Label ID="EtiHDebut" runat="server" Height="20px" Width="200px" BackColor="Transparent"
                        Font-Italic="False" Font-Bold="True" Font-Names="Trebuchet MS" Font-Size="Small"
                        ForeColor="#491A1A" Text="" style="text-align: center">
                    </asp:Label>
                </asp:TableCell>
                <asp:TableCell VerticalAlign="Top" Height="30px">
                    <asp:Label ID="EtiHFin" runat="server" Height="20px" Width="200px" BackColor="Transparent"
                         Font-Italic="False" Font-Bold="True" Font-Names="Trebuchet MS" Font-Size="Small"
                         ForeColor="#491A1A" Text="" style="text-align: center">
                    </asp:Label>
                </asp:TableCell>
            </asp:TableRow>
            <asp:TableRow>
                <asp:TableCell VerticalAlign="Top" Height="30px" ColumnSpan="2">
                    <asp:Label ID="EtiTraitement" runat="server" Height="20px" Width="800px" BackColor="Transparent"
                                Font-Italic="False" Font-Bold="True" Font-Names="Trebuchet MS" Font-Size="Small"
                                ForeColor="White" Text="" style="text-align: center">
                    </asp:Label>
                </asp:TableCell>
            </asp:TableRow>
            <asp:TableRow>
                <asp:TableCell HorizontalAlign="Center" VerticalAlign="Top" Height="70px" ColumnSpan="2">
                    <asp:UpdateProgress ID="UpdateAttente" runat="server">
                        <ProgressTemplate>
                            <asp:Table ID="CadreAttente" runat="server" BackColor="#6C9690">
                                <asp:TableRow>
                                    <asp:TableCell>
                                        <asp:Image ID="ImgAttente" runat="server" Height="30px" Width="30px" ImageUrl="~/Images/General/Attente.gif" />
                                    </asp:TableCell>
                                    <asp:TableCell Width="10px" Height="10px" />
                                    <asp:TableCell>
                                        <asp:Label ID="EtiAttente" runat="server" Height="20px" Width="400px" Text="Traitement en cours, veuillez patienter ..." 
                                            Font-Italic="False" Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                            BackColor="Transparent" ForeColor="White" BorderStyle="None" style="text-align: center" />
                                    </asp:TableCell>
                                </asp:TableRow>
                            </asp:Table>
                        </ProgressTemplate>
                    </asp:UpdateProgress>
                </asp:TableCell>
            </asp:TableRow>
            <asp:TableRow>
                <asp:TableCell VerticalAlign="Top">
                    <asp:HiddenField ID="HNumPage" runat="server" Value="0" />
                </asp:TableCell>
            </asp:TableRow>
        </asp:Table>
    </ContentTemplate>
</asp:UpdatePanel>
