﻿Option Strict On
Option Explicit On
Option Compare Text
Imports VI = Virtualia.Systeme.Constantes
Public Class FrmTachesAdmin
    Inherits System.Web.UI.Page
    Private WebFct As Virtualia.Net.Controles.WebFonctions
    Private WsNomStateUrl As String = "Administrer"

    Private Property V_UrlRetour As String
        Get
            Dim VCache As String

            VCache = CStr(Me.ViewState(WsNomStateUrl))
            If VCache Is Nothing Then
                Return ""
            End If
            Return VCache
        End Get
        Set(ByVal value As String)
            If Me.ViewState(WsNomStateUrl) IsNot Nothing Then
                Me.ViewState.Remove(WsNomStateUrl)
            End If
            Me.ViewState.Add(WsNomStateUrl, value)
        End Set
    End Property

    Private Sub CommandeRetour_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles CommandeRetour.Click
        Response.Redirect(V_UrlRetour & "?IDVirtualia=" & Server.HtmlEncode(Session.SessionID))
    End Sub

    Private Sub Page_Init(sender As Object, e As EventArgs) Handles Me.Init
        Dim Msg As String = ""
        Dim NumSession As String = Server.HtmlDecode(Request.QueryString("IDVirtualia"))
        If NumSession <> Session.SessionID Then
            Msg = "Erreur lors de l'identification."
            Response.Redirect("~/Fenetres/Commun/ErreurApplication.aspx?Msg=" & Msg)
            Exit Sub
        End If
        WebFct = New Virtualia.Net.Controles.WebFonctions(Me, 1)
        If WebFct.PointeurUtilisateur Is Nothing Then
            Msg = "Erreur de serveur. Vous avez été déconnecté(e)."
            Response.Redirect("~/Fenetres/Commun/ErreurApplication.aspx?Msg=" & Msg)
            Exit Sub
        End If
        If WebFct.VerifierCookie(Me, Session.SessionID) = False Then
            Msg = "Erreur lors de l'identification du dossier accédé. Session invalide."
            Response.Redirect("~/Fenetres/Commun/ErreurApplication.aspx?Msg=" & Msg)
            Exit Sub
        End If
        V_UrlRetour = Server.HtmlDecode(Request.QueryString("UrlRetour"))
    End Sub
End Class