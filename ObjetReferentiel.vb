﻿Option Explicit On
Option Strict On
Option Compare Text
Imports VI = Virtualia.Systeme.Constantes
Namespace WebAppli
    Public Class ObjetReferentiel
        Private WsParent As Virtualia.Net.Session.ObjetGlobal
        Private WsObjetEtablissement As Virtualia.Ressources.Datas.ObjetEtablissement = Nothing
        Private WsObjetAbsence As Virtualia.Ressources.Datas.ObjetAbsence = Nothing
        Private WsObjetPresence As Virtualia.Ressources.Datas.ObjetPresence = Nothing
        Private WsObjetCycleTravail As Virtualia.Ressources.Datas.ObjetCycleTravail = Nothing
        Private WsObjetUniteCycle As Virtualia.Ressources.Datas.ObjetUniteCycle = Nothing
        Private WsListeRubriques As List(Of Virtualia.TablesObjet.ShemaREF.OUT_RUBRIQUES)
        Private WsCouleursPlanning As Virtualia.Systeme.Planning.CouleursJour
        Private WsListeTableDecisions As List(Of Virtualia.TablesObjet.ShemaREF.OUT_DECISION)
        Private WsListeTableActivites As List(Of Virtualia.TablesObjet.ShemaREF.ONIC_MESURE) = Nothing
        '
        Private WsListeAccordRTT As List(Of Virtualia.TablesObjet.ShemaREF.ACCORD_RTT)
        '
        Private WsListeIndice100 As List(Of Virtualia.TablesObjet.ShemaREF.PAI_INDICE100)
        Private WsTauxResidence As Double
        Private WsIndicePlancherResidence As Integer = 0

        Public ReadOnly Property InstanceCouleur As Virtualia.Systeme.Planning.CouleursJour
            Get
                If WsCouleursPlanning IsNot Nothing AndAlso WsCouleursPlanning.Count > 0 Then
                    Return WsCouleursPlanning
                End If
                Dim FicheRef As Virtualia.TablesObjet.ShemaREF.PAI_COULEURS
                Dim LstRef As List(Of Virtualia.TablesObjet.ShemaREF.PAI_COULEURS) = Nothing
                Dim ToutUnObjet As Virtualia.Ressources.Datas.EnsembleFiches
                ToutUnObjet = New Virtualia.Ressources.Datas.EnsembleFiches(WsParent.VirNomUtilisateur, VI.PointdeVue.PVuePaie)
                ToutUnObjet.NumeroObjet(False) = 24
                If ToutUnObjet.ListedesFiches IsNot Nothing Then
                    LstRef = New List(Of Virtualia.TablesObjet.ShemaREF.PAI_COULEURS)
                    For IndiceI = 0 To ToutUnObjet.ListedesFiches.Count - 1
                        FicheRef = CType(ToutUnObjet.ListedesFiches.Item(IndiceI), Virtualia.TablesObjet.ShemaREF.PAI_COULEURS)
                        LstRef.Add(FicheRef)
                    Next IndiceI
                End If
                WsCouleursPlanning = New Virtualia.Systeme.Planning.CouleursJour
                If LstRef Is Nothing Then
                    Call WsCouleursPlanning.Initialiser_Defaut()
                ElseIf LstRef.Count = 0 Then
                    Call WsCouleursPlanning.Initialiser_Defaut()
                Else
                    Dim HexaCouleur As String
                    Dim ColStocke As String
                    Dim IndiceI As Integer
                    Dim LstValeurs As List(Of String)

                    For IndiceI = 0 To LstRef.Count - 1
                        HexaCouleur = Hex(LstRef.Item(IndiceI).Couleur)
                        Select Case HexaCouleur.Length
                            Case Is = 6
                                ColStocke = "#" & Strings.Right(HexaCouleur, 2) & Strings.Mid(HexaCouleur, 3, 2) & Strings.Left(HexaCouleur, 2)
                            Case Is = 4
                                ColStocke = "#" & Strings.Right(HexaCouleur, 2) & Strings.Left(HexaCouleur, 2) & "00"
                            Case Else
                                ColStocke = "#" & HexaCouleur & "0000"
                        End Select
                        LstValeurs = New List(Of String)
                        If LstRef.Item(IndiceI).Valeur_N1 <> "" Then
                            LstValeurs.Add(LstRef.Item(IndiceI).Valeur_N1)
                        End If
                        If LstRef.Item(IndiceI).Valeur_N2 <> "" Then
                            LstValeurs.Add(LstRef.Item(IndiceI).Valeur_N2)
                        End If
                        If LstRef.Item(IndiceI).Valeur_N3 <> "" Then
                            LstValeurs.Add(LstRef.Item(IndiceI).Valeur_N3)
                        End If
                        If LstRef.Item(IndiceI).Valeur_N4 <> "" Then
                            LstValeurs.Add(LstRef.Item(IndiceI).Valeur_N4)
                        End If
                        If LstRef.Item(IndiceI).Valeur_N5 <> "" Then
                            LstValeurs.Add(LstRef.Item(IndiceI).Valeur_N5)
                        End If
                        If LstRef.Item(IndiceI).Valeur_N6 <> "" Then
                            LstValeurs.Add(LstRef.Item(IndiceI).Valeur_N6)
                        End If
                        If LstRef.Item(IndiceI).Valeur_N7 <> "" Then
                            LstValeurs.Add(LstRef.Item(IndiceI).Valeur_N7)
                        End If
                        WsCouleursPlanning.AjouterItem(LstRef.Item(IndiceI).Etiquette, ColStocke, LstValeurs)
                    Next IndiceI
                End If
                Return WsCouleursPlanning
            End Get
        End Property

        Public ReadOnly Property FicheEtablissement(ByVal IntituleAdm As String) As Virtualia.TablesObjet.ShemaREF.ETA_IDENTITE
            Get
                If WsObjetEtablissement Is Nothing Then
                    WsObjetEtablissement = New Virtualia.Ressources.Datas.ObjetEtablissement(WsParent.VirNomUtilisateur)
                End If
                Return WsObjetEtablissement.Fiche_Etablissement(IntituleAdm)
            End Get
        End Property

        Public ReadOnly Property RessourceAbsence As Virtualia.Ressources.Datas.ObjetAbsence
            Get
                If WsObjetAbsence Is Nothing Then
                    WsObjetAbsence = New Virtualia.Ressources.Datas.ObjetAbsence(WsParent.VirNomUtilisateur)
                End If
                Return WsObjetAbsence
            End Get
        End Property

        Public ReadOnly Property RessourcePresence As Virtualia.Ressources.Datas.ObjetPresence
            Get
                If WsObjetPresence Is Nothing Then
                    WsObjetPresence = New Virtualia.Ressources.Datas.ObjetPresence(WsParent.VirNomUtilisateur)
                End If
                Return WsObjetPresence
            End Get
        End Property

        Public ReadOnly Property RessourceCycles As Virtualia.Ressources.Datas.ObjetCycleTravail
            Get
                If WsObjetCycleTravail Is Nothing Then
                    WsObjetCycleTravail = New Virtualia.Ressources.Datas.ObjetCycleTravail(WsParent.VirNomUtilisateur, 0)
                End If
                Return WsObjetCycleTravail
            End Get
        End Property

        Public ReadOnly Property RessourceUnitesCycle As Virtualia.Ressources.Datas.ObjetUniteCycle
            Get
                If WsObjetUniteCycle Is Nothing Then
                    WsObjetUniteCycle = New Virtualia.Ressources.Datas.ObjetUniteCycle(WsParent.VirNomUtilisateur, 0)
                End If
                Return WsObjetUniteCycle
            End Get
        End Property

        Public ReadOnly Property ListeOutDecision(ByVal Ide As Integer) As List(Of Virtualia.TablesObjet.ShemaREF.OUT_DECISION)
            Get
                If WsListeTableDecisions Is Nothing Then
                    Call LireTableDecision()
                End If
                Return (From Decision In WsListeTableDecisions Where Decision.Ide_Dossier = Ide Order By Decision.Rang Ascending).ToList
            End Get
        End Property

        Public ReadOnly Property OutRubrique(ByVal Code_Rubrique As String) As Virtualia.TablesObjet.ShemaREF.OUT_RUBRIQUES
            Get
                If WsListeRubriques Is Nothing Then
                    Call LireRubriques()
                End If
                If Strings.Right(Code_Rubrique, 3) = "-AA" Or Strings.Right(Code_Rubrique, 3) = "-AC" Then
                    Code_Rubrique = Strings.Left(Code_Rubrique, Code_Rubrique.Length - 3)
                End If
                If WsListeRubriques Is Nothing Then
                    Return Nothing
                End If
                Return WsListeRubriques.Find(Function(Recherche) Recherche.CodeRubrique = Code_Rubrique)
            End Get
        End Property

        Public Function CyclePrevisionnel(ByVal ArgumentDate As String, ByVal NomCycleTravail As String, ByVal DateValeurCycle As String) As Virtualia.Net.Controles.ItemJourCalendrier
            Dim CycleTravail As Virtualia.TablesObjet.ShemaREF.TRA_IDENTIFICATION
            Dim CycleBase As Virtualia.TablesObjet.ShemaREF.CYC_IDENTIFICATION
            Dim UniteCycle As Virtualia.TablesObjet.ShemaREF.CYC_UNITE
            Dim QuantDebutCycle As Integer
            Dim QuantJour As Integer
            Dim IndiceA As Integer
            Dim NbTotalRotation As Integer
            Dim IRotation As Integer
            Dim IRotationRelative As Integer
            Dim IndexCycleBase As Integer
            Dim NumObjetUnite As Integer
            Dim FicheCal As Virtualia.Net.Controles.ItemJourCalendrier = Nothing
            Dim IndicateurJournee As Boolean

            CycleTravail = RessourceCycles.Fiche_Cycle(NomCycleTravail)
            If CycleTravail Is Nothing Then
                Return Nothing
            End If
            NbTotalRotation = RessourceCycles.DureeRotation(CycleTravail, 8)
            QuantDebutCycle = WsParent.VirRhDates.CalcQuantieme(DateValeurCycle)
            QuantJour = WsParent.VirRhDates.CalcQuantieme(ArgumentDate)
            IRotation = 1
            For IndiceA = 1 To (QuantJour - QuantDebutCycle)
                IRotation += 1
                If IRotation > NbTotalRotation Then
                    IRotation = 1
                End If
            Next IndiceA
            IndexCycleBase = RessourceCycles.IndexCycledebase(CycleTravail, IRotation)
            CycleBase = RessourceCycles.CycledeBase(CycleTravail, IndexCycleBase)
            If CycleBase Is Nothing Then
                Return Nothing
            End If
            IRotationRelative = RessourceCycles.DureeRotation(CycleTravail, IndexCycleBase - 1)
            NumObjetUnite = (IRotation - IRotationRelative) + 1
            UniteCycle = CycleBase.Fiche_Cycle(NumObjetUnite)
            If UniteCycle Is Nothing Then
                Return Nothing
            End If
            IndicateurJournee = False
            If UniteCycle.Denomination_Plage1 <> UniteCycle.Denomination_Plage2 Then
                If UniteCycle.Denomination_Plage1 = "Journée" Or UniteCycle.Denomination_Plage2 = "Journée" Then
                    IndicateurJournee = True
                End If
            End If
            If RessourceAbsence.Fiche_Absence(UniteCycle.Denomination_Plage1) IsNot Nothing Then
                If RessourceAbsence.Fiche_Absence(UniteCycle.Denomination_Plage2) IsNot Nothing Then
                    FicheCal = New Virtualia.Net.Controles.ItemJourCalendrier(ArgumentDate, UniteCycle.Denomination_Plage1, "CYCLE", VI.NumeroPlage.Jour_Absence, VI.ObjetPer.ObaPresence)
                Else
                    Select Case IndicateurJournee
                        Case True
                            FicheCal = New Virtualia.Net.Controles.ItemJourCalendrier(ArgumentDate, "", "CYCLE", VI.NumeroPlage.Plage1_Absence, VI.ObjetPer.ObaPresence)
                        Case False
                            FicheCal = New Virtualia.Net.Controles.ItemJourCalendrier(ArgumentDate, UniteCycle.Denomination_Plage1, "CYCLE", VI.NumeroPlage.Plage1_Absence, VI.ObjetPer.ObaPresence)
                    End Select
                End If
            ElseIf RessourceAbsence.Fiche_Absence(UniteCycle.Denomination_Plage2) IsNot Nothing Then
                Select Case IndicateurJournee
                    Case True
                        FicheCal = New Virtualia.Net.Controles.ItemJourCalendrier(ArgumentDate, "", "CYCLE", VI.NumeroPlage.Plage2_Absence, VI.ObjetPer.ObaPresence)
                    Case False
                        FicheCal = New Virtualia.Net.Controles.ItemJourCalendrier(ArgumentDate, UniteCycle.Denomination_Plage2, "CYCLE", VI.NumeroPlage.Plage2_Absence, VI.ObjetPer.ObaPresence)
                End Select
            End If
            Return FicheCal
        End Function

        Public Function HeureFinCycle(ByVal ArgumentDate As String, ByVal NomCycleTravail As String, ByVal DateValeurCycle As String) As System.DateTime
            Dim CycleTravail As Virtualia.TablesObjet.ShemaREF.TRA_IDENTIFICATION
            Dim CycleBase As Virtualia.TablesObjet.ShemaREF.CYC_IDENTIFICATION
            Dim UniteCycle As Virtualia.TablesObjet.ShemaREF.CYC_UNITE
            Dim QuantDebutCycle As Integer
            Dim QuantJour As Integer
            Dim IndiceA As Integer
            Dim NbTotalRotation As Integer
            Dim IRotation As Integer
            Dim IRotationRelative As Integer
            Dim IndexCycleBase As Integer
            Dim NumObjetUnite As Integer

            CycleTravail = RessourceCycles.Fiche_Cycle(NomCycleTravail)
            If CycleTravail Is Nothing Then
                Return Nothing
            End If
            NbTotalRotation = RessourceCycles.DureeRotation(CycleTravail, 8)
            QuantDebutCycle = WsParent.VirRhDates.CalcQuantieme(DateValeurCycle)
            QuantJour = WsParent.VirRhDates.CalcQuantieme(ArgumentDate)
            IRotation = 1
            For IndiceA = 1 To (QuantJour - QuantDebutCycle)
                IRotation += 1
                If IRotation > NbTotalRotation Then
                    IRotation = 1
                End If
            Next IndiceA
            IndexCycleBase = RessourceCycles.IndexCycledebase(CycleTravail, IRotation)
            CycleBase = RessourceCycles.CycledeBase(CycleTravail, IndexCycleBase)
            If CycleBase Is Nothing Then
                Return Nothing
            End If
            IRotationRelative = RessourceCycles.DureeRotation(CycleTravail, IndexCycleBase - 1)
            NumObjetUnite = (IRotation - IRotationRelative) + 1
            UniteCycle = CycleBase.Fiche_Cycle(NumObjetUnite)
            If UniteCycle Is Nothing Then
                Return WsParent.VirRhDates.HeureTypee(ArgumentDate, "00:00")
            End If
            If UniteCycle.Denomination_Plage2 <> "" AndAlso UniteCycle.Heure_Fin_Plage2 <> "" Then
                Return WsParent.VirRhDates.HeureTypee(ArgumentDate, UniteCycle.Heure_Fin_Plage2)
            End If
            If UniteCycle.Denomination_Plage1 <> "" AndAlso UniteCycle.Heure_Fin_Plage1 <> "" Then
                Return WsParent.VirRhDates.HeureTypee(ArgumentDate, UniteCycle.Heure_Fin_Plage1)
            End If
            Return WsParent.VirRhDates.HeureTypee(ArgumentDate, "00:00")
        End Function

        Public ReadOnly Property ListeActivites() As List(Of String)
            Get
                If WsListeTableActivites Is Nothing Then
                    Dim ToutUnObjet As Virtualia.Ressources.Datas.EnsembleFiches
                    ToutUnObjet = New Virtualia.Ressources.Datas.EnsembleFiches(WsParent.VirNomUtilisateur, VI.PointdeVue.PVueActivite)
                    ToutUnObjet.NumeroObjet(False) = 2
                    If ToutUnObjet.ListedesFiches Is Nothing Then
                        Return Nothing
                    End If
                    WsListeTableActivites = New List(Of Virtualia.TablesObjet.ShemaREF.ONIC_MESURE)
                    For Each Fiche In ToutUnObjet.ListedesFiches
                        WsListeTableActivites.Add(CType(Fiche, Virtualia.TablesObjet.ShemaREF.ONIC_MESURE))
                    Next
                End If
                Return (From instance In WsListeTableActivites Order By instance.Activite Select instance.Activite).Distinct.ToList
            End Get
        End Property

        Public ReadOnly Property ListeActivites(ByVal SelActivite As String) As List(Of KeyValuePair(Of String, String))
            Get
                If SelActivite = "" Then
                    Return Nothing
                End If
                If WsListeTableActivites Is Nothing Then
                    Dim ToutUnObjet As Virtualia.Ressources.Datas.EnsembleFiches
                    ToutUnObjet = New Virtualia.Ressources.Datas.EnsembleFiches(WsParent.VirNomUtilisateur, VI.PointdeVue.PVueActivite)
                    ToutUnObjet.NumeroObjet(False) = 2
                    If ToutUnObjet.ListedesFiches Is Nothing Then
                        Return Nothing
                    End If
                    WsListeTableActivites = New List(Of Virtualia.TablesObjet.ShemaREF.ONIC_MESURE)
                    For Each Fiche In ToutUnObjet.ListedesFiches
                        WsListeTableActivites.Add(CType(Fiche, Virtualia.TablesObjet.ShemaREF.ONIC_MESURE))
                    Next
                End If
                Dim ListeOK As List(Of KeyValuePair(Of String, String)) = New List(Of KeyValuePair(Of String, String))
                For Each Tache In WsListeTableActivites
                    If Tache.Activite = SelActivite Then
                        ListeOK.Add(New KeyValuePair(Of String, String)(Tache.Mesure, Tache.Descriptif))
                    End If
                Next
                Return (From Paire In ListeOK Order By Paire.Key).ToList
            End Get
        End Property

        Public ReadOnly Property FicheAccordRTT(ByVal Pvue As Integer, ByVal Ide As Integer, ByVal ArgumentDate As Date) As Virtualia.TablesObjet.ShemaREF.ACCORD_RTT
            Get
                If ListeAccordRTT Is Nothing Then
                    Return Nothing
                End If
                Dim SousListe As List(Of Virtualia.TablesObjet.ShemaREF.ACCORD_RTT)
                Try
                    SousListe = (From FicheREF In WsListeAccordRTT Where FicheREF.PointdeVue = Pvue And FicheREF.Ide_Dossier = Ide Order By FicheREF.Date_Valeur_ToDate Descending).ToList
                Catch
                    Return Nothing
                End Try
                If SousListe Is Nothing OrElse SousListe.Count = 0 Then
                    Return Nothing
                End If
                For Each FicheREF In SousListe
                    If FicheREF.Date_Valeur_ToDate <= ArgumentDate Then
                        Return FicheREF
                    End If
                Next
                Return SousListe.Item(0)
            End Get
        End Property

        Public ReadOnly Property ListeAccordRTT As List(Of Virtualia.TablesObjet.ShemaREF.ACCORD_RTT)
            Get
                If WsListeAccordRTT IsNot Nothing AndAlso WsListeAccordRTT.Count > 0 Then
                    Return WsListeAccordRTT
                End If
                Dim Constructeur As Virtualia.Systeme.Sgbd.Sql.SqlInterne
                Dim OrdreSql As String
                Dim LstRes As List(Of Virtualia.Systeme.MetaModele.VIR_FICHE)
                Dim IndiceI As Integer
                Dim FicheAccord As Virtualia.TablesObjet.ShemaREF.ACCORD_RTT
                WsListeAccordRTT = New List(Of Virtualia.TablesObjet.ShemaREF.ACCORD_RTT)

                '**Etablissement
                Constructeur = New Virtualia.Systeme.Sgbd.Sql.SqlInterne(WsParent.VirModele, WsParent.VirInstanceBd)
                Constructeur.NombredeRequetes(VI.PointdeVue.PVueEtablissement, "", "31/12/" & Year(Now), VI.Operateurs.ET) = 1
                Constructeur.SiPasdeTriSurIdeDossier = False
                Constructeur.SiHistoriquedeSituation = True

                Constructeur.NoInfoSelection(0, 3) = 19
                For IndiceI = 0 To 22
                    Constructeur.InfoExtraite(IndiceI, 3, 0) = CInt(IndiceI)
                Next IndiceI
                OrdreSql = Constructeur.OrdreSqlDynamique
                Constructeur = Nothing
                LstRes = WsParent.VirServiceServeur.RequeteSql_ToFiches(WsParent.VirNomUtilisateur, VI.PointdeVue.PVueEtablissement, 3, OrdreSql)
                If LstRes IsNot Nothing AndAlso LstRes.Count > 0 Then
                    For Each Fiche In LstRes
                        FicheAccord = New Virtualia.TablesObjet.ShemaREF.ACCORD_RTT(VI.PointdeVue.PVueEtablissement, 3)
                        FicheAccord.ContenuTable = Fiche.Ide_Dossier & VI.Tild & Fiche.ContenuTable
                        WsListeAccordRTT.Add(FicheAccord)
                    Next
                End If
                '**Cycles de travail
                Constructeur = New Virtualia.Systeme.Sgbd.Sql.SqlInterne(WsParent.VirModele, WsParent.VirInstanceBd)
                Constructeur.NombredeRequetes(VI.PointdeVue.PVueCycle, "", "31/12/" & Year(Now), VI.Operateurs.ET) = 1
                Constructeur.SiPasdeTriSurIdeDossier = False
                Constructeur.SiHistoriquedeSituation = True

                Constructeur.NoInfoSelection(0, 3) = 19
                For IndiceI = 0 To 22
                    Constructeur.InfoExtraite(IndiceI, 3, 0) = CInt(IndiceI)
                Next IndiceI
                OrdreSql = Constructeur.OrdreSqlDynamique
                Constructeur = Nothing
                LstRes = WsParent.VirServiceServeur.RequeteSql_ToFiches(WsParent.VirNomUtilisateur, VI.PointdeVue.PVueCycle, 3, OrdreSql)
                If LstRes IsNot Nothing AndAlso LstRes.Count > 0 Then
                    For Each Fiche In LstRes
                        FicheAccord = New Virtualia.TablesObjet.ShemaREF.ACCORD_RTT(VI.PointdeVue.PVueCycle, 3)
                        FicheAccord.ContenuTable = Fiche.Ide_Dossier & VI.Tild & Fiche.ContenuTable
                        WsListeAccordRTT.Add(FicheAccord)
                    Next
                End If
                '**Presences
                Constructeur = New Virtualia.Systeme.Sgbd.Sql.SqlInterne(WsParent.VirModele, WsParent.VirInstanceBd)
                Constructeur.NombredeRequetes(VI.PointdeVue.PVuePresences, "", "31/12/" & Year(Now), VI.Operateurs.ET) = 1
                Constructeur.SiPasdeTriSurIdeDossier = False
                Constructeur.SiHistoriquedeSituation = True

                Constructeur.NoInfoSelection(0, 2) = 19
                For IndiceI = 0 To 2
                    Constructeur.InfoExtraite(IndiceI, 2, 0) = CInt(IndiceI)
                Next IndiceI
                OrdreSql = Constructeur.OrdreSqlDynamique
                Constructeur = Nothing
                LstRes = WsParent.VirServiceServeur.RequeteSql_ToFiches(WsParent.VirNomUtilisateur, VI.PointdeVue.PVuePresences, 2, OrdreSql)
                If LstRes IsNot Nothing AndAlso LstRes.Count > 0 Then
                    For Each Fiche In LstRes
                        FicheAccord = New Virtualia.TablesObjet.ShemaREF.ACCORD_RTT(VI.PointdeVue.PVuePresences, 2)
                        FicheAccord.ContenuTable = Fiche.Ide_Dossier & VI.Tild & Fiche.ContenuTable
                        WsListeAccordRTT.Add(FicheAccord)
                    Next
                End If
                Return WsListeAccordRTT
            End Get
        End Property

        Public ReadOnly Property IndicePlancherResidence As Integer
            Get
                Return WsIndicePlancherResidence
            End Get
        End Property

        Public ReadOnly Property TauxZoneResidence As Double
            Get
                Return WsTauxResidence
            End Get
        End Property

        Public ReadOnly Property ValeurAnnuelleI100(ByVal ArgumentDate As String) As Double
            Get
                If WsListeIndice100 Is Nothing Then
                    Dim ToutUnObjet As Virtualia.Ressources.Datas.EnsembleFiches
                    Dim ZoneResidence As Integer = 1

                    ToutUnObjet = New Virtualia.Ressources.Datas.EnsembleFiches(WsParent.VirNomUtilisateur, VI.PointdeVue.PVuePaie)
                    ToutUnObjet.NumeroObjet(False) = 4 'Zone de résidence
                    If ToutUnObjet.ListedesFiches IsNot Nothing Then
                        If ToutUnObjet.ListedesFiches.Count > 0 Then
                            ZoneResidence = CType(ToutUnObjet.ListedesFiches.Item(0), Virtualia.TablesObjet.ShemaREF.PAI_ZONE_RESIDENCE).ZonedeResidence
                        End If
                    End If

                    ToutUnObjet = New Virtualia.Ressources.Datas.EnsembleFiches(WsParent.VirNomUtilisateur, VI.PointdeVue.PVuePaie)
                    ToutUnObjet.NumeroObjet(True) = 6 'Indice 100
                    If ToutUnObjet.ListedesFiches IsNot Nothing Then
                        WsListeIndice100 = New List(Of Virtualia.TablesObjet.ShemaREF.PAI_INDICE100)
                        For Each Element In ToutUnObjet.ListedesFiches
                            WsListeIndice100.Add(CType(Element, Virtualia.TablesObjet.ShemaREF.PAI_INDICE100))
                        Next
                    End If

                    ToutUnObjet = New Virtualia.Ressources.Datas.EnsembleFiches(WsParent.VirNomUtilisateur, VI.PointdeVue.PVuePaie)
                    ToutUnObjet.NumeroObjet(False) = 9 'Plancher résidence
                    If ToutUnObjet.ListedesFiches IsNot Nothing Then
                        If ToutUnObjet.ListedesFiches.Count > 0 Then
                            WsIndicePlancherResidence = CType(ToutUnObjet.ListedesFiches.Item(0), Virtualia.TablesObjet.ShemaREF.PAI_RESIDENCE).IndicePlancher
                            Select Case ZoneResidence
                                Case 1
                                    WsTauxResidence = CType(ToutUnObjet.ListedesFiches.Item(0), Virtualia.TablesObjet.ShemaREF.PAI_RESIDENCE).Taux_Zone1 / 100
                                Case 2
                                    WsTauxResidence = CType(ToutUnObjet.ListedesFiches.Item(0), Virtualia.TablesObjet.ShemaREF.PAI_RESIDENCE).Taux_Zone2 / 100
                                Case 3
                                    WsTauxResidence = CType(ToutUnObjet.ListedesFiches.Item(0), Virtualia.TablesObjet.ShemaREF.PAI_RESIDENCE).Taux_Zone3 / 100
                            End Select
                        End If
                    End If
                End If
                Dim IndiceI As Integer
                For Each Element In WsListeIndice100
                    Select Case WsParent.VirRhDates.ComparerDates(Element.Date_de_Valeur, ArgumentDate)
                        Case VI.ComparaisonDates.Egalite, VI.ComparaisonDates.PlusPetit
                            Return WsListeIndice100.Item(IndiceI).Montant
                    End Select
                Next
                Return 0
            End Get
        End Property

        Private Sub LireRubriques()
            Dim ToutUnObjet As Virtualia.Ressources.Datas.EnsembleFiches
            Dim IndiceI As Integer
            Dim FicheRub As Virtualia.TablesObjet.ShemaREF.OUT_RUBRIQUES

            ToutUnObjet = New Virtualia.Ressources.Datas.EnsembleFiches(WsParent.VirNomUtilisateur, VI.PointdeVue.PVueInterface)
            ToutUnObjet.NumeroObjet(False) = 2
            If ToutUnObjet.ListedesFiches Is Nothing Then
                Exit Sub
            End If

            WsListeRubriques = New List(Of Virtualia.TablesObjet.ShemaREF.OUT_RUBRIQUES)

            For IndiceI = 0 To ToutUnObjet.ListedesFiches.Count - 1
                FicheRub = CType(ToutUnObjet.ListedesFiches.Item(IndiceI), Virtualia.TablesObjet.ShemaREF.OUT_RUBRIQUES)
                WsListeRubriques.Add(FicheRub)
            Next IndiceI
        End Sub

        Private Sub LireTableDecision()
            Dim ToutUnObjet As Virtualia.Ressources.Datas.EnsembleFiches
            Dim IndiceI As Integer
            '*******************************************************************************************
            '90 - Honda
            '********************************************************************************************
            ToutUnObjet = New Virtualia.Ressources.Datas.EnsembleFiches(WsParent.VirNomUtilisateur, VI.PointdeVue.PVueInterface)
            ToutUnObjet.NumeroObjet(False) = 4
            If ToutUnObjet.ListedesFiches Is Nothing Then
                Exit Sub
            End If

            WsListeTableDecisions = New List(Of Virtualia.TablesObjet.ShemaREF.OUT_DECISION)

            For IndiceI = 0 To ToutUnObjet.ListedesFiches.Count - 1
                WsListeTableDecisions.Add(CType(ToutUnObjet.ListedesFiches.Item(IndiceI), Virtualia.TablesObjet.ShemaREF.OUT_DECISION))
            Next IndiceI

        End Sub

        Public Sub Initialiser()
            WsListeTableActivites = Nothing
        End Sub

        Public Sub New(ByVal Host As Virtualia.Net.Session.ObjetGlobal)
            WsParent = Host
        End Sub
    End Class
End Namespace