﻿<%@ Page Language="vb" MasterPageFile="~/PagesMaitres/VirtualiaMain.Master" AutoEventWireup="false" CodeBehind="FrmTachesAdmin.aspx.vb" 
    Inherits="Virtualia.Net.FrmTachesAdmin" UICulture="fr" %>

<%@ MasterType VirtualPath="~/PagesMaitres/VirtualiaMain.Master" %>
<%@ Register src="~/Controles/Administration/CtlGenerationVues.ascx" tagname="VGenereVue" tagprefix="Virtualia" %>

<asp:Content ID="CadreAdmin" runat="server" ContentPlaceHolderID="Principal">
    <asp:Table ID="CadreVues" runat="server" Width="1050px" HorizontalAlign="Center" BackColor="#2D8781" CellSpacing="2" style="margin-top: 5px ">
        <asp:TableRow>
           <asp:TableCell ID="CadreCmd" HorizontalAlign="Left" runat="server">
                <asp:Panel ID="PanelCommande" runat="server" style="height:25px;width:200px" >
                    <asp:Table ID="CadreCmdStd" runat="server" Height="20px" CellPadding="0" 
                        CellSpacing="0" BackImageUrl="~/Images/Icones/Cmd_Std.bmp"
                        BorderWidth="2px" BorderStyle="Outset" BorderColor="#B6C7E2" ForeColor="#D7FAF3"
                        Width="150px" HorizontalAlign="Center" style="margin-top: 3px;">
                       <asp:TableRow VerticalAlign="Top">
                        <asp:TableCell>
                            <asp:Button ID="CommandeRetour" runat="server" Text="Retour" Width="135px" Height="20px"
                                BackColor="Transparent" BorderColor="#B6C7E2" ForeColor="#D7FAF3"
                                Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small" Font-Italic="true"
                                BorderStyle="None" style=" margin-left: 15px; text-align: left;"
                                Tooltip="" >
                            </asp:Button>
                        </asp:TableCell>
                       </asp:TableRow>
                    </asp:Table>
                </asp:Panel>
            </asp:TableCell>
        </asp:TableRow>
        <asp:TableRow>
            <asp:TableCell ID="ConteneurVues" Width="1050px" VerticalAlign="Top" HorizontalAlign="Center">
                <asp:MultiView ID="MultiVues" runat="server" ActiveViewIndex="0">
                    <asp:View ID="VueGeneration" runat="server">
                        <Virtualia:VGenereVue ID="Preparation" runat="server" V_Perimetre="100" ></Virtualia:VGenereVue>
                    </asp:View>
                </asp:MultiView>
            </asp:TableCell>
        </asp:TableRow>
    </asp:Table>
</asp:Content>

