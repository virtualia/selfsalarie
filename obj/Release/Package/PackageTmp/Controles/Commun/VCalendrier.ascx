﻿<%@ Control Language="VB" AutoEventWireup="false" Inherits="Virtualia.Net.Controles_VCalendrier" Codebehind="VCalendrier.ascx.vb" %>

<%@ Register src="~/Controles/Commun/VListeCombo.ascx" tagname="VListeCombo" tagprefix="Virtualia" %>
<%@ Register src="~/Controles/Saisies/VCocheSimple.ascx" tagname="VCocheSimple" tagprefix="Virtualia" %>

<asp:Table ID="VCalendrier" runat="server"  Height="322px" Width="268px" CellPadding="0" CellSpacing="0" 
                        BorderStyle="Ridge" BorderColor="#B0E0D7" BorderWidth="4px" >
    <asp:TableRow>
        <asp:TableCell>
            <asp:Label ID="EtiTitre" runat="server" Height="20px" Width="252px" BackColor="#8DA8A3"
                 ForeColor="#E9FDF9" BorderStyle="None" Text="Calendrier Virtualia"
                 Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="Smaller" Font-Italic="false"
                 style="margin-top: 3px; margin-left: 4px; text-indent:1px; text-align: center; vertical-align: top" />
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell Height="30px">
            <Virtualia:VListeCombo ID="CalLstMois" runat="server" EtiVisible="true" EtiText="Mois" EtiWidth="65px"
                 V_NomTable="Mois" LstWidth="178px" LstBorderColor="#B0E0D7" LstBackColor="#EEECFD"
                 EtiStyle="text-align:center" EtiBackColor="#8DA8A3" EtiForeColor="#E9FDF9" />
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell><div style="height: 3px; width: 175px"></div></asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell>
            <asp:Table ID="CalLibelles" runat="server" Height="20px" Width="252px" CellPadding="0" CellSpacing="0" 
                        BorderStyle="None" HorizontalAlign="Center">
                <asp:TableRow>
                    <asp:TableCell>
                        <asp:Label ID="EtiLundi" runat="server" Height="18px" Width="35px" BackColor="#8DA8A3" ForeColor="#E9FDF9"
                                BorderStyle="Solid" BorderColor="#216B68" BorderWidth="1px" Text="L"
                                Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="Smaller" Font-Italic="false"
                                style="margin-top: 1px; text-indent: 1px; text-align: center; border-right-style: none" />
                    </asp:TableCell>
                    <asp:TableCell>
                        <asp:Label ID="EtiMardi" runat="server" Height="18px" Width="35px" BackColor="#8DA8A3" ForeColor="#E9FDF9"
                                BorderStyle="Solid" BorderColor="#216B68" BorderWidth="1px" Text="M"
                                Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="Smaller" Font-Italic="false"
                                style="margin-top: 1px; text-indent: 1px; text-align: center; border-right-style: none" />
                    </asp:TableCell>
                    <asp:TableCell>
                        <asp:Label ID="EtiMercredi" runat="server" Height="18px" Width="35px" BackColor="#8DA8A3" ForeColor="#E9FDF9"
                                BorderStyle="Solid" BorderColor="#216B68" BorderWidth="1px" Text="M"
                                Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="Smaller" Font-Italic="false"
                                style="margin-top: 1px; text-indent: 1px; text-align: center; border-right-style: none" />
                    </asp:TableCell>
                    <asp:TableCell>
                        <asp:Label ID="EtiJeudi" runat="server" Height="18px" Width="35px" BackColor="#8DA8A3" ForeColor="#E9FDF9"
                                BorderStyle="Solid" BorderColor="#216B68" BorderWidth="1px" Text="J"
                                Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="Smaller" Font-Italic="false"
                                style="margin-top: 1px; text-indent: 1px; text-align: center; border-right-style: none" />
                    </asp:TableCell>
                    <asp:TableCell>
                        <asp:Label ID="EtiVendredi" runat="server" Height="18px" Width="35px" BackColor="#8DA8A3" ForeColor="#E9FDF9"
                                BorderStyle="Solid" BorderColor="#216B68" BorderWidth="1px" Text="V"
                                Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="Smaller" Font-Italic="false"
                                style="margin-top: 1px; text-indent: 1px; text-align: center; border-right-style: none" />
                    </asp:TableCell>
                    <asp:TableCell>
                        <asp:Label ID="EtiSamedi" runat="server" Height="18px" Width="35px" BackColor="#8DA8A3" ForeColor="#E9FDF9"
                                BorderStyle="Solid" BorderColor="#216B68" BorderWidth="1px" Text="S"
                                Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="Smaller" Font-Italic="false"
                                style="margin-top: 1px; text-indent: 1px; text-align: center; border-right-style: none" />
                    </asp:TableCell>
                    <asp:TableCell>
                        <asp:Label ID="EtiDimanche" runat="server" Height="18px" Width="35px" BackColor="#8DA8A3" ForeColor="#E9FDF9"
                                BorderStyle="Solid" BorderColor="#216B68" BorderWidth="1px" Text="D"
                                Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="Smaller" Font-Italic="false"
                                style="margin-top: 1px; text-indent: 1px; text-align: center" />
                    </asp:TableCell>
                </asp:TableRow>
            </asp:Table>
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell><div style="height: 3px; width: 175px"></div></asp:TableCell>
   </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell>
            <asp:Table ID="CalSemaine00" runat="server" Height="36px" Width="252px" CellPadding="0" CellSpacing="0" 
                        BorderStyle="None" HorizontalAlign="Center">
                  <asp:TableRow>
                       <asp:TableCell>
                            <asp:Button ID="CalAM00" runat="server" Height="17px" Width="35px" BackColor="LightGray" ForeColor="#064F4D"
                                BorderStyle="Solid" BorderColor="#216B68" BorderWidth="1px"
                                Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="X-Small" Font-Italic="false"
                                style="margin-top: 1px; text-indent:-3px; text-align: left; vertical-align: top; border-bottom-style: none" />
                       </asp:TableCell>
                       <asp:TableCell>
                            <asp:Button ID="CalAM01" runat="server" Height="17px" Width="35px" BackColor="LightGray" ForeColor="#064F4D"
                                BorderStyle="Solid" BorderColor="#216B68" BorderWidth="1px"
                                Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="X-Small" Font-Italic="false"
                                style="margin-top: 1px; text-indent:-3px; text-align: left; vertical-align: top; border-bottom-style: none" />
                       </asp:TableCell>
                       <asp:TableCell>
                            <asp:Button ID="CalAM02" runat="server" Height="17px" Width="35px" BackColor="LightGray" ForeColor="#064F4D"
                                BorderStyle="Solid" BorderColor="#216B68" BorderWidth="1px"
                                Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="X-Small" Font-Italic="false"
                                style="margin-top: 1px; text-indent:-3px; text-align: left; vertical-align: top; border-bottom-style: none" />
                       </asp:TableCell>
                       <asp:TableCell>
                            <asp:Button ID="CalAM03" runat="server" Height="17px" Width="35px" BackColor="LightGray" ForeColor="#064F4D"
                                BorderStyle="Solid" BorderColor="#216B68" BorderWidth="1px"
                                Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="X-Small" Font-Italic="false"
                                style="margin-top: 1px; text-indent:-3px; text-align: left; vertical-align: top; border-bottom-style: none" />
                       </asp:TableCell>
                        <asp:TableCell>
                            <asp:Button ID="CalAM04" runat="server" Height="17px" Width="35px" BackColor="LightGray" ForeColor="#064F4D"
                                BorderStyle="Solid" BorderColor="#216B68" BorderWidth="1px"
                                Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="X-Small" Font-Italic="false"
                                style="margin-top: 1px; text-indent:-3px; text-align: left; vertical-align: top; border-bottom-style: none" />
                       </asp:TableCell>
                        <asp:TableCell>
                            <asp:Button ID="CalAM05" runat="server" Height="17px" Width="35px" BackColor="LightGray" ForeColor="#064F4D"
                                BorderStyle="Solid" BorderColor="#216B68" BorderWidth="1px"
                                Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="X-Small" Font-Italic="false"
                                style="margin-top: 1px; text-indent:-3px; text-align: left; vertical-align: top; border-bottom-style: none" />
                       </asp:TableCell>
                        <asp:TableCell>
                            <asp:Button ID="CalAM06" runat="server" Height="17px" Width="35px" BackColor="LightGray" ForeColor="#064F4D"
                                BorderStyle="Solid" BorderColor="#216B68" BorderWidth="1px"
                                Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="X-Small" Font-Italic="false"
                                style="margin-top: 1px; text-indent:-3px; text-align: left; vertical-align: top; border-bottom-style: none" />
                       </asp:TableCell>
                  </asp:TableRow>
                  <asp:TableRow>
                       <asp:TableCell>
                            <asp:Button ID="CalPM00" runat="server" Height="17px" Width="35px" BackColor="LightGray" ForeColor="#064F4D"
                                BorderStyle="Solid" BorderColor="#216B68" BorderWidth="1px"
                                Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="X-Small" Font-Italic="false"
                                style="margin-bottom: 1px; text-indent:-3px; text-align: left; vertical-align: top; border-top-style: none" />
                       </asp:TableCell>
                       <asp:TableCell>
                            <asp:Button ID="CalPM01" runat="server" Height="17px" Width="35px" BackColor="LightGray" ForeColor="#064F4D"
                                BorderStyle="Solid" BorderColor="#216B68" BorderWidth="1px"
                                Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="X-Small" Font-Italic="false"
                                style="margin-bottom: 1px; text-indent:-3px; text-align: left; vertical-align: top; border-top-style: none" />
                       </asp:TableCell>
                        <asp:TableCell>
                            <asp:Button ID="CalPM02" runat="server" Height="17px" Width="35px" BackColor="LightGray" ForeColor="#064F4D"
                                BorderStyle="Solid" BorderColor="#216B68" BorderWidth="1px"
                                Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="X-Small" Font-Italic="false"
                                style="margin-bottom: 1px; text-indent:-3px; text-align: left; vertical-align: top; border-top-style: none" />
                       </asp:TableCell>
                        <asp:TableCell>
                            <asp:Button ID="CalPM03" runat="server" Height="17px" Width="35px" BackColor="LightGray" ForeColor="#064F4D"
                                BorderStyle="Solid" BorderColor="#216B68" BorderWidth="1px"
                                Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="X-Small" Font-Italic="false"
                                style="margin-bottom: 1px; text-indent:-3px; text-align: left; vertical-align: top; border-top-style: none" />
                       </asp:TableCell>
                       <asp:TableCell>
                            <asp:Button ID="CalPM04" runat="server" Height="17px" Width="35px" BackColor="LightGray" ForeColor="#064F4D"
                                BorderStyle="Solid" BorderColor="#216B68" BorderWidth="1px"
                                Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="X-Small" Font-Italic="false"
                                style="margin-bottom: 1px; text-indent:-3px; text-align: left; vertical-align: top; border-top-style: none" />
                       </asp:TableCell>
                       <asp:TableCell>
                            <asp:Button ID="CalPM05" runat="server" Height="17px" Width="35px" BackColor="LightGray" ForeColor="#064F4D"
                                BorderStyle="Solid" BorderColor="#216B68" BorderWidth="1px"
                                Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="X-Small" Font-Italic="false"
                                style="margin-bottom: 1px; text-indent:-3px; text-align: left; vertical-align: top; border-top-style: none" />
                       </asp:TableCell>
                       <asp:TableCell>
                            <asp:Button ID="CalPM06" runat="server" Height="17px" Width="35px" BackColor="LightGray" ForeColor="#064F4D"
                                BorderStyle="Solid" BorderColor="#216B68" BorderWidth="1px"
                                Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="X-Small" Font-Italic="false"
                                style="margin-bottom: 1px; text-indent:-3px; text-align: left; vertical-align: top; border-top-style: none" />
                       </asp:TableCell>
                  </asp:TableRow>
            </asp:Table>
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell>
            <asp:Table ID="CalSemaine01" runat="server" Height="36px" Width="252px" CellPadding="0" CellSpacing="0" 
                        BorderStyle="None" HorizontalAlign="Center">
                  <asp:TableRow>
                       <asp:TableCell>
                            <asp:Button ID="CalAM07" runat="server" Height="17px" Width="35px" BackColor="LightGray" ForeColor="#064F4D"
                                BorderStyle="Solid" BorderColor="#216B68" BorderWidth="1px"
                                Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="X-Small" Font-Italic="false"
                                style="margin-top: 1px; text-indent:-3px; text-align: left; vertical-align: top; border-bottom-style: none" />
                       </asp:TableCell>
                       <asp:TableCell>
                            <asp:Button ID="CalAM08" runat="server" Height="17px" Width="35px" BackColor="LightGray" ForeColor="#064F4D"
                                BorderStyle="Solid" BorderColor="#216B68" BorderWidth="1px"
                                Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="X-Small" Font-Italic="false"
                                style="margin-top: 1px; text-indent:-3px; text-align: left; vertical-align: top; border-bottom-style: none" />
                       </asp:TableCell>
                       <asp:TableCell>
                            <asp:Button ID="CalAM09" runat="server" Height="17px" Width="35px" BackColor="LightGray" ForeColor="#064F4D"
                                BorderStyle="Solid" BorderColor="#216B68" BorderWidth="1px"
                                Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="X-Small" Font-Italic="false"
                                style="margin-top: 1px; text-indent:-3px; text-align: left; vertical-align: top; border-bottom-style: none" />
                       </asp:TableCell>
                       <asp:TableCell>
                            <asp:Button ID="CalAM10" runat="server" Height="17px" Width="35px" BackColor="LightGray" ForeColor="#064F4D"
                                BorderStyle="Solid" BorderColor="#216B68" BorderWidth="1px"
                                Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="X-Small" Font-Italic="false"
                                style="margin-top: 1px; text-indent:-3px; text-align: left; vertical-align: top; border-bottom-style: none" />
                       </asp:TableCell>
                        <asp:TableCell>
                            <asp:Button ID="CalAM11" runat="server" Height="17px" Width="35px" BackColor="LightGray" ForeColor="#064F4D"
                                BorderStyle="Solid" BorderColor="#216B68" BorderWidth="1px"
                                Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="X-Small" Font-Italic="false"
                                style="margin-top: 1px; text-indent:-3px; text-align: left; vertical-align: top; border-bottom-style: none" />
                       </asp:TableCell>
                        <asp:TableCell>
                            <asp:Button ID="CalAM12" runat="server" Height="17px" Width="35px" BackColor="LightGray" ForeColor="#064F4D"
                                BorderStyle="Solid" BorderColor="#216B68" BorderWidth="1px"
                                Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="X-Small" Font-Italic="false"
                                style="margin-top: 1px; text-indent:-3px; text-align: left; vertical-align: top; border-bottom-style: none" />
                       </asp:TableCell>
                        <asp:TableCell>
                            <asp:Button ID="CalAM13" runat="server" Height="17px" Width="35px" BackColor="LightGray" ForeColor="#064F4D"
                                BorderStyle="Solid" BorderColor="#216B68" BorderWidth="1px"
                                Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="X-Small" Font-Italic="false"
                                style="margin-top: 1px; text-indent:-3px; text-align: left; vertical-align: top; border-bottom-style: none" />
                       </asp:TableCell>
                  </asp:TableRow>
                  <asp:TableRow>
                       <asp:TableCell>
                            <asp:Button ID="CalPM07" runat="server" Height="17px" Width="35px" BackColor="LightGray" ForeColor="#064F4D"
                                BorderStyle="Solid" BorderColor="#216B68" BorderWidth="1px"
                                Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="X-Small" Font-Italic="false"
                                style="margin-bottom: 1px; text-indent:-3px; text-align: left; vertical-align: top; border-top-style: none" />
                       </asp:TableCell>
                       <asp:TableCell>
                            <asp:Button ID="CalPM08" runat="server" Height="17px" Width="35px" BackColor="LightGray" ForeColor="#064F4D"
                                BorderStyle="Solid" BorderColor="#216B68" BorderWidth="1px"
                                Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="X-Small" Font-Italic="false"
                                style="margin-bottom: 1px; text-indent:-3px; text-align: left; vertical-align: top; border-top-style: none" />
                       </asp:TableCell>
                        <asp:TableCell>
                            <asp:Button ID="CalPM09" runat="server" Height="17px" Width="35px" BackColor="LightGray" ForeColor="#064F4D"
                                BorderStyle="Solid" BorderColor="#216B68" BorderWidth="1px"
                                Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="X-Small" Font-Italic="false"
                                style="margin-bottom: 1px; text-indent:-3px; text-align: left; vertical-align: top; border-top-style: none" />
                       </asp:TableCell>
                        <asp:TableCell>
                            <asp:Button ID="CalPM10" runat="server" Height="17px" Width="35px" BackColor="LightGray" ForeColor="#064F4D"
                                BorderStyle="Solid" BorderColor="#216B68" BorderWidth="1px"
                                Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="X-Small" Font-Italic="false"
                                style="margin-bottom: 1px; text-indent:-3px; text-align: left; vertical-align: top; border-top-style: none" />
                       </asp:TableCell>
                       <asp:TableCell>
                            <asp:Button ID="CalPM11" runat="server" Height="17px" Width="35px" BackColor="LightGray" ForeColor="#064F4D"
                                BorderStyle="Solid" BorderColor="#216B68" BorderWidth="1px"
                                Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="X-Small" Font-Italic="false"
                                style="margin-bottom: 1px; text-indent:-3px; text-align: left; vertical-align: top; border-top-style: none" />
                       </asp:TableCell>
                       <asp:TableCell>
                            <asp:Button ID="CalPM12" runat="server" Height="17px" Width="35px" BackColor="LightGray" ForeColor="#064F4D"
                                BorderStyle="Solid" BorderColor="#216B68" BorderWidth="1px"
                                Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="X-Small" Font-Italic="false"
                                style="margin-bottom: 1px; text-indent:-3px; text-align: left; vertical-align: top; border-top-style: none" />
                       </asp:TableCell>
                       <asp:TableCell>
                            <asp:Button ID="CalPM13" runat="server" Height="17px" Width="35px" BackColor="LightGray" ForeColor="#064F4D"
                                BorderStyle="Solid" BorderColor="#216B68" BorderWidth="1px"
                                Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="X-Small" Font-Italic="false"
                                style="margin-bottom: 1px; text-indent:-3px; text-align: left; vertical-align: top; border-top-style: none" />
                       </asp:TableCell>
                  </asp:TableRow>
            </asp:Table>
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell>
            <asp:Table ID="CalSemaine02" runat="server" Height="36px" Width="252px" CellPadding="0" CellSpacing="0" 
                        BorderStyle="None" HorizontalAlign="Center">
                  <asp:TableRow>
                       <asp:TableCell>
                            <asp:Button ID="CalAM14" runat="server" Height="17px" Width="35px" BackColor="LightGray" ForeColor="#064F4D"
                                BorderStyle="Solid" BorderColor="#216B68" BorderWidth="1px"
                                Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="X-Small" Font-Italic="false"
                                style="margin-top: 1px; text-indent:-3px; text-align: left; vertical-align: top; border-bottom-style: none" />
                       </asp:TableCell>
                       <asp:TableCell>
                            <asp:Button ID="CalAM15" runat="server" Height="17px" Width="35px" BackColor="LightGray" ForeColor="#064F4D"
                                BorderStyle="Solid" BorderColor="#216B68" BorderWidth="1px"
                                Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="X-Small" Font-Italic="false"
                                style="margin-top: 1px; text-indent:-3px; text-align: left; vertical-align: top; border-bottom-style: none" />
                       </asp:TableCell>
                       <asp:TableCell>
                            <asp:Button ID="CalAM16" runat="server" Height="17px" Width="35px" BackColor="LightGray" ForeColor="#064F4D"
                                BorderStyle="Solid" BorderColor="#216B68" BorderWidth="1px"
                                Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="X-Small" Font-Italic="false"
                                style="margin-top: 1px; text-indent:-3px; text-align: left; vertical-align: top; border-bottom-style: none" />
                       </asp:TableCell>
                       <asp:TableCell>
                            <asp:Button ID="CalAM17" runat="server" Height="17px" Width="35px" BackColor="LightGray" ForeColor="#064F4D"
                                BorderStyle="Solid" BorderColor="#216B68" BorderWidth="1px"
                                Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="X-Small" Font-Italic="false"
                                style="margin-top: 1px; text-indent:-3px; text-align: left; vertical-align: top; border-bottom-style: none" />
                       </asp:TableCell>
                        <asp:TableCell>
                            <asp:Button ID="CalAM18" runat="server" Height="17px" Width="35px" BackColor="LightGray" ForeColor="#064F4D"
                                BorderStyle="Solid" BorderColor="#216B68" BorderWidth="1px"
                                Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="X-Small" Font-Italic="false"
                                style="margin-top: 1px; text-indent:-3px; text-align: left; vertical-align: top; border-bottom-style: none" />
                       </asp:TableCell>
                        <asp:TableCell>
                            <asp:Button ID="CalAM19" runat="server" Height="17px" Width="35px" BackColor="LightGray" ForeColor="#064F4D"
                                BorderStyle="Solid" BorderColor="#216B68" BorderWidth="1px"
                                Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="X-Small" Font-Italic="false"
                                style="margin-top: 1px; text-indent:-3px; text-align: left; vertical-align: top; border-bottom-style: none" />
                       </asp:TableCell>
                        <asp:TableCell>
                            <asp:Button ID="CalAM20" runat="server" Height="17px" Width="35px" BackColor="LightGray" ForeColor="#064F4D"
                                BorderStyle="Solid" BorderColor="#216B68" BorderWidth="1px"
                                Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="X-Small" Font-Italic="false"
                                style="margin-top: 1px; text-indent:-3px; text-align: left; vertical-align: top; border-bottom-style: none" />
                       </asp:TableCell>
                  </asp:TableRow>
                  <asp:TableRow>
                       <asp:TableCell>
                            <asp:Button ID="CalPM14" runat="server" Height="17px" Width="35px" BackColor="LightGray" ForeColor="#064F4D"
                                BorderStyle="Solid" BorderColor="#216B68" BorderWidth="1px"
                                Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="X-Small" Font-Italic="false"
                                style="margin-bottom: 1px; text-indent:-3px; text-align: left; vertical-align: top; border-top-style: none" />
                       </asp:TableCell>
                       <asp:TableCell>
                            <asp:Button ID="CalPM15" runat="server" Height="17px" Width="35px" BackColor="LightGray" ForeColor="#064F4D"
                                BorderStyle="Solid" BorderColor="#216B68" BorderWidth="1px"
                                Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="X-Small" Font-Italic="false"
                                style="margin-bottom: 1px; text-indent:-3px; text-align: left; vertical-align: top; border-top-style: none" />
                       </asp:TableCell>
                        <asp:TableCell>
                            <asp:Button ID="CalPM16" runat="server" Height="17px" Width="35px" BackColor="LightGray" ForeColor="#064F4D"
                                BorderStyle="Solid" BorderColor="#216B68" BorderWidth="1px"
                                Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="X-Small" Font-Italic="false"
                                style="margin-bottom: 1px; text-indent:-3px; text-align: left; vertical-align: top; border-top-style: none" />
                       </asp:TableCell>
                        <asp:TableCell>
                            <asp:Button ID="CalPM17" runat="server" Height="17px" Width="35px" BackColor="LightGray" ForeColor="#064F4D"
                                BorderStyle="Solid" BorderColor="#216B68" BorderWidth="1px"
                                Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="X-Small" Font-Italic="false"
                                style="margin-bottom: 1px; text-indent:-3px; text-align: left; vertical-align: top; border-top-style: none" />
                       </asp:TableCell>
                       <asp:TableCell>
                            <asp:Button ID="CalPM18" runat="server" Height="17px" Width="35px" BackColor="LightGray" ForeColor="#064F4D"
                                BorderStyle="Solid" BorderColor="#216B68" BorderWidth="1px"
                                Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="X-Small" Font-Italic="false"
                                style="margin-bottom: 1px; text-indent:-3px; text-align: left; vertical-align: top; border-top-style: none" />
                       </asp:TableCell>
                       <asp:TableCell>
                            <asp:Button ID="CalPM19" runat="server" Height="17px" Width="35px" BackColor="LightGray" ForeColor="#064F4D"
                                BorderStyle="Solid" BorderColor="#216B68" BorderWidth="1px"
                                Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="X-Small" Font-Italic="false"
                                style="margin-bottom: 1px; text-indent:-3px; text-align: left; vertical-align: top; border-top-style: none" />
                       </asp:TableCell>
                       <asp:TableCell>
                            <asp:Button ID="CalPM20" runat="server" Height="17px" Width="35px" BackColor="LightGray" ForeColor="#064F4D"
                                BorderStyle="Solid" BorderColor="#216B68" BorderWidth="1px"
                                Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="X-Small" Font-Italic="false"
                                style="margin-bottom: 1px; text-indent:-3px; text-align: left; vertical-align: top; border-top-style: none" />
                       </asp:TableCell>
                  </asp:TableRow>
            </asp:Table>
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell>
            <asp:Table ID="CalSemaine03" runat="server" Height="36px" Width="252px" CellPadding="0" CellSpacing="0" 
                        BorderStyle="None" HorizontalAlign="Center">
                  <asp:TableRow>
                       <asp:TableCell>
                            <asp:Button ID="CalAM21" runat="server" Height="17px" Width="35px" BackColor="LightGray" ForeColor="#064F4D"
                                BorderStyle="Solid" BorderColor="#216B68" BorderWidth="1px"
                                Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="X-Small" Font-Italic="false"
                                style="margin-top: 1px; text-indent:-3px; text-align: left; vertical-align: top; border-bottom-style: none" />
                       </asp:TableCell>
                       <asp:TableCell>
                            <asp:Button ID="CalAM22" runat="server" Height="17px" Width="35px" BackColor="LightGray" ForeColor="#064F4D"
                                BorderStyle="Solid" BorderColor="#216B68" BorderWidth="1px"
                                Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="X-Small" Font-Italic="false"
                                style="margin-top: 1px; text-indent:-3px; text-align: left; vertical-align: top; border-bottom-style: none" />
                       </asp:TableCell>
                       <asp:TableCell>
                            <asp:Button ID="CalAM23" runat="server" Height="17px" Width="35px" BackColor="LightGray" ForeColor="#064F4D"
                                BorderStyle="Solid" BorderColor="#216B68" BorderWidth="1px"
                                Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="X-Small" Font-Italic="false"
                                style="margin-top: 1px; text-indent:-3px; text-align: left; vertical-align: top; border-bottom-style: none" />
                       </asp:TableCell>
                       <asp:TableCell>
                            <asp:Button ID="CalAM24" runat="server" Height="17px" Width="35px" BackColor="LightGray" ForeColor="#064F4D"
                                BorderStyle="Solid" BorderColor="#216B68" BorderWidth="1px"
                                Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="X-Small" Font-Italic="false"
                                style="margin-top: 1px; text-indent:-3px; text-align: left; vertical-align: top; border-bottom-style: none" />
                       </asp:TableCell>
                        <asp:TableCell>
                            <asp:Button ID="CalAM25" runat="server" Height="17px" Width="35px" BackColor="LightGray" ForeColor="#064F4D"
                                BorderStyle="Solid" BorderColor="#216B68" BorderWidth="1px"
                                Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="X-Small" Font-Italic="false"
                                style="margin-top: 1px; text-indent:-3px; text-align: left; vertical-align: top; border-bottom-style: none" />
                       </asp:TableCell>
                        <asp:TableCell>
                            <asp:Button ID="CalAM26" runat="server" Height="17px" Width="35px" BackColor="LightGray" ForeColor="#064F4D"
                                BorderStyle="Solid" BorderColor="#216B68" BorderWidth="1px"
                                Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="X-Small" Font-Italic="false"
                                style="margin-top: 1px; text-indent:-3px; text-align: left; vertical-align: top; border-bottom-style: none" />
                       </asp:TableCell>
                        <asp:TableCell>
                            <asp:Button ID="CalAM27" runat="server" Height="17px" Width="35px" BackColor="LightGray" ForeColor="#064F4D"
                                BorderStyle="Solid" BorderColor="#216B68" BorderWidth="1px"
                                Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="X-Small" Font-Italic="false"
                                style="margin-top: 1px; text-indent:-3px; text-align: left; vertical-align: top; border-bottom-style: none" />
                       </asp:TableCell>
                  </asp:TableRow>
                  <asp:TableRow>
                       <asp:TableCell>
                            <asp:Button ID="CalPM21" runat="server" Height="17px" Width="35px" BackColor="LightGray" ForeColor="#064F4D"
                                BorderStyle="Solid" BorderColor="#216B68" BorderWidth="1px"
                                Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="X-Small" Font-Italic="false"
                                style="margin-bottom: 1px; text-indent:-3px; text-align: left; vertical-align: top; border-top-style: none" />
                       </asp:TableCell>
                       <asp:TableCell>
                            <asp:Button ID="CalPM22" runat="server" Height="17px" Width="35px" BackColor="LightGray" ForeColor="#064F4D"
                                BorderStyle="Solid" BorderColor="#216B68" BorderWidth="1px"
                                Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="X-Small" Font-Italic="false"
                                style="margin-bottom: 1px; text-indent:-3px; text-align: left; vertical-align: top; border-top-style: none" />
                       </asp:TableCell>
                        <asp:TableCell>
                            <asp:Button ID="CalPM23" runat="server" Height="17px" Width="35px" BackColor="LightGray" ForeColor="#064F4D"
                                BorderStyle="Solid" BorderColor="#216B68" BorderWidth="1px"
                                Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="X-Small" Font-Italic="false"
                                style="margin-bottom: 1px; text-indent:-3px; text-align: left; vertical-align: top; border-top-style: none" />
                       </asp:TableCell>
                        <asp:TableCell>
                            <asp:Button ID="CalPM24" runat="server" Height="17px" Width="35px" BackColor="LightGray" ForeColor="#064F4D"
                                BorderStyle="Solid" BorderColor="#216B68" BorderWidth="1px"
                                Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="X-Small" Font-Italic="false"
                                style="margin-bottom: 1px; text-indent:-3px; text-align: left; vertical-align: top; border-top-style: none" />
                       </asp:TableCell>
                       <asp:TableCell>
                            <asp:Button ID="CalPM25" runat="server" Height="17px" Width="35px" BackColor="LightGray" ForeColor="#064F4D"
                                BorderStyle="Solid" BorderColor="#216B68" BorderWidth="1px"
                                Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="X-Small" Font-Italic="false"
                                style="margin-bottom: 1px; text-indent:-3px; text-align: left; vertical-align: top; border-top-style: none" />
                       </asp:TableCell>
                       <asp:TableCell>
                            <asp:Button ID="CalPM26" runat="server" Height="17px" Width="35px" BackColor="LightGray" ForeColor="#064F4D"
                                BorderStyle="Solid" BorderColor="#216B68" BorderWidth="1px"
                                Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="X-Small" Font-Italic="false"
                                style="margin-bottom: 1px; text-indent:-3px; text-align: left; vertical-align: top; border-top-style: none" />
                       </asp:TableCell>
                       <asp:TableCell>
                            <asp:Button ID="CalPM27" runat="server" Height="17px" Width="35px" BackColor="LightGray" ForeColor="#064F4D"
                                BorderStyle="Solid" BorderColor="#216B68" BorderWidth="1px"
                                Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="X-Small" Font-Italic="false"
                                style="margin-bottom: 1px; text-indent:-3px; text-align: left; vertical-align: top; border-top-style: none" />
                       </asp:TableCell>
                  </asp:TableRow>
            </asp:Table>
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell>
            <asp:Table ID="CalSemaine04" runat="server" Height="36px" Width="252px" CellPadding="0" CellSpacing="0" 
                        BorderStyle="None" HorizontalAlign="Center">
                  <asp:TableRow>
                       <asp:TableCell>
                            <asp:Button ID="CalAM28" runat="server" Height="17px" Width="35px" BackColor="LightGray" ForeColor="#064F4D"
                                BorderStyle="Solid" BorderColor="#216B68" BorderWidth="1px"
                                Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="X-Small" Font-Italic="false"
                                style="margin-top: 1px; text-indent:-3px; text-align: left; vertical-align: top; border-bottom-style: none" />
                       </asp:TableCell>
                       <asp:TableCell>
                            <asp:Button ID="CalAM29" runat="server" Height="17px" Width="35px" BackColor="LightGray" ForeColor="#064F4D"
                                BorderStyle="Solid" BorderColor="#216B68" BorderWidth="1px"
                                Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="X-Small" Font-Italic="false"
                                style="margin-top: 1px; text-indent:-3px; text-align: left; vertical-align: top; border-bottom-style: none" />
                       </asp:TableCell>
                       <asp:TableCell>
                            <asp:Button ID="CalAM30" runat="server" Height="17px" Width="35px" BackColor="LightGray" ForeColor="#064F4D"
                                BorderStyle="Solid" BorderColor="#216B68" BorderWidth="1px"
                                Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="X-Small" Font-Italic="false"
                                style="margin-top: 1px; text-indent:-3px; text-align: left; vertical-align: top; border-bottom-style: none" />
                       </asp:TableCell>
                       <asp:TableCell>
                            <asp:Button ID="CalAM31" runat="server" Height="17px" Width="35px" BackColor="LightGray" ForeColor="#064F4D"
                                BorderStyle="Solid" BorderColor="#216B68" BorderWidth="1px"
                                Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="X-Small" Font-Italic="false"
                                style="margin-top: 1px; text-indent:-3px; text-align: left; vertical-align: top; border-bottom-style: none" />
                       </asp:TableCell>
                        <asp:TableCell>
                            <asp:Button ID="CalAM32" runat="server" Height="17px" Width="35px" BackColor="LightGray" ForeColor="#064F4D"
                                BorderStyle="Solid" BorderColor="#216B68" BorderWidth="1px"
                                Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="X-Small" Font-Italic="false"
                                style="margin-top: 1px; text-indent:-3px; text-align: left; vertical-align: top; border-bottom-style: none" />
                       </asp:TableCell>
                        <asp:TableCell>
                            <asp:Button ID="CalAM33" runat="server" Height="17px" Width="35px" BackColor="LightGray" ForeColor="#064F4D"
                                BorderStyle="Solid" BorderColor="#216B68" BorderWidth="1px"
                                Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="X-Small" Font-Italic="false"
                                style="margin-top: 1px; text-indent:-3px; text-align: left; vertical-align: top; border-bottom-style: none" />
                       </asp:TableCell>
                        <asp:TableCell>
                            <asp:Button ID="CalAM34" runat="server" Height="17px" Width="35px" BackColor="LightGray" ForeColor="#064F4D"
                                BorderStyle="Solid" BorderColor="#216B68" BorderWidth="1px"
                                Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="X-Small" Font-Italic="false"
                                style="margin-top: 1px; text-indent:-3px; text-align: left; vertical-align: top; border-bottom-style: none" />
                       </asp:TableCell>
                  </asp:TableRow>
                  <asp:TableRow>
                       <asp:TableCell>
                            <asp:Button ID="CalPM28" runat="server" Height="17px" Width="35px" BackColor="LightGray" ForeColor="#064F4D"
                                BorderStyle="Solid" BorderColor="#216B68" BorderWidth="1px"
                                Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="X-Small" Font-Italic="false"
                                style="margin-bottom: 1px; text-indent:-3px; text-align: left; vertical-align: top; border-top-style: none" />
                       </asp:TableCell>
                       <asp:TableCell>
                            <asp:Button ID="CalPM29" runat="server" Height="17px" Width="35px" BackColor="LightGray" ForeColor="#064F4D"
                                BorderStyle="Solid" BorderColor="#216B68" BorderWidth="1px"
                                Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="X-Small" Font-Italic="false"
                                style="margin-bottom: 1px; text-indent:-3px; text-align: left; vertical-align: top; border-top-style: none" />
                       </asp:TableCell>
                        <asp:TableCell>
                            <asp:Button ID="CalPM30" runat="server" Height="17px" Width="35px" BackColor="LightGray" ForeColor="#064F4D"
                                BorderStyle="Solid" BorderColor="#216B68" BorderWidth="1px"
                                Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="X-Small" Font-Italic="false"
                                style="margin-bottom: 1px; text-indent:-3px; text-align: left; vertical-align: top; border-top-style: none" />
                       </asp:TableCell>
                        <asp:TableCell>
                            <asp:Button ID="CalPM31" runat="server" Height="17px" Width="35px" BackColor="LightGray" ForeColor="#064F4D"
                                BorderStyle="Solid" BorderColor="#216B68" BorderWidth="1px"
                                Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="X-Small" Font-Italic="false"
                                style="margin-bottom: 1px; text-indent:-3px; text-align: left; vertical-align: top; border-top-style: none" />
                       </asp:TableCell>
                       <asp:TableCell>
                            <asp:Button ID="CalPM32" runat="server" Height="17px" Width="35px" BackColor="LightGray" ForeColor="#064F4D"
                                BorderStyle="Solid" BorderColor="#216B68" BorderWidth="1px"
                                Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="X-Small" Font-Italic="false"
                                style="margin-bottom: 1px; text-indent:-3px; text-align: left; vertical-align: top; border-top-style: none" />
                       </asp:TableCell>
                       <asp:TableCell>
                            <asp:Button ID="CalPM33" runat="server" Height="17px" Width="35px" BackColor="LightGray" ForeColor="#064F4D"
                                BorderStyle="Solid" BorderColor="#216B68" BorderWidth="1px"
                                Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="X-Small" Font-Italic="false"
                                style="margin-bottom: 1px; text-indent:-3px; text-align: left; vertical-align: top; border-top-style: none" />
                       </asp:TableCell>
                       <asp:TableCell>
                            <asp:Button ID="CalPM34" runat="server" Height="17px" Width="35px" BackColor="LightGray" ForeColor="#064F4D"
                                BorderStyle="Solid" BorderColor="#216B68" BorderWidth="1px"
                                Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="X-Small" Font-Italic="false"
                                style="margin-bottom: 1px; text-indent:-3px; text-align: left; vertical-align: top; border-top-style: none" />
                       </asp:TableCell>
                  </asp:TableRow>
            </asp:Table>
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell>
            <asp:Table ID="CalSemaine05" runat="server" Height="36px" Width="252px" CellPadding="0" CellSpacing="0" 
                        BorderStyle="None" HorizontalAlign="Center">
                  <asp:TableRow>
                       <asp:TableCell>
                            <asp:Button ID="CalAM35" runat="server" Height="17px" Width="35px" BackColor="LightGray" ForeColor="#064F4D"
                                BorderStyle="Solid" BorderColor="#216B68" BorderWidth="1px"
                                Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="X-Small" Font-Italic="false"
                                style="margin-top: 1px; text-indent:-3px; text-align: left; vertical-align: top; border-bottom-style: none" />
                       </asp:TableCell>
                       <asp:TableCell>
                            <asp:Button ID="CalAM36" runat="server" Height="17px" Width="35px" BackColor="LightGray" ForeColor="#064F4D"
                                BorderStyle="Solid" BorderColor="#216B68" BorderWidth="1px"
                                Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="X-Small" Font-Italic="false"
                                style="margin-top: 1px; text-indent:-3px; text-align: left; vertical-align: top; border-bottom-style: none" />
                       </asp:TableCell>
                        <asp:TableCell>
                            <asp:Label ID="CalDateSel00" runat="server" Height="16px" Width="178px" BackColor="#124545" ForeColor="White"
                                BorderStyle="Solid" BorderColor="#8DA8A3" BorderWidth="1px"
                                Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="Smaller" Font-Italic="false"
                               style="margin-top: 1px; text-indent:-3px; text-align: center; vertical-align: top; border-bottom-style: none" />
                       </asp:TableCell>
                  </asp:TableRow>
                  <asp:TableRow>
                       <asp:TableCell>
                            <asp:Button ID="CalPM35" runat="server" Height="17px" Width="35px" BackColor="LightGray" ForeColor="#064F4D"
                                BorderStyle="Solid" BorderColor="#216B68" BorderWidth="1px"
                                Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="X-Small" Font-Italic="false"
                                style="margin-bottom: 1px; text-indent:-3px; text-align: left; vertical-align: top; border-top-style: none" />
                       </asp:TableCell>
                       <asp:TableCell>
                            <asp:Button ID="CalPM36" runat="server" Height="17px" Width="35px" BackColor="LightGray" ForeColor="#064F4D"
                                BorderStyle="Solid" BorderColor="#216B68" BorderWidth="1px"
                                Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="X-Small" Font-Italic="false"
                                style="margin-bottom: 1px; text-indent:-3px; text-align: left; vertical-align: top; border-top-style: none" />
                       </asp:TableCell>
                       <asp:TableCell>
                            <asp:Label ID="CalDateSel01" runat="server" Height="16px" Width="178px" BackColor="#124545" ForeColor="White"
                                BorderStyle="Solid" BorderColor="#8DA8A3" BorderWidth="1px"
                                Font-Bold="false" Font-Names="Trebuchet MS" Font-Size="Smaller" Font-Italic="false"
                                style="margin-bottom: 1px; text-indent:-3px; text-align: center; vertical-align: top; border-top-style: none" />
                       </asp:TableCell>
                  </asp:TableRow>
            </asp:Table>
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell>
            <Virtualia:VCocheSimple ID="CalCoche" runat="server"
                   V_PointdeVue="0" V_SiDonneeDico="false" V_Width="248px" V_Style="margin-left:4px; margin-top:5px"
                   V_BackColor="#8DA8A3" V_ForeColor="#E9FDF9" V_BorderColor="#B0E0D7" /> 
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell>
            <asp:HiddenField ID="HSelMois" runat="server" Value="" />
            <asp:HiddenField ID="HSelDate" runat="server" Value="" />
            <asp:HiddenField ID="HSelCoche" runat="server" Value="" />
        </asp:TableCell>
    </asp:TableRow>
</asp:Table>