﻿Option Explicit On
Option Strict On
Option Compare Text
Namespace Session
    Public Class ClefV3OldCnx
        Private WsRhDates As Virtualia.Systeme.Fonctions.CalculDates
        Private WsRhFct As Virtualia.Systeme.Fonctions.Generales
        Private WsChainePostee As String
        Private WsSiAccesManager As Boolean = False
        Private WsSiAccesGRH As Boolean = False
        Private WsAppliAccedee As String = ""
        Private WsUtilisateurV3 As Virtualia.Version3.Utilisateur.UtilisateurV3
        Private WsNomCnxV3 As String
        Private WsDossierPer As Virtualia.Net.WebAppli.SelfV4.DossierPersonne = Nothing

        Public ReadOnly Property SiAcces_Manager As Boolean
            Get
                Return WsSiAccesManager
            End Get
        End Property

        Public ReadOnly Property SiAcces_DRH As Boolean
            Get
                Return WsSiAccesGRH
            End Get
        End Property

        Public ReadOnly Property Appli_Accedee As String
            Get
                Return WsAppliAccedee
            End Get
        End Property

        Public ReadOnly Property Nom_CnxV3 As String
            Get
                Return WsNomCnxV3
            End Get
        End Property

        Public ReadOnly Property ObjetUti_CnxV3 As Virtualia.Version3.Utilisateur.UtilisateurV3
            Get
                Return WsUtilisateurV3
            End Get
        End Property

        Public ReadOnly Property ObjetDossier As Virtualia.Net.WebAppli.SelfV4.DossierPersonne
            Get
                Return WsDossierPer
            End Get
        End Property

        Public Function SiAccesSelf_OK(ByVal AppGlobal As Virtualia.Net.Session.ObjetGlobal) As Boolean
            Dim TabPOST(0) As String
            Dim Identifiant As String = ""
            Dim IdentifiantManager As String = ""
            Dim NomEvaluateur As String

            TabPOST = Strings.Split(WsChainePostee, "VTAPOST")
            Try
                Identifiant = TabPOST(4)
                NomEvaluateur = HttpUtility.UrlDecode(TabPOST(5), System.Text.Encoding.Default)
                IdentifiantManager = TabPOST(6)
                If Not IsNumeric(Identifiant) Then
                    Return False
                End If
                If Strings.Left(TabPOST(1), 4) <> "Self" Then
                    WsSiAccesManager = True
                End If
                WsDossierPer = AppGlobal.Dossier(CInt(Identifiant))
                If WsDossierPer Is Nothing Then
                    Return False
                End If
                If IsNumeric(IdentifiantManager) Then
                    WsDossierPer.Ide_Manager = CInt(IdentifiantManager)
                End If
            Catch ex2 As Exception
                Return False
            End Try
            WsAppliAccedee = TabPOST(1)
            Return True
        End Function

        Public Function SiAccesDRH_OK(ByVal AppGlobal As Virtualia.Net.Session.ObjetGlobal) As Boolean
            Dim TabPOST(0) As String
            Dim Licence As String = ""
            Dim Filtre As String = ""
            Dim SiHisto As Integer = 0
            Dim Identifiant As Integer = 0

            TabPOST = Strings.Split(WsChainePostee, "VTA")
            Try
                Licence = TabPOST(0)
                WsNomCnxV3 = HttpUtility.UrlDecode(TabPOST(1), System.Text.Encoding.Default)
                If IsNumeric(TabPOST(2)) Then
                    Identifiant = CInt(TabPOST(2))
                End If
                Filtre = HttpUtility.UrlDecode(TabPOST(3), System.Text.Encoding.Default)
                If IsNumeric(TabPOST(4)) Then
                    SiHisto = CInt(TabPOST(4))
                End If
                If IsNumeric(TabPOST(5)) Then
                    If VerificationdelAppel(CInt(TabPOST(5))) = False Then
                        Identifiant = 0
                    End If
                Else
                    Identifiant = 0
                End If

            Catch ex1 As Exception
                Return False
            End Try
            If Licence <> Strings.Replace(AppGlobal.VirModele.InstanceProduit.ClefProduit, "-", "") Or Identifiant = 0 Then
                Return False
            End If
            WsAppliAccedee = TabPOST(6)
            If WsAppliAccedee = "PFR" Then
                Dim TabDRH(0) As String
                Dim I As Integer
                TabDRH = Strings.Split(System.Configuration.ConfigurationManager.AppSettings("DRH"), ";", -1)
                For I = 0 To TabDRH.Count - 1
                    If WsNomCnxV3 = TabDRH(I) Then
                        Return True
                    End If
                Next I
            Else
                Return VerificationUtiVersion3()
            End If
            Return False
        End Function

        Private Function VerificationUtiVersion3() As Boolean
            If WsNomCnxV3 = "" Then
                Return False
            End If
            WsUtilisateurV3 = New Virtualia.Version3.Utilisateur.UtilisateurV3(System.Configuration.ConfigurationManager.AppSettings("RepertoireUtilisateurV3"), WsNomCnxV3)
            If WsUtilisateurV3 IsNot Nothing AndAlso WsUtilisateurV3.Nom.ToUpper = WsNomCnxV3.ToUpper Then
                Return True
            End If
            Return False
        End Function

        Private Function VerificationdelAppel(ByVal Valeur As Integer) As Boolean
            Dim Verif As Integer = Day(Now) + Month(Now) + Hour(Now) + Year(Now) - 1951
            If Verif = Valeur Or Verif = Valeur + 1 Then
                Return True
            End If
            Return False
        End Function

        Public Sub New(ByVal ChainePOST As String, ByVal SiDRH As Boolean)
            WsChainePostee = ChainePOST
            WsSiAccesGRH = SiDRH
            WsRhDates = New Virtualia.Systeme.Fonctions.CalculDates
            WsRhFct = New Virtualia.Systeme.Fonctions.Generales
        End Sub
    End Class
End Namespace