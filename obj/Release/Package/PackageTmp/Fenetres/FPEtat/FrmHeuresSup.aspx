﻿<%@ Page Language="vb" AutoEventWireup="false" MasterPageFile="~/PagesMaitres/VirtualiaMain.Master"  
         CodeBehind="FrmHeuresSup.aspx.vb" Inherits="Virtualia.Net.FrmHeuresSup" MaintainScrollPositionOnPostback="True" %>

<%@ MasterType VirtualPath="~/PagesMaitres/VirtualiaMain.master"  %>
<%@ Register src="~/Controles/Commun/VListeGrid.ascx" tagname="VListeGrid" tagprefix="Virtualia" %>

<asp:Content ID="Corps" ContentPlaceHolderID="Principal" runat="server">
    <asp:Table ID="CadreGeneral" runat="server" HorizontalAlign="Center" Width="1050px">
         <asp:TableRow>
            <asp:TableCell>
                <asp:Panel ID="CadreAffichage" runat="server" BackColor="Transparent" HorizontalAlign="Center" Width="1050px"
                    BackImageUrl="~/Images/Fonds/Fond_Saisie.jpg" BorderStyle="none">
                    <asp:Table ID="CadreFrais" runat="server" Width="1040px" HorizontalAlign="Center" BorderStyle="none">
                         <asp:TableRow Height="40px">
                            <asp:TableCell VerticalAlign="Top" ColumnSpan="2">
                                <asp:Label ID="EtiIdentité" runat="server" Height="20px" Width="600px" Text=""
                                    BackColor="#124545" BorderColor="#B6C7E2"  BorderStyle="Groove" BorderWidth="2px" ForeColor="#D7FAF3"
                                    Font-Bold="True" Font-Names="Trebuchet MS" Font-Size="Small"
                                    style="margin-top: 5px; margin-left: 4px; margin-bottom: 10px; text-indent: 5px; text-align: center;" >
                                </asp:Label>          
                            </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow>
                            <asp:TableCell>
                                <asp:Table ID="CadreAnnee" runat="server">
                                    <asp:TableRow Height="22px" Width="400px">
                                        <asp:TableCell VerticalAlign="Top" HorizontalAlign="Left">
                                            <asp:Label ID="EtiSelAnnee" runat="server" Text="Annee"
                                                        Height="20px" Width="100px" BackColor="#225C59" ForeColor="#D7FAF3"
                                                        BorderColor="#B6C7E2"  BorderStyle="Ridge" BorderWidth="2px"
                                                        Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small" Font-Italic="true"
                                                        style="text-align: center">
                                            </asp:Label>
                                        </asp:TableCell>
                                        <asp:TableCell VerticalAlign="Top" HorizontalAlign="Left">
                                            <asp:DropDownList ID="DropDownAnnee" runat="server" AutoPostBack="True" 
                                                Height="22px" Width="150px" BackColor="#124545" ForeColor="White"
                                                style="margin-top: 0px; margin-left: 5px; border-spacing: 2px; font-family:Trebuchet MS; 
                                                        font-size: small; font-weight: bold; font-style: normal; text-indent: 5px; text-align: left">
                                            </asp:DropDownList>
                                        </asp:TableCell>
                                    </asp:TableRow>
                                </asp:Table>
                           </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow>
                            <asp:TableCell VerticalAlign="Top">
                                <asp:Table ID="CadreSolde" runat="server" HorizontalAlign="Center" Width="410px">
                                    <asp:TableRow>
                                        <asp:TableCell VerticalAlign="Middle" HorizontalAlign="Center">
                                            <asp:Label ID="EtiSoldeAnte" runat="server" Height="30px" Width="200px" Text="Situation en début d'année"
                                                BackColor="#124545" BorderColor="#B6C7E2"  BorderStyle="Groove" BorderWidth="2px" ForeColor="#D7FAF3"
                                                Font-Bold="True" Font-Names="Trebuchet MS" Font-Size="Small"
                                                style="margin-left: 4px; text-indent: 5px; text-align: center;" >
                                            </asp:Label>          
                                        </asp:TableCell>
                                        <asp:TableCell VerticalAlign="Middle" HorizontalAlign="Left">
                                            <asp:Label ID="DonSoldeAnte" runat="server" Height="20px" Width="200px" Text="0,00"
                                                BackColor="White" BorderColor="#B6C7E2"  BorderStyle="Inset" BorderWidth="2px" ForeColor="#124545"
                                                Font-Bold="True" Font-Names="Trebuchet MS" Font-Size="Small"
                                                style="margin-left: 4px; text-indent: 5px; text-align: center;" >
                                            </asp:Label>          
                                        </asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell VerticalAlign="Middle" HorizontalAlign="Center">
                                            <asp:Label ID="EtiTotalHS" runat="server" Height="30px" Width="200px" Text="Total des heures supplémentaires de l'année"
                                                BackColor="#124545" BorderColor="#B6C7E2"  BorderStyle="Groove" BorderWidth="2px" ForeColor="#D7FAF3"
                                                Font-Bold="True" Font-Names="Trebuchet MS" Font-Size="Small"
                                                style="margin-left: 4px; text-indent: 5px; text-align: center;" >
                                            </asp:Label>          
                                        </asp:TableCell>
                                        <asp:TableCell VerticalAlign="Middle" HorizontalAlign="Left">
                                            <asp:Label ID="DonTotalHS" runat="server" Height="20px" Width="200px" Text="0,00"
                                                BackColor="White" BorderColor="#B6C7E2"  BorderStyle="Inset" BorderWidth="2px" ForeColor="#124545"
                                                Font-Bold="True" Font-Names="Trebuchet MS" Font-Size="Small"
                                                style="margin-left: 4px; text-indent: 5px; text-align: center;" >
                                            </asp:Label>          
                                        </asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell VerticalAlign="Middle" HorizontalAlign="Center">
                                            <asp:Label ID="EtiTotalRecup" runat="server" Height="30px" Width="200px" Text="Total des récupérations de l'année"
                                                BackColor="#124545" BorderColor="#B6C7E2"  BorderStyle="Groove" BorderWidth="2px" ForeColor="#D7FAF3"
                                                Font-Bold="True" Font-Names="Trebuchet MS" Font-Size="Small"
                                                style="margin-left: 4px; text-indent: 5px; text-align: center;" >
                                            </asp:Label>          
                                        </asp:TableCell>
                                        <asp:TableCell VerticalAlign="Middle" HorizontalAlign="Left">
                                            <asp:Label ID="DonTotalRecup" runat="server" Height="20px" Width="200px" Text="0,00"
                                                BackColor="White" BorderColor="#B6C7E2"  BorderStyle="Inset" BorderWidth="2px" ForeColor="#C71F23"
                                                Font-Bold="True" Font-Names="Trebuchet MS" Font-Size="Small"
                                                style="margin-left: 4px; text-indent: 5px; text-align: center;" >
                                            </asp:Label>          
                                        </asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell VerticalAlign="Middle" HorizontalAlign="Center">
                                            <asp:Label ID="EtiSoldeHS" runat="server" Height="30px" Width="200px" Text="Solde courant"
                                                BackColor="#124545" BorderColor="#B6C7E2"  BorderStyle="Groove" BorderWidth="2px" ForeColor="#D7FAF3"
                                                Font-Bold="True" Font-Names="Trebuchet MS" Font-Size="Small"
                                                style="margin-left: 4px; text-indent: 5px; text-align: center;" >
                                            </asp:Label>          
                                        </asp:TableCell>
                                        <asp:TableCell VerticalAlign="Middle" HorizontalAlign="Left">
                                            <asp:Label ID="DonSoldeHS" runat="server" Height="20px" Width="200px" Text="0,00"
                                                BackColor="White" BorderColor="#B6C7E2"  BorderStyle="Inset" BorderWidth="2px" ForeColor="#124545"
                                                Font-Bold="True" Font-Names="Trebuchet MS" Font-Size="Small"
                                                style="margin-left: 4px; text-indent: 5px; text-align: center;" >
                                            </asp:Label>          
                                        </asp:TableCell>
                                    </asp:TableRow>
                                </asp:Table>
                            </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow>
                            <asp:TableCell VerticalAlign="Top">
                                <asp:Label ID="EtiListeHS" runat="server" Height="20px" Width="500px" Text="Détail des heures supplémentaires"
                                    BackColor="#124545" BorderColor="#B6C7E2"  BorderStyle="Groove" BorderWidth="2px" ForeColor="#D7FAF3"
                                    Font-Bold="True" Font-Names="Trebuchet MS" Font-Size="Small"
                                    style="margin-top: 5px; margin-left: 4px; margin-bottom: 10px; text-indent: 5px; text-align: center;" >
                                </asp:Label>          
                            </asp:TableCell>
                        </asp:TableRow>
                       <asp:TableRow>
                            <asp:TableCell VerticalAlign="Top">
                                <Virtualia:VListeGrid ID="ListeHS" runat="server" CadreWidth="1040px"
                                        SiCaptionVisible="false" SiPagination="false" SiCaseAcocher="false"
                                        SiColonneSelect="false" TaillePage="200" />
                            </asp:TableCell>
                       </asp:TableRow>
                       <asp:TableRow>
                            <asp:TableCell VerticalAlign="Top">
                                <asp:Label ID="EtiListeRecup" runat="server" Height="20px" Width="500px" Text="Détail des récupérations"
                                    BackColor="#124545" BorderColor="#B6C7E2"  BorderStyle="Groove" BorderWidth="2px" ForeColor="#D7FAF3"
                                    Font-Bold="True" Font-Names="Trebuchet MS" Font-Size="Small"
                                    style="margin-top: 8px; margin-left: 4px; margin-bottom: 3px; text-indent: 5px; text-align: center;" >
                                </asp:Label>          
                            </asp:TableCell>
                        </asp:TableRow>
                       <asp:TableRow>
                            <asp:TableCell VerticalAlign="Top">
                                <Virtualia:VListeGrid ID="ListeRecup" runat="server" CadreWidth="500px"
                                        SiCaptionVisible="false" SiPagination="false" SiCaseAcocher="false"
                                        SiColonneSelect="false" TaillePage="200" />
                            </asp:TableCell>
                       </asp:TableRow>
                    </asp:Table>
                </asp:Panel>
            </asp:TableCell>
        </asp:TableRow>
    </asp:Table>
</asp:Content>



