﻿Option Strict On
Option Explicit On
Option Compare Text
Imports VI = Virtualia.Systeme.Constantes
Public Class PER_JOUR_ASTREINTE
    Inherits Virtualia.Net.Controles.ObjetWebControle
    Public Delegate Sub Retour_MsgEventHandler(ByVal sender As Object, ByVal e As Virtualia.Systeme.Evenements.DonneeChangeEventArgs)
    Public Event ValeurRetour As Retour_MsgEventHandler
    Private WsNumObjet As Integer = 171
    Private WsNomTable As String = "PER_JOUR_ASTREINTE"
    Private WsFiche As Virtualia.TablesObjet.ShemaPER.PER_JOUR_ASTREINTE
    Private WsNomState As String = "VAsstreinte"
    Private WsNomStateUti As String = "AsstreinteCA"
    Private WsCacheUti As ArrayList

    Protected Overridable Sub ReponseRetour(ByVal e As Virtualia.Systeme.Evenements.DonneeChangeEventArgs)
        RaiseEvent ValeurRetour(Me, e)
    End Sub
    Public ReadOnly Property MonCadre() As System.Web.UI.Control
        Get
            Return Me.CadreInfo
        End Get
    End Property

    Public WriteOnly Property CadreStyle() As String
        Set(ByVal value As String)
            Dim TableauData(0) As String
            Dim TableauW(0) As String
            Dim IndiceI As Integer
            TableauData = Strings.Split(value, ";")
            For IndiceI = 0 To TableauData.Count - 1
                If TableauData(IndiceI) = "" Then
                    Exit For
                End If
                TableauW = Strings.Split(TableauData(IndiceI), ":")
            Next IndiceI
        End Set
    End Property

    Public WriteOnly Property Identifiant() As Integer
        Set(ByVal value As Integer)
            Dim SiaActualiser As Boolean = False
            If Session("Identifiant") IsNot Nothing Then
                If Session("Identifiant").ToString <> CStr(value) Then
                    Session.Remove("Identifiant")
                    Session.Add("Identifiant", CStr(value))
                    SiaActualiser = True
                End If
            Else
                Session.Add("Identifiant", CStr(value))
                SiaActualiser = True
            End If
            If V_Identifiant <> value Then

                Dim ColHisto As New List(Of Integer)
                ColHisto.Add(0)
                ColHisto.Add(1)
                ColHisto.Add(4)
                ColHisto.Add(5)
                V_CacheColHisto = ColHisto

                V_Identifiant = value
                If SiaActualiser = True Then
                    Call V_ActualiserObjet(value)
                End If
                Call ActualiserListe()
            End If
        End Set
    End Property

    Private Sub ActualiserListe()
        CadreCmdOK.Visible = False
        Dim LstLibels As New List(Of String)
        LstLibels.Add("Aucune journée d'astreinte")
        LstLibels.Add("Une journée d'astreinte")
        LstLibels.Add("journées d'astreinte")
        ListeGrille.V_LibelCaption = LstLibels

        Dim LstColonnes As New List(Of String)
        LstColonnes.Add("Date de l'astreinte")
        LstColonnes.Add("Nature")
        LstColonnes.Add("Si validé DG")
        LstColonnes.Add("Date validation")
        LstColonnes.Add("Clef")
        ListeGrille.V_LibelColonne = LstColonnes

        If V_Identifiant > 0 Then
            Call InitialiserControles()
            If V_IndexFiche = -1 Then
                ListeGrille.V_Liste = Nothing
                Exit Sub
            End If
            ListeGrille.V_Liste = V_ListeFiches
        End If
    End Sub

    Protected Sub ListeGrille_ValeurChange(ByVal sender As Object, ByVal e As Virtualia.Systeme.Evenements.DonneeChangeEventArgs) Handles ListeGrille.ValeurChange
        If e.Valeur = "" Then
            Exit Sub
        End If
        V_Occurence = e.Valeur
    End Sub

    Private Sub LireLaFiche()
        Dim CacheDonnee As List(Of String) = V_CacheMaj
        If CacheDonnee Is Nothing Then
            Exit Sub
        End If
        Dim NumInfo As Integer
        Dim SiEnLecture As Boolean = False
        Dim Ctl As Control
        Dim VirControle As Controles_VCoupleEtiDonnee
        Dim IndiceI As Integer = 0
        If CacheDonnee(5) IsNot Nothing Then
            If CacheDonnee(5).ToString <> "" And CacheDonnee(5).ToString <> V_WebFonction.ViRhDates.DateduJour Then
                SiEnLecture = True
            End If
        End If
        Dim UtiSession As Virtualia.Net.Session.SessionModule = CType(V_WebFonction.PointeurUtilisateur.V_PointeurSessionModule, Virtualia.Net.Session.SessionModule)
        If UtiSession Is Nothing Then
            SiEnLecture = True
        End If
        Do
            Ctl = V_WebFonction.VirWebControle(Me.CadreInfo, "InfoH", IndiceI)
            If Ctl Is Nothing Then
                Exit Do
            End If
            NumInfo = CInt(Strings.Right(Ctl.ID, 2))
            VirControle = CType(Ctl, Controles_VCoupleEtiDonnee)
            If CacheDonnee(NumInfo) Is Nothing Then
                VirControle.DonText = ""
            Else
                VirControle.DonText = CacheDonnee(NumInfo).ToString
            End If
            VirControle.V_SiEnLectureSeule = True
            IndiceI += 1
        Loop

        IndiceI = 0
        Dim VirDonneeVerticale As Controles_VCoupleVerticalEtiDonnee
        Do
            Ctl = V_WebFonction.VirWebControle(Me.CadreInfo, "InfoV", IndiceI)
            If Ctl Is Nothing Then
                Exit Do
            End If
            NumInfo = CInt(Strings.Right(Ctl.ID, 2))
            VirDonneeVerticale = CType(Ctl, Controles_VCoupleVerticalEtiDonnee)
            If CacheDonnee(NumInfo) Is Nothing Then
                VirDonneeVerticale.DonText = ""
            Else
                VirDonneeVerticale.DonText = CacheDonnee(NumInfo).ToString
            End If
            VirDonneeVerticale.V_SiEnLectureSeule = SiEnLecture
            VirDonneeVerticale.V_SiAutoPostBack = Not SiEnLecture
            If SiEnLecture = False Then
                If UtiSession.SiAccesGRH = True Then
                    InfoV03.V_SiEnLectureSeule = True
                    InfoV06.V_SiEnLectureSeule = False
                    InfoV06.V_SiAutoPostBack = True
                Else
                    InfoV03.V_SiEnLectureSeule = False
                    InfoV03.V_SiAutoPostBack = True
                    InfoV06.V_SiEnLectureSeule = true
                End If
            End If
            IndiceI += 1
        Loop

        Dim VirDonneeDate As VCoupleEtiDate
        IndiceI = 0
        Do
            Ctl = V_WebFonction.VirWebControle(Me.CadreInfo, "InfoD", IndiceI)
            If Ctl Is Nothing Then
                Exit Do
            End If
            NumInfo = CInt(Strings.Right(Ctl.ID, 2))
            VirDonneeDate = CType(Ctl, VCoupleEtiDate)
            If CacheDonnee(NumInfo) Is Nothing Then
                VirDonneeDate.DonText = ""
            Else
                VirDonneeDate.DonText = CacheDonnee(NumInfo).ToString
            End If
            If VirDonneeDate.SiDateFin = True Then
                VirDonneeDate.DateDebut = CacheDonnee(0).ToString
            End If
            VirDonneeDate.V_SiEnLectureSeule = SiEnLecture
            VirDonneeDate.V_SiAutoPostBack = Not SiEnLecture
            IndiceI += 1
        Loop

        Dim VirRadioH As Controles_VTrioHorizontalRadio
        IndiceI = 0
        Do
            Ctl = V_WebFonction.VirWebControle(Me.CadreInfo, "RadioH", IndiceI)
            If Ctl Is Nothing Then
                Exit Do
            End If
            NumInfo = CInt(Strings.Right(Ctl.ID, 2))
            VirRadioH = CType(Ctl, Controles_VTrioHorizontalRadio)
            VirRadioH.RadioGaucheCheck = False
            VirRadioH.RadioCentreCheck = False
            VirRadioH.RadioDroiteCheck = False
            If CacheDonnee(NumInfo) IsNot Nothing Then
                If CacheDonnee(NumInfo).ToString <> "" Then
                    Select Case CacheDonnee(NumInfo).ToString
                        Case Is = VirRadioH.RadioGaucheText
                            VirRadioH.RadioGaucheCheck = True
                        Case Is = VirRadioH.RadioCentreText
                            VirRadioH.RadioCentreCheck = True
                        Case Is = VirRadioH.RadioDroiteText
                            VirRadioH.RadioDroiteCheck = True
                    End Select
                End If
            End If
            VirRadioH.V_SiEnLectureSeule = SiEnLecture
            VirRadioH.V_SiAutoPostBack = Not SiEnLecture
            If SiEnLecture = False Then
                If UtiSession.SiAccesGRH = True Then
                    VirRadioH.V_SiEnLectureSeule = False
                    VirRadioH.V_SiAutoPostBack = True
                Else
                    VirRadioH.V_SiEnLectureSeule = True
                End If
            End If
            IndiceI += 1
        Loop

    End Sub

    Protected Sub CommandeNew_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles CommandeNew.Click
        Call InitialiserControles()
        Call V_CommandeNewFiche()
        CadreCmdOK.Visible = True
    End Sub

    Protected Sub CommandeSupp_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles CommandeSupp.Click
        Dim CacheDonnee As List(Of String) = V_CacheMaj
        If CacheDonnee Is Nothing Then
            Exit Sub
        End If
        If CacheDonnee(5) IsNot Nothing Then
            If CacheDonnee(5).ToString <> "" Then
                Exit Sub
            End If
        End If
        Call V_CommandeSuppFiche()
    End Sub

    Public WriteOnly Property RetourDialogueSupp(ByVal Cmd As String) As String
        Set(ByVal value As String)
            V_RetourDialogueSupp(Cmd) = value
            Call ActualiserListe()
        End Set
    End Property

    Protected Sub CommandeOK_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles CommandeOK.Click
        Dim CacheDonnee As List(Of String) = V_CacheMaj
        If CacheDonnee Is Nothing Then
            Exit Sub
        End If
        If CacheDonnee(5) IsNot Nothing Then
            If CacheDonnee(5).ToString <> "" And CacheDonnee(5).ToString <> V_WebFonction.ViRhDates.DateduJour Then
                Exit Sub
            End If
        End If
        Dim UtiSession As Virtualia.Net.Session.SessionModule = CType(V_WebFonction.PointeurUtilisateur.V_PointeurSessionModule, Virtualia.Net.Session.SessionModule)
        If UtiSession Is Nothing Then
            Exit Sub
        End If
        If UtiSession.SiAccesGRH = True Then
            CacheDonnee(5) = V_WebFonction.ViRhDates.DateduJour
        End If
        Call V_MajFiche()
        Call ActualiserListe()
        CadreCmdOK.Visible = False
    End Sub

    Private Sub InfoD_ValeurChange(sender As Object, e As Virtualia.Systeme.Evenements.DonneeChangeEventArgs) Handles InfoD00.ValeurChange

        Dim CacheDonnee As List(Of String) = V_CacheMaj
        If CacheDonnee Is Nothing Then
            Exit Sub
        End If
        Dim NumInfo As Integer = CInt(Strings.Right(CType(sender, VCoupleEtiDate).ID, 2))

        If CacheDonnee(NumInfo) Is Nothing Then
            CType(sender, VCoupleEtiDate).DonBackColor = V_WebFonction.CouleurMaj
            CadreCmdOK.Visible = True
        End If
        If CacheDonnee(NumInfo).ToString = e.Valeur Then
            Exit Sub
        End If
        If V_WebFonction.ViRhDates.DateTypee(e.Valeur).DayOfWeek = DayOfWeek.Saturday Then
            Call V_ValeurChange(NumInfo, e.Valeur)
            Call V_ValeurChange(1, "SAMEDI")
            CType(sender, VCoupleEtiDate).DonBackColor = V_WebFonction.CouleurMaj
            CadreCmdOK.Visible = True
            Exit Sub
        End If
        If V_WebFonction.ViRhDates.DateTypee(e.Valeur).DayOfWeek = DayOfWeek.Sunday Then
            Call V_ValeurChange(NumInfo, e.Valeur)
            Call V_ValeurChange(1, "DIMANCHE")
            CType(sender, VCoupleEtiDate).DonBackColor = V_WebFonction.CouleurMaj
            CadreCmdOK.Visible = True
            Exit Sub
        End If
        If V_WebFonction.ViRhDates.SiJourFerie(e.Valeur) = True Then
            Call V_ValeurChange(NumInfo, e.Valeur)
            Call V_ValeurChange(1, "JOUR FERIE")
            CType(sender, VCoupleEtiDate).DonBackColor = V_WebFonction.CouleurMaj
            CadreCmdOK.Visible = True
            Exit Sub
        End If
        CType(sender, VCoupleEtiDate).DonText = ""
    End Sub

    Protected Sub InfoVerticale_ValeurChange(ByVal sender As Object, ByVal e As Systeme.Evenements.DonneeChangeEventArgs) Handles InfoV03.ValeurChange, InfoV06.ValeurChange
        Dim CacheDonnee As List(Of String) = V_CacheMaj
        Dim NumInfo As Integer = CInt(Strings.Right(CType(sender, Controles_VCoupleVerticalEtiDonnee).ID, 2))
        If CacheDonnee IsNot Nothing Then
            If CacheDonnee(NumInfo) Is Nothing Then
                CType(sender, Controles_VCoupleVerticalEtiDonnee).DonBackColor = V_WebFonction.CouleurMaj
                CadreCmdOK.Visible = True
            ElseIf CacheDonnee(NumInfo).ToString <> e.Valeur Then
                CType(sender, Controles_VCoupleVerticalEtiDonnee).DonBackColor = V_WebFonction.CouleurMaj
                CadreCmdOK.Visible = True
            End If
            Call V_ValeurChange(NumInfo, e.Valeur)
        End If
    End Sub

    Protected Sub RadioH_ValeurChange(ByVal sender As Object, ByVal e As Virtualia.Systeme.Evenements.DonneeChangeEventArgs) Handles RadioH04.ValeurChange
        Dim CacheDonnee As List(Of String) = V_CacheMaj
        Dim NumInfo As Integer = CInt(Strings.Right(CType(sender, Controles_VTrioHorizontalRadio).ID, 2))
        If CacheDonnee IsNot Nothing Then
            CadreCmdOK.Visible = True
            Call V_ValeurChange(NumInfo, e.Valeur)
            Call V_ValeurChange(5, V_WebFonction.ViRhDates.DateduJour)
        End If
    End Sub

    Protected Overrides Sub Page_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init
        V_Objet(TypeListe.SiListeGrid) = WsNumObjet
        V_NomTableSgbd = WsNomTable
        V_Fiche = WsFiche
        ListeGrille.Centrage_Colonne(0) = 1
        ListeGrille.Centrage_Colonne(1) = 1
        ListeGrille.Centrage_Colonne(2) = 1
        ListeGrille.Centrage_Colonne(3) = 1
        ListeGrille.Centrage_Colonne(4) = 1
        ListeGrille.Centrage_Colonne(5) = 1

        ''**********AKR 18/07/2017
        If V_NomUtilisateur = "" Then
            V_NomUtilisateur = Request.QueryString("Nomuser")
        End If

        If CType(V_WebFonction.PointeurUtilisateur.V_PointeurSessionModule, Virtualia.Net.Session.SessionModule).SiAccesGRH = True Then
            '***********AKR
            If DropDownMoisAnnee IsNot Nothing Then

                If DropDownMoisAnnee.Items.Count = 0 Then
                    Dim AnneeDebut As Integer = Year(Now)
                    '******AKR
                    Dim MoisDebut As Integer = Month(Now)
                    Dim Mois As Integer
                    If MoisDebut = 0 Then
                        AnneeDebut -= 1
                        MoisDebut = 12
                    End If
                    For Mois = MoisDebut To 1 Step -1
                        DropDownMoisAnnee.Items.Add(New ListItem(V_WebFonction.ViRhDates.MoisEnClair(Mois) & Strings.Space(1) & AnneeDebut, AnneeDebut & Strings.Format(Mois, "00")))
                    Next
                    For Mois = 12 To 1 Step -1
                        DropDownMoisAnnee.Items.Add(New ListItem(V_WebFonction.ViRhDates.MoisEnClair(Mois) & Strings.Space(1) & (AnneeDebut - 1), (AnneeDebut - 1) & Strings.Format(Mois, "00")))
                    Next
                End If
            End If
            LabelEtatIndividuel.Visible = True
            DropDownMoisAnnee.Visible = True
            EtiSelMoisAnnee.Visible = True
            CommandeImprimer.Visible = True
        Else
            LabelEtatIndividuel.Visible = False
            DropDownMoisAnnee.Visible = False
            EtiSelMoisAnnee.Visible = False
            CommandeImprimer.Visible = False
        End If

    End Sub

    Protected Overrides Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        Call LireLaFiche()

    End Sub

    Private Sub InitialiserControles()
        Dim Ctl As Control
        Dim VirControle As Controles_VCoupleEtiDonnee
        Dim IndiceI As Integer = 0
        Do
            Ctl = V_WebFonction.VirWebControle(Me.CadreInfo, "InfoH", IndiceI)
            If Ctl Is Nothing Then
                Exit Do
            End If
            VirControle = CType(Ctl, Controles_VCoupleEtiDonnee)
            VirControle.DonBackColor = Drawing.Color.White
            VirControle.DonText = ""
            IndiceI += 1
        Loop

        Dim VirVertical As Controles_VCoupleVerticalEtiDonnee
        IndiceI = 0
        Do
            Ctl = V_WebFonction.VirWebControle(Me.CadreInfo, "InfoV", IndiceI)
            If Ctl Is Nothing Then
                Exit Do
            End If
            VirVertical = CType(Ctl, Controles_VCoupleVerticalEtiDonnee)
            VirVertical.DonBackColor = Drawing.Color.White
            VirVertical.DonText = ""
            IndiceI += 1
        Loop

        Dim VirDonneeDate As VCoupleEtiDate
        IndiceI = 0
        Do
            Ctl = V_WebFonction.VirWebControle(Me.CadreInfo, "InfoD", IndiceI)
            If Ctl Is Nothing Then
                Exit Do
            End If
            VirDonneeDate = CType(Ctl, VCoupleEtiDate)
            VirDonneeDate.DonBackColor = Drawing.Color.White
            VirDonneeDate.DonText = ""
            VirDonneeDate.V_SiEnLectureSeule = Me.V_SiEnLectureSeule
            IndiceI += 1
        Loop
    End Sub

    Private Sub CommandeHS_Click(sender As Object, e As EventArgs) Handles CommandeHS.Click
        Dim Evenement As Virtualia.Systeme.Evenements.DonneeChangeEventArgs
        Evenement = New Virtualia.Systeme.Evenements.DonneeChangeEventArgs("")
        ReponseRetour(Evenement)
    End Sub
    Protected Sub CommandeImprimer_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles CommandeImprimer.Click
        Dim AstreinteExiste As Boolean = False
        Dim DossierPER As Virtualia.Net.WebAppli.SelfV4.DossierPersonne
        Dim UtiSession As Virtualia.Net.Session.SessionModule = CType(V_WebFonction.PointeurUtilisateur.V_PointeurSessionModule, Virtualia.Net.Session.SessionModule)
        If UtiSession Is Nothing Then
            Exit Sub
        End If
        DossierPER = UtiSession.DossierPER
        If DossierPER Is Nothing Then
            Exit Sub
        End If
        Dim Annee As Integer = CInt(Strings.Left(DropDownMoisAnnee.SelectedItem.Value, 4))
        Dim Mois As Integer = CInt(Strings.Right(DropDownMoisAnnee.SelectedItem.Value, 2))
        UtiSession.DossierPER.AnneeSelection = Annee
        UtiSession.DossierPER.MoisSelection = Mois
        UtiSession.DossierPER.V_ListeFiches = V_ListeFiches
        If V_ListeFiches IsNot Nothing Then
            For IndiceAstreinte = 0 To V_ListeFiches.Count - 1
                Dim moisAstreinte = Strings.Right(Strings.Left(UtiSession.DossierPER.V_ListeFiches(IndiceAstreinte), 5), 2)
                If CInt(moisAstreinte) = Mois Then
                    Dim TableauData(0) As String
                    TableauData = Strings.Split(UtiSession.DossierPER.V_ListeFiches(IndiceAstreinte), VI.Tild)
                    If TableauData(2) = "Oui" Then
                        AstreinteExiste = True
                    End If
                End If
            Next

            If AstreinteExiste = True Then
                Response.Redirect("~/Controles/HeuresSupp/FrmImpressionAsstreintes.aspx?IDVirtualia=" & UtiSession.VParent.V_IDSession)
            Else
                Exit Sub
            End If
        Else
            Exit Sub
        End If
    End Sub
    Public Property V_NomUtilisateur As String
        Get
            If Me.ViewState(WsNomStateUti) Is Nothing Then
                Return ""
            End If
            WsCacheUti = CType(Me.ViewState(WsNomStateUti), ArrayList)
            If WsCacheUti IsNot Nothing Then
                If WsCacheUti(0) IsNot Nothing Then
                    Return WsCacheUti(0).ToString
                Else
                    Return ""
                End If
            Else
                Return ""
            End If
        End Get
        Set(ByVal value As String)
            If Me.ViewState(WsNomStateUti) IsNot Nothing Then
                Me.ViewState.Remove(WsNomStateUti)
            End If
            WsCacheUti = New ArrayList
            WsCacheUti.Add(value)
            Me.ViewState.Add(WsNomStateUti, WsCacheUti)
        End Set
    End Property
End Class