﻿Option Strict On
Option Explicit On
Option Compare Text
Imports Virtualia.Systeme.Evenements
Imports VI = Virtualia.Systeme.Constantes
Public Class PER_ACTIVITE_JOUR
    Inherits System.Web.UI.UserControl
    Public Delegate Sub MsgDialog_MsgEventHandler(ByVal sender As Object, ByVal e As Virtualia.Systeme.Evenements.MessageSaisieEventArgs)
    Public Event MessageDialogue As MsgDialog_MsgEventHandler
    Public Delegate Sub Retour_MsgEventHandler(ByVal sender As Object, ByVal e As Virtualia.Systeme.Evenements.MessageRetourEventArgs)
    Public Event ValeurRetour As Retour_MsgEventHandler
    Private WebFct As Virtualia.Net.Controles.WebFonctions
    Private WsNomStateCache As String = "VPerActivite"
    Private WsCtl_Cache As Virtualia.Net.Controles.CacheControleActivite

    Protected Overridable Sub V_Retour(ByVal e As Virtualia.Systeme.Evenements.MessageRetourEventArgs)
        RaiseEvent ValeurRetour(Me, e)
    End Sub

    Protected Overridable Sub V_MessageDialog(ByVal e As Virtualia.Systeme.Evenements.MessageSaisieEventArgs)
        RaiseEvent MessageDialogue(Me, e)
    End Sub

    Private ReadOnly Property V_WebFonction As Virtualia.Net.Controles.WebFonctions
        Get
            If WebFct Is Nothing Then
                WebFct = New Virtualia.Net.Controles.WebFonctions(Me, 0)
            End If
            Return WebFct
        End Get
    End Property

    Private Property CacheVirControle As Virtualia.Net.Controles.CacheControleActivite
        Get
            If Me.ViewState(WsNomStateCache) IsNot Nothing Then
                Return CType(Me.ViewState(WsNomStateCache), Virtualia.Net.Controles.CacheControleActivite)
            End If
            Dim NewCache As Virtualia.Net.Controles.CacheControleActivite
            NewCache = New Virtualia.Net.Controles.CacheControleActivite
            Return NewCache
        End Get
        Set(value As Virtualia.Net.Controles.CacheControleActivite)
            If Me.ViewState(WsNomStateCache) IsNot Nothing Then
                Me.ViewState.Remove(WsNomStateCache)
            End If
            If value Is Nothing Then
                Exit Property
            End If
            Me.ViewState.Add(WsNomStateCache, value)
        End Set
    End Property

    Public Sub Initialiser()
        CacheVirControle = Nothing
        VComboSemaines.V_Liste("") = ""
        CellSaisie.Visible = False
    End Sub

    Private Sub Page_PreRender(sender As Object, e As EventArgs) Handles Me.PreRender
        Dim TableauW(0) As String
        Dim DateW As String
        Dim SiChoixOK As Boolean
        Dim Chaine As String

        If CType(V_WebFonction.PointeurUtilisateur.V_PointeurSessionModule, Virtualia.Net.Session.SessionModule).SiAccesGRH = True Then
            CellRetour.Visible = True
        End If
        WsCtl_Cache = CacheVirControle
        If CType(V_WebFonction.PointeurUtilisateur.V_PointeurSessionModule, Virtualia.Net.Session.SessionModule).SiAccesManager = True Then
            RadioSemaine.RadioGaucheVisible = False
            If WsCtl_Cache.Choix = "" Then
                RadioSemaine.RadioDroiteCheck = True
                WsCtl_Cache.Choix = RadioSemaine.RadioDroiteText
            End If
        End If
        If WsCtl_Cache.DateValeur = "" Then
            Dim DateDebut As String = "01/12/" & Year(Now) - 1
            If Now.Month < 3 Then
                DateDebut = "01/10/" & Year(Now) - 1
            End If
            SiChoixOK = FaireListeSemaines(DateDebut, V_WebFonction.ViRhDates.DateduJour, WsCtl_Cache.Choix)
            Call FaireListesDates(DateDebut, V_WebFonction.ViRhDates.DateduJour)
            If SiChoixOK = False Then
                RadioSemaine.RadioGaucheCheck = True
                If CType(V_WebFonction.PointeurUtilisateur.V_PointeurSessionModule, Virtualia.Net.Session.SessionModule).SiAccesManager = True Then
                    DateDebut = V_WebFonction.ViRhDates.DateduJour
                End If
                SiChoixOK = FaireListeSemaines(DateDebut, V_WebFonction.ViRhDates.DateduJour, "")
            End If
            WsCtl_Cache.DateValeur = V_WebFonction.ViRhDates.Semaine_Date_Debut(V_WebFonction.ViRhDates.DateduJour)
            WsCtl_Cache = CacheVirControle
        End If
        TableauW = Strings.Split(WsCtl_Cache.ValeurSelection, " - ", -1)
        If VComboSemaines.EtiText = "" And WsCtl_Cache.ValeurSelection <> "" Then
            VComboSemaines.LstText = WsCtl_Cache.ValeurSelection
        End If
        LabelTitreSemaine.Text = TableauW(0)
        If TableauW.Count > 1 Then
            LabelDateActivite.Text = TableauW(1)
        End If
        DateW = WsCtl_Cache.DateValeur
        If DateW = "" Then
            DateW = V_WebFonction.ViRhDates.Semaine_Date_Debut(V_WebFonction.ViRhDates.DateduJour)
        End If
        LabelDateLundiActivite.Text = Strings.Left(DateW, 5)
        LabelDateMardiActivite.Text = Strings.Left(V_WebFonction.ViRhDates.DateTypee(DateW).AddDays(1).ToShortDateString, 5)
        LabelDateMercrediActivite.Text = Strings.Left(V_WebFonction.ViRhDates.DateTypee(DateW).AddDays(2).ToShortDateString, 5)
        LabelDateJeudiActivite.Text = Strings.Left(V_WebFonction.ViRhDates.DateTypee(DateW).AddDays(3).ToShortDateString, 5)
        LabelDateVendrediActivite.Text = Strings.Left(V_WebFonction.ViRhDates.DateTypee(DateW).AddDays(4).ToShortDateString, 5)
        LabelDateSamediActivite.Text = Strings.Left(V_WebFonction.ViRhDates.DateTypee(DateW).AddDays(5).ToShortDateString, 5)
        LabelDateDimancheActivite.Text = Strings.Left(V_WebFonction.ViRhDates.DateTypee(DateW).AddDays(6).ToShortDateString, 5)

        Chaine = CType(V_WebFonction.PointeurUtilisateur.V_PointeurSessionModule, Virtualia.Net.Session.SessionModule).DossierPER.Nom & Strings.Space(1) & CType(V_WebFonction.PointeurUtilisateur.V_PointeurSessionModule, Virtualia.Net.Session.SessionModule).DossierPER.Prenom & "&nbsp;&nbsp"
        'Chaine &= Strings.Space(5) & "-- Totaux" & "&nbsp;&nbsp"
        Etiquette.Text = "Activités travaillées - " & Chaine
        EtiDossier.Text = Chaine

        CellLecture.Visible = True
        CadreLecture.Date_Valeur = DateW
        If CellSaisie.Visible = True Then
            CadreSaisie.Date_Valeur = DateW
        End If
        Call FairePlanning(DateW)
        Call FaireEtatdesSaisies()
        Call FaireRecapitulatif()
    End Sub

    Private Sub FairePlanning(ByVal DateDebut As String)
        Dim LstCalend As List(Of Virtualia.Net.Controles.ItemJourCalendrier) = Nothing
        Dim ListeActivites As List(Of Virtualia.TablesObjet.ShemaPER.PER_ACTIVITE_JOUR) = Nothing
        Dim ListeJours As List(Of Virtualia.TablesObjet.ShemaPER.PER_ACTIVITE_JOUR) = Nothing
        Dim LstAbs As List(Of Virtualia.Net.Controles.ItemJourCalendrier) = Nothing
        Dim FicheAbs As Virtualia.Net.Controles.ItemJourCalendrier = Nothing
        Dim DateW As String
        Dim IndiceJ As Integer
        Dim Couleur As Drawing.Color
        Dim SiDemiJour As Boolean
        Dim SiSaisie_Valide As Boolean

        If DateDebut = "" Then
            Exit Sub
        End If
        ListeActivites = CType(V_WebFonction.PointeurUtilisateur.V_PointeurSessionModule, Virtualia.Net.Session.SessionModule).DossierPER.VListeActivites(DateDebut, V_WebFonction.ViRhDates.CalcDateMoinsJour(DateDebut, "6", "0"))
        LabelPlanningPM.Text = ""
        CadreCmdNew.Visible = True
        If ListeActivites IsNot Nothing Then
            If ListeActivites.Item(0).Date_Visa <> "" Then
                LabelPlanningPM.Text = "Validé le " & ListeActivites.Item(0).Date_Visa
                If ListeActivites.Item(0).Visa <> "" Then
                    LabelPlanningPM.Text &= " par " & ListeActivites.Item(0).Visa
                End If
                CadreCmdNew.Visible = False
            End If
        End If
        LstCalend = CType(V_WebFonction.PointeurUtilisateur.V_PointeurSessionModule, Virtualia.Net.Session.SessionModule).DossierPER.VDossier_Calendrier(DateDebut, V_WebFonction.ViRhDates.CalcDateMoinsJour(DateDebut, "4", "0"))

        SiSaisie_Valide = True
        IndiceJ = 0
        DateW = DateDebut
        Do
            VBackColor(IndiceJ, "CalAM") = V_WebFonction.PointeurGlobal.InstanceCouleur.Item(0).Couleur_Web
            VBackColor(IndiceJ, "CalPM") = V_WebFonction.PointeurGlobal.InstanceCouleur.Item(0).Couleur_Web
            VBorderColor(IndiceJ, "CalAM") = Drawing.Color.LightGray
            VBorderColor(IndiceJ, "CalPM") = Drawing.Color.LightGray
            VDemiJourToolTip(IndiceJ, "CalAM") = ""
            VDemiJourToolTip(IndiceJ, "CalPM") = ""
            LstAbs = Nothing
            FicheAbs = Nothing
            SiDemiJour = False
            '** Inclure Le prévisionnbel ajusté et les absences
            If LstCalend IsNot Nothing Then
                LstAbs = (From FicheCal In LstCalend Where FicheCal.Date_Valeur_ToDate = V_WebFonction.ViRhDates.DateTypee(DateW)).ToList
            End If
            If LstAbs IsNot Nothing AndAlso LstAbs.Count > 0 Then
                For Each FicheAbs In LstAbs
                    Select Case FicheAbs.Caracteristique
                        Case VI.NumeroPlage.Jour_Formation, VI.NumeroPlage.Plage1_Formation, VI.NumeroPlage.Plage2_Formation
                            Couleur = V_WebFonction.PointeurGlobal.InstanceCouleur.Item(3).Couleur_Web
                        Case Else
                            Couleur = V_WebFonction.PointeurGlobal.InstanceCouleur.Item(V_WebFonction.PointeurGlobal.InstanceCouleur.IndexCouleurPlanning(FicheAbs.Intitule)).Couleur_Web
                    End Select
                    Select Case FicheAbs.Caracteristique
                        Case VI.NumeroPlage.Jour_Absence, VI.NumeroPlage.Jour_Formation
                            VBackColor(IndiceJ, "CalAM") = Couleur
                            VDemiJourToolTip(IndiceJ, "CalAM") = FicheAbs.Intitule
                            VBackColor(IndiceJ, "CalPM") = Couleur
                            VDemiJourToolTip(IndiceJ, "CalPM") = FicheAbs.Intitule
                            SiDemiJour = False
                        Case VI.NumeroPlage.Plage1_Absence, VI.NumeroPlage.Plage1_Formation
                            VBackColor(IndiceJ, "CalAM") = Couleur
                            VDemiJourToolTip(IndiceJ, "CalAM") = FicheAbs.Intitule
                            If SiDemiJour = False Then
                                SiDemiJour = True
                            Else
                                SiDemiJour = False
                            End If
                            If FicheAbs.Intitule = "" And FicheAbs.LibelleComplementaire = "CYCLE" Then
                                If LstAbs.Count = 1 Then
                                    SiDemiJour = False
                                End If
                            End If
                        Case VI.NumeroPlage.Plage2_Absence, VI.NumeroPlage.Plage2_Formation
                            VBackColor(IndiceJ, "CalPM") = Couleur
                            VDemiJourToolTip(IndiceJ, "CalPM") = FicheAbs.Intitule
                            If SiDemiJour = False Then
                                SiDemiJour = True
                            Else
                                SiDemiJour = False
                            End If
                            If FicheAbs.Intitule = "" And FicheAbs.LibelleComplementaire = "CYCLE" Then
                                If LstAbs.Count = 1 Then
                                    SiDemiJour = False
                                End If
                            End If
                    End Select
                Next
            End If
            If V_WebFonction.ViRhDates.SiJourOuvre(DateW, True) = False Then
                VBackColor(IndiceJ, "CalAM") = V_WebFonction.PointeurGlobal.InstanceCouleur.Item(1).Couleur_Web
                VBackColor(IndiceJ, "CalPM") = V_WebFonction.PointeurGlobal.InstanceCouleur.Item(1).Couleur_Web
            End If
            If V_WebFonction.ViRhDates.SiJourFerie(DateW) = True Then
                VBorderColor(IndiceJ, "CalAM") = Drawing.Color.Red
                VBorderColor(IndiceJ, "CalPM") = Drawing.Color.Red
            End If
            '** Totaux **
            If ListeActivites IsNot Nothing Then
                ListeJours = ListeActivites.FindAll(Function(Recherche) Recherche.Date_Valeur_ToDate = V_WebFonction.ViRhDates.DateTypee(DateW))
            End If
            If ListeJours Is Nothing OrElse ListeJours.Count = 0 Then
                If IndiceJ < 5 Then
                    If (LstAbs Is Nothing OrElse LstAbs.Count = 0) And V_WebFonction.ViRhDates.SiJourFerie(DateW) = False Then
                        SiSaisie_Valide = False
                        VTotal(IndiceJ, SiDemiJour, True) = 0
                    ElseIf (LstAbs IsNot Nothing AndAlso LstAbs.Count > 0) And V_WebFonction.ViRhDates.SiJourFerie(DateW) = False And SiDemiJour = True Then
                        SiSaisie_Valide = False
                        VTotal(IndiceJ, SiDemiJour, True) = 0
                    Else
                        VTotal(IndiceJ, SiDemiJour, False) = 0
                    End If
                Else
                    VTotal(IndiceJ, SiDemiJour, False) = 0
                End If
            Else
                Dim ZCalc As Double = 0
                For Each Fiche As Virtualia.TablesObjet.ShemaPER.PER_ACTIVITE_JOUR In ListeJours
                    ZCalc += Fiche.Pourcentage_Repartition
                Next
                Select Case SiDemiJour
                    Case True
                        If ZCalc > 0 And ZCalc <> 50 Then
                            SiSaisie_Valide = False
                        End If
                    Case False
                        If ZCalc > 0 And ZCalc <> 100 Then
                            SiSaisie_Valide = False
                        End If
                End Select
                VTotal(IndiceJ, SiDemiJour) = CInt(ZCalc)
            End If
            '************
            DateW = V_WebFonction.ViRhDates.CalcDateMoinsJour(DateW, "1", "0")
            IndiceJ += 1
            If IndiceJ > 6 Then
                Exit Do
            End If
        Loop
        CellCmdSigner.Visible = False
        If CType(V_WebFonction.PointeurUtilisateur.V_PointeurSessionModule, Virtualia.Net.Session.SessionModule).SiAccesManager = True Then
            If ListeActivites IsNot Nothing Then
                If ListeActivites.Item(0).Date_Visa = "" Then
                    CellCmdSigner.Visible = SiSaisie_Valide
                    If SiSaisie_Valide = True And CType(V_WebFonction.PointeurUtilisateur.V_PointeurSessionModule, Virtualia.Net.Session.SessionModule).SiAccesGRH = False Then
                        CellCmdOK.Visible = False
                    End If
                End If
            End If
        End If
        If SiSaisie_Valide = False And CadreCmdNew.Visible = True Then
            Call AfficherNouveau()
            CadreSaisie.Date_Valeur = DateDebut
        ElseIf SiSaisie_Valide = True And WsCtl_Cache.Tag <> "NEW" Then
            CellSaisie.Visible = False
        Else
            WsCtl_Cache = CacheVirControle
            WsCtl_Cache.Tag = ""
            CacheVirControle = WsCtl_Cache
        End If
    End Sub

    Private Sub FaireEtatdesSaisies()
        Dim LstCalend As List(Of Virtualia.Net.Controles.ItemJourCalendrier) = Nothing
        Dim ListeActivites As List(Of Virtualia.TablesObjet.ShemaPER.PER_ACTIVITE_JOUR) = Nothing
        Dim ListeJours As List(Of Virtualia.TablesObjet.ShemaPER.PER_ACTIVITE_JOUR) = Nothing
        Dim LstAbs As List(Of Virtualia.Net.Controles.ItemJourCalendrier) = Nothing
        Dim FicheAbs As Virtualia.Net.Controles.ItemJourCalendrier = Nothing
        Dim LibColonnes As List(Of String)
        Dim LstDonnees As List(Of String)
        Dim ZCalc As Double
        Dim DateDebut As String = Session.Item("Debut").ToString
        Dim DateFin As String = Session.Item("Fin").ToString
        Dim DateWSemaine As String
        Dim DateWJour As String
        Dim LstSemaines As List(Of KeyValuePair(Of Integer, String))
        Dim IndiceWeek As Integer
        Dim IndiceJ As Integer
        Dim SiDemiJour As Boolean
        Dim SiSaisie_Valide As Boolean

        LibColonnes = New List(Of String)
        LibColonnes.Add("Semaine")
        LibColonnes.Add("Période")
        LibColonnes.Add("Statut")
        LibColonnes.Add("Clef")
        ListeStatutSemaine.V_LibelColonne = LibColonnes

        LstSemaines = New List(Of KeyValuePair(Of Integer, String))
        IndiceWeek = V_WebFonction.ViRhDates.Semaine_Numero(DateDebut)
        DateWSemaine = V_WebFonction.ViRhDates.Semaine_Date_Debut(IndiceWeek, DateDebut)
        Do
            IndiceWeek = V_WebFonction.ViRhDates.Semaine_Numero(DateWSemaine)
            If IndiceWeek > 1 And V_WebFonction.ViRhDates.DateTypee(DateWSemaine).DayOfWeek <> DayOfWeek.Monday Then
                DateWSemaine = V_WebFonction.ViRhDates.Semaine_Date_Debut(IndiceWeek, DateWSemaine)
            ElseIf IndiceWeek = 1 Then
                DateWSemaine = V_WebFonction.ViRhDates.Semaine_Date_Debut(DateWSemaine)
            End If
            LstCalend = CType(V_WebFonction.PointeurUtilisateur.V_PointeurSessionModule, Virtualia.Net.Session.SessionModule).DossierPER.VDossier_Calendrier(DateWSemaine, V_WebFonction.ViRhDates.CalcDateMoinsJour(DateWSemaine, "4", "0"))
            ZCalc = 5
            DateWJour = DateWSemaine
            IndiceJ = 0
            Do
                '** Inclure Le prévisionnbel ajusté et les absences
                If LstCalend IsNot Nothing AndAlso LstCalend.Count > 0 Then
                    For Each FicheAbs In LstCalend
                        If FicheAbs.Date_Valeur = DateWJour Then
                            Select Case FicheAbs.Caracteristique
                                Case VI.NumeroPlage.Jour_Absence, VI.NumeroPlage.Jour_Formation
                                    ZCalc -= 1
                                    Exit For
                                Case VI.NumeroPlage.Plage1_Absence, VI.NumeroPlage.Plage1_Formation
                                    ZCalc -= 1 / 2
                                    If FicheAbs.Intitule = "" And FicheAbs.LibelleComplementaire = "CYCLE" Then
                                        If LstCalend.Count = 1 Then
                                            ZCalc -= 1 / 2
                                        End If
                                    End If
                                Case VI.NumeroPlage.Plage2_Absence, VI.NumeroPlage.Plage2_Formation
                                    ZCalc -= 1 / 2
                                    If FicheAbs.Intitule = "" And FicheAbs.LibelleComplementaire = "CYCLE" Then
                                        If LstCalend.Count = 1 Then
                                            ZCalc -= 1 / 2
                                        End If
                                    End If
                            End Select
                        End If
                    Next
                End If
                DateWJour = V_WebFonction.ViRhDates.CalcDateMoinsJour(DateWJour, "1", "0")
                IndiceJ += 1
                If IndiceJ > 6 Then
                    Exit Do
                End If
            Loop
            If ZCalc > 0 Then
                LstSemaines.Add(New KeyValuePair(Of Integer, String)(IndiceWeek, DateWSemaine))
            End If
            DateWSemaine = V_WebFonction.ViRhDates.CalcDateMoinsJour(DateWSemaine, "7", "0")
            If V_WebFonction.ViRhDates.DateTypee(DateWSemaine) > V_WebFonction.ViRhDates.DateTypee(DateFin) Then
                Exit Do
            End If
        Loop
        If LstSemaines.Count = 0 Then
            Exit Sub
        End If
        DateDebut = LstSemaines.Item(0).Value
        DateFin = V_WebFonction.ViRhDates.CalcDateMoinsJour(LstSemaines.Item(LstSemaines.Count - 1).Value, "7", "0")
        ListeActivites = CType(V_WebFonction.PointeurUtilisateur.V_PointeurSessionModule, Virtualia.Net.Session.SessionModule).DossierPER.VListeActivites(DateDebut, DateFin)
        If ListeActivites Is Nothing Then
            LstDonnees = New List(Of String)
            For Each Semaine In LstSemaines
                LstDonnees.Add(Semaine.Key & VI.Tild & Semaine.Value & " au " & V_WebFonction.ViRhDates.Semaine_Date_Fin(Semaine.Value) & VI.Tild & "Non saisie" & VI.Tild & Semaine.Key & VI.SigneBarre)
            Next
            ListeStatutSemaine.V_Liste = LstDonnees
            Exit Sub
        End If
        '*** Statut
        LstDonnees = New List(Of String)
        For Each Semaine In LstSemaines
            ListeJours = CType(V_WebFonction.PointeurUtilisateur.V_PointeurSessionModule, Virtualia.Net.Session.SessionModule).DossierPER.VListeActivites(Semaine.Value, V_WebFonction.ViRhDates.CalcDateMoinsJour(Semaine.Value, "6", "0"))
            If ListeJours Is Nothing OrElse ListeJours.Count = 0 Then
                LstDonnees.Add(Semaine.Key & VI.Tild & Semaine.Value & " au " & V_WebFonction.ViRhDates.Semaine_Date_Fin(Semaine.Value) & VI.Tild & "Non saisie" & VI.Tild & Semaine.Key & VI.SigneBarre)
            ElseIf ListeJours.Item(0).Date_Visa <> "" Then
                LstDonnees.Add(Semaine.Key & VI.Tild & Semaine.Value & " au " & V_WebFonction.ViRhDates.Semaine_Date_Fin(Semaine.Value) & VI.Tild & "Validée" & VI.Tild & Semaine.Key & VI.SigneBarre)
            Else
                DateWJour = Semaine.Value
                SiSaisie_Valide = True
                IndiceJ = 0
                Do
                    FicheAbs = Nothing
                    SiDemiJour = False
                    '** Inclure Le prévisionnbel ajusté et les absences
                    If LstCalend IsNot Nothing Then
                        LstAbs = (From FicheCal In LstCalend Where FicheCal.Date_Valeur_ToDate = V_WebFonction.ViRhDates.DateTypee(DateWJour)).ToList
                        If LstAbs IsNot Nothing AndAlso LstAbs.Count > 0 Then
                            For Each FicheAbs In LstAbs
                                Select Case FicheAbs.Caracteristique
                                    Case VI.NumeroPlage.Jour_Absence, VI.NumeroPlage.Jour_Formation
                                        SiDemiJour = False
                                    Case VI.NumeroPlage.Plage1_Absence, VI.NumeroPlage.Plage1_Formation
                                        If SiDemiJour = False Then
                                            SiDemiJour = True
                                        Else
                                            SiDemiJour = False
                                        End If
                                        If FicheAbs.Intitule = "" And FicheAbs.LibelleComplementaire = "CYCLE" Then
                                            If LstAbs.Count = 1 Then
                                                SiDemiJour = False
                                            End If
                                        End If
                                    Case VI.NumeroPlage.Plage2_Absence, VI.NumeroPlage.Plage2_Formation
                                        If SiDemiJour = False Then
                                            SiDemiJour = True
                                        Else
                                            SiDemiJour = False
                                        End If
                                        If FicheAbs.Intitule = "" And FicheAbs.LibelleComplementaire = "CYCLE" Then
                                            If LstAbs.Count = 1 Then
                                                SiDemiJour = False
                                            End If
                                        End If
                                End Select
                            Next
                        End If
                    End If
                    ListeJours = ListeActivites.FindAll(Function(Recherche) Recherche.Date_Valeur_ToDate = V_WebFonction.ViRhDates.DateTypee(DateWJour))
                    If ListeJours Is Nothing OrElse ListeJours.Count = 0 Then
                        If IndiceJ < 5 Then
                            If (LstAbs Is Nothing OrElse LstAbs.Count = 0) And V_WebFonction.ViRhDates.SiJourFerie(DateWJour) = False Then
                                SiSaisie_Valide = False
                                Exit Do
                            ElseIf (LstAbs IsNot Nothing AndAlso LstAbs.Count > 0) And SiDemiJour = True Then
                                SiSaisie_Valide = False
                                Exit Do
                            End If
                        End If
                    Else
                        ZCalc = 0
                        For Each Fiche As Virtualia.TablesObjet.ShemaPER.PER_ACTIVITE_JOUR In ListeJours
                            ZCalc += Fiche.Pourcentage_Repartition
                        Next
                        Select Case SiDemiJour
                            Case True
                                If ZCalc > 0 And ZCalc <> 50 Then
                                    SiSaisie_Valide = False
                                    Exit Do
                                End If
                            Case False
                                If ZCalc > 0 And ZCalc <> 100 Then
                                    SiSaisie_Valide = False
                                    Exit Do
                                End If
                        End Select
                    End If
                    DateWJour = V_WebFonction.ViRhDates.CalcDateMoinsJour(DateWJour, "1", "0")
                    IndiceJ += 1
                    If IndiceJ > 6 Then
                        Exit Do
                    End If
                Loop
                Select Case SiSaisie_Valide
                    Case True
                        LstDonnees.Add(Semaine.Key & VI.Tild & Semaine.Value & " au " & V_WebFonction.ViRhDates.Semaine_Date_Fin(Semaine.Value) & VI.Tild & "Complète à valider" & VI.Tild & Semaine.Key & VI.SigneBarre)
                    Case False
                        LstDonnees.Add(Semaine.Key & VI.Tild & Semaine.Value & " au " & V_WebFonction.ViRhDates.Semaine_Date_Fin(Semaine.Value) & VI.Tild & "A compléter" & VI.Tild & Semaine.Key & VI.SigneBarre)
                End Select
            End If
        Next
        ListeStatutSemaine.Centrage_Colonne(0) = 1
        ListeStatutSemaine.Centrage_Colonne(1) = 1
        ListeStatutSemaine.Centrage_Colonne(2) = 1
        ListeStatutSemaine.V_Liste = LstDonnees
    End Sub

    Private Sub FaireRecapitulatif()
        Dim ListeActivites As List(Of Virtualia.TablesObjet.ShemaPER.PER_ACTIVITE_JOUR) = Nothing
        Dim ListeJours As List(Of Virtualia.TablesObjet.ShemaPER.PER_ACTIVITE_JOUR) = Nothing
        Dim LstPlanifie As List(Of Virtualia.Net.Controles.ItemJourCalendrier) = Nothing
        Dim LstAbs As List(Of Virtualia.Net.Controles.ItemJourCalendrier) = Nothing
        Dim FicheAbs As Virtualia.Net.Controles.ItemJourCalendrier = Nothing
        Dim ListeResultat As List(Of Virtualia.Net.WebAppli.SelfV4.RecapitulatifActivite)
        Dim LigneRecap As Virtualia.Net.WebAppli.SelfV4.RecapitulatifActivite
        Dim LibColonnes As List(Of String)
        Dim LstDonnees As List(Of String)
        Dim Chaine As String
        Dim RuptN1 As String = ""
        Dim RuptN2 As String = ""
        Dim ZCalc As Double
        Dim DateDebut As String = Session.Item("Debut").ToString
        Dim DateFin As String = Session.Item("Fin").ToString

        LibColonnes = New List(Of String)
        LibColonnes.Add("Projet")
        LibColonnes.Add("Etape")
        LibColonnes.Add("Total")
        LibColonnes.Add("%")
        LibColonnes.Add("Clef")
        ListeRecap.V_LibelColonne = LibColonnes

        ListeActivites = CType(V_WebFonction.PointeurUtilisateur.V_PointeurSessionModule, Virtualia.Net.Session.SessionModule).DossierPER.VListeActivites(DateDebut, DateFin)
        If ListeActivites Is Nothing Then
            ListeRecap.V_Liste = Nothing
            Exit Sub
        End If
        '** Recapitulatif
        ListeResultat = New List(Of Virtualia.Net.WebAppli.SelfV4.RecapitulatifActivite)
        ListeJours = (From Jour In ListeActivites Order By Jour.Activite Ascending, Jour.Tache_Etape Ascending).ToList
        ZCalc = 0
        For Each Jour As Virtualia.TablesObjet.ShemaPER.PER_ACTIVITE_JOUR In ListeJours
            If Jour.Activite & Jour.Tache_Etape <> RuptN1 & RuptN2 AndAlso RuptN1 <> "" Then
                LigneRecap = New Virtualia.Net.WebAppli.SelfV4.RecapitulatifActivite
                LigneRecap.Activite = RuptN1
                LigneRecap.Tache = RuptN2
                LigneRecap.Total = ZCalc
                ListeResultat.Add(LigneRecap)
                ZCalc = 0
            End If
            RuptN1 = Jour.Activite
            RuptN2 = Jour.Tache_Etape
            ZCalc += Jour.Pourcentage_Repartition
        Next
        LigneRecap = New Virtualia.Net.WebAppli.SelfV4.RecapitulatifActivite
        LigneRecap.Activite = RuptN1
        LigneRecap.Tache = RuptN2
        LigneRecap.Total = ZCalc
        ListeResultat.Add(LigneRecap)

        ZCalc = 0 'Total général
        For Each LigneRecap In ListeResultat
            ZCalc += LigneRecap.Total
        Next
        LstDonnees = New List(Of String)
        For Each LigneRecap In ListeResultat
            LigneRecap.Pourcentage = (LigneRecap.Total / ZCalc) * 100
            Chaine = LigneRecap.Activite & VI.Tild & LigneRecap.Tache & VI.Tild
            Chaine &= Strings.Format(LigneRecap.Total, "0.00") & VI.Tild
            Chaine &= Strings.Format(LigneRecap.Pourcentage, "0.00") & VI.Tild
            Chaine &= LigneRecap.Activite & LigneRecap.Tache & VI.SigneBarre
            LstDonnees.Add(Chaine)
        Next
        ListeRecap.Centrage_Colonne(2) = 1
        ListeRecap.Centrage_Colonne(3) = 1
        ListeRecap.V_Liste = LstDonnees
    End Sub

    Private Function FaireListeSemaines(ByVal DateDebut As String, ByVal Datefin As String, ByVal ChoixListe As String) As Boolean
        Dim ListeActivites As List(Of Virtualia.TablesObjet.ShemaPER.PER_ACTIVITE_JOUR) = Nothing
        Dim Liste_Semaines As System.Text.StringBuilder
        Dim Libel As String
        Dim DateW_Debut As String
        Dim DateW_Fin As String
        Dim DateW As String
        Dim IndiceK As Integer
        Dim IndiceWeek As Integer
        Dim DateCourante As Date
        Dim SemaineCourante As String = ""

        DateCourante = V_WebFonction.ViRhDates.DateTypee(Datefin)

        Liste_Semaines = New System.Text.StringBuilder
        Select Case ChoixListe
            Case "", "Toutes"
                Dim FicheEtablissement As Virtualia.TablesObjet.ShemaPER.PER_COLLECTIVITE
                FicheEtablissement = CType(V_WebFonction.PointeurUtilisateur.V_PointeurSessionModule, Virtualia.Net.Session.SessionModule).DossierPER.VFiche_Etablissement(Datefin)
                If FicheEtablissement Is Nothing Then
                    Exit Select
                End If
                DateW = DateDebut
                If FicheEtablissement.Date_Valeur_ToDate > V_WebFonction.ViRhDates.DateTypee(DateW) Then
                    DateW = FicheEtablissement.Date_de_Valeur
                End If
                If FicheEtablissement.Date_de_depart <> "" AndAlso V_WebFonction.ViRhDates.DateTypee(FicheEtablissement.Date_de_depart) < V_WebFonction.ViRhDates.DateTypee(Datefin) Then
                    Datefin = FicheEtablissement.Date_de_depart
                    DateCourante = V_WebFonction.ViRhDates.DateTypee(FicheEtablissement.Date_de_depart)
                End If
                Do
                    IndiceWeek = V_WebFonction.ViRhDates.Semaine_Numero(DateW)
                    If IndiceWeek = 1 Then
                        DateW_Debut = V_WebFonction.ViRhDates.Semaine_Date_Debut(DateW)
                        DateW_Fin = V_WebFonction.ViRhDates.Semaine_Date_Fin(DateW_Debut)
                    Else
                        DateW_Debut = V_WebFonction.ViRhDates.Semaine_Date_Debut(IndiceWeek, DateW)
                        DateW_Fin = V_WebFonction.ViRhDates.Semaine_Date_Fin(IndiceWeek, DateW_Debut)
                    End If
                    Liste_Semaines.Append("Semaine " & IndiceWeek.ToString & " - " & DateW_Debut & " au " & DateW_Fin & VI.Tild)
                    If SemaineCourante = "" Then
                        If DateCourante >= V_WebFonction.ViRhDates.DateTypee(DateW_Debut) And DateCourante <= V_WebFonction.ViRhDates.DateTypee(DateW_Fin) Then
                            SemaineCourante = "Semaine " & IndiceWeek.ToString & " - " & DateW_Debut & " au " & DateW_Fin
                        End If
                    End If
                    DateW = V_WebFonction.ViRhDates.CalcDateMoinsJour(DateW_Debut, "7", "0")
                    If V_WebFonction.ViRhDates.DateTypee(DateW) > V_WebFonction.ViRhDates.DateTypee(Datefin) Then
                        Exit Do
                    End If
                Loop
            Case Else
                ListeActivites = CType(V_WebFonction.PointeurUtilisateur.V_PointeurSessionModule, Virtualia.Net.Session.SessionModule).DossierPER.VListeActivites(V_WebFonction.ViRhDates.CalcDateMoinsJour(DateDebut, "0", "366"), Datefin)
                If ListeActivites IsNot Nothing Then
                    IndiceK = 0
                    For Each Fiche In ListeActivites
                        IndiceWeek = V_WebFonction.ViRhDates.Semaine_Numero(Fiche.Date_de_Valeur)
                        If IndiceWeek <> IndiceK Then
                            Select Case IndiceWeek
                                Case 1
                                    DateW_Debut = V_WebFonction.ViRhDates.Semaine_Date_Debut(Fiche.Date_de_Valeur)
                                    DateW_Fin = V_WebFonction.ViRhDates.Semaine_Date_Fin(DateW_Debut)
                                Case Else
                                    DateW_Debut = V_WebFonction.ViRhDates.Semaine_Date_Debut(IndiceWeek, Fiche.Date_de_Valeur)
                                    DateW_Fin = V_WebFonction.ViRhDates.Semaine_Date_Fin(IndiceWeek, DateW_Debut)
                            End Select
                            If ChoixListe = "Validées" Then
                                If Fiche.Date_Visa <> "" Then
                                    Liste_Semaines.Append("Semaine " & IndiceWeek.ToString & " - " & DateW_Debut & " au " & DateW_Fin & VI.Tild)
                                    SemaineCourante = "Semaine " & IndiceWeek.ToString & " - " & DateW_Debut & " au " & DateW_Fin
                                End If
                            ElseIf Fiche.Date_Visa = "" Then
                                Liste_Semaines.Append("Semaine " & IndiceWeek.ToString & " - " & DateW_Debut & " au " & DateW_Fin & VI.Tild)
                                SemaineCourante = "Semaine " & IndiceWeek.ToString & " - " & DateW_Debut & " au " & DateW_Fin
                            End If
                            IndiceK = IndiceWeek
                        End If
                    Next
                End If
        End Select

        Libel = "Aucune semaine" & VI.Tild & "Une semaine" & VI.Tild & "semaines"
        VComboSemaines.V_Liste(Libel) = Liste_Semaines.ToString
        VComboSemaines.LstText = ""

        If SemaineCourante = "" Then
            Return False
        End If

        Dim TableauW(0) As String
        TableauW = Strings.Split(SemaineCourante, " - ", -1)
        TableauW = Strings.Split(TableauW(1), " au ", -1)

        WsCtl_Cache = CacheVirControle
        WsCtl_Cache.DateValeur = TableauW(0)
        WsCtl_Cache.ValeurSelection = SemaineCourante
        CacheVirControle = WsCtl_Cache
        Return True
    End Function

    Private Sub FaireListesDates(ByVal DateDebut As String, ByVal Datefin As String)
        Dim FicheEtablissement As Virtualia.TablesObjet.ShemaPER.PER_COLLECTIVITE
        Dim Liste_Debut As System.Text.StringBuilder
        Dim Liste_Fin As System.Text.StringBuilder
        Dim DateW_Debut As String
        Dim DateW_Fin As String
        Dim DateW As String

        FicheEtablissement = CType(V_WebFonction.PointeurUtilisateur.V_PointeurSessionModule, Virtualia.Net.Session.SessionModule).DossierPER.VFiche_Etablissement(Datefin)
        If FicheEtablissement Is Nothing Then
            Exit Sub
        End If
        DateW = DateDebut
        If FicheEtablissement.Date_Valeur_ToDate > V_WebFonction.ViRhDates.DateTypee(DateW) Then
            DateW = FicheEtablissement.Date_de_Valeur
        End If
        If FicheEtablissement.Date_de_depart <> "" AndAlso V_WebFonction.ViRhDates.DateTypee(FicheEtablissement.Date_de_depart) < V_WebFonction.ViRhDates.DateTypee(Datefin) Then
            Datefin = FicheEtablissement.Date_de_depart
        End If
        Liste_Debut = New System.Text.StringBuilder
        Liste_Fin = New System.Text.StringBuilder
        Do
            DateW_Debut = DateW
            DateW_Fin = V_WebFonction.ViRhDates.DateSaisieVerifiee("31" & Strings.Right(DateW, 8))
            If V_WebFonction.ViRhDates.DateTypee(DateW_Fin) > V_WebFonction.ViRhDates.DateTypee(Datefin) Then
                DateW_Fin = Datefin
            End If
            Liste_Debut.Append(DateW_Debut & VI.Tild)
            Liste_Fin.Append(DateW_Fin & VI.Tild)
            DateW = "01" & Strings.Right(V_WebFonction.ViRhDates.CalcDateMoinsJour(DateW_Debut, "31", "0"), 8)
            If V_WebFonction.ViRhDates.DateTypee(DateW) > V_WebFonction.ViRhDates.DateTypee(Datefin) Then
                Exit Do
            End If
        Loop
        VComboDatesDebut.V_Liste("") = Liste_Debut.ToString
        VComboDatesDebut.LstText = DateW_Debut
        Session.Add("Debut", DateW_Debut)
        VComboDatesFin.V_Liste("") = Liste_Fin.ToString
        VComboDatesFin.LstText = DateW_Fin
        Session.Add("Fin", DateW_Fin)
    End Sub

    Private Sub VComboSemaines_ValeurChange(sender As Object, e As Systeme.Evenements.DonneeChangeEventArgs) Handles VComboSemaines.ValeurChange
        If e.Valeur = "" Then
            Exit Sub
        End If
        Dim TableauW(0) As String
        TableauW = Strings.Split(e.Valeur, " - ", -1)
        TableauW = Strings.Split(TableauW(1), " au ", -1)
        WsCtl_Cache = CacheVirControle
        WsCtl_Cache.ValeurSelection = e.Valeur
        WsCtl_Cache.DateValeur = TableauW(0)
        CacheVirControle = WsCtl_Cache
        CellSaisie.Visible = False
        CellCmdOK.Visible = False
    End Sub

    Private Sub VComboDatesDebut_ValeurChange(sender As Object, e As DonneeChangeEventArgs) Handles VComboDatesDebut.ValeurChange
        Session.Remove("Debut")
        Session.Add("Debut", e.Valeur)
    End Sub

    Private Sub VComboDatesFin_ValeurChange(sender As Object, e As DonneeChangeEventArgs) Handles VComboDatesFin.ValeurChange
        Session.Remove("Fin")
        Session.Add("Fin", e.Valeur)
    End Sub

    Private WriteOnly Property VBackColor(ByVal NoJour As Integer, ByVal Prefixe As String) As System.Drawing.Color
        Set(ByVal value As System.Drawing.Color)
            Dim Ctl As Control
            Dim VirControle As System.Web.UI.WebControls.Label
            Ctl = V_WebFonction.VirWebControle(Me.CadreEnteteActivite, Prefixe, NoJour)
            If Ctl Is Nothing Then
                Exit Property
            End If
            VirControle = CType(Ctl, System.Web.UI.WebControls.Label)
            VirControle.BackColor = value
        End Set
    End Property

    Private WriteOnly Property VBorderColor(ByVal NoJour As Integer, ByVal Prefixe As String) As System.Drawing.Color
        Set(ByVal value As System.Drawing.Color)
            Dim Ctl As Control
            Dim VirControle As System.Web.UI.WebControls.Label
            Ctl = V_WebFonction.VirWebControle(Me.CadreEnteteActivite, Prefixe, NoJour)
            If Ctl Is Nothing Then
                Exit Property
            End If
            VirControle = CType(Ctl, System.Web.UI.WebControls.Label)
            VirControle.BorderColor = value
        End Set
    End Property

    Public WriteOnly Property VDemiJourToolTip(ByVal NoJour As Integer, ByVal Prefixe As String) As String
        Set(ByVal value As String)
            Dim Ctl As Control
            Dim VirControle As System.Web.UI.WebControls.Label
            Ctl = V_WebFonction.VirWebControle(Me.CadreEnteteActivite, Prefixe, NoJour)
            If Ctl Is Nothing Then
                Exit Property
            End If
            VirControle = CType(Ctl, System.Web.UI.WebControls.Label)
            VirControle.ToolTip = value
        End Set
    End Property

    Public WriteOnly Property VTotal(ByVal NoJour As Integer, ByVal SiDemieJournee As Boolean, Optional ByVal SiTravaille As Boolean = True) As Integer
        Set(ByVal value As Integer)
            Dim Ctl As Control
            Dim VirControle As System.Web.UI.WebControls.Label
            Ctl = V_WebFonction.VirWebControle(Me.CadreEnteteActivite, "EtiTotal", NoJour)
            If Ctl Is Nothing Then
                Exit Property
            End If
            VirControle = CType(Ctl, System.Web.UI.WebControls.Label)
            VirControle.ForeColor = V_WebFonction.ConvertCouleur("#124545")
            If value = 0 Then
                VirControle.Text = ""
                Select Case SiTravaille
                    Case True
                        If NoJour < 5 Then
                            VirControle.Text = "0 %"
                            VirControle.ForeColor = V_WebFonction.ConvertCouleur("#A44655")
                        End If
                End Select
            Else
                Select Case SiDemieJournee
                    Case True
                        If value <> 50 Then
                            VirControle.ForeColor = V_WebFonction.ConvertCouleur("#A44655")
                        End If
                    Case False
                        If value <> 100 Then
                            VirControle.ForeColor = V_WebFonction.ConvertCouleur("#A44655")
                        End If
                End Select
                VirControle.Text = CStr(value) & " %"
            End If
        End Set
    End Property

    Private Sub CommandeNew_Click(sender As Object, e As EventArgs) Handles CommandeNew.Click
        Call AfficherNouveau()
        WsCtl_Cache = CacheVirControle
        WsCtl_Cache.Tag = "NEW"
        CacheVirControle = WsCtl_Cache
    End Sub

    Private Sub CommandeOK_Click(sender As Object, e As System.EventArgs) Handles CommandeOK.Click
        If CellSaisie.Visible = False Then
            Exit Sub
        End If
        Dim Cretour As Integer = CadreSaisie.ValiderLaSaisie(False)
        Select Case Cretour
            Case 0
                Dim Evenement As Virtualia.Systeme.Evenements.MessageSaisieEventArgs
                Evenement = New Virtualia.Systeme.Evenements.MessageSaisieEventArgs("Anomalie", 0, "", "OK", "Saisie non conforme", "Projet et Etape sont obligatoires")
                Call V_MessageDialog(Evenement)
            Case 1
                CellSaisie.Visible = False
                CellCmdOK.Visible = False
            Case 2, 3
                Dim TitreMsg As String = "Enregistrement de la saisie effectuée."
                Dim Msg As String = ""
                Dim NatureCmd As String = "Oui;Non"
                Dim NomEmis As String = "Anomalie"
                Select Case Cretour
                    Case 2
                        Msg = "Cette saisie n'est pas conforme (% supérieur à 100%) et va entrainer son annulation partielle." & vbCrLf
                    Case 3
                        Msg = "Cette saisie n'est pas conforme car elle est en doublon et va entrainer son annulation partielle." & vbCrLf
                End Select
                Msg &= "Confirmez-vous la réalisation de cette opération ?"

                Dim Evenement As Virtualia.Systeme.Evenements.MessageSaisieEventArgs
                Evenement = New Virtualia.Systeme.Evenements.MessageSaisieEventArgs(NomEmis, 0, "", NatureCmd, TitreMsg, Msg)
                Call V_MessageDialog(Evenement)
        End Select
    End Sub

    Private Sub CommandeSigner_Click(sender As Object, e As System.EventArgs) Handles CommandeSigner.Click
        Dim TitreMsg As String = "Opération validant la saisie effectuée."
        Dim Msg As String = "Cette opération va figer toute possibilité de modification." & vbCrLf
        Msg &= "Confirmez-vous la réalisation de cette opération ?"
        Dim NatureCmd As String = "Oui;Non"

        Dim Evenement As Virtualia.Systeme.Evenements.MessageSaisieEventArgs
        Evenement = New Virtualia.Systeme.Evenements.MessageSaisieEventArgs("Figer", 0, "", NatureCmd, TitreMsg, Msg)
        Call V_MessageDialog(Evenement)
    End Sub

    Private Sub CommandeRetour_Click(sender As Object, e As ImageClickEventArgs) Handles CommandeRetour.Click
        Dim Evenement As New Virtualia.Systeme.Evenements.MessageRetourEventArgs("W", 0, "", "")
        Call V_Retour(Evenement)
    End Sub

    Private Sub CadreLecture_Modifier(sender As Object, e As Systeme.Evenements.DonneeChangeEventArgs) Handles CadreLecture.Modifier
        Select Case e.Parametre
            Case "MAJ"
                CellSaisie.Visible = True
                CellCmdOK.Visible = True
                Dim IndiceJ As Integer
                CadreSaisie.Clef_Selectionnee = e.Valeur
                For IndiceJ = 0 To 6
                    CadreSaisie.VText(IndiceJ) = ""
                Next IndiceJ
                WsCtl_Cache = CacheVirControle
                WsCtl_Cache.Tag = "NEW"
                CacheVirControle = WsCtl_Cache
            Case "SUP"
                Dim ListeActivites As List(Of Virtualia.TablesObjet.ShemaPER.PER_ACTIVITE_JOUR) = Nothing
                Dim FichePER As Virtualia.TablesObjet.ShemaPER.PER_ACTIVITE_JOUR
                Dim SiOk As Boolean

                WsCtl_Cache = CacheVirControle
                If WsCtl_Cache.DateValeur = "" Then
                    Exit Sub
                End If
                ListeActivites = CType(V_WebFonction.PointeurUtilisateur.V_PointeurSessionModule, Virtualia.Net.Session.SessionModule).DossierPER.VListeActivites(WsCtl_Cache.DateValeur, V_WebFonction.ViRhDates.CalcDateMoinsJour(WsCtl_Cache.DateValeur, "6", "0"))
                If ListeActivites Is Nothing OrElse ListeActivites.Count = 0 Then
                    Exit Sub
                End If
                For Each FichePER In ListeActivites
                    If FichePER.ClefConcatenee = e.Valeur Then
                        SiOk = V_WebFonction.PointeurGlobal.VirServiceServeur.MiseAjour_Fiche(V_WebFonction.PointeurGlobal.VirNomUtilisateur, VI.PointdeVue.PVueApplicatif, 112, FichePER.Ide_Dossier, "S", FichePER.FicheLue, "", True)
                    End If
                Next
        End Select
    End Sub

    Private Sub RadioSemaine_ValeurChange(sender As Object, e As Systeme.Evenements.DonneeChangeEventArgs) Handles RadioSemaine.ValeurChange
        WsCtl_Cache = CacheVirControle
        WsCtl_Cache.Choix = e.Valeur
        WsCtl_Cache.DateValeur = ""
        CacheVirControle = WsCtl_Cache
    End Sub

    Public Sub ViserLaSemaine()
        Dim ListeActivites As List(Of Virtualia.TablesObjet.ShemaPER.PER_ACTIVITE_JOUR) = Nothing
        Dim SiOK As Boolean

        WsCtl_Cache = CacheVirControle
        If WsCtl_Cache.DateValeur = "" Then
            Exit Sub
        End If
        ListeActivites = CType(V_WebFonction.PointeurUtilisateur.V_PointeurSessionModule, Virtualia.Net.Session.SessionModule).DossierPER.VListeActivites(WsCtl_Cache.DateValeur, V_WebFonction.ViRhDates.CalcDateMoinsJour(WsCtl_Cache.DateValeur, "6", "0"))
        If ListeActivites Is Nothing Then
            Exit Sub
        End If
        For Each Fiche In ListeActivites
            Fiche.Date_Visa = V_WebFonction.ViRhDates.DateduJour
            Fiche.Visa = CType(V_WebFonction.PointeurUtilisateur.V_PointeurSessionModule, Virtualia.Net.Session.SessionModule).DossierPER.Nom_Prenom_Manager
            SiOK = V_WebFonction.PointeurGlobal.VirServiceServeur.MiseAjour_Fiche(V_WebFonction.PointeurGlobal.VirNomUtilisateur, VI.PointdeVue.PVueApplicatif, 112, Fiche.Ide_Dossier, "M", Fiche.FicheLue, Fiche.ContenuTable, True)
        Next
    End Sub

    Public Sub ValiderSaisieEtape()
        Dim Cretour As Integer = CadreSaisie.ValiderLaSaisie(True)
        Select Case Cretour
            Case 0, 1
                CellSaisie.Visible = False
                CellCmdOK.Visible = False
        End Select
    End Sub

    Private Sub AfficherNouveau()
        If CellSaisie.Visible = True Then
            Exit Sub
        End If
        CellSaisie.Visible = True
        CellCmdOK.Visible = True
        Dim IndiceJ As Integer
        CadreSaisie.Clef_Selectionnee = ""
        For IndiceJ = 0 To 6
            CadreSaisie.VText(IndiceJ) = ""
        Next IndiceJ
    End Sub

    Private Sub CommandePDF_Click(sender As Object, e As ImageClickEventArgs) Handles CommandePDF.Click
        Dim Util As New Virtualia.Systeme.Outils.Utilitaires
        Dim FichierHtml As String
        Dim FluxHtml As String
        Dim NomPdf As String
        Dim FluxPDF As Byte()

        NomPdf = V_WebFonction.ViRhFonction.ChaineSansAccent(CType(V_WebFonction.PointeurUtilisateur.V_PointeurSessionModule, Virtualia.Net.Session.SessionModule).DossierPER.Nom & "_" & CType(V_WebFonction.PointeurUtilisateur.V_PointeurSessionModule, Virtualia.Net.Session.SessionModule).DossierPER.Prenom, False) & ".pdf"

        FichierHtml = V_WebFonction.PointeurGlobal.VirRepertoireTemporaire & "\" & Session.SessionID & ".html"
        Call Util.TransformeFicHtml(FichierHtml, "CadreEdition", 200)

        Try
            Dim WebServiceOutil As New Virtualia.Net.WebService.VirtualiaOutils(V_WebFonction.PointeurGlobal.UrlWebOutil)
            FluxHtml = My.Computer.FileSystem.ReadAllText(FichierHtml, System.Text.Encoding.UTF8)
            FluxPDF = WebServiceOutil.ConversionPDF(FluxHtml, True)
            WebServiceOutil.Dispose()

            If FluxPDF IsNot Nothing Then
                Dim response As System.Web.HttpResponse = System.Web.HttpContext.Current.Response
                response.Clear()
                response.AddHeader("Content-Type", "binary/octet-stream")
                response.AddHeader("Content-Disposition", "attachment; filename=" & NomPdf.Replace(Strings.Space(1), "_") & "; size=" & FluxPDF.Length.ToString())
                response.Flush()
                response.BinaryWrite(FluxPDF)
                response.Flush()
                response.End()
            End If
        Catch ex As Exception
            Exit Try
        End Try
    End Sub

    Private Sub CommandeMode_Click(sender As Object, e As ImageClickEventArgs) Handles CommandeMode.Click
        CellModeSaisie.Visible = Not CellModeSaisie.Visible
        CellEditionActivite.Visible = CellModeSaisie.Visible

        CellModeControle.Visible = Not CellModeSaisie.Visible
        CellEditionControle.Visible = CellModeControle.Visible

        If CellModeSaisie.Visible = False Then
            CommandeMode.ImageUrl = "~/Images/Boutons/ModeClassique.bmp"
            CellCmdMode.Width = New Unit(55)
        Else
            CommandeMode.ImageUrl = "~/Images/Boutons/ModeSynthese.bmp"
            CellCmdMode.Width = New Unit(75)
        End If
    End Sub

End Class