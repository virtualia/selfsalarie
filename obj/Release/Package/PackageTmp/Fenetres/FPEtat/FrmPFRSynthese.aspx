﻿<%@ Page Language="vb" AutoEventWireup="false" MasterPageFile="~/PagesMaitres/VirtualiaMain.Master" CodeBehind="FrmPFRSynthese.aspx.vb" 
    Inherits="Virtualia.Net.FrmPFRSynthese" MaintainScrollPositionOnPostback="True" %>

<%@ MasterType VirtualPath="~/PagesMaitres/VirtualiaMain.master" %>
<%@ Register src="~/Controles/Commun/VListeGrid.ascx" tagname="VListeGrid" tagprefix="Virtualia" %>
<%@ Register src="~/Controles/Commun/VListeCombo.ascx" tagname="VListeCombo" tagprefix="Virtualia" %>

<asp:Content ID="LigneAnnee" ContentPlaceHolderID="HautdePage" runat="server">
    <asp:Table ID="CadreAnnee" runat="server" HorizontalAlign="Center" Width="1050px">
        <asp:TableRow>
            <asp:TableCell HorizontalAlign="Left">
                <Virtualia:VListeCombo ID="ComboSelAnnee" runat="server" EtiVisible="true" EtiText="ANNEE COURANTE" EtiWidth="150px"
                        V_NomTable="LstAnnee" LstWidth="100px" LstBorderColor="#B0E0D7" LstBackColor="#EEECFD" LstStyle="font-weight:bold"
                        EtiStyle="text-align:center;font-weight:bold;margin-left:1px" EtiBackColor="#0E5F5C" EtiForeColor="White" />
            </asp:TableCell>
        </asp:TableRow>
    </asp:Table>
</asp:Content>

<asp:Content ID="CadreSynthese" runat="server" ContentPlaceHolderID="Principal">
    <asp:Table ID="CadreEDition" runat="server" Width="1050px" HorizontalAlign="Center" BackColor="#2D8781" CellSpacing="2" style="margin-top: 5px ">
        <asp:TableRow>
           <asp:TableCell ID="CadreCmd" HorizontalAlign="Right" runat="server">
                <asp:Panel ID="PanelCommande" runat="server" style="height:25px;width:650px" >
                    <asp:Table ID="CadreCmdStd" runat="server" Height="20px" CellPadding="0" 
                        CellSpacing="0" BackImageUrl="~/Images/Icones/Cmd_Std.bmp"
                        BorderWidth="2px" BorderStyle="Outset" BorderColor="#B6C7E2" ForeColor="#D7FAF3"
                        Width="600px" HorizontalAlign="Center" style="margin-top: 3px;">
                       <asp:TableRow VerticalAlign="Top">
                            <asp:TableCell>
                                <asp:Button ID="CmdDotation" runat="server" Text="DOTATIONS" Width="135px" Height="20px"
                                    BackColor="Transparent" BorderColor="#B6C7E2" ForeColor="#D7FAF3"
                                    Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small" Font-Italic="true"
                                    BorderStyle="None" style=" margin-left: 15px; text-align: left;"
                                    Tooltip="Tableau des dotations budgétaires des régimes indemnitaires" >
                                </asp:Button>
                            </asp:TableCell>
                            <asp:TableCell>
                                <asp:Button ID="CmdEnveloppe" runat="server" Text="MODULATIONS" Width="135px" Height="20px"
                                    BackColor="Transparent" BorderColor="#B6C7E2" ForeColor="#D7FAF3"
                                    Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small" Font-Italic="true"
                                    BorderStyle="None" style=" margin-left: 15px; text-align: left;"
                                    Tooltip="Enveloppe de modulation" >
                                </asp:Button>
                            </asp:TableCell>
                            <asp:TableCell>
                                <asp:Button ID="CommandeCsv" runat="server" Text="Fichier Csv" Width="135px" Height="20px"
                                    BackColor="Transparent" BorderColor="#B6C7E2" ForeColor="#D7FAF3"
                                    Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small" Font-Italic="true"
                                    BorderStyle="None" style=" margin-left: 15px; text-align: left;"
                                    Tooltip="Obtenir un fichier au format csv" >
                                </asp:Button>
                            </asp:TableCell>
                            <asp:TableCell>
                                <asp:Button ID="CommandeRetour" runat="server" Text="Retour" Width="135px" Height="20px"
                                    BackColor="Transparent" BorderColor="#B6C7E2" ForeColor="#D7FAF3"
                                    Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small" Font-Italic="true"
                                    BorderStyle="None" style=" margin-left: 15px; text-align: left;"
                                    Tooltip="" >
                                </asp:Button>
                            </asp:TableCell>
                       </asp:TableRow>
                    </asp:Table>
                </asp:Panel>
            </asp:TableCell>
        </asp:TableRow>
        <asp:TableRow>
            <asp:TableCell>
                <asp:Table ID="CadreVues" runat="server" HorizontalAlign="Center" borderStyle="None">
                    <asp:TableRow>
                        <asp:TableCell HorizontalAlign="Center" VerticalAlign="Top">
                            <asp:MultiView ID="MultiVues" runat="server" ActiveViewIndex="0">
                                <asp:View ID="VueEdition" runat="server">
                                     <Virtualia:VListeGrid ID="ListeSynthese" runat="server" CadreWidth="1000px" SiColonneSelect="false"
                                            SiCaseAcocher="false" SiPagination="false" TaillePage="25" />
                                </asp:View>
                            </asp:MultiView>
                        </asp:TableCell>
                    </asp:TableRow>
                </asp:Table>
            </asp:TableCell>
        </asp:TableRow>
                       
    </asp:Table>
</asp:Content>

