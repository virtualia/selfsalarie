﻿Option Strict On
Option Explicit On
Option Compare Text
Imports VI = Virtualia.Systeme.Constantes
Public Class VSaisieCalendrier
    Inherits System.Web.UI.UserControl
    Public Delegate Sub Valeur_ChangeEventHandler(ByVal sender As Object, ByVal e As Virtualia.Systeme.Evenements.DonneeChangeEventArgs)
    Public Event ValeurChange As Valeur_ChangeEventHandler
    Private WebFct As Virtualia.Net.Controles.WebFonctions
    Private WsCtl_Cache As Virtualia.Net.VCaches.CacheCalendrier
    Private WsNomStateCache As String = "VCalendrierMensuel"
    '***********************************************************
    Private WsTypeCalend As String = "Standard" 'ou Indidividuel
    Private WsSiWEJFEnable As Boolean = True
    Private WsInstanceCouleur As Virtualia.Systeme.Planning.CouleursJour
    Private WsCouleurSelection As System.Drawing.Color
    Private WsForeColorSelection As System.Drawing.Color

    Protected Overridable Sub Saisie_Change(ByVal e As Virtualia.Systeme.Evenements.DonneeChangeEventArgs)
        RaiseEvent ValeurChange(Me, e)
    End Sub

    Private ReadOnly Property V_WebFonction As Virtualia.Net.Controles.WebFonctions
        Get
            If WebFct Is Nothing Then
                WebFct = New Virtualia.Net.Controles.WebFonctions(Me, 0)
            End If
            Return WebFct
        End Get
    End Property

    Public Property TypeCalendrier() As String
        Get
            Return WsTypeCalend
        End Get
        Set(ByVal value As String)
            WsTypeCalend = value
        End Set
    End Property

    Public Property SiEnableWEJF As Boolean
        Get
            Return WsSiWEJFEnable
        End Get
        Set(value As Boolean)
            WsSiWEJFEnable = value
        End Set
    End Property

    Public Sub Initialiser()
        CalDateSel00.Text = ""
        CalDateSel01.Text = ""
        RadioV1.Checked = True
    End Sub

    Private Sub VSaisieCalendrier_PreRender(sender As Object, e As EventArgs) Handles Me.PreRender
        WsCtl_Cache = CacheVirControle
        Call FaireListeMois()
        Call FaireCalendrier()
    End Sub

    Public Property Identifiant() As Integer
        Get
            WsCtl_Cache = CacheVirControle
            Return WsCtl_Cache.Ide_Dossier
        End Get
        Set(ByVal value As Integer)
            If value = 0 Then
                Exit Property
            End If
            WsCtl_Cache = CacheVirControle
            If WsCtl_Cache.Ide_Dossier = value Then
                Exit Property
            End If
            WsCtl_Cache = New Virtualia.Net.VCaches.CacheCalendrier
            WsCtl_Cache.Ide_Dossier = value
            CacheVirControle = WsCtl_Cache

            Dim UtiSession As Virtualia.Net.Session.ObjetSession = V_WebFonction.PointeurUtilisateur
            If UtiSession Is Nothing Then
                Exit Property
            End If
            Call Initialiser()
        End Set
    End Property

    Private Property CacheVirControle As Virtualia.Net.VCaches.CacheCalendrier
        Get
            If Me.ViewState(WsNomStateCache) IsNot Nothing Then
                Return CType(Me.ViewState(WsNomStateCache), Virtualia.Net.VCaches.CacheCalendrier)
            End If
            Dim NewCache As Virtualia.Net.VCaches.CacheCalendrier
            NewCache = New Virtualia.Net.VCaches.CacheCalendrier
            NewCache.Annee_Selection = System.DateTime.Now.Year
            NewCache.Mois_Selection = System.DateTime.Now.Month
            NewCache.Coche_Butoir = ""
            Return NewCache
        End Get
        Set(value As Virtualia.Net.VCaches.CacheCalendrier)
            If value Is Nothing Then
                Exit Property
            End If
            If Me.ViewState(WsNomStateCache) IsNot Nothing Then
                Me.ViewState.Remove(WsNomStateCache)
            End If
            Me.ViewState.Add(WsNomStateCache, value)
        End Set
    End Property

    Public WriteOnly Property DateButoir(ByVal CocheSel As String) As String
        Set(value As String)
            If value = "" Then
                Exit Property
            End If
            Dim DateReferente As Date = V_WebFonction.ViRhDates.DateTypee(value)
            WsCtl_Cache = CacheVirControle
            WsCtl_Cache.Date_Butoir = value
            If WsCtl_Cache.Annee_Selection < DateReferente.Year Then
                WsCtl_Cache.Annee_Selection = DateReferente.Year
                WsCtl_Cache.Mois_Selection = DateReferente.Month
            ElseIf WsCtl_Cache.Annee_Selection = DateReferente.Year And WsCtl_Cache.Mois_Selection < DateReferente.Month Then
                WsCtl_Cache.Mois_Selection = DateReferente.Month
            End If
            WsCtl_Cache.Coche_Selection = CocheSel
            CacheVirControle = WsCtl_Cache
        End Set
    End Property

    Public WriteOnly Property CocheButoir() As Boolean
        Set(value As Boolean)
            WsCtl_Cache = CacheVirControle
            Select Case value
                Case True
                    WsCtl_Cache.Coche_Butoir = "Oui"
                Case False
                    WsCtl_Cache.Coche_Butoir = ""
            End Select
            CacheVirControle = WsCtl_Cache
        End Set
    End Property

    Public Property AnneeSelectionnee() As Integer
        Get
            WsCtl_Cache = CacheVirControle
            Return WsCtl_Cache.Annee_Selection
        End Get
        Set(ByVal value As Integer)
            WsCtl_Cache = CacheVirControle
            WsCtl_Cache.Annee_Selection = value
            CacheVirControle = WsCtl_Cache
        End Set
    End Property

    Public Property MoisSelectionne() As Integer
        Get
            WsCtl_Cache = CacheVirControle
            Return WsCtl_Cache.Mois_Selection
        End Get
        Set(ByVal value As Integer)
            WsCtl_Cache = CacheVirControle
            WsCtl_Cache.Mois_Selection = value
            CacheVirControle = WsCtl_Cache
        End Set
    End Property

    Public Property DateSelectionnee() As String
        Get
            Return CalDateSel00.Text
        End Get
        Set(ByVal value As String)
            CalDateSel00.Text = value
            If value <> "" Then
                WsCtl_Cache = CacheVirControle
                WsCtl_Cache.Date_Valeur = value
                WsCtl_Cache.Annee_Selection = V_WebFonction.ViRhDates.DateTypee(value).Year
                WsCtl_Cache.Mois_Selection = V_WebFonction.ViRhDates.DateTypee(value).Month
                CacheVirControle = WsCtl_Cache
            End If
            CalLstAnnee.Items.Clear()
        End Set
    End Property

    Public Property OptionSelectionnee() As String
        Get
            WsCtl_Cache = CacheVirControle
            Return WsCtl_Cache.Coche_Selection
        End Get
        Set(ByVal value As String)
            WsCtl_Cache = CacheVirControle
            WsCtl_Cache.Coche_Selection = value
            CacheVirControle = WsCtl_Cache
            Select Case value
                Case "CALAM"
                    RadioV0.Checked = True
                Case "CALPM"
                    RadioV2.Checked = True
                Case Else
                    RadioV1.Checked = True
            End Select
        End Set
    End Property

    Public Property CalTitre As String
        Get
            Return EtiTitre.Text
        End Get
        Set(value As String)
            EtiTitre.Text = value
        End Set
    End Property

    Private Sub CalLstAnnee_SelectedIndexChanged(sender As Object, e As EventArgs) Handles CalLstAnnee.SelectedIndexChanged
        AnneeSelectionnee = CInt(CalLstAnnee.SelectedValue)
        If WsCtl_Cache.Date_Butoir <> "" Then
            CalLstAnnee.Items.Clear()
        End If
    End Sub

    Private Sub CalLstMois_SelectedIndexChanged(sender As Object, e As EventArgs) Handles CalLstMois.SelectedIndexChanged
        MoisSelectionne = CInt(CalLstMois.SelectedValue)
    End Sub

    Public Property SiEtiDateSelVisible() As Boolean
        Get
            Return CalDateSel00.Visible
        End Get
        Set(ByVal value As Boolean)
            CalDateSel00.Visible = value
            CalDateSel01.Visible = value
        End Set
    End Property

    Public WriteOnly Property SiJourVisible(ByVal NoJour As Integer, ByVal Prefixe As String) As Boolean
        Set(ByVal value As Boolean)
            Dim Ctl As Control
            Dim VirControle As System.Web.UI.WebControls.Button
            Ctl = V_WebFonction.VirWebControle(Me, Prefixe, NoJour)
            If Ctl Is Nothing Then
                Exit Property
            End If
            VirControle = CType(Ctl, System.Web.UI.WebControls.Button)
            Select Case value
                Case True
                    VirControle.BackColor = Drawing.Color.LightGray
                    VirControle.BorderStyle = BorderStyle.Solid
                    Select Case WsTypeCalend
                        Case Is = "Individuel"
                            CalJourStyle(NoJour, "CalPM") = "solid"
                    End Select
                Case False
                    VirControle.BackColor = Drawing.Color.Transparent
                    VirControle.BorderStyle = BorderStyle.None
                    VirControle.Text = ""
                    CalJourStyle(NoJour, Prefixe) = "none"
            End Select
        End Set
    End Property

    Public Property SiJourEnable(ByVal NoJour As Integer, ByVal Prefixe As String) As Boolean
        Get
            Dim Ctl As Control
            Dim VirControle As System.Web.UI.WebControls.Button
            Ctl = V_WebFonction.VirWebControle(Me, Prefixe, NoJour)
            If Ctl Is Nothing Then
                Return False
            End If
            VirControle = CType(Ctl, System.Web.UI.WebControls.Button)
            Return VirControle.Enabled
        End Get
        Set(ByVal value As Boolean)
            Dim Ctl As Control
            Dim VirControle As System.Web.UI.WebControls.Button
            Ctl = V_WebFonction.VirWebControle(Me, Prefixe, NoJour)
            If Ctl Is Nothing Then
                Exit Property
            End If
            VirControle = CType(Ctl, System.Web.UI.WebControls.Button)
            VirControle.Enabled = value
        End Set
    End Property

    Public WriteOnly Property CalText(ByVal NoJour As Integer, ByVal Prefixe As String) As String
        Set(ByVal value As String)
            Dim Ctl As Control
            Dim VirControle As System.Web.UI.WebControls.Button
            Ctl = V_WebFonction.VirWebControle(Me, Prefixe, NoJour)
            If Ctl Is Nothing Then
                Exit Property
            End If
            VirControle = CType(Ctl, System.Web.UI.WebControls.Button)
            VirControle.Text = value
        End Set
    End Property

    Public WriteOnly Property CalDemiJourToolTip(ByVal NoJour As Integer, ByVal Prefixe As String) As String
        Set(ByVal value As String)
            Dim Ctl As Control
            Dim VirControle As System.Web.UI.WebControls.Button
            Ctl = V_WebFonction.VirWebControle(Me, Prefixe, NoJour)
            If Ctl Is Nothing Then
                Exit Property
            End If
            VirControle = CType(Ctl, System.Web.UI.WebControls.Button)
            VirControle.ToolTip = value
        End Set
    End Property

    Public WriteOnly Property CalBorduresColor(ByVal Prefixe As String) As System.Drawing.Color
        Set(ByVal value As System.Drawing.Color)
            Dim IndiceI As Integer
            For IndiceI = 0 To 36
                CalBorderColor(IndiceI, Prefixe) = value
            Next IndiceI
        End Set
    End Property

    Public WriteOnly Property CalBackColor(ByVal NoJour As Integer, ByVal Prefixe As String) As System.Drawing.Color
        Set(ByVal value As System.Drawing.Color)
            Dim Ctl As Control
            Dim VirControle As System.Web.UI.WebControls.Button
            Ctl = V_WebFonction.VirWebControle(Me, Prefixe, NoJour)
            If Ctl Is Nothing Then
                Exit Property
            End If
            VirControle = CType(Ctl, System.Web.UI.WebControls.Button)
            VirControle.BackColor = value
        End Set
    End Property

    Public WriteOnly Property CalBorderColor(ByVal NoJour As Integer, ByVal Prefixe As String) As System.Drawing.Color
        Set(ByVal value As System.Drawing.Color)
            Dim Ctl As Control
            Dim VirControle As System.Web.UI.WebControls.Button
            Ctl = V_WebFonction.VirWebControle(Me, Prefixe, NoJour)
            If Ctl Is Nothing Then
                Exit Property
            End If
            VirControle = CType(Ctl, System.Web.UI.WebControls.Button)
            VirControle.BorderColor = value
        End Set
    End Property

    Public WriteOnly Property CalSelectionColor() As String
        Set(ByVal value As String)
            WsCouleurSelection = V_WebFonction.ConvertCouleur(value)
            If WsCouleurSelection.GetBrightness > 0.35 Then
                WsForeColorSelection = Drawing.Color.Black
            Else
                WsForeColorSelection = Drawing.Color.White
            End If
            CalDateSel00.BackColor = WsCouleurSelection
            CalDateSel01.BackColor = WsCouleurSelection
            CalDateSel00.ForeColor = WsForeColorSelection
            CalDateSel01.ForeColor = WsForeColorSelection
        End Set
    End Property

    Public WriteOnly Property CalJoursStyle() As String
        Set(ByVal value As String)
            Dim IndiceI As Integer
            For IndiceI = 0 To 36
                CalJourStyle(IndiceI, "CalPM") = value
            Next IndiceI
        End Set
    End Property

    Public WriteOnly Property CalJourStyle(ByVal NoJour As Integer, ByVal Prefixe As String) As String
        Set(ByVal value As String)
            Dim Ctl As Control
            Dim VirControle As System.Web.UI.WebControls.Button
            Dim StyleBordure As String = "border-top-style"
            Dim NumBouton As Integer

            If Prefixe = "CalAM" Then
                StyleBordure = "border-bottom-style"
            End If
            Ctl = V_WebFonction.VirWebControle(Me, Prefixe, NoJour)
            If Ctl Is Nothing Then
                Exit Property
            End If
            VirControle = CType(Ctl, System.Web.UI.WebControls.Button)
            NumBouton = CInt(Strings.Right(VirControle.ID, 2))
            If NumBouton = NoJour Then
                VirControle.Style.Remove(StyleBordure)
                VirControle.Style.Add(StyleBordure, value)
            End If
        End Set
    End Property

    Public WriteOnly Property CalFontSize(ByVal NoJour As Integer, ByVal Prefixe As String) As System.Web.UI.WebControls.FontUnit
        Set(ByVal value As System.Web.UI.WebControls.FontUnit)
            Dim Ctl As Control
            Dim VirControle As System.Web.UI.WebControls.Button
            Ctl = V_WebFonction.VirWebControle(Me, Prefixe, NoJour)
            If Ctl Is Nothing Then
                Exit Property
            End If
            VirControle = CType(Ctl, System.Web.UI.WebControls.Button)
            VirControle.Font.Size = value
        End Set
    End Property

    Public WriteOnly Property CalFontBold(ByVal NoJour As Integer, ByVal Prefixe As String) As Boolean
        Set(ByVal value As Boolean)
            Dim Ctl As Control
            Dim VirControle As System.Web.UI.WebControls.Button
            Ctl = V_WebFonction.VirWebControle(Me, Prefixe, NoJour)
            If Ctl Is Nothing Then
                Exit Property
            End If
            VirControle = CType(Ctl, System.Web.UI.WebControls.Button)
            VirControle.Font.Bold = value
        End Set
    End Property

    Protected Sub CalAM_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles CalAM00.Click, CalAM01.Click,
    CalAM02.Click, CalAM03.Click, CalAM04.Click, CalAM05.Click, CalAM06.Click, CalAM07.Click, CalAM08.Click, CalAM09.Click,
    CalAM10.Click, CalAM11.Click, CalAM12.Click, CalAM13.Click, CalAM14.Click, CalAM15.Click, CalAM08.Click, CalAM16.Click,
    CalAM17.Click, CalAM18.Click, CalAM19.Click, CalAM20.Click, CalAM21.Click, CalAM22.Click, CalAM23.Click, CalAM24.Click,
    CalAM25.Click, CalAM26.Click, CalAM27.Click, CalAM28.Click, CalAM29.Click, CalAM30.Click, CalAM31.Click, CalAM32.Click,
    CalAM33.Click, CalAM34.Click, CalAM35.Click, CalAM36.Click

        Dim Jour As Integer
        Dim DateW As String

        If CType(sender, System.Web.UI.WebControls.Button).Text = "" OrElse IsNumeric(CType(sender, System.Web.UI.WebControls.Button).Text) = False Then
            Exit Sub
        End If
        WsCtl_Cache = CacheVirControle
        Jour = CInt(CType(sender, System.Web.UI.WebControls.Button).Text)
        DateW = Strings.Format(Jour, "00") & VI.Slash & Strings.Format(WsCtl_Cache.Mois_Selection, "00") & VI.Slash & WsCtl_Cache.Annee_Selection
        If WsCtl_Cache.Date_Butoir <> "" Then
            Select Case V_WebFonction.ViRhDates.ComparerDates(DateW, WsCtl_Cache.Date_Butoir)
                Case VI.ComparaisonDates.PlusPetit
                    Exit Sub
            End Select
        End If
        WsCtl_Cache.Date_Valeur = DateW
        CalDateSel00.Text = DateW
        CacheVirControle = WsCtl_Cache

        Dim Evenement As Virtualia.Systeme.Evenements.DonneeChangeEventArgs
        Evenement = New Virtualia.Systeme.Evenements.DonneeChangeEventArgs(DateW)
        Saisie_Change(Evenement)
    End Sub

    Protected Sub CalPM_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles CalPM00.Click, CalPM01.Click,
    CalPM02.Click, CalPM03.Click, CalPM04.Click, CalPM05.Click, CalPM06.Click, CalPM07.Click, CalPM08.Click, CalPM09.Click,
    CalPM10.Click, CalPM11.Click, CalPM12.Click, CalPM13.Click, CalPM14.Click, CalPM15.Click, CalPM08.Click, CalPM16.Click,
    CalPM17.Click, CalPM18.Click, CalPM19.Click, CalPM20.Click, CalPM21.Click, CalPM22.Click, CalPM23.Click, CalPM24.Click,
    CalPM25.Click, CalPM26.Click, CalPM27.Click, CalPM28.Click, CalPM29.Click, CalPM30.Click, CalPM31.Click, CalPM32.Click,
    CalPM33.Click, CalPM34.Click, CalPM35.Click, CalPM36.Click

        Dim Jour As Integer
        Dim DateW As String
        Dim Ctl As Control
        Dim VirControle As System.Web.UI.WebControls.Button
        WsCtl_Cache = CacheVirControle
        Jour = CInt(Strings.Right(CType(sender, System.Web.UI.WebControls.Button).ID, 2))
        Ctl = V_WebFonction.VirWebControle(Me, "CalAM", Jour)
        If Ctl Is Nothing Then
            Exit Sub
        End If
        VirControle = CType(Ctl, System.Web.UI.WebControls.Button)
        If IsNumeric(VirControle.Text) Then
            Jour = CInt(VirControle.Text)
        Else
            Exit Sub
        End If
        DateW = Strings.Format(Jour, "00") & VI.Slash & Strings.Format(WsCtl_Cache.Mois_Selection, "00") & VI.Slash & WsCtl_Cache.Annee_Selection
        If WsCtl_Cache.Date_Butoir <> "" Then
            Select Case V_WebFonction.ViRhDates.ComparerDates(DateW, WsCtl_Cache.Date_Butoir)
                Case VI.ComparaisonDates.PlusPetit
                    Exit Sub
            End Select
        End If
        WsCtl_Cache.Date_Valeur = DateW
        CalDateSel00.Text = DateW
        CacheVirControle = WsCtl_Cache

        Dim Evenement As Virtualia.Systeme.Evenements.DonneeChangeEventArgs
        Evenement = New Virtualia.Systeme.Evenements.DonneeChangeEventArgs(DateW)
        Saisie_Change(Evenement)
    End Sub

    Private Sub Radio_CheckedChanged(sender As Object, e As EventArgs) Handles RadioV0.CheckedChanged, RadioV1.CheckedChanged, RadioV2.CheckedChanged
        WsCtl_Cache = CacheVirControle
        WsCtl_Cache.Coche_Selection = ""
        Select Case CType(sender, RadioButton).ID
            Case "RadioV0"
                WsCtl_Cache.Coche_Selection = "CALAM"
            Case "RadioV2"
                WsCtl_Cache.Coche_Selection = "CALPM"
        End Select
        CacheVirControle = WsCtl_Cache
    End Sub

    Private Sub FaireListeMois()
        If CalLstAnnee.Items.Count > 0 Then
            Exit Sub
        End If
        Dim IndiceI As Integer
        Dim Annee As Integer = 1950
        Dim Mois As Integer = 1
        If WsCtl_Cache.Date_Butoir <> "" Then
            Annee = V_WebFonction.ViRhDates.DateTypee(WsCtl_Cache.Date_Butoir).Year
            Mois = V_WebFonction.ViRhDates.DateTypee(WsCtl_Cache.Date_Butoir).Month
        End If
        CalLstAnnee.Items.Clear()
        For IndiceI = Annee To Year(Now) + 7
            CalLstAnnee.Items.Add(New ListItem(CStr(IndiceI), CStr(IndiceI)))
        Next IndiceI

        CalLstMois.Items.Clear()
        If WsCtl_Cache.Date_Butoir <> "" Then
            Select Case WsCtl_Cache.Annee_Selection
                Case = Annee
                    For IndiceI = Mois To 12
                        CalLstMois.Items.Add(New ListItem(V_WebFonction.ViRhDates.MoisEnClair(IndiceI), CStr(IndiceI)))
                    Next IndiceI
                    Try
                        CalLstAnnee.SelectedValue = CStr(Annee)
                    Catch ex As Exception
                        Exit Try
                    End Try
                    Try
                        CalLstMois.SelectedValue = CStr(WsCtl_Cache.Mois_Selection)
                    Catch ex As Exception
                        CalLstMois.SelectedValue = CStr(Mois)
                    End Try
                    Exit Sub
            End Select
        End If
        For IndiceI = 1 To 12
            CalLstMois.Items.Add(New ListItem(V_WebFonction.ViRhDates.MoisEnClair(IndiceI), CStr(IndiceI)))
        Next IndiceI
        Try
            CalLstAnnee.SelectedValue = CStr(WsCtl_Cache.Annee_Selection)
        Catch ex As Exception
            Exit Try
        End Try
        Try
            CalLstMois.SelectedValue = CStr(WsCtl_Cache.Mois_Selection)
        Catch ex As Exception
            Exit Try
        End Try
    End Sub

    Private Sub FaireCalendrier()
        Dim LstCalend As List(Of Virtualia.Net.Controles.ItemJourCalendrier) = Nothing
        Dim LstAbsJour As List(Of Virtualia.Net.Controles.ItemJourCalendrier) = Nothing
        Dim FicheAbs As Virtualia.Net.Controles.ItemJourCalendrier = Nothing
        Dim Mois As Integer
        Dim Annee As Integer
        Dim DateW As String
        Dim DateFin As String
        Dim NoJour As Integer
        Dim IndiceJ As Integer
        Dim IndiceCtl As Integer
        Dim Ide_Dossier As Integer = Identifiant
        Dim Couleur As Drawing.Color

        WsInstanceCouleur = V_WebFonction.PointeurUtilisateur.V_PointeurGlobal.InstanceCouleur

        For IndiceCtl = 0 To 36
            SiJourEnable(IndiceCtl, "CalAM") = True
            SiJourEnable(IndiceCtl, "CalPM") = True
        Next IndiceCtl
        Annee = WsCtl_Cache.Annee_Selection
        Mois = WsCtl_Cache.Mois_Selection
        DateW = "01/" & Strings.Format(Mois, "00") & "/" & Annee
        DateFin = V_WebFonction.ViRhDates.DateSaisieVerifiee("31" & "/" & Strings.Format(Mois, "00") & "/" & Annee.ToString)

        If Ide_Dossier > 0 Then
            If V_WebFonction.PointeurUtilisateur IsNot Nothing Then
                LstCalend = V_WebFonction.PointeurUtilisateur.V_ObjetCalendrier(Ide_Dossier, "01/01/" & Annee)
            End If
        End If

        NoJour = Weekday(DateValue(DateW), Microsoft.VisualBasic.FirstDayOfWeek.Monday) - 1
        For IndiceCtl = 0 To NoJour - 1
            SiJourVisible(IndiceCtl, "CalAM") = False
            SiJourVisible(IndiceCtl, "CalPM") = False
        Next IndiceCtl
        IndiceJ = NoJour

        Do 'Traitement des jours du mois
            '** Calendrier Standard
            SiJourVisible(IndiceJ, "CalAM") = True
            SiJourVisible(IndiceJ, "CalPM") = True
            CalBorderColor(IndiceJ, "CalAM") = V_WebFonction.ConvertCouleur("#216B68")
            CalBorderColor(IndiceJ, "CalPM") = V_WebFonction.ConvertCouleur("#216B68")
            CalText(IndiceJ, "CalAM") = CShort(Strings.Left(DateW, 2)).ToString
            CalDemiJourToolTip(IndiceJ, "CalAM") = V_WebFonction.ViRhDates.ClairDate(DateW, True)
            CalDemiJourToolTip(IndiceJ, "CalPM") = V_WebFonction.ViRhDates.ClairDate(DateW, True)
            If V_WebFonction.ViRhDates.SiJourOuvre(DateW, True) = True Then
                CalBackColor(IndiceJ, "CalAM") = WsInstanceCouleur.Item(0).Couleur_Web
                CalBackColor(IndiceJ, "CalPM") = WsInstanceCouleur.Item(0).Couleur_Web
            Else
                CalBackColor(IndiceJ, "CalAM") = WsInstanceCouleur.Item(1).Couleur_Web
                CalBackColor(IndiceJ, "CalPM") = WsInstanceCouleur.Item(1).Couleur_Web
                CalJourStyle(IndiceJ, "CalPM") = "none"
                If V_WebFonction.ViRhDates.SiJourFerie(DateW) = True Then
                    CalBorderColor(IndiceJ, "CalAM") = Drawing.Color.Red
                    CalBorderColor(IndiceJ, "CalPM") = Drawing.Color.Red
                End If
                SiJourEnable(IndiceJ, "CalAM") = SiEnableWEJF
                SiJourEnable(IndiceJ, "CalPM") = SiEnableWEJF
            End If
            '*** Le Planning de la personne ***********************
            LstAbsJour = Nothing
            If Ide_Dossier > 0 Then
                If LstCalend IsNot Nothing Then
                    LstAbsJour = (From FicheCal In LstCalend Where FicheCal.Date_Valeur_ToDate = V_WebFonction.ViRhDates.DateTypee(DateW)).ToList
                End If
            End If
            If LstAbsJour IsNot Nothing AndAlso LstAbsJour.Count > 0 Then
                For Each FicheAbs In LstAbsJour
                    Select Case FicheAbs.ObjetOrigine
                        Case VI.ObjetPer.ObaFormation
                            Couleur = WsInstanceCouleur.Item(WsInstanceCouleur.IndexCouleurPlanning("Formation")).Couleur_Web
                        Case VI.ObjetPer.ObaMission
                            Couleur = WsInstanceCouleur.Item(WsInstanceCouleur.IndexCouleurPlanning("Mission")).Couleur_Web
                        Case VI.ObjetPer.ObaSaisieIntranet
                            Couleur = WsInstanceCouleur.Item(4).Couleur_Web
                        Case Else
                            Couleur = WsInstanceCouleur.Item(WsInstanceCouleur.IndexCouleurPlanning(FicheAbs.Intitule)).Couleur_Web
                    End Select
                    Select Case FicheAbs.Caracteristique
                        Case VI.NumeroPlage.Jour_Absence, VI.NumeroPlage.Jour_Formation, VI.NumeroPlage.Jour_Mission
                            CalJourStyle(IndiceJ, "CalAM") = "none"
                            CalBackColor(IndiceJ, "CalAM") = Couleur
                            CalDemiJourToolTip(IndiceJ, "CalAM") = FicheAbs.Intitule
                            CalBackColor(IndiceJ, "CalPM") = Couleur
                            CalDemiJourToolTip(IndiceJ, "CalPM") = FicheAbs.Intitule
                            SiJourEnable(IndiceJ, "CalAM") = False
                            SiJourEnable(IndiceJ, "CalPM") = False
                        Case VI.NumeroPlage.Plage1_Absence, VI.NumeroPlage.Plage1_Formation, VI.NumeroPlage.Plage1_Mission
                            CalJourStyle(IndiceJ, "CalAM") = "solid"
                            CalBackColor(IndiceJ, "CalAM") = Couleur
                            CalDemiJourToolTip(IndiceJ, "CalAM") = FicheAbs.Intitule
                            SiJourEnable(IndiceJ, "CalAM") = False
                        Case VI.NumeroPlage.Plage2_Absence, VI.NumeroPlage.Plage2_Formation, VI.NumeroPlage.Plage2_Mission
                            CalJourStyle(IndiceJ, "CalAM") = "solid"
                            CalBackColor(IndiceJ, "CalPM") = Couleur
                            CalDemiJourToolTip(IndiceJ, "CalPM") = FicheAbs.Intitule
                            SiJourEnable(IndiceJ, "CalPM") = False
                    End Select
                Next
            End If
            '***** Les jours sélectionnés **************************
            If WsCouleurSelection.IsEmpty Then
                WsCouleurSelection = WsInstanceCouleur.Item(4).Couleur_Web
            End If
            If DateW = WsCtl_Cache.Date_Valeur Then
                If SiJourEnable(IndiceJ, "CalAM") = True Then
                    CalBackColor(IndiceJ, "CalAM") = WsCouleurSelection
                End If
                If SiJourEnable(IndiceJ, "CalPM") = True Then
                    CalBackColor(IndiceJ, "CalPM") = WsCouleurSelection
                End If
                If CalRadio.Visible = True Then
                    Select Case WsCtl_Cache.Coche_Selection
                        Case <> ""
                            If SiJourEnable(IndiceJ, WsCtl_Cache.Coche_Selection) = True Then
                                CalBackColor(IndiceJ, WsCtl_Cache.Coche_Selection) = WsCouleurSelection
                            Else
                                Call Initialiser()
                            End If
                    End Select
                End If
            ElseIf WsCtl_Cache.Date_Butoir <> "" AndAlso WsCtl_Cache.Date_Valeur <> "" Then
                If V_WebFonction.ViRhDates.DateTypee(DateW) <= V_WebFonction.ViRhDates.DateTypee(WsCtl_Cache.Date_Valeur) And V_WebFonction.ViRhDates.DateTypee(DateW) >= V_WebFonction.ViRhDates.DateTypee(WsCtl_Cache.Date_Butoir) Then
                    If SiJourEnable(IndiceJ, "CalAM") = True And WsCtl_Cache.Coche_Butoir = "Oui" Then
                        CalBackColor(IndiceJ, "CalAM") = WsCouleurSelection
                    End If
                    If SiJourEnable(IndiceJ, "CalPM") = True And WsCtl_Cache.Coche_Butoir = "Oui" Then
                        CalBackColor(IndiceJ, "CalPM") = WsCouleurSelection
                    End If
                End If
            End If
            '************************************************************************************
            If WsCtl_Cache.Date_Butoir <> "" Then
                If V_WebFonction.ViRhDates.DateTypee(DateW) < V_WebFonction.ViRhDates.DateTypee(WsCtl_Cache.Date_Butoir) Then
                    SiJourVisible(IndiceJ, "CalAM") = False
                    SiJourVisible(IndiceJ, "CalPM") = False
                End If
            End If
            DateW = V_WebFonction.ViRhDates.CalcDateMoinsJour(DateW, "1", "0")
            If CInt(Strings.Mid(DateW, 4, 2)) <> Mois Then
                For IndiceCtl = IndiceJ + 1 To 36
                    SiJourVisible(IndiceCtl, "CalAM") = False
                    SiJourVisible(IndiceCtl, "CalPM") = False
                Next IndiceCtl
                Exit Do
            End If
            NoJour = Weekday(DateValue(DateW), Microsoft.VisualBasic.FirstDayOfWeek.Monday) - 1
            IndiceJ += 1
        Loop
    End Sub

    Private Sub CmdOK_Click(sender As Object, e As EventArgs) Handles CmdOK.Click
        Dim Evenement As Virtualia.Systeme.Evenements.DonneeChangeEventArgs
        Evenement = New Virtualia.Systeme.Evenements.DonneeChangeEventArgs(DateSelectionnee(0))
        Saisie_Change(Evenement)
    End Sub

    Private Sub CmdCancel_Click(sender As Object, e As EventArgs) Handles CmdCancel.Click
        Dim Evenement As Virtualia.Systeme.Evenements.DonneeChangeEventArgs
        Evenement = New Virtualia.Systeme.Evenements.DonneeChangeEventArgs("")
        Saisie_Change(Evenement)
    End Sub
End Class