﻿'------------------------------------------------------------------------------
' <généré automatiquement>
'     Ce code a été généré par un outil.
'
'     Les modifications apportées à ce fichier peuvent provoquer un comportement incorrect et seront perdues si
'     le code est régénéré.
' </généré automatiquement>
'------------------------------------------------------------------------------

Option Strict On
Option Explicit On


Partial Public Class VNotificationPFR
    
    '''<summary>
    '''Contrôle PFR_PAGE_NOTIF.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents PFR_PAGE_NOTIF As Global.System.Web.UI.WebControls.Table
    
    '''<summary>
    '''Contrôle PFR_CadreInfo.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents PFR_CadreInfo As Global.System.Web.UI.WebControls.Table
    
    '''<summary>
    '''Contrôle PFR_CadreTitreNotification.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents PFR_CadreTitreNotification As Global.System.Web.UI.WebControls.Table
    
    '''<summary>
    '''Contrôle PFR_Etablissement.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents PFR_Etablissement As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle PFR_LieuDate.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents PFR_LieuDate As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle PFR_TitreNotif.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents PFR_TitreNotif As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle PFR_EtiAnneeEntretien.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents PFR_EtiAnneeEntretien As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle PFR_AnneeEntretien.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents PFR_AnneeEntretien As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle PFR_CadreEnteteNotif.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents PFR_CadreEnteteNotif As Global.System.Web.UI.WebControls.Table
    
    '''<summary>
    '''Contrôle PFR_EtiTitreAgent.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents PFR_EtiTitreAgent As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle PFR_EtiMatricule.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents PFR_EtiMatricule As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle PFR_Matricule.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents PFR_Matricule As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle PFR_EtiNom.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents PFR_EtiNom As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle PFR_Nom.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents PFR_Nom As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle PFR_EtiPrenom.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents PFR_EtiPrenom As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle PFR_Prenom.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents PFR_Prenom As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle PFR_EtiAffectationNiv1.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents PFR_EtiAffectationNiv1 As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle PFR_AffectationNiv1.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents PFR_AffectationNiv1 As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle PFR_EtiAffectationNiv2.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents PFR_EtiAffectationNiv2 As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle PFR_AffectationNiv2.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents PFR_AffectationNiv2 As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle PFR_EtiGrade.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents PFR_EtiGrade As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle PFR_Grade.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents PFR_Grade As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle PFR_EtiCoefficient.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents PFR_EtiCoefficient As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle PFR_Coefficient.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents PFR_Coefficient As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle PFR_CadreTableau.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents PFR_CadreTableau As Global.System.Web.UI.WebControls.Table
    
    '''<summary>
    '''Contrôle PFR_L1_C1.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents PFR_L1_C1 As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle PFR_L1_C2.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents PFR_L1_C2 As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle PFR_L1_C3.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents PFR_L1_C3 As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle PFR_L1_C4.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents PFR_L1_C4 As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle PFR_L1_C5.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents PFR_L1_C5 As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle PFR_L1_C6.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents PFR_L1_C6 As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle PFR_L1_C7.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents PFR_L1_C7 As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle PFR_L2_C1.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents PFR_L2_C1 As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle PFR_Col_15.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents PFR_Col_15 As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle PFR_Col_16.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents PFR_Col_16 As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle PFR_Col_31.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents PFR_Col_31 As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle PFR_Col_32.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents PFR_Col_32 As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle PFR_Col_23.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents PFR_Col_23 As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle PFR_Col_34.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents PFR_Col_34 As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle PFR_L3_C1.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents PFR_L3_C1 As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle PFR_Col_18.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents PFR_Col_18 As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle PFR_Col_19.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents PFR_Col_19 As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle PFR_L3_C4.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents PFR_L3_C4 As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle PFR_L3_C5.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents PFR_L3_C5 As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle PFR_Col_33.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents PFR_Col_33 As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle PFR_L3_C7.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents PFR_L3_C7 As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle PFR_L4_C1.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents PFR_L4_C1 As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle PFR_L4_C2.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents PFR_L4_C2 As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle PFR_L4_C3.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents PFR_L4_C3 As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle PFR_L4_C4.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents PFR_L4_C4 As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle PFR_L4_C5.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents PFR_L4_C5 As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle PFR_Col_24Ter.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents PFR_Col_24Ter As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle PFR_Col_33Bis.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents PFR_Col_33Bis As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle PFR_L5_C1.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents PFR_L5_C1 As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle PFR_Col_17Ter.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents PFR_Col_17Ter As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle PFR_L5_C3.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents PFR_L5_C3 As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle PFR_L5_C4.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents PFR_L5_C4 As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle PFR_L5_C5.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents PFR_L5_C5 As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle PFR_L5_C6.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents PFR_L5_C6 As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle PFR_Col_17Bis.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents PFR_Col_17Bis As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle PFR_L6_C1.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents PFR_L6_C1 As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle PFR_Col_24a.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents PFR_Col_24a As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle PFR_L6_C3.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents PFR_L6_C3 As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle PFR_L6_C4.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents PFR_L6_C4 As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle PFR_L6_C5.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents PFR_L6_C5 As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle PFR_L6_C6.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents PFR_L6_C6 As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle PFR_Col_33a.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents PFR_Col_33a As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle PFR_L7_C1.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents PFR_L7_C1 As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle PFR_L7_C2.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents PFR_L7_C2 As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle PFR_L7_C3.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents PFR_L7_C3 As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle PFR_L7_C4.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents PFR_L7_C4 As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle PFR_L7_C5.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents PFR_L7_C5 As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle PFR_L7_C6.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents PFR_L7_C6 As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle PFR_Col_34Bis.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents PFR_Col_34Bis As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle PFR_CadreBasPageNotification.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents PFR_CadreBasPageNotification As Global.System.Web.UI.WebControls.Table
    
    '''<summary>
    '''Contrôle PFR_BasPage1.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents PFR_BasPage1 As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle PFR_BasPage2.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents PFR_BasPage2 As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle PFR_CadreSignatureFAM.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents PFR_CadreSignatureFAM As Global.System.Web.UI.WebControls.Table
    
    '''<summary>
    '''Contrôle PFR_SignatureFAM.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents PFR_SignatureFAM As Global.System.Web.UI.WebControls.Table
End Class
