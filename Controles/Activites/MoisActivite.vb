﻿Option Explicit On
Option Strict On
Option Compare Text
Imports VI = Virtualia.Systeme.Constantes
Namespace WebAppli
    Namespace SelfV4
        Public Class MoisActivite
            Private WsParent As Virtualia.Net.Session.ObjetSession
            Private WsAnnee As Integer
            Private WsMois As Integer
            Private WsFiche_EtatCivil As Virtualia.TablesObjet.ShemaPER.PER_ETATCIVIL
            Private WsFiche_Statut As Virtualia.TablesObjet.ShemaPER.PER_STATUT = Nothing
            Private WsFiche_Position As Virtualia.TablesObjet.ShemaPER.PER_POSITION = Nothing
            Private WsFiche_Grade As Virtualia.TablesObjet.ShemaPER.PER_GRADE = Nothing
            Private WsFiche_Etablissement As Virtualia.TablesObjet.ShemaPER.PER_COLLECTIVITE = Nothing
            Private WsFiche_Affectation As Virtualia.TablesObjet.ShemaPER.PER_AFFECTATION = Nothing
            Private WsFiche_Cycle As Virtualia.TablesObjet.ShemaPER.PER_TRAVAIL = Nothing
            Private WsETPActivite As Double
            Private WsListeBulletinPaie As List(Of Virtualia.TablesObjet.ShemaPER.PER_BULLETIN) = Nothing
            Private WsListeActivites As List(Of Virtualia.TablesObjet.ShemaPER.PER_ACTIVITE_JOUR) = Nothing

            Public ReadOnly Property Lignes_Grid_No1(ByVal Separateur As String) As List(Of String)
                Get
                    Dim LstLignes As List(Of String)
                    Dim DatasMois As System.Text.StringBuilder
                    Dim DatasJour As System.Text.StringBuilder
                    Dim SiRecup As Boolean = False
                    Dim Chaine As String

                    DatasMois = New System.Text.StringBuilder

                    DatasMois.Append(Annee & Separateur)
                    DatasMois.Append(Mois & Separateur)
                    DatasMois.Append(Identifiant & Separateur)
                    DatasMois.Append(Nom & Separateur)
                    DatasMois.Append(Prenom & Separateur)
                    DatasMois.Append(Emploi_Exerce & Separateur)
                    DatasMois.Append(Statut & Separateur)
                    DatasMois.Append(Position & Separateur)
                    DatasMois.Append(Modalite & Separateur)
                    DatasMois.Append(Taux_Activite & Separateur)
                    DatasMois.Append(Cycle_Travail & Separateur)
                    DatasMois.Append(Horaire_Journalier & Separateur)
                    DatasMois.Append(Corps & Separateur)
                    DatasMois.Append(Grade & Separateur)
                    DatasMois.Append(IndiceMajore & Separateur)
                    DatasMois.Append(Etablissement & Separateur)
                    DatasMois.Append(Structure_N1 & Separateur)
                    DatasMois.Append(Structure_N2 & Separateur)
                    DatasMois.Append(Structure_N3 & Separateur)
                    DatasMois.Append(Structure_N4 & Separateur)
                    DatasMois.Append(Structure_N5 & Separateur)
                    DatasMois.Append(Structure_N6 & Separateur)
                    DatasMois.Append(ETP_Activite & Separateur)
                    DatasMois.Append(MasseSalariale & Separateur)

                    LstLignes = New List(Of String)
                    For Each FicheJour In WsListeActivites
                        If FicheJour.Activite = "NON TRAVAILLE" And FicheJour.Tache_Etape.StartsWith("Récupération") Then
                            SiRecup = True
                        Else
                            DatasJour = New System.Text.StringBuilder
                            DatasJour.Append(FicheJour.Activite & Separateur)
                            Chaine = FicheJour.Tache_Etape.Replace(Separateur, ":")
                            If Chaine.Contains(vbCrLf) Then
                                DatasJour.Append(Chaine.Replace(vbCrLf, "_") & Separateur)
                            ElseIf Chaine.Contains(vbLf) Then
                                DatasJour.Append(Chaine.Replace(vbLf, "_") & Separateur)
                            Else
                                DatasJour.Append(Chaine & Separateur)
                            End If
                            Chaine = FicheJour.Precision.Replace(Separateur, ":")
                            If Chaine.Contains(vbCrLf) Then
                                DatasJour.Append(Chaine.Replace(vbCrLf, "_") & Separateur)
                            ElseIf Chaine.Contains(vbLf) Then
                                DatasJour.Append(Chaine.Replace(vbLf, "_") & Separateur)
                            Else
                                DatasJour.Append(Chaine & Separateur)
                            End If
                            DatasJour.Append(FicheJour.Pourcentage_Repartition)
                            LstLignes.Add(DatasMois.ToString & DatasJour.ToString)
                        End If
                    Next
                    If SiRecup = True And LstLignes.Count = 0 Then
                        Return Nothing
                    End If
                    Return LstLignes
                End Get
            End Property

            Public ReadOnly Property Entete_Grid_No1(ByVal Separateur As String) As String
                Get
                    Dim Chaine As System.Text.StringBuilder
                    Chaine = New System.Text.StringBuilder
                    Chaine.Append("Année" & Separateur)
                    Chaine.Append("Mois" & Separateur)
                    Chaine.Append("Identifiant" & Separateur)
                    Chaine.Append("Nom" & Separateur)
                    Chaine.Append("Prénom" & Separateur)
                    Chaine.Append("Emploi" & Separateur)
                    Chaine.Append("Statut" & Separateur)
                    Chaine.Append("Position" & Separateur)
                    Chaine.Append("Modalité" & Separateur)
                    Chaine.Append("Taux_Activité" & Separateur)
                    Chaine.Append("Cycle de travail" & Separateur)
                    Chaine.Append("Horaire journalier" & Separateur)
                    Chaine.Append("Corps" & Separateur)
                    Chaine.Append("Grade" & Separateur)
                    Chaine.Append("Indice majoré" & Separateur)
                    Chaine.Append("Etablissement" & Separateur)
                    Chaine.Append("Niveau 1" & Separateur)
                    Chaine.Append("Niveau 2" & Separateur)
                    Chaine.Append("Niveau 3" & Separateur)
                    Chaine.Append("Niveau 4" & Separateur)
                    Chaine.Append("Niveau 5" & Separateur)
                    Chaine.Append("Niveau 6" & Separateur)
                    Chaine.Append("ETPT" & Separateur)
                    Chaine.Append("Masse salariale" & Separateur)
                    Chaine.Append("Projet" & Separateur)
                    Chaine.Append("Etape" & Separateur)
                    Chaine.Append("Précision" & Separateur)
                    Chaine.Append("Pourcentage de répartition")
                    Return Chaine.ToString
                End Get
            End Property

            Public ReadOnly Property SiOK As Boolean
                Get
                    If WsListeActivites Is Nothing OrElse WsListeActivites.Count = 0 Then
                        Return False
                    End If
                    Return True
                End Get
            End Property

            Public ReadOnly Property Annee As Integer
                Get
                    Return WsAnnee
                End Get
            End Property

            Public ReadOnly Property Mois As Integer
                Get
                    Return WsMois
                End Get
            End Property

            Public ReadOnly Property Date_Debut As String
                Get
                    Return "01/" & Strings.Format(WsMois, "00") & "/" & WsAnnee
                End Get
            End Property

            Public ReadOnly Property Date_Fin As String
                Get
                    Return WsParent.V_RhDates.DateSaisieVerifiee("31/" & Strings.Format(WsMois, "00") & "/" & WsAnnee)
                End Get
            End Property

            Public ReadOnly Property NombreJoursOuvres As Integer
                Get
                    Return WsParent.V_RhDates.NombredeJoursTravailles(CStr(Annee), Mois)
                End Get
            End Property

            Public ReadOnly Property NombreJoursDeclares As Integer
                Get
                    Dim LstJours As List(Of String)
                    If WsListeActivites Is Nothing OrElse WsListeActivites.Count = 0 Then
                        Return 0
                    End If
                    LstJours = (From instance In WsListeActivites Select instance.Date_de_Valeur).Distinct.ToList
                    Return LstJours.Count
                End Get
            End Property

            Public ReadOnly Property NombreJoursDeclares(ByVal Activite As String, ByVal Etape As String) As Integer
                Get
                    Dim LstJours As List(Of String)
                    Dim LstActivites As List(Of Virtualia.TablesObjet.ShemaPER.PER_ACTIVITE_JOUR)
                    If WsListeActivites Is Nothing OrElse WsListeActivites.Count = 0 Then
                        Return 0
                    End If
                    LstActivites = (From instance In WsListeActivites Where instance.Activite = Activite And instance.Tache_Etape = Etape).ToList
                    If LstActivites Is Nothing OrElse LstActivites.Count = 0 Then
                        Return 0
                    End If
                    LstJours = (From instance In LstActivites Select instance.Date_de_Valeur).Distinct.ToList
                    Return LstJours.Count
                End Get
            End Property

            Public ReadOnly Property Identifiant As Integer
                Get
                    Return WsFiche_EtatCivil.Ide_Dossier
                End Get
            End Property

            Public ReadOnly Property Nom As String
                Get
                    Return WsFiche_EtatCivil.Nom
                End Get
            End Property

            Public ReadOnly Property Prenom As String
                Get
                    Return WsFiche_EtatCivil.Prenom
                End Get
            End Property

            Public ReadOnly Property Statut As String
                Get
                    If WsFiche_Statut Is Nothing Then
                        Return ""
                    End If
                    Return WsFiche_Statut.Statut
                End Get
            End Property

            Public ReadOnly Property Position As String
                Get
                    If WsFiche_Position Is Nothing Then
                        Return ""
                    End If
                    Return WsFiche_Position.Position
                End Get
            End Property

            Public ReadOnly Property Modalite As String
                Get
                    If WsFiche_Position Is Nothing Then
                        Return ""
                    End If
                    Return WsFiche_Position.Modaliteservice
                End Get
            End Property

            Public ReadOnly Property Taux_Activite As String
                Get
                    If WsFiche_Position Is Nothing Then
                        Return ""
                    End If
                    Return WsFiche_Position.Taux_d_activite
                End Get
            End Property

            Public ReadOnly Property Cycle_Travail As String
                Get
                    If WsFiche_Cycle Is Nothing Then
                        Return ""
                    End If
                    Return WsFiche_Cycle.CycledeTravail
                End Get
            End Property
            Public ReadOnly Property Horaire_Journalier As String
                Get
                    If WsFiche_Cycle Is Nothing Then
                        Return ""
                    End If
                    Return WsFiche_Cycle.HoraireJournalier
                End Get
            End Property
            Public ReadOnly Property Corps As String
                Get
                    If WsFiche_Grade Is Nothing Then
                        Return ""
                    End If
                    Return WsFiche_Grade.Corps
                End Get
            End Property

            Public ReadOnly Property Grade As String
                Get
                    If WsFiche_Grade Is Nothing Then
                        Return ""
                    End If
                    Return WsFiche_Grade.Grade
                End Get
            End Property

            Public ReadOnly Property Categorie As String
                Get
                    If WsFiche_Grade Is Nothing Then
                        Return ""
                    End If
                    Return WsFiche_Grade.Categorie
                End Get
            End Property

            Public ReadOnly Property IndiceMajore As String
                Get
                    If WsFiche_Grade Is Nothing Then
                        Return ""
                    End If
                    Return WsFiche_Grade.Indice_majore
                End Get
            End Property

            Public ReadOnly Property Etablissement As String
                Get
                    If WsFiche_Etablissement Is Nothing Then
                        Return ""
                    End If
                    Return WsFiche_Etablissement.Administration
                End Get
            End Property
            Public ReadOnly Property Emploi_Exerce As String
                Get
                    If WsFiche_Affectation Is Nothing Then
                        Return ""
                    End If
                    Return WsFiche_Affectation.Fonction_exercee
                End Get
            End Property
            Public ReadOnly Property Structure_N1 As String
                Get
                    If WsFiche_Affectation Is Nothing Then
                        Return ""
                    End If
                    Return WsFiche_Affectation.Structure_de_1er_niveau
                End Get
            End Property

            Public ReadOnly Property Structure_N2 As String
                Get
                    If WsFiche_Affectation Is Nothing Then
                        Return ""
                    End If
                    Return WsFiche_Affectation.Structure_de_2e_niveau
                End Get
            End Property

            Public ReadOnly Property Structure_N3 As String
                Get
                    If WsFiche_Affectation Is Nothing Then
                        Return ""
                    End If
                    Return WsFiche_Affectation.Structure_de_3e_niveau
                End Get
            End Property

            Public ReadOnly Property Structure_N4 As String
                Get
                    If WsFiche_Affectation Is Nothing Then
                        Return ""
                    End If
                    Return WsFiche_Affectation.Structure_de_4e_niveau
                End Get
            End Property

            Public ReadOnly Property Structure_N5 As String
                Get
                    If WsFiche_Affectation Is Nothing Then
                        Return ""
                    End If
                    Return WsFiche_Affectation.Structure_de_5e_niveau
                End Get
            End Property

            Public ReadOnly Property Nom_Prenom_Manager As String
                Get
                    If WsListeActivites Is Nothing OrElse WsListeActivites.Count = 0 Then
                        Return ""
                    End If
                    Return WsListeActivites.Item(WsListeActivites.Count - 1).Visa
                End Get
            End Property

            Public ReadOnly Property Structure_N6 As String
                Get
                    If WsFiche_Affectation Is Nothing Then
                        Return ""
                    End If
                    Return WsFiche_Affectation.Structure_de_6e_niveau
                End Get
            End Property

            Public Property ETP_Activite() As Double
                Get
                    Return Math.Round(WsETPActivite, 3)
                End Get
                Set(value As Double)
                    WsETPActivite = value
                End Set
            End Property

            Public ReadOnly Property MasseSalariale As Double
                Get
                    If WsListeBulletinPaie Is Nothing Then
                        Return 0
                    End If
                    Return Math.Round(Cumuls_Virtualia("BRUT") + Cumuls_Virtualia("PP"), 2)
                End Get
            End Property

            Private ReadOnly Property Cumuls_Virtualia(ByVal CodeRegroupement As String) As Double
                Get
                    Dim LstBul As List(Of Virtualia.TablesObjet.ShemaPER.PER_BULLETIN)
                    Dim CatMini As Integer
                    Dim CatMaxi As Integer = 19
                    Dim MontantTotal As Double = 0

                    If WsListeBulletinPaie Is Nothing Then
                        Return 0
                    End If

                    Select Case CodeRegroupement
                        Case Is = "BRUT"
                            CatMini = 10
                            CatMaxi = 19
                        Case Is = "IMPOT"
                            CatMini = 50
                            CatMaxi = 50
                        Case Is = "PO"
                            CatMini = 20
                            CatMaxi = 29
                        Case Is = "PP"
                            CatMini = 30
                            CatMaxi = 39
                        Case Else
                            Return 0
                    End Select
                    LstBul = (From Ligne In WsListeBulletinPaie Where (Ligne.Categorie >= CatMini And Ligne.Categorie <= CatMaxi)
                                                   Order By Ligne.Date_Valeur_ToDate Ascending).ToList

                    If LstBul Is Nothing OrElse LstBul.Count = 0 Then
                        Return 0
                    End If
                    For Each BulPaie In LstBul
                        MontantTotal += BulPaie.Montant
                    Next
                    Return Math.Round(MontantTotal, 2)
                End Get
            End Property

            Public Sub New(ByVal SessionVirtualia As Virtualia.Net.Session.ObjetSession, ByVal AnneeDemandee As Integer, ByVal MoisDemande As Integer,
                           ByVal SiValidees As Boolean, ByVal ListeFiches As List(Of Virtualia.Systeme.MetaModele.VIR_FICHE))
                Dim Predicat As Virtualia.Systeme.MetaModele.Predicats.PredicateFiche
                Dim TabObjet As List(Of Integer)
                Dim LstRes As List(Of Virtualia.Systeme.MetaModele.VIR_FICHE)
                Dim LstActivites As List(Of Virtualia.TablesObjet.ShemaPER.PER_ACTIVITE_JOUR)
                Dim FicheJour As Virtualia.TablesObjet.ShemaPER.PER_ACTIVITE_JOUR
                Dim LstAbsences As List(Of Virtualia.TablesObjet.ShemaPER.PER_ABSENCE) = Nothing
                Dim LstFormations As List(Of Virtualia.TablesObjet.ShemaPER.PER_SESSION) = Nothing
                Dim DateW As String
                Dim SiOK As Boolean
                Dim CyclePrevu As Virtualia.Net.Controles.ItemJourCalendrier

                WsParent = SessionVirtualia
                WsAnnee = AnneeDemandee
                WsMois = MoisDemande

                TabObjet = New List(Of Integer)
                TabObjet.Add(VI.ObjetPer.ObaCivil)
                Predicat = New Virtualia.Systeme.MetaModele.Predicats.PredicateFiche(TabObjet)
                WsFiche_EtatCivil = CType(ListeFiches.Find(AddressOf Predicat.SiFicheDansListeObjets), Virtualia.TablesObjet.ShemaPER.PER_ETATCIVIL)

                TabObjet = New List(Of Integer)
                TabObjet.Add(VI.ObjetPer.ObaStatut)
                Predicat = New Virtualia.Systeme.MetaModele.Predicats.PredicateFiche(TabObjet)
                WsFiche_Statut = CType(ListeFiches.Find(AddressOf Predicat.SiFicheDansListeObjets), Virtualia.TablesObjet.ShemaPER.PER_STATUT)
                If WsFiche_Statut Is Nothing Then
                    LstRes = WsParent.V_PointeurGlobal.SelectionObjetValable(Identifiant, Date_Debut, VI.ObjetPer.ObaStatut)
                    If LstRes IsNot Nothing Then
                        WsFiche_Statut = CType(LstRes.Item(0), Virtualia.TablesObjet.ShemaPER.PER_STATUT)
                    End If
                End If

                TabObjet = New List(Of Integer)
                TabObjet.Add(VI.ObjetPer.ObaActivite)
                Predicat = New Virtualia.Systeme.MetaModele.Predicats.PredicateFiche(TabObjet)
                WsFiche_Position = CType(ListeFiches.Find(AddressOf Predicat.SiFicheDansListeObjets), Virtualia.TablesObjet.ShemaPER.PER_POSITION)
                If WsFiche_Position Is Nothing Then
                    LstRes = WsParent.V_PointeurGlobal.SelectionObjetValable(Identifiant, Date_Debut, VI.ObjetPer.ObaActivite)
                    If LstRes IsNot Nothing Then
                        WsFiche_Position = CType(LstRes.Item(0), Virtualia.TablesObjet.ShemaPER.PER_POSITION)
                    End If
                End If

                TabObjet = New List(Of Integer)
                TabObjet.Add(VI.ObjetPer.ObaGrade)
                Predicat = New Virtualia.Systeme.MetaModele.Predicats.PredicateFiche(TabObjet)
                WsFiche_Grade = CType(ListeFiches.Find(AddressOf Predicat.SiFicheDansListeObjets), Virtualia.TablesObjet.ShemaPER.PER_GRADE)

                TabObjet = New List(Of Integer)
                TabObjet.Add(VI.ObjetPer.ObaSociete)
                Predicat = New Virtualia.Systeme.MetaModele.Predicats.PredicateFiche(TabObjet)
                WsFiche_Etablissement = CType(ListeFiches.Find(AddressOf Predicat.SiFicheDansListeObjets), Virtualia.TablesObjet.ShemaPER.PER_COLLECTIVITE)
                If WsFiche_Etablissement Is Nothing Then
                    LstRes = WsParent.V_PointeurGlobal.SelectionObjetValable(Identifiant, Date_Debut, VI.ObjetPer.ObaSociete)
                    If LstRes IsNot Nothing Then
                        WsFiche_Etablissement = CType(LstRes.Item(0), Virtualia.TablesObjet.ShemaPER.PER_COLLECTIVITE)
                    End If
                End If

                TabObjet = New List(Of Integer)
                TabObjet.Add(VI.ObjetPer.ObaOrganigramme)
                Predicat = New Virtualia.Systeme.MetaModele.Predicats.PredicateFiche(TabObjet)
                WsFiche_Affectation = CType(ListeFiches.Find(AddressOf Predicat.SiFicheDansListeObjets), Virtualia.TablesObjet.ShemaPER.PER_AFFECTATION)
                If WsFiche_Affectation Is Nothing Then
                    LstRes = WsParent.V_PointeurGlobal.SelectionObjetValable(Identifiant, Date_Debut, VI.ObjetPer.ObaOrganigramme)
                    If LstRes IsNot Nothing Then
                        WsFiche_Affectation = CType(LstRes.Item(0), Virtualia.TablesObjet.ShemaPER.PER_AFFECTATION)
                    End If
                End If

                TabObjet = New List(Of Integer)
                TabObjet.Add(VI.ObjetPer.ObaPresence)
                Predicat = New Virtualia.Systeme.MetaModele.Predicats.PredicateFiche(TabObjet)
                WsFiche_Cycle = CType(ListeFiches.Find(AddressOf Predicat.SiFicheDansListeObjets), Virtualia.TablesObjet.ShemaPER.PER_TRAVAIL)

                WsListeActivites = New List(Of Virtualia.TablesObjet.ShemaPER.PER_ACTIVITE_JOUR)

                TabObjet = New List(Of Integer)
                TabObjet.Add(112)
                Predicat = New Virtualia.Systeme.MetaModele.Predicats.PredicateFiche(TabObjet)
                LstRes = ListeFiches.FindAll(AddressOf Predicat.SiFicheDansListeObjets)
                If LstRes IsNot Nothing AndAlso LstRes.Count > 0 Then
                    LstActivites = WsParent.V_RhFonction.ConvertisseurListeFiches(Of Virtualia.TablesObjet.ShemaPER.PER_ACTIVITE_JOUR)(LstRes)
                    For Each FichePER In LstActivites
                        Select Case SiValidees
                            Case True
                                If FichePER.Visa <> "" Then
                                    WsListeActivites.Add(FichePER)
                                End If
                            Case False
                                WsListeActivites.Add(FichePER)
                        End Select
                    Next
                End If

                TabObjet = New List(Of Integer)
                TabObjet.Add(VI.ObjetPer.ObaBulletin)
                Predicat = New Virtualia.Systeme.MetaModele.Predicats.PredicateFiche(TabObjet)
                LstRes = ListeFiches.FindAll(AddressOf Predicat.SiFicheDansListeObjets)
                If LstRes IsNot Nothing Then
                    WsListeBulletinPaie = WsParent.V_RhFonction.ConvertisseurListeFiches(Of Virtualia.TablesObjet.ShemaPER.PER_BULLETIN)(LstRes)
                End If

                TabObjet = New List(Of Integer)
                TabObjet.Add(VI.ObjetPer.ObaAbsence)
                Predicat = New Virtualia.Systeme.MetaModele.Predicats.PredicateFiche(TabObjet)
                LstRes = ListeFiches.FindAll(AddressOf Predicat.SiFicheDansListeObjets)
                If LstRes IsNot Nothing Then
                    LstAbsences = WsParent.V_RhFonction.ConvertisseurListeFiches(Of Virtualia.TablesObjet.ShemaPER.PER_ABSENCE)(LstRes)
                    For Each FichePER In LstAbsences
                        DateW = FichePER.Date_de_Valeur
                        Do
                            SiOK = False
                            If WsParent.V_PointeurGlobal.VirRhDates.DateTypee(DateW) >= CDate("01/" & Strings.Format(WsMois, "00") & " / " & WsAnnee) And
                                WsParent.V_PointeurGlobal.VirRhDates.DateTypee(DateW) <= CDate(WsParent.V_PointeurGlobal.VirRhDates.DateSaisieVerifiee("31/" & Strings.Format(WsMois, "00") & " / " & WsAnnee)) Then
                                SiOK = Not (FichePER.Nature.StartsWith("Mission"))
                            End If
                            If SiOK = True Then
                                SiOK = WsParent.V_PointeurGlobal.VirRhDates.SiJourOuvre(DateW, True)
                            End If
                            If SiOK = True Then
                                If WsFiche_Cycle IsNot Nothing Then
                                    CyclePrevu = CType(WsParent.V_PointeurGlobal, Virtualia.Net.Session.ObjetGlobal).VirReferentiel.CyclePrevisionnel(DateW, WsFiche_Cycle.CycledeTravail, WsFiche_Cycle.Date_de_Valeur)
                                    If CyclePrevu IsNot Nothing Then
                                        Select Case CyclePrevu.Caracteristique
                                            Case VI.NumeroPlage.Jour_Absence
                                                SiOK = False
                                        End Select
                                    End If
                                End If
                            End If
                            If SiOK = True Then
                                FicheJour = New Virtualia.TablesObjet.ShemaPER.PER_ACTIVITE_JOUR
                                FicheJour.Date_de_Valeur = DateW
                                FicheJour.Activite = "NON TRAVAILLE"
                                FicheJour.Tache_Etape = FichePER.Nature
                                FicheJour.Precision = ""
                                Select Case FichePER.VTypeJour(DateW)
                                    Case VI.NumeroPlage.Jour_Absence, VI.NumeroPlage.Jour_Formation
                                        FicheJour.Pourcentage_Repartition = 100
                                    Case Else
                                        FicheJour.Pourcentage_Repartition = 50
                                End Select
                                WsListeActivites.Add(FicheJour)
                            End If
                            Try
                                DateW = WsParent.V_PointeurGlobal.VirRhDates.CalcDateMoinsJour(DateW, "1", "0")
                            Catch ex As Exception 'Cas improbable des dates de fin à NULL
                                Exit Do
                            End Try
                            If WsParent.V_PointeurGlobal.VirRhDates.DateTypee(DateW) > FichePER.Date_Fin_ToDate Then
                                Exit Do
                            End If
                        Loop
                    Next
                End If

                TabObjet = New List(Of Integer)
                TabObjet.Add(VI.ObjetPer.ObaFormation)
                Predicat = New Virtualia.Systeme.MetaModele.Predicats.PredicateFiche(TabObjet)
                LstRes = ListeFiches.FindAll(AddressOf Predicat.SiFicheDansListeObjets)
                If LstRes IsNot Nothing Then
                    LstFormations = WsParent.V_RhFonction.ConvertisseurListeFiches(Of Virtualia.TablesObjet.ShemaPER.PER_SESSION)(LstRes)
                    For Each FichePER In LstFormations
                        DateW = FichePER.Date_de_Valeur
                        Do
                            SiOK = False
                            If WsParent.V_PointeurGlobal.VirRhDates.DateTypee(DateW) >= CDate("01/" & Strings.Format(WsMois, "00") & " / " & WsAnnee) And
                                WsParent.V_PointeurGlobal.VirRhDates.DateTypee(DateW) <= CDate(WsParent.V_PointeurGlobal.VirRhDates.DateSaisieVerifiee("31/" & Strings.Format(WsMois, "00") & " / " & WsAnnee)) Then
                                SiOK = WsParent.V_PointeurGlobal.VirRhDates.SiJourOuvre(DateW, True)
                            End If
                            If SiOK = True Then
                                FicheJour = New Virtualia.TablesObjet.ShemaPER.PER_ACTIVITE_JOUR
                                FicheJour.Date_de_Valeur = DateW
                                FicheJour.Activite = "NON TRAVAILLE"
                                FicheJour.Tache_Etape = "FORMATION"
                                FicheJour.Precision = FichePER.Intitule
                                FicheJour.Pourcentage_Repartition = 100
                                WsListeActivites.Add(FicheJour)
                            End If
                            DateW = WsParent.V_PointeurGlobal.VirRhDates.CalcDateMoinsJour(DateW, "1", "0")
                            If WsParent.V_PointeurGlobal.VirRhDates.DateTypee(DateW) > FichePER.Date_Fin_ToDate Then
                                Exit Do
                            End If
                        Loop
                    Next
                End If

                If WsListeActivites Is Nothing OrElse WsListeActivites.Count = 0 Then
                    Exit Sub
                End If
                Dim RuptClef As String
                Dim RuptActivite As String = ""
                Dim RuptEtape As String = ""
                Dim RuptPrecision As String = ""
                Dim Total As Double = 0
                LstActivites = (From FichePER In WsListeActivites Select FichePER Order By FichePER.ClefConcatenee).ToList
                WsListeActivites = New List(Of Virtualia.TablesObjet.ShemaPER.PER_ACTIVITE_JOUR)
                RuptClef = LstActivites.Item(0).ClefConcatenee
                For Each FichePER In LstActivites
                    If FichePER.ClefConcatenee <> RuptClef Then
                        FicheJour = New Virtualia.TablesObjet.ShemaPER.PER_ACTIVITE_JOUR
                        FicheJour.Date_de_Valeur = "01/" & Strings.Format(WsMois, "00") & " / " & WsAnnee
                        FicheJour.Activite = RuptActivite
                        FicheJour.Tache_Etape = RuptEtape
                        FicheJour.Precision = RuptPrecision
                        FicheJour.Pourcentage_Repartition = Total
                        WsListeActivites.Add(FicheJour)
                        Total = 0
                    End If
                    RuptActivite = FichePER.Activite
                    RuptEtape = FichePER.Tache_Etape
                    RuptPrecision = FichePER.Precision
                    Total += FichePER.Pourcentage_Repartition
                    RuptClef = FichePER.ClefConcatenee
                Next
                If Total > 0 Then
                    FicheJour = New Virtualia.TablesObjet.ShemaPER.PER_ACTIVITE_JOUR
                    FicheJour.Date_de_Valeur = "01/" & Strings.Format(WsMois, "00") & " / " & WsAnnee
                    FicheJour.Activite = RuptActivite
                    FicheJour.Tache_Etape = RuptEtape
                    FicheJour.Precision = RuptPrecision
                    FicheJour.Pourcentage_Repartition = Total
                    WsListeActivites.Add(FicheJour)
                End If
            End Sub

        End Class
    End Namespace
End Namespace