﻿'------------------------------------------------------------------------------
' <généré automatiquement>
'     Ce code a été généré par un outil.
'
'     Les modifications apportées à ce fichier peuvent provoquer un comportement incorrect et seront perdues si
'     le code est régénéré.
' </généré automatiquement>
'------------------------------------------------------------------------------

Option Strict On
Option Explicit On


Partial Public Class CtlLectureActivite
    
    '''<summary>
    '''Contrôle CadreGlobal.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CadreGlobal As Global.System.Web.UI.WebControls.Table
    
    '''<summary>
    '''Contrôle CadreElement_01.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CadreElement_01 As Global.System.Web.UI.WebControls.Table
    
    '''<summary>
    '''Contrôle CadreIntitule_01.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CadreIntitule_01 As Global.System.Web.UI.WebControls.Table
    
    '''<summary>
    '''Contrôle CellEtiActivite_01.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CellEtiActivite_01 As Global.System.Web.UI.WebControls.TableCell
    
    '''<summary>
    '''Contrôle EtiActivite_01.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents EtiActivite_01 As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle CellDonActivite_01.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CellDonActivite_01 As Global.System.Web.UI.WebControls.TableCell
    
    '''<summary>
    '''Contrôle DonActivite_01.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents DonActivite_01 As Global.System.Web.UI.WebControls.TextBox
    
    '''<summary>
    '''Contrôle CellEtiProduit_01.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CellEtiProduit_01 As Global.System.Web.UI.WebControls.TableCell
    
    '''<summary>
    '''Contrôle EtiProduit_01.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents EtiProduit_01 As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle CellDonProduit_01.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CellDonProduit_01 As Global.System.Web.UI.WebControls.TableCell
    
    '''<summary>
    '''Contrôle DonProduit_01.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents DonProduit_01 As Global.System.Web.UI.WebControls.TextBox
    
    '''<summary>
    '''Contrôle CellEtiPrecision_01.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CellEtiPrecision_01 As Global.System.Web.UI.WebControls.TableCell
    
    '''<summary>
    '''Contrôle EtiPrecision_01.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents EtiPrecision_01 As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle CellDonPrecision_01.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CellDonPrecision_01 As Global.System.Web.UI.WebControls.TableCell
    
    '''<summary>
    '''Contrôle DonPrecision_01.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents DonPrecision_01 As Global.System.Web.UI.WebControls.TextBox
    
    '''<summary>
    '''Contrôle CadreRepartition_01.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CadreRepartition_01 As Global.System.Web.UI.WebControls.Table
    
    '''<summary>
    '''Contrôle CellDateLundi_01.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CellDateLundi_01 As Global.System.Web.UI.WebControls.TableCell
    
    '''<summary>
    '''Contrôle EtiDateLundi_01.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents EtiDateLundi_01 As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle CellDateMardi_01.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CellDateMardi_01 As Global.System.Web.UI.WebControls.TableCell
    
    '''<summary>
    '''Contrôle EtiDateMardi_01.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents EtiDateMardi_01 As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle CellDateMercredi_01.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CellDateMercredi_01 As Global.System.Web.UI.WebControls.TableCell
    
    '''<summary>
    '''Contrôle EtiDateMercredi_01.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents EtiDateMercredi_01 As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle CellDateJeudi_01.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CellDateJeudi_01 As Global.System.Web.UI.WebControls.TableCell
    
    '''<summary>
    '''Contrôle EtiDateJeudi_01.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents EtiDateJeudi_01 As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle CellDateVendredi_01.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CellDateVendredi_01 As Global.System.Web.UI.WebControls.TableCell
    
    '''<summary>
    '''Contrôle EtiDateVendredi_01.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents EtiDateVendredi_01 As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle CellDateSamedi_01.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CellDateSamedi_01 As Global.System.Web.UI.WebControls.TableCell
    
    '''<summary>
    '''Contrôle EtiDateSamedi_01.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents EtiDateSamedi_01 As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle CellDateDimanche_01.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CellDateDimanche_01 As Global.System.Web.UI.WebControls.TableCell
    
    '''<summary>
    '''Contrôle EtiDateDimanche_01.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents EtiDateDimanche_01 As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle CellDateSemaine_01.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CellDateSemaine_01 As Global.System.Web.UI.WebControls.TableCell
    
    '''<summary>
    '''Contrôle EtiDateSemaine_01.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents EtiDateSemaine_01 As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle CellLundi_01.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CellLundi_01 As Global.System.Web.UI.WebControls.TableCell
    
    '''<summary>
    '''Contrôle EtiLundi_01.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents EtiLundi_01 As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle CellMardi_01.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CellMardi_01 As Global.System.Web.UI.WebControls.TableCell
    
    '''<summary>
    '''Contrôle EtiMardi_01.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents EtiMardi_01 As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle CellMercredi_01.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CellMercredi_01 As Global.System.Web.UI.WebControls.TableCell
    
    '''<summary>
    '''Contrôle EtiMercredi_01.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents EtiMercredi_01 As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle CellJeudi_01.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CellJeudi_01 As Global.System.Web.UI.WebControls.TableCell
    
    '''<summary>
    '''Contrôle EtiJeudi_01.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents EtiJeudi_01 As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle Cell01_Vendredi.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Cell01_Vendredi As Global.System.Web.UI.WebControls.TableCell
    
    '''<summary>
    '''Contrôle Eti01_Vendredi.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Eti01_Vendredi As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle CellSamedi_01.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CellSamedi_01 As Global.System.Web.UI.WebControls.TableCell
    
    '''<summary>
    '''Contrôle EtiSamedi_01.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents EtiSamedi_01 As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle CellDimanche_01.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CellDimanche_01 As Global.System.Web.UI.WebControls.TableCell
    
    '''<summary>
    '''Contrôle EtiDimanche_01.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents EtiDimanche_01 As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle CellSemaine_01.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CellSemaine_01 As Global.System.Web.UI.WebControls.TableCell
    
    '''<summary>
    '''Contrôle EtiSemaine_01.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents EtiSemaine_01 As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle Donnee_01_00.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Donnee_01_00 As Global.System.Web.UI.WebControls.TextBox
    
    '''<summary>
    '''Contrôle Donnee_01_01.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Donnee_01_01 As Global.System.Web.UI.WebControls.TextBox
    
    '''<summary>
    '''Contrôle Donnee_01_02.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Donnee_01_02 As Global.System.Web.UI.WebControls.TextBox
    
    '''<summary>
    '''Contrôle Donnee_01_03.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Donnee_01_03 As Global.System.Web.UI.WebControls.TextBox
    
    '''<summary>
    '''Contrôle Donnee_01_04.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Donnee_01_04 As Global.System.Web.UI.WebControls.TextBox
    
    '''<summary>
    '''Contrôle Donnee_01_05.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Donnee_01_05 As Global.System.Web.UI.WebControls.TextBox
    
    '''<summary>
    '''Contrôle Donnee_01_06.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Donnee_01_06 As Global.System.Web.UI.WebControls.TextBox
    
    '''<summary>
    '''Contrôle CellModifier_01.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CellModifier_01 As Global.System.Web.UI.WebControls.TableCell
    
    '''<summary>
    '''Contrôle CmdModifier_01.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CmdModifier_01 As Global.System.Web.UI.WebControls.ImageButton
    
    '''<summary>
    '''Contrôle CmdSupprimer_01.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CmdSupprimer_01 As Global.System.Web.UI.WebControls.ImageButton
End Class
