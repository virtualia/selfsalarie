﻿Option Explicit On
Option Strict On
Option Compare Text
Imports VI = Virtualia.Systeme.Constantes
Imports Virtualia.Systeme.Sgbd.Sql
Namespace WebAppli
    Namespace SelfV4
        Public Class PlanFormation
            Private WsParent As Virtualia.Net.Session.SessionModule
            Private WsListeDesFormations As List(Of Virtualia.Net.WebAppli.SelfV4.DossierFormation)

            Public ReadOnly Property FicheSession(ByVal Ide As Integer, ByVal DateValeur As String) As Virtualia.Net.WebAppli.SelfV4.DossierFormation
                Get
                    If WsListeDesFormations Is Nothing OrElse WsListeDesFormations.Count = 0 Then
                        Return Nothing
                    End If
                    Try
                        If DateValeur = "" Then
                            Return (From ItemStage In WsListeDesFormations Where ItemStage.Identifiant = Ide Order By ItemStage.Date_Debut Descending).FirstOrDefault
                        Else
                            Return (From ItemStage In WsListeDesFormations Where ItemStage.Identifiant = Ide And ItemStage.Date_Debut = DateValeur).FirstOrDefault
                        End If
                    Catch ex As Exception
                        Return Nothing
                    End Try
                End Get
            End Property

            Public ReadOnly Property CalendrierSessions(ByVal Annee As Integer, ByVal Mois As Integer) As List(Of Virtualia.Net.WebAppli.SelfV4.DossierFormation)
                Get
                    If WsListeDesFormations Is Nothing OrElse WsListeDesFormations.Count = 0 Then
                        Return Nothing
                    End If
                    Dim LstRes As List(Of Virtualia.Net.WebAppli.SelfV4.DossierFormation)
                    LstRes = (From Element In WsListeDesFormations Where Element.SiDatesValides = True).ToList
                    If LstRes Is Nothing OrElse LstRes.Count = 0 Then
                        Return Nothing
                    End If
                    Return (From ItemSession In LstRes Where ItemSession.Date_Debut_ToDate.Year = Annee And ItemSession.Date_Debut_ToDate.Month = Mois).ToList
                End Get
            End Property
            Public ReadOnly Property ListeParPlan(ByVal Filtre As String, ByVal Lettres As String) As List(Of Virtualia.Net.WebAppli.SelfV4.DossierFormation)
                Get
                    Dim LstResultat As List(Of Virtualia.Net.WebAppli.SelfV4.DossierFormation)
                    LstResultat = ListeFiltree(Filtre, Lettres)
                    If LstResultat Is Nothing Then
                        Return Nothing
                    End If
                    Return LstResultat.OrderBy(Of String)(Function(Element)
                                                              Return Element.Plan & "~" & Element.Domaine & "~" & Element.Theme & "~" & Element.IntituleStage
                                                          End Function).ToList()
                End Get
            End Property
            Public ReadOnly Property ListeParDomaine(ByVal Filtre As String, ByVal Lettres As String) As List(Of Virtualia.Net.WebAppli.SelfV4.DossierFormation)
                Get
                    Dim LstResultat As List(Of Virtualia.Net.WebAppli.SelfV4.DossierFormation)
                    LstResultat = ListeFiltree(Filtre, Lettres)
                    If LstResultat Is Nothing Then
                        Return Nothing
                    End If
                    Return LstResultat.OrderBy(Of String)(Function(Element)
                                                              Return Element.Domaine & "~" & Element.Theme & "~" & Element.IntituleStage
                                                          End Function).ToList()
                End Get
            End Property
            Public ReadOnly Property ListeAlphabetique(ByVal Filtre As String, ByVal Lettres As String) As List(Of Virtualia.Net.WebAppli.SelfV4.DossierFormation)
                Get
                    Dim LstResultat As List(Of Virtualia.Net.WebAppli.SelfV4.DossierFormation)
                    LstResultat = ListeFiltree(Filtre, Lettres)
                    If LstResultat Is Nothing Then
                        Return Nothing
                    End If
                    Return LstResultat.OrderBy(Of String)(Function(Element)
                                                              Return Element.IntituleStage
                                                          End Function).ToList()
                End Get
            End Property

            Private ReadOnly Property ListeFiltree(ByVal Filtre As String, ByVal Lettres As String) As List(Of Virtualia.Net.WebAppli.SelfV4.DossierFormation)
                Get
                    If WsListeDesFormations Is Nothing OrElse WsListeDesFormations.Count = 0 Then
                        Return Nothing
                    End If
                    Dim LstRes As List(Of Virtualia.Net.WebAppli.SelfV4.DossierFormation)
                    Select Case Filtre
                        Case ""
                            Select Case Lettres
                                Case ""
                                    LstRes = (From Element In WsListeDesFormations Where Element.SiDatesValides = True).ToList
                                Case Else
                                    Select Case Lettres.Length
                                        Case 1
                                            LstRes = (From Element In WsListeDesFormations Where Element.SiDatesValides = True And Element.IntituleStage.ToUpper.StartsWith(Lettres)).ToList
                                        Case Else
                                            LstRes = (From Element In WsListeDesFormations Where Element.SiDatesValides = True And Element.IntituleStage.ToUpper.Contains(Lettres)).ToList
                                    End Select
                            End Select
                        Case Else
                            Select Case Lettres
                                Case ""
                                    LstRes = (From Element In WsListeDesFormations Where Element.SiDatesValides = True And Element.Plan = Filtre).ToList
                                Case Else
                                    Select Case Lettres.Length
                                        Case 1
                                            LstRes = (From Element In WsListeDesFormations Where Element.SiDatesValides = True And Element.Plan = Filtre And Element.IntituleStage.ToUpper.StartsWith(Lettres)).ToList
                                        Case Else
                                            LstRes = (From Element In WsListeDesFormations Where Element.SiDatesValides = True And Element.Plan = Filtre And Element.IntituleStage.ToUpper.Contains(Lettres)).ToList
                                    End Select
                            End Select
                    End Select
                    If LstRes Is Nothing OrElse LstRes.Count = 0 Then
                        Return Nothing
                    End If
                    Return LstRes
                End Get
            End Property

            Private Sub RequetePlanFormation()
                Dim LstSel As List(Of SqlInterne_Selection)
                Dim LstExt As List(Of SqlInterne_Extraction)
                Dim ObjetStructure As SqlInterne_Structure
                Dim InfoSel As SqlInterne_Selection
                Dim ConstructeurFOR As SqlInterne
                Dim LstResultats As List(Of Virtualia.Net.ServiceServeur.VirRequeteType)
                Dim ListeDesStages As List(Of Virtualia.Net.WebAppli.SelfV4.DossierFormation)
                Dim AffectationEtablissement As String

                Try
                    AffectationEtablissement = WsParent.DossierPER.VFiche_Etablissement(WsParent.VParent.V_RhDates.DateduJour).Administration
                Catch ex As Exception
                    AffectationEtablissement = WsParent.VParent.Etablissement
                End Try

                ConstructeurFOR = New SqlInterne(WsParent.VPointeurGlobal.VirModele, WsParent.VPointeurGlobal.VirInstanceBd)
                LstExt = New List(Of SqlInterne_Extraction)
                LstExt.Add(New SqlInterne_Extraction(1, 15, 0)) 'Etablissement
                LstExt.Add(New SqlInterne_Extraction(1, 5, 0))  'Plan
                LstExt.Add(New SqlInterne_Extraction(1, 3, 0))  'Domaine
                LstExt.Add(New SqlInterne_Extraction(1, 4, 0))  'Thème
                LstExt.Add(New SqlInterne_Extraction(1, 1, 0))  'Intitulé
                LstExt.Add(New SqlInterne_Extraction(1, 2, 0))  'Organisme
                LstExt.Add(New SqlInterne_Extraction(1, 14, 0)) 'Url
                LstExt.Add(New SqlInterne_Extraction(1, 8, 0))  'Date de clôture

                ObjetStructure = New SqlInterne_Structure(VI.PointdeVue.PVueFormation)
                ObjetStructure.Operateur_Liaison = VI.Operateurs.ET
                ObjetStructure.SiPasdeTriSurIdeDossier = True

                LstSel = New List(Of SqlInterne_Selection)
                InfoSel = New SqlInterne_Selection(1, 17)
                InfoSel.Operateur_Comparaison = VI.Operateurs.Inclu
                InfoSel.Valeurs_AComparer = (New String() {"01/09/" & (DateTime.Now.Year - 1), "31/12/" & (DateTime.Now.Year + 1)}).ToList()
                LstSel.Add(InfoSel)

                If System.Configuration.ConfigurationManager.AppSettings("SiFiltreEtablissement") = "Oui" Then
                    InfoSel = New SqlInterne_Selection(1, 15)
                    InfoSel.Operateur_Comparaison = VI.Operateurs.Inclu
                    InfoSel.Valeurs_AComparer = (New String() {AffectationEtablissement, "Commun", ""}).ToList()
                    InfoSel.SiAjouter_ValeurNulle = True
                    LstSel.Add(InfoSel)
                Else
                    InfoSel = New SqlInterne_Selection(3, 14)
                    InfoSel.Operateur_Comparaison = VI.Operateurs.Inclu
                    InfoSel.Valeurs_AComparer = (New String() {AffectationEtablissement, ""}).ToList()
                    InfoSel.SiAjouter_ValeurNulle = True
                    LstSel.Add(InfoSel)
                End If
                InfoSel = New SqlInterne_Selection(3, 10)
                InfoSel.Operateur_Comparaison = VI.Operateurs.Difference
                InfoSel.Valeurs_AComparer = (New String() {"Réservé"}).ToList()
                InfoSel.SiAjouter_ValeurNulle = True
                LstSel.Add(InfoSel)

                ConstructeurFOR.StructureRequete = ObjetStructure
                ConstructeurFOR.ListeInfosAExtraire = LstExt
                ConstructeurFOR.ListeInfosASelectionner = LstSel

                LstResultats = WsParent.VPointeurGlobal.VirServiceServeur.RequeteSql_ToListeType(WsParent.VPointeurGlobal.VirNomUtilisateur, VI.PointdeVue.PVueFormation, 1, ConstructeurFOR.OrdreSqlDynamique)
                If LstResultats Is Nothing OrElse LstResultats.Count = 0 Then
                    WsListeDesFormations = Nothing
                    Exit Sub
                End If
                ListeDesStages = New List(Of Virtualia.Net.WebAppli.SelfV4.DossierFormation)
                LstResultats.ForEach(Sub(VRequeteType)
                                         ListeDesStages.Add(New Virtualia.Net.WebAppli.SelfV4.DossierFormation() _
                                                        With {.Identifiant = VRequeteType.Ide_Dossier, .Etablissement = If(VRequeteType.Valeurs(0).Trim() = "", "Commun",
                                                              VRequeteType.Valeurs(0).Trim()), .Plan = VRequeteType.Valeurs(1).Trim(), .Domaine = VRequeteType.Valeurs(2).Trim(),
                                                              .Theme = VRequeteType.Valeurs(3).Trim(), .IntituleStage = VRequeteType.Valeurs(4).Trim(), .Organisme = VRequeteType.Valeurs(5).Trim(),
                                                              .Lien_Url = VRequeteType.Valeurs(6).Trim(), .DateCloture = VRequeteType.Valeurs(7).Trim()})
                                     End Sub)

                ConstructeurFOR = New SqlInterne(WsParent.VPointeurGlobal.VirModele, WsParent.VPointeurGlobal.VirInstanceBd)
                LstExt = New List(Of SqlInterne_Extraction)()
                LstExt.Add(New SqlInterne_Extraction(5, 0, 0))      'FOR_SESSION Date de début
                LstExt.Add(New SqlInterne_Extraction(5, 15, 0))     'FOR_SESSION Date de fin
                LstExt.Add(New SqlInterne_Extraction(5, 18, 0))     'FOR_SESSION Intitulé de la session
                LstExt.Add(New SqlInterne_Extraction(5, 11, 0))     'FOR_SESSION Heure de début matin
                LstExt.Add(New SqlInterne_Extraction(5, 12, 0))     'FOR_SESSION Heure de fin matin
                LstExt.Add(New SqlInterne_Extraction(5, 13, 0))     'FOR_SESSION Heure de début AM
                LstExt.Add(New SqlInterne_Extraction(5, 14, 0))     'FOR_SESSION Heure de fin AM
                LstExt.Add(New SqlInterne_Extraction(5, 7, 0))      'FOR_SESSION Lieu du stage
                LstExt.Add(New SqlInterne_Extraction(1, 15, 0))     'FOR_IDENTIFICATION Etablissement
                LstExt.Add(New SqlInterne_Extraction(1, 5, 0))      'FOR_IDENTIFICATION Plan
                LstExt.Add(New SqlInterne_Extraction(1, 3, 0))      'FOR_IDENTIFICATION Domaine
                LstExt.Add(New SqlInterne_Extraction(1, 4, 0))      'FOR_IDENTIFICATION Thème
                LstExt.Add(New SqlInterne_Extraction(1, 1, 0))      'FOR_IDENTIFICATION Intitulé du stage
                LstExt.Add(New SqlInterne_Extraction(1, 2, 0))      'FOR_IDENTIFICATION Organisme
                LstExt.Add(New SqlInterne_Extraction(1, 14, 0))     'FOR_IDENTIFICATION Url
                LstExt.Add(New SqlInterne_Extraction(1, 8, 0))      'FOR_IDENTIFICATION Date Cloture

                ObjetStructure = New SqlInterne_Structure(VI.PointdeVue.PVueFormation)
                ObjetStructure.Operateur_Liaison = VI.Operateurs.ET
                ConstructeurFOR.StructureRequete = ObjetStructure
                ConstructeurFOR.ListeInfosAExtraire = LstExt
                ConstructeurFOR.ListeInfosASelectionner = LstSel

                LstResultats = WsParent.VPointeurGlobal.VirServiceServeur.RequeteSql_ToListeType(WsParent.VPointeurGlobal.VirNomUtilisateur, VI.PointdeVue.PVueFormation, 1, ConstructeurFOR.OrdreSqlDynamique)
                If LstResultats Is Nothing OrElse LstResultats.Count = 0 Then
                    WsListeDesFormations = ListeDesStages
                    Exit Sub
                End If
                WsListeDesFormations = New List(Of Virtualia.Net.WebAppli.SelfV4.DossierFormation)
                LstResultats.ForEach(Sub(VRequeteType)
                                         WsListeDesFormations.Add(New Virtualia.Net.WebAppli.SelfV4.DossierFormation() With {.Identifiant = VRequeteType.Ide_Dossier, .Date_Debut = VRequeteType.Valeurs(0),
                                                                                             .Date_Fin = VRequeteType.Valeurs(1), .IntituleSession = VRequeteType.Valeurs(2),
                                                                                             .Heure_AM_Debut = VRequeteType.Valeurs(3), .Heure_AM_Fin = VRequeteType.Valeurs(4),
                                                                                             .Heure_PM_Debut = VRequeteType.Valeurs(5), .Heure_PM_Fin = VRequeteType.Valeurs(6), .Lieu = VRequeteType.Valeurs(7).Trim(),
                                                                                             .Etablissement = If(VRequeteType.Valeurs(8).Trim() = "", "Commun", VRequeteType.Valeurs(8).Trim()),
                                                                                             .Plan = VRequeteType.Valeurs(9).Trim(), .Domaine = VRequeteType.Valeurs(10).Trim(),
                                                                                             .Theme = VRequeteType.Valeurs(11).Trim(), .IntituleStage = VRequeteType.Valeurs(12).Trim(),
                                                                                             .Organisme = VRequeteType.Valeurs(13).Trim(), .Lien_Url = VRequeteType.Valeurs(14).Trim(), .DateCloture = VRequeteType.Valeurs(15).Trim()})
                                     End Sub)

                If WsListeDesFormations.Count = 0 Then
                    Exit Sub
                End If
                Dim LstIde_FOR_SESSION As List(Of Integer) = (From ItemSession In WsListeDesFormations Where ItemSession.DateCloture = "" Select ItemSession.Identifiant).Distinct().ToList()
                Dim LstIde_FOR_IDENTIFICATION As List(Of Integer) = (From ItemStage In ListeDesStages Where ItemStage.DateCloture = "" Select ItemStage.Identifiant).Distinct().ToList()
                Dim LstIde_OK As List(Of Integer) = LstIde_FOR_IDENTIFICATION.Except(LstIde_FOR_SESSION).ToList()
                WsListeDesFormations.AddRange((From ItemStage In ListeDesStages Where LstIde_OK.Contains(ItemStage.Identifiant) Select ItemStage).ToList())
            End Sub

            Public Sub New(ByVal Host As Virtualia.Net.Session.SessionModule)
                WsParent = Host
                Call RequetePlanFormation()
            End Sub
        End Class

        Public Class DossierFormation
            Private WsIdentifiant As Integer = 0
            Private WsPlan As String = ""
            Private WsIntituleStage As String = ""
            Private WsEtablissement As String = ""
            Private WsDomaine As String = ""
            Private WsTheme As String = ""
            Private WsOrganisme As String = ""
            Private WsLienUrl As String = ""
            Private WsDateCloture As String = ""
            Private WsIntituleSession As String = ""
            Private WsDateDebut As String = ""
            Private WsDateFin As String = ""
            Private WsHeure_AM_Debut As String = ""
            Private WsHeure_AM_Fin As String = ""
            Private WsHeure_PM_Debut As String = ""
            Private WsHeure_PM_Fin As String = ""
            Private WsLieu As String = ""

            Public Property Identifiant As Integer
                Get
                    Return WsIdentifiant
                End Get
                Set(value As Integer)
                    WsIdentifiant = value
                End Set
            End Property

            Public Property Plan As String
                Get
                    Return WsPlan
                End Get
                Set(value As String)
                    WsPlan = value
                End Set
            End Property

            Public Property IntituleStage As String
                Get
                    Return WsIntituleStage
                End Get
                Set(value As String)
                    WsIntituleStage = value
                End Set
            End Property

            Public Property Etablissement As String
                Get
                    Return WsEtablissement
                End Get
                Set(value As String)
                    WsEtablissement = value
                End Set
            End Property

            Public Property Domaine As String
                Get
                    Return WsDomaine
                End Get
                Set(value As String)
                    WsDomaine = value
                End Set
            End Property

            Public Property Theme As String
                Get
                    Return WsTheme
                End Get
                Set(value As String)
                    WsTheme = value
                End Set
            End Property

            Public Property Organisme As String
                Get
                    Return WsOrganisme
                End Get
                Set(value As String)
                    WsOrganisme = value
                End Set
            End Property

            Public Property Lien_Url As String
                Get
                    Return WsLienUrl
                End Get
                Set(value As String)
                    WsLienUrl = value
                End Set
            End Property

            Public Property DateCloture As String
                Get
                    Return WsDateCloture
                End Get
                Set(value As String)
                    WsDateCloture = value
                End Set
            End Property

            Public Property IntituleSession As String
                Get
                    Return WsIntituleSession
                End Get
                Set(value As String)
                    WsIntituleSession = value
                End Set
            End Property

            Public Property Date_Debut As String
                Get
                    Return WsDateDebut
                End Get
                Set(value As String)
                    WsDateDebut = value
                End Set
            End Property

            Public ReadOnly Property Date_Debut_ToDate As Date
                Get
                    If WsDateDebut = "" Then
                        Return System.DateTime.MaxValue
                    End If
                    Return CDate(WsDateDebut)
                End Get
            End Property

            Public Property Date_Fin As String
                Get
                    Return WsDateFin
                End Get
                Set(value As String)
                    WsDateFin = value
                End Set
            End Property

            Public ReadOnly Property Date_Fin_ToDate As Date
                Get
                    If WsDateFin = "" Then
                        Return System.DateTime.MaxValue
                    End If
                    Return CDate(WsDateFin)
                End Get
            End Property

            Public Property Heure_AM_Debut As String
                Get
                    Return WsHeure_AM_Debut
                End Get
                Set(value As String)
                    WsHeure_AM_Debut = value
                End Set
            End Property

            Public Property Heure_AM_Fin As String
                Get
                    Return WsHeure_AM_Fin
                End Get
                Set(value As String)
                    WsHeure_AM_Fin = value
                End Set
            End Property

            Public Property Heure_PM_Debut As String
                Get
                    Return WsHeure_PM_Debut
                End Get
                Set(value As String)
                    WsHeure_PM_Debut = value
                End Set
            End Property

            Public Property Heure_PM_Fin As String
                Get
                    Return WsHeure_PM_Fin
                End Get
                Set(value As String)
                    WsHeure_PM_Fin = value
                End Set
            End Property

            Public Property Lieu As String
                Get
                    Return WsLieu
                End Get
                Set(value As String)
                    WsLieu = value
                End Set
            End Property

            'Public ReadOnly Property SiDatesValides As Boolean
            '    Get
            '        If Date_Fin_ToDate <= CDate("01/01/2015") Then
            '            Return False
            '        End If
            '        If Date_Debut_ToDate <= CDate("01/01/2015") Then
            '            Return False
            '        End If
            '        Return True
            '    End Get
            'End Property

            Public ReadOnly Property SiDatesValides As Boolean
                Get
                    If Date_Fin_ToDate <= System.DateTime.Now Then
                        Return False
                    End If
                    If Date_Debut_ToDate <= System.DateTime.Now Then
                        Return False
                    End If
                    Return True
                End Get
            End Property

            Public ReadOnly Property EtiquetteSession As String
                Get
                    If Date_Debut = "" And Date_Fin = "" Then
                        Return ""
                    End If
                    Dim CRetour As String = "Session du "
                    CRetour &= Date_Debut & " au " & Date_Fin
                    If IntituleSession.Trim() <> "" Then
                        CRetour &= Strings.Space(1) & VI.DoubleQuote & IntituleSession.Trim() & VI.DoubleQuote
                    End If
                    Return CRetour
                End Get
            End Property

            Public ReadOnly Property ToolTipSession As String
                Get
                    If (Date_Debut & Date_Fin & IntituleSession.Trim() = "") Then
                        Return ""
                    End If
                    Dim CRetour As String = "Session "
                    If IntituleSession.Trim() = "" Then
                        CRetour &= "du "
                    Else
                        CRetour &= VI.DoubleQuote & IntituleSession.Trim() & VI.DoubleQuote & " du "
                    End If
                    CRetour &= Date_Debut & " au " & Date_Fin & " (" & Horaires.Trim() & ")"
                    Return CRetour
                End Get
            End Property

            Private ReadOnly Property Horaires As String
                Get
                    Dim Chaine As String = ""
                    If Heure_AM_Debut.Trim() <> "" Then
                        Chaine = "1"
                    Else
                        Chaine = "0"
                    End If
                    If Heure_AM_Fin.Trim() <> "" Then
                        Chaine &= "1"
                    Else
                        Chaine &= "0"
                    End If
                    If Heure_PM_Debut.Trim() <> "" Then
                        Chaine &= "1"
                    Else
                        Chaine &= "0"
                    End If
                    If Heure_PM_Fin.Trim() <> "" Then
                        Chaine &= "1"
                    Else
                        Chaine &= "0"
                    End If
                    Select Case Chaine
                        Case "1100"
                            Return "de " & Heure_AM_Debut & " à " & Heure_AM_Fin
                        Case "0011"
                            Return "de " & Heure_PM_Debut & " à " & Heure_PM_Fin
                        Case "1001"
                            Return "de " & Heure_AM_Debut & " à " & Heure_PM_Fin
                        Case "1111"
                            Return "de " & Heure_AM_Debut & " à " & Heure_AM_Fin & " et de " & Heure_PM_Debut & " à " & Heure_PM_Fin
                    End Select
                    Return "journée complète"
                End Get
            End Property
        End Class
    End Namespace
End Namespace