﻿'------------------------------------------------------------------------------
' <généré automatiquement>
'     Ce code a été généré par un outil.
'
'     Les modifications apportées à ce fichier peuvent provoquer un comportement incorrect et seront perdues si
'     le code est régénéré.
' </généré automatiquement>
'------------------------------------------------------------------------------

Option Strict On
Option Explicit On


Partial Public Class VNotificationPS
    
    '''<summary>
    '''Contrôle PS_PAGE_NOTIF.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents PS_PAGE_NOTIF As Global.System.Web.UI.WebControls.Table
    
    '''<summary>
    '''Contrôle PS_CadreInfo.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents PS_CadreInfo As Global.System.Web.UI.WebControls.Table
    
    '''<summary>
    '''Contrôle PS_CadreTitreNotification.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents PS_CadreTitreNotification As Global.System.Web.UI.WebControls.Table
    
    '''<summary>
    '''Contrôle PS_Etablissement.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents PS_Etablissement As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle PS_LieuDate.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents PS_LieuDate As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle PS_TitreNotif.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents PS_TitreNotif As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle PS_EtiAnneeEntretien.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents PS_EtiAnneeEntretien As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle PS_AnneeEntretien.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents PS_AnneeEntretien As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle PS_CadreEnteteNotif.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents PS_CadreEnteteNotif As Global.System.Web.UI.WebControls.Table
    
    '''<summary>
    '''Contrôle PS_EtiTitreAgent.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents PS_EtiTitreAgent As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle PS_EtiMatricule.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents PS_EtiMatricule As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle PS_Matricule.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents PS_Matricule As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle PS_EtiNom.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents PS_EtiNom As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle PS_Nom.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents PS_Nom As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle PS_EtiPrenom.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents PS_EtiPrenom As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle PS_Prenom.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents PS_Prenom As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle PS_EtiAffectationNiv1.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents PS_EtiAffectationNiv1 As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle PS_AffectationNiv1.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents PS_AffectationNiv1 As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle PS_EtiAffectationNiv2.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents PS_EtiAffectationNiv2 As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle PS_AffectationNiv2.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents PS_AffectationNiv2 As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle PS_EtiGrade.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents PS_EtiGrade As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle PS_Grade.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents PS_Grade As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle PS_EtiEchelon.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents PS_EtiEchelon As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle PS_Echelon.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents PS_Echelon As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle PS_CadreTableau.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents PS_CadreTableau As Global.System.Web.UI.WebControls.Table
    
    '''<summary>
    '''Contrôle PS_L1_C1.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents PS_L1_C1 As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle PS_L1_C2.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents PS_L1_C2 As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle PS_L1_C3.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents PS_L1_C3 As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle PS_L1_C4.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents PS_L1_C4 As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle PS_L1_C5.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents PS_L1_C5 As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle PS_L1_C6.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents PS_L1_C6 As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle PS_L1_C7.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents PS_L1_C7 As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle PS_L2_C1.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents PS_L2_C1 As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle PS_Col_18.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents PS_Col_18 As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle PS_Col_17.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents PS_Col_17 As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle PS_Col_19.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents PS_Col_19 As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle PS_Col_20.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents PS_Col_20 As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle PS_Col_21.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents PS_Col_21 As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle PS_Col_22.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents PS_Col_22 As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle PS_L3_C1.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents PS_L3_C1 As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle PS_L3_C2.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents PS_L3_C2 As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle PS_L3_C3.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents PS_L3_C3 As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle PS_L3_C4.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents PS_L3_C4 As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle PS_L3_C5.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents PS_L3_C5 As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle PS_Col_22Bis.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents PS_Col_22Bis As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle PS_Col_22BisBis.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents PS_Col_22BisBis As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle PS_L4_C1.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents PS_L4_C1 As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle PS_L4_C2.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents PS_L4_C2 As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle PS_L4_C3.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents PS_L4_C3 As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle PS_L4_C4.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents PS_L4_C4 As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle PS_L4_C5.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents PS_L4_C5 As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle PS_L4_C6.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents PS_L4_C6 As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle PS_Col_21Bis.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents PS_Col_21Bis As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle PS_L5_C1.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents PS_L5_C1 As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle PS_L5_C2.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents PS_L5_C2 As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle PS_L5C3.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents PS_L5C3 As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle PS_L5_C4.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents PS_L5_C4 As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle PS_L5_C5.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents PS_L5_C5 As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle PS_L5_C6.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents PS_L5_C6 As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle PS_Col_22Ter.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents PS_Col_22Ter As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle PS_CadreBasPageNotification.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents PS_CadreBasPageNotification As Global.System.Web.UI.WebControls.Table
    
    '''<summary>
    '''Contrôle PS_BasPage.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents PS_BasPage As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle PS_CadreSignatureFAM.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents PS_CadreSignatureFAM As Global.System.Web.UI.WebControls.Table
    
    '''<summary>
    '''Contrôle PS_SignatureFAM.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents PS_SignatureFAM As Global.System.Web.UI.WebControls.Table
End Class
