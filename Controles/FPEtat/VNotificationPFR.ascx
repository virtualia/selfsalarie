﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="VNotificationPFR.ascx.vb" Inherits="Virtualia.Net.VNotificationPFR" %>

<style type="text/css">
.EtiTitre
    {
        background-color:transparent;
        border-style:none;
        color:gray;
        font-family:'Trebuchet MS';
        font-size:medium;
        font-style:normal;
        font-weight:bold;
        margin-bottom:0px;
        margin-left:0px;
        margin-top:2px;
        text-align:left;
        text-indent:0px;
    }
     .EtiSousTitre
    {
        background-color:transparent;
        border-style:none;
        color:black;
        font-family:'Trebuchet MS';
        font-size:small;
        font-style:normal;
        font-weight:normal;
        margin-bottom:0px;
        margin-left:0px;
        margin-top:2px;
        text-align:center;
        text-indent:0px;
    }
    .EtiEntete
    {
        background-color:transparent;
        color:black;
        font-family:'Trebuchet MS';
        font-size:small;
        font-style:italic;
        font-weight:normal;
        height:20px;
        margin-bottom:0px;
        margin-left:0px;
        margin-top:0px;
        text-indent:4px;
        text-align:left;
        width:250px;
    }
    .DonEntete
    {
        background-color:transparent;
        color:black;
        font-family:'Trebuchet MS';
        font-size:small;
        font-style:normal;
        font-weight:normal;
        height:20px;
        margin-bottom:0px;
        margin-left:0px;
        margin-top:0px;
        text-indent:1px;
        text-align:left;
        width:496px;
    }
    .EtiIntituleRight
    {
        background-color:transparent;
        border-right-style:solid;
        border-right-width:1px;
        color:black;
        font-family:'Trebuchet MS';
        font-size:small;
        font-style:normal;
        font-weight:normal;
        margin-bottom:0px;
        margin-left:0px;
        margin-top:0px;
        text-indent:0px;
        text-align:center;
    }
     .EtiIntituleTop
    {
        background-color:transparent;
        border-top-style:none;
        color:black;
        font-family:'Trebuchet MS';
        font-size:small;
        font-style:normal;
        font-weight:normal;
        margin-bottom:0px;
        margin-left:0px;
        margin-top:0px;
        text-indent:0px;
        text-align:center;
    }
</style>

<asp:Table ID="PFR_PAGE_NOTIF" runat="server" Width="750px">
    <asp:TableRow>
        <asp:TableCell>
            <asp:Table ID="PFR_CadreInfo" runat="server" BorderStyle="None" BorderWidth="2px" Visible="true"
                BorderColor="Transparent" Width="750px" HorizontalAlign="Left" style="margin-top: 1px;">
                <asp:TableRow>
                  <asp:TableCell>
                    <asp:Table ID="PFR_CadreTitreNotification" runat="server" Height="50px" CellPadding="0" Width="750px" 
                        CellSpacing="0" HorizontalAlign="Center" BorderStyle="none" BorderWidth="1px" Visible="true">
                        <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Center">
                                 <asp:Label ID="PFR_Etablissement" runat="server" Text="FranceAgriMer" 
                                    Height="22px" Width="500px" CssClass="EtiTitre" style="text-indent:2px;">
                                 </asp:Label>          
                            </asp:TableCell>
                            <asp:TableCell HorizontalAlign="Center">
                                 <asp:Label ID="PFR_LieuDate" runat="server" Text="Montreuil, le 16 décembre 2016" 
                                    Height="22px" Width="246px" CssClass="EtiTitre" style="margin-top:0px;text-align:right;">
                                 </asp:Label>          
                            </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow>
                            <asp:TableCell Height="12px" ColumnSpan="2" Width="746px"></asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Center" ColumnSpan="2">
                                <asp:Label ID="PFR_TitreNotif" runat="server" Text="NOTIFICATION INDIVIDUELLE DES MONTANTS ALLOUES AU TITRE DE LA PFR"
                                    Height="25px" Width="746px" CssClass="EtiTitre" Font-Underline="true"
                                    style="margin-top:1px;text-align:center;">
                                </asp:Label>          
                            </asp:TableCell>      
                        </asp:TableRow>
                        <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Center">
                                 <asp:Label ID="PFR_EtiAnneeEntretien" runat="server" Text="Fonctionnaires - Année de référence :" 
                                    Height="22px" Width="500px" CssClass="EtiTitre" style="text-align:right;">
                                 </asp:Label>          
                            </asp:TableCell>
                            <asp:TableCell HorizontalAlign="Center">
                                 <asp:Label ID="PFR_AnneeEntretien" runat="server" Text="2016" 
                                    Height="22px" Width="246px" CssClass="EtiTitre" style="text-indent:3px;">
                                 </asp:Label>          
                            </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow>
                            <asp:TableCell Height="32px" ColumnSpan="2" Width="746px"></asp:TableCell>
                        </asp:TableRow>
                    </asp:Table>
                  </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                  <asp:TableCell>
                    <asp:Table ID="PFR_CadreEnteteNotif" runat="server" CellPadding="0" CellSpacing="0" Width="750px"
                        BorderStyle="Solid" BorderWidth="1px" BorderColor="Black" Visible="true" HorizontalAlign="Center">
                        <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Center" ColumnSpan="2">
                                 <asp:Label ID="PFR_EtiTitreAgent" runat="server" Height="30px" Width="350px"
                                    Text="Renseignements relatifs à l'agent" CssClass="EtiSousTitre"
                                    Font-Size="Medium" style="margin-top:5px;text-align:center;">
                                 </asp:Label>          
                            </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow>
                            <asp:TableCell Height="1px" ColumnSpan="2" Width="750px"
                                BorderStyle="Solid" BorderWidth="1px" BorderColor="Black" 
                                style="border-bottom-style:none;">
                            </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow>
                            <asp:TableCell Height="5px" ColumnSpan="2" Width="750px"></asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Left">
                                 <asp:Label ID="PFR_EtiMatricule" runat="server" Text="Matricule :" CssClass="EtiEntete">
                                 </asp:Label>          
                            </asp:TableCell>
                            <asp:TableCell HorizontalAlign="Left">
                                 <asp:Label ID="PFR_Matricule" runat="server" CssClass="DonEntete">
                                 </asp:Label>          
                            </asp:TableCell> 
                        </asp:TableRow>
                        <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Left">
                                 <asp:Label ID="PFR_EtiNom" runat="server" Text="Nom :" CssClass="EtiEntete">
                                 </asp:Label>          
                            </asp:TableCell>
                            <asp:TableCell HorizontalAlign="Left">
                                 <asp:Label ID="PFR_Nom" runat="server" CssClass="DonEntete">
                                 </asp:Label>          
                            </asp:TableCell> 
                        </asp:TableRow>
                        <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Left">
                                 <asp:Label ID="PFR_EtiPrenom" runat="server" Text="Prénom :" CssClass="EtiEntete">
                                 </asp:Label>          
                            </asp:TableCell>
                            <asp:TableCell HorizontalAlign="Left">
                                 <asp:Label ID="PFR_Prenom" runat="server" CssClass="DonEntete">
                                 </asp:Label>          
                            </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Left">
                                 <asp:Label ID="PFR_EtiAffectationNiv1" runat="server" Text="Affectation :" CssClass="EtiEntete">
                                 </asp:Label>          
                            </asp:TableCell>
                            <asp:TableCell HorizontalAlign="Left">
                                 <asp:Label ID="PFR_AffectationNiv1" runat="server" CssClass="DonEntete">
                                 </asp:Label>          
                            </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Left">
                                 <asp:Label ID="PFR_EtiAffectationNiv2" runat="server" CssClass="EtiEntete">
                                 </asp:Label>          
                            </asp:TableCell>
                            <asp:TableCell HorizontalAlign="Left">
                                 <asp:Label ID="PFR_AffectationNiv2" runat="server" CssClass="DonEntete">
                                 </asp:Label>          
                            </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Left">
                                 <asp:Label ID="PFR_EtiGrade" runat="server" Text="Grade ou emploi :" CssClass="EtiEntete">
                                 </asp:Label>          
                            </asp:TableCell>
                            <asp:TableCell HorizontalAlign="Left">
                                 <asp:Label ID="PFR_Grade" runat="server" CssClass="DonEntete">
                                 </asp:Label>          
                            </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Left">
                                 <asp:Label ID="PFR_EtiCoefficient" runat="server" Text="Coefficient de la part liée aux fonctions :"
                                    CssClass="EtiEntete">
                                 </asp:Label>          
                            </asp:TableCell>
                            <asp:TableCell HorizontalAlign="Left">
                                 <asp:Label ID="PFR_Coefficient" runat="server" CssClass="DonEntete">
                                 </asp:Label>          
                            </asp:TableCell>
                        </asp:TableRow> 
                    </asp:Table>
                  </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                  <asp:TableCell Height="32px" Width="750px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                  <asp:TableCell>
                    <asp:Table ID="PFR_CadreTableau" runat="server" Height="425px" CellPadding="0" Width="750px" 
                        CellSpacing="0" HorizontalAlign="Center" BorderStyle="Solid" BorderWidth="2px" BorderColor="Black" Visible="true">
                        <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Center">
                                 <asp:Label ID="PFR_L1_C1" runat="server" Text="" 
                                    Height="110px" Width="97px" CssClass="EtiIntituleRight">
                                 </asp:Label>          
                            </asp:TableCell>
                            <asp:TableCell HorizontalAlign="Center">
                                 <asp:Label ID="PFR_L1_C2" runat="server" Text="Coefficient<br/>multiplicateur" 
                                    Height="110px" Width="92px" CssClass="EtiIntituleRight">
                                 </asp:Label>          
                            </asp:TableCell>
                            <asp:TableCell HorizontalAlign="Center">
                                 <asp:Label ID="PFR_L1_C3" runat="server" Text="Montant de la part<br/>de référence en €<br/>(selon le grade)" 
                                    Height="110px" Width="125px" CssClass="EtiIntituleRight">
                                 </asp:Label>          
                            </asp:TableCell>
                            <asp:TableCell HorizontalAlign="Center">
                                 <asp:Label ID="PFR_L1_C4" runat="server" Text="% moyen de<br/>proratisation (en<br/>fonction du temps<br/>de travail et<br/>abattements<br/>maladie)" 
                                    Height="110px" Width="119px" CssClass="EtiIntituleRight">
                                 </asp:Label>          
                            </asp:TableCell>
                            <asp:TableCell HorizontalAlign="Center">
                                 <asp:Label ID="PFR_L1_C5" runat="server" Text="Montant<br/>proratisé<br/>annuel<br/>(base 100)" 
                                    Height="110px" Width="100px" CssClass="EtiIntituleRight">
                                 </asp:Label>          
                            </asp:TableCell>
                            <asp:TableCell HorizontalAlign="Center">
                                 <asp:Label ID="PFR_L1_C6" runat="server" Text="Modulation<br/>(coefficient et<br/>montant)" 
                                    Height="110px" Width="103px" CssClass="EtiIntituleRight">
                                 </asp:Label>          
                            </asp:TableCell>
                            <asp:TableCell HorizontalAlign="Center">
                                 <asp:Label ID="PFR_L1_C7" runat="server" Text="Montant total de<br/>prime  modulée<br/>allouée" 
                                    Height="110px" Width="105px" CssClass="EtiSousTitre"
                                    style="margin-top:0px;">
                                 </asp:Label>          
                            </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow>
                            <asp:TableCell Height="1px" ColumnSpan="7" Width="750px"
                                BorderStyle="Solid" BorderWidth="1px" BorderColor="Black" 
                                style="border-bottom-style:none;">
                            </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Center">
                                 <asp:Label ID="PFR_L2_C1" runat="server" Text="Part liée aux<br/>fonctions (1)" 
                                    Height="50px" Width="97px" CssClass="EtiIntituleRight">
                                 </asp:Label>          
                            </asp:TableCell>
                            <asp:TableCell HorizontalAlign="Center">
                                 <asp:Label ID="PFR_Col_15" runat="server" Height="35px" Width="92px" CssClass="EtiIntituleRight"
                                    style="padding-top: 15px;">
                                 </asp:Label>          
                            </asp:TableCell>
                            <asp:TableCell HorizontalAlign="Center">
                                 <asp:Label ID="PFR_Col_16" runat="server" Text="" 
                                    Height="35px" Width="125px" CssClass="EtiIntituleRight"
                                    style="padding-top: 15px;">
                                 </asp:Label>          
                            </asp:TableCell>
                            <asp:TableCell HorizontalAlign="Center">
                                 <asp:Label ID="PFR_Col_31" runat="server" Text="" 
                                    Height="35px" Width="119px" CssClass="EtiIntituleRight"
                                    style="padding-top: 15px;">
                                 </asp:Label>          
                            </asp:TableCell>
                            <asp:TableCell HorizontalAlign="Center">
                                 <asp:Label ID="PFR_Col_32" runat="server" Text="" 
                                    Height="35px" Width="100px" CssClass="EtiIntituleRight"
                                    style="padding-top: 15px;">
                                 </asp:Label>          
                            </asp:TableCell>
                            <asp:TableCell HorizontalAlign="Center">
                                 <asp:Label ID="PFR_Col_23" runat="server" Text="" 
                                    Height="35px" Width="103px" CssClass="EtiIntituleRight"
                                    style="padding-top: 15px;">
                                 </asp:Label>          
                            </asp:TableCell>
                            <asp:TableCell HorizontalAlign="Center">
                                 <asp:Label ID="PFR_Col_34" runat="server" Text="" 
                                    Height="35px" Width="105px" CssClass="EtiSousTitre"
                                    style="margin-top:0px;padding-top:15px;">
                                 </asp:Label>          
                            </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow>
                            <asp:TableCell Height="1px" ColumnSpan="3" Width="315px"
                                BorderStyle="Solid" BorderWidth="1px" BorderColor="Black" 
                                style="border-bottom-style:none;">
                            </asp:TableCell>
                            <asp:TableCell Height="1px" ColumnSpan="4" Width="435px"
                                BorderStyle="Solid" BorderWidth="1px" BorderColor="Transparent" 
                                style="border-bottom-style:none;">
                            </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Center">
                                 <asp:Label ID="PFR_L3_C1" runat="server" Text="Part liée aux<br/>résultats" 
                                    Height="50px" Width="97px" CssClass="EtiIntituleRight">
                                 </asp:Label>          
                            </asp:TableCell>
                            <asp:TableCell HorizontalAlign="Center">
                                 <asp:Label ID="PFR_Col_18" runat="server" Text="" 
                                    Height="35px" Width="92px" CssClass="EtiIntituleRight"
                                    style="padding-top: 15px;">
                                 </asp:Label>          
                            </asp:TableCell>
                            <asp:TableCell HorizontalAlign="Center">
                                 <asp:Label ID="PFR_Col_19" runat="server" Text="" 
                                    Height="35px" Width="125px" CssClass="EtiIntituleRight"
                                    style="padding-top: 15px;">
                                 </asp:Label>          
                            </asp:TableCell>
                            <asp:TableCell HorizontalAlign="Center">
                                 <asp:Label ID="PFR_L3_C4" runat="server" Text="" 
                                    Height="50px" Width="119px" CssClass="EtiIntituleRight">
                                 </asp:Label>          
                            </asp:TableCell>
                            <asp:TableCell HorizontalAlign="Center">
                                 <asp:Label ID="PFR_L3_C5" runat="server" Text="" 
                                    Height="50px" Width="100px" CssClass="EtiIntituleRight">
                                 </asp:Label>          
                            </asp:TableCell>
                            <asp:TableCell HorizontalAlign="Center">
                                 <asp:Label ID="PFR_Col_33" runat="server" Text="" 
                                    Height="35px" Width="103px" CssClass="EtiIntituleRight"
                                    style="padding-top: 15px;">
                                 </asp:Label>          
                            </asp:TableCell>
                            <asp:TableCell HorizontalAlign="Center">
                                 <asp:Label ID="PFR_L3_C7" runat="server" Text="" 
                                    Height="50px" Width="105px" CssClass="EtiSousTitre"
                                    style="margin-top:0px;">
                                 </asp:Label>          
                            </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow>
                            <asp:TableCell Height="1px" ColumnSpan="7" Width="750px"
                                BorderStyle="Solid" BorderWidth="1px" BorderColor="Black" 
                                style="border-bottom-style:none;">
                            </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow >
                            <asp:TableCell HorizontalAlign="Center">
                                 <asp:Label ID="PFR_L4_C1" runat="server" Text="Complément<br/>indemnitaire<br/>transitoire" 
                                    Height="60px" Width="97px" CssClass="EtiIntituleRight" style="background-color:white">
                                 </asp:Label>          
                            </asp:TableCell>
                            <asp:TableCell HorizontalAlign="Center" BackColor="#E9E9E9">
                                 <asp:Label ID="PFR_L4_C2" runat="server" Text="" CssClass="EtiIntituleTop"
                                    Height="60px" Width="92px">
                                 </asp:Label>          
                            </asp:TableCell>
                            <asp:TableCell HorizontalAlign="Center" BackColor="#E9E9E9">
                                 <asp:Label ID="PFR_L4_C3" runat="server" Text="" 
                                    Height="60px" Width="125px" CssClass="EtiIntituleTop">
                                 </asp:Label>          
                            </asp:TableCell>
                            <asp:TableCell HorizontalAlign="Center" BackColor="#E9E9E9">
                                 <asp:Label ID="PFR_L4_C4" runat="server" Text="" 
                                    Height="60px" Width="119px" CssClass="EtiIntituleTop">
                                 </asp:Label>          
                            </asp:TableCell>
                            <asp:TableCell HorizontalAlign="Center" BackColor="#E9E9E9">
                                 <asp:Label ID="PFR_L4_C5" runat="server" Text="" 
                                    Height="60px" Width="100px" CssClass="EtiIntituleRight">
                                 </asp:Label>          
                            </asp:TableCell>
                            <asp:TableCell HorizontalAlign="Center">
                                 <asp:Label ID="PFR_Col_24Ter" runat="server" Text="" 
                                    Height="40px" Width="103px" CssClass="EtiIntituleRight"
                                    style="background-color:white;padding-top: 20px;">
                                 </asp:Label>          
                            </asp:TableCell>
                            <asp:TableCell HorizontalAlign="Center">
                                 <asp:Label ID="PFR_Col_33Bis" runat="server" Text="" 
                                    Height="40px" Width="105px" CssClass="EtiIntituleTop" 
                                    style="background-color:white;padding-top: 20px;">
                                 </asp:Label>          
                            </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow>
                            <asp:TableCell Height="1px" ColumnSpan="7" Width="750px"
                                BorderStyle="Solid" BorderWidth="1px" BorderColor="Black" 
                                style="border-bottom-style:none;">
                            </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Center">
                                 <asp:Label ID="PFR_L5_C1" runat="server" Text="Indemnisation<br/>des sujétions<br/>période du<br/>01/11/2015 au<br/>31/10/2016" 
                                    Height="90px" Width="97px" CssClass="EtiIntituleRight"
                                    style="background-color:white">
                                 </asp:Label>          
                            </asp:TableCell>
                            <asp:TableCell HorizontalAlign="Center">
                                 <asp:Label ID="PFR_Col_17Ter" runat="server" Text="" 
                                    Height="50px" Width="92px" CssClass="EtiIntituleRight"
                                    style="background-color:white;padding-top: 40px;">
                                 </asp:Label>          
                            </asp:TableCell>
                            <asp:TableCell HorizontalAlign="Center" BackColor="#E9E9E9">
                                 <asp:Label ID="PFR_L5_C3" runat="server" Text="" 
                                    Height="90px" Width="125px" CssClass="EtiIntituleTop">
                                 </asp:Label>          
                            </asp:TableCell>
                            <asp:TableCell HorizontalAlign="Center" BackColor="#E9E9E9">
                                 <asp:Label ID="PFR_L5_C4" runat="server" Text="" 
                                    Height="90px" Width="119px" CssClass="EtiIntituleTop" >
                                 </asp:Label>          
                            </asp:TableCell>
                            <asp:TableCell HorizontalAlign="Center" BackColor="#E9E9E9">
                                 <asp:Label ID="PFR_L5_C5" runat="server" Text="" 
                                    Height="90px" Width="100px" CssClass="EtiIntituleTop" >
                                 </asp:Label>          
                            </asp:TableCell>
                            <asp:TableCell HorizontalAlign="Center" BackColor="#E9E9E9">
                                 <asp:Label ID="PFR_L5_C6" runat="server" Text="" 
                                    Height="90px" Width="103px" CssClass="EtiIntituleRight">
                                 </asp:Label>          
                            </asp:TableCell>
                            <asp:TableCell HorizontalAlign="Center">
                                 <asp:Label ID="PFR_Col_17Bis" runat="server" Text="" 
                                    Height="50px" Width="105px" CssClass="EtiIntituleTop"
                                    style="background-color:white;padding-top: 40px;">
                                 </asp:Label>          
                            </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow>
                            <asp:TableCell Height="1px" ColumnSpan="7" Width="750px"
                                BorderStyle="Solid" BorderWidth="1px" BorderColor="Black" 
                                style="border-bottom-style:none;">
                            </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Center">
                                 <asp:Label ID="PFR_L6_C1" runat="server" Text="Modulation complémentaire exceptionnelle" 
                                    Height="90px" Width="97px" CssClass="EtiIntituleRight"
                                    style="background-color:white">
                                 </asp:Label>          
                            </asp:TableCell>
                            <asp:TableCell HorizontalAlign="Center">
                                 <asp:Label ID="PFR_Col_24a" runat="server" Text="" 
                                    Height="50px" Width="92px" CssClass="EtiIntituleRight"
                                    style="background-color:white;padding-top: 40px;">
                                 </asp:Label>          
                            </asp:TableCell>
                            <asp:TableCell HorizontalAlign="Center" BackColor="#E9E9E9">
                                 <asp:Label ID="PFR_L6_C3" runat="server" Text="" 
                                    Height="90px" Width="125px" CssClass="EtiIntituleTop">
                                 </asp:Label>          
                            </asp:TableCell>
                            <asp:TableCell HorizontalAlign="Center" BackColor="#E9E9E9">
                                 <asp:Label ID="PFR_L6_C4" runat="server" Text="" 
                                    Height="90px" Width="119px" CssClass="EtiIntituleTop" >
                                 </asp:Label>          
                            </asp:TableCell>
                            <asp:TableCell HorizontalAlign="Center" BackColor="#E9E9E9">
                                 <asp:Label ID="PFR_L6_C5" runat="server" Text="" 
                                    Height="90px" Width="100px" CssClass="EtiIntituleTop" >
                                 </asp:Label>          
                            </asp:TableCell>
                            <asp:TableCell HorizontalAlign="Center" BackColor="#E9E9E9">
                                 <asp:Label ID="PFR_L6_C6" runat="server" Text="" 
                                    Height="90px" Width="103px" CssClass="EtiIntituleRight">
                                 </asp:Label>          
                            </asp:TableCell>
                            <asp:TableCell HorizontalAlign="Center">
                                 <asp:Label ID="PFR_Col_33a" runat="server" Text="" 
                                    Height="50px" Width="105px" CssClass="EtiIntituleTop"
                                    style="background-color:white;padding-top: 40px;">
                                 </asp:Label>          
                            </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow>
                            <asp:TableCell Height="1px" ColumnSpan="7" Width="750px"
                                BorderStyle="Solid" BorderWidth="1px" BorderColor="Black" 
                                style="border-bottom-style:none;">
                            </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Center">
                                 <asp:Label ID="PFR_L7_C1" runat="server" Text="TOTAL PFR" 
                                    Height="35px" Width="97px" CssClass="EtiIntituleRight"
                                    style="background-color:white;Padding-top: 15px;">
                                 </asp:Label>          
                            </asp:TableCell>
                            <asp:TableCell HorizontalAlign="Center" BackColor="#E9E9E9">
                                 <asp:Label ID="PFR_L7_C2" runat="server" Text="" 
                                    Height="50px" Width="92px" CssClass="EtiIntituleTop">
                                 </asp:Label>          
                            </asp:TableCell>
                            <asp:TableCell HorizontalAlign="Center" BackColor="#E9E9E9">
                                 <asp:Label ID="PFR_L7_C3" runat="server" Text="" 
                                    Height="50px" Width="125px" CssClass="EtiIntituleTop">
                                 </asp:Label>          
                            </asp:TableCell>
                            <asp:TableCell HorizontalAlign="Center" BackColor="#E9E9E9">
                                 <asp:Label ID="PFR_L7_C4" runat="server" Text="" 
                                    Height="50px" Width="119px" CssClass="EtiIntituleTop">
                                 </asp:Label>          
                            </asp:TableCell>
                            <asp:TableCell HorizontalAlign="Center" BackColor="#E9E9E9">
                                 <asp:Label ID="PFR_L7_C5" runat="server" Text="" 
                                    Height="50px" Width="100px" CssClass="EtiIntituleTop">
                                 </asp:Label>          
                            </asp:TableCell>
                            <asp:TableCell HorizontalAlign="Center" BackColor="#E9E9E9">
                                 <asp:Label ID="PFR_L7_C6" runat="server" Text="" 
                                    Height="50px" Width="103px" CssClass="EtiIntituleRight">
                                 </asp:Label>          
                            </asp:TableCell>
                            <asp:TableCell HorizontalAlign="Center">
                                 <asp:Label ID="PFR_Col_34Bis" runat="server" Text="" 
                                    Height="35px" Width="105px" CssClass="EtiIntituleTop" 
                                     style="background-color:white;padding-top: 15px;">
                                 </asp:Label>          
                            </asp:TableCell>
                        </asp:TableRow>   
                    </asp:Table>
                  </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                  <asp:TableCell>
                    <asp:Table ID="PFR_CadreBasPageNotification" runat="server" Height="50px" CellPadding="0" Width="750px" 
                        CellSpacing="0" HorizontalAlign="Center" BorderStyle="none" BorderWidth="1px" Visible="true">
                        <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Center">
                                 <asp:Label ID="PFR_BasPage1" runat="server" 
				                    Text="(1) Pour les attachés, afin de compenser l’application des montants de référence de services 
                                    déconcentrés aux agents du siège, une surcote temporaire de 0.5 a été appliquée aux cotations des
                                    attachés du siège. Cette surcote disparaîtra en cas de modification réglementaire permettant l’application
                                    des montants de référence administration centrale aux attachés exerçant leurs fonctions au siège." 
                                    Height="80px" Width="746px" CssClass="EtiSousTitre" style="text-align:left;">
                                 </asp:Label>          
                            </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow>
                            <asp:TableCell Height="20px" Width="746px"></asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Center">
                                <asp:Label ID="PFR_BasPage2" runat="server" 
				                    Text="Tout agent souhaitant contester le montant qui lui a été alloué au titre de la PFR doit au préalable
                                    formuler un recours hiérarchique auprès du Directeur général de l’Etablissement. Si le désaccord persiste,
                                    il peut déposer un recours auprès du président de la CAP de son corps d'origine dans un délai de 2 mois à 
                                    compter de la notification. Il adresse copie de ce recours au Service des ressources humaines de FranceAgriMer.
                                    Le recours formulé au-delà de ce délai ne sera pas traité."
                                    Height="80px" Width="746px" CssClass="EtiSousTitre" style="text-align:left;">
                                </asp:Label>          
                            </asp:TableCell>      
                        </asp:TableRow>
                        <asp:TableRow>
                            <asp:TableCell Height="12px" Width="746px"></asp:TableCell>
                        </asp:TableRow>
                    </asp:Table>
                  </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                  <asp:TableCell>
                    <asp:Table ID="PFR_CadreSignatureFAM" runat="server" Height="50px" CellPadding="0" Width="750px" 
                        CellSpacing="0" HorizontalAlign="Center" BorderWidth="1px" BorderStyle="None" Visible="true">
                        <asp:TableRow>
                            <asp:TableCell Height="5px"></asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Right">
                                <asp:Table ID="PFR_SignatureFAM" runat="server" BackImageUrl="~/Images/Specifique/SignatureFAM.jpg" Width="155px"
                                    BorderStyle="None" CellPadding="0" CellSpacing="0" Visible="true" HorizontalAlign="right">
                                    <asp:TableRow>
                                        <asp:TableCell Height="93px" BackColor="Transparent" Width="152px" HorizontalAlign="right"></asp:TableCell>
                                    </asp:TableRow>
                                </asp:Table>    
                            </asp:TableCell>
                        </asp:TableRow> 
                        <asp:TableRow>
                            <asp:TableCell>
                                <div style="page-break-before:always;"></div>
                            </asp:TableCell>
                        </asp:TableRow> 
                    </asp:Table>
                  </asp:TableCell>
                </asp:TableRow>
              </asp:Table>
        </asp:TableCell>
    </asp:TableRow>
</asp:Table>