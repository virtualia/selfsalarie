﻿Option Strict On
Option Explicit On
Option Compare Text
Imports Virtualia.Net.Evenements
Imports VI = Virtualia.Systeme.Constantes
Public Class Fenetre_ONIC_MESURE
    Inherits System.Web.UI.UserControl
    Public Delegate Sub MsgDialog_MsgEventHandler(ByVal sender As Object, ByVal e As Virtualia.Systeme.Evenements.MessageSaisieEventArgs)
    Public Event MessageDialogue As MsgDialog_MsgEventHandler
    Public Delegate Sub Retour_MsgEventHandler(ByVal sender As Object, ByVal e As Virtualia.Systeme.Evenements.MessageRetourEventArgs)
    Public Event ValeurRetour As Retour_MsgEventHandler
    Private WsNomStateCache As String = "VRefActivite"
    Private WsCtl_Cache As List(Of String)
    Private WebFct As Virtualia.Net.Controles.WebFonctions
    Private WsPointdeVue As Integer = VI.PointdeVue.PVueActivite
    Private WsNumObjet As Integer = 2
    Private WsNomTable As String = "ONIC_MESURE"

    Protected Overridable Sub V_Retour(ByVal e As Virtualia.Systeme.Evenements.MessageRetourEventArgs)
        RaiseEvent ValeurRetour(Me, e)
    End Sub

    Protected Overridable Sub V_MessageDialog(ByVal e As Virtualia.Systeme.Evenements.MessageSaisieEventArgs)
        RaiseEvent MessageDialogue(Me, e)
    End Sub

    Private Property CacheVirControle As List(Of String)
        Get
            If Me.ViewState(WsNomStateCache) IsNot Nothing Then
                Return CType(Me.ViewState(WsNomStateCache), List(Of String))
            End If
            Dim NewCache As List(Of String)
            NewCache = New List(Of String)
            For IndiceI As Integer = 0 To 5
                NewCache.Add("")
            Next
            Return NewCache
        End Get
        Set(value As List(Of String))
            If Me.ViewState(WsNomStateCache) IsNot Nothing Then
                Me.ViewState.Remove(WsNomStateCache)
            End If
            If value Is Nothing Then
                Exit Property
            End If
            Me.ViewState.Add(WsNomStateCache, value)
        End Set
    End Property

    Private ReadOnly Property V_WebFonction As Virtualia.Net.Controles.WebFonctions
        Get
            If WebFct Is Nothing Then
                WebFct = New Virtualia.Net.Controles.WebFonctions(Me, 0)
            End If
            Return WebFct
        End Get
    End Property

    Public ReadOnly Property MonCadre() As System.Web.UI.Control
        Get
            Return Me.CadreInfo
        End Get
    End Property

    Public WriteOnly Property CadreStyle() As String
        Set(ByVal value As String)
            Dim TableauData(0) As String
            Dim TableauW(0) As String
            Dim IndiceI As Integer
            TableauData = Strings.Split(value, ";")
            For IndiceI = 0 To TableauData.Count - 1
                If TableauData(IndiceI) = "" Then
                    Exit For
                End If
                TableauW = Strings.Split(TableauData(IndiceI), ":")
                CadreInfo.Style.Remove(Strings.Trim(TableauW(0)))
                CadreInfo.Style.Add(Strings.Trim(TableauW(0)), Strings.Trim(TableauW(1)))
            Next IndiceI
        End Set
    End Property

    Public WriteOnly Property Identifiant() As Integer
        Set(ByVal value As Integer)
            WsCtl_Cache = CacheVirControle
            If WsCtl_Cache(0) <> CStr(value) Then
                Call InitialiserControles()
                WsCtl_Cache(0) = CStr(value)
                CacheVirControle = WsCtl_Cache
            End If
        End Set
    End Property

    Private Sub Fenetre_ONIC_MESURE_PreRender(sender As Object, e As EventArgs) Handles Me.PreRender
        WsCtl_Cache = CacheVirControle
        If WsCtl_Cache(0) = "" Then
            Exit Sub
        End If
        Call LireLaFiche()
        If VListeActivites.Items.Count > 0 Then
            Exit Sub
        End If
        VListeActivites.Items.Add("")
        Dim LstSel As List(Of String)
        LstSel = CType(V_WebFonction.PointeurGlobal, Virtualia.Net.Session.ObjetGlobal).VirReferentiel.ListeActivites
        If LstSel Is Nothing OrElse LstSel.Count = 0 Then
            Exit Sub
        End If
        For Each Element In LstSel
            VListeActivites.Items.Add(New ListItem(Element, Element))
        Next
    End Sub

    Private Sub LireLaFiche()
        Dim NumInfo As Integer
        Dim Ctl As Control
        Dim VirControle As Virtualia.Net.Controles_VCoupleEtiDonnee
        Dim IndiceI As Integer = 0

        Do
            Ctl = V_WebFonction.VirWebControle(Me.CadreInfo, "InfoH", IndiceI)
            If Ctl Is Nothing Then
                Exit Do
            End If
            NumInfo = CInt(Strings.Right(Ctl.ID, 2))
            VirControle = CType(Ctl, Virtualia.Net.Controles_VCoupleEtiDonnee)
            If WsCtl_Cache(NumInfo) Is Nothing Then
                VirControle.DonText = ""
            Else
                VirControle.DonText = WsCtl_Cache(NumInfo).ToString
            End If
            IndiceI += 1
        Loop

        Dim VirVertical As Virtualia.Net.Controles_VCoupleVerticalEtiDonnee
        IndiceI = 0
        Do
            Ctl = V_WebFonction.VirWebControle(Me.CadreInfo, "InfoV", IndiceI)
            If Ctl Is Nothing Then
                Exit Do
            End If
            NumInfo = CInt(Strings.Right(Ctl.ID, 2))
            VirVertical = CType(Ctl, Virtualia.Net.Controles_VCoupleVerticalEtiDonnee)
            If WsCtl_Cache(NumInfo) Is Nothing Then
                VirVertical.DonText = ""
            Else
                VirVertical.DonText = WsCtl_Cache(NumInfo).ToString
            End If
            IndiceI += 1
        Loop
    End Sub

    Private Sub InfoH_ValeurChange(sender As Object, e As Systeme.Evenements.DonneeChangeEventArgs) Handles InfoH01.ValeurChange, InfoH02.ValeurChange, InfoH03.ValeurChange
        WsCtl_Cache = CacheVirControle
        Dim NumInfo As Integer = CInt(Strings.Right(CType(sender, Virtualia.Net.Controles_VCoupleEtiDonnee).ID, 2))
        If WsCtl_Cache IsNot Nothing Then
            If WsCtl_Cache(NumInfo) Is Nothing Then
                CType(sender, Virtualia.Net.Controles_VCoupleEtiDonnee).DonBackColor = V_WebFonction.CouleurMaj
                CadreCmdOK.Visible = True
            ElseIf WsCtl_Cache(NumInfo) <> e.Valeur Then
                CType(sender, Virtualia.Net.Controles_VCoupleEtiDonnee).DonBackColor = V_WebFonction.CouleurMaj
                CadreCmdOK.Visible = True
            End If
            WsCtl_Cache(NumInfo) = e.Valeur
            CacheVirControle = WsCtl_Cache
        End If
    End Sub

    Private Sub InfoVerticale_ValeurChange(ByVal sender As Object, ByVal e As Systeme.Evenements.DonneeChangeEventArgs) Handles InfoV04.ValeurChange
        WsCtl_Cache = CacheVirControle
        Dim NumInfo As Integer = CInt(Strings.Right(CType(sender, Virtualia.Net.Controles_VCoupleVerticalEtiDonnee).ID, 2))
        If WsCtl_Cache IsNot Nothing Then
            If WsCtl_Cache(NumInfo) Is Nothing Then
                CadreCmdOK.Visible = True
            ElseIf WsCtl_Cache(NumInfo) <> e.Valeur Then
                CadreCmdOK.Visible = True
            End If
            WsCtl_Cache(NumInfo) = e.Valeur
            CacheVirControle = WsCtl_Cache
        End If
    End Sub

    Private Sub InitialiserControles()
        Dim Ctl As Control
        Dim VirControle As Virtualia.Net.Controles_VCoupleEtiDonnee
        Dim IndiceI As Integer = 0
        Do
            Ctl = V_WebFonction.VirWebControle(Me.CadreInfo, "InfoH", IndiceI)
            If Ctl Is Nothing Then
                Exit Do
            End If
            VirControle = CType(Ctl, Virtualia.Net.Controles_VCoupleEtiDonnee)
            VirControle.DonBackColor = Drawing.Color.White
            VirControle.DonText = ""
            IndiceI += 1
        Loop
        Dim VirVertical As Virtualia.Net.Controles_VCoupleVerticalEtiDonnee
        IndiceI = 0
        Do
            Ctl = V_WebFonction.VirWebControle(Me.CadreInfo, "InfoV", IndiceI)
            If Ctl Is Nothing Then
                Exit Do
            End If
            VirVertical = CType(Ctl, Virtualia.Net.Controles_VCoupleVerticalEtiDonnee)
            VirVertical.DonBackColor = Drawing.Color.White
            VirVertical.DonText = ""
            IndiceI += 1
        Loop
    End Sub

    Private Sub VListeActivites_SelectedIndexChanged(sender As Object, e As EventArgs) Handles VListeActivites.SelectedIndexChanged
        Dim LstSel As List(Of KeyValuePair(Of String, String))
        VListeEtapes.Items.Clear()
        VListeEtapes.Items.Add("")
        If VListeActivites.SelectedValue <> "" Then
            LstSel = CType(V_WebFonction.PointeurGlobal, Virtualia.Net.Session.ObjetGlobal).VirReferentiel.ListeActivites(VListeActivites.SelectedValue)
            For Each Element In LstSel
                VListeEtapes.Items.Add(New ListItem(Element.Key, Element.Key))
            Next
        End If
        WsCtl_Cache = CacheVirControle
        Dim FicheREF As Virtualia.TablesObjet.ShemaREF.ONIC_MESURE = Fiche_Reference(VListeActivites.SelectedValue, "")
        WsCtl_Cache(1) = ""
        WsCtl_Cache(2) = VListeActivites.SelectedValue
        WsCtl_Cache(3) = ""
        If FicheREF Is Nothing Then
            WsCtl_Cache(4) = ""
        Else
            WsCtl_Cache(4) = FicheREF.Descriptif
        End If
        CacheVirControle = WsCtl_Cache
    End Sub

    Private Sub VListeEtapes_SelectedIndexChanged(sender As Object, e As EventArgs) Handles VListeEtapes.SelectedIndexChanged
        Dim FicheREF As Virtualia.TablesObjet.ShemaREF.ONIC_MESURE
        WsCtl_Cache = CacheVirControle
        FicheREF = Fiche_Reference(WsCtl_Cache(2), VListeEtapes.SelectedValue)
        If FicheREF IsNot Nothing Then
            WsCtl_Cache(1) = CStr(FicheREF.Rang_Numero)
            WsCtl_Cache(4) = FicheREF.Descriptif
        End If
        WsCtl_Cache(3) = VListeEtapes.SelectedValue
        CacheVirControle = WsCtl_Cache
    End Sub

    Private ReadOnly Property Fiche_Reference(ByVal Projet As String, ByVal Etape As String) As Virtualia.TablesObjet.ShemaREF.ONIC_MESURE
        Get
            If Projet = "" Then
                Return Nothing
            End If
            Dim LstFiches As List(Of Virtualia.Systeme.MetaModele.VIR_FICHE)
            Dim LstActivites As List(Of Virtualia.TablesObjet.ShemaREF.ONIC_MESURE)
            LstFiches = V_WebFonction.PointeurGlobal.VirServiceServeur.LectureObjet_ToFiches(V_WebFonction.PointeurGlobal.VirNomUtilisateur, WsPointdeVue, WsNumObjet, CInt(WsCtl_Cache(0)), False)
            If LstFiches Is Nothing OrElse LstFiches.Count = 0 Then
                Return Nothing
            End If
            LstActivites = V_WebFonction.ViRhFonction.ConvertisseurListeFiches(Of Virtualia.TablesObjet.ShemaREF.ONIC_MESURE)(LstFiches)
            If Etape <> "" Then
                Return (From instance In LstActivites Select instance Where instance.Activite = Projet And instance.Mesure = Etape).FirstOrDefault
            Else
                Return (From instance In LstActivites Select instance Where instance.Activite = Projet).First
            End If
        End Get
    End Property

    Private ReadOnly Property Fiche_Reference(ByVal Rang As Integer) As Virtualia.TablesObjet.ShemaREF.ONIC_MESURE
        Get
            Dim LstFiches As List(Of Virtualia.Systeme.MetaModele.VIR_FICHE)
            Dim LstActivites As List(Of Virtualia.TablesObjet.ShemaREF.ONIC_MESURE)
            LstFiches = V_WebFonction.PointeurGlobal.VirServiceServeur.LectureObjet_ToFiches(V_WebFonction.PointeurGlobal.VirNomUtilisateur, WsPointdeVue, WsNumObjet, CInt(WsCtl_Cache(0)), False)
            If LstFiches Is Nothing OrElse LstFiches.Count = 0 Then
                Return Nothing
            End If
            LstActivites = V_WebFonction.ViRhFonction.ConvertisseurListeFiches(Of Virtualia.TablesObjet.ShemaREF.ONIC_MESURE)(LstFiches)
            Return (From instance In LstActivites Select instance Where instance.Rang_Numero = Rang).First
        End Get
    End Property

    Private ReadOnly Property New_Rang() As Integer
        Get
            Dim CptMaxi As Integer = 0
            Dim LstFiches As List(Of Virtualia.Systeme.MetaModele.VIR_FICHE)
            Dim LstActivites As List(Of Virtualia.TablesObjet.ShemaREF.ONIC_MESURE)
            LstFiches = V_WebFonction.PointeurGlobal.VirServiceServeur.LectureObjet_ToFiches(V_WebFonction.PointeurGlobal.VirNomUtilisateur, WsPointdeVue, WsNumObjet, CInt(WsCtl_Cache(0)), False)
            If LstFiches Is Nothing OrElse LstFiches.Count = 0 Then
                Return Nothing
            End If
            LstActivites = V_WebFonction.ViRhFonction.ConvertisseurListeFiches(Of Virtualia.TablesObjet.ShemaREF.ONIC_MESURE)(LstFiches)
            For Each FicheRef In LstActivites
                If FicheRef.Rang_Numero > CptMaxi Then
                    CptMaxi = FicheRef.Rang_Numero
                End If
            Next
            Return CptMaxi + 1
        End Get
    End Property

    Private Sub CommandeNew_Click(sender As Object, e As EventArgs) Handles CommandeNew.Click
        WsCtl_Cache = CacheVirControle
        For IndiceI As Integer = 1 To 5
            WsCtl_Cache(IndiceI) = ""
        Next
        CacheVirControle = WsCtl_Cache
    End Sub

    Protected Sub CommandeOK_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles CommandeOK.Click
        Dim FicheREF As Virtualia.TablesObjet.ShemaREF.ONIC_MESURE
        Dim Cretour As Boolean
        Dim TitreMsg As String = ""

        WsCtl_Cache = CacheVirControle
        '** Controle préalable Activité et Mesure obligatoires
        If WsCtl_Cache(2) = "" Or WsCtl_Cache(3) = "" Then
            Call AfficherMessage("Saisie d'un paramètre", "Projet et Etape sont obligatoires.")
            Exit Sub
        End If
        '**
        If WsCtl_Cache(1) = "" Then
            FicheREF = New Virtualia.TablesObjet.ShemaREF.ONIC_MESURE
            FicheREF.Ide_Dossier = CInt(WsCtl_Cache(0))
            FicheREF.Rang_Numero = New_Rang
            TitreMsg = "Création d'un paramètre"
        Else
            FicheREF = Fiche_Reference(CInt(WsCtl_Cache(1)))
            If FicheREF Is Nothing Then
                Exit Sub
            End If
            TitreMsg = "Modification d'un paramètre"
        End If
        FicheREF.Activite = WsCtl_Cache(2)
        FicheREF.Mesure = WsCtl_Cache(3)
        FicheREF.Descriptif = WsCtl_Cache(4)
        If WsCtl_Cache(1) = "" Then
            Cretour = V_WebFonction.PointeurGlobal.VirServiceServeur.MiseAjour_Fiche(V_WebFonction.PointeurGlobal.VirNomUtilisateur, WsPointdeVue, WsNumObjet,
                                                                                 FicheREF.Ide_Dossier, "C", "", FicheREF.ContenuTable)
        Else
            Cretour = V_WebFonction.PointeurGlobal.VirServiceServeur.MiseAjour_Fiche(V_WebFonction.PointeurGlobal.VirNomUtilisateur, WsPointdeVue, WsNumObjet,
                                                                                 FicheREF.Ide_Dossier, "M", FicheREF.FicheLue, FicheREF.ContenuTable)
        End If
        If Cretour = True Then
            CadreCmdOK.Visible = False
            WsCtl_Cache(1) = CStr(FicheREF.Rang_Numero)
            CacheVirControle = WsCtl_Cache
            Call CType(V_WebFonction.PointeurGlobal, Virtualia.Net.Session.ObjetGlobal).VirReferentiel.Initialiser()
            VListeActivites.Items.Clear()
            VListeEtapes.Items.Clear()
            Call AfficherMessage(TitreMsg, "L'opération de mise à jour a bien été effectuée.")
        Else
            Call AfficherMessage(TitreMsg, "L'opération de mise à jour n'a pas aboutie.")
        End If
    End Sub

    Private Sub CommandeRetour_Click(sender As Object, e As ImageClickEventArgs) Handles CommandeRetour.Click
        Dim Evenement As New Virtualia.Systeme.Evenements.MessageRetourEventArgs("REF", 0, "", "")
        Call V_Retour(Evenement)
    End Sub

    Private Sub AfficherMessage(ByVal TitreMsg As String, ByVal Msg As String)
        Dim NatureCmd As String = "OK"
        Dim NomEmetteur As String = "REFActivite"

        Dim Evenement As Virtualia.Systeme.Evenements.MessageSaisieEventArgs
        Evenement = New Virtualia.Systeme.Evenements.MessageSaisieEventArgs(NomEmetteur, 0, "", NatureCmd, TitreMsg, Msg)
        Call V_MessageDialog(Evenement)
    End Sub

    Private Sub CommandeSupp_Click(sender As Object, e As EventArgs) Handles CommandeSupp.Click
        Dim FicheREF As Virtualia.TablesObjet.ShemaREF.ONIC_MESURE
        Dim Cretour As Boolean
        WsCtl_Cache = CacheVirControle
        If WsCtl_Cache(1) = "" Then
            Exit Sub
        End If
        FicheREF = Fiche_Reference(CInt(WsCtl_Cache(1)))
        If FicheREF Is Nothing Then
            Exit Sub
        End If
        '** Controle Si Item Utilisé
        If SiEstUtilise(FicheREF.Activite, FicheREF.Mesure) Then
            Call AfficherMessage("Suppression d'un paramètre", "Suppression impossible car ce paramètre est utilisé.")
            Exit Sub
        End If
        '**
        Cretour = V_WebFonction.PointeurGlobal.VirServiceServeur.MiseAjour_Fiche(V_WebFonction.PointeurGlobal.VirNomUtilisateur, WsPointdeVue, WsNumObjet,
                                                                                 CInt(WsCtl_Cache(0)), "S", FicheREF.ContenuTable, "")
        If Cretour = True Then
            For IndiceI As Integer = 1 To 5
                WsCtl_Cache(IndiceI) = ""
            Next
            CacheVirControle = WsCtl_Cache
            Call CType(V_WebFonction.PointeurGlobal, Virtualia.Net.Session.ObjetGlobal).VirReferentiel.Initialiser()
            VListeActivites.Items.Clear()
            VListeEtapes.Items.Clear()
            Call AfficherMessage("Suppression d'un paramètre", "Opération de suppression effectuée.")
        Else
            Call AfficherMessage("Suppression d'un paramètre", "L'opération de suppression n'a pas aboutie.")
        End If
    End Sub

    Public ReadOnly Property SiEstUtilise(ByVal Projet As String, ByVal Etape As String) As Boolean
        Get
            Dim Constructeur As Virtualia.Systeme.Sgbd.Sql.SqlInterne
            Dim ChaineSql As String
            Dim LstRes As List(Of Virtualia.Net.ServiceServeur.VirRequeteType)
            Constructeur = New Virtualia.Systeme.Sgbd.Sql.SqlInterne(V_WebFonction.PointeurGlobal.VirModele, V_WebFonction.PointeurGlobal.VirInstanceBd)
            Constructeur.NombredeRequetes(VI.PointdeVue.PVueApplicatif, "01/01/" & Year(Now) - 2, V_WebFonction.ViRhDates.DateduJour, VI.Operateurs.ET) = 2
            Constructeur.SiHistoriquedeSituation = True
            Constructeur.NoInfoSelection(0, 112) = 2
            Constructeur.ValeuraComparer(0, VI.Operateurs.OU, VI.Operateurs.Egalite, False) = Projet
            Constructeur.NoInfoSelection(1, 112) = 3
            Constructeur.ValeuraComparer(1, VI.Operateurs.OU, VI.Operateurs.Egalite, False) = Etape
            Constructeur.InfoExtraite(0, 112, 0) = 0
            Constructeur.InfoExtraite(1, 112, 0) = 2
            Constructeur.InfoExtraite(2, 112, 0) = 3
            ChaineSql = Constructeur.OrdreSqlDynamique
            LstRes = V_WebFonction.PointeurGlobal.VirServiceServeur.RequeteSql_ToListeType(V_WebFonction.PointeurGlobal.VirNomUtilisateur, VI.PointdeVue.PVueApplicatif, 112, ChaineSql)
            If LstRes IsNot Nothing AndAlso LstRes.Count > 0 Then
                Return True
            End If
            Return False
        End Get
    End Property

    Private Sub CommandeCsv_Click(sender As Object, e As EventArgs) Handles CommandeCsv.Click
        Dim UtiSession As Virtualia.Net.Session.SessionModule = CType(V_WebFonction.PointeurUtilisateur.V_PointeurSessionModule, Virtualia.Net.Session.SessionModule)
        Dim NomFichier As String = ""
        If UtiSession Is Nothing Then
            Exit Sub
        End If
        If UtiSession.VParent.V_NomdeConnexion <> "" Then
            NomFichier = V_WebFonction.PointeurGlobal.VirRepertoireTemporaire & "\" & UtiSession.VParent.V_NomdeConnexion & "." & Strings.Right(CType(sender, System.Web.UI.Control).ID, 3)
        ElseIf UtiSession.DossierPER IsNot Nothing Then
            NomFichier = V_WebFonction.PointeurGlobal.VirRepertoireTemporaire & "\" & UtiSession.DossierPER.Nom & "." & Strings.Right(CType(sender, System.Web.UI.Control).ID, 3)
        Else
            Exit Sub
        End If
        Dim FluxTeleChargement As Byte()

        Call EcrireFichier(NomFichier, Strings.Right(CType(sender, System.Web.UI.Control).ID, 3))

        If My.Computer.FileSystem.FileExists(NomFichier) = False Then
            Exit Sub
        End If
        FluxTeleChargement = My.Computer.FileSystem.ReadAllBytes(NomFichier)
        If FluxTeleChargement IsNot Nothing Then
            Dim response As System.Web.HttpResponse = System.Web.HttpContext.Current.Response
            response.Clear()
            response.AddHeader("Content-Type", "binary/octet-stream")
            response.AddHeader("Content-Disposition", "attachment; filename=" & NomFichier & "; size=" & FluxTeleChargement.Length.ToString())
            response.Flush()
            response.BinaryWrite(FluxTeleChargement)
            response.Flush()
            response.End()
        End If
    End Sub

    Private Sub EcrireFichier(ByVal Nom_Fichier As String, ByVal Extension As String)
        Dim FicStream As System.IO.FileStream
        Dim FicWriter As System.IO.StreamWriter
        Dim SysCodeIso As System.Text.Encoding = System.Text.Encoding.GetEncoding(1252)
        Dim LstFiches As List(Of Virtualia.Systeme.MetaModele.VIR_FICHE)
        Dim LstActivites As List(Of Virtualia.TablesObjet.ShemaREF.ONIC_MESURE)
        Dim LstTries As List(Of Virtualia.TablesObjet.ShemaREF.ONIC_MESURE)
        Dim Sep As String = Strings.Chr(9)
        Dim Chaine As String

        WsCtl_Cache = CacheVirControle
        LstFiches = V_WebFonction.PointeurGlobal.VirServiceServeur.LectureObjet_ToFiches(V_WebFonction.PointeurGlobal.VirNomUtilisateur, WsPointdeVue, WsNumObjet, CInt(WsCtl_Cache(0)), False)
        If LstFiches Is Nothing OrElse LstFiches.Count = 0 Then
            Exit Sub
        End If
        LstActivites = V_WebFonction.ViRhFonction.ConvertisseurListeFiches(Of Virtualia.TablesObjet.ShemaREF.ONIC_MESURE)(LstFiches)
        LstTries = (From Fiche In LstActivites Select Fiche Order By Fiche.Activite Ascending, Fiche.Mesure Ascending).ToList
        If Extension = "Csv" Then
            Sep = VI.PointVirgule
        End If
        FicStream = New System.IO.FileStream(Nom_Fichier, IO.FileMode.Create, IO.FileAccess.Write)
        FicWriter = New System.IO.StreamWriter(FicStream, SysCodeIso)

        FicWriter.WriteLine("Projet" & Sep & "Etape" & Sep & "Observation")
        For Each Fiche As Virtualia.TablesObjet.ShemaREF.ONIC_MESURE In LstTries
            Chaine = Fiche.Descriptif.Replace(vbLf, Strings.Space(1))
            FicWriter.WriteLine(Fiche.Activite & Sep & Fiche.Mesure & Sep & Chaine.Replace(vbCr, Strings.Space(1)))
        Next
        FicWriter.Flush()
        FicWriter.Close()
    End Sub
End Class
