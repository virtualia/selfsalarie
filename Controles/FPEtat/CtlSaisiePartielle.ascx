﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="CtlSaisiePartielle.ascx.vb" Inherits="Virtualia.Net.CtlSaisiePartielle" %>

<%@ Register src="~/Controles/Saisies/VZoneSaisie.ascx" tagname="VZone" tagprefix="Virtualia" %>

<asp:Table ID="Saisie" runat="server" CellPadding="1" CellSpacing="0" Width="780px" BorderStyle="None" 
           BorderWidth="1px" BorderColor="#B0E0D7" BackColor="#5E9598" style="margin-top: 20px">
    <asp:TableRow>
          <asp:TableCell>
            <asp:Table ID="CadreCommandes" runat="server" Height="22px" CellPadding="0" CellSpacing="0"
                Width="158px" HorizontalAlign="Right" style="margin-top: 3px;margin-right: 3px">
                <asp:TableRow>
                    <asp:TableCell VerticalAlign="Bottom">
                        <asp:Table ID="CadreRetour" runat="server" BackImageUrl="~/Images/Icones/Cmd_Std.bmp" 
                            Width="70px" CellPadding="0" CellSpacing="0"
                            BorderWidth="2px" BorderStyle="Outset" BorderColor="#B0E0D7" ForeColor="#D7FAF3">
                            <asp:TableRow>
                                <asp:TableCell>
                                    <asp:Button ID="CommandeRetour" runat="server" Text="Retour" Width="68px" Height="20px"
                                        BackColor="Transparent" BorderColor="#B0E0D7" ForeColor="#D7FAF3"
                                        Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small" Font-Italic="true"
                                        BorderStyle="None" style=" margin-left: 10px; text-align: right;"
                                        Tooltip="Revenir à l'onglet initial">
                                    </asp:Button>
                                </asp:TableCell>
                            </asp:TableRow>
                        </asp:Table>    
                    </asp:TableCell>
                    <asp:TableCell VerticalAlign="Bottom">
                        <asp:Table ID="CadreCmdOK" runat="server" BackImageUrl="~/Images/Icones/Cmd_Std.bmp" Width="70px"
                            BorderWidth="2px" BorderStyle="Outset" BorderColor="#B0E0D7" ForeColor="#D7FAF3"
                            CellPadding="0" CellSpacing="0" Visible="false">
                            <asp:TableRow>
                                <asp:TableCell>
                                    <asp:Button ID="CommandeOK" runat="server" Text="Valider" Width="65px" Height="20px"
                                        BackColor="Transparent" BorderColor="#B0E0D7" ForeColor="#D7FAF3" 
                                        Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small" Font-Italic="true"
                                        BorderStyle="None" style=" margin-left: 6px; text-align: right;">
                                        </asp:Button>
                                </asp:TableCell>
                            </asp:TableRow>
                        </asp:Table>  
                    </asp:TableCell>
                </asp:TableRow>
              </asp:Table>
          </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell>
             <asp:Label ID="EtiTitre" runat="server" Text="" Visible="true"
                BackColor="#216B68" BorderColor="#B0E0D7" BorderStyle="Solid" BorderWidth="3px"
                ForeColor="White" Height="25px" Width="780px"  Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                style="margin-top: 10px; font-style: normal;
                text-indent: 5px; text-align: center"/>   
        </asp:TableCell>
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell> 
            <Virtualia:VZone ID="Zone00" runat="server">
            </Virtualia:VZone>
        </asp:TableCell>   
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell> 
            <Virtualia:VZone ID="Zone01" runat="server">
            </Virtualia:VZone>
        </asp:TableCell>   
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell> 
            <Virtualia:VZone ID="Zone02" runat="server">
            </Virtualia:VZone>
        </asp:TableCell>   
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell> 
            <Virtualia:VZone ID="Zone03" runat="server">
            </Virtualia:VZone>
        </asp:TableCell>   
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell> 
            <Virtualia:VZone ID="Zone04" runat="server">
            </Virtualia:VZone>
        </asp:TableCell>   
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell> 
            <Virtualia:VZone ID="Zone05" runat="server">
            </Virtualia:VZone>
        </asp:TableCell>   
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell> 
            <Virtualia:VZone ID="Zone06" runat="server">
            </Virtualia:VZone>
        </asp:TableCell>   
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell> 
            <Virtualia:VZone ID="Zone07" runat="server">
            </Virtualia:VZone>
        </asp:TableCell>   
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell> 
            <Virtualia:VZone ID="Zone08" runat="server">
            </Virtualia:VZone>
        </asp:TableCell>   
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell> 
            <Virtualia:VZone ID="Zone09" runat="server">
            </Virtualia:VZone>
        </asp:TableCell>   
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell> 
            <Virtualia:VZone ID="Zone10" runat="server">
            </Virtualia:VZone>
        </asp:TableCell>   
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell> 
            <Virtualia:VZone ID="Zone11" runat="server">
            </Virtualia:VZone>
        </asp:TableCell>   
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell> 
            <Virtualia:VZone ID="Zone12" runat="server">
            </Virtualia:VZone>
        </asp:TableCell>   
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell> 
            <Virtualia:VZone ID="Zone13" runat="server">
            </Virtualia:VZone>
        </asp:TableCell>   
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell> 
            <Virtualia:VZone ID="Zone14" runat="server">
            </Virtualia:VZone>
        </asp:TableCell>   
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell> 
            <Virtualia:VZone ID="Zone15" runat="server">
            </Virtualia:VZone>
        </asp:TableCell>   
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell> 
            <Virtualia:VZone ID="Zone16" runat="server">
            </Virtualia:VZone>
        </asp:TableCell>   
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell> 
            <Virtualia:VZone ID="Zone17" runat="server">
            </Virtualia:VZone>
        </asp:TableCell>   
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell> 
            <Virtualia:VZone ID="Zone18" runat="server">
            </Virtualia:VZone>
        </asp:TableCell>   
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell> 
            <Virtualia:VZone ID="Zone19" runat="server">
            </Virtualia:VZone>
        </asp:TableCell>   
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell> 
            <Virtualia:VZone ID="Zone20" runat="server">
            </Virtualia:VZone>
        </asp:TableCell>   
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell> 
            <Virtualia:VZone ID="Zone21" runat="server">
            </Virtualia:VZone>
        </asp:TableCell>   
    </asp:TableRow>
    <asp:TableRow>
        <asp:TableCell>
             <asp:TextBox ID="MsgOperation" runat="server" Text="" Visible="true" AutoPostBack="false" ReadOnly="true"
                BackColor="#1C5151" ForeColor="#FF9849" BorderColor="#B0E0D7" BorderStyle="Solid" BorderWidth="3px"
                Height="25px" Width="780px" Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                style="margin-top: 2px; font-style: normal;
                text-indent: 5px; text-align: center"/>   
        </asp:TableCell>
    </asp:TableRow>
</asp:Table>
