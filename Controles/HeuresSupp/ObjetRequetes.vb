﻿Option Explicit On
Option Strict Off
Option Compare Text
Imports VI = Virtualia.Systeme.Constantes
Imports Virtualia.Systeme.Sgbd.Sql
Public Class ObjetRequetes
    Private WsParent As Session.ObjetSession

    Public Sub New(ByVal Host As Session.ObjetSession)
        WsParent = Host
    End Sub

    Public Function LesHeuresSupplementaires_APayer(ByVal DateValeur As String) As List(Of Virtualia.Net.ServiceServeur.VirRequeteType)
        Dim ConstructeurHS As SqlInterne = New SqlInterne(WsParent.V_PointeurGlobal.VirModele, WsParent.V_PointeurGlobal.VirInstanceBd)
        Dim LstSel As List(Of SqlInterne_Selection) = New List(Of SqlInterne_Selection)()
        Dim DonSel As SqlInterne_Selection
        Dim LstExt As List(Of SqlInterne_Extraction) = New List(Of SqlInterne_Extraction)()
        Dim ObjetStructure As SqlInterne_Structure = New SqlInterne_Structure(VI.PointdeVue.PVueApplicatif)
        Dim LstFiltres As List(Of String)

        DonSel = New SqlInterne_Selection(VI.ObjetPer.ObaHeuresSup, 0)
        DonSel.Operateur_Comparaison = VI.Operateurs.Inclu
        DonSel.Valeurs_AComparer = (New String() {"01" & Strings.Right(DateValeur, 8), WsParent.V_PointeurGlobal.VirRhDates.DateSaisieVerifiee("31" & Strings.Right(DateValeur, 8))}).ToList()
        LstSel.Add(DonSel)

        LstExt.Add(New SqlInterne_Extraction(VI.ObjetPer.ObaCivil, 2, 0))
        LstExt.Add(New SqlInterne_Extraction(VI.ObjetPer.ObaCivil, 3, 0))

        ObjetStructure.SiForcerClauseDistinct = True
        ObjetStructure.Operateur_Liaison = VI.Operateurs.ET
        ObjetStructure.Date_Debut = ""
        ObjetStructure.Date_Fin = DateTime.MaxValue.ToShortDateString

        ConstructeurHS.StructureRequete = ObjetStructure
        ConstructeurHS.ListeInfosAExtraire = LstExt
        ConstructeurHS.ListeInfosASelectionner = LstSel
        LstFiltres = ListeConfidentialite("31/12/" & DateTime.Now.Year)
        For Each Clause In LstFiltres
            ConstructeurHS.ClauseFiltreIN = Clause
        Next
        Dim Requete As String = ConstructeurHS.OrdreSqlDynamique

        Dim LstResultat As List(Of Virtualia.Net.ServiceServeur.VirRequeteType) = WsParent.V_PointeurGlobal.VirServiceServeur.RequeteSql_ToListeType(WsParent.V_PointeurGlobal.VirNomUtilisateur, 1, VI.ObjetPer.ObaCivil, Requete)
        '************AKR
        'Call WsParent.V_PointeurGlobal.EcrireLogTraitement("HeureSupp", True, "HEURES SUPP A VALIDER:" & Requete)
        '****************
        If LstResultat Is Nothing OrElse LstResultat.Count = 0 Then
            Return Nothing
        End If
        Return (From Resultat In LstResultat Order By Resultat.Valeurs(0), Resultat.Valeurs(1) Select Resultat).ToList
    End Function

    Public Function LesHeuresSupplementaires_AValider(ByVal DateValeur As String) As List(Of Virtualia.Net.ServiceServeur.VirRequeteType)
        Dim ConstructeurHS As SqlInterne = New SqlInterne(WsParent.V_PointeurGlobal.VirModele, WsParent.V_PointeurGlobal.VirInstanceBd)
        Dim LstSel As List(Of SqlInterne_Selection) = New List(Of SqlInterne_Selection)()
        Dim DonSel As SqlInterne_Selection
        Dim LstExt As List(Of SqlInterne_Extraction) = New List(Of SqlInterne_Extraction)()
        Dim ObjetStructure As SqlInterne_Structure = New SqlInterne_Structure(VI.PointdeVue.PVueApplicatif)
        Dim LstFiltres As List(Of String)


        DonSel = New SqlInterne_Selection(VI.ObjetPer.ObaMensuelHS, 0)
        DonSel.Operateur_Comparaison = VI.Operateurs.Inclu
        DonSel.Valeurs_AComparer = (New String() {"01" & Strings.Right(DateValeur, 8), WsParent.V_PointeurGlobal.VirRhDates.DateSaisieVerifiee("31" & Strings.Right(DateValeur, 8))}).ToList()
        LstSel.Add(DonSel)

        DonSel = New SqlInterne_Selection(VI.ObjetPer.ObaMensuelHS, 13)
        DonSel.Operateur_Comparaison = VI.Operateurs.Egalite
        DonSel.Valeurs_AComparer = (New String() {"Accord"}).ToList()
        LstSel.Add(DonSel)


        'DonSel = New SqlInterne_Selection(VI.ObjetPer.ObaMensuelHS, 17)
        'DonSel.Operateur_Comparaison = VI.Operateurs.Exclu
        'DonSel.Valeurs_AComparer = (New String() {"Accord", "Refus"}).ToList()
        'LstSel.Add(DonSel)

        ' *************AKR
        DonSel = New SqlInterne_Selection(VI.ObjetPer.ObaMensuelHS, 17)
        DonSel.Operateur_Comparaison = VI.Operateurs.Egalite
        DonSel.Valeurs_AComparer = (New String() {"NULLE"}).ToList()
        LstSel.Add(DonSel)
        '*************

        LstExt.Add(New SqlInterne_Extraction(VI.ObjetPer.ObaCivil, 2, 0))
        LstExt.Add(New SqlInterne_Extraction(VI.ObjetPer.ObaCivil, 3, 0))

        ObjetStructure.SiForcerClauseDistinct = True
        ObjetStructure.Operateur_Liaison = VI.Operateurs.ET
        ObjetStructure.Date_Debut = ""
        ObjetStructure.Date_Fin = DateTime.MaxValue.ToShortDateString

        ConstructeurHS.StructureRequete = ObjetStructure
        ConstructeurHS.ListeInfosAExtraire = LstExt
        ConstructeurHS.ListeInfosASelectionner = LstSel
        LstFiltres = ListeConfidentialite("31/12/" & DateTime.Now.Year)
        For Each Clause In LstFiltres
            ConstructeurHS.ClauseFiltreIN = Clause
        Next
        Dim Requete As String = ConstructeurHS.OrdreSqlDynamique

        '************AKR
        If WsParent.V_PointeurGlobal.VirInstanceBd.TypeduSgbd = VI.TypeSgbdrNumeric.SgbdrMySql Then
            Requete = Requete.Replace("AND y.Date_de_valeur <= '9999-12-31' AND x.Date_de_valeur <= '9999-12-31'", "")
            Requete = Requete.Replace("GROUP BY a.Ide_Dossier, d.Nom, d.Prenom, a.Date_de_valeur HAVING a.Date_de_valeur = MAX(a.Date_de_valeur) AND a.Date_de_valeur = MAX(a.Date_de_valeur) AND a.Date_de_valeur = MAX(a.Date_de_valeur)", "")
        End If
        'Call WsParent.V_PointeurGlobal.EcrireLogTraitement("HeureSupp", True, "HEURES SUPP A PAYER  :" & Requete)
        '****************

        Dim LstResultat As List(Of Virtualia.Net.ServiceServeur.VirRequeteType) = WsParent.V_PointeurGlobal.VirServiceServeur.RequeteSql_ToListeType(WsParent.V_PointeurGlobal.VirNomUtilisateur, 1, VI.ObjetPer.ObaCivil, Requete)

        If LstResultat Is Nothing OrElse LstResultat.Count = 0 Then
            Return Nothing
        End If
        Return (From Resultat In LstResultat Order By Resultat.Valeurs(0), Resultat.Valeurs(1) Select Resultat).ToList
    End Function

    Public Function LesAstreintes_AValider(ByVal DateValeur As String) As List(Of Virtualia.Net.ServiceServeur.VirRequeteType)
        Dim ConstructeurHS As SqlInterne = New SqlInterne(WsParent.V_PointeurGlobal.VirModele, WsParent.V_PointeurGlobal.VirInstanceBd)
        Dim LstSel As List(Of SqlInterne_Selection) = New List(Of SqlInterne_Selection)()
        Dim DonSel As SqlInterne_Selection
        Dim LstExt As List(Of SqlInterne_Extraction) = New List(Of SqlInterne_Extraction)()
        Dim ObjetStructure As SqlInterne_Structure = New SqlInterne_Structure(VI.PointdeVue.PVueApplicatif)
        Dim LstFiltres As List(Of String)

        DonSel = New SqlInterne_Selection(171, 0)
        DonSel.Operateur_Comparaison = VI.Operateurs.Inclu
        DonSel.Valeurs_AComparer = (New String() {"01" & Strings.Right(DateValeur, 8), WsParent.V_PointeurGlobal.VirRhDates.DateSaisieVerifiee("31" & Strings.Right(DateValeur, 8))}).ToList()
        LstSel.Add(DonSel)

        DonSel = New SqlInterne_Selection(171, 4)
        'DonSel.Operateur_Comparaison = VI.Operateurs.Difference
        'DonSel.Valeurs_AComparer = (New String() {"Oui"}).ToList()
        '***********AKR
        DonSel.Operateur_Comparaison = VI.Operateurs.Egalite
        DonSel.Valeurs_AComparer = (New String() {"NULLE"}).ToList()
        '************
        LstSel.Add(DonSel)

        LstExt.Add(New SqlInterne_Extraction(VI.ObjetPer.ObaCivil, 2, 0))
        LstExt.Add(New SqlInterne_Extraction(VI.ObjetPer.ObaCivil, 3, 0))
        LstExt.Add(New SqlInterne_Extraction(171, 5, 0))

        ObjetStructure.SiForcerClauseDistinct = True
        ObjetStructure.Operateur_Liaison = VI.Operateurs.ET
        ObjetStructure.Date_Debut = ""
        ObjetStructure.Date_Fin = DateTime.MaxValue.ToShortDateString

        ConstructeurHS.StructureRequete = ObjetStructure
        ConstructeurHS.ListeInfosAExtraire = LstExt
        ConstructeurHS.ListeInfosASelectionner = LstSel
        LstFiltres = ListeConfidentialite("31/12/" & DateTime.Now.Year)
        For Each Clause In LstFiltres
            ConstructeurHS.ClauseFiltreIN = Clause
        Next
        Dim Requete As String = ConstructeurHS.OrdreSqlDynamique

        '************AKR
        If WsParent.V_PointeurGlobal.VirInstanceBd.TypeduSgbd = VI.TypeSgbdrNumeric.SgbdrMySql Then
            Requete = Requete.Replace("AND y.Date_astreinte <= '9999-12-31'", "")
            Requete = Requete.Replace("GROUP BY a.Ide_Dossier, c.Nom, c.Prenom, a.Date_validation, a.Date_astreinte HAVING a.Date_astreinte = MAX(a.Date_astreinte) AND a.Date_astreinte = MAX(a.Date_astreinte)", "")
        End If
        'Call WsParent.V_PointeurGlobal.EcrireLogTraitement("HeureSupp", True, "ASTREINTE A VALIDER  :" & Requete)
        '****************

        Dim LstResultat As List(Of Virtualia.Net.ServiceServeur.VirRequeteType) = WsParent.V_PointeurGlobal.VirServiceServeur.RequeteSql_ToListeType(WsParent.V_PointeurGlobal.VirNomUtilisateur, 1, VI.ObjetPer.ObaCivil, Requete)

        If LstResultat Is Nothing OrElse LstResultat.Count = 0 Then
            Return Nothing
        End If
        Return (From Resultat In LstResultat Order By Resultat.Valeurs(0), Resultat.Valeurs(1) Select Resultat Where Resultat.Valeurs(2) = "").ToList
    End Function

    Public Function LesAstreintes_APayer(ByVal DateValeur As String) As List(Of Virtualia.Net.ServiceServeur.VirRequeteType)
        Dim ConstructeurHS As SqlInterne = New SqlInterne(WsParent.V_PointeurGlobal.VirModele, WsParent.V_PointeurGlobal.VirInstanceBd)
        Dim LstSel As List(Of SqlInterne_Selection) = New List(Of SqlInterne_Selection)()
        Dim DonSel As SqlInterne_Selection
        Dim LstExt As List(Of SqlInterne_Extraction) = New List(Of SqlInterne_Extraction)()
        Dim ObjetStructure As SqlInterne_Structure = New SqlInterne_Structure(VI.PointdeVue.PVueApplicatif)
        Dim LstFiltres As List(Of String)

        DonSel = New SqlInterne_Selection(171, 0)
        DonSel.Operateur_Comparaison = VI.Operateurs.Inclu
        DonSel.Valeurs_AComparer = (New String() {"01" & Strings.Right(DateValeur, 8), WsParent.V_PointeurGlobal.VirRhDates.DateSaisieVerifiee("31" & Strings.Right(DateValeur, 8))}).ToList()
        LstSel.Add(DonSel)

        DonSel = New SqlInterne_Selection(171, 4)
        DonSel.Operateur_Comparaison = VI.Operateurs.Egalite
        DonSel.Valeurs_AComparer = (New String() {"Oui"}).ToList()
        LstSel.Add(DonSel)

        DonSel = New SqlInterne_Selection(171, 5)
        DonSel.Operateur_Comparaison = VI.Operateurs.Difference
        DonSel.Valeurs_AComparer = (New String() {""}).ToList()
        LstSel.Add(DonSel)

        LstExt.Add(New SqlInterne_Extraction(VI.ObjetPer.ObaCivil, 2, 0))
        LstExt.Add(New SqlInterne_Extraction(VI.ObjetPer.ObaCivil, 3, 0))

        ObjetStructure.SiForcerClauseDistinct = True
        ObjetStructure.Operateur_Liaison = VI.Operateurs.ET
        ObjetStructure.Date_Debut = ""
        ObjetStructure.Date_Fin = DateTime.MaxValue.ToShortDateString

        ConstructeurHS.StructureRequete = ObjetStructure
        ConstructeurHS.ListeInfosAExtraire = LstExt
        ConstructeurHS.ListeInfosASelectionner = LstSel
        LstFiltres = ListeConfidentialite("31/12/" & DateTime.Now.Year)
        For Each Clause In LstFiltres
            ConstructeurHS.ClauseFiltreIN = Clause
        Next
        Dim Requete As String = ConstructeurHS.OrdreSqlDynamique

        '************AKR 
        If WsParent.V_PointeurGlobal.VirInstanceBd.TypeduSgbd = VI.TypeSgbdrNumeric.SgbdrMySql Then
            Requete = Requete.Replace("d.Prenom, a.Date_astreinte", "d.Prenom")
            Requete = Requete.Replace("AND y.Date_astreinte <= '9999-12-31' AND x.Date_astreinte <= '9999-12-31'", "")
            Requete = Requete.Replace("GROUP BY a.Ide_Dossier, d.Nom, d.Prenom HAVING a.Date_astreinte = MAX(a.Date_astreinte) AND a.Date_astreinte = MAX(a.Date_astreinte) AND a.Date_astreinte = MAX(a.Date_astreinte)", "")
        End If
        'Call WsParent.V_PointeurGlobal.EcrireLogTraitement("HeureSupp", True, "ASTREINTE A PAYER    :" & Requete)
        '****************

        Dim LstResultat As List(Of Virtualia.Net.ServiceServeur.VirRequeteType) = WsParent.V_PointeurGlobal.VirServiceServeur.RequeteSql_ToListeType(WsParent.V_PointeurGlobal.VirNomUtilisateur, 1, VI.ObjetPer.ObaCivil, Requete)

        If LstResultat Is Nothing OrElse LstResultat.Count = 0 Then
            Return Nothing
        End If
        Return (From Resultat In LstResultat Order By Resultat.Valeurs(0), Resultat.Valeurs(1) Select Resultat).ToList
    End Function

    Public ReadOnly Property ListeConfidentialite(Optional ByVal DateFin As String = "") As List(Of String)
        Get
            Dim LstConfidentialite As List(Of String)
            If DateFin = "" Then
                DateFin = WsParent.V_PointeurGlobal.VirRhDates.DateduJour
            End If
            LstConfidentialite = New List(Of String)
            If WsParent.Etablissement <> "" Then
                LstConfidentialite.Add(ClauseFiltre_Etablissement(DateFin))
            End If
            If WsParent.FiltreV3 <> "" Then
                LstConfidentialite.Add(WsParent.FiltreV3.Replace(Strings.Right(WsParent.V_PointeurGlobal.VirRhDates.DateduJour, 4), Strings.Right(DateFin, 4)))
            End If
            Return LstConfidentialite
        End Get
    End Property

    Private ReadOnly Property ClauseFiltre_Etablissement(ByVal DateFin As String) As String
        Get
            Dim Constructeur As Virtualia.Systeme.Sgbd.Sql.SqlInterne
            Dim ChaineSql As String

            Constructeur = New Virtualia.Systeme.Sgbd.Sql.SqlInterne(WsParent.V_PointeurGlobal.VirModele, WsParent.V_PointeurGlobal.VirInstanceBd)
            Constructeur.LettreAlias = "eta"
            Constructeur.NombredeRequetes(VI.PointdeVue.PVueApplicatif, "01/01/" & Strings.Right(DateFin, 4), "31/12/" & Strings.Right(DateFin, 4), VI.Operateurs.ET) = 1
            Constructeur.SiPasdeTriSurIdeDossier = True
            Constructeur.SiHistoriquedeSituation = False
            Constructeur.SiForcerClauseDistinct = True
            Constructeur.NoInfoSelection(0, VI.ObjetPer.ObaSociete) = 1
            Constructeur.ValeuraComparer(0, VI.Operateurs.OU, VI.Operateurs.Egalite, False) = WsParent.Etablissement

            ChaineSql = Constructeur.OrdreSqlDynamique
            Constructeur = Nothing
            Return ChaineSql
        End Get
    End Property
End Class
