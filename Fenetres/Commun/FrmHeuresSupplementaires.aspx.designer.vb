﻿'------------------------------------------------------------------------------
' <généré automatiquement>
'     Ce code a été généré par un outil.
'
'     Les modifications apportées à ce fichier peuvent provoquer un comportement incorrect et seront perdues si
'     le code est régénéré.
' </généré automatiquement>
'------------------------------------------------------------------------------

Option Strict On
Option Explicit On


Partial Public Class FrmHeuresSupplementaires
    
    '''<summary>
    '''Contrôle CadreGeneral.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CadreGeneral As Global.System.Web.UI.WebControls.Table
    
    '''<summary>
    '''Contrôle MultiVues.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents MultiVues As Global.System.Web.UI.WebControls.MultiView
    
    '''<summary>
    '''Contrôle VueHeureSup.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents VueHeureSup As Global.System.Web.UI.WebControls.View
    
    '''<summary>
    '''Contrôle CadreAffichage.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CadreAffichage As Global.System.Web.UI.WebControls.Panel
    
    '''<summary>
    '''Contrôle CadreHSupp.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CadreHSupp As Global.System.Web.UI.WebControls.Table
    
    '''<summary>
    '''Contrôle CadreIdentite.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CadreIdentite As Global.System.Web.UI.WebControls.Table
    
    '''<summary>
    '''Contrôle CellRetour.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CellRetour As Global.System.Web.UI.WebControls.TableCell
    
    '''<summary>
    '''Contrôle CommandeRetour.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CommandeRetour As Global.System.Web.UI.WebControls.ImageButton
    
    '''<summary>
    '''Contrôle EtiIdentité.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents EtiIdentité As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle CommandeAstreinte.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CommandeAstreinte As Global.System.Web.UI.WebControls.Button
    
    '''<summary>
    '''Contrôle CadreSituationHS.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CadreSituationHS As Global.System.Web.UI.WebControls.Table
    
    '''<summary>
    '''Contrôle CadreSoldeAnnuel.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CadreSoldeAnnuel As Global.System.Web.UI.WebControls.Table
    
    '''<summary>
    '''Contrôle EtiTitreSolde.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents EtiTitreSolde As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle EtiSoldeAnte.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents EtiSoldeAnte As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle EtiTotalHS.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents EtiTotalHS As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle EtiTotalRemu.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents EtiTotalRemu As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle EtiTotalRecup.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents EtiTotalRecup As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle Eti2MoisCompensable.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Eti2MoisCompensable As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle Eti2MoisRecup.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Eti2MoisRecup As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle EtiSoldeHS.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents EtiSoldeHS As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle DonSoldeAnte.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents DonSoldeAnte As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle DonTotalHS.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents DonTotalHS As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle DonTotalRemu.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents DonTotalRemu As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle DonTotalRecup.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents DonTotalRecup As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle Don2MoisCompensable.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Don2MoisCompensable As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle Don2MoisRecup.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Don2MoisRecup As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle DonSoldeHS.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents DonSoldeHS As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle CadreDetailMensuel.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CadreDetailMensuel As Global.System.Web.UI.WebControls.Table
    
    '''<summary>
    '''Contrôle EtiSelMoisAnnee.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents EtiSelMoisAnnee As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle DropDownMoisAnnee.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents DropDownMoisAnnee As Global.System.Web.UI.WebControls.DropDownList
    
    '''<summary>
    '''Contrôle HsMensuel.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents HsMensuel As Global.Virtualia.Net.CtlPerHeureSupMensuel
    
    '''<summary>
    '''Contrôle EtiListeHS.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents EtiListeHS As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle ListeDetailHS.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents ListeDetailHS As Global.Virtualia.Net.VGrillePersonnalisee
    
    '''<summary>
    '''Contrôle EtiListeRecup.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents EtiListeRecup As Global.System.Web.UI.WebControls.Label
    
    '''<summary>
    '''Contrôle ListeRecup.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents ListeRecup As Global.Virtualia.Net.Controles_VListeGrid
    
    '''<summary>
    '''Contrôle VueAstreinte.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents VueAstreinte As Global.System.Web.UI.WebControls.View
    
    '''<summary>
    '''Contrôle PER_ASTREINTE_171.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents PER_ASTREINTE_171 As Global.Virtualia.Net.PER_JOUR_ASTREINTE
    
    '''<summary>
    '''Contrôle VueArmoire.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents VueArmoire As Global.System.Web.UI.WebControls.View
    
    '''<summary>
    '''Contrôle ArmoirePER.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents ArmoirePER As Global.Virtualia.Net.VArmoirePER
    
    '''<summary>
    '''Contrôle CellSaisieOri.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CellSaisieOri As Global.System.Web.UI.WebControls.TableCell
    
    '''<summary>
    '''Contrôle PopupSaisie.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents PopupSaisie As Global.AjaxControlToolkit.ModalPopupExtender
    
    '''<summary>
    '''Contrôle PanelSaisiePopup.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents PanelSaisiePopup As Global.System.Web.UI.WebControls.Panel
    
    '''<summary>
    '''Contrôle SaisieFaitGen.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents SaisieFaitGen As Global.Virtualia.Net.CtlSaisieFaitGenerateur
    
    '''<summary>
    '''Contrôle HPopupSaisie.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents HPopupSaisie As Global.System.Web.UI.WebControls.HiddenField
    
    '''<summary>
    '''Contrôle CellSaisieHS.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CellSaisieHS As Global.System.Web.UI.WebControls.TableCell
    
    '''<summary>
    '''Contrôle PopupDemande.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents PopupDemande As Global.AjaxControlToolkit.ModalPopupExtender
    
    '''<summary>
    '''Contrôle PanelDemandePopup.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents PanelDemandePopup As Global.System.Web.UI.WebControls.Panel
    
    '''<summary>
    '''Contrôle SaisieDemande.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents SaisieDemande As Global.Virtualia.Net.CtlSaisieDemandeHS
    
    '''<summary>
    '''Contrôle HPopupDemande.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents HPopupDemande As Global.System.Web.UI.WebControls.HiddenField
    
    '''<summary>
    '''Propriété Master.
    '''</summary>
    '''<remarks>
    '''Propriété générée automatiquement.
    '''</remarks>
    Public Shadows ReadOnly Property Master() As Virtualia.Net.VirtualiaMain
        Get
            Return CType(MyBase.Master,Virtualia.Net.VirtualiaMain)
        End Get
    End Property
End Class
