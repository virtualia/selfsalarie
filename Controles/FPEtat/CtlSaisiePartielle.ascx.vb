﻿Option Strict On
Option Explicit On
Option Compare Text
Imports VI = Virtualia.Systeme.Constantes
Public Class CtlSaisiePartielle
    Inherits System.Web.UI.UserControl
    Public Delegate Sub Retour_MsgEventHandler(ByVal sender As Object, ByVal e As Virtualia.Systeme.Evenements.MessageRetourEventArgs)
    Public Event ValeurRetour As Retour_MsgEventHandler
    Private WebFct As Virtualia.Net.Controles.WebFonctions
    Private WsNomStateIde As String = "IdeSaisie"

    Public Property V_Identifiant() As Integer
        Get
            Dim VCache As ArrayList
            If Me.ViewState(WsNomStateIde) IsNot Nothing Then
                VCache = CType(Me.ViewState(WsNomStateIde), ArrayList)
                Return CInt(VCache(0))
            End If
            Return 0
        End Get
        Set(ByVal value As Integer)
            Dim VCache As ArrayList
            If Me.ViewState(WsNomStateIde) IsNot Nothing Then
                VCache = CType(Me.ViewState(WsNomStateIde), ArrayList)
                VCache(0) = value
                Me.ViewState.Remove(WsNomStateIde)
            Else
                VCache = New ArrayList
                VCache.Add(value) 'Identifiant
                VCache.Add("") 'Occurence
                VCache.Add("") 'Parametre
                VCache.Add("") 'Régime Prime
            End If
            Me.ViewState.Add(WsNomStateIde, VCache)
            MsgOperation.Text = ""
        End Set
    End Property

    Public Property V_Occurence As String
        Get
            Dim VCache As ArrayList
            If Me.ViewState(WsNomStateIde) IsNot Nothing Then
                VCache = CType(Me.ViewState(WsNomStateIde), ArrayList)
                Return VCache(1).ToString
            End If
            Return ""
        End Get
        Set(ByVal value As String)
            Dim VCache As ArrayList
            If Me.ViewState(WsNomStateIde) IsNot Nothing Then
                VCache = CType(Me.ViewState(WsNomStateIde), ArrayList)
                VCache(1) = value
                Me.ViewState.Remove(WsNomStateIde)
                Me.ViewState.Add(WsNomStateIde, VCache)
            End If
        End Set
    End Property

    Public Property V_Parametre As String
        Get
            Dim VCache As ArrayList
            If Me.ViewState(WsNomStateIde) IsNot Nothing Then
                VCache = CType(Me.ViewState(WsNomStateIde), ArrayList)
                Return VCache(2).ToString
            End If
            Return ""
        End Get
        Set(ByVal value As String)
            Dim VCache As ArrayList
            If Me.ViewState(WsNomStateIde) IsNot Nothing Then
                VCache = CType(Me.ViewState(WsNomStateIde), ArrayList)
                VCache(2) = value
                Me.ViewState.Remove(WsNomStateIde)
                Me.ViewState.Add(WsNomStateIde, VCache)
            End If
        End Set
    End Property

    Public Property V_Regime As String
        Get
            Dim VCache As ArrayList
            If Me.ViewState(WsNomStateIde) IsNot Nothing Then
                VCache = CType(Me.ViewState(WsNomStateIde), ArrayList)
                Return VCache(3).ToString
            End If
            Return ""
        End Get
        Set(ByVal value As String)
            Dim VCache As ArrayList
            If Me.ViewState(WsNomStateIde) IsNot Nothing Then
                VCache = CType(Me.ViewState(WsNomStateIde), ArrayList)
                VCache(3) = value
                Me.ViewState.Remove(WsNomStateIde)
                Me.ViewState.Add(WsNomStateIde, VCache)
            End If
        End Set
    End Property

    Protected Overridable Sub ReponseRetour(ByVal e As Virtualia.Systeme.Evenements.MessageRetourEventArgs)
        RaiseEvent ValeurRetour(Me, e)
    End Sub

    Private Sub Page_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init
        WebFct = New Virtualia.Net.Controles.WebFonctions(Me, 0)
        Saisie.BackColor = WebFct.CouleurCharte(1, "Cadre")
    End Sub

    Private ReadOnly Property WebCtlZone(ByVal Index As Integer) As Virtualia.Net.VZoneSaisie
        Get
            Dim IndiceI As Integer
            Dim Ctl As Control
            Dim VirControle As Virtualia.Net.VZoneSaisie

            IndiceI = 0
            Do
                Ctl = WebFct.VirWebControle(Me.Saisie, "Zone", IndiceI)
                If Ctl Is Nothing Then
                    Exit Do
                End If
                VirControle = CType(Ctl, Virtualia.Net.VZoneSaisie)
                If IsNumeric(Strings.Right(VirControle.ID, 2)) Then
                    If CInt(Strings.Right(VirControle.ID, 2)) = Index Then
                        Return VirControle
                    End If
                End If
                IndiceI += 1
            Loop
            Return Nothing
        End Get
    End Property

    Public ReadOnly Property NombreZones As Integer
        Get
            Return 22
        End Get
    End Property

    Public Property V_Titre As String
        Get
            Return EtiTitre.Text
        End Get
        Set(ByVal value As String)
            EtiTitre.Text = value
        End Set
    End Property

    Public Property SiZoneVisible(ByVal Index As Integer) As Boolean
        Get
            Dim VirControle As Virtualia.Net.VZoneSaisie = WebCtlZone(Index)
            If VirControle IsNot Nothing Then
                Return VirControle.SiVisible
            Else
                Return Nothing
            End If
        End Get
        Set(ByVal value As Boolean)
            Dim VirControle As Virtualia.Net.VZoneSaisie = WebCtlZone(Index)
            If VirControle IsNot Nothing Then
                VirControle.SiVisible = value
            End If
        End Set
    End Property

    Public Property SiZoneComboEnable(ByVal Index As Integer) As Boolean
        Get
            Dim VirControle As Virtualia.Net.VZoneSaisie = WebCtlZone(Index)
            If VirControle IsNot Nothing Then
                Return VirControle.SiComboEnable
            Else
                Return Nothing
            End If
        End Get
        Set(ByVal value As Boolean)
            Dim VirControle As Virtualia.Net.VZoneSaisie = WebCtlZone(Index)
            If VirControle IsNot Nothing Then
                VirControle.SiComboEnable = value
            End If
        End Set
    End Property

    Public Property SiZoneComboVisible(ByVal Index As Integer) As Boolean
        Get
            Dim VirControle As Virtualia.Net.VZoneSaisie = WebCtlZone(Index)
            If VirControle IsNot Nothing Then
                Return VirControle.SiComboVisible
            Else
                Return Nothing
            End If
        End Get
        Set(ByVal value As Boolean)
            Dim VirControle As Virtualia.Net.VZoneSaisie = WebCtlZone(Index)
            If VirControle IsNot Nothing Then
                VirControle.SiComboVisible = value
            End If
        End Set
    End Property

    Public Property SiZoneNonSaisissable(ByVal Index As Integer) As Boolean
        Get
            Dim VirControle As Virtualia.Net.VZoneSaisie = WebCtlZone(Index)
            If VirControle IsNot Nothing Then
                Return VirControle.SiNonSaisissable
            Else
                Return Nothing
            End If
        End Get
        Set(ByVal value As Boolean)
            Dim VirControle As Virtualia.Net.VZoneSaisie = WebCtlZone(Index)
            If VirControle IsNot Nothing Then
                VirControle.SiNonSaisissable = value
            End If
            If value = False Then
                CadreCmdOK.Visible = True
            End If
        End Set
    End Property

    Public Property Etiquette(ByVal Index As Integer) As String
        Get
            Dim VirControle As Virtualia.Net.VZoneSaisie = WebCtlZone(Index)
            If VirControle IsNot Nothing Then
                Return VirControle.Etiquette
            Else
                Return ""
            End If
        End Get
        Set(ByVal value As String)
            Dim VirControle As Virtualia.Net.VZoneSaisie = WebCtlZone(Index)
            If VirControle IsNot Nothing Then
                VirControle.Etiquette = value
            End If
        End Set
    End Property

    Public Property ToolTipAide(ByVal Index As Integer) As String
        Get
            Dim VirControle As Virtualia.Net.VZoneSaisie = WebCtlZone(Index)
            If VirControle IsNot Nothing Then
                Return VirControle.ToolTipAide
            Else
                Return ""
            End If
        End Get
        Set(ByVal value As String)
            Dim VirControle As Virtualia.Net.VZoneSaisie = WebCtlZone(Index)
            If VirControle IsNot Nothing Then
                VirControle.ToolTipAide = value
            End If
        End Set
    End Property

    Public Property Donnee(ByVal Index As Integer) As String
        Get
            Dim VirControle As Virtualia.Net.VZoneSaisie = WebCtlZone(Index)
            If VirControle IsNot Nothing Then
                Return VirControle.TexteSaisi
            Else
                Return ""
            End If
        End Get
        Set(ByVal value As String)
            Dim VirControle As Virtualia.Net.VZoneSaisie = WebCtlZone(Index)
            If VirControle IsNot Nothing Then
                VirControle.TexteSaisi = value
            End If
        End Set
    End Property

    Public Property NatureDonnee(ByVal Index As Integer) As String
        Get
            Dim VirControle As Virtualia.Net.VZoneSaisie = WebCtlZone(Index)
            If VirControle IsNot Nothing Then
                Return VirControle.NatureDonnee
            Else
                Return ""
            End If
        End Get
        Set(ByVal value As String)
            Dim VirControle As Virtualia.Net.VZoneSaisie = WebCtlZone(Index)
            If VirControle IsNot Nothing Then
                VirControle.NatureDonnee = value
            End If
        End Set
    End Property

    Public Property FormatDonnee(ByVal Index As Integer) As String
        Get
            Dim VirControle As Virtualia.Net.VZoneSaisie = WebCtlZone(Index)
            If VirControle IsNot Nothing Then
                Return VirControle.FormatDonnee
            Else
                Return ""
            End If
        End Get
        Set(ByVal value As String)
            Dim VirControle As Virtualia.Net.VZoneSaisie = WebCtlZone(Index)
            If VirControle IsNot Nothing Then
                VirControle.FormatDonnee = value
            End If
        End Set
    End Property

    Public Property LongueurDonnee(ByVal Index As Integer) As Integer
        Get
            Dim VirControle As Virtualia.Net.VZoneSaisie = WebCtlZone(Index)
            If VirControle IsNot Nothing Then
                Return VirControle.Longueur
            Else
                Return 0
            End If
        End Get
        Set(ByVal value As Integer)
            Dim VirControle As Virtualia.Net.VZoneSaisie = WebCtlZone(Index)
            If VirControle IsNot Nothing Then
                VirControle.Longueur = value
            End If
        End Set
    End Property

    Public Property ListeValeurs(ByVal Index As Integer) As List(Of String)
        Get
            Dim VirControle As Virtualia.Net.VZoneSaisie = WebCtlZone(Index)
            If VirControle IsNot Nothing Then
                Return VirControle.ValeursCombo
            Else
                Return Nothing
            End If
        End Get
        Set(ByVal value As List(Of String))
            Dim VirControle As Virtualia.Net.VZoneSaisie = WebCtlZone(Index)
            If VirControle IsNot Nothing Then
                VirControle.ValeursCombo = value
            End If
        End Set
    End Property

    Private Sub CommandeOK_Click(sender As Object, e As EventArgs) Handles CommandeOK.Click
        Dim UtiSession As Virtualia.Net.Session.ObjetSession = WebFct.PointeurUtilisateur
        Dim LstVues As List(Of Virtualia.TablesObjet.ShemaVUE.VUE_ATTRIBUTAIRE_PFR)
        Dim Dictionnaire As List(Of Virtualia.Systeme.MetaModele.Outils.DictionnaireVirtuel) = Nothing
        Dim FicheVue As Virtualia.TablesObjet.ShemaVUE.VUE_ATTRIBUTAIRE_PFR = Nothing
        Dim LstValeurs As List(Of String)
        Dim Ide As Integer = V_Identifiant
        Dim DateValeur As String = V_Occurence
        Dim IndiceI As Integer

        If UtiSession Is Nothing Then
            Exit Sub
        End If
        If WebFct Is Nothing Then
            WebFct = New Virtualia.Net.Controles.WebFonctions(Me, 1)
        End If
        LstVues = CType(WebFct.PointeurUtilisateur.V_PointeurSessionModule, Virtualia.Net.Session.SessionModule).ListeVues_PFR(WebFct.ViRhDates.DateTypee(DateValeur).Year, V_Regime, "Alphabétique")
        If LstVues Is Nothing Then
            Exit Sub
        End If
        Try
            FicheVue = (From instance In LstVues Select instance Where instance.Ide_Dossier = Ide And instance.Date_de_Valeur = DateValeur).First
        Catch ex As Exception
            FicheVue = Nothing
        End Try
        If FicheVue Is Nothing Then
            Exit Sub
        End If
        FicheVue.FaireDicoVirtuel(V_Parametre)
        Dictionnaire = FicheVue.V_ListeDicoVirtuel
        LstValeurs = New List(Of String)
        For IndiceI = 0 To Dictionnaire.Count - 1
            If Dictionnaire.Item(IndiceI).SiNonSaisissable = False Then
                LstValeurs.Add(Donnee(IndiceI))
            End If
        Next IndiceI
        If LstValeurs.Count = 0 Then
            Exit Sub
        End If
        FicheVue.V_GrilleVirtuelle(V_Parametre) = LstValeurs

        Dim Cretour As Boolean
        Cretour = UtiSession.V_PointeurGlobal.VirServiceServeur.MiseAjour_Fiche(UtiSession.V_PointeurGlobal.VirNomUtilisateur, VI.PointdeVue.PVueVueLogique, 7,
                                                           FicheVue.Ide_Dossier, "M", FicheVue.FicheLue, FicheVue.ContenuTable)
        If Cretour = True Then
            MsgOperation.Text = "Validation effectuée"
        Else
            MsgOperation.Text = "ANOMALIE. L'enregistrement ne s'est pas effectué."
        End If
    End Sub

    Private Sub CommandeRetour_Click(sender As Object, e As EventArgs) Handles CommandeRetour.Click
        Dim Evenement As New Virtualia.Systeme.Evenements.MessageRetourEventArgs(V_Titre, 0, "", "")
        ReponseRetour(Evenement)
    End Sub
End Class