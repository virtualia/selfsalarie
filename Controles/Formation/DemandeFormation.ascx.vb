﻿Option Strict Off
Option Explicit On
Option Compare Text
Imports Virtualia.Systeme.Evenements
Imports VI = Virtualia.Systeme.Constantes
Public Class DemandeFormation
    Inherits System.Web.UI.UserControl
    Public Delegate Sub MsgDialog_MsgEventHandler(ByVal sender As Object, ByVal e As Virtualia.Systeme.Evenements.MessageSaisieEventArgs)
    Public Event MessageDialogue As MsgDialog_MsgEventHandler
    Private WebFct As Virtualia.Net.Controles.WebFonctions
    Private WsAct_CocheChange As Action(Of Boolean)

    Protected Overridable Sub V_MessageDialog(ByVal e As Virtualia.Systeme.Evenements.MessageSaisieEventArgs)
        RaiseEvent MessageDialogue(Me, e)
    End Sub

    Public WriteOnly Property Act_CocheChange As Action(Of Boolean)
        Set(value As Action(Of Boolean))
            WsAct_CocheChange = value
        End Set
    End Property

    Public Property IntituleStage As String
        Get
            Return InfoH01.DonText
        End Get
        Set(value As String)
            InfoH01.DonText = value
        End Set
    End Property
    Public Property DateDuStage As String
        Get
            Return InfoH02.DonText
        End Get
        Set(value As String)
            InfoH02.DonText = value
        End Set
    End Property
    Public Property DateDeFinStage As String
        Get
            Return InfoH07.DonText
        End Get
        Set(value As String)
            InfoH07.DonText = value
        End Set
    End Property
    Public Property Organisme As String
        Get
            Return InfoH05.DonText
        End Get
        Set(value As String)
            InfoH05.DonText = value
        End Set
    End Property
    Public Property LieuDuStage As String
        Get
            Return InfoH06.DonText
        End Get
        Set(value As String)
            InfoH06.DonText = value
        End Set
    End Property
    Public Property UrlDuStage As String
        Get
            Return LienFormation.NavigateUrl
        End Get
        Set(value As String)
            LienFormation.NavigateUrl = value
            LienFormation.Text = value
        End Set
    End Property
    Private ReadOnly Property V_WebFonction As Virtualia.Net.Controles.WebFonctions
        Get
            If WebFct Is Nothing Then
                WebFct = New Virtualia.Net.Controles.WebFonctions(Me, 0)
            End If
            Return WebFct
        End Get
    End Property

    Private Sub DemandeFormation_PreRender(sender As Object, e As EventArgs) Handles Me.PreRender
        If ListeEcheance.V_NombreItems = 0 Then
            Call FaireListEcheance
        End If
        If System.Configuration.ConfigurationManager.AppSettings("SiCocheHorsPlan") = "Non" Then
            Coche04.Visible = False
        End If
        CadreInfo.BackColor = V_WebFonction.CouleurCharte(1, "Cadre")
        CadreInfo.BorderColor = V_WebFonction.CouleurCharte(1, "Bordure")
        InfoH00.DonText = V_WebFonction.ViRhDates.DateduJour
    End Sub

    Private Sub FaireListEcheance()
        Dim Annee As Integer = Now.Year
        Dim Mois As Integer = Now.Month
        Dim IndiceI As Integer
        Dim Chaine As System.Text.StringBuilder = New System.Text.StringBuilder
        Do
            For IndiceI = Mois To 12
                Chaine.Append(Strings.Format(IndiceI, "00") & "/" & Annee & VI.Tild)
            Next IndiceI
            Mois = 1
            Annee += 1
            If Annee > Now.Year + 2 Then
                Exit Do
            End If
        Loop
        ListeEcheance.V_Liste("") = Chaine.ToString
        ListeEcheance.LstText = Strings.Format(Now.Month, "00") & "/" & Now.Year
    End Sub

    Private Sub Coche04_ValeurChange(sender As Object, e As DonneeChangeEventArgs) Handles Coche04.ValeurChange
        Select Case Coche04.V_Check
            Case True
                InfoH01.V_SiEnLectureSeule = False
                InfoH01.DonBackColor = Drawing.Color.White
                InfoH02.DonText = ""
                InfoH05.DonText = ""
                InfoH06.DonText = ""
                InfoH07.DonText = ""
                UrlDuStage = ""
            Case False
                InfoH01.V_SiEnLectureSeule = True
                InfoH01.DonBackColor = V_WebFonction.ConvertCouleur("#E2F5F1")
                InfoH02.V_SiEnLectureSeule = True
                InfoH02.DonBackColor = V_WebFonction.ConvertCouleur("#E2F5F1")
        End Select
        If WsAct_CocheChange IsNot Nothing Then
            WsAct_CocheChange(Coche04.V_Check)
        End If
    End Sub

    Private Sub CommandeOK_Click(sender As Object, e As EventArgs) Handles CommandeOK.Click
        Dim FichePer As Virtualia.TablesObjet.ShemaPER.PER_MAJINTRANET
        Dim ChaineObs As String
        Dim ChaineContenu As String = ""
        Dim Cretour As Boolean
        Dim LstErreurs As String = SiMajPossible()
        If LstErreurs <> "" Then
            Call AfficherMessage("Formation continue", LstErreurs)
            Exit Sub
        End If

        FichePer = New Virtualia.TablesObjet.ShemaPER.PER_MAJINTRANET
        FichePer.Ide_Dossier = V_WebFonction.PointeurUtilisateur.V_Identifiant
        FichePer.Rang = New_Rang
        FichePer.Date_Demande = V_WebFonction.ViRhDates.DateduJour
        FichePer.Heure_Demande = Strings.Format(Now.Hour, "00") & ":" & Strings.Format(Now.Minute, "00")
        FichePer.Nature_Gestion = 3 'En attente
        ChaineObs = V_WebFonction.ViRhFonction.ChaineVirtualiaValide(InfoV03.DonText).Replace(vbCrLf, Strings.Space(2))
        FichePer.Observations_Demandeur = ChaineObs
        FichePer.NumeroObjetPER = VI.ObjetPer.ObaDemandeFormation
        ChaineContenu = V_WebFonction.ViRhDates.DateduJour & VI.PointVirgule & InfoH01.DonText & VI.PointVirgule & InfoH02.DonText & VI.PointVirgule
        ChaineContenu &= "Virtualia.net" & Strings.StrDup(3, VI.PointVirgule)
        ChaineContenu &= "01/" & ListeEcheance.LstText & VI.PointVirgule & ChaineObs.Replace(VI.PointVirgule, VI.Virgule) & VI.PointVirgule
        If Coche04.V_Check = True Then
            ChaineContenu &= "Oui" & VI.PointVirgule & Organisme & VI.PointVirgule
        Else
            ChaineContenu &= "Non" & VI.PointVirgule & Organisme & VI.PointVirgule
        End If
        If Coche08.V_Check = True Then
            ChaineContenu &= "Oui" & VI.PointVirgule & "Non" & VI.PointVirgule
        Else
            ChaineContenu &= "Non" & VI.PointVirgule & "Non" & VI.PointVirgule
        End If
        FichePer.Contenu = ChaineContenu

        Cretour = V_WebFonction.PointeurGlobal.VirServiceServeur.MiseAjour_Fiche(V_WebFonction.PointeurGlobal.VirNomUtilisateur, VI.PointdeVue.PVueApplicatif, VI.ObjetPer.ObaSaisieIntranet,
                                                                                FichePer.Ide_Dossier, "C", "", FichePer.ContenuTable)
        If Cretour = True Then
            Call EnvoyerMail_Emanager()
            IntituleStage = ""
            DateDuStage = ""
            DateDeFinStage = ""
            Organisme = ""
            LieuDuStage = ""
            UrlDuStage = ""
            InfoV03.DonText = ""
            Call AfficherMessage("Formation continue", "La demande de formation a bien été prise en compte." & vbCrLf & "Un email a été envoyé à votre responsable hiérarchique.")
        Else
            Call AfficherMessage("Formation continue", "L'opération de mise à jour n'a pas aboutie.")
        End If
    End Sub

    Private Function SiMajPossible() As String
        Dim TableauErreur As String = ""
        If IntituleStage = "" Then
            TableauErreur &= "La saisie de l'intitulé du stage est obligatoire."
        End If
        If InfoV03.DonText = "" Then
            If TableauErreur <> "" Then
                TableauErreur &= vbCrLf
            End If
            TableauErreur &= "La saisie de la motivation de la demande de formation est obligatoire."
        End If
        Return TableauErreur
    End Function

    Private ReadOnly Property New_Rang() As Integer
        Get
            Dim CptMaxi As Integer = 0
            Dim LstFiches As List(Of Virtualia.Systeme.MetaModele.VIR_FICHE)
            Dim LstDemandes As List(Of Virtualia.TablesObjet.ShemaPER.PER_MAJINTRANET)
            LstFiches = V_WebFonction.PointeurGlobal.VirServiceServeur.LectureObjet_ToFiches(V_WebFonction.PointeurGlobal.VirNomUtilisateur,
                                                VI.PointdeVue.PVueApplicatif, VI.ObjetPer.ObaSaisieIntranet, V_WebFonction.PointeurUtilisateur.V_Identifiant, False)
            If LstFiches Is Nothing OrElse LstFiches.Count = 0 Then
                Return 1
            End If
            LstDemandes = V_WebFonction.ViRhFonction.ConvertisseurListeFiches(Of Virtualia.TablesObjet.ShemaPER.PER_MAJINTRANET)(LstFiches)
            For Each FicheRef In LstDemandes
                If FicheRef.Rang > CptMaxi Then
                    CptMaxi = FicheRef.Rang
                End If
            Next
            Return CptMaxi + 1
        End Get
    End Property

    Private Sub AfficherMessage(ByVal TitreMsg As String, ByVal Msg As String)
        Dim NatureCmd As String = "OK"
        Dim NomEmetteur As String = "DemandeFOR"

        Dim Evenement As Virtualia.Systeme.Evenements.MessageSaisieEventArgs
        Evenement = New Virtualia.Systeme.Evenements.MessageSaisieEventArgs(NomEmetteur, 0, "", NatureCmd, TitreMsg, Msg)
        Call V_MessageDialog(Evenement)
    End Sub

    Private Sub EnvoyerMail_Emanager()
        Dim DossierManager As Virtualia.Net.WebAppli.SelfV4.DossierPersonne = CType(V_WebFonction.PointeurGlobal, Virtualia.Net.Session.ObjetGlobal).Dossier(CType(V_WebFonction.PointeurUtilisateur.V_PointeurSessionModule, Virtualia.Net.Session.SessionModule).DossierPER.Ide_Manager)
        If DossierManager Is Nothing OrElse DossierManager.EMail = "" Then
            Exit Sub
        End If
        Dim Sujet As String = "Demande d’inscription à une formation"
        Dim Msg As String = "Bonjour, " & vbCrLf
        Msg &= "Pour votre information, " & CType(V_WebFonction.PointeurUtilisateur.V_PointeurSessionModule, Virtualia.Net.Session.SessionModule).DossierPER.Prenom & Strings.Space(1) & CType(V_WebFonction.PointeurUtilisateur.V_PointeurSessionModule, Virtualia.Net.Session.SessionModule).DossierPER.Nom
        Msg &= " a déposé une demande d’inscription au stage : " & IntituleStage & "." & vbCrLf
        If DateDuStage <> "" Then
            Msg &= "Cette session aura lieu du " & DateDuStage & " au " & DateDeFinStage & "." & vbCrLf
        End If
        If LieuDuStage <> "" Then
            Msg &= "A " & LieuDuStage & "." & vbCrLf
        End If
        Msg &= "Une réponse de votre part est attendue dans Virtualia.net (" & System.Configuration.ConfigurationManager.AppSettings("UrlSelfV3") & ")." & vbCrLf
        Msg &= "Ce message est généré automatiquement par l’application Virtualia.net à la suite d’une demande d’inscription à une formation."
        Call CType(V_WebFonction.PointeurGlobal, Virtualia.Net.Session.ObjetGlobal).EnvoyerEmail(Sujet, "", DossierManager.EMail, Msg)
        If DossierManager.Ide_Delegue > 0 AndAlso DossierManager.EMail_Delegue <> "" Then
            Call CType(V_WebFonction.PointeurGlobal, Virtualia.Net.Session.ObjetGlobal).EnvoyerEmail(Sujet, "", DossierManager.EMail_Delegue, Msg)
        End If
        If DossierManager.Ide_Manager = 0 OrElse DossierManager.Niveau_Manager = 1 Then
            Msg &= ""
            Exit Sub
        End If
        '** Supérieur hiérarchique direct du Manager
        DossierManager = CType(V_WebFonction.PointeurGlobal, Virtualia.Net.Session.ObjetGlobal).Dossier(DossierManager.Ide_Manager)
        If DossierManager Is Nothing Then
            Msg &= ""
            Exit Sub
        End If
        If DossierManager.Ide_Manager > 0 AndAlso DossierManager.EMail <> "" Then
            If DossierManager.EMail_Manager <> "" Then
                Call CType(V_WebFonction.PointeurGlobal, Virtualia.Net.Session.ObjetGlobal).EnvoyerEmail(Sujet, "", DossierManager.EMail, Msg)
            End If
        End If
        Msg &= ""
    End Sub

End Class