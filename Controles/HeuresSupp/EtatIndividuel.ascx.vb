﻿
Imports VI = Virtualia.Systeme.Constantes
Public Class EtatIndividuel
    Inherits Virtualia.Net.Controles.ObjetWebControle
    Private WsNumObjet As Integer = 171
    Private WsNomTable As String = "PER_JOUR_ASTREINTE"
    Private WsFiche As Virtualia.TablesObjet.ShemaPER.PER_JOUR_ASTREINTE
    Private WsAnneeSelection As Integer
    Private WsMoisSelection As Integer
    Private WsNombreAstreinte As Integer
    Private V_ListeFichesMois As List(Of String) = Nothing
    Private V_ListeFichesMoisSelection As List(Of String) = Nothing
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

    End Sub
    Private Sub FaireListeChoix()
        Dim UtiSession As Virtualia.Net.Session.SessionModule = CType(V_WebFonction.PointeurUtilisateur.V_PointeurSessionModule, Virtualia.Net.Session.SessionModule)
        If UtiSession Is Nothing Then
            Exit Sub
        End If
        Dim MontantTauxAstreinte As Double
        Dim MontantAstreinte As Double
        Dim Fiche_Etablissement As TablesObjet.ShemaPER.PER_COLLECTIVITE = Nothing
        Dim Fiche_Affectation As TablesObjet.ShemaPER.PER_AFFECTATION = Nothing
        Dim LstRes As List(Of Virtualia.Systeme.MetaModele.VIR_FICHE) = Nothing
        Dim Fiche_Annualisation As TablesObjet.ShemaPER.PER_ANNUALISATION = Nothing

        Fiche_Etablissement = UtiSession.DossierPER.VFiche_Etablissement(V_WebFonction.ViRhDates.DateduJour)
        Fiche_Affectation = UtiSession.DossierPER.VFiche_Affectation(V_WebFonction.ViRhDates.DateduJour)
        LstRes = UtiSession.DossierPER.VParent.SelectionObjetValable(V_Identifiant, V_WebFonction.ViRhDates.DateduJour, VI.ObjetPer.ObaAnnualisation)
        If LstRes IsNot Nothing Then
            Fiche_Annualisation = CType(LstRes.Item(0), Virtualia.TablesObjet.ShemaPER.PER_ANNUALISATION)
        End If

        Dim LstDecision = CType(V_WebFonction.PointeurGlobal, Virtualia.Net.Session.ObjetGlobal).ListeTablesDecision(50)
        If LstDecision IsNot Nothing Then
            For Each OutDecision As Virtualia.TablesObjet.ShemaREF.OUT_DECISION In LstDecision
                If Year(V_WebFonction.ViRhDates.DateTypee(OutDecision.ValeurInterne(0))).ToString = Year(Now).ToString Then
                    MontantTauxAstreinte = V_WebFonction.ViRhFonction.ConversionDouble(OutDecision.CodeExterne(0))
                End If
            Next
        End If
        'WsNombreAstreinte = V_ListeFiches.Count
        MontantAstreinte = MontantTauxAstreinte * WsNombreAstreinte


        Mois.Text = V_WebFonction.ViRhDates.MoisEnClair(WsMoisSelection) & Strings.Space(1) & WsAnneeSelection
        Juridiction.Text = Fiche_Etablissement.Administration
        Fonctions.Text = Fiche_Affectation.Fonction_exercee
        ServiceAstreinte.Text = Fiche_Annualisation.RegimeduContrat
        NombreAstreinte.Text = WsNombreAstreinte.ToString
        Remuneration.Text = Strings.Format(MontantAstreinte, "# ### ### ##0")
        DateEtat.Text = V_WebFonction.ViRhDates.DateduJour.ToString
        DirecteurGreffe.Text = ""
        NomEtPrenom.Text = UtiSession.DossierPER.Civilite & Strings.Space(1) & UtiSession.DossierPER.Nom & Strings.Space(1) & UtiSession.DossierPER.Prenom
    End Sub


    Private Sub ActualiserListe()
        Dim LstLibels As New List(Of String)
        LstLibels.Add("Aucune journée d'astreinte")
        LstLibels.Add("Une journée d'astreinte")
        LstLibels.Add("journées d'astreinte")
        ListeGrille.V_LibelCaption = LstLibels

        Dim LstColonnes As New List(Of String)
        LstColonnes.Add("Samedi")
        LstColonnes.Add("Dimanche")
        LstColonnes.Add("Jour férié")
        LstColonnes.Add("Date validation")
        ListeGrille.V_LibelColonne = LstColonnes

        If V_Identifiant > 0 Then
            Call InitialiserControles()
            If V_IndexFiche = -1 Then
                ListeGrille.V_Liste = Nothing
                Exit Sub
            End If

            V_ListeFichesMoisSelection = New List(Of String)
            V_ListeFichesMois = New List(Of String)

            If V_ListeFiches IsNot Nothing Then
                V_ListeFichesMois = V_ListeFiches
                For IndiceAstreinte = 0 To V_ListeFiches.Count - 1
                    Dim moisAstreinte = Strings.Right(Strings.Left(V_ListeFichesMois(IndiceAstreinte), 5), 2)
                    If CInt(moisAstreinte) = WsMoisSelection Then
                        V_ListeFichesMoisSelection.Add(V_ListeFichesMois(IndiceAstreinte))
                    End If

                Next
            End If
            ListeGrille.V_Liste = V_ListeFichesMoisSelection
            WsNombreAstreinte = V_ListeFichesMoisSelection.Count

        End If
        FaireListeChoix()
    End Sub
    Private Sub InitialiserControles()
        Dim Ctl As Control
        Dim VirControle As Controles_VCoupleEtiDonnee
        Dim IndiceI As Integer = 0
        Do
            Ctl = V_WebFonction.VirWebControle(Me.CadreInfo, "InfoH", IndiceI)
            If Ctl Is Nothing Then
                Exit Do
            End If
            VirControle = CType(Ctl, Controles_VCoupleEtiDonnee)
            VirControle.DonBackColor = Drawing.Color.White
            VirControle.DonText = ""
            IndiceI += 1
        Loop

        Dim VirVertical As Controles_VCoupleVerticalEtiDonnee
        IndiceI = 0
        Do
            Ctl = V_WebFonction.VirWebControle(Me.CadreInfo, "InfoV", IndiceI)
            If Ctl Is Nothing Then
                Exit Do
            End If
            VirVertical = CType(Ctl, Controles_VCoupleVerticalEtiDonnee)
            VirVertical.DonBackColor = Drawing.Color.White
            VirVertical.DonText = ""
            IndiceI += 1
        Loop

        Dim VirDonneeDate As VCoupleEtiDate
        IndiceI = 0
        Do
            Ctl = V_WebFonction.VirWebControle(Me.CadreInfo, "InfoD", IndiceI)
            If Ctl Is Nothing Then
                Exit Do
            End If
            VirDonneeDate = CType(Ctl, VCoupleEtiDate)
            VirDonneeDate.DonBackColor = Drawing.Color.White
            VirDonneeDate.DonText = ""
            VirDonneeDate.V_SiEnLectureSeule = Me.V_SiEnLectureSeule
            IndiceI += 1
        Loop
    End Sub

    Private Sub LireLaFiche()
        Dim CacheDonnee As List(Of String) = V_CacheMaj
        If CacheDonnee Is Nothing Then
            Exit Sub
        End If
        Dim NumInfo As Integer
        Dim SiEnLecture As Boolean = False
        Dim Ctl As Control
        Dim VirControle As Controles_VCoupleEtiDonnee
        Dim IndiceI As Integer = 0
        If CacheDonnee(5) IsNot Nothing Then
            If CacheDonnee(5).ToString <> "" And CacheDonnee(5).ToString <> V_WebFonction.ViRhDates.DateduJour Then
                SiEnLecture = True
            End If
        End If
        Dim UtiSession As Virtualia.Net.Session.SessionModule = CType(V_WebFonction.PointeurUtilisateur.V_PointeurSessionModule, Virtualia.Net.Session.SessionModule)
        If UtiSession Is Nothing Then
            SiEnLecture = True
        End If
        Do
            Ctl = V_WebFonction.VirWebControle(Me.CadreInfo, "InfoH", IndiceI)
            If Ctl Is Nothing Then
                Exit Do
            End If
            NumInfo = CInt(Strings.Right(Ctl.ID, 2))
            VirControle = CType(Ctl, Controles_VCoupleEtiDonnee)
            If CacheDonnee(NumInfo) Is Nothing Then
                VirControle.DonText = ""
            Else
                VirControle.DonText = CacheDonnee(NumInfo).ToString
            End If
            VirControle.V_SiEnLectureSeule = True
            IndiceI += 1
        Loop

        IndiceI = 0
        Dim VirDonneeVerticale As Controles_VCoupleVerticalEtiDonnee
        Do
            Ctl = V_WebFonction.VirWebControle(Me.CadreInfo, "InfoV", IndiceI)
            If Ctl Is Nothing Then
                Exit Do
            End If
            NumInfo = CInt(Strings.Right(Ctl.ID, 2))
            VirDonneeVerticale = CType(Ctl, Controles_VCoupleVerticalEtiDonnee)
            If CacheDonnee(NumInfo) Is Nothing Then
                VirDonneeVerticale.DonText = ""
            Else
                VirDonneeVerticale.DonText = CacheDonnee(NumInfo).ToString
            End If
            VirDonneeVerticale.V_SiEnLectureSeule = SiEnLecture
            VirDonneeVerticale.V_SiAutoPostBack = Not SiEnLecture
            'If SiEnLecture = False Then
            '    If UtiSession.SiAccesGRH = True Then
            '        InfoV03.V_SiEnLectureSeule = True
            '        InfoV06.V_SiEnLectureSeule = False
            '        InfoV06.V_SiAutoPostBack = True
            '    Else
            '        InfoV03.V_SiEnLectureSeule = False
            '        InfoV03.V_SiAutoPostBack = True
            '        InfoV06.V_SiEnLectureSeule = True
            '    End If
            'End If
            IndiceI += 1
        Loop

        Dim VirDonneeDate As VCoupleEtiDate
        IndiceI = 0
        Do
            Ctl = V_WebFonction.VirWebControle(Me.CadreInfo, "InfoD", IndiceI)
            If Ctl Is Nothing Then
                Exit Do
            End If
            NumInfo = CInt(Strings.Right(Ctl.ID, 2))
            VirDonneeDate = CType(Ctl, VCoupleEtiDate)
            If CacheDonnee(NumInfo) Is Nothing Then
                VirDonneeDate.DonText = ""
            Else
                VirDonneeDate.DonText = CacheDonnee(NumInfo).ToString
            End If
            If VirDonneeDate.SiDateFin = True Then
                VirDonneeDate.DateDebut = CacheDonnee(0).ToString
            End If
            VirDonneeDate.V_SiEnLectureSeule = SiEnLecture
            VirDonneeDate.V_SiAutoPostBack = Not SiEnLecture
            IndiceI += 1
        Loop

        Dim VirRadioH As Controles_VTrioHorizontalRadio
        IndiceI = 0
        Do
            Ctl = V_WebFonction.VirWebControle(Me.CadreInfo, "RadioH", IndiceI)
            If Ctl Is Nothing Then
                Exit Do
            End If
            NumInfo = CInt(Strings.Right(Ctl.ID, 2))
            VirRadioH = CType(Ctl, Controles_VTrioHorizontalRadio)
            VirRadioH.RadioGaucheCheck = False
            VirRadioH.RadioCentreCheck = False
            VirRadioH.RadioDroiteCheck = False
            If CacheDonnee(NumInfo) IsNot Nothing Then
                If CacheDonnee(NumInfo).ToString <> "" Then
                    Select Case CacheDonnee(NumInfo).ToString
                        Case Is = VirRadioH.RadioGaucheText
                            VirRadioH.RadioGaucheCheck = True
                        Case Is = VirRadioH.RadioCentreText
                            VirRadioH.RadioCentreCheck = True
                        Case Is = VirRadioH.RadioDroiteText
                            VirRadioH.RadioDroiteCheck = True
                    End Select
                End If
            End If
            VirRadioH.V_SiEnLectureSeule = SiEnLecture
            VirRadioH.V_SiAutoPostBack = Not SiEnLecture
            If SiEnLecture = False Then
                If UtiSession.SiAccesGRH = True Then
                    VirRadioH.V_SiEnLectureSeule = False
                    VirRadioH.V_SiAutoPostBack = True
                Else
                    VirRadioH.V_SiEnLectureSeule = True
                End If
            End If
            IndiceI += 1
        Loop

    End Sub
    Protected Overrides Sub Page_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init
        V_Objet(TypeListe.SiListeGrid) = WsNumObjet
        V_NomTableSgbd = WsNomTable
        V_Fiche = WsFiche
        ListeGrille.Centrage_Colonne(0) = 1
        ListeGrille.Centrage_Colonne(1) = 1
        ListeGrille.Centrage_Colonne(2) = 1
        ListeGrille.Centrage_Colonne(3) = 1
        ListeGrille.Centrage_Colonne(4) = 1
        ListeGrille.Centrage_Colonne(5) = 1
    End Sub
    Public WriteOnly Property Identifiant() As Integer
        Set(ByVal value As Integer)
            Dim SiaActualiser As Boolean = False
            If Session("Identifiant") IsNot Nothing Then
                If Session("Identifiant").ToString <> CStr(value) Then
                    Session.Remove("Identifiant")
                    Session.Add("Identifiant", CStr(value))
                    SiaActualiser = True
                End If
            Else
                Session.Add("Identifiant", CStr(value))
                SiaActualiser = True
            End If
            If V_Identifiant <> value Then

                Dim ColHisto As New List(Of Integer)
                ColHisto.Add(0)
                ColHisto.Add(1)
                ColHisto.Add(4)
                ColHisto.Add(5)
                V_CacheColHisto = ColHisto

                V_Identifiant = value
                If SiaActualiser = True Then
                    Call V_ActualiserObjet(value)
                End If
                Call ActualiserListe()
            Else
                Call V_ActualiserObjet(value)
                Call ActualiserListe()
            End If
        End Set
    End Property
    Protected Overrides Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        Call LireLaFiche()
    End Sub
    Public Property AnneeSelection As Integer
        Get
            Return WsAnneeSelection
        End Get
        Set(ByVal value As Integer)
            WsAnneeSelection = value
        End Set
    End Property
    Public Property MoisSelection As Integer
        Get
            Return WsMoisSelection
        End Get
        Set(ByVal value As Integer)
            WsMoisSelection = value
        End Set
    End Property


    Private Sub PDF_Click(sender As Object, e As EventArgs) Handles PDF.Click
        Dim Util As New Virtualia.Systeme.Outils.Utilitaires
        Dim FichierHtml As String
        Dim FluxHtml As String
        Dim NomPdf As String
        Dim FluxPDF As Byte()

        NomPdf = V_WebFonction.ViRhFonction.ChaineSansAccent(CType(V_WebFonction.PointeurUtilisateur.V_PointeurSessionModule, Virtualia.Net.Session.SessionModule).DossierPER.Nom & "_" & CType(V_WebFonction.PointeurUtilisateur.V_PointeurSessionModule, Virtualia.Net.Session.SessionModule).DossierPER.Prenom, False) & ".pdf"

        FichierHtml = V_WebFonction.PointeurGlobal.VirRepertoireTemporaire & "\" & Session.SessionID & ".html"
        Call Util.TransformeFicHtml(FichierHtml, "CadreInfo", 200)

        Try
            Dim WebServiceOutil As New Virtualia.Net.WebService.VirtualiaOutils(V_WebFonction.PointeurGlobal.UrlWebOutil)
            FluxHtml = My.Computer.FileSystem.ReadAllText(FichierHtml, System.Text.Encoding.UTF8)
            FluxPDF = WebServiceOutil.ConversionPDF(FluxHtml, True)
            WebServiceOutil.Dispose()

            If FluxPDF IsNot Nothing Then
                Dim response As System.Web.HttpResponse = System.Web.HttpContext.Current.Response
                response.Clear()
                response.AddHeader("Content-Type", "binary/octet-stream")
                response.AddHeader("Content-Disposition", "attachment; filename=" & NomPdf.Replace(Strings.Space(1), "_") & "; size=" & FluxPDF.Length.ToString())
                response.Flush()
                response.BinaryWrite(FluxPDF)
                response.Flush()
                response.End()
            End If
        Catch ex As Exception
            Debug.Write(ex.Message)
            Exit Try
        End Try
    End Sub

End Class