﻿Option Strict On
Option Explicit On
Option Compare Text
Imports VI = Virtualia.Systeme.Constantes
Public Class FrmHeuresSup
    Inherits System.Web.UI.Page
    Private WebFct As Virtualia.Net.Controles.WebFonctions
    Private WsNomState As String = "HeuresSup"

    Public Property V_Annee As Integer
        Get
            Dim VCache As ArrayList
            If Me.ViewState(WsNomState) IsNot Nothing Then
                VCache = CType(Me.ViewState(WsNomState), ArrayList)
                If IsNumeric(VCache(0)) Then
                    Return CInt(VCache(0))
                Else
                    Return Year(Now)
                End If
            End If
            Return Year(Now)
        End Get
        Set(ByVal value As Integer)
            Dim VCache As ArrayList
            If Me.ViewState(WsNomState) IsNot Nothing Then
                VCache = CType(Me.ViewState(WsNomState), ArrayList)
                VCache(0) = value
                Me.ViewState.Remove(WsNomState)
            Else
                VCache = New ArrayList
                VCache.Add(value) 'Année
                VCache.Add(Month(Now)) 'Mois
            End If
            Me.ViewState.Add(WsNomState, VCache)
        End Set
    End Property

    Private Sub FrmHeuresSup_Init(sender As Object, e As System.EventArgs) Handles Me.Init
        Dim Msg As String = ""
        Dim NumSession As String = Server.HtmlDecode(Request.QueryString("IDVirtualia"))
        If NumSession <> Session.SessionID Then
            Msg = "Erreur lors de l'identification."
            Response.Redirect("~/Fenetres/Commun/ErreurApplication.aspx?Msg=" & Msg)
            Exit Sub
        End If
        WebFct = New Virtualia.Net.Controles.WebFonctions(Me, 1)
        If WebFct.PointeurUtilisateur Is Nothing Then
            Msg = "Erreur de serveur. Vous avez été déconnecté(e)."
            Response.Redirect("~/Fenetres/Commun/ErreurApplication.aspx?Msg=" & Msg)
            Exit Sub
        End If
        If WebFct.VerifierCookie(Me, Session.SessionID) = False Then
            Msg = "Erreur lors de l'identification du dossier accédé. Session invalide."
            Response.Redirect("~/Fenetres/Commun/ErreurApplication.aspx?Msg=" & Msg)
            Exit Sub
        End If
    End Sub

    Private Sub FrmHeuresSup_PreRender(sender As Object, e As System.EventArgs) Handles Me.PreRender
        Dim UtiSession As Virtualia.Net.Session.ObjetSession = WebFct.PointeurUtilisateur
        If UtiSession Is Nothing Then
            Exit Sub
        End If

        Call FaireListeChoix()
    End Sub

    Private Sub FaireListeChoix()
        Dim UtiSession As Virtualia.Net.Session.SessionModule = CType(WebFct.PointeurUtilisateur.V_PointeurSessionModule, Virtualia.Net.Session.SessionModule)
        If UtiSession Is Nothing Then
            Exit Sub
        End If
        If DropDownAnnee.Items.Count = 0 Then
            V_Annee = Year(Now)
            DropDownAnnee.Items.Add(Year(Now).ToString)
            DropDownAnnee.Items.Add((Year(Now) - 1).ToString)
            DropDownAnnee.Items.Add((Year(Now) - 2).ToString)
        End If

        EtiIdentité.Text = UtiSession.DossierPER.Civilite & Strings.Space(1) & UtiSession.DossierPER.Nom _
            & Strings.Space(1) & UtiSession.DossierPER.Prenom
        DonTotalHS.Text = UtiSession.DossierPER.VDossier_HeuresSup.TotalCreditHeuresSup(V_Annee.ToString)
        DonSoldeAnte.Text = UtiSession.DossierPER.VDossier_HeuresSup.Solde_HS_Anterieur
        DonTotalRecup.Text = UtiSession.DossierPER.VDossier_HeuresSup.TotalRecuperations
        DonSoldeHS.Text = UtiSession.DossierPER.VDossier_HeuresSup.Solde_HS

        Dim LstHeures As List(Of Virtualia.TablesObjet.ShemaPER.PER_EVENEMENTJOUR) = UtiSession.DossierPER.VDossier_HeuresSup.ListeDesHeuresSup(V_Annee.ToString)
        Dim LstRecups As List(Of Virtualia.TablesObjet.ShemaPER.PER_ABSENCE) = UtiSession.DossierPER.VDossier_HeuresSup.ListeDesRecuperations(V_Annee.ToString)
        Dim LstChaineF As List(Of String)
        Dim Entete As List(Of String)
        Dim ChaineF As String
        Dim IndiceI As Integer
        Dim ZCalcul As Double

        Entete = New List(Of String)
        Entete.Add("Date" & VI.Tild)
        Entete.Add("Nature" & VI.Tild)
        Entete.Add("Durée" & VI.Tild)
        Entete.Add("Coeff ou Forfait" & VI.Tild)
        Entete.Add("Nombre d'heures" & VI.Tild)
        Entete.Add("CLEF")
        ListeHS.V_LibelColonne = Entete
        For IndiceI = 2 To 5
            ListeHS.Centrage_Colonne(IndiceI) = 1
        Next IndiceI
        LstChaineF = New List(Of String)
        If LstHeures IsNot Nothing Then
            For IndiceI = 0 To LstHeures.Count - 1
                ChaineF = LstHeures.Item(IndiceI).Date_de_Valeur & VI.Tild
                ChaineF &= LstHeures.Item(IndiceI).Nature_Evt & VI.Tild
                ChaineF &= LstHeures.Item(IndiceI).Duree & VI.Tild
                If LstHeures.Item(IndiceI).V_Coefficient_HS > 0 Then
                    ChaineF &= Strings.Format(LstHeures.Item(IndiceI).V_Coefficient_HS, "0.00") & VI.Tild
                    ZCalcul = WebFct.ViRhFonction.ConversionDouble(WebFct.ViRhDates.CalcHeure(LstHeures.Item(IndiceI).Duree, "", 1)) _
                        * LstHeures.Item(IndiceI).V_Coefficient_HS
                Else
                    ZCalcul = LstHeures.Item(IndiceI).V_Forfait_HS
                    ChaineF &= WebFct.ViRhDates.CalcHeure(CStr(CInt(ZCalcul)), "", 2) & VI.Tild
                End If
                ChaineF &= WebFct.ViRhDates.CalcHeure(CStr(CInt(ZCalcul)), "", 2) & VI.Tild
                ChaineF &= LstHeures.Item(IndiceI).Date_de_Valeur
                LstChaineF.Add(ChaineF)
            Next IndiceI
        End If
        ListeHS.V_Liste = LstChaineF

        Entete = New List(Of String)
        Entete.Add("Date")
        Entete.Add("Durée en jours")
        Entete.Add("Nombre d'heures")
        Entete.Add("CLEF")
        ListeRecup.V_LibelColonne = Entete
        For IndiceI = 0 To 3
            ListeRecup.Centrage_Colonne(IndiceI) = 1
        Next IndiceI
        LstChaineF = New List(Of String)
        If LstRecups IsNot Nothing Then
            For IndiceI = 0 To LstRecups.Count - 1
                ChaineF = LstRecups.Item(IndiceI).Date_de_Valeur & VI.Tild
                ChaineF &= Strings.Format(LstRecups.Item(IndiceI).JoursOuvres, "0.00") & VI.Tild
                ZCalcul = LstRecups.Item(IndiceI).JoursOuvres * UtiSession.DossierPER.VDossier_HeuresSup.HoraireJournalier(LstRecups.Item(IndiceI).Date_de_Valeur)
                ChaineF &= WebFct.ViRhDates.CalcHeure(CStr(CInt(ZCalcul)), "", 2) & VI.Tild
                ChaineF &= LstRecups.Item(IndiceI).Date_de_Valeur
                LstChaineF.Add(ChaineF)
            Next IndiceI
        End If
        ListeRecup.V_Liste = LstChaineF
    End Sub

    Private Sub DropDownAnnee_SelectedIndexChanged(sender As Object, e As System.EventArgs) Handles DropDownAnnee.SelectedIndexChanged
        V_Annee = CInt(CType(sender, System.Web.UI.WebControls.DropDownList).SelectedItem.Text)
    End Sub

End Class