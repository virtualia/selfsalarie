﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="VNotificationPS.ascx.vb" Inherits="Virtualia.Net.VNotificationPS" %>

<style type="text/css">
.EtiTitre
    {
        background-color:transparent;
        border-style:none;
        color:gray;
        font-family:'Trebuchet MS';
        font-size:medium;
        font-style:normal;
        font-weight:bold;
        margin-bottom:0px;
        margin-left:0px;
        margin-top:2px;
        text-align:left;
        text-indent:0px;
    }
     .EtiSousTitre
    {
        background-color:transparent;
        border-style:none;
        color:black;
        font-family:'Trebuchet MS';
        font-size:small;
        font-style:normal;
        font-weight:normal;
        margin-bottom:0px;
        margin-left:0px;
        margin-top:2px;
        text-align:center;
        text-indent:0px;
    }
    .EtiEntete
    {
        background-color:transparent;
        color:black;
        font-family:'Trebuchet MS';
        font-size:small;
        font-style:italic;
        font-weight:normal;
        height:20px;
        margin-bottom:0px;
        margin-left:0px;
        margin-top:0px;
        text-indent:4px;
        text-align:left;
        width:250px;
    }
    .DonEntete
    {
        background-color:transparent;
        color:black;
        font-family:'Trebuchet MS';
        font-size:small;
        font-style:normal;
        font-weight:normal;
        height:20px;
        margin-bottom:0px;
        margin-left:0px;
        margin-top:0px;
        text-indent:1px;
        text-align:left;
        width:496px;
    }
    .EtiIntituleRight
    {
        background-color:transparent;
        border-right-style:solid;
        border-right-width:1px;
        color:black;
        font-family:'Trebuchet MS';
        font-size:small;
        font-style:normal;
        font-weight:normal;
        margin-bottom:0px;
        margin-left:0px;
        margin-top:0px;
        text-indent:0px;
        text-align:center;
    }
     .EtiIntituleTop
    {
        background-color:transparent;
        border-top-style:none;
        color:black;
        font-family:'Trebuchet MS';
        font-size:small;
        font-style:normal;
        font-weight:normal;
        margin-bottom:0px;
        margin-left:0px;
        margin-top:0px;
        text-indent:0px;
        text-align:center;
    }
</style>

<asp:Table ID="PS_PAGE_NOTIF" runat="server" Width="750px">
    <asp:TableRow>
        <asp:TableCell>
            <asp:Table ID="PS_CadreInfo" runat="server" BorderStyle="None" BorderWidth="2px" Visible="true"
                BorderColor="Transparent" Width="750px" HorizontalAlign="Left" style="margin-top: 1px;">
                <asp:TableRow>
                  <asp:TableCell>
                    <asp:Table ID="PS_CadreTitreNotification" runat="server" Height="50px" CellPadding="0" Width="750px" 
                        CellSpacing="0" HorizontalAlign="Center" BorderStyle="none" BorderWidth="1px" Visible="true">
                        <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Center">
                                 <asp:Label ID="PS_Etablissement" runat="server" Text="FranceAgriMer" 
                                    Height="22px" Width="500px" CssClass="EtiTitre" style="text-indent:2px;">
                                 </asp:Label>          
                            </asp:TableCell>
                            <asp:TableCell HorizontalAlign="Center">
                                 <asp:Label ID="PS_LieuDate" runat="server" Text="Montreuil, le 16 décembre 2014" 
                                    Height="22px" Width="246px" CssClass="EtiTitre" style="margin-top:0px;text-align:right;">
                                 </asp:Label>          
                            </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow>
                            <asp:TableCell Height="12px" ColumnSpan="2" Width="746px"></asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Center" ColumnSpan="2">
                                <asp:Label ID="PS_TitreNotif" runat="server" Text="NOTIFICATION INDIVIDUELLE DES MONTANTS ALLOUES AU TITRE DE LA PRIME SPECIALE"
                                    Height="25px" Width="746px" CssClass="EtiTitre" Font-Underline="true"
                                    style="margin-top:1px;text-align:center;">
                                </asp:Label>          
                            </asp:TableCell>      
                        </asp:TableRow>
                        <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Center">
                                 <asp:Label ID="PS_EtiAnneeEntretien" runat="server" Text="Fonctionnaires - Année de référence :" 
                                    Height="22px" Width="500px" CssClass="EtiTitre" style="text-align:right;">
                                 </asp:Label>          
                            </asp:TableCell>
                            <asp:TableCell HorizontalAlign="Center">
                                 <asp:Label ID="PS_AnneeEntretien" runat="server" Text="2014" 
                                    Height="22px" Width="246px" CssClass="EtiTitre" style="text-indent:3px;">
                                 </asp:Label>          
                            </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow>
                            <asp:TableCell Height="32px" ColumnSpan="2" Width="746px"></asp:TableCell>
                        </asp:TableRow>
                    </asp:Table>
                  </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                  <asp:TableCell>
                    <asp:Table ID="PS_CadreEnteteNotif" runat="server" CellPadding="0" CellSpacing="0" Width="750px"
                        BorderStyle="Solid" BorderWidth="1px" BorderColor="Black" Visible="true" HorizontalAlign="Center">
                        <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Center" ColumnSpan="2">
                                 <asp:Label ID="PS_EtiTitreAgent" runat="server" Height="30px" Width="350px"
                                    BackColor="Transparent" BorderStyle="None" Text="Renseignements relatifs à l'agent"
                                    CssClass="EtiSousTitre" Font-Size="Medium" style="margin-top:5px;text-align:center;">
                                 </asp:Label>          
                            </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow>
                            <asp:TableCell Height="1px" ColumnSpan="2" Width="750px"
                                BorderStyle="Solid" BorderWidth="1px" BorderColor="Black" 
                                style="border-bottom-style:none;">
                            </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow>
                            <asp:TableCell Height="5px" ColumnSpan="2" Width="750px"></asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Left">
                                 <asp:Label ID="PS_EtiMatricule" runat="server" Text="Matricule :" CssClass="EtiEntete">
                                 </asp:Label>          
                            </asp:TableCell>
                            <asp:TableCell HorizontalAlign="Left">
                                 <asp:Label ID="PS_Matricule" runat="server" CssClass="DonEntete">
                                 </asp:Label>          
                            </asp:TableCell> 
                        </asp:TableRow>
                        <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Left">
                                 <asp:Label ID="PS_EtiNom" runat="server" Text="Nom :" CssClass="EtiEntete">
                                 </asp:Label>          
                            </asp:TableCell>
                            <asp:TableCell HorizontalAlign="Left">
                                 <asp:Label ID="PS_Nom" runat="server" CssClass="DonEntete">
                                 </asp:Label>          
                            </asp:TableCell> 
                        </asp:TableRow>
                        <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Left">
                                 <asp:Label ID="PS_EtiPrenom" runat="server" Text="Prénom :" CssClass="EtiEntete">
                                 </asp:Label>          
                            </asp:TableCell>
                            <asp:TableCell HorizontalAlign="Left">
                                 <asp:Label ID="PS_Prenom" runat="server" CssClass="DonEntete">
                                 </asp:Label>          
                            </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Left">
                                 <asp:Label ID="PS_EtiAffectationNiv1" runat="server" Text="Affectation :" CssClass="EtiEntete">
                                 </asp:Label>          
                            </asp:TableCell>
                            <asp:TableCell HorizontalAlign="Left">
                                 <asp:Label ID="PS_AffectationNiv1" runat="server" CssClass="DonEntete">
                                 </asp:Label>          
                            </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Left">
                                 <asp:Label ID="PS_EtiAffectationNiv2" runat="server" Text="" CssClass="EtiEntete">
                                 </asp:Label>          
                            </asp:TableCell>
                            <asp:TableCell HorizontalAlign="Left">
                                 <asp:Label ID="PS_AffectationNiv2" runat="server" CssClass="DonEntete">
                                 </asp:Label>          
                            </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Left">
                                 <asp:Label ID="PS_EtiGrade" runat="server" Text="Grade ou emploi :" CssClass="EtiEntete">
                                 </asp:Label>          
                            </asp:TableCell>
                            <asp:TableCell HorizontalAlign="Left">
                                 <asp:Label ID="PS_Grade" runat="server" CssClass="DonEntete">
                                 </asp:Label>          
                            </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Left">
                                 <asp:Label ID="PS_EtiEchelon" runat="server" Text="Echelon :" CssClass="EtiEntete">
                                 </asp:Label>          
                            </asp:TableCell>
                            <asp:TableCell HorizontalAlign="Left">
                                 <asp:Label ID="PS_Echelon" runat="server" CssClass="DonEntete">
                                 </asp:Label>          
                            </asp:TableCell>
                        </asp:TableRow> 
                    </asp:Table>
                  </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                  <asp:TableCell Height="32px" Width="746px"></asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                  <asp:TableCell>
                    <asp:Table ID="PS_CadreTableau" runat="server" Height="245px" CellPadding="0" Width="750px" 
                        CellSpacing="0" HorizontalAlign="Center" BorderStyle="Solid" BorderWidth="1px" BorderColor="Black" Visible="true">
                        <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Center">
                                 <asp:Label ID="PS_L1_C1" runat="server" Text="" 
                                    Height="110px" Width="98px" CssClass="EtiIntituleRight">
                                 </asp:Label>          
                            </asp:TableCell>
                            <asp:TableCell HorizontalAlign="Center">
                                 <asp:Label ID="PS_L1_C2" runat="server" Text="Montant du<br/>barème de<br/>référence (selon<br/>le grade,<br/>l’échelon et<br/>l’affectation)" 
                                    Height="110px" Width="120px" CssClass="EtiIntituleRight">
                                 </asp:Label>          
                            </asp:TableCell>
                            <asp:TableCell HorizontalAlign="Center">
                                 <asp:Label ID="PS_L1_C3" runat="server" Text="% de<br/>proratisation (en<br/>fonction du<br/>temps de travail<br/>et abattements<br/>maladie)" 
                                    Height="110px" Width="120px" CssClass="EtiIntituleRight">
                                 </asp:Label>          
                            </asp:TableCell>
                            <asp:TableCell HorizontalAlign="Center">
                                 <asp:Label ID="PS_L1_C4" runat="server" Text="Montant<br/>proratisé<br/>annuel<br/>(base 100)" 
                                    Height="110px" Width="100px" CssClass="EtiIntituleRight">
                                 </asp:Label>          
                            </asp:TableCell>
                            <asp:TableCell HorizontalAlign="Center">
                                 <asp:Label ID="PS_L1_C5" runat="server" Text="Taux de<br/>modulation" 
                                    Height="110px" Width="100px" CssClass="EtiIntituleRight">
                                 </asp:Label>          
                            </asp:TableCell>
                            <asp:TableCell HorizontalAlign="Center">
                                 <asp:Label ID="PS_L1_C6" runat="server" Text="Montant de la<br/>modulation" 
                                    Height="110px" Width="103px" CssClass="EtiIntituleRight">
                                 </asp:Label>          
                            </asp:TableCell>
                            <asp:TableCell HorizontalAlign="Center">
                                 <asp:Label ID="PS_L1_C7" runat="server" Text="Montant total de<br/>prime allouée" 
                                    Height="110px" Width="103px" CssClass="EtiSousTitre"
                                    style="margin-top:0px;">
                                 </asp:Label>          
                            </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow>
                            <asp:TableCell Height="1px" ColumnSpan="7" Width="750px"
                                BorderStyle="Solid" BorderWidth="1px" BorderColor="Black" 
                                style="border-bottom-style:none;">
                            </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Center">
                                 <asp:Label ID="PS_L2_C1" runat="server" Text="Régime<br/>indemnitaire" 
                                    Height="50px" Width="98px" CssClass="EtiIntituleRight">
                                 </asp:Label>          
                            </asp:TableCell>
                            <asp:TableCell HorizontalAlign="Center">
                                 <asp:Label ID="PS_Col_18" runat="server" Text="" 
                                    Height="35px" Width="120px" CssClass="EtiIntituleRight"
                                    style="padding-top:15px;">
                                 </asp:Label>          
                            </asp:TableCell>
                            <asp:TableCell HorizontalAlign="Center">
                                 <asp:Label ID="PS_Col_17" runat="server" Text="" 
                                    Height="35px" Width="120px"
                                    CssClass="EtiIntituleRight" style="padding-top:15px;">
                                 </asp:Label>          
                            </asp:TableCell>
                            <asp:TableCell HorizontalAlign="Center">
                                 <asp:Label ID="PS_Col_19" runat="server" Text="" 
                                    Height="35px" Width="100px" CssClass="EtiIntituleRight"
                                    style="padding-top: 15px;">
                                 </asp:Label>          
                            </asp:TableCell>
                            <asp:TableCell HorizontalAlign="Center">
                                 <asp:Label ID="PS_Col_20" runat="server" Text="" 
                                    Height="35px" Width="100px" CssClass="EtiIntituleRight"
                                    style="padding-top: 15px;">
                                 </asp:Label>          
                            </asp:TableCell>
                            <asp:TableCell HorizontalAlign="Center">
                                 <asp:Label ID="PS_Col_21" runat="server" Text="" 
                                    Height="35px" Width="103px" CssClass="EtiIntituleRight"
                                    style="padding-top: 15px;">
                                 </asp:Label>          
                            </asp:TableCell>
                            <asp:TableCell HorizontalAlign="Center">
                                 <asp:Label ID="PS_Col_22" runat="server" Text="" Height="35px" Width="103px"
                                    CssClass="EtiSousTitre" style="margin-top:0px;padding-top:15px">
                                 </asp:Label>          
                            </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow>
                            <asp:TableCell Height="1px" ColumnSpan="7" Width="750px"
                                BorderStyle="Solid" BorderWidth="1px" BorderColor="Black" 
                                style="border-bottom-style:none;">
                            </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Center">
                                 <asp:Label ID="PS_L3_C1" runat="server" Text="Indemnisation<br/>des sujétions<br/>du 01/11/2013<br/>au 31/10/2014" 
                                    Height="85px" Width="98px" CssClass="EtiIntituleRight" Style="background-color:white">
                                 </asp:Label>          
                            </asp:TableCell>
                            <asp:TableCell HorizontalAlign="Center" BackColor="#E9E9E9">
                                 <asp:Label ID="PS_L3_C2" runat="server" Text="" 
                                    Height="85px" Width="120px" CssClass="EtiIntituleTop">
                                 </asp:Label>          
                            </asp:TableCell>
                            <asp:TableCell HorizontalAlign="Center" BackColor="#E9E9E9">
                                 <asp:Label ID="PS_L3_C3" runat="server" Text="" 
                                    Height="85px" Width="120px" CssClass="EtiIntituleTop">
                                 </asp:Label>          
                            </asp:TableCell>
                            <asp:TableCell HorizontalAlign="Center" BackColor="#E9E9E9">
                                 <asp:Label ID="PS_L3_C4" runat="server" Text="" 
                                    Height="85px" Width="100px" CssClass="EtiIntituleTop">
                                 </asp:Label>          
                            </asp:TableCell>
                            <asp:TableCell HorizontalAlign="Center" BackColor="#E9E9E9">
                                 <asp:Label ID="PS_L3_C5" runat="server" Text="" 
                                    Height="85px" Width="100px" CssClass="EtiIntituleRight">
                                 </asp:Label>          
                            </asp:TableCell>
                            <asp:TableCell HorizontalAlign="Center">
                                 <asp:Label ID="PS_Col_22Bis" runat="server" Text="" 
                                    Height="50px" Width="103px" CssClass="EtiIntituleRight"
                                    style="background-color:white;padding-top:35px;">
                                 </asp:Label>          
                            </asp:TableCell>
                            <asp:TableCell HorizontalAlign="Center">
                                 <asp:Label ID="PS_Col_22BisBis" runat="server" Text="" 
                                    Height="50px" Width="103px" CssClass="EtiIntituleTop" style="background-color:white;padding-top:35px;">
                                 </asp:Label>          
                            </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow>
                            <asp:TableCell Height="1px" ColumnSpan="7" Width="750px"
                                BorderStyle="Solid" BorderWidth="1px" BorderColor="Black" 
                                style="border-bottom-style:none;">
                            </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Center">
                                 <asp:Label ID="PS_L4_C1" runat="server" Text="Modulation complémentaire exceptionnelle" 
                                    Height="85px" Width="98px" CssClass="EtiIntituleRight" Style="background-color:white">
                                 </asp:Label>          
                            </asp:TableCell>
                            <asp:TableCell HorizontalAlign="Center" BackColor="#E9E9E9">
                                 <asp:Label ID="PS_L4_C2" runat="server" Text="" 
                                    Height="85px" Width="120px" CssClass="EtiIntituleTop">
                                 </asp:Label>          
                            </asp:TableCell>
                            <asp:TableCell HorizontalAlign="Center" BackColor="#E9E9E9">
                                 <asp:Label ID="PS_L4_C3" runat="server" Text="" 
                                    Height="85px" Width="120px" CssClass="EtiIntituleTop">
                                 </asp:Label>          
                            </asp:TableCell>
                            <asp:TableCell HorizontalAlign="Center" BackColor="#E9E9E9">
                                 <asp:Label ID="PS_L4_C4" runat="server" Text="" 
                                    Height="85px" Width="100px" CssClass="EtiIntituleTop">
                                 </asp:Label>          
                            </asp:TableCell>
                            <asp:TableCell HorizontalAlign="Center" BackColor="#E9E9E9">
                                 <asp:Label ID="PS_L4_C5" runat="server" Text="" 
                                    Height="85px" Width="100px" CssClass="EtiIntituleTop">
                                 </asp:Label>          
                            </asp:TableCell>
                            <asp:TableCell HorizontalAlign="Center" BackColor="#E9E9E9">
                                 <asp:Label ID="PS_L4_C6" runat="server" Text="" 
                                    Height="50px" Width="103px" CssClass="EtiIntituleRight"
                                    style="padding-top:35px;">
                                 </asp:Label>          
                            </asp:TableCell>
                            <asp:TableCell HorizontalAlign="Center">
                                 <asp:Label ID="PS_Col_21Bis" runat="server" Text="" 
                                    Height="50px" Width="103px" CssClass="EtiIntituleTop" style="background-color:white;padding-top:35px;">
                                 </asp:Label>          
                            </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow>
                            <asp:TableCell Height="1px" ColumnSpan="7" Width="750px"
                                BorderStyle="Solid" BorderWidth="1px" BorderColor="Black" 
                                style="border-bottom-style:none;">
                            </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Center">
                                 <asp:Label ID="PS_L5_C1" runat="server" Text="TOTAL PS" 
                                    Height="35px" Width="98px" CssClass="EtiIntituleRight"
                                    style="background-color:white;Padding-top:15px;">
                                 </asp:Label>          
                            </asp:TableCell>
                            <asp:TableCell HorizontalAlign="Center" BackColor="#E9E9E9">
                                 <asp:Label ID="PS_L5_C2" runat="server" Text="" 
                                    Height="50px" Width="120px" CssClass="EtiIntituleTop">
                                 </asp:Label>          
                            </asp:TableCell>
                            <asp:TableCell HorizontalAlign="Center" BackColor="#E9E9E9">
                                 <asp:Label ID="PS_L5C3" runat="server" Text="" 
                                    Height="50px" Width="120px" CssClass="EtiIntituleTop">
                                 </asp:Label>          
                            </asp:TableCell>
                            <asp:TableCell HorizontalAlign="Center" BackColor="#E9E9E9">
                                 <asp:Label ID="PS_L5_C4" runat="server" Text="" 
                                    Height="50px" Width="100px" CssClass="EtiIntituleTop">
                                 </asp:Label>          
                            </asp:TableCell>
                            <asp:TableCell HorizontalAlign="Center" BackColor="#E9E9E9">
                                 <asp:Label ID="PS_L5_C5" runat="server" Text="" 
                                    Height="50px" Width="100px" CssClass="EtiIntituleTop">
                                 </asp:Label>          
                            </asp:TableCell>
                            <asp:TableCell HorizontalAlign="Center" BackColor="#E9E9E9">
                                 <asp:Label ID="PS_L5_C6" runat="server" Text="" 
                                    Height="50px" Width="103px" CssClass="EtiIntituleRight">
                                 </asp:Label>          
                            </asp:TableCell>
                            <asp:TableCell HorizontalAlign="Center">
                                 <asp:Label ID="PS_Col_22Ter" runat="server" Text="" 
                                    Height="35px" Width="103px" CssClass="EtiIntituleTop"
                                    style="background-color:white;padding-top: 15px;">
                                 </asp:Label>          
                            </asp:TableCell>
                        </asp:TableRow>
                    </asp:Table>
                  </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                  <asp:TableCell>
                    <asp:Table ID="PS_CadreBasPageNotification" runat="server" Height="50px" CellPadding="0" Width="750px" 
                        CellSpacing="0" HorizontalAlign="Center" BorderStyle="none" BorderWidth="1px" Visible="true">
                        <asp:TableRow>
                            <asp:TableCell Height="15px" Width="746px"></asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Center">
                                 <asp:Label ID="PS_BasPage" runat="server" 
				                    Text="Tout agent souhaitant contester le montant qui lui a été alloué au titre de la Prime Spéciale 
                                    doit au préalable formuler un recours hiérarchique auprès du Directeur général de l’Etablissement. 
                                    Si le désaccord persiste, il peut déposer un recours auprès du président de la CAP de son corps d'origine
                                    dans un délai de 2 mois à compter de la notification. Il adresse copie de ce recours au Service des 
                                    ressources humaines de FranceAgriMer. Le recours formulé au-delà de ce délai ne sera pas traité." 
                                    Height="80px" Width="746px" CssClass="EtiSousTitre" style="text-align:left;">
                                 </asp:Label>          
                            </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow>
                            <asp:TableCell Height="12px" Width="746px"></asp:TableCell>
                        </asp:TableRow>
                    </asp:Table>
                  </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                  <asp:TableCell>
                    <asp:Table ID="PS_CadreSignatureFAM" runat="server" Height="50px" CellPadding="0" Width="750px" 
                        CellSpacing="0" HorizontalAlign="Center" BorderWidth="1px" BorderStyle="None" Visible="true">
                        <asp:TableRow>
                            <asp:TableCell Height="5px"></asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Right">
                                <asp:Table ID="PS_SignatureFAM" runat="server" BackImageUrl="~/Images/Specifique/SignatureFAM.jpg" Width="155px"
                                    BorderStyle="None" CellPadding="0" CellSpacing="0" Visible="true" HorizontalAlign="right">
                                    <asp:TableRow>
                                        <asp:TableCell Height="93px" BackColor="Transparent" Width="152px" HorizontalAlign="right"></asp:TableCell>
                                    </asp:TableRow>
                                </asp:Table>    
                            </asp:TableCell>
                        </asp:TableRow>  
                    </asp:Table>
                  </asp:TableCell>
                </asp:TableRow>
                <asp:TableRow>
                    <asp:TableCell>
                        <div style="page-break-before:always;"></div>
                    </asp:TableCell>
                </asp:TableRow> 
              </asp:Table>
        </asp:TableCell>
    </asp:TableRow>
</asp:Table>