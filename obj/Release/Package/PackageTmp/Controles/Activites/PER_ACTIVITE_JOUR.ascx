﻿<%@ Control Language="vb" AutoEventWireup="false" CodeBehind="PER_ACTIVITE_JOUR.ascx.vb" Inherits="Virtualia.Net.PER_ACTIVITE_JOUR" %>

<%@ Register src="~/Controles/Commun/VListeGrid.ascx" tagname="VListeGrid" tagprefix="Virtualia" %>
<%@ Register src="~/Controles/Commun/VListeCombo.ascx" tagname="VListeCombo" tagprefix="Virtualia" %>
<%@ Register src="~/Controles/Saisies/VTrioHorizontalRadio.ascx" tagname="VTrioHorizontalRadio" tagprefix="Virtualia" %>
<%@ Register src="~/Controles/Activites/CtlLectureActivite.ascx" tagname="VLuActivite" tagprefix="Virtualia" %>
<%@ Register src="~/Controles/Activites/CtlSaisieActivite.ascx" tagname="VMajActivite" tagprefix="Virtualia" %>

<style type="text/css">
.EtiTotal
    {
        background-color:#B2CDC7;
        border-color:LightGray;
        border-style:none;
        border-width:1px;
        color:#124545;
        height:23px;
        font-family:'Trebuchet MS';
        font-size:11px;
        font-style:normal;
        font-weight:bold;
        padding-top: 2px;
        text-indent:0px;
        text-align:center;
        vertical-align:middle;
        width:35px;
        word-wrap:normal;
    }
</style>

<asp:Table ID="CadreInfo" runat="server" BorderStyle="None" BorderWidth="2px" Visible="true"
  BorderColor="#B0E0D7" Width="946px" HorizontalAlign="Center" style="margin-top: 3px;">
    <asp:TableRow>
        <asp:TableCell>
          <asp:Table ID="CadreTitre" runat="server" Height="30px" CellPadding="0" Width="950px" 
             CellSpacing="0" HorizontalAlign="Center" BorderColor="Black" Visible="true" BorderStyle="NotSet">
                <asp:TableRow>
                    <asp:TableCell ID="CellRetour" HorizontalAlign="Left" Visible="false">
                        <asp:ImageButton ID="CommandeRetour" runat="server" Width="32px" Height="32px" 
                            BorderStyle="None" ImageUrl="~/Images/Boutons/FlecheRetourContexte.jpg" 
                            ImageAlign="Middle" style="margin-left: 1px;">
                        </asp:ImageButton>
                    </asp:TableCell>
                    <asp:TableCell HorizontalAlign="Center">
                        <asp:Label ID="Etiquette" runat="server" Text="Activités travaillées" Height="20px" Width="550px"
                            BackColor="#216B68" BorderColor="#B0E0D7"  BorderStyle="Groove"
                            BorderWidth="2px" ForeColor="#D7FAF3"
                            Font-Bold="True" Font-Names="Trebuchet MS" Font-Size="Small"
                            style="margin-top: 5px; margin-left: 4px; margin-bottom: 4px;
                            font-style: oblique; text-indent: 5px; text-align: center;">
                        </asp:Label>          
                    </asp:TableCell>      
                </asp:TableRow>
          </asp:Table>
        </asp:TableCell>
      </asp:TableRow>
      <asp:TableRow>
          <asp:TableCell ID="CellModeSaisie" Height="55px">
              <asp:Table ID="CadreSaisieSemaine" runat="server">
                  <asp:TableRow>
                      <asp:TableCell>
                          <asp:Table ID="CadreListeSemaines" runat="server" CellPadding="0" Width="830px" CellSpacing="0" HorizontalAlign="Right" BorderColor="Black" Visible="true" BorderStyle="NotSet">
                                <asp:TableRow>
                                    <asp:TableCell BackColor="Transparent" BorderColor="LightGray" BorderStyle="None" HorizontalAlign="Center" BorderWidth="1px"> 
                                        <asp:Label ID="LabelListeSemaines" runat="server" Height="23px" Width="150px"
                                            BackColor="#CAEBE4" BorderColor="LightGray" BorderStyle="Outset" Text="Liste des semaines"
                                            BorderWidth="2px" ForeColor="#142425" Font-Italic="False"
                                            Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Smaller"
                                            style="margin-top:1px;text-indent:0px;text-align:center;padding-top:2px;">
                                        </asp:Label>          
                                    </asp:TableCell> 
                                    <asp:TableCell BackColor="White" BorderColor= "#D2D2D2" BorderStyle="None" HorizontalAlign="Left" BorderWidth="1px"> 
                                        <Virtualia:VListeCombo ID="VComboSemaines" runat="server" EtiVisible="false" V_NomTable="Semaine" SiLigneBlanche="true" 
                                            LstHeight="22px" LstWidth="300px" LstBorderColor="#7EC8BE" LstBackColor="White" LstForeColor="#142425"
                                            LstStyle="border-color:#7EC8BE;border-width:2px;border-style:inset;display:table-cell;font-style:oblique;" 
                                            EtiBackColor="#8DA8A3" EtiForeColor="#E9FDF9" />
                                    </asp:TableCell> 
                                    <asp:TableCell BackColor="White" BorderColor= "#D2D2D2" BorderStyle="None" HorizontalAlign="Center" BorderWidth="1px">
                                        <Virtualia:VTrioHorizontalRadio ID="RadioSemaine" runat="server" V_Groupe="OptionSemaine" V_SiDonneeDico="false"
                                                   RadioGaucheWidth="90px" RadioCentreWidth="90px" RadioDroiteWidth="120px" V_SiAutoPostBack="true"
                                                   RadioGaucheText="Toutes" RadioCentreText="Validées" RadioDroiteText="Non validées"
                                                   RadioGaucheHeight="22px" RadioCentreHeight="22px" RadioDroiteHeight="22px"
                                                   RadioGaucheBackColor="#CAEBE4" RadioCentreBackColor="#C2E1DA" RadioDroiteBackColor="#BAD7D1"
                                                   RadioGaucheBorderColor="#B0E0D7" RadioCentreBorderColor="#B0E0D7" RadioDroiteBorderColor="#B0E0D7"
                                                   RadioGaucheStyle="margin-left:10px;font-style:oblique;vertical-align:right;text-indent:5px;text-align:left"
                                                   RadioCentreStyle="margin-left:1px;font-style:oblique;vertical-align:right;text-indent:5px;text-align:left" 
                                                   RadioDroiteStyle="margin-left:1px;font-style:oblique;vertical-align:right;text-indent:5px;text-align:left" 
			                                       RadioGaucheCheck="true" Visible="true"/>
                                    </asp:TableCell>        
                                </asp:TableRow>
                          </asp:Table>
                      </asp:TableCell>
                  </asp:TableRow>
                  <asp:TableRow>
                      <asp:TableCell>
                          <asp:Table ID="CadrageActivite" runat="server" CellPadding="0" CellSpacing="0" Width="950px" BorderStyle="None" BorderWidth="1px" BorderColor="Black" HorizontalAlign="Left">
                            <asp:TableRow ID="LigneTotalActivite" Width="96px" Height="5px">
                                <asp:TableCell ID="TitreTotalAct" BackColor="White" BorderColor="#A8BBB8" BorderStyle="None" HorizontalAlign="Center" Height="5px"> 
                                  <asp:Label ID="LabelTotalAct" runat="server" Height="5px" Width="189px"
                                      BackColor="Transparent" BorderColor="#A8BBB8" BorderStyle="none" Text=""
                                      BorderWidth="1px" ForeColor="#124545" Font-Italic="False"
                                      Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="13px"
                                      style="margin-top:1px; margin-left:0px; margin-bottom:0px; font-style: normal; text-indent:0px; text-align:center; padding-top:2px;">
                                  </asp:Label>          
                                </asp:TableCell>
                              </asp:TableRow>
                          </asp:Table>
                      </asp:TableCell>
                  </asp:TableRow>
                  <asp:TableRow>
                      <asp:TableCell VerticalAlign="Top">
                          <asp:Table ID="CadreCommandes" runat="server" Height="22px" CellPadding="0" CellSpacing="1" HorizontalAlign="Right">
                            <asp:TableRow>
                                <asp:TableCell VerticalAlign="Bottom">
                                    <asp:Table ID="CadreCmdNew" runat="server" BackImageUrl="~/Images/Boutons/New_Std.bmp" 
                                                Width="70px" CellPadding="0" CellSpacing="0"  BorderWidth="2px" BorderStyle="Outset" BorderColor="#B0E0D7" ForeColor="#D7FAF3">
                                        <asp:TableRow>
                                            <asp:TableCell>
                                                <asp:Button ID="CommandeNew" runat="server" Text="Nouveau" Width="68px" Height="20px"
                                                        BackColor="Transparent" BorderColor="#B0E0D7" ForeColor="#D7FAF3"
                                                        Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small" Font-Italic="true"
                                                        BorderStyle="None" style=" margin-left: 10px; text-align: right;"
                                                        Tooltip="Nouvelle fiche" >
                                                    </asp:Button>
                                            </asp:TableCell>
                                        </asp:TableRow>
                                    </asp:Table>    
                                </asp:TableCell>
                                <asp:TableCell ID="CellCmdOK" VerticalAlign="Bottom" Visible="false">
                                    <asp:Table ID="CadreCmdOK" runat="server" BackImageUrl="~/Images/Boutons/OK_Std.bmp" Width="77px"
                                                BorderWidth="2px" BorderStyle="Outset" BorderColor="#B0E0D7" ForeColor="#D7FAF3"
                                                CellPadding="0" CellSpacing="0">
                                        <asp:TableRow>
                                            <asp:TableCell>
                                                <asp:Button ID="CommandeOK" runat="server" Text="Enregistrer" Width="76px" Height="20px"
                                                        BackColor="Transparent" BorderColor="#B0E0D7" ForeColor="#D7FAF3" 
                                                        Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small" Font-Italic="true"
                                                        BorderStyle="None" style=" margin-left: 6px; text-align: right;">
                                                    </asp:Button>
                                            </asp:TableCell>
                                        </asp:TableRow>
                                    </asp:Table>    
                                </asp:TableCell>
                                <asp:TableCell ID="CellCmdSigner" VerticalAlign="Bottom" Visible="false">
                                    <asp:Table ID="CadreCmdSigner" runat="server" BackImageUrl="~/Images/Boutons/Signer_Std.bmp" Width="70px"
                                                BorderWidth="2px" BorderStyle="Outset" BorderColor="#B0E0D7" ForeColor="#D7FAF3"
                                                CellPadding="0" CellSpacing="0">
                                        <asp:TableRow>
                                            <asp:TableCell>
                                                <asp:Button ID="CommandeSigner" runat="server" Text="Valider" Width="68px" Height="20px"
                                                        BackColor="Transparent" BorderColor="#B0E0D7" ForeColor="#D7FAF3" 
                                                        Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small" Font-Italic="true"
                                                        BorderStyle="None" style=" margin-left: 6px; text-align: right;">
                                                    </asp:Button>
                                            </asp:TableCell>
                                        </asp:TableRow>
                                    </asp:Table>    
                                </asp:TableCell>
                            </asp:TableRow>
                        </asp:Table>
                      </asp:TableCell>
                  </asp:TableRow>
              </asp:Table>
          </asp:TableCell>
      </asp:TableRow>
      <asp:TableRow>
          <asp:TableCell ID="CellModeControle" Height="55px" Visible="false">
              <asp:Table ID="CadreControle" runat="server">
                  <asp:TableRow>
                      <asp:TableCell>
                          <asp:Table ID="CadreListeControles" runat="server" CellPadding="0" Width="600px" CellSpacing="0" HorizontalAlign="Right" BorderColor="Black" Visible="true" BorderStyle="NotSet">
                                <asp:TableRow>
                                    <asp:TableCell BackColor="White" BorderColor= "#D2D2D2" BorderStyle="None" HorizontalAlign="Left" BorderWidth="1px"> 
                                        <Virtualia:VListeCombo ID="VComboDatesDebut" runat="server" EtiVisible="true" EtiText="Du" EtiWidth="80px"
                                            LstHeight="22px" LstWidth="200px" LstBorderColor="#7EC8BE" LstBackColor="White" LstForeColor="#142425"
                                            LstStyle="border-color:#7EC8BE;border-width:2px;border-style:inset;display:table-cell;font-style:oblique;" 
                                            EtiBackColor="#CAEBE4" EtiForeColor="#124545" />
                                    </asp:TableCell> 
                                    <asp:TableCell BackColor="White" BorderColor= "#D2D2D2" BorderStyle="None" HorizontalAlign="Left" BorderWidth="1px"> 
                                        <Virtualia:VListeCombo ID="VComboDatesFin" runat="server" EtiVisible="true" EtiText="au" EtiWidth="80px"
                                            LstHeight="22px" LstWidth="200px" LstBorderColor="#7EC8BE" LstBackColor="White" LstForeColor="#142425"
                                            LstStyle="border-color:#7EC8BE;border-width:2px;border-style:inset;display:table-cell;font-style:oblique;" 
                                            EtiBackColor="#CAEBE4" EtiForeColor="#124545" />
                                    </asp:TableCell> 
                                </asp:TableRow>
                          </asp:Table>
                      </asp:TableCell>
                  </asp:TableRow>
              </asp:Table>
          </asp:TableCell>
      </asp:TableRow>
      <asp:TableRow>
          <asp:TableCell VerticalAlign="Top" Height="20px">
              <asp:Table ID="CadreMode" runat="server" BackColor="#B0E0D7" style="margin-left:2px">
                  <asp:TableRow>
                      <asp:TableCell ID="CellCmdMode" Width="75px">
                          <asp:ImageButton ID="CommandeMode" runat="server" ImageUrl="~/Images/Boutons/ModeSynthese.bmp" ToolTip="Basculer entre le mode saisie et le mode synthèse"
                                     BorderColor="#B0E0D7" BorderStyle="Outset" BorderWidth="2px" />
                      </asp:TableCell>
                      <asp:TableCell Width="43px">
                          <asp:ImageButton ID="CommandePDF" runat="server" ImageUrl="~/Images/Boutons/PDF_off.bmp" ToolTip="Imprimer au format PDF"
                                     BorderColor="#B0E0D7" BorderStyle="Outset" BorderWidth="2px" />
                      </asp:TableCell>
                  </asp:TableRow>
              </asp:Table>
          </asp:TableCell>
      </asp:TableRow>
      <asp:TableRow>
          <asp:TableCell VerticalAlign="Top">
              <asp:Table ID="CadreEdition" runat="server">
                  <asp:TableRow>
                      <asp:TableCell ID="CellEditionActivite">
                          <asp:Table ID="CadreEditionActivite" runat="server">
                              <asp:TableRow>
                                  <asp:TableCell>
                                        <asp:Table ID="CadreEnteteActivite" runat="server" CellPadding="0" CellSpacing="0" Width="950px"
                                        BorderStyle="Notset" BorderWidth="1px" BorderColor="LightGray" HorizontalAlign="Left">
                                        <asp:TableRow ID="LigneControleJourActivite" Width="946px" Height="23px">
                                            <asp:TableCell ID="CellDossier" BackColor="#E2F5F1" BorderColor="LightGray" HorizontalAlign="Center"> 
                                                <asp:Label ID="EtiDossier" runat="server" Height="21px" Width="666px"
                                                    BackColor="#E2F5F1" BorderColor="#D2D2D2" BorderStyle="none" Text="Total &nbsp"
                                                    ForeColor="#216B68" Font-Italic="False"
                                                    Font-Bold="True" Font-Names="Trebuchet MS" Font-Size="Small"
                                                    style="margin-top:1px; margin-left:0px; margin-bottom:0px;
                                                    font-style:normal; text-indent:0px; text-align:right; padding-top:2px;">
                                                </asp:Label>            
                                            </asp:TableCell>
                                            <asp:TableCell ID="ControleLundiActivite" BackColor="#B2CDC7" BorderColor="LightGray" BorderStyle="NotSet" BorderWidth="1px"
                                                    HorizontalAlign="Center" VerticalAlign="Middle"> 
                                                <asp:Label ID="EtiTotal00" runat="server" CssClass="EtiTotal"></asp:Label>          
                                            </asp:TableCell>
                                            <asp:TableCell ID="ControleMardiActivite" BackColor="#B2CDC7" BorderColor="LightGray" BorderStyle="NotSet" BorderWidth="1px"
                                                HorizontalAlign="Center" VerticalAlign="Middle"> 
                                                <asp:Label ID="EtiTotal01" runat="server" CssClass="EtiTotal"></asp:Label>             
                                            </asp:TableCell>
                                            <asp:TableCell ID="ControleMercrediActivite" BackColor="#B2CDC7" BorderColor="LightGray" BorderStyle="NotSet" BorderWidth="1px"
                                                HorizontalAlign="Center" VerticalAlign="Middle"> 
                                                <asp:Label ID="EtiTotal03" runat="server" CssClass="EtiTotal"></asp:Label>
                                            </asp:TableCell>
                                            <asp:TableCell ID="ControleJeudiActivite" BackColor="#B2CDC7" BorderColor="LightGray" BorderStyle="NotSet" BorderWidth="1px"
                                                HorizontalAlign="Center" VerticalAlign="Middle"> 
                                                <asp:Label ID="EtiTotal04" runat="server" CssClass="EtiTotal"></asp:Label>        
                                            </asp:TableCell>
                                            <asp:TableCell ID="ControleVendrediActivite" BackColor="#B2CDC7" BorderColor="LightGray" BorderStyle="NotSet" BorderWidth="1px"
                                                HorizontalAlign="Center" VerticalAlign="Middle"> 
                                                <asp:Label ID="EtiTotal05" runat="server" CssClass="EtiTotal"></asp:Label>
                                            </asp:TableCell>
                                            <asp:TableCell ID="ControleSamediActivite" BackColor="#B2CDC7" BorderColor="LightGray" BorderStyle="NotSet" BorderWidth="1px"
                                                HorizontalAlign="Center" VerticalAlign="Middle"> 
                                                <asp:Label ID="EtiTotal06" runat="server" CssClass="EtiTotal"></asp:Label>       
                                            </asp:TableCell>
                                            <asp:TableCell ID="ControleDimancheActivite" BackColor="#B2CDC7" BorderColor="LightGray" BorderStyle="NotSet" BorderWidth="1px"
                                                HorizontalAlign="Center" VerticalAlign="Middle"> 
                                                <asp:Label ID="EtiTotal07" runat="server" CssClass="EtiTotal"></asp:Label>
                                            </asp:TableCell>
                                            <asp:TableCell ID="ControleSemaineActivite" BackColor="#B2CDC7" BorderColor="LightGray" BorderStyle="NotSet" BorderWidth="1px"
                                                HorizontalAlign="Center" VerticalAlign="Middle"> 
                                                <asp:Label ID="LabelSemaineControle" runat="server" CssClass="EtiTotal" Width="20px"></asp:Label>          
                                            </asp:TableCell>     
                                        </asp:TableRow>

                                        <asp:TableRow ID="LigneEnteteJourActivite" Width="946px" Height="23px">
                                            <asp:TableCell ID="CellTitreSemaine" BackColor="#E2F5F1" BorderColor="LightGray" HorizontalAlign="Center"> 
                                                <asp:Label ID="LabelTitreSemaine" runat="server" Height="23px" Width="666px"
                                                    BackColor="#CAEBE4" BorderColor="#D2D2D2" BorderStyle="none" Text=""
                                                    BorderWidth="1px" ForeColor="#216B68" Font-Italic="False"
                                                    Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Medium"
                                                    style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px;
                                                    font-style: normal; text-indent: 0px; text-align:  center; padding-top:2px;">
                                                </asp:Label>          
                                            </asp:TableCell>
                                            <asp:TableCell ID="CellLundiActivite" BackColor="#D2D2D2" BorderColor="LightGray" >
                                                <asp:Label ID="LabelLundiActivite" runat="server" Height="23px" Width="35px" 
                                                    BackColor="#8A9A97" BorderColor="LightGray" BorderStyle="Solid" Text="Lun"
                                                    BorderWidth="1px" ForeColor="#E2F5F1" Font-Italic="False"
                                                    Font-Bold="True" Font-Names="Trebuchet MS" Font-Size= "Smaller"
                                                    style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px; border-bottom-style:none;
                                                    font-style: normal; text-indent: 0px;  text-align:  center; padding-top: 2px;">
                                                </asp:Label>          
                                            </asp:TableCell>
                                            <asp:TableCell ID="CellMardiActivite" BackColor="#D2D2D2" BorderColor="LightGray">
                                                <asp:Label ID="LabelMardiActivite" runat="server" Height="23px" Width="35px" 
                                                    BackColor="#8A9A97" BorderColor="LightGray" BorderStyle="Solid" Text="Mar"
                                                    BorderWidth="1px" ForeColor="#E2F5F1" Font-Italic="False"
                                                    Font-Bold="True" Font-Names="Trebuchet MS" Font-Size= "Smaller"
                                                    style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px; border-bottom-style:none;
                                                    font-style: normal; text-indent: 0px;  text-align:  center; padding-top: 2px;">
                                                </asp:Label>          
                                            </asp:TableCell>
                                            <asp:TableCell ID="CellMercrediActivite" BackColor="#D2D2D2" BorderColor="LightGray">
                                                <asp:Label ID="LabelMercrediActivite" runat="server" Height="23px" Width="35px" 
                                                    BackColor="#8A9A97" BorderColor="LightGray" BorderStyle="Solid" Text="Mer"
                                                    BorderWidth="1px" ForeColor="#E2F5F1" Font-Italic="False"
                                                    Font-Bold="True" Font-Names="Trebuchet MS" Font-Size= "Smaller"
                                                    style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px; border-bottom-style:none;
                                                    font-style: normal; text-indent: 0px;  text-align:  center; padding-top: 2px;">
                                                </asp:Label>          
                                            </asp:TableCell>
                                            <asp:TableCell ID="CellJeudiActivite" BackColor="#D2D2D2" BorderColor="LightGray">
                                                <asp:Label ID="LabelJeudiActivite" runat="server" Height="23px" Width="35px" 
                                                    BackColor="#8A9A97" BorderColor="LightGray" BorderStyle="Solid" Text="Jeu"
                                                    BorderWidth="1px" ForeColor="#E2F5F1" Font-Italic="False"
                                                    Font-Bold="True" Font-Names="Trebuchet MS" Font-Size= "Smaller"
                                                    style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px; border-bottom-style:none;
                                                    font-style: normal; text-indent: 0px;  text-align:  center; padding-top: 2px;">
                                                </asp:Label>          
                                            </asp:TableCell>
                                            <asp:TableCell ID="CellVendrediActivite" BackColor="#D2D2D2" BorderColor="LightGray">
                                                <asp:Label ID="LabelVendrediActivite" runat="server" Height="23px" Width="35px" 
                                                    BackColor="#8A9A97" BorderColor="LightGray" BorderStyle="Solid" Text="Ven"
                                                    BorderWidth="1px" ForeColor="#E2F5F1" Font-Italic="False"
                                                    Font-Bold="True" Font-Names="Trebuchet MS" Font-Size= "Smaller"
                                                    style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px; border-bottom-style:none;
                                                    font-style: normal; text-indent: 0px;  text-align:  center; padding-top: 2px;">
                                                </asp:Label>          
                                            </asp:TableCell>
                                            <asp:TableCell ID="CellSamediActivite" BackColor="#D2D2D2" BorderColor="LightGray">
                                                <asp:Label ID="LabelSamediActivite" runat="server" Height="23px" Width="35px" 
                                                    BackColor="#8A9A97" BorderColor="LightGray" BorderStyle="Solid" Text="Sam"
                                                    BorderWidth="1px" ForeColor="#E2F5F1" Font-Italic="False"
                                                    Font-Bold="True" Font-Names="Trebuchet MS" Font-Size= "Smaller"
                                                    style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px; border-bottom-style:none;
                                                    font-style: normal; text-indent: 0px;  text-align:  center; padding-top:2px;">
                                                </asp:Label>          
                                            </asp:TableCell>
                                            <asp:TableCell ID="CellDimancheActivite" BackColor="#D2D2D2" BorderColor="LightGray">
                                                <asp:Label ID="LabelDimancheActivite" runat="server" Height="23px" Width="35px" 
                                                    BackColor="#8A9A97" BorderColor="LightGray" BorderStyle="Solid" Text="Dim"
                                                    BorderWidth="1px" ForeColor="#E2F5F1" Font-Italic="False"
                                                    Font-Bold="True" Font-Names="Trebuchet MS" Font-Size= "Smaller"
                                                    style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px; border-bottom-style:none;
                                                    font-style: normal; text-indent: 0px;  text-align:  center; padding-top:2px;">
                                                </asp:Label>          
                                            </asp:TableCell>
                                            <asp:TableCell ID="CellSemaineActivite" BackColor="#D2D2D2" BorderColor="LightGray">
                                                <asp:Label ID="LabelSemaineActivite" runat="server" Height="23px" Width="20px" 
                                                    BackColor="#8A9A97" BorderColor="LightGray" BorderStyle="Solid" Text=""
                                                    BorderWidth="1px" ForeColor="#E2F5F1" Font-Italic="False"
                                                    Font-Bold="True" Font-Names="Trebuchet MS" Font-Size= "Smaller"
                                                    style="margin-top: 0px; margin-left: 0px; margin-bottom: 0px; border-bottom-style:none;
                                                    font-style: normal; text-indent: 0px;  text-align:  center; padding-top:2px;">
                                                </asp:Label>          
                                            </asp:TableCell>     
                                            </asp:TableRow>
          
                                            <asp:TableRow ID="LigneEnteteDateActivite" Width="946px" Height="23px">
                                            <asp:TableCell ID="CellDateActivite" BackColor="#CAEBE4" BorderColor="LightGray" HorizontalAlign="Center"> 
                                                <asp:Label ID="LabelDateActivite" runat="server" Height="23px" Width="666px"
                                                    BackColor="#CAEBE4" BorderColor="#8A9A97" BorderStyle="None" Text=""
                                                    BorderWidth="1px" ForeColor="#216B68" Font-Italic="False"
                                                    Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Medium"
                                                    style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px;
                                                    font-style: normal; text-indent: 0px; text-align:  center; padding-top:4px;">
                                                </asp:Label>          
                                            </asp:TableCell>
                                            <asp:TableCell ID="CellDateLundiActivite" BackColor="LightGray" BorderColor="#8A9A97">
                                                <asp:Label ID="LabelDateLundiActivite" runat="server" Height="23px" Width="35px" 
                                                    BackColor="#8A9A97" BorderColor="LightGray" BorderStyle="Solid" Text=""
                                                    BorderWidth="1px" ForeColor="#E2F5F1" Font-Italic="False"
                                                    Font-Bold="True" Font-Names="Trebuchet MS" Font-Size="11px"
                                                    style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px;
                                                    font-style: normal; text-indent: 0px;  text-align:  center; padding-top: 4px;">
                                                </asp:Label>          
                                            </asp:TableCell>
                                                <asp:TableCell ID="CellDateMardiActivite" BackColor="LightGray" BorderColor="#8A9A97">
                                                <asp:Label ID="LabelDateMardiActivite" runat="server" Height="23px" Width="35px" 
                                                    BackColor="#8A9A97" BorderColor="LightGray" BorderStyle="Solid" Text=""
                                                    BorderWidth="1px" ForeColor="#E2F5F1" Font-Italic="False"
                                                    Font-Bold="True" Font-Names="Trebuchet MS" Font-Size="11px"
                                                    style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px;
                                                    font-style: normal; text-indent: 0px;  text-align:  center; padding-top: 4px;">
                                                </asp:Label>          
                                            </asp:TableCell>
                                            <asp:TableCell ID="CellDateMercrediActivite" BackColor="LightGray" BorderColor="#8A9A97">
                                                <asp:Label ID="LabelDateMercrediActivite" runat="server" Height="23px" Width="35px" 
                                                    BackColor="#8A9A97" BorderColor="LightGray" BorderStyle="Solid" Text=""
                                                    BorderWidth="1px" ForeColor="#E2F5F1" Font-Italic="False"
                                                    Font-Bold="True" Font-Names="Trebuchet MS" Font-Size="11px"
                                                    style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px;
                                                    font-style: normal; text-indent: 0px;  text-align:  center; padding-top: 4px;">
                                                </asp:Label>          
                                            </asp:TableCell>
                                            <asp:TableCell ID="CellDateJeudiActivite" BackColor="LightGray" BorderColor="#8A9A97">
                                                <asp:Label ID="LabelDateJeudiActivite" runat="server" Height="23px" Width="35px" 
                                                    BackColor="#8A9A97" BorderColor="LightGray" BorderStyle="Solid" Text=""
                                                    BorderWidth="1px" ForeColor="#E2F5F1" Font-Italic="False"
                                                    Font-Bold="True" Font-Names="Trebuchet MS" Font-Size="11px"
                                                    style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px;
                                                    font-style: normal; text-indent: 0px;  text-align:  center; padding-top: 4px;">
                                                </asp:Label>          
                                            </asp:TableCell>
                                            <asp:TableCell ID="CellDateVendrediActivite" BackColor="LightGray" BorderColor="#8A9A97">
                                                <asp:Label ID="LabelDateVendrediActivite" runat="server" Height="23px" Width="35px" 
                                                    BackColor="#8A9A97" BorderColor="LightGray" BorderStyle="Solid" Text=""
                                                    BorderWidth="1px" ForeColor="#E2F5F1" Font-Italic="False"
                                                    Font-Bold="True" Font-Names="Trebuchet MS" Font-Size="11px"
                                                    style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px;
                                                    font-style: normal; text-indent: 0px;  text-align:  center; padding-top: 4px;">
                                                </asp:Label>          
                                            </asp:TableCell>
                                            <asp:TableCell ID="CellDateSamediActivite" BackColor="LightGray" BorderColor="#8A9A97">
                                                <asp:Label ID="LabelDateSamediActivite" runat="server" Height="23px" Width="35px" 
                                                    BackColor="#8A9A97" BorderColor="LightGray" BorderStyle="Solid" Text=""
                                                    BorderWidth="1px" ForeColor="#E2F5F1" Font-Italic="False"
                                                    Font-Bold="True" Font-Names="Trebuchet MS" Font-Size="11px"
                                                    style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px;
                                                    font-style: normal; text-indent: 0px;  text-align:  center; padding-top: 4px;">
                                                </asp:Label>          
                                            </asp:TableCell>
                                            <asp:TableCell ID="CellDateDimancheActivite" BackColor="LightGray" BorderColor="#8A9A97">
                                                <asp:Label ID="LabelDateDimancheActivite" runat="server" Height="23px" Width="35px" 
                                                    BackColor="#8A9A97" BorderColor="LightGray" BorderStyle="Solid" Text=""
                                                    BorderWidth="1px" ForeColor="#E2F5F1" Font-Italic="False"
                                                    Font-Bold="True" Font-Names="Trebuchet MS" Font-Size="11px"
                                                    style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px;
                                                    font-style: normal; text-indent: 0px;  text-align:  center; padding-top: 4px;">
                                                </asp:Label>          
                                            </asp:TableCell>
                                            <asp:TableCell ID="CellDateSemaineActivite" BackColor="LightGray" BorderColor="#8A9A97">
                                                <asp:Label ID="LabelDateSemaineActivite" runat="server" Height="23px" Width="20px" 
                                                    BackColor="#8A9A97" BorderColor="LightGray" BorderStyle="Solid" Text=""
                                                    BorderWidth="1px" ForeColor="#E2F5F1" Font-Italic="False"
                                                    Font-Bold="True" Font-Names="Trebuchet MS" Font-Size="11px"
                                                    style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px;
                                                    font-style: normal; text-indent: 0px;  text-align:  center; padding-top: 4px;">
                                                </asp:Label>          
                                            </asp:TableCell>  
                                            </asp:TableRow>

                                            <asp:TableRow ID="LigneEnteteDatePlanningAM" Width="946px" Height="11px">
                                            <asp:TableCell ID="CellPlanningAM" BackColor="#E2F5F1" BorderColor="LightGray" HorizontalAlign="Center"> 
                                                <asp:Label ID="LabelPlanningAM" runat="server" Height="11px" Width="666px"
                                                    BackColor="#E2F5F1" BorderColor="#D2D2D2" BorderStyle="None" Text=""
                                                    BorderWidth="1px" ForeColor="#216B68" Font-Italic="False"
                                                    Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Medium"
                                                    style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px;
                                                    font-style: normal; text-indent: 0px; text-align:  center; padding-top: 4px;">
                                                </asp:Label>          
                                            </asp:TableCell>
                                            <asp:TableCell ID="CellLundiAM" BackColor="#D2D2D2" BorderColor="LightGray" >
                                                <asp:Label ID="CalAM00" runat="server" Height="11px" Width="35px" 
                                                    BackColor="#F0F0F0" BorderColor="LightGray" BorderStyle="Solid" Text=""
                                                    BorderWidth="1px" ForeColor="#E2F5F1" Font-Italic="False"
                                                    Font-Bold="True" Font-Names="Trebuchet MS" Font-Size="11px"
                                                    style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px;
                                                    font-style: normal; text-indent: 0px;  text-align:  center; padding-top: 4px;">
                                                </asp:Label>          
                                            </asp:TableCell>
                                                <asp:TableCell ID="CellMardiAM" BackColor="#D2D2D2" BorderColor="LightGray" >
                                                <asp:Label ID="CalAM01" runat="server" Height="11px" Width="35px" 
                                                    BackColor="#F0F0F0" BorderColor="LightGray" BorderStyle="Solid" Text=""
                                                    BorderWidth="1px" ForeColor="#E2F5F1" Font-Italic="False"
                                                    Font-Bold="True" Font-Names="Trebuchet MS" Font-Size="11px"
                                                    style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px;
                                                    font-style: normal; text-indent: 0px;  text-align:  center; padding-top: 4px;">
                                                </asp:Label>          
                                            </asp:TableCell>
                                            <asp:TableCell ID="CellMercrediAM" BackColor="#D2D2D2" BorderColor="LightGray" >
                                                <asp:Label ID="CalAM02" runat="server" Height="11px" Width="35px" 
                                                    BackColor="#F0F0F0" BorderColor="LightGray" BorderStyle="Solid" Text=""
                                                    BorderWidth="1px" ForeColor="#E2F5F1" Font-Italic="False"
                                                    Font-Bold="True" Font-Names="Trebuchet MS" Font-Size="11px"
                                                    style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px;
                                                    font-style: normal; text-indent: 0px;  text-align:  center; padding-top: 4px;">
                                                </asp:Label>          
                                            </asp:TableCell>
                                            <asp:TableCell ID="CellJeudiAM" BackColor="#D2D2D2" BorderColor="LightGray" >
                                                <asp:Label ID="CalAM03" runat="server" Height="11px" Width="35px" 
                                                    BackColor="#B0E0D7" BorderColor="LightGray" BorderStyle="Solid" Text=""
                                                    BorderWidth="1px" ForeColor="#E2F5F1" Font-Italic="False"
                                                    Font-Bold="True" Font-Names="Trebuchet MS" Font-Size="11px"
                                                    style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px;
                                                    font-style: normal; text-indent: 0px;  text-align:  center; padding-top: 4px;">
                                                </asp:Label>          
                                            </asp:TableCell>
                                            <asp:TableCell ID="CellVendrediAM" BackColor="#D2D2D2" BorderColor="LightGray" >
                                                <asp:Label ID="CalAM04" runat="server" Height="11px" Width="35px" 
                                                    BackColor="#B0E0D7" BorderColor="LightGray" BorderStyle="Solid" Text=""
                                                    BorderWidth="1px" ForeColor="#E2F5F1" Font-Italic="False"
                                                    Font-Bold="True" Font-Names="Trebuchet MS" Font-Size="11px"
                                                    style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px;
                                                    font-style: normal; text-indent: 0px;  text-align:  center; padding-top: 4px;">
                                                </asp:Label>          
                                            </asp:TableCell>
                                            <asp:TableCell ID="CellSamediAM" BackColor="#D2D2D2" BorderColor="LightGray" >
                                                <asp:Label ID="CalAM05" runat="server" Height="11px" Width="35px" 
                                                    BackColor="Gray" BorderColor="LightGray" BorderStyle="Solid" Text=""
                                                    BorderWidth="1px" ForeColor="#E2F5F1" Font-Italic="False"
                                                    Font-Bold="True" Font-Names="Trebuchet MS" Font-Size="11px"
                                                    style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px;
                                                    font-style: normal; text-indent: 0px;  text-align:  center; padding-top: 4px;">
                                                </asp:Label>          
                                            </asp:TableCell>
                                            <asp:TableCell ID="CellDimancheAM" BackColor="#D2D2D2" BorderColor="LightGray" >
                                                <asp:Label ID="CalAM06" runat="server" Height="11px" Width="35px" 
                                                    BackColor="Gray" BorderColor="LightGray" BorderStyle="Solid" Text=""
                                                    BorderWidth="1px" ForeColor="#E2F5F1" Font-Italic="False"
                                                    Font-Bold="True" Font-Names="Trebuchet MS" Font-Size="11px"
                                                    style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px;
                                                    font-style: normal; text-indent: 0px;  text-align:  center; padding-top: 4px;">
                                                </asp:Label>          
                                            </asp:TableCell>
                                            <asp:TableCell ID="CellDateSemainePlanningAM" BackColor="#D2D2D2" BorderColor="LightGray" >
                                                <asp:Label ID="LabelDateSemainePlanningAM" runat="server" Height="11px" Width="20px" 
                                                    BackColor="#8A9A97" BorderColor="LightGray" BorderStyle="Solid" Text=""
                                                    BorderWidth="1px" ForeColor="#E2F5F1" Font-Italic="False"
                                                    Font-Bold="True" Font-Names="Trebuchet MS" Font-Size="11px"
                                                    style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px;
                                                    font-style: normal; text-indent: 0px;  text-align:  center; padding-top: 4px;">
                                                </asp:Label>          
                                            </asp:TableCell>  
                                            </asp:TableRow>

                                            <asp:TableRow ID="LigneEnteteDatePlanningPM" Width="946px" Height="11px">
                                            <asp:TableCell ID="CellPlanningPM" BackColor="#E2F5F1" BorderColor="LightGray" BorderStyle="None" HorizontalAlign="Center" BorderWidth="1px"> 
                                                <asp:Label ID="LabelPlanningPM" runat="server" Height="11px" Width="661px"
                                                    BackColor="#E2F5F1" BorderColor="#D2D2D2" BorderStyle="None" Text=""
                                                    BorderWidth="1px" ForeColor="#216B68" Font-Italic="true"
                                                    Font-Bold="False" Font-Names="Trebuchet MS" Font-Size="Small"
                                                    style="vertical-align:middle;text-indent:0px; text-align:right; margin-right:5px; padding-bottom:2px">
                                                </asp:Label>          
                                            </asp:TableCell>
                                            <asp:TableCell ID="CellLundiPM" BackColor="#D2D2D2" BorderColor="LightGray" >
                                                <asp:Label ID="CalPM00" runat="server" Height="11px" Width="35px" 
                                                    BackColor="#F0F0F0" BorderColor="LightGray" BorderStyle="Solid" Text=""
                                                    BorderWidth="1px" ForeColor="#E2F5F1" Font-Italic="False"
                                                    Font-Bold="True" Font-Names="Trebuchet MS" Font-Size="11px"
                                                    style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px;
                                                    font-style: normal; text-indent: 0px;  text-align:  center; padding-top: 4px;">
                                                </asp:Label>          
                                            </asp:TableCell>
                                                <asp:TableCell ID="CellMardiPM" BackColor="#D2D2D2" BorderColor="LightGray" >
                                                <asp:Label ID="CalPM01" runat="server" Height="11px" Width="35px" 
                                                    BackColor="#F0F0F0" BorderColor="LightGray" BorderStyle="Solid" Text=""
                                                    BorderWidth="1px" ForeColor="#E2F5F1" Font-Italic="False"
                                                    Font-Bold="True" Font-Names="Trebuchet MS" Font-Size="11px"
                                                    style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px;
                                                    font-style: normal; text-indent: 0px;  text-align:  center; padding-top: 4px;">
                                                </asp:Label>          
                                            </asp:TableCell>
                                            <asp:TableCell ID="CellMercrediPM" BackColor="#D2D2D2" BorderColor="LightGray" >
                                                <asp:Label ID="CalPM02" runat="server" Height="11px" Width="35px" 
                                                    BackColor="#B0E0D7" BorderColor="LightGray" BorderStyle="Solid" Text=""
                                                    BorderWidth="1px" ForeColor="#E2F5F1" Font-Italic="False"
                                                    Font-Bold="True" Font-Names="Trebuchet MS" Font-Size="11px"
                                                    style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px;
                                                    font-style: normal; text-indent: 0px;  text-align:  center; padding-top: 4px;">
                                                </asp:Label>          
                                            </asp:TableCell>
                                            <asp:TableCell ID="CellJeudiPM" BackColor="#D2D2D2" BorderColor="LightGray" >
                                                <asp:Label ID="CalPM03" runat="server" Height="11px" Width="35px" 
                                                    BackColor="#B0E0D7" BorderColor="LightGray" BorderStyle="Solid" Text=""
                                                    BorderWidth="1px" ForeColor="#E2F5F1" Font-Italic="False"
                                                    Font-Bold="True" Font-Names="Trebuchet MS" Font-Size="11px"
                                                    style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px;
                                                    font-style: normal; text-indent: 0px;  text-align:  center; padding-top: 4px;">
                                                </asp:Label>          
                                            </asp:TableCell>
                                            <asp:TableCell ID="CellVendrediPM" BackColor="#D2D2D2" BorderColor="LightGray" >
                                                <asp:Label ID="CalPM04" runat="server" Height="11px" Width="35px" 
                                                    BackColor="#B0E0D7" BorderColor="LightGray" BorderStyle="Solid" Text=""
                                                    BorderWidth="1px" ForeColor="#E2F5F1" Font-Italic="False"
                                                    Font-Bold="True" Font-Names="Trebuchet MS" Font-Size="11px"
                                                    style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px;
                                                    font-style: normal; text-indent: 0px;  text-align:  center; padding-top: 4px;">
                                                </asp:Label>          
                                            </asp:TableCell>
                                            <asp:TableCell ID="CellSamediPM" BackColor="#D2D2D2" BorderColor="LightGray" >
                                                <asp:Label ID="CalPM05" runat="server" Height="11px" Width="35px" 
                                                    BackColor="Gray" BorderColor="LightGray" BorderStyle="Solid" Text=""
                                                    BorderWidth="1px" ForeColor="#D7FAF3" Font-Italic="False"
                                                    Font-Bold="True" Font-Names="Trebuchet MS" Font-Size="11px"
                                                    style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px;
                                                    font-style: normal; text-indent: 0px;  text-align:  center; padding-top: 4px;">
                                                </asp:Label>          
                                            </asp:TableCell>
                                            <asp:TableCell ID="CellDimanchePM" BackColor="#D2D2D2" BorderColor="LightGray" >
                                                <asp:Label ID="CalPM06" runat="server" Height="11px" Width="35px" 
                                                    BackColor="Gray" BorderColor="LightGray" BorderStyle="Solid" Text=""
                                                    BorderWidth="1px" ForeColor="#E2F5F1" Font-Italic="False"
                                                    Font-Bold="True" Font-Names="Trebuchet MS" Font-Size="11px"
                                                    style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px;
                                                    font-style: normal; text-indent: 0px;  text-align:  center; padding-top: 4px;">
                                                </asp:Label>          
                                            </asp:TableCell>
                                            <asp:TableCell ID="CellDateSemainePlanningPM" BackColor="#D2D2D2" BorderColor="LightGray" >
                                                <asp:Label ID="LabelDateSemainePlanningPM" runat="server" Height="11px" Width="20px" 
                                                    BackColor="#8A9A97" BorderColor="LightGray" BorderStyle="Solid" Text=""
                                                    BorderWidth="1px" ForeColor="#E2F5F1" Font-Italic="False"
                                                    Font-Bold="True" Font-Names="Trebuchet MS" Font-Size="11px"
                                                    style="margin-top: 1px; margin-left: 0px; margin-bottom: 0px;
                                                    font-style: normal; text-indent: 0px;  text-align:  center; padding-top: 4px;">
                                                </asp:Label>          
                                            </asp:TableCell>  
                                            </asp:TableRow>
                                        </asp:Table>
                                    </asp:TableCell>
                              </asp:TableRow>
                              <asp:TableRow>
                                  <asp:TableCell ID="CellSaisie" Visible="false">
                                        <Virtualia:VMajActivite ID="CadreSaisie" runat="server"></Virtualia:VMajActivite>
                                  </asp:TableCell>
                              </asp:TableRow>
                              <asp:TableRow>
                                  <asp:TableCell Height="5px"></asp:TableCell>
                              </asp:TableRow>
                              <asp:TableRow>
                                   <asp:TableCell ID="CellLecture" Visible="false">
                                        <Virtualia:VLuActivite ID="CadreLecture" runat="server" NombreElements="20"></Virtualia:VLuActivite>
                                    </asp:TableCell>
                              </asp:TableRow>
                          </asp:Table>
                      </asp:TableCell>
                  </asp:TableRow>
                  <asp:TableRow>
                      <asp:TableCell ID="CellEditionControle" Visible="false">
                          <Virtualia:VListeGrid ID="ListeStatutSemaine" runat="server" CadreWidth="650px" SiColonneSelect="false" SiCaseAcocher="false" />
                          <Virtualia:VListeGrid ID="ListeRecap" runat="server" CadreWidth="650px" SiColonneSelect="false" SiCaseAcocher="false" />
                      </asp:TableCell>
                  </asp:TableRow>
              </asp:Table>
          </asp:TableCell>
      </asp:TableRow>
 </asp:Table>
