﻿Option Strict On
Option Explicit On
Option Compare Text
Imports Virtualia.Systeme.Evenements
Imports VI = Virtualia.Systeme.Constantes
Public Class CtlPerHeureSupMensuel
    Inherits System.Web.UI.UserControl
    Public Delegate Sub Click_MsgEventHandler(ByVal sender As Object, ByVal e As Virtualia.Systeme.Evenements.DonneeChangeEventArgs)
    Public Event DemandePaieHS As Click_MsgEventHandler
    Private WebFct As Virtualia.Net.Controles.WebFonctions
    Private WsDossierHS As Virtualia.Net.WebAppli.SelfV4.DossierNewHeureSup

    Protected Overridable Sub ReponseDemande(ByVal e As Virtualia.Systeme.Evenements.DonneeChangeEventArgs)
        RaiseEvent DemandePaieHS(Me, e)
    End Sub

    Public Property V_DateValeur As String
        Get
            Return EtiDateValeur.ToolTip
        End Get
        Set(value As String)
            EtiDateValeur.ToolTip = value
            EtiDateValeur.Text = "Situation des heures supplémentaires - " & Strings.Format(V_WebFonction.ViRhDates.MoisEnClair(CDate(value).Month)) & Strings.Space(1) & CDate(value).Year
            Call LireLaFiche()
        End Set
    End Property
    Private ReadOnly Property V_WebFonction() As Virtualia.Net.Controles.WebFonctions
        Get
            If WebFct Is Nothing Then
                WebFct = New Virtualia.Net.Controles.WebFonctions(Me, 0)
            End If
            Return WebFct
        End Get
    End Property

    Private ReadOnly Property V_UtiSession As Virtualia.Net.Session.SessionModule
        Get
            Return CType(V_WebFonction.PointeurUtilisateur.V_PointeurSessionModule, Virtualia.Net.Session.SessionModule)
        End Get
    End Property

    Private Sub LireLaFiche()
        Dim NumObjet As Integer
        Dim NumInfo As Integer
        Dim Ctl As Control
        Dim IndiceI As Integer = 0
        Dim SiLectureOnly As Boolean = True
        Dim FichePER As Virtualia.TablesObjet.ShemaPER.PER_MENSUEL_HS
        Dim Zcalc As Double

        Dim VirControle As Controles_VCoupleEtiDonnee
        Do
            Ctl = V_WebFonction.VirWebControle(Me.CadreInfo, "InfoH", IndiceI)
            If Ctl Is Nothing Then
                Exit Do
            End If
            NumInfo = CInt(Strings.Right(Ctl.ID, 2))
            VirControle = CType(Ctl, Controles_VCoupleEtiDonnee)
            NumObjet = VirControle.V_Objet
            If VirControle.V_SiDonneeDico = True Then
                Select Case NumInfo
                    Case 1 To 8
                        VirControle.DonText = ConvertCentiemeEnHeureMinute(V_WebFonction.ViRhFonction.ConversionDouble(ValeurLue(NumInfo)))
                        VirControle.DonTooltip = ValeurLue(NumInfo)
                    Case 19 To 25
                        VirControle.DonText = ConvertCentiemeEnHeureMinute(V_WebFonction.ViRhFonction.ConversionDouble(ValeurLue(NumInfo)))
                        VirControle.DonTooltip = ValeurLue(NumInfo)
                    Case Else
                        VirControle.DonText = ValeurLue(NumInfo)
                        VirControle.DonTooltip = ""
                End Select
            Else
                VirControle.DonText = ""
                VirControle.DonTooltip = ""
            End If
            VirControle.V_SiEnLectureSeule = SiLectureOnly
            VirControle.V_SiAutoPostBack = False
            IndiceI += 1
        Loop

        InfoJour14.DonText = ""
        InfoJour25.DonText = ""
        InfoNuit14.DonText = ""
        InfoNuit25.DonText = ""
        InfoJF14.DonText = ""
        InfoJF25.DonText = ""
        InfoJour14.DonTooltip = ""
        InfoJour25.DonTooltip = ""
        InfoNuit14.DonTooltip = ""
        InfoNuit25.DonTooltip = ""
        InfoJF14.DonTooltip = ""
        InfoJF25.DonTooltip = ""

        InfoOuv2.DonText = ""
        InfoOuv3.DonText = ""
        InfoJFT1.DonText = ""
        InfoJFT2.DonText = ""
        InfoJFT3.DonText = ""
        InfoOuv2.DonTooltip = ""
        InfoOuv3.DonTooltip = ""
        InfoJFT1.DonTooltip = ""
        InfoJFT2.DonTooltip = ""
        InfoJFT3.DonTooltip = ""

        InfoJour40.DonText = ""
        InfoJour41.DonText = ""
        InfoNuit40.DonText = ""
        InfoNuit41.DonText = ""
        InfoJF40.DonText = ""
        InfoJF41.DonText = ""
        InfoJour40.DonTooltip = ""
        InfoJour41.DonTooltip = ""
        InfoNuit40.DonTooltip = ""
        InfoNuit41.DonTooltip = ""
        InfoJF40.DonTooltip = ""
        InfoJF41.DonTooltip = ""

        FichePER = FicheMensuel
        '*** Commande demande Visible ou pas **********
        If FichePER Is Nothing Then
            CmdDemande.Visible = False
            Exit Sub
        End If
        Select Case FichePER.SiExisteHeuresRemunerees
            Case True
                CmdDemande.Visible = False
            Case False
                If V_UtiSession.SiAccesGRH = True Then
                    If InfoH09.DonText <> "" Then
                        CmdDemande.Visible = True
                        CmdDemande.Text = "Viser la demande"
                        CmdDemande.ToolTip = "Viser la demande de rémunération des heures supplémentaires"
                    Else
                        CmdDemande.Visible = False
                    End If
                ElseIf V_UtiSession.SiAccesManager = True Then
                    If Session.Item("Manager") IsNot Nothing And InfoH09.DonText <> "" Then
                        CmdDemande.Visible = True
                        CmdDemande.Text = "Valider la demande"
                        CmdDemande.ToolTip = "Valider la demande de rémunération des heures supplémentaires"
                    Else
                        CmdDemande.Visible = False
                    End If
                Else
                    CmdDemande.Text = "Effectuer une demande"
                    CmdDemande.ToolTip = "Effectuer une demande de rémunération des heures supplémentaires"
                    CmdDemande.Visible = V_UtiSession.VPointeurGlobal.SiMaxiDemandeHS(FichePER.Date_Valeur_ToDate)
                End If
        End Select
        '************************************
        InfoJour14.DonText = ConvertCentiemeEnHeureMinute(FichePER.TotalHeures_Jour_T1)
        InfoJour14.DonTooltip = CStr(FichePER.TotalHeures_Jour_T1)
        InfoJour25.DonText = ConvertCentiemeEnHeureMinute(FichePER.TotalHeures_Jour_T2)
        InfoJour25.DonTooltip = CStr(FichePER.TotalHeures_Jour_T2)
        InfoNuit14.DonText = ConvertCentiemeEnHeureMinute(FichePER.TotalHeures_Nuit_T1)
        InfoNuit14.DonTooltip = CStr(FichePER.TotalHeures_Nuit_T1)
        InfoNuit25.DonText = ConvertCentiemeEnHeureMinute(FichePER.TotalHeures_Nuit_T2)
        InfoNuit25.DonTooltip = CStr(FichePER.TotalHeures_Nuit_T2)
        InfoJF14.DonText = ConvertCentiemeEnHeureMinute(FichePER.TotalHeures_JF_T1)
        InfoJF14.DonTooltip = CStr(FichePER.TotalHeures_JF_T1)
        InfoJF25.DonText = ConvertCentiemeEnHeureMinute(FichePER.TotalHeures_JF_T2)
        InfoJF25.DonTooltip = CStr(FichePER.TotalHeures_JF_T2)

        '** Détail des heures de jours ouvrables
        InfoOuv2.DonText = ConvertCentiemeEnHeureMinute(FichePER.TotalHeures_Jour_de20a22h * 1.5)
        InfoOuv2.DonTooltip = CStr(FichePER.TotalHeures_Jour_de20a22h * 1.5)
        InfoOuv3.DonText = ConvertCentiemeEnHeureMinute(FichePER.TotalHeures_Nuit * 2)
        InfoOuv3.DonTooltip = CStr(FichePER.TotalHeures_Nuit * 2)
        '** Détail des heures de dimanches et jours fériés
        InfoJFT1.DonText = ConvertCentiemeEnHeureMinute(FichePER.TotalHeures_Astreinte_Jusqua20h * 1.25)
        InfoJFT1.DonTooltip = CStr(FichePER.TotalHeures_Astreinte_Jusqua20h * 1.25)
        InfoJFT2.DonText = ConvertCentiemeEnHeureMinute(FichePER.TotalHeures_Astreinte_de20a22h * 1.5)
        InfoJFT2.DonTooltip = CStr(FichePER.TotalHeures_Astreinte_de20a22h * 1.5)
        InfoJFT3.DonText = ConvertCentiemeEnHeureMinute(FichePER.TotalHeures_Astreinte_Nuit * 2)
        InfoJFT3.DonTooltip = CStr(FichePER.TotalHeures_Astreinte_Nuit * 2)
        '** Heures compensables
        Zcalc = FichePER.NombreHeuresCompensables_Jour(True)
        If Zcalc <= 25 Then
            InfoJour40.DonText = ConvertCentiemeEnHeureMinute(Zcalc)
            InfoJour40.DonTooltip = CStr(Zcalc)
        Else
            InfoJour40.DonText = ConvertCentiemeEnHeureMinute(25)
            InfoJour41.DonText = ConvertCentiemeEnHeureMinute(Zcalc - 25)
            InfoJour40.DonTooltip = "25"
            InfoJour41.DonTooltip = CStr(Zcalc - 25)
        End If
        Zcalc = FichePER.NombreHeuresCompensables_Nuit(True)
        If Zcalc <= 25 Then
            InfoNuit40.DonText = ConvertCentiemeEnHeureMinute(Zcalc)
            InfoNuit40.DonTooltip = CStr(Zcalc)
        Else
            InfoNuit40.DonText = ConvertCentiemeEnHeureMinute(25)
            InfoNuit41.DonText = ConvertCentiemeEnHeureMinute(Zcalc - 25)
            InfoNuit40.DonTooltip = "25"
            InfoNuit41.DonTooltip = CStr(Zcalc - 25)
        End If
        Zcalc = FichePER.NombreHeuresCompensables_JF(True)
        If Zcalc <= 25 Then
            InfoJF40.DonText = ConvertCentiemeEnHeureMinute(Zcalc)
            InfoJF40.DonTooltip = CStr(Zcalc)
        Else
            InfoJF40.DonText = ConvertCentiemeEnHeureMinute(25)
            InfoJF41.DonText = ConvertCentiemeEnHeureMinute(Zcalc - 25)
            InfoJF40.DonTooltip = "25"
            InfoJF41.DonTooltip = CStr(Zcalc - 25)
        End If

    End Sub

    Protected Sub InfoH_ValeurChange(ByVal sender As Object, ByVal e As Virtualia.Systeme.Evenements.DonneeChangeEventArgs) Handles InfoH04.ValeurChange,
        InfoH05.ValeurChange, InfoH06.ValeurChange, InfoH07.ValeurChange, InfoH08.ValeurChange, InfoH09.ValeurChange, InfoH11.ValeurChange, InfoH13.ValeurChange,
        InfoH15.ValeurChange, InfoH17.ValeurChange, InfoH20.ValeurChange, InfoH21.ValeurChange, InfoH22.ValeurChange, InfoH23.ValeurChange,
        InfoH24.ValeurChange, InfoH25.ValeurChange

        If V_UtiSession Is Nothing Then
            Exit Sub
        End If
        WsDossierHS = V_UtiSession.DossierPER.VDossier_NewHeuresSup
        If WsDossierHS Is Nothing Then
            Exit Sub
        End If
        Dim NumInfo As Integer = CInt(Strings.Right(CType(sender, Controles_VCoupleEtiDonnee).ID, 2))
        If WsDossierHS.Donnee_FicheMensuel_HS(NumInfo, EtiDateValeur.ToolTip) <> e.Valeur Then
            CType(sender, Controles_VCoupleEtiDonnee).DonBackColor = V_WebFonction.CouleurMaj
            WsDossierHS.Donnee_FicheMensuel_HS(NumInfo, EtiDateValeur.ToolTip) = e.Valeur
        End If
    End Sub

    Private Sub CmdDemande_Click(sender As Object, e As EventArgs) Handles CmdDemande.Click
        Dim Evenement As Virtualia.Systeme.Evenements.DonneeChangeEventArgs
        Evenement = New Virtualia.Systeme.Evenements.DonneeChangeEventArgs("")
        ReponseDemande(Evenement)
    End Sub

    Private ReadOnly Property ValeurLue(ByVal NoInfo As Integer) As String
        Get
            If V_UtiSession Is Nothing Then
                Return ""
            End If
            WsDossierHS = V_UtiSession.DossierPER.VDossier_NewHeuresSup
            If WsDossierHS Is Nothing Then
                Return ""
            End If
            Return WsDossierHS.Donnee_FicheMensuel_HS(NoInfo, EtiDateValeur.ToolTip)
        End Get
    End Property

    Private ReadOnly Property FicheMensuel As Virtualia.TablesObjet.ShemaPER.PER_MENSUEL_HS
        Get
            If V_UtiSession Is Nothing Then
                Return Nothing
            End If
            WsDossierHS = V_UtiSession.DossierPER.VDossier_NewHeuresSup
            If WsDossierHS Is Nothing Then
                Return Nothing
            End If
            Return WsDossierHS.FicheMensuel_HS(V_WebFonction.ViRhDates.DateTypee(EtiDateValeur.ToolTip).Year, V_WebFonction.ViRhDates.DateTypee(EtiDateValeur.ToolTip).Month)
        End Get
    End Property

    Private Function ConvertCentiemeEnHeureMinute(ByVal NbCentiemes As Double) As String
        If NbCentiemes = 0 Then
            Return ""
        End If
        Dim NbMinutes As Double = NbCentiemes * 60
        Dim NBHeures As Long = CInt(NbMinutes) \ 60
        NbMinutes -= NBHeures * 60
        Return NBHeures & " h " & Strings.Format(NbMinutes, "00")
    End Function

End Class