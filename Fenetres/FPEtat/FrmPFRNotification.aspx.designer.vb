﻿'------------------------------------------------------------------------------
' <généré automatiquement>
'     Ce code a été généré par un outil.
'
'     Les modifications apportées à ce fichier peuvent provoquer un comportement incorrect et seront perdues si
'     le code est régénéré.
' </généré automatiquement>
'------------------------------------------------------------------------------

Option Strict On
Option Explicit On


Partial Public Class FrmPFRNotification

    '''<summary>
    '''Contrôle PanelCommande.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents PanelCommande As Global.System.Web.UI.WebControls.Panel

    '''<summary>
    '''Contrôle CadreCmdStd.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CadreCmdStd As Global.System.Web.UI.WebControls.Table

    '''<summary>
    '''Contrôle CommandePdf.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CommandePdf As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''Contrôle CommandeRetour.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CommandeRetour As Global.System.Web.UI.WebControls.Button

    '''<summary>
    '''Contrôle CadreGeneral.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents CadreGeneral As Global.System.Web.UI.WebControls.Table

    '''<summary>
    '''Contrôle Notification_001.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Notification_001 As Global.Virtualia.Net.VNotificationPFR

    '''<summary>
    '''Contrôle Notification_002.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Notification_002 As Global.Virtualia.Net.VNotificationPFR

    '''<summary>
    '''Contrôle Notification_003.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Notification_003 As Global.Virtualia.Net.VNotificationPFR

    '''<summary>
    '''Contrôle Notification_004.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Notification_004 As Global.Virtualia.Net.VNotificationPFR

    '''<summary>
    '''Contrôle Notification_005.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Notification_005 As Global.Virtualia.Net.VNotificationPFR

    '''<summary>
    '''Contrôle Notification_006.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Notification_006 As Global.Virtualia.Net.VNotificationPFR

    '''<summary>
    '''Contrôle Notification_007.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Notification_007 As Global.Virtualia.Net.VNotificationPFR

    '''<summary>
    '''Contrôle Notification_008.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Notification_008 As Global.Virtualia.Net.VNotificationPFR

    '''<summary>
    '''Contrôle Notification_009.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Notification_009 As Global.Virtualia.Net.VNotificationPFR

    '''<summary>
    '''Contrôle Notification_010.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Notification_010 As Global.Virtualia.Net.VNotificationPFR

    '''<summary>
    '''Contrôle Notification_011.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Notification_011 As Global.Virtualia.Net.VNotificationPFR

    '''<summary>
    '''Contrôle Notification_012.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Notification_012 As Global.Virtualia.Net.VNotificationPFR

    '''<summary>
    '''Contrôle Notification_013.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Notification_013 As Global.Virtualia.Net.VNotificationPFR

    '''<summary>
    '''Contrôle Notification_014.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Notification_014 As Global.Virtualia.Net.VNotificationPFR

    '''<summary>
    '''Contrôle Notification_015.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Notification_015 As Global.Virtualia.Net.VNotificationPFR

    '''<summary>
    '''Contrôle Notification_016.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Notification_016 As Global.Virtualia.Net.VNotificationPFR

    '''<summary>
    '''Contrôle Notification_017.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Notification_017 As Global.Virtualia.Net.VNotificationPFR

    '''<summary>
    '''Contrôle Notification_018.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Notification_018 As Global.Virtualia.Net.VNotificationPFR

    '''<summary>
    '''Contrôle Notification_019.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Notification_019 As Global.Virtualia.Net.VNotificationPFR

    '''<summary>
    '''Contrôle Notification_020.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Notification_020 As Global.Virtualia.Net.VNotificationPFR

    '''<summary>
    '''Contrôle Notification_021.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Notification_021 As Global.Virtualia.Net.VNotificationPFR

    '''<summary>
    '''Contrôle Notification_022.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Notification_022 As Global.Virtualia.Net.VNotificationPFR

    '''<summary>
    '''Contrôle Notification_023.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Notification_023 As Global.Virtualia.Net.VNotificationPFR

    '''<summary>
    '''Contrôle Notification_024.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Notification_024 As Global.Virtualia.Net.VNotificationPFR

    '''<summary>
    '''Contrôle Notification_025.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Notification_025 As Global.Virtualia.Net.VNotificationPFR

    '''<summary>
    '''Contrôle Notification_026.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Notification_026 As Global.Virtualia.Net.VNotificationPFR

    '''<summary>
    '''Contrôle Notification_027.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Notification_027 As Global.Virtualia.Net.VNotificationPFR

    '''<summary>
    '''Contrôle Notification_028.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Notification_028 As Global.Virtualia.Net.VNotificationPFR

    '''<summary>
    '''Contrôle Notification_029.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Notification_029 As Global.Virtualia.Net.VNotificationPFR

    '''<summary>
    '''Contrôle Notification_030.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Notification_030 As Global.Virtualia.Net.VNotificationPFR

    '''<summary>
    '''Contrôle Notification_031.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Notification_031 As Global.Virtualia.Net.VNotificationPFR

    '''<summary>
    '''Contrôle Notification_032.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Notification_032 As Global.Virtualia.Net.VNotificationPFR

    '''<summary>
    '''Contrôle Notification_033.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Notification_033 As Global.Virtualia.Net.VNotificationPFR

    '''<summary>
    '''Contrôle Notification_034.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Notification_034 As Global.Virtualia.Net.VNotificationPFR

    '''<summary>
    '''Contrôle Notification_035.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Notification_035 As Global.Virtualia.Net.VNotificationPFR

    '''<summary>
    '''Contrôle Notification_036.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Notification_036 As Global.Virtualia.Net.VNotificationPFR

    '''<summary>
    '''Contrôle Notification_037.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Notification_037 As Global.Virtualia.Net.VNotificationPFR

    '''<summary>
    '''Contrôle Notification_038.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Notification_038 As Global.Virtualia.Net.VNotificationPFR

    '''<summary>
    '''Contrôle Notification_039.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Notification_039 As Global.Virtualia.Net.VNotificationPFR

    '''<summary>
    '''Contrôle Notification_040.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Notification_040 As Global.Virtualia.Net.VNotificationPFR

    '''<summary>
    '''Contrôle Notification_041.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Notification_041 As Global.Virtualia.Net.VNotificationPFR

    '''<summary>
    '''Contrôle Notification_042.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Notification_042 As Global.Virtualia.Net.VNotificationPFR

    '''<summary>
    '''Contrôle Notification_043.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Notification_043 As Global.Virtualia.Net.VNotificationPFR

    '''<summary>
    '''Contrôle Notification_044.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Notification_044 As Global.Virtualia.Net.VNotificationPFR

    '''<summary>
    '''Contrôle Notification_045.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Notification_045 As Global.Virtualia.Net.VNotificationPFR

    '''<summary>
    '''Contrôle Notification_046.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Notification_046 As Global.Virtualia.Net.VNotificationPFR

    '''<summary>
    '''Contrôle Notification_047.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Notification_047 As Global.Virtualia.Net.VNotificationPFR

    '''<summary>
    '''Contrôle Notification_048.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Notification_048 As Global.Virtualia.Net.VNotificationPFR

    '''<summary>
    '''Contrôle Notification_049.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Notification_049 As Global.Virtualia.Net.VNotificationPFR

    '''<summary>
    '''Contrôle Notification_050.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Notification_050 As Global.Virtualia.Net.VNotificationPFR

    '''<summary>
    '''Contrôle Notification_051.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Notification_051 As Global.Virtualia.Net.VNotificationPFR

    '''<summary>
    '''Contrôle Notification_052.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Notification_052 As Global.Virtualia.Net.VNotificationPFR

    '''<summary>
    '''Contrôle Notification_053.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Notification_053 As Global.Virtualia.Net.VNotificationPFR

    '''<summary>
    '''Contrôle Notification_054.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Notification_054 As Global.Virtualia.Net.VNotificationPFR

    '''<summary>
    '''Contrôle Notification_055.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Notification_055 As Global.Virtualia.Net.VNotificationPFR

    '''<summary>
    '''Contrôle Notification_056.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Notification_056 As Global.Virtualia.Net.VNotificationPFR

    '''<summary>
    '''Contrôle Notification_057.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Notification_057 As Global.Virtualia.Net.VNotificationPFR

    '''<summary>
    '''Contrôle Notification_058.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Notification_058 As Global.Virtualia.Net.VNotificationPFR

    '''<summary>
    '''Contrôle Notification_059.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Notification_059 As Global.Virtualia.Net.VNotificationPFR

    '''<summary>
    '''Contrôle Notification_060.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Notification_060 As Global.Virtualia.Net.VNotificationPFR

    '''<summary>
    '''Contrôle Notification_061.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Notification_061 As Global.Virtualia.Net.VNotificationPFR

    '''<summary>
    '''Contrôle Notification_062.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Notification_062 As Global.Virtualia.Net.VNotificationPFR

    '''<summary>
    '''Contrôle Notification_063.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Notification_063 As Global.Virtualia.Net.VNotificationPFR

    '''<summary>
    '''Contrôle Notification_064.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Notification_064 As Global.Virtualia.Net.VNotificationPFR

    '''<summary>
    '''Contrôle Notification_065.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Notification_065 As Global.Virtualia.Net.VNotificationPFR

    '''<summary>
    '''Contrôle Notification_066.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Notification_066 As Global.Virtualia.Net.VNotificationPFR

    '''<summary>
    '''Contrôle Notification_067.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Notification_067 As Global.Virtualia.Net.VNotificationPFR

    '''<summary>
    '''Contrôle Notification_068.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Notification_068 As Global.Virtualia.Net.VNotificationPFR

    '''<summary>
    '''Contrôle Notification_069.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Notification_069 As Global.Virtualia.Net.VNotificationPFR

    '''<summary>
    '''Contrôle Notification_070.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Notification_070 As Global.Virtualia.Net.VNotificationPFR

    '''<summary>
    '''Contrôle Notification_071.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Notification_071 As Global.Virtualia.Net.VNotificationPFR

    '''<summary>
    '''Contrôle Notification_072.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Notification_072 As Global.Virtualia.Net.VNotificationPFR

    '''<summary>
    '''Contrôle Notification_073.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Notification_073 As Global.Virtualia.Net.VNotificationPFR

    '''<summary>
    '''Contrôle Notification_074.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Notification_074 As Global.Virtualia.Net.VNotificationPFR

    '''<summary>
    '''Contrôle Notification_075.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Notification_075 As Global.Virtualia.Net.VNotificationPFR

    '''<summary>
    '''Contrôle Notification_076.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Notification_076 As Global.Virtualia.Net.VNotificationPFR

    '''<summary>
    '''Contrôle Notification_077.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Notification_077 As Global.Virtualia.Net.VNotificationPFR

    '''<summary>
    '''Contrôle Notification_078.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Notification_078 As Global.Virtualia.Net.VNotificationPFR

    '''<summary>
    '''Contrôle Notification_079.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Notification_079 As Global.Virtualia.Net.VNotificationPFR

    '''<summary>
    '''Contrôle Notification_080.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Notification_080 As Global.Virtualia.Net.VNotificationPFR

    '''<summary>
    '''Contrôle Notification_081.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Notification_081 As Global.Virtualia.Net.VNotificationPFR

    '''<summary>
    '''Contrôle Notification_082.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Notification_082 As Global.Virtualia.Net.VNotificationPFR

    '''<summary>
    '''Contrôle Notification_083.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Notification_083 As Global.Virtualia.Net.VNotificationPFR

    '''<summary>
    '''Contrôle Notification_084.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Notification_084 As Global.Virtualia.Net.VNotificationPFR

    '''<summary>
    '''Contrôle Notification_085.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Notification_085 As Global.Virtualia.Net.VNotificationPFR

    '''<summary>
    '''Contrôle Notification_086.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Notification_086 As Global.Virtualia.Net.VNotificationPFR

    '''<summary>
    '''Contrôle Notification_087.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Notification_087 As Global.Virtualia.Net.VNotificationPFR

    '''<summary>
    '''Contrôle Notification_088.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Notification_088 As Global.Virtualia.Net.VNotificationPFR

    '''<summary>
    '''Contrôle Notification_089.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Notification_089 As Global.Virtualia.Net.VNotificationPFR

    '''<summary>
    '''Contrôle Notification_090.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Notification_090 As Global.Virtualia.Net.VNotificationPFR

    '''<summary>
    '''Contrôle Notification_091.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Notification_091 As Global.Virtualia.Net.VNotificationPFR

    '''<summary>
    '''Contrôle Notification_092.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Notification_092 As Global.Virtualia.Net.VNotificationPFR

    '''<summary>
    '''Contrôle Notification_093.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Notification_093 As Global.Virtualia.Net.VNotificationPFR

    '''<summary>
    '''Contrôle Notification_094.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Notification_094 As Global.Virtualia.Net.VNotificationPFR

    '''<summary>
    '''Contrôle Notification_095.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Notification_095 As Global.Virtualia.Net.VNotificationPFR

    '''<summary>
    '''Contrôle Notification_096.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Notification_096 As Global.Virtualia.Net.VNotificationPFR

    '''<summary>
    '''Contrôle Notification_097.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Notification_097 As Global.Virtualia.Net.VNotificationPFR

    '''<summary>
    '''Contrôle Notification_098.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Notification_098 As Global.Virtualia.Net.VNotificationPFR

    '''<summary>
    '''Contrôle Notification_099.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Notification_099 As Global.Virtualia.Net.VNotificationPFR

    '''<summary>
    '''Contrôle Notification_100.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Notification_100 As Global.Virtualia.Net.VNotificationPFR

    '''<summary>
    '''Contrôle Notification_101.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Notification_101 As Global.Virtualia.Net.VNotificationPFR

    '''<summary>
    '''Contrôle Notification_102.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Notification_102 As Global.Virtualia.Net.VNotificationPFR

    '''<summary>
    '''Contrôle Notification_103.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Notification_103 As Global.Virtualia.Net.VNotificationPFR

    '''<summary>
    '''Contrôle Notification_104.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Notification_104 As Global.Virtualia.Net.VNotificationPFR

    '''<summary>
    '''Contrôle Notification_105.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Notification_105 As Global.Virtualia.Net.VNotificationPFR

    '''<summary>
    '''Contrôle Notification_106.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Notification_106 As Global.Virtualia.Net.VNotificationPFR

    '''<summary>
    '''Contrôle Notification_107.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Notification_107 As Global.Virtualia.Net.VNotificationPFR

    '''<summary>
    '''Contrôle Notification_108.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Notification_108 As Global.Virtualia.Net.VNotificationPFR

    '''<summary>
    '''Contrôle Notification_109.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Notification_109 As Global.Virtualia.Net.VNotificationPFR

    '''<summary>
    '''Contrôle Notification_110.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Notification_110 As Global.Virtualia.Net.VNotificationPFR

    '''<summary>
    '''Contrôle Notification_111.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Notification_111 As Global.Virtualia.Net.VNotificationPFR

    '''<summary>
    '''Contrôle Notification_112.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Notification_112 As Global.Virtualia.Net.VNotificationPFR

    '''<summary>
    '''Contrôle Notification_113.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Notification_113 As Global.Virtualia.Net.VNotificationPFR

    '''<summary>
    '''Contrôle Notification_114.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Notification_114 As Global.Virtualia.Net.VNotificationPFR

    '''<summary>
    '''Contrôle Notification_115.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Notification_115 As Global.Virtualia.Net.VNotificationPFR

    '''<summary>
    '''Contrôle Notification_116.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Notification_116 As Global.Virtualia.Net.VNotificationPFR

    '''<summary>
    '''Contrôle Notification_117.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Notification_117 As Global.Virtualia.Net.VNotificationPFR

    '''<summary>
    '''Contrôle Notification_118.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Notification_118 As Global.Virtualia.Net.VNotificationPFR

    '''<summary>
    '''Contrôle Notification_119.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Notification_119 As Global.Virtualia.Net.VNotificationPFR

    '''<summary>
    '''Contrôle Notification_120.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Notification_120 As Global.Virtualia.Net.VNotificationPFR

    '''<summary>
    '''Contrôle Notification_121.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Notification_121 As Global.Virtualia.Net.VNotificationPFR

    '''<summary>
    '''Contrôle Notification_122.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Notification_122 As Global.Virtualia.Net.VNotificationPFR

    '''<summary>
    '''Contrôle Notification_123.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Notification_123 As Global.Virtualia.Net.VNotificationPFR

    '''<summary>
    '''Contrôle Notification_124.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Notification_124 As Global.Virtualia.Net.VNotificationPFR

    '''<summary>
    '''Contrôle Notification_125.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Notification_125 As Global.Virtualia.Net.VNotificationPFR

    '''<summary>
    '''Contrôle Notification_126.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Notification_126 As Global.Virtualia.Net.VNotificationPFR

    '''<summary>
    '''Contrôle Notification_127.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Notification_127 As Global.Virtualia.Net.VNotificationPFR

    '''<summary>
    '''Contrôle Notification_128.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Notification_128 As Global.Virtualia.Net.VNotificationPFR

    '''<summary>
    '''Contrôle Notification_129.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Notification_129 As Global.Virtualia.Net.VNotificationPFR

    '''<summary>
    '''Contrôle Notification_130.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Notification_130 As Global.Virtualia.Net.VNotificationPFR

    '''<summary>
    '''Contrôle Notification_131.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Notification_131 As Global.Virtualia.Net.VNotificationPFR

    '''<summary>
    '''Contrôle Notification_132.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Notification_132 As Global.Virtualia.Net.VNotificationPFR

    '''<summary>
    '''Contrôle Notification_133.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Notification_133 As Global.Virtualia.Net.VNotificationPFR

    '''<summary>
    '''Contrôle Notification_134.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Notification_134 As Global.Virtualia.Net.VNotificationPFR

    '''<summary>
    '''Contrôle Notification_135.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Notification_135 As Global.Virtualia.Net.VNotificationPFR

    '''<summary>
    '''Contrôle Notification_136.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Notification_136 As Global.Virtualia.Net.VNotificationPFR

    '''<summary>
    '''Contrôle Notification_137.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Notification_137 As Global.Virtualia.Net.VNotificationPFR

    '''<summary>
    '''Contrôle Notification_138.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Notification_138 As Global.Virtualia.Net.VNotificationPFR

    '''<summary>
    '''Contrôle Notification_139.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Notification_139 As Global.Virtualia.Net.VNotificationPFR

    '''<summary>
    '''Contrôle Notification_140.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Notification_140 As Global.Virtualia.Net.VNotificationPFR

    '''<summary>
    '''Contrôle Notification_141.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Notification_141 As Global.Virtualia.Net.VNotificationPFR

    '''<summary>
    '''Contrôle Notification_142.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Notification_142 As Global.Virtualia.Net.VNotificationPFR

    '''<summary>
    '''Contrôle Notification_143.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Notification_143 As Global.Virtualia.Net.VNotificationPFR

    '''<summary>
    '''Contrôle Notification_144.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Notification_144 As Global.Virtualia.Net.VNotificationPFR

    '''<summary>
    '''Contrôle Notification_145.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Notification_145 As Global.Virtualia.Net.VNotificationPFR

    '''<summary>
    '''Contrôle Notification_146.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Notification_146 As Global.Virtualia.Net.VNotificationPFR

    '''<summary>
    '''Contrôle Notification_147.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Notification_147 As Global.Virtualia.Net.VNotificationPFR

    '''<summary>
    '''Contrôle Notification_148.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Notification_148 As Global.Virtualia.Net.VNotificationPFR

    '''<summary>
    '''Contrôle Notification_149.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Notification_149 As Global.Virtualia.Net.VNotificationPFR

    '''<summary>
    '''Contrôle Notification_150.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Notification_150 As Global.Virtualia.Net.VNotificationPFR

    '''<summary>
    '''Contrôle Notification_151.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Notification_151 As Global.Virtualia.Net.VNotificationPFR

    '''<summary>
    '''Contrôle Notification_152.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Notification_152 As Global.Virtualia.Net.VNotificationPFR

    '''<summary>
    '''Contrôle Notification_153.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Notification_153 As Global.Virtualia.Net.VNotificationPFR

    '''<summary>
    '''Contrôle Notification_154.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Notification_154 As Global.Virtualia.Net.VNotificationPFR

    '''<summary>
    '''Contrôle Notification_155.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Notification_155 As Global.Virtualia.Net.VNotificationPFR

    '''<summary>
    '''Contrôle Notification_156.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Notification_156 As Global.Virtualia.Net.VNotificationPFR

    '''<summary>
    '''Contrôle Notification_157.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Notification_157 As Global.Virtualia.Net.VNotificationPFR

    '''<summary>
    '''Contrôle Notification_158.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Notification_158 As Global.Virtualia.Net.VNotificationPFR

    '''<summary>
    '''Contrôle Notification_159.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Notification_159 As Global.Virtualia.Net.VNotificationPFR

    '''<summary>
    '''Contrôle Notification_160.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Notification_160 As Global.Virtualia.Net.VNotificationPFR

    '''<summary>
    '''Contrôle Notification_161.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Notification_161 As Global.Virtualia.Net.VNotificationPFR

    '''<summary>
    '''Contrôle Notification_162.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Notification_162 As Global.Virtualia.Net.VNotificationPFR

    '''<summary>
    '''Contrôle Notification_163.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Notification_163 As Global.Virtualia.Net.VNotificationPFR

    '''<summary>
    '''Contrôle Notification_164.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Notification_164 As Global.Virtualia.Net.VNotificationPFR

    '''<summary>
    '''Contrôle Notification_165.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Notification_165 As Global.Virtualia.Net.VNotificationPFR

    '''<summary>
    '''Contrôle Notification_166.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Notification_166 As Global.Virtualia.Net.VNotificationPFR

    '''<summary>
    '''Contrôle Notification_167.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Notification_167 As Global.Virtualia.Net.VNotificationPFR

    '''<summary>
    '''Contrôle Notification_168.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Notification_168 As Global.Virtualia.Net.VNotificationPFR

    '''<summary>
    '''Contrôle Notification_169.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Notification_169 As Global.Virtualia.Net.VNotificationPFR

    '''<summary>
    '''Contrôle Notification_170.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Notification_170 As Global.Virtualia.Net.VNotificationPFR

    '''<summary>
    '''Contrôle Notification_171.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Notification_171 As Global.Virtualia.Net.VNotificationPFR

    '''<summary>
    '''Contrôle Notification_172.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Notification_172 As Global.Virtualia.Net.VNotificationPFR

    '''<summary>
    '''Contrôle Notification_173.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Notification_173 As Global.Virtualia.Net.VNotificationPFR

    '''<summary>
    '''Contrôle Notification_174.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Notification_174 As Global.Virtualia.Net.VNotificationPFR

    '''<summary>
    '''Contrôle Notification_175.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Notification_175 As Global.Virtualia.Net.VNotificationPFR

    '''<summary>
    '''Contrôle Notification_176.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Notification_176 As Global.Virtualia.Net.VNotificationPFR

    '''<summary>
    '''Contrôle Notification_177.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Notification_177 As Global.Virtualia.Net.VNotificationPFR

    '''<summary>
    '''Contrôle Notification_178.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Notification_178 As Global.Virtualia.Net.VNotificationPFR

    '''<summary>
    '''Contrôle Notification_179.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Notification_179 As Global.Virtualia.Net.VNotificationPFR

    '''<summary>
    '''Contrôle Notification_180.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Notification_180 As Global.Virtualia.Net.VNotificationPFR

    '''<summary>
    '''Contrôle Notification_181.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Notification_181 As Global.Virtualia.Net.VNotificationPFR

    '''<summary>
    '''Contrôle Notification_182.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Notification_182 As Global.Virtualia.Net.VNotificationPFR

    '''<summary>
    '''Contrôle Notification_183.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Notification_183 As Global.Virtualia.Net.VNotificationPFR

    '''<summary>
    '''Contrôle Notification_184.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Notification_184 As Global.Virtualia.Net.VNotificationPFR

    '''<summary>
    '''Contrôle Notification_185.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Notification_185 As Global.Virtualia.Net.VNotificationPFR

    '''<summary>
    '''Contrôle Notification_186.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Notification_186 As Global.Virtualia.Net.VNotificationPFR

    '''<summary>
    '''Contrôle Notification_187.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Notification_187 As Global.Virtualia.Net.VNotificationPFR

    '''<summary>
    '''Contrôle Notification_188.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Notification_188 As Global.Virtualia.Net.VNotificationPFR

    '''<summary>
    '''Contrôle Notification_189.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Notification_189 As Global.Virtualia.Net.VNotificationPFR

    '''<summary>
    '''Contrôle Notification_190.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Notification_190 As Global.Virtualia.Net.VNotificationPFR

    '''<summary>
    '''Contrôle Notification_191.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Notification_191 As Global.Virtualia.Net.VNotificationPFR

    '''<summary>
    '''Contrôle Notification_192.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Notification_192 As Global.Virtualia.Net.VNotificationPFR

    '''<summary>
    '''Contrôle Notification_193.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Notification_193 As Global.Virtualia.Net.VNotificationPFR

    '''<summary>
    '''Contrôle Notification_194.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Notification_194 As Global.Virtualia.Net.VNotificationPFR

    '''<summary>
    '''Contrôle Notification_195.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Notification_195 As Global.Virtualia.Net.VNotificationPFR

    '''<summary>
    '''Contrôle Notification_196.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Notification_196 As Global.Virtualia.Net.VNotificationPFR

    '''<summary>
    '''Contrôle Notification_197.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Notification_197 As Global.Virtualia.Net.VNotificationPFR

    '''<summary>
    '''Contrôle Notification_198.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Notification_198 As Global.Virtualia.Net.VNotificationPFR

    '''<summary>
    '''Contrôle Notification_199.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Notification_199 As Global.Virtualia.Net.VNotificationPFR

    '''<summary>
    '''Contrôle Notification_200.
    '''</summary>
    '''<remarks>
    '''Champ généré automatiquement.
    '''Pour modifier, déplacez la déclaration de champ du fichier de concepteur dans le fichier code-behind.
    '''</remarks>
    Protected WithEvents Notification_200 As Global.Virtualia.Net.VNotificationPFR

    '''<summary>
    '''Propriété Master.
    '''</summary>
    '''<remarks>
    '''Propriété générée automatiquement.
    '''</remarks>
    Public Shadows ReadOnly Property Master() As Virtualia.Net.VirtualiaMain
        Get
            Return CType(MyBase.Master, Virtualia.Net.VirtualiaMain)
        End Get
    End Property
End Class
