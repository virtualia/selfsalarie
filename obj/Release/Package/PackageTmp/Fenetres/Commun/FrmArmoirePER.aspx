﻿<%@ Page Language="vb" AutoEventWireup="false" MasterPageFile="~/PagesMaitres/VirtualiaMain.Master" CodeBehind="FrmArmoirePER.aspx.vb" 
    Inherits="Virtualia.Net.FrmArmoirePER" MaintainScrollPositionOnPostback="True" %>

<%@ MasterType VirtualPath="~/PagesMaitres/VirtualiaMain.master" %>
<%@ Register src="~/Controles/Commun/VArmoirePER.ascx" tagname="VArmoire" tagprefix="Virtualia" %>

<asp:Content ID="Corps" ContentPlaceHolderID="Principal" runat="server">
    <asp:Table ID="CadreGeneral" runat="server" HorizontalAlign="Center" Width="1050px" BackColor="#5E9598">
         <asp:TableRow>
            <asp:TableCell HorizontalAlign="Center">
                <Virtualia:VArmoire ID="ListeArmoire" runat="server" />
            </asp:TableCell>
         </asp:TableRow>
    </asp:Table>
</asp:Content>