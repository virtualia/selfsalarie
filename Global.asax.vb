﻿Imports System.Web.SessionState
Imports VI = Virtualia.Systeme.Constantes
Public Class Global_asax
    Inherits System.Web.HttpApplication
    Private Const SysFicLog As String = "WebSelfSalarie.log"
    Private SysCodeIso As System.Text.Encoding = System.Text.Encoding.UTF8
    Private VirObjetGlobal As Virtualia.Net.Session.ObjetGlobal

    Sub Application_Start(ByVal sender As Object, ByVal e As EventArgs)
        ' Se déclenche lorsque l'application est démarrée
        Dim Utilisateur As String
        Dim Pw As String
        Dim NoBd As Integer

        Pw = System.Configuration.ConfigurationManager.AppSettings("Password")
        Utilisateur = System.Configuration.ConfigurationManager.AppSettings("NomUtilisateur")
        NoBd = CInt(System.Configuration.ConfigurationManager.AppSettings("NumeroDatabase"))

        VirObjetGlobal = New Virtualia.Net.Session.ObjetGlobal

        Try
            Select Case VirObjetGlobal.VirServiceServeur.OuvertureSession(System.Configuration.ConfigurationManager.AppSettings("NomUtilisateur"),
                                                          System.Configuration.ConfigurationManager.AppSettings("Password"),
                                                          CInt(System.Configuration.ConfigurationManager.AppSettings("NumeroDatabase")))
                Case Virtualia.Systeme.Constantes.CnxUtilisateur.CnxOK, Virtualia.Systeme.Constantes.CnxUtilisateur.CnxDejaConnecte, Virtualia.Systeme.Constantes.CnxUtilisateur.CnxReprise, Virtualia.Systeme.Constantes.CnxUtilisateur.CnxVirtualia
                    Application.Add("Database", CInt(System.Configuration.ConfigurationManager.AppSettings("NumeroDatabase")))
                Case Else
                    Call EcrireLog("---- Erreur à la connexion au WCF ServiceServeur ---")
                    Me.Dispose()
                    Exit Sub
            End Select
        Catch ex As Exception
            Call EcrireLog("---- Erreur le WCF ServiceServeur ne répond pas ---" & Strings.Space(1) & ex.Message)
            Me.Dispose()
            Exit Sub
        End Try

        Try
            VirObjetGlobal = New Virtualia.Net.Session.ObjetGlobal(System.Configuration.ConfigurationManager.AppSettings("NomUtilisateur"), CInt(System.Configuration.ConfigurationManager.AppSettings("NumeroDatabase")))
            Application.Add("VirGlobales", VirObjetGlobal)
        Catch ex As Exception
            Call EcrireLog("---- Erreur au Chargement de l'application ---" & Strings.Space(1) & ex.Message)
            Me.Dispose()
            Exit Sub
        End Try

    End Sub

    Sub Session_Start(ByVal sender As Object, ByVal e As EventArgs)
        ' Se déclenche lorsque la session est démarrée
    End Sub

    Sub Application_BeginRequest(ByVal sender As Object, ByVal e As EventArgs)
        ' Se déclenche au début de chaque demande
    End Sub

    Sub Application_AuthenticateRequest(ByVal sender As Object, ByVal e As EventArgs)
        ' Se déclenche lors d'une tentative d'authentification de l'utilisation
    End Sub

    Sub Application_Error(ByVal sender As Object, ByVal e As EventArgs)
        ' Se déclenche lorsqu'une erreur se produit
    End Sub

    Sub Session_End(ByVal sender As Object, ByVal e As EventArgs)
        ' Se déclenche lorsque la session se termine
        Try
            VirObjetGlobal = CType(Application.Item("VirGlobales"), Virtualia.Net.Session.ObjetGlobal)
            VirObjetGlobal.LibererSessionModule(Session.SessionID)
        Catch ex As Exception
            Exit Sub
        End Try
    End Sub

    Sub Application_End(ByVal sender As Object, ByVal e As EventArgs)
        ' Se déclenche lorsque l'application se termine
    End Sub

    Private Sub EcrireLog(ByVal Msg As String)
        Dim FicStream As System.IO.FileStream
        Dim FicWriter As System.IO.StreamWriter
        Dim NomLog As String

        NomLog = VI.DossierVirtualiaService("Logs") & SysFicLog
        FicStream = New System.IO.FileStream(NomLog, IO.FileMode.Append, IO.FileAccess.Write)
        FicWriter = New System.IO.StreamWriter(FicStream, SysCodeIso)
        FicWriter.WriteLine(Format(System.DateTime.Now, "g") & Space(1) & Msg)
        FicWriter.Flush()
        FicWriter.Close()
    End Sub

End Class