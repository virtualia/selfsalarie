﻿Option Explicit On
Option Strict On
Option Compare Text
Namespace WebAppli
    Namespace SelfV4
        Public Class RecapitulatifActivite
            Private WsActivite As String
            Private WsTache As String
            Private WsTotal As Double
            Private WsPourcentage As Double

            Public Property Activite As String
                Get
                    Return WsActivite
                End Get
                Set(value As String)
                    WsActivite = value
                End Set
            End Property

            Public Property Tache As String
                Get
                    Return WsTache
                End Get
                Set(value As String)
                    WsTache = value
                End Set
            End Property

            Public Property Total As Double
                Get
                    Return WsTotal
                End Get
                Set(value As Double)
                    WsTotal = value
                End Set
            End Property

            Public Property Pourcentage As Double
                Get
                    Return WsPourcentage
                End Get
                Set(value As Double)
                    WsPourcentage = value
                End Set
            End Property
        End Class
    End Namespace
End Namespace